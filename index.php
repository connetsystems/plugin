<?php
/**
 * Niel's Hacks 101
 * 
 * Added Quick and Dirty Capslock checking, might be a better way
 * 
 */
/**
 * This simple check insures that our users are accessing via HTTPS instead of HTTP.
 * If they arrive via HTTP, we redirect them immediately to HTTPS instead
 */
LoginHelper::assertHttps();

/**
 * Open the session, then destroy the data if the user is in the process of logging out
 */
session_start();

if (isset($_GET['logged']) && $_GET['logged'] == 'out') {
   session_unset();
}

require_once('php/config/dbConn.php');
require_once('php/login.php');

if (isset($_GET['err'])) {
   $err1 = $_GET['err'];
} else {
   $err1 = '';
}

//if this is switched to true, it means we are on Connet Systems main page, and not a reseller page, so we can show our own support details and password reset
$isConnetMain = false;

$serverUrlSubStr = strstr($_SERVER['HTTP_HOST'], '.', true);

//if www is included in the URL, cut that out, and get the next part of the prefix
if ($serverUrlSubStr == 'www') {
   $serverUrlSubStrPOS = strpos($_SERVER['HTTP_HOST'], '.');
   $serverUrlSubStr = substr($_SERVER['HTTP_HOST'], $serverUrlSubStrPOS + 1);
   $serverUrlSubStr = strstr($serverUrlSubStr, '.', true);
}


//check if it is a mian Connet client and not a resseler sub account
if ($serverUrlSubStr == "plugin" || $serverUrlSubStr == "portal2" || $serverUrlSubStr == "plugin-beta") {
   $isConnetMain = true;
}

/*
 * Temporary Fix for using URL SUFFIXES for whitelabelled sites
 * 
 */
$pdo = getPDOMasterCore();
$account_details = false;
list($suffix_host) = explode(':', trim(strtolower($_SERVER['HTTP_HOST'])));

foreach ($pdo->query('SELECT * FROM `core`.`core_account_settings` WHERE `url_suffix` IS NOT NULL')->fetchAll(PDO::FETCH_ASSOC) as $row) {

   $url_suffix = trim(strtolower($row['url_suffix']));
   if (strpos($url_suffix, '.') !== false) { //At least one segment
      $regex = '/' . str_replace('\*', '.*?', str_replace('\?', '.?', preg_quote($url_suffix))) . '$/';
      if (preg_match($regex, $suffix_host, $matches)) {

         if (!isset($account_details['_suffix_match']) || strlen($account_details['_suffix_match']) < strlen($matches[0])) {

            $account_details = array(
                '_suffix_regex' => $regex,
                '_suffix_match' => $matches[0]) + $row;
         }
      }
   }
}

if (!isset($account_details['account_id'])) {
   /*
    * NEW! SQL search for prefix URL to get resources for an account prefix
    * THE HANDLING OF WHITELABELLING NEEDS A REWRITE!!!
    */
   $conn = getPDOMasterCore();


//prepare the data
   $data = array(':url_prefix' => $serverUrlSubStr);

//prepare the query
   $stmt_acc_deets = $conn->prepare('SELECT * FROM core.core_account_settings WHERE url_prefix = :url_prefix ');

//set the fetch mode
   $stmt_acc_deets->setFetchMode(PDO::FETCH_ASSOC);

//execute it
   $stmt_acc_deets->execute($data);

//fetch the data
   $account_details = $stmt_acc_deets->fetch();
}

if (!isset($account_details['account_id'])) {
   //default to connet main
   $accountId = 1;

   //set the image URL to connet default
   $imageUrl = 'img/skinlogos/' . $accountId . '/' . $accountId . '.png';
   $faviconUrl = 'img/skinlogos/1/1_icon.png';
   ;
} else {
   $accountId = $account_details['account_id'];

   if (isset($account_details['login_logo']) && isset($account_details['tab_logo'])) {
      //set the image URL from the settings record
      $imageUrl = str_replace("../", "", $account_details['login_logo']);
      $faviconUrl = str_replace("../", "", $account_details['tab_logo']);
   } else {
      //set the image URL to connet default
      $imageUrl = 'img/skinlogos/' . $accountId . '/' . $accountId . '.png';
      $faviconUrl = 'img/skinlogos/1/1_icon.png';
      ;
   }
}
?>

<!DOCTYPE html>
<html class="bg-black">
   <head>
      <meta charset="UTF-8">
      <title><?php echo ucfirst($serverUrlSubStr); ?> Portal Log In</title>
      <meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
      <!-- bootstrap 3.0.2 -->
      <link href="css/bootstrap.min.css" rel="stylesheet" type="text/css" />
      <!-- font Awesome -->
      <link href="css/font-awesome.min.css" rel="stylesheet" type="text/css" />
      <!-- Theme style -->
      <link href="css/AdminLTE.css" rel="stylesheet" type="text/css" />

      <link rel="icon" type="image/png" href="<?php echo $faviconUrl; ?>">

      <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
      <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
      <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
      <![endif]-->
   </head>
   <body class="bg-black">
      <div class="form-box" id="login-box">
         <div class="header">
            <img src="<?php echo $imageUrl ?>" />
            <!--a style="background-image: url('img/ConnetSmall.png');   background-repeat: no-repeat; background-position: center center;" href="dashboard.php" class="logo">
                Sign In
            </a-->
         </div>
         <form action="index.php" method="post">
            <div class="body bg-gray">

               <?php
               if ($err1 == 1) {
                  echo '<div class="callout callout-danger" style="margin-top:-10px; margin-left:-20px; margin-right:-20px;">';
                  echo '<div style="width:100%;margin-bottom:-10px;">';
                  echo '<h4>You have entered invalid login details.</h4>';
                  echo '</div>';
                  echo '<div style="float:right; margin-top:-20px; margin-right:0px;">';
                  echo '<i class="fa fa-exclamation-triangle" style="color:#dFb5b4; font-size:16pt;"></i>';
                  echo '</div>';
                  echo '</div>';
               }
               if ($err1 == 2) {
                  echo '<div class="callout callout-danger" style="margin-top:-10px; margin-left:-20px; margin-right:-20px;">';
                  echo '<div style="width:100%;margin-bottom:-10px;">';
                  echo "<h4>You don't have an account assigned to your username.</h4>";
                  echo '</div>';
                  echo '<div style="float:right; margin-top:-20px; margin-right:0px;">';
                  echo '<i class="fa fa-exclamation-triangle" style="color:#dFb5b4; font-size:16pt;"></i>';
                  echo '</div>';
                  echo '</div>';
               }
               if ($err1 == 3) {
                  echo '<div class="callout callout-danger" style="margin-top:-10px; margin-left:-20px; margin-right:-20px;">';
                  echo '<div style="width:100%;margin-bottom:-10px;">';
                  echo "<h4>You don't have a service assigned to your username.</h4>";
                  echo '</div>';
                  echo '<div style="float:right; margin-top:-20px; margin-right:0px;">';
                  echo '<i class="fa fa-exclamation-triangle" style="color:#dFb5b4; font-size:16pt;"></i>';
                  echo '</div>';
                  echo '</div>';
               }
               if ($err1 == 'acc') {
                  echo '<div class="callout callout-danger" style="margin-top:-10px; margin-left:-20px; margin-right:-20px;">';
                  echo '<div style="width:100%;margin-bottom:-10px;">';
                  echo "<h4>Your account has been disabled.</h4>";
                  echo '</div>';
                  echo '<div style="float:right; margin-top:-20px; margin-right:0px;">';
                  echo '<i class="fa fa-exclamation-triangle" style="color:#dFb5b4; font-size:16pt;"></i>';
                  echo '</div>';
                  echo '</div>';
               }
               if ($err1 == 'ser') {
                  echo '<div class="callout callout-danger" style="margin-top:-10px; margin-left:-20px; margin-right:-20px;">';
                  echo '<div style="width:100%;margin-bottom:-10px;">';
                  echo "<h4>Your service has been disabled.</h4>";
                  echo '</div>';
                  echo '<div style="float:right; margin-top:-20px; margin-right:0px;">';
                  echo '<i class="fa fa-exclamation-triangle" style="color:#dFb5b4; font-size:16pt;"></i>';
                  echo '</div>';
                  echo '</div>';
               }
               if ($err1 == 'use') {
                  echo '<div class="callout callout-danger" style="margin-top:-10px; margin-left:-20px; margin-right:-20px;">';
                  echo '<div style="width:100%;margin-bottom:-10px;">';
                  echo "<h4>Your user has been disabled.</h4>";
                  echo '</div>';
                  echo '<div style="float:right; margin-top:-20px; margin-right:0px;">';
                  echo '<i class="fa fa-exclamation-triangle" style="color:#dFb5b4; font-size:16pt;"></i>';
                  echo '</div>';
                  echo '</div>';
               }
               /* echo '<div class="box" style="margin-top:-12px;">';
                 echo '<center>';
                 echo '<div class="callout callout-danger" style="border-color: #fcf2f2; margin-left:-20px; margin-right:-20px;">';
                 echo '<i class="fa fa-exclamation-triangle fa-2" style="color:#B94A48; padding-bottom:10px;"></i>';
                 echo '<h4>You have entered invalid login details.</h4>';
                 echo '</div>';
                 echo '</center>';
                 echo '</div>'; */
               /* echo '<div class="alert alert-danger alert-dismissable">';
                 echo '<i class="fa fa-ban"></i>';
                 echo '<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>';
                 echo '<b>Incorrect Password!</b></div>'; */
               ?>
               <div class="form-group">
                  <label for="un">Username</label>
                  <input type="text" name="un" class="form-control" placeholder="Please enter your username..." tabindex="1"/>
               </div>
               <div class="form-group">
                  <label for="ps">Password</label>
                  <input id="password" type="password" name="pw" class="form-control" placeholder="Please enter your selected password..." tabindex="2"/>

               </div>
               <div id="capslock-warning" style="display:none; text-align:center;">
                  <span class="fa fa-warning" style="color:red;"></span>
                  Caps Lock is on
               </div>
               <?php
//only show for NON RESELLER
               if ($isConnetMain) {
                  echo '<div style="text-align: right;"><a href="#" id="btn_forgot">[I forgot my password]</a></div>';
               }
               ?>
               <!--div class="form-group">
                   <input type="checkbox" name="remember_me"/> Remember me
               </div-->
               <input type="hidden" name="check"/>
            </div>
            <div class="footer" style="text-align:center;">
               <button type="submit" class="btn bg-myColour btn-block" style="color:white" tabindex="3">Sign me in</button>
               <!--a href="register.html" class="text-center">Register a new membership</a-->
               <?php
//only show for NON RESELLER
               if ($isConnetMain) {
                  echo '<small>If you are struggling to log in, please contact us at <b><a href="mailto:support@connet-systems.com">support@connet-systems.com</a></b> or call us at <b>(012)9914328</b>.</small>';
               }
               ?>
            </div>
         </form>
      </div>


      <link rel="stylesheet" href="//code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">


      <!-- jQuery 2.0.2 -->
      <script src="js/jquery-2.1.4.min.js"></script>


      <!-- Bootstrap -->
      <!--script src="../js/bootstrap.min.js" type="text/javascript"></script-->



      <script type="text/javascript">

         //make sure JQUERY is ready
         $(document).ready(function (e) {
            var const_support = "Please contact Connet Systems support at support@connet-systems.com or call us at (012) 991 4328.";

            //assign a click listener to the forgot password link
            $("#btn_forgot").on("click", function (e)
            {
               e.preventDefault();
               //prompt the user for a username
               var email = prompt("Please enter your Email Address to reset your password.", "");

               //check that the username is actually set
               if (email != null && email != "")
               {
                  //now run anq AJAX call that submits the email address and sends the password reset
                  $.post("php/ajaxRequestPassword.php", {email: email}).done(function (data)
                  {
                     //process the response and inform the user accordingly
                     var json = $.parseJSON(data);
                     if (json.success) {
                        alert("Success! A reset password link has been sent to your email address.");
                     } else if (json.error && json.error == "no_msisdn") {
                        alert("Your email address is not registered on our system. " + const_support);
                     } else if (json.error && json.error == "not_registered") {
                        alert("You are not registered on our system. " + const_support);
                     } else {
                        alert("There was a server error. " + const_support);
                     }
                  }).fail(function (data)
                  {
                     alert("There was a server error. " + const_support);
                  });
               }
            });

            /* Caps Lock Warning */
            /* Source: http://stackoverflow.com/questions/348792/how-do-you-tell-if-caps-lock-is-on-using-javascript */
            $("#password").keypress(function (e) {

               var s = String.fromCharCode(e.which);
               if ((s.toUpperCase() === s && s.toLowerCase() !== s && !e.shiftKey) || (s.toUpperCase() !== s && s.toLowerCase() === s && e.shiftKey)) {

                  $("#capslock-warning").show();

               } else if ((s.toLowerCase() === s && s.toUpperCase() !== s && !e.shiftKey) || (s.toLowerCase() !== s && s.toUpperCase() === s && e.shiftKey)) {

                  $("#capslock-warning").hide();
               }
            });
         });

      </script>
   </body>
</html>