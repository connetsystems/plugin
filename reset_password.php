<?php
/**
 * This simple check insures that our users are accessing via HTTPS instead of HTTP.
 * If they arrive via HTTP, we redirect them immediately to HTTPS instead
 */
LoginHelper::assertHttps();

//set the image URL
$accountId = 1; //this can be changed to later handle resellers
$imageUrl = 'img/skinlogos/' . $accountId . '/' . $accountId . '.png';

//open the DB object
require_once('php/config/dbConn.php');
require_once('php/helperPassword.php');
$conn = getPDOMasterCore();

$token_valid = false;

//check if the user has submitted a new password
if (isset($_POST['change_password']) && $_POST['change_password'] == 1) {
   //validate and save the new password
   $token = $_POST['token'];
   $new_password = $_POST['new_password'];
   $new_password_confirm = $_POST['new_password_confirm'];
   $user_id = getCheckValidTokenAndGetUserId($token);
   if ($user_id > 0) {
      $token_valid = true;
      if ($new_password == "" || $new_password_confirm == "") {
         $password_error = true;
         $password_error_string = "You must fill in both fields.";
      } else if (strcmp($new_password, $new_password_confirm) !== 0) {
         $password_error = true;
         $password_error_string = "The passwords you entered do not match, please try again.";
      } else {
         //hash and save the new password for the user
         $hashed_password = helperPassword::createHashedPassword($new_password);

         //prepare the data
         $data_update_password = array(':user_id' => $user_id, ':hashed_password' => $hashed_password);

         //prepare the query
         $stmt_update_password = $conn->prepare("UPDATE core_user
                SET user_password_hashed = :hashed_password
                WHERE user_id = :user_id ");

         //execute it
         $stmt_update_password->execute($data_update_password);

         //prepare the data
         $data_remove_token = array(':user_id' => $user_id, ':token' => null, ':expiry' => null);

         //prepare the query
         $stmt_remove_token = $conn->prepare("UPDATE core_user
                SET password_reset_token = :token, password_reset_expiry = :expiry
                WHERE user_id = :user_id ");

         //execute it
         $stmt_remove_token->execute($data_remove_token);

         $password_success = true;
         $password_success_string = "Your password was successfully reset.";
      }
   }
} else {
   $token = $_GET['token'];

   if (isset($token) && $token != "" && getCheckValidTokenAndGetUserId($token) > 0) {
      $token_valid = true;
   }
}

/**
 * Returns 0 if the token is invalid, or any ID greater than 0 if the token is valid
 * @param $token - The token to check
 * @return int - The ID of the user, or 0 if the token is invalid
 */
function getCheckValidTokenAndGetUserId($token) {
   $user_id = 0; //assume token is invalid unless proven otherwise
   $conn = getPDOMasterCore();

   //prepare the data
   $data = array(':password_reset_token' => $token);

   //prepare the query
   $stmt = $conn->prepare('SELECT * FROM account_details WHERE password_reset_token = :password_reset_token');

   //set the fetch mode
   $stmt->setFetchMode(PDO::FETCH_ASSOC);

   //execute it
   $stmt->execute($data);

   //fetch the data
   $user_account_details = $stmt->fetch();

   //check if there are any results
   if (isset($user_account_details)) {
      //check if the token hasn't already expired
      if (date("Y-m-d h:i:s") < $user_account_details['password_reset_expiry']) {
         //it hasn't, so the user may reset their password
         $user_id = $user_account_details['core_user_id'];
      }
   }

   return $user_id;
}
?>

<!DOCTYPE html>
<html class="bg-black">
   <head>
      <meta charset="UTF-8">
      <title>Reset Password</title>
      <meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
      <!-- bootstrap 3.0.2 -->
      <link href="css/bootstrap.min.css" rel="stylesheet" type="text/css" />
      <!-- font Awesome -->
      <link href="css/font-awesome.min.css" rel="stylesheet" type="text/css" />
      <!-- Theme style -->
      <link href="css/AdminLTE.css" rel="stylesheet" type="text/css" />

      <link rel="icon" type="image/png" href="../img/windowIcons/Small.png">
      <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
      <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
      <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
      <![endif]-->
   </head>
   <body class="bg-black">
      <div class="form-box" id="login-box">
         <div class="header">
            <img src="<?php echo $imageUrl ?>" />
         </div>
         <div class="body bg-gray">
<?php
if (!$token_valid) {
   echo "<h4>Invalid Token</h4>";
   echo "<h5>Apologies, this link has expired. Please <a href=\"index.php\">click here</a> to login.</h5>";
} else if (isset($password_success) && $password_success == true) {
   echo '<div class="callout callout-success" style="margin-bottom:0px;">';
   echo 'Success';
   echo '<div style="float:right; margin-top:-3px">';
   echo '<i class="fa fa-check" style="color:#5cb157; font-size:20pt; margin-right:10px;"></i>';
   echo '</div>';
   echo '</div><br/>';
   echo '<p>' . $password_success_string . ' <br/>Please <a href="index.php">click here</a> to login.</p>';
} else {
   if (isset($password_error) && $password_error == true) {
      echo '<div class="callout callout-danger" style="margin-bottom:0px;">';
      echo '<b>Password change error: ' . $password_error_string . '</b>';
      echo '<div style="float:right; margin-top:-3px">';
      echo '<i class="fa fa-exclamation-triangle" style="color:#dFb5b4; font-size:20pt; margin-right:10px;"></i>';
      echo '</div>';
      echo '</div><br/>';
   }
   ?>
               <p>Almost there, please type in your new password and confirm it in the fields below.</p>
               <form role="form" action="reset_password.php" method="post" id="changePassword" style="border:0px;">
                  <div class="form-group">
                     <label for="new_password">New Password:</label>
                     <input type="password" class="form-control" maxlength="8" style="border:1px solid #aaaaaa;" name="new_password" placeholder="Your new password..." value="" />
                  </div>
                  <div class="form-group">
                     <label for="new_password_confirm">Confirm New Password:</label>
                     <input type="password" class="form-control" maxlength="8" style="border:1px solid #aaaaaa;" name="new_password_confirm" placeholder="Confirm new password..." value="" />
                  </div>
                  <input type="hidden" name="token" id="token" value="<?php echo $token; ?>">
                  <input type="hidden" name="change_password" id="change_password" value="1">
                  <button type="submit" class="btn bg-myColour btn-block" style="color:white" tabindex="3">Change Password</button>
               </form>
            <?php } ?>
         </div>
      </div>
      <!-- jQuery 2.0.2 -->
      <script src="js/jquery-2.1.4.min.js"></script>

      <!-- Bootstrap -->
      <!--script src="../js/bootstrap.min.js" type="text/javascript"></script-->

      <script type="text/javascript">

         //make sure JQUERY is ready
         $(document).ready(function (e) {

         });

      </script>
   </body>
</html>