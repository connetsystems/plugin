<?php

/**
 * This class is a temporary hack to prepare for templating
 */
class TemplateHelper {

   static $buffer = false;
   static $values = array();

   static function initialize() {

      if (self::$buffer === false) {

         ob_start();
         require_once(__DIR__ . '/../pages/template_header.php');
         self::$buffer = ob_get_clean();
      }
   }

   static function disgard() {

      ob_end_clean();
      self::$buffer = false;
   }

   static function setPageTitle($title) {

      self::initialize();
      self::$values['[template-page-title]'] = htmlentities($title);
   }

   static function renderPage() {

      if (self::$buffer !== false) {

         if (!isset(self::$values['[template-page-title]'])) {

            global $headerPageTitle;
            if (isset($headerPageTitle)) {
               self::$values['[template-page-title]'] = htmlentities($headerPageTitle);
            } else {
               self::$values['[template-page-title]'] = 'Plugin';
            }
         }

//         if (isset($_SERVER['SERVER_PORT']) && $_SERVER['SERVER_PORT'] != 80 && $_SERVER['SERVER_PORT'] != 443) {
//
//            self::$values['[template-base-url]'] = (isset($_SERVER["HTTPS"]) ? 'https://' : 'http://') . $_SERVER['SERVER_NAME'] . ':' . $_SERVER['SERVER_PORT'];
//         } else {
//
//            self::$values['[template-base-url]'] = (isset($_SERVER["HTTPS"]) ? 'https://' : 'http://') . $_SERVER['SERVER_NAME'];
//         }
         self::$values['[template-base-url]'] = (isset($_SERVER["HTTPS"]) ? 'https://' : 'http://') . $_SERVER['HTTP_HOST'];
         
         self::$values['[json:template-base-url]'] = json_encode(self::$values['[template-base-url]']);

         self::$buffer = str_replace('[template-page-content]', ob_get_clean(), self::$buffer); //Done First
         foreach (self::$values as $field => $value) {

            self::$buffer = str_replace($field, $value, self::$buffer);
         }

         die(self::$buffer);
      }
   }

}

register_shutdown_function('TemplateHelper::renderPage');

//********************** end of file ******************************
