<?php

define('NETWORK_LOOKUP_MNP_REDIS_HOST', '10.3.0.50;8'); /* redis2 */
define('NETWORK_LOOKUP_PREFIX_PDO_CONNECTION', 'mysql:host=afridb4;dbname=core;charset=utf8');
define('NETWORK_LOOKUP_PREFIX_PDO_USERNAME', 'root');
define('NETWORK_LOOKUP_PREFIX_PASSWORD', 'mysql');

class NetworkLookupHelper {

   static $prefixes = array();

   /**
    * Fetch Prefixes from Database
    * 
    * @throws PDOException
    */
   static function fetchPrefixes() {
      // <editor-fold defaultstate="collapsed">
      $pdo = new PDO(NETWORK_LOOKUP_PREFIX_PDO_CONNECTION, NETWORK_LOOKUP_PREFIX_PDO_USERNAME, NETWORK_LOOKUP_PREFIX_PASSWORD, array(
          PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION
      ));

      $prefixes = array();
      foreach ($pdo->query('SELECT * FROM `core_prefix` WHERE `prefix_status`="ENABLED"')
              ->fetchAll(PDO::FETCH_ASSOC) as $row) {

         $mccmnc = trim($row['prefix_mccmnc']);
         $prefix = preg_replace('/[^0-9]/', '', $row['prefix']);

         $prefixes[$prefix] = $mccmnc;
      }

      $lengths = array_map('strlen', array_keys($prefixes));

      self::$prefixes = array(min($lengths), max($lengths), $prefixes);
      // </editor-fold>
   }

   /**
    * Perform Network Lookup
    * 
    * @param array $msisdn_array
    * @return array
    */
   static function networkLookup(array $msisdn_array) {

      if (empty($msisdn_array)) {

         return array();
      }

      //--------------------------------------------------------------
      // Load Prefixes
      //--------------------------------------------------------------     
      if (empty(self::$prefixes)) {

         self::fetchPrefixes();
      }

      list($min, $max, $prefixes) = self::$prefixes;

      //--------------------------------------------------------------
      // MNP / Prefix Lookup
      //--------------------------------------------------------------        
      $redis = RedisConnection::getInstance(NETWORK_LOOKUP_MNP_REDIS_HOST);
      $redis->pipeline();

      $indexes = array();
      foreach ($msisdn_array as $index => $msisdn) {

         $msisdn_array[$index] = array(
             'msisdn' => $msisdn,
             'mccmnc' => null,
             'ported' => 0,
         );

         //--------------------------------------------------------------
         // Prefix
         //--------------------------------------------------------------              
         $length = min(strlen($msisdn), $max);
         while ($length >= $min) {

            $prefix = substr($msisdn, 0, $length--);
            if (isset($prefixes[$prefix])) {

               $msisdn_array[$index]['mccmnc'] = $prefixes[$prefix];
               break;
            }
         }

         //--------------------------------------------------------------
         // MNP
         //--------------------------------------------------------------                  
         if (strpos($msisdn, '27') === 0) {

            $indexes[] = $index;
            $redis->hGet('mnp:' . $msisdn, 'mccmnc');
         }
      }

      //--------------------------------------------------------------
      // MNP Results
      //--------------------------------------------------------------            
      foreach ($redis->exec() as $multi_index => $mccmnc) {

         if (false !== $mccmnc) {

            $index = $indexes[$multi_index];
            $msisdn_array[$index]['mccmnc'] = $mccmnc;
            $msisdn_array[$index]['ported'] = 1;
         }
      }

      return $msisdn_array;
   }

}

//********************** end of file ******************************
