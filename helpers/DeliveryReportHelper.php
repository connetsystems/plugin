<?php

//-----------------------------------------------
// Delivery Report Mask
//-----------------------------------------------
define('DLR_DELIVERED', 1); //DELIVERED: The SMS has been delivered to the subscriber.
define('DLR_FAILED', 2); //FAILED: Delivery of the SMS has failed.
define('DLR_QUEUED', 4); //QUEUED: The SMS has been queued for delivery.
define('DLR_SMSC_SUBMIT', 8); //SENT: The SMS has been sent, but not yet delivered
define('DLR_SMSC_REJECTED', 16); //REJECTED: The SMS has been rejected by the SMSC
define('DLR_CUSTOM_ACCEPTED', 32); //ACCEPTED: The SMS has been accepted by the gateway.
define('DLR_CUSTOM_REJECTED', 64); //REJECTED2: The SMS has been rejected by the gateway.
define('DLR_GATEWAY_ROUTED', 128); //GATEWAY_ROUTED: Routable
define('DLR_GATEWAY_NOT_ROUTABLE', 256); //GATEWAY_NOT_ROUTABLE: Failed to route message
define('DLR_GATEWAY_REMOTE_ACCEPTED', 512); //GATEWAY_REMOTE_ACCEPTED: Accepted for sending by remote gateway / smsc
define('DLR_GATEWAY_REMOTE_REJECTED', 1024); //GATEWAY_REMOTE_REJECTED: REJECTED by remote gateway / smsc
define('DLR_GATEWAY_REMOTE_ERROR', 2048); //DLR_GATEWAY_REMOTE_ERROR: Remote Gateway Error
define('DLR_CUSTOM_DNC_REJECTED', 4096); //DLR_CUSTOM_DNC_REJECTED: Exclusion list public
define('DLR_CUSTOM_DNC_REJECTED_PRIVATE', 8192); //DLR_CUSTOM_DNC_REJECTED_PRIVATE: Exclusion list private

class DeliveryReportHelper {

   const DLR_STRING_CONSTANT = 0;
   const DLR_STRING_PRETTY = 1;

   static function dlrMaskToString($dlr, $format = self::DLR_STRING_PRETTY) {

      static $strings = array(
          DLR_DELIVERED => array('DELIVERED', 'Delivered'),
          DLR_FAILED => array('FAILED', 'Failed'),
          DLR_CUSTOM_DNC_REJECTED => array('FAILED-E', 'Failed'),
          DLR_CUSTOM_DNC_REJECTED_PRIVATE => array('FAILED-E2', 'Failed'),
          DLR_GATEWAY_NOT_ROUTABLE => array('NOT_ROUTABLE', 'Rejected'),
          DLR_GATEWAY_REMOTE_REJECTED => array('REMOTE_REJECTED', 'Rejected'),
          DLR_SMSC_REJECTED => array('SMSC_REJECTED', 'Rejected'),
          DLR_CUSTOM_DNC_REJECTED => array('FAILED', 'Failed'),
          DLR_CUSTOM_REJECTED => array('REJECTED', 'Rejected'),
          DLR_GATEWAY_REMOTE_ERROR => array('REMOTE_ERROR', 'Failed'),
          DLR_SMSC_SUBMIT => array('SUBMIT', 'Pending'),
          DLR_QUEUED => array('QUEUED', 'Pending'),
          DLR_GATEWAY_ROUTED => array('ROUTED', 'Pending'),
          DLR_GATEWAY_REMOTE_ACCEPTED => array('REMOTE_ACCEPTED', 'Pending'),
          DLR_CUSTOM_ACCEPTED => array('ACCEPTED', 'Pending'),
          0 => array('UNKNOWN', 'Pending')
      );

      foreach ($strings as $mask => $string) {

         if (((integer) $dlr & (integer) $mask) == (integer) $mask) {
            return $string[$format];
         }
      }
   }

}

//********************** end of file ******************************
