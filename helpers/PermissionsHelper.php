<?php

class PermissionsHelper {

   static function getAllPermissions() {
      // <editor-fold defaultstate="collapsed">
      static $permissions = array();
      if (empty($permissions)) {

         $permissions = getUserServicePermissions(LoginHelper::getCurrentUserId());
      }

      return $permissions;
      // </editor-fold>
   }

   static function getAllServicesWithPermissions($filter_permission = NULL) {
      // <editor-fold defaultstate="collapsed">

      $services = self::getAllPermissions();

      if (is_null($filter_permission)) {

         $filter_permission = array();
      } elseif (!is_array($filter_permission)) {

         $filter_permission = array((string) $filter_permission);
      }

      if (LoginHelper::isResellerService() && empty($filter_permission)) {

         $account_id = LoginHelper::getCurrentAccountId();
         $reseller_id = LoginHelper::getCurrentResellerId();

         foreach (getServicesInAccount($account_id) as $row) {

            $service_id_reseller = (integer) $row['service_id_reseller'];
            $service_id = (integer) $row['service_id'];

            if ($service_id_reseller !== $service_id && $service_id_reseller === $reseller_id && !isset($services[$service_id])) {

               $services[$service_id] = array(
                   'account_id' => $account_id,
                   'account_name' => $row['account_name'],
                   'service_id' => $service_id,
                   'service_login' => isset($row['service_login']) ? $row['service_login'] : null,
                   'service_name' => $row['service_name'],
                   'service_id_reseller' => isset($row['service_id_reseller']) ? $row['service_id_reseller'] : null,
                   'user_id' => isset($row['user_id']) ? $row['user_id'] : null,
                   'user_username' => isset($row['user_username']) ? $row['user_username'] : null,
                   'permissions' => array(),
               );
            }
         }
      }

      foreach ($filter_permission as $permission) {

         $perm = trim(strtolower($permission));
         foreach ($services as $index => $service) {

            if (!isset($service['permissions'][$perm]) && !isset($service['permissions']['super'])) {

               unset($services[$index]);
            }
         }
      }

      usort($services, function($item1, $item2) {

         $str1 = trim($item1['account_name']) . '-' . trim($item1['service_name']);
         $str2 = trim($item2['account_name']) . '-' . trim($item2['service_name']);
         return strcasecmp($str1, $str2);
      });


      $by_service_id = array();
      foreach ($services as $service) {


         $service_id = (integer) $service['service_id'];
         $by_service_id[$service_id] = $service;
      }

      return $by_service_id;
      // </editor-fold>
   }

   static function hasServicePermission($service_id, $permission = 'any') {
      // <editor-fold defaultstate="collapsed">

      $permissions = self::getAllPermissions();

      $perm = trim(strtolower($permission));
      if ($perm == 'any') {

         return (isset($permissions[$service_id]['permissions']) && !empty($permissions[$service_id]['permissions']));
      }

      if (isset($permissions[$service_id]['permissions'][$perm])) {
         return true;
      }

      if (isset($permissions[$service_id]['permissions']['super'])) {
         return true;
      }
      return false;
      // </editor-fold>
   }

}

//********************** end of file ******************************
