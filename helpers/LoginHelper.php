<?php

class LoginHelper {

   /**
    * TODO: Port?
    */
   static function assertHttps() {

      switch ($_SERVER['REMOTE_ADDR']) {

         case '127.0.0.1':
         case '::1':
            break;

         default:

            switch ($_SERVER["HTTP_HOST"]) {

               case 'plugin.connet-systems.com':
               case 'plugin-beta.connet-systems.com':

                  if (!isset($_SERVER["HTTPS"]) && strpos($_SERVER['SERVER_ADDR'], '10.') !== 0) {

                     header("Location: https://" . $_SERVER["HTTP_HOST"] . $_SERVER["REQUEST_URI"]);
                     die('Redirecting');
                  }

                  break;

               default:

                  $redirect = true;
                  foreach (array('vodacombulk.co.za') as $filter) {
                                    
                     if (strpos($_SERVER["HTTP_HOST"], $filter) !== false) {
                        $redirect = false;
                        break;
                     }
                  }

                  if ($redirect && isset($_SERVER["HTTPS"])) {

                     header("Location: http://" . $_SERVER["HTTP_HOST"] . $_SERVER["REQUEST_URI"]);
                     die('Redirecting');
                  }
            }
      }
   }

   static function startSession() {

      if (!session_id()) {

         session_start();
      }
   }

   static function isResellerService() {

      self::startSession();
      if (isset($_SESSION['serviceId'], $_SESSION['resellerId'])) {

         $reseller_id = (integer) $_SESSION['resellerId'];
         $service_id = (integer) $_SESSION['serviceId'];

         return ($reseller_id > 0 && $reseller_id == $service_id ? true : false);
      }
      return false;
   }

   static function isSystemAdmin() {

      self::startSession();
      if (isset($_SESSION['userId'], $_SESSION['userSystemAdmin'])) {

         return ($_SESSION['userSystemAdmin'] ? true : false);
      }
      return false;
   }

   static function getCurrentServiceId() {

      self::startSession();
      return (isset($_SESSION['serviceId']) ? (integer) $_SESSION['serviceId'] : false);
   }

   static function getCurrentResellerId() {

      self::startSession();
      return (isset($_SESSION['resellerId']) ? (integer) $_SESSION['resellerId'] : false);
   }

   static function getCurrentAccountId() {

      self::startSession();
      return (isset($_SESSION['accountId']) ? (integer) $_SESSION['accountId'] : false);
   }

   static function getCurrentUserId() {

      self::startSession();
      return (isset($_SESSION['userId']) ? (integer) $_SESSION['userId'] : false);
   }

   static function assertLogin() {

      self::startSession();
      if (!isset($_SESSION['userId'], $_SESSION['serviceId'], $_SESSION['accountId'])) {

         header('Location: /index.php');
         die('Redirecting');
      }
   }

}

//********************** end of file ******************************
