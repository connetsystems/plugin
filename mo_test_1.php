<?php

define('_SCRIPT_START', microtime(true));

ob_start();

define('ERROR_REPORTING', true);
require_once('ConnetAutoloader.php');
preset::plain();

$statistics = array();

$batch_id = isset($_REQUEST['batch_id']) ? (integer) $_REQUEST['batch_id'] : 1071812;

$query = array(
    'fields' => array('meta.status'),
    'query' => array(
        'bool' => array(
            'must' => array(
                array('match' => array('meta.batch_id' => $batch_id)),
            )
        )
    ),
);
$http = curl::post('http://10.3.0.109:9200/core/core_mtsms/_search?size=1000&search_type=scan&scroll=1m', json_encode($query), 'application/json');
$response = json_decode($http->content, true);

$scroll_id = $response['_scroll_id'];
$mtsms_id_list = array();
$start = microtime(true);
while (microtime(true) - $start < 10) {

   $http = curl::get('http://10.3.0.109:9200/_search/scroll?scroll=1m&scroll_id=' . $scroll_id);
   $response = json_decode($http->content, true);

   if (empty($response['hits']['hits'])) {
      break;
   }

   foreach ($response['hits']['hits'] as $document) {

      $mtsms_id = $document['_id'];
      $status = strtolower($document['fields']['meta.status'][0]);


      $statistics[$status] = (isset($statistics[$status]) ? $statistics[$status] + 1 : 1);
      $mtsms_id_list[$mtsms_id] = $mtsms_id;
   }
}

clog('MT:', count($mtsms_id_list), 'Elapsed:', microtime(true) - $start);

$mosms_list = array();

foreach (array_chunk($mtsms_id_list, 10000) as $list) {

   $query = array(
       'fields' => array(),
       'query' => array(
           'filtered' => array(
               'filter' => array(
                   'terms' => array(
                       'meta.mtsms_id' => array_values($list)
                   )
               )
           )
       ),
   );

   $http = curl::post('http://10.3.0.109:9200/core/core_mosms/_search?size=0', json_encode($query), 'application/json');
   $response = json_decode($http->content, true);


   $statistics['mosms'] = (isset($statistics['mosms']) ? $statistics['mosms'] + $response['hits']['total'] : $response['hits']['total']);

   //If we wanted to docs
//   foreach ($response['hits']['hits'] as $document) {
//
//      $mosms_list[$document['_id']] = $document['_source'];
//   }
}

clog($statistics);

clog('MO:', count($mosms_list));

header('Content-Type: application/json');
ob_end_clean();

die(json_encode(array(
    'status' => 200,
    'message' => 'Success',
    'data' => array('batch_id' => $batch_id, 'summary' => $statistics),
    'elapsed' => microtime(true) - $start
)));


clog::info('* Exit');


//********************** end of file ******************************
