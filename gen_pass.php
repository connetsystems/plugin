<?php
if (!ini_get('display_errors')) {
   ini_set('display_errors', 1);
}


//required to connect to the DB
require_once('php/config/dbConn.php');
require_once('php/helperPassword.php');

/*
  //for JACQUES
  $password = "jsb12345";
  $user_id = 479;
  $hashed_password = helperPassword::createHashedPassword($password);

  //open the DB object
  $conn = getPDOMasterCore();

  //prepare the data
  $data = array(':user_id' => $user_id, ':hashed_password' => $hashed_password);

  //prepare the query
  $stmt = $conn->prepare("UPDATE core_user
  SET user_password_hashed = :hashed_password
  WHERE user_id = :user_id ");;

  //execute it
  return $stmt->execute($data);

 */

echo "starting...";

//open the DB object
$conn = getPDOMasterCore();

//prepare the query
$stmt_users = $conn->prepare("SELECT * FROM core.core_user WHERE user_password_hashed IS NULL");

//set the fetch mode
$stmt_users->setFetchMode(PDO::FETCH_ASSOC);

//execute it
$stmt_users->execute();

//fetch the data
$users = $stmt_users->fetchAll();


foreach ($users as $user)
{
   if (!isset($user['user_password_hashed']) || $user['user_password_hashed'] == "")
   {
      $hashed_password = helperPassword::createHashedPassword($user['user_password']);

      //prepare the data
      $data = array(':user_id' => $user['user_id'], ':hashed_password' => $hashed_password);

      //prepare the query
      $stmt = $conn->prepare("UPDATE core_user
                SET user_password_hashed = :hashed_password
                WHERE user_id = :user_id ");

      //execute it
      $stmt->execute($data);
   }
}


$conn->close();

echo "done";

die();
