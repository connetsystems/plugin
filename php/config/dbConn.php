<?php

/**
 * MAKE SURE THE LOGGER IS AVAILABLE
 */
if (!defined('ERROR_REPORTING') && !defined('LOG_FILENAME')) {
   define('ERROR_REPORTING', true);
   define('LOG_FILENAME', '/var/log/connet/plugin/' . basename(__FILE__, '.php') . '.log');
   require_once('ConnetAutoloader.php');
}

if (defined('PLUGIN_DB_MASTER_DB_HOST')) {

   return;
}

/**
 * DEFINE OUR CONSTANTS
 */
//for the master writing DB
define("PLUGIN_DB_MASTER_DB_HOST", "waspmasterdb");
//define("PLUGIN_DB_MASTER_DB_HOST", "afridb4");
define("PLUGIN_DB_MASTER_DB_NAME", "core");
define("PLUGIN_DB_MASTER_DB_USER", "portal");
define("PLUGIN_DB_MASTER_DB_PASS", "phaeXe7Kee6OhZi");

//for reporting DB (SLAVE)
define("PLUGIN_DB_REPORT_DB_HOST", "waspreportdb1");
//define("PLUGIN_DB_REPORT_DB_HOST", "afridb4");
define("PLUGIN_DB_REPORT_DB_NAME", "core");
define("PLUGIN_DB_REPORT_DB_USER", "portal");
define("PLUGIN_DB_REPORT_DB_PASS", "phaeXe7Kee6OhZi");

define("PLUGIN_ELASTICSEARCH_HOST", "http://elasticmaster:9200");

/**
 * This method connects to the main core DB, with a mysqli object for prepared statements.
 * @return bool|mysqli
 */
function connToMasterCore() {
   $con = new mysqli(PLUGIN_DB_MASTER_DB_HOST, PLUGIN_DB_MASTER_DB_USER, PLUGIN_DB_MASTER_DB_PASS, PLUGIN_DB_MASTER_DB_NAME);
   /* check connection */
   if (mysqli_connect_errno()) {
      echo "Failed to connect to server. If your internet connection is working, the problem might be on the server side. Err: " . mysqli_connect_error();
      return false;
   }

   return $con;
}

/**
 * This method connects to the main core DB, with a mysqli object for prepared statements.
 * @return bool|mysqli
 */
function connToReportingCore() {

   $con = new mysqli(PLUGIN_DB_REPORT_DB_HOST, PLUGIN_DB_REPORT_DB_USER, PLUGIN_DB_REPORT_DB_PASS, PLUGIN_DB_REPORT_DB_NAME);
   /* check connection */
   if (mysqli_connect_errno()) {
      echo "Failed to connect to server. If your internet connection is working, the problem might be on the server side. Err: " . mysqli_connect_error();
      return false;
   }

   return $con;
}

/**
 * This method connects to the main core DB, with a PDO object for prepared statements.
 * @return PDO|bool
 */
function getPDOMasterCore() {
   try {

      if (defined('USE_PDO_EXCEPTIONS') && USE_PDO_EXCEPTIONS) {

         $con = new PDO("mysql:host=" . PLUGIN_DB_MASTER_DB_HOST . ";dbname=" . PLUGIN_DB_MASTER_DB_NAME . "", PLUGIN_DB_MASTER_DB_USER, PLUGIN_DB_MASTER_DB_PASS, array(
             PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION));
      } else {
         $con = new PDO("mysql:host=" . PLUGIN_DB_MASTER_DB_HOST . ";dbname=" . PLUGIN_DB_MASTER_DB_NAME . "", PLUGIN_DB_MASTER_DB_USER, PLUGIN_DB_MASTER_DB_PASS);
      }
   } catch (PDOException $e) {
      clog::info('* PDO - MYSQL error: ' . $e->getMessage());
   }

   return $con;
}

/**
 * This method connects to the main core DB, with a PDO object for prepared statements.
 * @return bool|mysqli
 */
function getPDOReportingCore() {
   try {
      $con = new PDO("mysql:host=" . PLUGIN_DB_REPORT_DB_HOST . ";dbname=" . PLUGIN_DB_REPORT_DB_NAME . "", PLUGIN_DB_REPORT_DB_USER, PLUGIN_DB_REPORT_DB_PASS);
   } catch (PDOException $e) {
      clog::info('* PDO - MYSQL error: ' . $e->getMessage());
   }

   return $con;
}

//Jacques Just for testing Finacials
function getWaspdb1() {
   try {
      $con = new PDO("mysql:host=10.3.0.41;dbname=" . PLUGIN_DB_REPORT_DB_NAME . "", PLUGIN_DB_REPORT_DB_USER, PLUGIN_DB_REPORT_DB_PASS);
   } catch (PDOException $e) {
      clog::info('* PDO - MYSQL error: ' . $e->getMessage());
   }

   return $con;
}

/**
 * This method takes in a raw elasticsearch query string and runs a CURL request on CORE records to return the data
 * @param string $query - THe elasticsearch query to run
 * @return array $json - The ARRAY converted from the JSON response
 */
function runRawMCoreElasticsearchQuery($query) {
   $output = "[]";
   try {
      $ch = curl_init();

      $cUrlReq = PLUGIN_ELASTICSEARCH_HOST . "/core/_search?pretty";

      curl_setopt($ch, CURLOPT_POSTFIELDS, $query);

      curl_setopt($ch, CURLOPT_URL, $cUrlReq);
      // return the transfer as a string
      curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);

      curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 10);
      curl_setopt($ch, CURLOPT_TIMEOUT, 30); //timeout in seconds
      // $output contains the output string
      $output = utf8_decode(curl_exec($ch));

      curl_close($ch);
   } catch (Exception $e) {
      clog::error('Error in elasticsearch... ' . $e->getMessage());
   }

   return json_decode($output, true);
}

/**
 * This method takes in a raw elasticsearch query string and runs a CURL request on MTSMS records to return the data
 * @param string $query - THe elasticsearch query to run
 * @return array $json - The ARRAY converted from the JSON response
 */
function runRawMTSMSElasticsearchQuery($query) {
   $output = "[]";
   try {
      $ch = curl_init();

      $cUrlReq = PLUGIN_ELASTICSEARCH_HOST . "/core/core_mtsms/_search?pretty";

      curl_setopt($ch, CURLOPT_POSTFIELDS, $query);

      curl_setopt($ch, CURLOPT_URL, $cUrlReq);

      // return the transfer as a string
      curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);

      curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 10);
      curl_setopt($ch, CURLOPT_TIMEOUT, 30); //timeout in seconds
      // $output contains the output string
      $output = curl_exec($ch);

      curl_close($ch);
   } catch (Exception $e) {
      clog::error('Error in elasticsearch... ' . $e->getMessage());
   }

   return json_decode($output, true);
}

/**
 * This method takes in a raw elasticsearch query string and runs a CURL request on MOSMS records to return the data
 * @param string $query - THe elasticsearch query to run
 * @return array $json - The ARRAY converted from the JSON response
 */
function runRawMOSMSElasticsearchQuery($query) {
   $output = "[]";
   try {
      $ch = curl_init();

      $cUrlReq = PLUGIN_ELASTICSEARCH_HOST . "/core/core_mosms/_search?pretty";

      curl_setopt($ch, CURLOPT_POSTFIELDS, $query);
      curl_setopt($ch, CURLOPT_URL, $cUrlReq);

      // return the transfer as a string
      curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);

      curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 10);
      curl_setopt($ch, CURLOPT_TIMEOUT, 30); //timeout in seconds
      // $output contains the output string
      $output = curl_exec($ch);

      curl_close($ch);
   } catch (Exception $e) {
      clog::error('Error in elasticsearch... ' . $e->getMessage());
   }

   return json_decode($output, true);
}

/* * ***********************************
 *
 * EVERYTHING BELOW IS LEGACY CODE
 *
 * *********************************** */

function connToMasterDB($dbToConnect, $host = null, $user = null, $pass = null) {
   if ($host == null) {
      //$host = '10.3.0.41'; // WASP1 Slave
      //$host = '10.3.0.39'; // Reporting Slave
      //$host = '10.3.0.48'; // TestDB 
      //$host = '10.3.0.42'; // MASTER!!
      $host = 'waspmasterdb'; // MASTER!! (Set this for appstaging1 aka portal2.connet-systems.com)
   }
   if ($user == null) {
      $user = 'portal';
   }
   if ($pass == null) {
      $pass = 'phaeXe7Kee6OhZi';
   }

   $con = mysqli_connect($host, $user, $pass, $dbToConnect);
   //	Check connection
   if (mysqli_connect_errno()) {
      echo "Failed to connect to server. If your internet connection is working, the problem might be on the server side. Err: " . mysqli_connect_error();
      return false;
   } else {
      return $con;
   }
}

function connToDB($dbToConnect, $host = null, $user = null, $pass = null) {
   //$dbToConnect = 'waspreportdb1';
   if ($host == null) {
      //$host = '10.3.0.41'; // WASP1 Slave
      $host = 'waspreportdb1'; // Reporting Slave
      //$host = '10.3.0.39'; // Reporting Slave
      //$host = '10.3.0.48'; // TestDB
      //$host = 'masterdb'; // MASTER!!
   }
   if ($user == null) {
      $user = 'portal';
      //$user = 'jacquesb';
   }
   if ($pass == null) {
      $pass = 'phaeXe7Kee6OhZi';
      //$pass = 'pei9ZeToh3iuvee';
   }

   $con = mysqli_connect($host, $user, $pass, $dbToConnect);
   //	Check connection
   if (mysqli_connect_errno()) {
      echo "Failed to connect to server. If your internet connection is working, the problem might be on the server side. Err: " . mysqli_connect_error();
      return false;
   } else {
      return $con;
   }
}

function connToDBTest($dbToConnect, $host = null, $user = null, $pass = null) {
   if ($host == null) {
      //$host = '10.3.0.41'; // WASP1 Slave
      //$host = '10.3.0.39'; // Reporting Slave
      $host = '10.3.0.48'; // TestDB 
      //$host = 'masterdb'; // MASTER!!
   }
   if ($user == null) {
      $user = 'portal';
      $user = 'jacquesb';
   }
   if ($pass == null) {
      $pass = 'phaeXe7Kee6OhZi';
      $pass = 'pei9ZeToh3iuvee';
   }

   $con = mysqli_connect($host, $user, $pass, $dbToConnect);
   //	Check connection
   if (mysqli_connect_errno()) {
      echo "Failed to connect to server. If your internet connection is working, the problem might be on the server side. Err: " . mysqli_connect_error();
      return false;
   } else {
      return $con;
   }
}

?>