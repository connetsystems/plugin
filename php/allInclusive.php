<?php

/**
 *  Load ConnetAutoloader so we can use CLOG for our logging tasks
 */
if (!class_exists('ConnetAutoloader', false)) {

   define('ERROR_REPORTING', true);
   define('LOG_FILENAME', '/var/log/connet/plugin/' . basename(__FILE__, '.php') . '.log');
   require_once('ConnetAutoloader.php');
}
/**
 *  Remember to loaf dbConn.php for a central DB access point
 */
require_once('config/dbConn.php');

//import for the elastics helper
//require_once('../PluginAPI/PluginAPIAutoload.php');
//Helper Import
require_once('helperSMSGeneral.php'); //holds general tasks
require_once('helperRouting.php');
require_once('helperFinance.php');
require_once('helperPassword.php');
require_once('helperPermissions.php');
require_once('helperContactLists.php');
require_once('helperBulkSend.php');
require_once('helperCurlMulti.php');

$cl = 0;

/**
 * This function simply updates a record in the database, particularly a single column of your choosing.
 * @param $varToUpdate - The value to set in the update
 * @param $whatToUpdate - The target column in the table
 * @param $whereToUpdate - The table to update
 * @param $whatIs - The column used to identify the row
 * @param $what - The variable that must match in the particular colum to update
 */
function updateServer($varToUpdate, $whatToUpdate, $whereToUpdate, $whatIs, $what) {
   try {
      //open the DB object
      $conn = getPDOMasterCore();

      //prepare the data
      $data = array(':what' => $what);

      //prepare the query
      $stmt = $conn->prepare('UPDATE ' . $whereToUpdate . ' SET `' . $whatToUpdate . '`="' . $varToUpdate . '" WHERE ' . $whatIs . '= :what');

      //execute it
      $stmt->execute($data);
   } catch (Exception $e) {
      //log our exception pls
      clog::info('* All Inclusive - MYSQL PDO error: ' . $e->getMessage());
   }
}

function setInitId($id) {
   global $cl;
   $cl = $id;
}

function getInitId() {
   global $cl;
   $id = $cl;
   return $id;
}

//bulksmscont.php | confirmquicksms.php
function convertSmartQuotes($string) {
   $search = array(chr(145),
       chr(146),
       chr(147),
       chr(148),
       chr(151));

   $replace = array("'",
       "'",
       '"',
       '"',
       '-');

   return str_replace($search, $replace, $string);
}

/**
 * Assumed to check if the user is enabled so they can send
 * @param $service_id - The ID of the service
 * @return int $rC - 0 for can't send, 1 or more for send
 */
//DOUG: converted to PDO - bulksms.php | quicksms.php
function checkUserCanSend($service_id, $user_id) {
//default
   $rC = 0;

//open the DB object
   $conn = getPDOMasterCore();

//prepare the data
   $data = array(':service_id' => $service_id, ':user_id' => $user_id);

//prepare the query
   $stmt = $conn->prepare("SELECT count(p.user_id) as rc
                FROM core.core_permission p
                inner join core.core_user u
                on p.user_id = u.user_id
                inner join core.core_service s
                on p.service_id = s.service_id
                WHERE p.service_id = :service_id
                AND p.user_id = :user_id
                AND p.permission_status = 'ENABLED' and u.user_status='ENABLED' and s.service_status = 'ENABLED'");

//set the fetch mode
   $stmt->setFetchMode(PDO::FETCH_ASSOC);

//execute it
   $stmt->execute($data);

//fetch the data
   $row = $stmt->fetch();
   if (isset($row['rc']) && $row['rc'] > 0) {
      $rC = $row['rc'];
   }

   return $rC;
}

//DOUG: converted to PDO - campaign.php
function updateCampaignClient($client_id, $name, $desc) {
//open the DB object
   $conn = getPDOMasterCore();

//prepare the data
   $data = array(':name' => $name, ':client_id' => $client_id, ':desc' => $desc);

//prepare the query
   $stmt = $conn->prepare('UPDATE core.campaign_client SET `client_name` = :name, `client_description` = :desc WHERE client_id = :client_id ');

//execute it
   $stmt->execute($data);

   return true;
}

//DOUG: converted to PDO - campaign.php
function updateCampaignCampaign($campaign_id, $name, $desc, $code, $campaign_status) {
//open the DB object
   $conn = getPDOMasterCore();

//prepare the data
   $data = array(':campaign_id' => $campaign_id, ':name' => $name, ':desc' => $desc, ':code' => $code, ':campaign_status' => $campaign_status);

//prepare the query
   $stmt = $conn->prepare('UPDATE core.campaign_campaign SET `campaign_name` = :name, `campaign_description` = :desc, `campaign_code` = :code, `campaign_status` = :campaign_status WHERE campaign_id = :campaign_id ');

//execute it
   $stmt->execute($data);

   return true;
}

//DOUG: converted to PDO - campaign.php
function updateCampaignTemplate($template_id, $text, $desc, $status) {
//open the DB object
   $conn = getPDOMasterCore();

//prepare the data
   $data = array(':template_id' => $template_id, ':template_text' => $text, ':template_desc' => $desc, ':template_status' => $status);

//prepare the query
   $stmt = $conn->prepare('UPDATE core.campaign_template SET `template_description` = :template_desc, `template_content` = :template_text, `template_status` = :template_status WHERE template_id = :template_id ');

//execute it
   $stmt->execute($data);

   return true;
}

//DOUG: converted to PDO - campaign.php
function checkNewCampaignClient($service_id, $nName, $nDesc) {
//open the DB object
   $conn = getPDOMasterCore();

//prepare the data
   $data = array(':service_id' => $service_id);

//prepare the query
   $stmt = $conn->prepare('SELECT client_name, client_description
                FROM core.campaign_client
                WHERE service_id = :service_id ');

//set the fetch mode
   $stmt->setFetchMode(PDO::FETCH_ASSOC);

//execute it
   $stmt->execute($data);

//fetch the data
   $row = $stmt->fetch();

   $checker = 0;
   if (isset($row['client_name']) && isset($row['client_description'])) {
      if ($nName == $row['client_name'] && $nDesc == $row['client_description']) {
         $checker = -1;
      } else {
         $checker = 0;
      }
   }
   return $checker;
}

//DOUG: converted to PDO - sucessbulkfile.php
function getNewBatchId($remote_filename) {
   //open the DB object
   $conn = getPDOMasterCore();

   //prepare the data
   $data = array(':remote_filename' => $remote_filename);

   //prepare the query
   $stmt = $conn->prepare('SELECT batch_id
                FROM core.batch_batch
                WHERE batch_csv_process_filename = :remote_filename ');

   //set the fetch mode
   $stmt->setFetchMode(PDO::FETCH_ASSOC);

   //execute it
   $stmt->execute($data);

   //fetch the data
   $row = $stmt->fetch();

   $id = $row['batch_id'];

   return $id;
}

//DOUG: converted to PDO - pause.php
function getBatchData($batch_id) {
   //open the DB object
   $conn = getPDOMasterCore();

   //prepare the data
   $data = array(':batch_id' => $batch_id);

   //prepare the query
   $stmt = $conn->prepare("SELECT batch_id, batch_created as 'batch_created', client_name, ifnull(campaign_name, 'API Interface') as 'campaign_name', campaign_code, batch_reference,
          CASE batch_status WHEN 'NEW' THEN 'Draft' WHEN 'CSV'THEN 'Processing' WHEN 'WAIT'THEN batch_submit_status WHEN 'ERROR' THEN 'Error' WHEN 'BLACKLIST' THEN 'Blacklist' WHEN 'DONE' THEN 'Completed'
         WHEN 'BUCKET' THEN 'None' WHEN 'SEND' THEN 'Sending' ELSE batch_submit_status END as 'batch_submit_status', batch_mtsms_total
         FROM core.batch_batch b
         LEFT JOIN core.campaign_batch USING (batch_id)
         LEFT JOIN core.campaign_campaign USING (campaign_id)
         LEFT JOIN core.campaign_client USING (client_id)
         WHERE b.batch_id = :batch_id
         AND batch_reference IS NOT NULL");


   //set the fetch mode
   $stmt->setFetchMode(PDO::FETCH_ASSOC);

   //execute it
   $stmt->execute($data);

   //fetch the data
   $batch_data = $stmt->fetch();

   return $batch_data;
}

//DOUG: converted to PDO - pause.php
function getBatchStatusData($batch_id) {
//open the DB object
   $conn = getPDOMasterCore();

//prepare the data
   $data = array(':batch_id' => $batch_id);

//prepare the query
   $stmt = $conn->prepare('SELECT batch_csv_content, batch_identifier, user_id, batch_schedule, batch_submit_status, batch_status, batch_meta, batch_created, batch_status_message
                FROM core.batch_batch
                WHERE batch_id = :batch_id ');

//set the fetch mode
   $stmt->setFetchMode(PDO::FETCH_ASSOC);

//execute it
   $stmt->execute($data);

//fetch the data
   $bData = $stmt->fetchAll();

   return $bData;
}

//DOUG: converted to PDO - sucessbulkfile.php
function insertIntoBatchBatch($user_id, $service_id, $smsText, $camCode, $fileLocation, $status, $sTime, $date, $tfn) {
//open the DB object
   $conn = getPDOMasterCore();

   if ($status == 'Paused') {
      $status_reformat = 'PAUSE';
//prepare the data
      $data = array(':service_id' => $service_id, ':user_id' => $user_id, ':cam_code' => $camCode,
          ':file_location' => $fileLocation, ':sms_text' => $smsText, ':status_reformat' => $status_reformat,
          ':datetime' => $date, ':unique_id' => uniqid(), ':tfn' => $tfn);

//prepare the query
      $stmt = $conn->prepare("INSERT INTO core.batch_batch (`service_id`, `user_id`, `batch_identifier`, `batch_description`, `batch_csv_process_filename`, `batch_csv_content`, `batch_status`, `batch_status_message`, `batch_submit_status`, `batch_schedule`, `batch_src`, `batch_validity_flag`, `batch_meta`)
                    VALUES (:service_id, :user_id, :cam_code, 'NEW CM Bulk upload', :file_location, :sms_text, 'CSV', 'Portal Submit', :status_reformat, :datetime, :unique_id, '0', :tfn) ");
   } elseif ($status == 'Scheduled') {
      $status_reformat = 'SCHEDULE';
      $sTime = $sTime . ':00';
      $sTime = date($sTime);

//prepare the data
      $data = array(':service_id' => $service_id, ':user_id' => $user_id, ':cam_code' => $camCode,
          ':file_location' => $fileLocation, ':sms_text' => $smsText, ':status_reformat' => $status_reformat,
          ':datetime' => $sTime, ':unique_id' => uniqid(), ':tfn' => $tfn);

//prepare the query
      $stmt = $conn->prepare("INSERT INTO core.batch_batch (`service_id`, `user_id`, `batch_identifier`, `batch_description`, `batch_csv_process_filename`, `batch_csv_content`, `batch_status`, `batch_status_message`, `batch_submit_status`, `batch_schedule`, `batch_src`, `batch_validity_flag`, `batch_meta`)
                    VALUES (:service_id, :user_id, :cam_code, 'NEW CM Bulk upload', :file_location, :sms_text, 'CSV', 'Portal Submit', :status_reformat, :datetime, :unique_id, '0', :tfn) ");
   } elseif ($status == 'Send Immediately') {
      $status_reformat = 'SEND';

//prepare the data
      $data = array(':service_id' => $service_id, ':user_id' => $user_id, ':cam_code' => $camCode,
          ':file_location' => $fileLocation, ':sms_text' => $smsText, ':status_reformat' => $status_reformat,
          ':datetime' => $sTime, ':unique_id' => uniqid(), ':tfn' => $tfn);

//prepare the query
      $stmt = $conn->prepare("INSERT INTO core.batch_batch (`service_id`, `user_id`, `batch_identifier`, `batch_description`, `batch_csv_process_filename`, `batch_csv_content`, `batch_status`, `batch_status_message`, `batch_submit_status`, `batch_schedule`, `batch_src`, `batch_validity_flag`, `batch_meta`)
                    VALUES (:service_id, :user_id, :cam_code, 'NEW CM Bulk upload', :file_location, :sms_text, 'CSV', 'Portal Submit', :status_reformat, :datetime, :unique_id, '0', :tfn) ");
   }

//execute it
   if ($stmt->execute($data)) {
      return 1;
   } else {
      return 0;
   }
}

//DOUG: converted to PDO - sucessbulkfile.php
function insertIntoCampaignBatch($batch_id, $refText, $campaign_id) {
//open the DB object
   $conn = getPDOMasterCore();

//prepare the data
   $data = array(':batch_id' => $batch_id, ':batch_reference' => $refText, ':campaign_id' => $campaign_id);

//prepare the query
   $stmt = $conn->prepare("INSERT INTO core.campaign_batch (`batch_id`, `batch_reference`, `campaign_id`)
                VALUES (:batch_id, :batch_reference, :campaign_id) ");

   $insertCBatch = $stmt->execute($data);

   return $insertCBatch;
}

//DOUG: converted to PDO - campaign.php
function addNewCampaignClient($service_id, $user_id, $client_name, $client_description, $client_status) {
//open the DB object
   $conn = getPDOMasterCore();

//prepare the data
   $data = array(':service_id' => $service_id, ':user_id' => $user_id, ':client_name' => $client_name, ':client_description' => $client_description, ':client_status' => $client_status);

//prepare the query
   $stmt = $conn->prepare("INSERT INTO core.campaign_client (`service_id`, `user_id`, `client_name`, `client_description`, `client_status`)
                 VALUES (:service_id, :user_id, :client_name, :client_description, :client_status) ");

   $insertClient = $stmt->execute($data);

   return $insertClient;
}

//DOUG: converted to PDO - campaign.php
function checkNewCampaignCamp($client_id, $nCampName, $nCampDesc, $nCampCode, $nStatus) {
//open the DB object
   $conn = getPDOMasterCore();

//prepare the data
   $data = array(':client_id' => $client_id);

//prepare the query
   $stmt = $conn->prepare('SELECT campaign_name, campaign_code, campaign_description, campaign_status
                FROM core.campaign_campaign
                WHERE client_id = :client_id ');

//set the fetch mode
   $stmt->setFetchMode(PDO::FETCH_ASSOC);

   $stmt->execute($data);

   $results = $stmt->fetchAll();

   $checker = 0;
   foreach ($results as $row) {
      if ($nCampName == $row['campaign_name'] && $nCampDesc == $row['campaign_description'] && $nCampCode == $row['campaign_code'] && $nStatus == $row['campaign_status']) {
         $checker++;
      }
   }

   if ($checker == 0) {
      return 0;
   } else {
      return -1;
   }
}

//DOUG: converted to PDO - campaign.php
function addNewCampaignCamp($client_id, $user_id, $campaign_name, $campaign_code, $campaign_description, $campaign_status) {
//open the DB object
   $conn = getPDOMasterCore();

//prepare the data
   $data = array(':client_id' => $client_id, ':user_id' => $user_id, ':campaign_name' => $campaign_name, ':campaign_code' => $campaign_code,
       ':campaign_description' => $campaign_description, ':campaign_status' => $campaign_status);

//prepare the query
   $stmt = $conn->prepare("INSERT INTO core.campaign_campaign (`client_id`, `user_id`, `campaign_name`, `campaign_code`, `campaign_description`, `campaign_status`)
                 VALUES (:client_id, :user_id, :campaign_name, :campaign_code, :campaign_description, :campaign_status) ");

   $insertCamp = $stmt->execute($data);

   return $insertCamp;
}

//DOUG: converted to PDO - campaign.php
function addNewTemplate($campaign_id, $user_id, $template_description, $template_content, $template_status) {
   //open the DB object
   $conn = getPDOMasterCore();

   //prepare the data
   $data = array(':campaign_id' => $campaign_id, ':user_id' => $user_id, ':template_description' => $template_description, ':template_content' => $template_content,
       ':template_status' => $template_status);

   //prepare the query
   $stmt = $conn->prepare("INSERT INTO core.campaign_template (`campaign_id`, `user_id`, `template_content`, `template_description`, `template_status`)
                 VALUES (:campaign_id, :user_id, :template_content, :template_description, :template_status) ");

   $insertCamp = $stmt->execute($data);

   return $insertCamp;
}

//DOUG: converted to PDO - settings.php | newservice.php
function checkSettingExists($account_id) {
//open the DB object
   $conn = getPDOMasterCore();

//prepare the query
   $sql_count = 'SELECT COUNT(*)
                FROM core.core_account_settings
                WHERE account_id = ' . $account_id;

   if ($result = $conn->query($sql_count)) {
      if ($result->fetchColumn() == 0) {
         createDefaultSkin($account_id);
      }
   }
}

//DOUG: converted to PDO - allInclusive.php
function createDefaultSkin($account_id) {
//open the DB object
   $conn = getPDOMasterCore();

//prepare the data
   $data = array(':account_id' => $account_id);

//prepare the query
   $stmt = $conn->prepare("INSERT INTO core.core_account_settings (account_id, header_logo, login_logo, tab_logo, account_colour)
                VALUES (:account_id, '/img/skinlogos/1/1.png', '/img/skinlogos/1/1_header.png', '/img/skinlogos/1/1_icon.png', '#3c5d6d') ");

   $insert_success = $stmt->execute($data);

   return $insert_success;
}

//DOUG: converted to PDO - assignroles.php
function getRoles($userId) {
//open the DB object
   $conn = getPDOMasterCore();

//prepare the data
   $data = array(':userId' => $userId);

//prepare the query
   $stmt = $conn->prepare('SELECT r.role_id, role_name, role_url, role_icon, role_type
                FROM core.core_user_role r
                INNER JOIN core.core_role rl
                ON r.role_id = rl.role_id
                WHERE user_id = :userId
                ORDER BY role_order,role_id');

//set the fetch mode
   $stmt->setFetchMode(PDO::FETCH_ASSOC);

   $stmt->execute($data);

   $roleIds = $stmt->fetchAll();

   if (isset($roleIds)) {
      return $roleIds;
   } else {
      return 0;
   }
}

//DOUG: converted to PDO - assignroles.php
function getUnassignedRoles($userId) {
//open the DB object
   $conn = getPDOMasterCore();

//prepare the data
   $data = array(':userId' => $userId);

//prepare the query
   $stmt = $conn->prepare('SELECT r.role_id, role_name, role_url, role_icon,role_type
                FROM core.core_role r
                WHERE role_id not IN (SELECT role_id FROM core.core_user_role WHERE user_id = :userId)
                ORDER BY role_order,role_id');

//set the fetch mode
   $stmt->setFetchMode(PDO::FETCH_ASSOC);

   $stmt->execute($data);

   $roleIds = $stmt->fetchAll();

   if (isset($roleIds)) {
      return $roleIds;
   } else {
      return 0;
   }
}

//DOUG: converted to PDO - editroles.php
function editRole($role_id, $role_icon, $role_name, $role_url, $role_type, $role_order) {
//open the DB object
   $conn = getPDOMasterCore();

//prepare the data
   $data = array(':role_id' => $role_id, ':role_name' => $role_name, ':role_url' => $role_url,
       ':role_type' => $role_type, ':role_order' => $role_order, ':role_icon' => $role_icon);

//prepare the query
   $stmt = $conn->prepare('UPDATE core.core_role SET `role_name` = :role_name, `role_url` = :role_url,
        `role_icon` = :role_icon, `role_type` = :role_type, `role_order` = :role_order WHERE role_id = :role_id');

   $success = $stmt->execute($data);

   return $success;
}

//DOUG: converted to PDO - editroles.php
function insertNewRole($role_id, $role_icon, $role_name, $role_url, $role_type, $role_order) {
//open the DB object
   $conn = getPDOMasterCore();

//prepare the data
   $data = array(':role_id' => $role_id);

//prepare the query
   $stmt = $conn->prepare('SELECT count(*)
                FROM core.core_role
                WHERE role_id = :role_id ');

//set the fetch mode
   $stmt->setFetchMode(PDO::FETCH_ASSOC);

//execute it
   $stmt->execute($data);

//fetch the data
   $number_of_rows = $stmt->fetchColumn();

//if no matches do insert
   if ($number_of_rows == 0) {
//prepare the data
      $data_insert = array(':role_id' => $role_id, ':role_name' => $role_name, ':role_url' => $role_url,
          ':role_type' => $role_type, ':role_order' => $role_order, ':role_icon' => $role_icon);

//prepare the query
      $stmt_insert = $conn->prepare("INSERT INTO core.core_role (`role_id`, `role_name`, `role_url`, `role_icon`, `role_type`, `role_order`)
                    VALUES (:role_id, :role_name, :role_url, :role_icon, :role_type, :role_order) ");

      $success = $stmt_insert->execute($data_insert);

      return $success;
   } else {
      return 0;
   }
}

//DOUG: converted to PDO - assignroles.php
function addRoleToUser($user_id, $role_id) {
   $main_role_id = substr($role_id, 0, 1);
   $main_role_id = $main_role_id . '00';
   $ch = checkMainRoleAdd($user_id, $main_role_id);

   //open the DB object
   $conn = getPDOMasterCore();

   if ($ch == 0) {
      //prepare the data
      $data_insert = array(':main_role_id' => $main_role_id, ':user_id' => $user_id);

      //prepare the query
      $stmt_insert = $conn->prepare("INSERT INTO core.core_user_role (user_id, role_id)
                    VALUES (:user_id, :main_role_id) ");

      $stmt_insert->execute($data_insert);
   }

   //prepare the data
   $data_insert2 = array(':role_id' => $role_id, ':user_id' => $user_id);

   //prepare the query
   $stmt_insert2 = $conn->prepare("INSERT INTO core.core_user_role (user_id, role_id)
                    VALUES (:user_id, :role_id) ");

   $success = $stmt_insert2->execute($data_insert2);

   return $success;
}

//DOUG: converted to PDO - assignroles.php
function removeRoleFromUser($user_id, $role_id) {
   $main_role_id = substr($role_id, 0, 1);
   $main_role_id = $main_role_id . '00';
   $ch = checkMainRoleRemove($user_id, $main_role_id);

//open the DB object
   $conn = getPDOMasterCore();

   if ($ch == 2) {
//prepare the data
      $data = array(':main_role_id' => $main_role_id, ':user_id' => $user_id);

//prepare the query
      $stmt = $conn->prepare("DELETE FROM `core`.`core_user_role`
                    WHERE `role_id` = :main_role_id
                    AND `user_id` = :user_id ");

      $stmt->execute($data);
   }

//prepare the data
   $data2 = array(':role_id' => $role_id, ':user_id' => $user_id);

//prepare the query
   $stmt2 = $conn->prepare("DELETE FROM `core`.`core_user_role`
                    WHERE `role_id` = :role_id
                    AND `user_id` = :user_id ");

   $success = $stmt2->execute($data2);

   return $success;
}

//DOUG: converted to PDO - allInclusive.php
function checkMainRoleAdd($user_id, $main_role_id) {
   //open the DB object
   $conn = getPDOMasterCore();

   //prepare the data
   $data = array(':main_role_id' => $main_role_id, ':user_id' => $user_id);

   //prepare the query
   $stmt = $conn->prepare('SELECT count(*)
                FROM core.core_user_role
                WHERE role_id = :main_role_id
                AND user_id = :user_id');

   //execute it
   $stmt->execute($data);

   //get the count
   $number_of_rows = $stmt->fetchColumn();

   return $number_of_rows;
}

//DOUG: converted to PDO - allmo.php
function getAllMos($service_id, $start_date, $end_date) {
   //open the DB object
   $conn = getPDOReportingCore();

   //prepare the data
   $data = array(':service_id' => $service_id, ':start_date' => $start_date, ':end_date' => $end_date);

   //prepare the query
   $stmt = $conn->prepare("SELECT batch_id as 'batch_id',sms_id as 'Id',sms_timestamp as 'Date',sms_src as 'From',client_name as 'Client',ifnull(campaign_name,'API Interface') as 'Campaign',
                campaign_code as 'Campaign Code',batch_reference as 'Reference',sms_content as 'Content'
                FROM core.batch_sms s
                LEFT JOIN core.campaign_batch USING (batch_id)
                LEFT JOIN core.campaign_campaign USING (campaign_id)
                LEFT JOIN core.campaign_client USING (client_id)
                WHERE s.service_id = :service_id
                AND date(`sms_timestamp`) >= :start_date AND date(`sms_timestamp`) <= :end_date
                AND s.sms_status = 'MOSMS'");

   //set the fetch mode
   $stmt->setFetchMode(PDO::FETCH_ASSOC);

   $stmt->execute($data);

   $results = $stmt->fetchAll();

   return $results;
}

//DOUG: converted to PDO - batchlist.php
function getBatchlistExtraData($batch_id) {
//open the DB object
   $conn = getPDOReportingCore();

//prepare the data
   $data = array(':batch_id' => $batch_id);

//prepare the query
   $stmt = $conn->prepare("SELECT count(sms_id) as count, sms_status
                FROM core.batch_sms
                WHERE batch_id = :batch_id
                GROUP BY sms_status");

//set the fetch mode
   $stmt->setFetchMode(PDO::FETCH_ASSOC);

   $stmt->execute($data);

   $results = $stmt->fetchAll();

   return $results;
}

//DOUG: converted to PDO - batchlistmo.php
function getBatchlistExtraMOs($batch_id, $type) {
   //open the DB object
   $conn = getPDOReportingCore();

   //prepare the data
   $data = array(':batch_id' => $batch_id, ':type' => $type);

   //prepare the query
   $stmt = $conn->prepare("SELECT mosms_id,`timestamp`,`to`,`from`,content,mccmnc,smsc,prefix_network_name
                FROM (
                SELECT sms_id as 'mosms_id',sms_timestamp as 'timestamp',sms_dest as 'to',sms_src as 'from',sms_content as 'content',
                    SPLIT_STR(SPLIT_STR(sms_meta,'&',1),'=',2) as 'smsc',
                    SPLIT_STR(SPLIT_STR(sms_meta,'&',2),'=',2) as 'mccmnc'
                FROM core.batch_sms
                WHERE batch_id = :batch_id
                AND sms_status IN (:type)
                ) as MO
                LEFT JOIN
                (SELECT prefix_mccmnc, prefix_network_name, prefix_country_name FROM core.core_prefix GROUP BY prefix_mccmnc) AS core_prefix
                ON prefix_mccmnc = mccmnc");

   //set the fetch mode
   $stmt->setFetchMode(PDO::FETCH_ASSOC);

   $stmt->execute($data);

   $results = $stmt->fetchAll();

   //----
   // DOUG N.B.: I have disabled querying the archive table because it times out completely every time, this must all be moved to elasticsearch anyway
   // ---

   /*
     //is there any data in the main database
     if (count($results) == 0)
     {
     //There's nothing in there, so check the archive table quickly
     //prepare the query
     $stmt2 = $conn->prepare("SELECT mosms_id,`timestamp`,`to`,`from`,content,mccmnc,smsc,prefix_network_name
     FROM (
     SELECT sms_id as 'mosms_id',sms_timestamp as 'timestamp',sms_dest as 'to',sms_src as 'from',sms_content as 'content',
     SPLIT_STR(SPLIT_STR(sms_meta,'&',1),'=',2) as 'smsc',
     SPLIT_STR(SPLIT_STR(sms_meta,'&',2),'=',2) as 'mccmnc'
     FROM core.batch_sms_archive
     WHERE batch_id = :batch_id
     AND sms_status IN (:type)
     ) as MO
     LEFT JOIN
     (SELECT prefix_mccmnc, prefix_network_name, prefix_country_name FROM core.core_prefix GROUP BY prefix_mccmnc) AS core_prefix
     ON prefix_mccmnc = mccmnc");

     //set the fetch mode
     $stmt2->setFetchMode(PDO::FETCH_ASSOC);

     $stmt2->execute($data);

     $results = $stmt2->fetchAll();
     }
    */

   return $results;
}

//DOUG: converted to PDO - createCSVbatchlistMo.phh
function getBatchlistExtraMOsSQL($batch_id, $type, $starting, $chunk_size) {
//open the DB object
   $conn = getPDOReportingCore();

//prepare the data
   $data = array(':batch_id' => $batch_id, ':type' => $type, ':starting' => $starting, ':chunk_size' => $chunk_size);

//prepare the query
   $stmt = $conn->prepare("SELECT mosms_id,`timestamp`,`to`,`from`,content,mccmnc,smsc,prefix_network_name
                FROM (
                SELECT sms_id as 'mosms_id',sms_timestamp as 'timestamp',sms_dest as 'to',sms_src as 'from',sms_content as 'content',
                    SPLIT_STR(SPLIT_STR(sms_meta,'&',1),'=',2) as 'smsc',
                    SPLIT_STR(SPLIT_STR(sms_meta,'&',2),'=',2) as 'mccmnc'
                FROM core.batch_sms
                WHERE batch_id = :batch_id
                AND sms_status IN (:type)
                ) as MO
                LEFT JOIN
                (SELECT prefix_mccmnc, prefix_network_name, prefix_country_name FROM core.core_prefix GROUP BY prefix_mccmnc) AS core_prefix
                ON prefix_mccmnc = mccmnc
                LIMIT :chunk_size, :starting");

//set the fetch mode
   $stmt->setFetchMode(PDO::FETCH_ASSOC);

   $stmt->execute($data);

   $results = $stmt->fetchAll();

   return $results;
}

//DOUG: converted to PDO - allInclusive.php
function checkMainRoleRemove($user_id, $rNId) {
//open the DB object
   $conn = getPDOMasterCore();

//prepare the data
   $data = array(':user_id' => $user_id, ':rNId' => $rNId);

//prepare the query
   $stmt = $conn->prepare('SELECT role_id, substring(role_id,1,1)
                FROM core.core_user_role
                WHERE user_id = :user_id
                AND substring(role_id,1,1) = :rNId ');

//execute it
   $stmt->execute($data);

//get the count
   $number_of_rows = $stmt->fetchColumn();

   return $number_of_rows;
}

//DOUG: converted to PDO - clienthome.php | clienthomeOld.php | resellerhome.php | resellerhomeOld.php
function getRoleIDFromUrl($role_url) {
//open the DB object
   $conn = getPDOMasterCore();

//prepare the data
   $data = array(':role_url' => $role_url);

//prepare the query
   $stmt = $conn->prepare('SELECT role_id
                FROM core.core_role
                WHERE role_url
                LIKE :role_url ');

//set the fetch mode
   $stmt->setFetchMode(PDO::FETCH_ASSOC);

//execute it
   $stmt->execute($data);

//fetch the data
   $role = $stmt->fetch();

   return $role['role_id'];
}

//DOUG: converted to PDO - editroles.php
function getAllRoles() {
//open the DB object
   $conn = getPDOMasterCore();

//prepare the query
   $stmt = $conn->prepare('SELECT *
                FROM core.core_role;');

//set the fetch mode
   $stmt->setFetchMode(PDO::FETCH_ASSOC);

//execute it
   $stmt->execute();

//fetch the data
   $allRoles = $stmt->fetchAll();

   return $allRoles;
}

//setting.php
function updateNewColor($userRGB, $sId) {
   updateServer($userRGB, "account_colour", "core_account_settings", "account_id", $sId);
}

//DOUG: converted to PDO - setting.php
function getAccColor($account_id) {
//open the DB object
   $conn = getPDOMasterCore();

//prepare the data
   $data = array(':account_id' => $account_id);

//prepare the query
   $stmt = $conn->prepare('SELECT `account_colour`
                FROM core.core_account_settings
                WHERE `account_id` = :account_id ');

//set the fetch mode
   $stmt->setFetchMode(PDO::FETCH_ASSOC);

//execute it
   $stmt->execute($data);

//fetch the data
   $accRGB = $stmt->fetch();

   $col = '';
   if (isset($accRGB['account_colour'])) {
      $col = $accRGB['account_colour'];
   }

   return $col;
}

//DOUG: converted to PDO - .php
function getManagers() {
//open the DB object
   $conn = getPDOMasterCore();

//prepare the query
   $stmt = $conn->prepare("SELECT user_username FROM core.core_user");

//set the fetch mode
   $stmt->setFetchMode(PDO::FETCH_ASSOC);

//execute it
   $stmt->execute();

//fetch the data
   $managers = $stmt->fetchAll();

   return $managers;
}

function getMOCountForBatch($batchId)
{
   //open the DB object
   $conn = getPDOReportingCore();

   //prepare the data
   $data = array(':batch_id' => $batchId);

   //prepare the query
   $stmt = $conn->prepare("SELECT count(mosms_id) as total
             FROM core.core_mosms
             WHERE batchId = :batch_id");

   //set the fetch mode
   $stmt->setFetchMode(PDO::FETCH_ASSOC);

   //execute it
   $stmt->execute($data);

   //run the query
   $row = $stmt->fetch();

   $total_mos = $row['total'];

   return $total_mos;
}

//DOUG: converted to PDO - networkpercentmomsg.php
function getMosMsgDataNetworkReport($date, $mcc, $smsc) {
//open the DB object
   $conn = getPDOReportingCore();

//prepare the data
   $data = array(':date' => $date, ':mcc' => "mccmnc=" . $mcc, ':smsc' => "smsc=" . $smsc);

//prepare the query
   $stmt = $conn->prepare("SELECT mosms_id,`timestamp`,`to`,`from`,content,mccmnc,smsc,prefix_network_name
                FROM (
                SELECT mosms_id,timestamp,src as 'to',dest as 'from',content,replace(SPLIT_STR(meta,'&',2),'mccmnc=','') as 'mccmnc',SPLIT_STR(meta,'&',1) as 'smsc'
                FROM core.core_mosms
                WHERE date(`timestamp`) = :date
                AND SPLIT_STR(meta,'&',2) =  :mcc
                AND SPLIT_STR(meta,'&',1) =  :smsc
                ) as MO
                LEFT JOIN
                (SELECT prefix_mccmnc, prefix_network_name, prefix_country_name FROM core.core_prefix GROUP BY prefix_mccmnc) AS core_prefix
                ON prefix_mccmnc=mccmnc");

//set the fetch mode
   $stmt->setFetchMode(PDO::FETCH_ASSOC);

//execute it
   $stmt->execute($data);

//fetch the data
   $moDats = $stmt->fetchAll();

   return $moDats;
}

//DOUG: converted to PDO - apimomsgSQL.php
function getMosMsgDataAPISId($date, $service_id) {
//open the DB object
   $conn = getPDOReportingCore();

//prepare the data
   $data = array(':date' => $date, ':service_id' => $service_id);

//prepare the query
   $stmt = $conn->prepare("SELECT mosms_id,`timestamp`,`to`,`from`,content,mccmnc,smsc,prefix_network_name
                FROM (
                SELECT mosms_id,timestamp,src as 'to',dest as 'from',content,replace(SPLIT_STR(meta,'&',2),'mccmnc=','') as 'mccmnc',SPLIT_STR(meta,'&',1) as 'smsc'
                FROM core.core_mosms
                WHERE service_id = :service_id
                AND date(`timestamp`) = :date
                ) as MO
                LEFT JOIN
                (SELECT prefix_mccmnc, prefix_network_name, prefix_country_name FROM core.core_prefix GROUP BY prefix_mccmnc) AS core_prefix
                ON prefix_mccmnc=mccmnc");

//set the fetch mode
   $stmt->setFetchMode(PDO::FETCH_ASSOC);

//execute it
   $stmt->execute($data);

//fetch the data
   $moDats = $stmt->fetchAll();

   return $moDats;
}

//DOUG: converted to PDO - searchMOAdminold.php | searchMOold.php //Refactored, but SHOULD DELETE IF NOT USED
function getMOForSearch($start_date, $end_date, $cont, $dest, $src, $offset, $order) {
//open the DB object
   $conn = getPDOReportingCore();

//prepare the data
   $data = array(':start_date' => $start_date, ':end_date' => $end_date, ':cont' => $cont, ':dest' => $dest,
       ':src' => $src, ':offset' => $offset, ':order' => $order);

//prepare the query
   $stmt = $conn->prepare("SELECT mosms_id,`timestamp`,`to`,`from`,content,mccmnc,smsc,prefix_network_name
                FROM (
                SELECT mosms_id,timestamp,src as 'to',dest as 'from',content,replace(SPLIT_STR(meta,'&',2),'mccmnc=','') as 'mccmnc',replace(SPLIT_STR(meta,'&',1),'smsc=','') as 'smsc'
                FROM core.core_mosms
                WHERE date(timestamp) >= :start_date
                AND date(timestamp) <= :end_date
                :cont
                :dest
                :src
                ) as MO
                LEFT JOIN
                (SELECT prefix_mccmnc, prefix_network_name, prefix_country_name FROM core.core_prefix GROUP BY prefix_mccmnc) AS core_prefix
                ON prefix_mccmnc = mccmnc
                :order
                LIMIT :offset,100; ");

//set the fetch mode
   $stmt->setFetchMode(PDO::FETCH_ASSOC);

//execute it
   $stmt->execute($data);

//fetch the data
   $moData = $stmt->fetchAll();

   return $moData;
}

//DOUG: converted to PDO - clienthomeOld.php | home.php | homeOld.php | resellerhomeOld.php
function getMonthTotalsForDash($start_date, $end_date) {
   //open the DB object
   $conn = getPDOReportingCore();

   //prepare the data
   $data = array(':start_date' => $start_date, ':end_date' => $end_date);

   //prepare the query
   $stmt = $conn->prepare("SELECT YEAR(cdr_record_date) as 'Year', MONTHNAME(cdr_record_date) as 'month',sum(cdr_record_units) as 'cdr_record_units'
                           FROM reporting.cdr_record_archive cr
                           INNER JOIN reporting.cdr c
                           on cr.cdr_id = c.cdr_id
                           where date(cdr_record_date) >= :start_date and date(cdr_record_date) < :end_date
                           and SPLIT_STR(cdr_record_reference,'|',1) not in ('legacycore')
                           group by YEAR(cdr_record_date),MONTHNAME(cdr_record_date)
                           ORDER BY cdr_record_date");

   //set the fetch mode
   $stmt->setFetchMode(PDO::FETCH_ASSOC);

   //execute it
   $stmt->execute($data);

   //fetch the data
   $networkReport = $stmt->fetchAll();

   return $networkReport;
}

//DOUG: converted to PDO - netrep.php
function getNetworkReport($start_date, $end_date) {
   //open the DB object
   $conn = getPDOReportingCore();

   //prepare the data
   $data = array(':start_date' => $start_date, ':end_date' => $end_date);

   //prepare the query
   $stmt = $conn->prepare("SELECT prefix_country_name,concat(prefix_network_name,' (',mccmnc,')') as network,smsc,SUM(cdr_record_units) AS cdr_record_units, cdr_cost_per_unit
                FROM
                (SELECT SUBSTRING_INDEX(SUBSTRING_INDEX(cdr_provider, '|', -2), '|', 1) AS `smsc`,SUBSTRING_INDEX(SUBSTRING_INDEX(cdr_provider, '|', -1), '|', 1) AS `mccmnc`,cdr.*
                FROM reporting.cdr WHERE cdr_provider LIKE 'core|mtsms|%' OR cdr_provider LIKE 'legacy|mtsms|%' OR cdr_provider LIKE 'rdnc|%'  ORDER BY cdr_id DESC) as cdr
                LEFT JOIN
                (SELECT prefix_mccmnc, prefix_network_name, prefix_country_name FROM core.core_prefix GROUP BY prefix_mccmnc) AS core_prefix
                ON prefix_mccmnc=mccmnc
                LEFT JOIN reporting.cdr_record_archive USING (cdr_id)
                WHERE date(cdr_record_date) >= :start_date and date(cdr_record_date) < :end_date
                GROUP BY smsc, prefix_country_name, concat(prefix_network_name,' (',mccmnc,')'), cdr_cost_per_unit
                ORDER BY 4 desc");

   //set the fetch mode
   $stmt->setFetchMode(PDO::FETCH_ASSOC);

   //execute it
   $stmt->execute($data);

   //fetch the data
   $networkReport = $stmt->fetchAll();

   return $networkReport;
}

//DOUG: converted to PDO - pause.php
function getClientAndCampaignName($campaign_id) {
   //open the DB object
   $conn = getPDOReportingCore();

   //prepare the data
   $data = array(':campaign_id' => $campaign_id);

   //prepare the query
   $stmt = $conn->prepare("SELECT client_name, campaign_name
                FROM core.campaign_client
                LEFT JOIN core.campaign_campaign USING (client_id)
                WHERE campaign_id = :campaign_id AND campaign_status = 'ENABLED' ");

   //set the fetch mode
   $stmt->setFetchMode(PDO::FETCH_ASSOC);

   //execute it
   $stmt->execute($data);

   //fetch the data
   $campData = $stmt->fetch();

   return $campData;
}

//LUBEN: converted to PDO - clients.php
function getClientReport($startDate, $endDate) {
   //open the DB object
   $conn = getPDOReportingCore();

   //prepare the data
   $data = array(':start_date' => $startDate, ':end_date' => $endDate);

   //prepare the query
   $stmt = $conn->prepare("SELECT SUM(cdr_record_units) as 'Units',
                a.account_name as 'Account',
                s.service_name as 'Service',
                sc.service_credit_billing_type as 'Billing',
                sc.service_credit_available as 'Balance',
                u.user_fullname
                FROM
                (SELECT cdr_id
                FROM reporting.cdr WHERE cdr_provider LIKE 'core|mtsms|%' ORDER BY cdr_id DESC) 
                as CDR
                LEFT JOIN 
                (
                SELECT cdr_id,cdr_record_units,SPLIT_STR(cdr_record_reference,'|',2) as 'service_id'
                FROM reporting.cdr_record_archive
                WHERE date(cdr_record_date) >= :start_date and date(cdr_record_date) < :end_date
                ) as CDR_RECORD
                ON CDR.cdr_id = CDR_RECORD.cdr_id
                LEFT JOIN core.core_service s
                ON CDR_RECORD.service_id = s.service_id
                LEFT JOIN core.core_account a
                ON s.account_id = a.account_id
                LEFT JOIN core.core_service_credit sc
                ON s.service_id = sc.service_id
                LEFT JOIN core.core_user u
                ON s.service_manager_user_id = u.user_id
                Where cdr_record_units is not null
                GROUP BY a.account_name,s.service_name,sc.service_credit_billing_type,u.user_fullname");

   //set the fetch mode
   $stmt->setFetchMode(PDO::FETCH_ASSOC);

   //execute it
   $stmt->execute($data);

   //fetch the data
   $clientData = $stmt->fetchAll();

   return $clientData;
}

//DOUG: converted to PDO - bulksms.php | bulksms2.php | bulksmsold.php
function getCampaignData($service_id) {
   //open the DB object
   $conn = getPDOReportingCore();

   //prepare the data
   $data = array(':service_id' => $service_id);

   //prepare the query
   $stmt = $conn->prepare("SELECT client_id, client_name, client_description, campaign_id, campaign_name, campaign_code
                FROM core.campaign_client
                LEFT JOIN core.campaign_campaign USING (client_id)
                WHERE service_id = :service_id AND campaign_status = 'ENABLED'
                ORDER BY client_name ASC");

   //set the fetch mode
   $stmt->setFetchMode(PDO::FETCH_ASSOC);

   //execute it
   $stmt->execute($data);

   //fetch the data
   $campData = $stmt->fetchAll();

   return $campData;
}

//DOUG: converted to PDO - bulksms.php | bulksms2.php | bulksmscont.php | bulksmscont2.php | bulksmscontold.php | bulksmsold.php
function getTempData($campaign_id) {
   //open the DB object
   $conn = getPDOMasterCore();

   //prepare the data
   $data = array(':campaign_id' => $campaign_id);

   //prepare the query
   $stmt = $conn->prepare("SELECT template_id, template_content, template_description, template_status
                FROM core.campaign_template
                WHERE campaign_id = :campaign_id
                ORDER BY template_description ASC");

   //set the fetch mode
   $stmt->setFetchMode(PDO::FETCH_ASSOC);

   //execute it
   $stmt->execute($data);

   //fetch the data
   $template_data = $stmt->fetchAll();

   return $template_data;
}

function getTemplateById($template_id) {
   //open the DB object
   $conn = getPDOMasterCore();

   //prepare the data
   $data = array(':template_id' => $template_id);

   //prepare the query
   $stmt = $conn->prepare("SELECT template_id, template_content, template_description, template_status
                FROM core.campaign_template
                WHERE template_id = :template_id");

   //set the fetch mode
   $stmt->setFetchMode(PDO::FETCH_ASSOC);

   //execute it
   $stmt->execute($data);

   //fetch the data
   $template_data = $stmt->fetch();

   return $template_data;
}

//createservice.php
function createNewResellerService($accType, $bilType, $serviceName, $serviceLogin, $servicePass, $resellerId, $threshold, $threshEmail, $threshCell, $moDRL, $dlrURL) {
   $accountID = $_SESSION['accountId'];

   $managerID = $_SESSION['userId'];

   $serviceID = insertInto_core_service($accountID, $serviceName, $serviceLogin, $managerID);

   updateServer($resellerId, 'service_id_reseller', 'core.core_service', 'service_id', $serviceID);

   $iICSC = insertInto_core_service_credit($serviceID, $bilType, $threshold, $threshEmail, $threshCell);

   $uId = createNewUser($serviceLogin, $servicePass, $serviceName, $accountID, $serviceID);

   $clieID = createClient($serviceID, $uId);

   createCampaign($clieID, $uId);

   addConnetUsers($serviceID);

   addUser($uId, $serviceID);

   if (checkUserInPerm($managerID, $serviceID) == 0) {
      addUser($managerID, $serviceID);
   }

   addRolesToUser($uId, 'Client');

//createDlrUrls($serviceID, $dlrURL, 'ENABLED', $accType, "dlr", "strict=1");
   setDlrUrls($serviceID, $dlrURL, 'ENABLED', $accType, "strict=1");

   setMosmsUrls($serviceID, $moDRL, 'ENABLED', $accType, "strict=1");

   return 1;
}

//createservice.php
function createNewResellerServiceNoDLRs($accType, $bilType, $serviceName, $serviceLogin, $servicePass, $resellerId, $threshold, $threshEmail, $threshCell) {
   $accountID = $_SESSION['accountId'];

   $managerID = $_SESSION['userId'];

   $serviceID = insertInto_core_service($accountID, $serviceName, $serviceLogin, $managerID);

   updateServer($resellerId, 'service_id_reseller', 'core.core_service', 'service_id', $serviceID);

   $iICSC = insertInto_core_service_credit($serviceID, $bilType, $threshold, $threshEmail, $threshCell);

   $uId = createNewUser($serviceLogin, $servicePass, $serviceName, $accountID, $serviceID);

   $clieID = createClient($serviceID, $uId);

   createCampaign($clieID, $uId);

   addConnetUsers($serviceID);

   addUser($uId, $serviceID);

   if (checkUserInPerm($managerID, $serviceID) == 0) {
      addUser($managerID, $serviceID);
   }

   addRolesToUser($uId, 'Client');

//createDlrUrls($serviceID, $dlrURL, 'ENABLED', $accType, "dlr", "strict=1");
   /* setDlrUrls($serviceID, $dlrURL, 'ENABLED', $accType, "strict=1");

     setMosmsUrls($serviceID, $moDRL, 'ENABLED', $accType, "strict=1"); */

   return 1;
}

//DOUG: converted to PDO - allInclusive.php
function getResellerServiceId($account_id) {
//open the DB object
   $conn = getPDOMasterCore();

//prepare the data
   $data = array(':account_id' => $account_id);

//prepare the query
   $stmt = $conn->prepare('SELECT service_id
                FROM core.core_service
                WHERE service_id = service_id_reseller
                AND account_id = :account_id ');

//set the fetch mode
   $stmt->setFetchMode(PDO::FETCH_ASSOC);

//execute it
   $stmt->execute($data);

//fetch the data
   $row = $stmt->fetch();

   $service_id = 0;
   if (isset($row)) {
      $service_id = $row['service_id'];
   }

   return $service_id;
}

//newservice.php
function createNewService($manager, $accType, $bilType, $accountName, $serviceName, $serviceLogin, $servicePass, $vodaCost, $mtnCost, $cellCost, $telkCost, $threshold, $threshEmail, $threshCell, $moDRL, $dlrURL, $newAcc, $resell)
{
   if ($newAcc == 'true')
   {
      $accountID = createNewAccount($accountName);
   }
   else
   {
      $accountID = getAccounts($accountName);
   }

   $manager = lcfirst($manager);

   $routePrices = [$vodaCost, $mtnCost, $cellCost, $telkCost];

   $managerID = getConnetManagers($manager);

   $serviceID = insertInto_core_service($accountID, $serviceName, $serviceLogin, $managerID);

   if ($resell == 'true')
   {
      updateServer($serviceID, 'service_id_reseller', 'core.core_service', 'service_id', $serviceID);
   }
   else
   {
      $rRId = getResellerServiceId($accountID);
      if ($rRId == 0)
      {
         updateServer('1', 'service_id_reseller', 'core.core_service', 'service_id', $serviceID);
      }
      else
      {
         updateServer($rRId, 'service_id_reseller', 'core.core_service', 'service_id', $serviceID);
      }
   }

   $iICSC = insertInto_core_service_credit($serviceID, $bilType, $threshold, $threshEmail, $threshCell);

   $uId = createNewUser($serviceLogin, $servicePass, $serviceName, $accountID, $serviceID);

   $clieID = createClient($serviceID, $uId);

   createCampaign($clieID, $uId);

   addConnetUsers($serviceID);

   addUser($uId, $serviceID);

   if (checkUserInPerm($managerID, $serviceID) == 0) {
      addUser($managerID, $serviceID);
   }

//createDlrUrls($serviceID, $dlrURL, 'ENABLED', $accType, "dlr", "strict=1");
   setDlrUrls($serviceID, $dlrURL, 'ENABLED', $accType, "strict=1");

   setMosmsUrls($serviceID, $moDRL, 'ENABLED', $accType, "strict=1");

//$moDRL = insertMODRL()
//$dlrURL = insertDRLURL()

   for ($i = 0; $i < count($routePrices); $i++) {
      if ($i == 0) {
         $mcc = '65501';
         $providerID = '27';
         $network = 'OUT Vodacom Autotag ';
         $rdn = '2782***************';
         $rc = '2782***************';
      } elseif ($i == 1) {
         $mcc = '65510';
         $providerID = '14';
         $network = 'OUT MTN Autotag ';
         $rdn = '2783***************';
         $rc = '2783***************';
      } elseif ($i == 2) {
         $mcc = '65507';
         $providerID = '21';
         $network = 'OUT Cell C Autotag ';
         $rdn = '2784***************';
         $rc = '2784***************';
      } elseif ($i == 3) {
         $mcc = '65502';
         $providerID = '14';
         $network = 'OUT 8ta Autotag ';
         $rdn = '2781***************';
         $rc = '2781***************';
      }

      insertInto_core_route($providerID, $serviceID, $network, $accountID, $serviceName, $serviceLogin, $rdn, $rc, $managerID, $routePrices[$i], $mcc);
   }

   return 1;
}

//allInclusive.php
function createClient($service_id, $user_id) {
//open the DB object
   $conn = getPDOMasterCore();

//prepare the data
   $data = array(':service_id' => $service_id, ':user_id' => $user_id);

//prepare the query
   $stmt = $conn->prepare('INSERT INTO core.campaign_client (service_id,user_id,client_name,client_status)
                VALUES (:service_id, :user_id, "Default", "ENABLED")');

//execute it
   $stmt->execute($data);

//return the insert ID
   return $conn->lastInsertId('client_id');
}

//allInclusive.php
function createCampaign($client_id, $user_id) {
//open the DB object
   $conn = getPDOMasterCore();

//prepare the data
   $data = array(':client_id' => $client_id, ':user_id' => $user_id);

//prepare the query
   $stmt = $conn->prepare('INSERT INTO core.campaign_campaign(client_id,user_id,campaign_name,campaign_code,campaign_status)
                VALUES (:client_id, :user_id, "Default", "DEFAULT", "ENABLED") ');

//execute it
   $stmt->execute($data);

//return the insert ID
   return $conn->lastInsertId();
}

//allInclusive.php
function checkUserInPerm($manager_id, $service_id) {
//open the DB object
   $conn = getPDOMasterCore();

//prepare the data
   $data = array(':manager_id' => $manager_id, ':service_id' => $service_id);

//prepare the query
   $stmt = $conn->prepare('SELECT count(*)
                FROM core.core_permission
                WHERE user_id = :manager_id
                AND service_id = :service_id ');

//execute it
   $stmt->execute($data);

//get the count
   $number_of_rows = $stmt->fetchColumn();

   if ($number_of_rows == 0) {
      return 0;
   } else {
      return 1;
   }
}

//allInclusive.php
function addConnetUsers($service_id) {
//open the DB object
   $conn = getPDOMasterCore();

//prepare the data
   $data = array(':service_id' => $service_id);

//add all the relevant admin users
   $sql = 'INSERT INTO core.core_permission (user_id, service_id, permission_type) VALUES ';
   $sql .= '(1, :service_id, "submit.mtsms"),(1, :service_id, "SUPER"),'; //Niel
   $sql .= '(3, :service_id, "submit.mtsms"),(3, :service_id, "SUPER"),'; //Brendan
   $sql .= '(40, :service_id, "submit.mtsms"),(40, :service_id, "SUPER"),'; //Caitrin
   $sql .= '(225, :service_id, "submit.mtsms"),(225, :service_id, "SUPER"),'; //Louise
   $sql .= '(92, :service_id, "submit.mtsms"),(92, :service_id, "SUPER"),'; //Thinus
   $sql .= '(18, :service_id, "submit.mtsms"),(18, :service_id, "SUPER"),'; //wayne.connet
   $sql .= '(521, :service_id, "submit.mtsms"),(521, :service_id, "SUPER")'; //michiel.connet
//prepare the query
   $stmt = $conn->prepare($sql);

//execute it
   $stmt->execute($data);

//return the insert ID
   return $conn->lastInsertId();
}

//addroutes.php | addrouteadmin.php | newresellerroutes.php | newroutes.php
function checkRouteExistsInService($provider_id, $service_id, $route_code, $route_mccmnc) {
   //open the DB object
   $conn = getPDOMasterCore();

   //prepare the data
   $data = array(':provider_id' => $provider_id, ':service_id' => $service_id, ':route_code' => $route_code, ':route_mccmnc' => $route_mccmnc);

   //prepare the query
   $stmt = $conn->prepare("SELECT count(*) FROM core.core_route
      WHERE provider_id = :provider_id AND service_id = :service_id
      AND route_code = :route_code AND route_mccmnc = :route_mccmnc");

   //execute it
   $stmt->execute($data);

   //return if it exists
   $number_of_rows = $stmt->fetchColumn();

   if ($number_of_rows > 0) {
      return true;
   } else {
      return false;
   }
}

//addroutes.php | addrouteadmin.php | newresellerroutes.php | newroutes.php
function createNewClientRoutes($route_price, $service_id, $manId, $route_description, $provider_id, $route_msg_type, $route_display_name, $route_code, $route_status, $regex, $route_match_priority, $route_mccmnc, $route_collection_id) {
   //some preparation
   $route_match_regex = urldecode($regex);
   $route_billing = $route_price . ',0,0,0,' . $route_price;

   //open the DB object
   $conn = getPDOMasterCore();

   //prepare the data
   $data = array(':provider_id' => $provider_id, ':service_id' => $service_id, ':route_msg_type' => $route_msg_type, ':route_description' => $route_description,
       ':route_display_name' => $route_display_name, ':route_code' => $route_code, ':route_status' => $route_status, ':route_match_regex' => $route_match_regex,
       ':route_match_priority' => $route_match_priority, ':route_billing' => $route_billing, ':route_mccmnc' => $route_mccmnc, ':route_collection_id' => $route_collection_id);


   //prepare the query
   $stmt = $conn->prepare("INSERT INTO core.core_route (provider_id, service_id, route_msg_type, route_description, route_display_name, route_code, route_status, route_match_regex, route_match_priority, route_billing, route_mccmnc, route_collection_id)
                VALUES (:provider_id, :service_id, :route_msg_type, :route_description, :route_display_name, :route_code, :route_status, :route_match_regex,
                :route_match_priority, :route_billing, :route_mccmnc, :route_collection_id) ");

   //execute it
   $stmt->execute($data);

   //return the insert ID
   return $conn->lastInsertId();
}

//allInclusive.php
function addRolesToUser($user_id, $type) {
   //open the DB object
   $conn = getPDOMasterCore();

   if ($type == 'Reseller') {
      //Reseller ids for now : 10, 11, 13;
      $roleIds = [100, 104, 400, 404, 405, 401, 402, 403, 407, 409, 410, 200, 206, 101, 103, 105, 106, 107, 212, 800, 801];
      for ($i = 0; $i < count($roleIds); $i++) {
         //prepare the data
         $data = array(':user_id' => $user_id, ':role_id' => $roleIds[$i]);

         //prepare the query
         $stmt = $conn->prepare("INSERT INTO core.core_user_role (user_id, role_id)
                        VALUES (:user_id, :role_id) ");

         //execute it
         $stmt->execute($data);
      }
   }
   if ($type == 'Client')
   {
      $roleIds = [400, 401, 402, 403, 407, 404, 405, 409, 410, 200, 206, 100, 104, 212];
      for ($i = 0; $i < count($roleIds); $i++)
      {
         //prepare the data
         $data = array(':user_id' => $user_id, ':role_id' => $roleIds[$i]);

         //prepare the query
         $stmt = $conn->prepare("INSERT INTO core.core_user_role (user_id, role_id)
                        VALUES (:user_id, :role_id) ");

         //execute it
         $stmt->execute($data);
      }
   }

//wtf why is this here?
//if($type == 'Admin'){}

   return true;
}

//allInclusive.php
function createNewAccount($account_name) {
//open the DB object
   $conn = getPDOMasterCore();

//prepare the data
   $data = array(':account_name' => $account_name);

//prepare the query
   $stmt = $conn->prepare("INSERT INTO core.core_account (account_name, account_status)
                VALUES (:account_name, 'ENABLED') ");

//execute it
   $stmt->execute($data);

//return the insert ID
   return $conn->lastInsertId('account_id');
}

//allInclusive.php //DOUG HERE
function createNewUser($user_username, $user_password, $user_fullname, $account_id, $service_id) {
   //open the DB object
   $conn = getPDOMasterCore();

   //new, HASH THAT PASSWORD
   $hashed_password = helperPassword::createHashedPassword($user_password);

   //prepare the data
   $data = array(':user_username' => $user_username, ':user_password' => $user_password, ':user_fullname' => $user_fullname,
       ':user_account_id' => $account_id, ':user_default_service_id' => $service_id, ':user_password_hashed' => $hashed_password);

   //prepare the query
   $stmt = $conn->prepare('INSERT INTO core.core_user (user_username, user_password, user_fullname, user_account_id, user_default_service_id, user_password_hashed)
                    VALUES (:user_username, :user_password, :user_fullname, :user_account_id, :user_default_service_id, :user_password_hashed) ');

   //execute it
   $stmt->execute($data);

   //return the insert ID
   return $conn->lastInsertId('user_id');
}

//pause.php
function getUserNameFromID($user_id) {
//open the DB object
   $conn = getPDOMasterCore();

//prepare the data
   $data = array(':user_id' => $user_id);

//prepare the query
   $stmt = $conn->prepare("SELECT user_username FROM core.core_user WHERE user_id = :user_id ");

//set the fetch mode
   $stmt->setFetchMode(PDO::FETCH_ASSOC);

//execute it
   $stmt->execute($data);

//fetch the data
   $row = $stmt->fetch();

   $userName = '';
   if (isset($row['user_username'])) {
      $userName = $row['user_username'];
   }

   return $userName;
}

//bulksms.php | confirmbulkfile.php | quicksms.php
function getServiceCredit($service_id) {
//open the DB object
   $conn = getPDOMasterCore();

//prepare the data
   $data = array(':service_id' => $service_id);

//prepare the query
   $stmt = $conn->prepare("SELECT service_credit_available FROM core.core_service_credit WHERE service_id = :service_id ");

//set the fetch mode
   $stmt->setFetchMode(PDO::FETCH_ASSOC);

//execute it
   $stmt->execute($data);

//fetch the data
   $row = $stmt->fetch();

   $serC = 0;
   if (isset($row['service_credit_available'])) {
      $serC = $row['service_credit_available'];
   }

   return $serC;
}

//bulksms.php | confirmbulkfile.php | quicksms.php
function getServiceType($service_id) {
//open the DB object
   $conn = getPDOMasterCore();

//prepare the data
   $data = array(':service_id' => $service_id);

//prepare the query
   $stmt = $conn->prepare("SELECT service_credit_billing_type FROM core.core_service_credit  WHERE service_id = :service_id ");

//set the fetch mode
   $stmt->setFetchMode(PDO::FETCH_ASSOC);

//execute it
   $stmt->execute($data);

//fetch the data
   $row = $stmt->fetch();

   $serT = 0;
   if (isset($row['service_credit_billing_type'])) {
      $serT = $row['service_credit_billing_type'];
   }

   return $serT;
}

//allInclusive.php
function insertInto_core_service_credit($service_id, $billing_type, $threshold, $notification_email, $notification_sms) {
//open the DB object
   $conn = getPDOMasterCore();

//prepare the data
   $data = array(':service_id' => $service_id, ':billing_type' => $billing_type, ':threshold' => $threshold,
       ':notification_email' => $notification_email, ':notification_sms' => $notification_sms);

//prepare the query
   $stmt = $conn->prepare("INSERT INTO core.core_service_credit (service_id, service_credit_msg_type, service_credit_billing_type, service_credit_available,
                service_iso_id, service_notification_threshold, service_notification_email, service_notification_sms)
                VALUES (:service_id, 'MTSMS', :billing_type, '0', '166', :threshold, :notification_email, :notification_sms ) ");

//execute it
   $stmt->execute($data);

//return the insert ID
   return $conn->lastInsertId();
}

//allInclusive.php
/**
 * @param $provider_id
 * @param $service_id
 * @param $network
 * @param $accountID
 * @param $serviceName
 * @param $service_login
 * @param $route_display_name
 * @param $route_code
 * @param $managerID
 * @param $route_prices
 * @param $mcc
 * @return mixed
 */
function insertInto_core_route($provider_id, $service_id, $network, $accountID, $serviceName, $service_login, $route_display_name, $route_code, $managerID, $route_prices, $mcc) {
   $route_billing = $route_prices . ',0,0,0,' . $route_prices;
   $route_regex = "/\|mtsms\|" . $mcc . "\|.*/";

   //open the DB object
   $conn = getPDOMasterCore();

   //prepare the data
   $data = array(':provider_id' => $provider_id, ':service_id' => $service_id, ':route_description' => $network . " " . $service_login,
       ':route_display_name' => $route_display_name, ':route_code' => $route_code, ':route_match_regex' => $route_regex,
       ':route_billing' => $route_billing, ':route_mccmnc' => $mcc);

   //prepare the query
   $stmt = $conn->prepare("INSERT INTO core.core_route (provider_id, service_id, route_msg_type, route_description, route_display_name, route_code,
                route_status, route_match_regex, route_match_priority, route_billing, route_mccmnc, route_collection_id)
                VALUES (:provider_id, :service_id, 'MTSMS', :route_description, :route_display_name, :route_code,
                'ENABLED', :route_match_regex, '50', :route_billing, :route_mccmnc, '14') ");

   //execute it
   $stmt->execute($data);

   //return the insert ID
   return $conn->lastInsertId();
}

//allInclusive.php
function insertInto_core_service($accountID, $serviceName, $serviceLogin, $managerID) {
   $tmpStr = " " . $serviceName . " ";
   if (stripos($tmpStr, 'smpp')) {
      $meta = 'cm_slots=00-99&cm_smpp&smpp_queued_submit';
   } else {
      $meta = 'cm_slots=00-99';
   }

   //open the DB object
   $conn = getPDOMasterCore();

   //prepare the data
   $data = array(
       ':account_id' => $accountID,
       ':service_name' => $serviceName,
       ':service_login' => $serviceLogin,
       ':service_meta' => $meta,
       ':service_manager_user_id' => $managerID);

   //prepare the query
   $sql = <<<SQLGOESHERE
           
INSERT INTO core.core_service (account_id, service_name, service_login, service_meta, service_manager_user_id)
VALUES (:account_id, :service_name, :service_login, :service_meta, :service_manager_user_id)            
           
SQLGOESHERE;


   $stmt = $conn->prepare($sql);

   //set the fetch mode
   $stmt->setFetchMode(PDO::FETCH_ASSOC);

   //execute it
   $stmt->execute($data);

   $service_id = $conn->lastInsertId('service_id');
   define('LAST_INSERTED_SERVICE_ID', $service_id);

   return $service_id;
}

//addUser.php
function getAccIdAccNServN($service_id) {
//open the DB object
   $conn = getPDOMasterCore();

//prepare the data
   $data = array(':service_id' => $service_id);

//prepare the query
   $stmt = $conn->prepare("SELECT s.service_name, a.account_id, a.account_name
                FROM core.core_service s
                JOIN core.core_account a
                ON s.account_id = a.account_id
                WHERE service_id =  :service_id");

//set the fetch mode
   $stmt->setFetchMode(PDO::FETCH_ASSOC);

//execute it
   $stmt->execute($data);

//fetch the data
   $row = $stmt->fetchAll();

   return $row;
}

//allInclusive.php
function selectFromDB($qry, $dbToSelect = null, $con = null) {
//open the DB object
   $conn = getPDOReportingCore();

//prepare the query
   $stmt = $conn->prepare($qry);

//set the fetch mode
   $stmt->setFetchMode(PDO::FETCH_ASSOC);

//execute it
   $stmt->execute();

   $result = $stmt->fetchAll();

   return $result;
}

//allInclusive.php - CANDIDATE FOR DELETION
function selectFromMasterDB($qry, $dbToSelect, $con = null) {
   if ($con == null) {
      $con = connToMasterDB($dbToSelect);
   }
   $result = mysqli_query($con, $qry);

   return $result;
}

//allInclusive.php - CANDIDATE FOR DELETION
function updateToDB($qry, $dbToConnect, $con = null) {
   if ($con == null) {
      $con = connToMasterDB($dbToConnect);
   }
   mysqli_query($con, $qry);
   mysqli_close($con);

   return true;
}

//newservice.php
function checkAccountName($account_name) {
//open the DB object
   $conn = getPDOReportingCore();

//prepare the data
   $data = array(':account_name' => $account_name);

//prepare the query
   $stmt = $conn->prepare("SELECT count(*) FROM core.core_account
                WHERE account_name LIKE :account_name ");

//execute it
   $stmt->execute($data);

//fetch the data
   $number_of_rows = $stmt->fetchColumn();

   if ($number_of_rows == 0) {
      return 1;
   } else {
      return 0;
   }
}

//createservice.php | newservice.php | updateservice.php
function checkServiceName($service_name) {
//open the DB object
   $conn = getPDOReportingCore();

//prepare the data
   $data = array(':service_name' => $service_name);

//prepare the query
   $stmt = $conn->prepare('SELECT count(*) FROM core.core_service
                    WHERE service_name LIKE :service_name ');

//execute it
   $stmt->execute($data);

//fetch the data
   $number_of_rows = $stmt->fetchColumn();

   if ($number_of_rows == 0) {
      return 1;
   } else {
      return 0;
   }
}

//createservice.php
function checkServiceLogin($service_login) {
//open the DB object
   $conn = getPDOReportingCore();

//prepare the data
   $data = array(':service_login' => $service_login);

//prepare the query
   $stmt = $conn->prepare('SELECT count(*) FROM core.core_service
                    WHERE service_login LIKE :service_login ');

//execute it
   $stmt->execute($data);

//fetch the data
   $number_of_rows = $stmt->fetchColumn();

   if ($number_of_rows == 0) {
      return 1;
   } else {
      return 0;
   }
}

//addUser.php | newuser.php | updateservice.php
function checkUserName($user_username) {
   //open the DB object
   $conn = getPDOReportingCore();

   //prepare the data
   $data = array(':user_username' => $user_username);

   //prepare the query
   $stmt = $conn->prepare('SELECT count(*) FROM core.core_user
                    WHERE user_username LIKE :user_username ');

   //execute it
   $stmt->execute($data);

   //fetch the data
   $number_of_rows = $stmt->fetchColumn();

   if ($number_of_rows == 0) {
      return 1;
   } else {
      return 0;
   }
}

//updateservice.php
function checkLowBalanceEntry($service_id, $column) {
   //open the DB object
   $conn = getPDOReportingCore();

   //prepare the data
   $data = array(':service_id' => $service_id, ':column' => $column);

   //prepare the query
   $stmt = $conn->prepare('SELECT :column
    			FROM core.core_service_credit
    			WHERE service_id = :service_id ');

   //execute it
   $stmt->execute($data);

   //fetch the data
   $result = $stmt->fetch();

   if ($result[$column] != 0 || $result[$column] != "") {
      $return = $result[$column];
   } else {
      $return = -1;
   }

   return $return;
}

//updateservice.php
function checkDLR($dlr_address, $service_id) {
   //open the DB object
   $conn = getPDOReportingCore();

   //prepare the data
   $data = array(':dlr_address' => $dlr_address, ':service_id' => $service_id);

   //prepare the query
   $stmt = $conn->prepare("SELECT endpoint_dlr_id
                     FROM core.push_endpoint_dlr
                     WHERE endpoint_dlr_address = :dlr_address AND service_id = :service_id");

   //execute it
   $stmt->execute($data);

   //fetch the data
   $row = $stmt->fetch();

   if (isset($row['endpoint_dlr_id'])) {
      return 1;
   } else {
      return 0;
   }
}

//updateservice.php
function checkMOSMS($mosms_address, $service_id) {
   //open the DB object
   $conn = getPDOReportingCore();

   //prepare the data
   $data = array(':mosms_address' => $mosms_address, ':service_id' => $service_id);

   //prepare the query
   $stmt = $conn->prepare("SELECT endpoint_mosms_id
                     FROM core.push_endpoint_mosms
                     WHERE endpoint_mosms_address = :mosms_address AND service_id = :service_id");

   //execute it
   $stmt->execute($data);

   //fetch the data
   $row = $stmt->fetch();
   if (isset($row['endpoint_mosms_id'])) {
      return 1;
   } else {
      return 0;
   }
}

function getBillingInfoByMCCMNC($service_id, $mccmnc) {
   $conn = getPDOMasterCore();

   $data = array(':service_id' => $service_id, ':mccmnc' => $mccmnc);

   //query
   $query = 'SELECT route_billing, prefix_country_name, route_currency, TRIM(SUBSTRING_INDEX(prefix_network_name,\'/\',-1)) as `prefix_network_name`
            FROM (
            SELECT route_billing,route_mccmnc,route_currency
            FROM core.core_route
            WHERE service_id = :service_id
            AND route_mccmnc = :mccmnc
            ) AS ROUTE
            LEFT JOIN
            (SELECT prefix_mccmnc, prefix_network_name, prefix_country_name FROM core.core_prefix GROUP BY prefix_mccmnc) AS core_prefix
            ON prefix_mccmnc = route_mccmnc';

   //prepare the query
   $stmt = $conn->prepare($query);

   //set the fetch mode
   $stmt->setFetchMode(PDO::FETCH_ASSOC);

   //execute it
   $stmt->execute($data);

   //fetch the data
   $mccmnc = $stmt->fetch();

   $conn = null;

   return $mccmnc;
}

//billing.php
function getInternationalBillingUnits($start_date, $end_date, $mccmnc) {
//open the DB object
   $conn = getPDOReportingCore();

//prepare the data
   $data = array(':start_date' => $start_date, ':end_date' => $end_date);

//if mccmnc
   if (isset($mccmnc) && !empty($mccmnc)) {
      $data[":mccmnc"] = $mccmnc;
   }

   $query = "SELECT sum(cdr_record_units) as 'units', SPLIT_STR(cdr_record_reference,'|',2) as 'service_id'
                FROM reporting.cdr_record_archive cr
                INNER JOIN reporting.cdr c
                on cr.cdr_id = c.cdr_id
                where date(cdr_record_date) >= :start_date and date(cdr_record_date) <= :end_date
                and SPLIT_STR(cdr_provider,'|',4) ";
   if (isset($mccmnc) && !empty($mccmnc)) {
      $query .= " = :mcc ";
   }

   $query .= " and SPLIT_STR(cdr_record_reference, '|', 1) not in ('legacycore')
                group by SPLIT_STR(cdr_record_reference, '|', 2)";

   //prepare the query
   $stmt = $conn->prepare($query);

   //execute it
   $stmt->execute($data);

   //fetch the data
   $billingUnits = $stmt->fetchAll();

   return $billingUnits;
}

//Used in the allInclusive.php
function getSMSCs() {
   //open the DB object
   $conn = getPDOMasterCore();

   //prepare the query
   $stmt = $conn->prepare("SELECT DISTINCT route_collection_route_smsc_name
FROM core.core_route_collection_route
ORDER BY route_collection_route_smsc_name");

   //execute it
   $stmt->execute();

   //fetch the data
   $networks = $stmt->fetchAll();

   return $networks;
}

//networktrafficsummary.php
function getResellersOnly() {
   //open the DB object
   $conn = getPDOMasterCore();

   //prepare the query
   $stmt = $conn->prepare("SELECT a.account_name, a.account_status, s.service_name, s.service_id, s.account_id
FROM core.core_account a
INNER JOIN core.core_service s
ON a.account_id = s.account_id
WHERE s.service_id_reseller = s.service_id
ORDER BY a.account_name, s.service_name ASC");

   //execute it
   $stmt->execute();

   //fetch the data
   $clients = $stmt->fetchAll();

   return $clients;
}

//networktrafficsummary.php
function getSubsFromReseller($account_id) {
   //open the DB object
   $conn = getPDOMasterCore();

   //prepare the data
   $data = array(':account_id' => $account_id);

   //prepare the query
   $stmt = $conn->prepare("SELECT a.account_name, a.account_status, s.service_name, s.service_id
FROM core.core_account a
INNER JOIN core.core_service s
ON a.account_id = s.account_id
WHERE a.account_id = :account_id ");

   //execute it
   $stmt->execute($data);

   //fetch the data
   $clients = $stmt->fetchAll();

   $cle = '';
   foreach ($clients as $key => $value) {
      $cle .= $value['service_id'] . ' OR service_id:';
   }

   return $cle;
}

//networktrafficsummary.php
function getRouteCostingForReseller($service_id) {
   //open the DB object
   $conn = getPDOMasterCore();

   //prepare the data
   $data = array(':service_id' => $service_id);

   //prepare the query
   $stmt = $conn->prepare("SELECT route_billing, route_mccmnc, route_currency
FROM core.core_route
WHERE service_id = :service_id");

   //execute it
   $stmt->execute($data);

   //fetch the data
   $routeData = $stmt->fetchAll();

   return $routeData;
}

//networktrafficsummary.php
function getRouteCostingForResellerWithMCCMNC($service_id, $mccmnc) {
   //open the DB object
   $conn = getPDOMasterCore();

   //prepare the data
   $data = array(':service_id' => $service_id, ':mccmnc' => $mccmnc);

   //prepare the query
   $stmt = $conn->prepare("SELECT route_billing, route_currency
FROM core.core_route
WHERE service_id = :service_id AND route_mccmnc = :mccmnc");

   //execute it
   $stmt->execute($data);

   //fetch the data
   $routeData = $stmt->fetch();

   return $routeData;
}

//clienthome.php | resellerhome.php
function getResellerNetCost($service_id, $mcc) {
   //open the DB object
   $conn = getPDOMasterCore();

   //prepare the data
   $data = array(':service_id' => $service_id, ':mcc' => $mcc);

   //prepare the query
   $stmt = $conn->prepare("SELECT route_billing
FROM core.core_route
WHERE service_id = :service_id
AND route_mccmnc = :mcc");

   //execute it
   $stmt->execute($data);

   //fetch the data
   $routeData = $stmt->fetch();

   $cost = 0;
   if (isset($routeData)) {
      $cost = $routeData['route_billing'];
   }
   return $cost;
}

//allmo.php | apitraffic.php | apitrafficadmin.php | batchlist.php | batchreplies.php | bulksms.php | bulksms2.php | bulksmscont.php | bulksmscont2.php
//bulkdsmscontold.php | bulsmsold.php | campaign.php | contactlists.php | contactlistadd.php | delivery.php | networktraffic.php | networktrafficelastic.php
//networktrafficOld.php | newroutes.php | quicksms.php | servicepercent.php | servicepercentadmin.php | servicepercentadminnew.php | updateservice.php | viewservice.php
//viewserviceAdmin.php
function getServicesInAccount($account_id = null, $sId = null) {
   //open the DB object
   $conn = getPDOMasterCore();

   if ($account_id == 1 || $account_id == null) {
      //prepare the data
      $data = array();

      //prepare the query
      $stmt = $conn->prepare('SELECT a.account_name, a.account_status, 
                    s.service_name, s.service_id, s.service_id_reseller, s.service_login
                    FROM core.core_account a
                    LEFT JOIN core.core_service s USING (account_id)
                    ORDER BY a.account_name,s.service_name ASC');
   } elseif ($account_id == 650) {
      //prepare the data
      $data = array(':service_id' => $_SESSION['serviceId'], ':account_id' => $account_id);

      //prepare the query
      $stmt = $conn->prepare('SELECT a.account_name, a.account_status, 
                    s.service_name, s.service_id, s.service_id_reseller, s.service_login
                    FROM core.core_account a
                    JOIN core.core_service s USING (account_id)
                    WHERE a.account_id = :account_id
                    AND service_id = :service_id
                    ORDER BY a.account_name,s.service_name ASC');
   } else {
      //prepare the data
      $data = array(':account_id' => $account_id);

      //prepare the query
      $stmt = $conn->prepare('SELECT a.account_name, a.account_status, 
                    s.service_name, s.service_id, s.service_id_reseller, s.service_login
                    FROM core.core_account a
                    JOIN core.core_service s USING (account_id)
                    WHERE a.account_id = :account_id
                    ORDER BY a.account_name,s.service_name ASC');
   }

   //execute it
   $stmt->execute($data);

   //fetch the data
   $clients = $stmt->fetchAll();
   return $clients;
}

//networktrafficpostpaid.php
function getServicesInAccountPostPaid($account_id = null) {
   //open the DB object
   $conn = getPDOMasterCore();

   if ($account_id == 1 || $account_id == null) {
      //prepare the data
      $data = array();

      //prepare the query
      $stmt = $conn->prepare('SELECT a.account_name, a.account_status, s.service_name, s.service_id
                    FROM core.core_account a
                    INNER JOIN core.core_service s
                    ON a.account_id = s.account_id
                    INNER JOIN core.core_service_credit c
                    ON s.service_id = c.service_id
                    WHERE c.service_credit_billing_type  = "POSTPAID"
                    ORDER BY a.account_name,s.service_name ASC');
   } else {
      //prepare the data
      $data = array(':account_id' => $account_id);

      //prepare the query
      $stmt = $conn->prepare('SELECT a.account_name, a.account_status, s.service_name, s.service_id
                    FROM core.core_account a
                    INNER JOIN core.core_service s
                    ON a.account_id = s.account_id
                    INNER JOIN core.core_service_credit c
                    ON s.service_id = c.service_id
                    WHERE c.service_credit_billing_type  = "POSTPAID"
                    AND a.account_id = :account_id
                    ORDER BY a.account_name,s.service_name ASC');
   }
   //execute it
   $stmt->execute($data);

   //fetch the data
   $clients = $stmt->fetchAll();
   return $clients;
}

//addroute.php | addrouteadmin.php | adduser.php | newroutes.php | updatecredit.php | updatecredits.php
function getServicesInAccountWithOutRid($account_id) {
   //open the DB object
   $conn = getPDOMasterCore();

   //prepare the data
   $data = array(':account_id' => $account_id);

   //prepare the query
   $stmt = $conn->prepare('SELECT a.account_name, a.account_status, s.service_name, s.service_id, s.service_id_reseller
                FROM core.core_account a
                INNER JOIN core.core_service s
                ON a.account_id = s.account_id
                WHERE a.account_id = :account_id
                AND s.service_id <> s.service_id_reseller
                ORDER BY a.account_name,s.service_name ASC');

   //execute it
   $stmt->execute($data);

   //fetch the data
   $clients = $stmt->fetchAll();
   return $clients;
}

//addroute.php | addrouteadmin.php | adduser.php | updatecredit.php | updatecredits.php
function getClients() {
   //open the DB object
   $conn = getPDOMasterCore();

   //prepare the query
   $stmt = $conn->prepare('SELECT a.account_name, a.account_status, s.service_name, s.service_id, s.service_id_reseller
                FROM core.core_account a
                INNER JOIN core.core_service s
                ON a.account_id = s.account_id
                ORDER BY a.account_name, s.service_name ASC');

   //execute it
   $stmt->execute();

   //fetch the data
   $clients = $stmt->fetchAll();
   return $clients;
}

//createservice.php
function getServiceID($service_name) {
   //open the DB object
   $conn = getPDOMasterCore();

   //prepare the data
   $data = array(':service_name' => $service_name);

   //prepare the query
   $stmt = $conn->prepare("SELECT service_id
FROM core.core_service
WHERE service_name
LIKE :service_name ");

   //execute it
   $stmt->execute($data);

   //fetch the data
   $serviceID = $stmt->fetchAll();
   return $serviceID;
}

//*****************************************
//updateservice.php | viewservice.php | viewserviceAdmin.php
function getClient($service_id) {
   //open the DB object
   $conn = getPDOMasterCore();

   //prepare the data
   $data = array(':service_id' => $service_id);

   //prepare the query
   $stmt = $conn->prepare("SELECT a.account_name, s.service_status, s.service_name, s.service_id
FROM core.core_account a
INNER JOIN core.core_service s
ON a.account_id = s.account_id
WHERE s.service_id = :service_id ");

   //execute it
   $stmt->execute($data);

   //fetch the data
   $client = $stmt->fetchAll();
   return $client;
}

//pause.php
function getBatchReferenceAndCampId($batch_id) {
   //open the DB object
   $conn = getPDOMasterCore();

   //prepare the data
   $data = array(':batch_id' => $batch_id);

   //prepare the query
   $stmt = $conn->prepare("SELECT batch_reference, campaign_id
FROM core.campaign_batch
WHERE batch_id = :batch_id ");

   //set the fetch mode
   $stmt->setFetchMode(PDO::FETCH_ASSOC);

   //execute it
   $stmt->execute($data);

   //fetch the data
   $bName = $stmt->fetchAll();

   return $bName;
}

//home.php | resellerhome.php
function getServiceName($service_id) {
   //open the DB object
   $conn = getPDOMasterCore();

   //prepare the data
   $data = array(':service_id' => $service_id);

   //prepare the query
   $stmt = $conn->prepare("SELECT service_name
      FROM core.core_service
      WHERE service_id = :service_id ");

   //set the fetch mode
   $stmt->setFetchMode(PDO::FETCH_ASSOC);

   //execute it
   $stmt->execute($data);

   //fetch the data
   $serviceName = $stmt->fetch();

   if(isset($serviceName['service_name']) && $serviceName['service_name'] != "")
   {
      return $serviceName['service_name'];
   }
   else
   {
      return 'n/a';
   }
}

//clienthome.php | resellerhome.php
function getServiceNameAndCurrency($service_id, $mcc) {
   //open the DB object
   $conn = getPDOMasterCore();

   //prepare the data
   $data = array(':service_id' => $service_id, ':mcc' => $mcc);

   //prepare the query
   $stmt = $conn->prepare("SELECT s.service_name, r.route_currency
FROM core.core_service s
INNER JOIN core.core_route r
ON s.service_id = r.service_id
WHERE s.service_id = :service_id
AND r.route_mccmnc = :mcc
AND r.route_status = 'ENABLED' ");

   //set the fetch mode
   $stmt->setFetchMode(PDO::FETCH_ASSOC);

   //execute it
   $stmt->execute($data);

   //fetch the data
   $servArr = $stmt->fetchAll();
   return $servArr;
}

//cinfirmcredit.php | confirmcredits.php
function getAccountId($service_id) {
   //open the DB object
   $conn = getPDOMasterCore();

   //prepare the data
   $data = array(':service_id' => $service_id);

   //prepare the query
   $stmt = $conn->prepare("SELECT account_id
FROM core.core_service
WHERE service_id = :service_id ");

   //set the fetch mode
   $stmt->setFetchMode(PDO::FETCH_ASSOC);

   //execute it
   $stmt->execute($data);

   //fetch the data
   $row = $stmt->fetch();
   return $row['account_id'];
}

//**********************************
//confirmcredit.php | confirmcredit.php
function createPayment($service_id, $user_id, $payment_value, $payment_description, $payment_previous_balance) {
   //open the DB object
   $conn = getPDOMasterCore();

   //prepare the data
   $data = array(':service_id' => $service_id, ':user_id' => $user_id, ':payment_value' => $payment_value,
       ':payment_description' => $payment_description, ':payment_previous_balance' => $payment_previous_balance);

   //prepare the query
   $stmt = $conn->prepare("INSERT INTO core.core_service_payment(`service_id`, `user_id`, `payment_value`, `payment_description`, `payment_previous_balance`)
        VALUES(:service_id, :user_id, :payment_value, :payment_description, :payment_previous_balance)");

   //execute it
   $stmt->execute($data);
   return true;
}

//search.php | searchMo.php | searchMoAdmin.php
function getAllServicesInReseller($service_id_reseller) {
   //open the DB object
   $conn = getPDOMasterCore();

   //prepare the data
   $data = array(':service_id_reseller' => $service_id_reseller);

   //prepare the query
   $stmt = $conn->prepare('SELECT service_id
                FROM core.core_service
                WHERE service_id_reseller = :service_id_reseller');

   //set the fetch mode
   $stmt->setFetchMode(PDO::FETCH_ASSOC);

   //execute it
   $stmt->execute($data);

   //fetch the data
   $serNames = $stmt->fetchAll();

   $ids = '';
   foreach ($serNames as $key => $value) {
      $ids .= 'service_id:' . $value['service_id'] . ' OR ';
   }
   $ids = substr($ids, 0, -4);
   return $ids;
}

function getAllServicesInResellerNotString($service_id_reseller) {
   //open the DB object
   $conn = getPDOMasterCore();

   //prepare the data
   $data = array(':service_id_reseller' => $service_id_reseller);

   //prepare the query
   $stmt = $conn->prepare('SELECT service_id
                FROM core.core_service
                WHERE service_id_reseller = :service_id_reseller');

   //set the fetch mode
   $stmt->setFetchMode(PDO::FETCH_ASSOC);

   //execute it
   $stmt->execute($data);

   //fetch the data
   $services = $stmt->fetchAll();

   return $services;
}

//confirmcredit.php | confirmcredits.php | updatecredit.php | updatecredits.php
function getFundsOnly($service_id) {
   //open the DB object
   $conn = getPDOMasterCore();

   //prepare the data
   $data = array(':service_id' => $service_id);

   //prepare the query
   $stmt = $conn->prepare('SELECT service_credit_available
                FROM core.core_service_credit
                WHERE service_id = :service_id');

   //set the fetch mode
   $stmt->setFetchMode(PDO::FETCH_ASSOC);

   //execute it
   $stmt->execute($data);

   //fetch the data
   $funds = $stmt->fetch();
   return $funds['service_credit_available'];
}

//updateservice.php | viewservice.php | viewserviceAdmin.php
function getFunds($service_id) {
   //open the DB object
   $conn = getPDOMasterCore();

   //prepare the data
   $data = array(':service_id' => $service_id);

   //prepare the query
   $stmt = $conn->prepare('SELECT *
                FROM core.core_service_credit
                WHERE service_id = :service_id');

   //set the fetch mode
   $stmt->setFetchMode(PDO::FETCH_ASSOC);

   //execute it
   $stmt->execute($data);

   //fetch the data
   $funds = $stmt->fetchAll();

   $fundsList = array();
   foreach ($funds as $key => $value) {
      $fundsList = [$value['service_credit_billing_type'], $value['service_credit_available']];
   }
   return $fundsList;
}

//Used in billing.php
function getMCCFromNet($prefix_network_name) {
   //open the DB object
   $conn = getPDOMasterCore();

   //prepare the data
   $data = array(':prefix_network_name' => $prefix_network_name);

   //prepare the query
   $stmt = $conn->prepare('SELECT DISTINCT prefix_mccmnc
                FROM core.core_prefix WHERE prefix_network_name = :prefix_network_name');

   //set the fetch mode
   $stmt->setFetchMode(PDO::FETCH_ASSOC);

   //execute it
   $stmt->execute($data);

   $nN = "";

   //fetch the data
   $netName = $stmt->fetchAll();
   foreach ($netName as $key => $value) {
      $nN = $value['prefix_mccmnc'];
   }
   return $nN;
}

//Used in the allInclusive.php
function getMCCFromCountry($prefix_country_name) {
   //open the DB object
   $conn = getPDOMasterCore();

   //prepare the data
   $data = array(':prefix_country_name' => $prefix_country_name);

   //prepare the query
   $stmt = $conn->prepare('SELECT DISTINCT prefix_mccmnc
                FROM core.core_prefix WHERE prefix_country_name = :prefix_country_name');

   //set the fetch mode
   $stmt->setFetchMode(PDO::FETCH_ASSOC);

   //execute it
   $stmt->execute($data);

   //fetch the data
   $netName = $stmt->fetchAll();
   return $netName['prefix_mccmnc'];
}

//routecosting.php
function getCdrType($type = null) {
   //open the DB object
   $conn = getPDOMasterCore();

   if ($type == null) {
      //prepare the data
      $data = array();

      //prepare the query
      $stmt = $conn->prepare('SELECT DISTINCT cdr_type
                    FROM reporting.cdr');
   } else {
      //prepare the data
      $data = array(':type' => $type);

      //prepare the query
      $stmt = $conn->prepare('SELECT cdr_type
                    FROM reporting.cdr
                    WHERE cdr_type LIKE "%:type%"');
   }
   //set the fetch mode
   $stmt->setFetchMode(PDO::FETCH_ASSOC);

   //execute it
   $stmt->execute($data);

   //fetch the data
   $typeNames = $stmt->fetchAll();
   return $typeNames;
}

//Used in the allInclusive.php
function getConnetManagers($where_id_is = null) {
   //open the DB object
   $conn = getPDOMasterCore();

   if ($where_id_is != null) {
      //prepare the data
      $data = array(':where_id_is' => $where_id_is);

      //prepare the query
      $stmt = $conn->prepare("SELECT user_id
FROM core.core_user
WHERE user_username LIKE :where_id_is ");

      //set the fetch mode
      $stmt->setFetchMode(PDO::FETCH_ASSOC);

      //execute it
      $stmt->execute($data);

      //fetch the data
      $manager = $stmt->fetch();
      $managers = $manager['user_id'];
   } else {
      //prepare the query
      $stmt = $conn->prepare("SELECT *
FROM core.core_user
WHERE user_id IN (3, 15, 745, 563, 555, 92, 1179, 225, 40, 521, 479)
ORDER BY 2");

      //set the fetch mode
      $stmt->setFetchMode(PDO::FETCH_ASSOC);

      //execute it
      $stmt->execute();

      //fetch the data
      $managers = $stmt->fetchAll();
   }

   return $managers;
}

//Used in the allInclusive.php
function getAccounts($account_name = null) {
   //open the DB object
   $conn = getPDOMasterCore();

   if ($account_name != null) {
      //prepare the data
      $data = array(':account_name' => $account_name);

      //prepare the query
      $stmt = $conn->prepare("SELECT account_id
FROM core.core_account
WHERE account_name LIKE :account_name ");

      //set the fetch mode
      $stmt->setFetchMode(PDO::FETCH_ASSOC);

      //execute it
      $stmt->execute($data);

      //fetch the data
      $account = $stmt->fetch();
      $accounts = $account['account_id'];
   } else {
      //prepare the query
      $stmt = $conn->prepare("SELECT account_id, account_name
FROM core.core_account
ORDER BY 2");

      //set the fetch mode
      $stmt->setFetchMode(PDO::FETCH_ASSOC);

      //execute it
      $stmt->execute();

      //fetch the data
      $accounts = $stmt->fetchAll();
   }

   return $accounts;
}

//routecosting.php //THIS QUERY NEEDS CLEANING UP, THIS FUNCTION IS UNREADABLE, WRITE SOME COMMENTS
function getRouteCost($s = null) {
   //prepare the data
   $data = array();

   if (($s == null) || ($s[0] == 'Show All' && $s[1] == 'Show All' && $s[2] == 'Show All' && $s[3] == 'Show All')) {
      $qry = "SELECT cdr_provider, cdr_cost_per_unit, cdr_effective_date, SPLIT_STR(cdr_provider, '|', 4) as mccmnc, cdr_comments, cdr_type, cdr_id
from reporting.cdr c
where (cdr_type <> 'Not Working' or cdr_type is null)
and cdr_provider like 'core|%'
and cdr_id = (
select cdr_id
from reporting.cdr
where SPLIT_STR(cdr_provider, '|', 4) = SPLIT_STR(c.cdr_provider, '|', 4)
and SPLIT_STR(cdr_provider, '|', 3) = SPLIT_STR(c.cdr_provider, '|', 3)
and cdr_provider like 'core|%'
order by cdr_effective_date desc, cdr_cost_per_unit desc
limit 1
)";
   } else {
      $s0 = 0;
      foreach ($s as $key => $value) {
         //echo '<br>value='.$value;
         if ($value != 'Show All') {
            if ($s0 == 0) {
               if ($key == 0) {
                  $t = ' WHERE SPLIT_STR(cdr_provider,"|",3) = "' . ($value) . '" ';
                  //$t = ' WHERE cdr_provider LIKE "%'.($value).'_smsc%" ';
                  $s0++;
                  # code...
               } elseif ($key == 1) {
                  //echo '<br>SELECTING='.$value;
                  $nextVal = getMCCFromNet(lcfirst($value));
                  //echo '<br>RETURNING='.$nextVal;

                  $t = ' WHERE SPLIT_STR(cdr_provider,"|",4) = "' . $nextVal . '" ';
                  //$t = ' WHERE cdr_provider LIKE "%'.($value).'_smsc%" ';
                  $s0++;
               } elseif ($key == 2) {
                  //$nextVal = substr(getMCCFromCountry(lcfirst($value)), 0, (strlen(getMCCFromCountry(lcfirst($value))) - 2));
                  $nextVal = substr(getMCCFromCountry(lcfirst($value)), 0, 3);
                  //$t = ' WHERE SPLIT_STR(cdr_provider,"|",4) = "'.$nextVal.'" ';
                  $t = ' WHERE cdr_provider LIKE "%' . $nextVal . '%" ';
                  $s0++;
               } elseif ($key == 3) {
                  //$nextVal = substr(getMCCFromCountry(lcfirst($value)), 0, (strlen(getMCCFromCountry(lcfirst($value))) - 2));
                  //$nextVal = substr(getMCCFromCountry(lcfirst($value)), 0, 3);
                  $t = ' WHERE cdr_type LIKE "%' . $value . '%" ';
                  $s0++;
               }
            } else {
               if ($key == 1) {
                  $nextVal = getMCCFromNet(lcfirst($value));
               } elseif ($key == 2) {
                  $nextVal = getMCCFromCountry(lcfirst($value));
                  $nextVal = substr($nextVal, 0, 3);
               }

               $t = ' WHERE SPLIT_STR(cdr_provider,"|",4) = "' . $nextVal . '" ';
               $t .= 'AND cdr_provider LIKE "%' . $nextVal . '%" ';

               if ($key == 3) {
                  $t .= 'AND cdr_type LIKE "%' . $value . '%" ';
               }
            }
         }
      }

      $qry = "SELECT cdr_provider, cdr_cost_per_unit, cdr_effective_date, SPLIT_STR(cdr_provider, '|', 4) as mccmnc, cdr_comments, cdr_type, cdr_id
from reporting.cdr c
" . $t . "
and cdr_provider like 'core|%'
and (cdr_type <> 'Not Working' or cdr_type is null)
and cdr_id = (
select cdr_id
from reporting.cdr
where SPLIT_STR(cdr_provider, '|', 4) = SPLIT_STR(c.cdr_provider, '|', 4)
and SPLIT_STR(cdr_provider, '|', 3) = SPLIT_STR(c.cdr_provider, '|', 3)
and cdr_provider like 'core|%'
order by cdr_effective_date desc, cdr_cost_per_unit desc
limit 1
)";
   }


   //open the DB object
   $conn = getPDOMasterCore();

   //prepare the query
   $stmt = $conn->prepare($qry);

   //set the fetch mode
   $stmt->setFetchMode(PDO::FETCH_ASSOC);

   //execute it
   $stmt->execute($data);

   //fetch the data
   $routeCost = $stmt->fetchAll();
   return $routeCost;
}

//**********************************
//clienthome.php | resellerhome.php
function getNetFromMCC($prefix_mccmnc) {
   //open the DB object
   $conn = getPDOMasterCore();

   //prepare the data
   $data = array(':prefix_mccmnc' => $prefix_mccmnc);

   //prepare the query
   $stmt = $conn->prepare('SELECT DISTINCT prefix_network_name
                FROM core.core_prefix
                WHERE prefix_mccmnc = :prefix_mccmnc');

   //set the fetch mode
   $stmt->setFetchMode(PDO::FETCH_ASSOC);

   //execute it
   $stmt->execute($data);

   //fetch the data
   $netName = $stmt->fetch();
   return $netName['prefix_network_name'];
}

//clienthome.php | resellerhome.php
function getCountryFromMCC($prefix_mccmnc) {
   //open the DB object
   $conn = getPDOMasterCore();

   //prepare the data
   $data = array(':prefix_mccmnc' => $prefix_mccmnc);

   //prepare the query
   $stmt = $conn->prepare('SELECT DISTINCT prefix_country_name
                FROM core.core_prefix
                WHERE prefix_mccmnc = :prefix_mccmnc');

   //set the fetch mode
   $stmt->setFetchMode(PDO::FETCH_ASSOC);

   //execute it
   $stmt->execute($data);

   //fetch the data
   $country = $stmt->fetch();
   return $country['prefix_country_name'];
}

//**********************************
//updateservice.php | viewservice.php | viewserviceAdmin.php
function getServiceData($service_id) {
   //open the DB object
   $conn = getPDOMasterCore();

   //prepare the data
   $data = array(':service_id' => $service_id);

   //prepare the query
   $stmt = $conn->prepare("SELECT account_name, service_name, service_login, user_fullname, service_status, sc.service_credit_available, sc.service_credit_billing_type, service_notification_threshold, service_notification_email, service_notification_sms,
 d.endpoint_dlr_id as 'DLR_ID_LEGACY', d.endpoint_dlr_address as 'DLR_ADDRESS_LEGACY', d.endpoint_dlr_status as 'DLR_STATUS_LEGACY',
 m.endpoint_mosms_id as 'MO_ID_LEGACY', m.endpoint_mosms_address as 'MO_ADDRESS_LEGACY', m.endpoint_mosms_status as 'MO_STATUS_LEGACY',
 if(ds.endpoint_dlr_status is null, 'DISABLED', ds.endpoint_dlr_status) as 'SMPP', ds.endpoint_dlr_id as 'DLR_ID_SMPP', ms.endpoint_mosms_id as 'MO_ID_SMPP', service_meta, service_login
FROM core.core_service s
INNER JOIN core.core_account a
ON s.account_id = a.account_id
INNER JOIN core_service_credit sc
ON s.service_id = sc.service_id
LEFT OUTER JOIN core.core_user u
ON s.service_manager_user_id = u.user_id
LEFT OUTER JOIN core.push_endpoint_dlr d
ON s.service_id = d.service_id
AND d.endpoint_dlr_method = 'LEGACY'
LEFT OUTER JOIN core.push_endpoint_mosms m
ON s.service_id = m.service_id
AND m.endpoint_mosms_method = 'LEGACY'
LEFT OUTER JOIN core.push_endpoint_dlr ds
ON s.service_id = ds.service_id
and ds.endpoint_dlr_method = 'SMPP'
LEFT OUTER JOIN core.push_endpoint_mosms ms
ON s.service_id = ms.service_id
AND ms.endpoint_mosms_method = 'SMPP'
WHERE s.service_id = :service_id ");

   //set the fetch mode
   $stmt->setFetchMode(PDO::FETCH_ASSOC);

   //execute it
   $stmt->execute($data);

   //fetch the data
   $serviceData = $stmt->fetchAll();

   return $serviceData;
}

//updateservice.php
function deleteNotifications($service_id) {
   //open the DB object
   $conn = getPDOMasterCore();

   //prepare the data
   $data = array(':service_id' => $service_id);

   //prepare the query
   $stmt = $conn->prepare("UPDATE core.core_service_credit
SET service_notification_threshold = null, service_notification_email = null, service_notification_sms = null, service_notification_lastsend = null
WHERE service_id = :service_id ");

   return $stmt->execute($data);
}

//confirmcredits.php
function deleteNotificationData($service_id) {
   //open the DB object
   $conn = getPDOMasterCore();

   //prepare the data
   $data = array(':service_id' => $service_id);

   //prepare the query
   $stmt = $conn->prepare("UPDATE core.core_service_credit
      SET service_notification_lastsend = null
      WHERE service_id = :service_id ");

   return $stmt->execute($data);
}

//statistics.php
function getAllStatistics($service_id) {
   //open the DB object
   $conn = getPDOReportingCore();

   //prepare the data
   $data = array(':service_id' => $service_id);

   //prepare the query
   $stmt = $conn->prepare("SELECT 'mt' as `type`, count(mtsms_id) as 'SENT', DATE(timestamp) as `timestamp`, sum(billing_units) as 'UNITS',
 SUM(If ((dlr & 1 = 1), 1, 0)) as 'DELIVERED',
 SUM(If ((dlr & 2 = 2), 1, 0)) as 'FAILED',
 mccmnc, smsc, sum(rdnc) as 'rdnc', prefix_network_name
FROM core.core_mtsms s
INNER JOIN core.core_prefix p
ON s.mccmnc = p.prefix_mccmnc
WHERE DATE(`timestamp`) >= DATE(DATE_SUB(NOW(), INTERVAL 2 DAY)) AND service_id = :service_id
group by DATE(timestamp), mccmnc, smsc
union
SELECT 'mo' as `type`, count(mosms_id) as 'SENT', DATE(timestamp) as `timestamp`, 1 as `UNITS`,
 0 as 'DELIVERED', 0 as 'FAILED', replace(SPLIT_STR(meta, '&', 2), 'mccmnc=', '') as mccmnc, replace(SPLIT_STR(meta, '&', 1), 'smsc=', '') as smsc, 0 as rdnc, '' as prefix_network_name
FROM core.core_mosms
WHERE DATE(`timestamp`) >= DATE(DATE_SUB(NOW(), INTERVAL 2 DAY)) AND service_id = " . $cl . "
group by `type`, DATE(timestamp), replace(SPLIT_STR(meta, '&', 2), 'mccmnc=', ''), replace(SPLIT_STR(meta, '&', 1), 'smsc=', '')
order by DATE(timestamp), mccmnc, smsc");

   //set the fetch mode
   $stmt->setFetchMode(PDO::FETCH_ASSOC);

   //execute it
   $stmt->execute($data);

   //fetch the data
   $delMoData = $stmt->fetchAll();
   return $delMoData;
}

//networkpercent.php
function getAllNetworks() {
   //open the DB object
   $conn = getPDOReportingCore();

   //prepare the query
   $stmt = $conn->prepare("SELECT 'mt' as `type`, count(mtsms_id) as 'SENT', DATE(timestamp) as `timestamp`, sum(billing_units) as 'UNITS',
 SUM(If ((dlr & 1 = 1), 1, 0)) as 'DELIVERED',
 SUM(If ((dlr & 2 = 2), 1, 0)) as 'FAILED', smsc
FROM core.core_mtsms s
WHERE DATE(`timestamp`) >= DATE(DATE_SUB(NOW(), INTERVAL 2 DAY))
and mccmnc = 65501
group by DATE(timestamp), mccmnc, smsc");

   //set the fetch mode
   $stmt->setFetchMode(PDO::FETCH_ASSOC);

   //execute it
   $stmt->execute();

   //fetch the data
   $delMoData = $stmt->fetchAll();
   return $delMoData;
}

//providersummary.php
function getProviderSummary($start_date, $end_date) {
   //open the DB object
   $conn = getPDOReportingCore();

   //prepare the data
   $data = array(':start_date' => $start_date, ':end_date' => $end_date,);

   //prepare the query
   $stmt = $conn->prepare("SELECT `type`, prefix_country_name, concat(prefix_network_name, ' (', `mccmnc`, ')') as `network`, `smsc`, units, `cost_price`
FROM(
SELECT SPLIT_STR(cdr_record_reference, '|', 1) as 'type', SPLIT_STR(cdr_provider, '|', 3) as `smsc`, SPLIT_STR(cdr_provider, '|', 4) as `mccmnc`, SUM(cdr_record_units) as units, (cdr_cost_per_unit/10000) as 'cost_price'
FROM reporting.cdr
LEFT JOIN reporting.cdr_record_archive using (cdr_id)
WHERE DATE(cdr_record_date) >= :start_date and DATE(cdr_record_date) <= :end_date
AND SPLIT_STR(cdr_record_reference, '|', 1) not in ('rdnc', 'legacycore')
GROUP BY SPLIT_STR(cdr_record_reference, '|', 1), SPLIT_STR(cdr_provider, '|', 3), SPLIT_STR(cdr_record_reference, '|', 1), SPLIT_STR(cdr_provider, '|', 4)
) AS CDR
LEFT JOIN
(SELECT prefix_mccmnc, prefix_network_name, prefix_country_name FROM core.core_prefix GROUP BY prefix_mccmnc) AS core_prefix
ON prefix_mccmnc = mccmnc");

   //set the fetch mode
   $stmt->setFetchMode(PDO::FETCH_ASSOC);

   //execute it
   $stmt->execute($data);

   //fetch the data
   $provSum = $stmt->fetchAll();
   return $provSum;
}

//networkpercent.php
function getMOsForElastics($start_date, $end_date) {
   //open the DB object
   $conn = getPDOReportingCore();

   //prepare the data
   $data = array(':start_date' => $start_date, ':end_date' => $end_date);

   //prepare the query
   $stmt = $conn->prepare("SELECT 'mo' as type, count(mosms_id) as id, DATE(timestamp) as `timestamp`, 1 as billing_units, 0 as dlr, replace(SPLIT_STR(meta, '&', 2), 'mccmnc=', '') as mccmnc, replace(SPLIT_STR(meta, '&', 1), 'smsc=', '') as smsc, '' as rdnc
FROM core.core_mosms
WHERE DATE(`timestamp`) >= :start_date
AND DATE(`timestamp`) <= :end_date
group by mccmnc, smsc, DATE(timestamp)");

   //set the fetch mode
   $stmt->setFetchMode(PDO::FETCH_ASSOC);

   //execute it
   $stmt->execute($data);

   //fetch the data
   $moData = $stmt->fetchAll();
   return $moData;
}

//servicepercentadminnew.php
function getMOsForElasticsSId($service_id, $start_date, $end_date) {
   //open the DB object
   $conn = getPDOReportingCore();

   //prepare the data
   $data = array(':service_id' => $service_id, ':start_date' => $start_date, ':end_date' => $end_date);

   //prepare the query
   $stmt = $conn->prepare("SELECT 'mo' as type, count(mosms_id) as id, DATE(timestamp) as `timestamp`, 1 as billing_units, 0 as dlr, replace(SPLIT_STR(meta, '&', 2), 'mccmnc=', '') as mccmnc, replace(SPLIT_STR(meta, '&', 1), 'smsc=', '') as smsc, '' as rdnc
FROM core.core_mosms
WHERE service_id = :service_id
AND DATE(`timestamp`) >= :start_date
AND DATE(`timestamp`) <= :end_date
group by mccmnc, smsc, DATE(timestamp)");

   //set the fetch mode
   $stmt->setFetchMode(PDO::FETCH_ASSOC);

   //execute it
   $stmt->execute($data);

   //fetch the data
   $moData = $stmt->fetchAll();
   return $moData;
}

//updateservice.php | viewservice.php | viewserviceAdmin.php
function getDefaultRouteData($service_id) {
   //open the DB object
   $conn = getPDOMasterCore();

   //prepare the data
   $data = array(':service_id' => $service_id);

   //prepare the query
   $stmt = $conn->prepare("SELECT COUNTRY, NETWORK, `STATUS`, RATE, COLLECTION, Route.route_collection_id, MCCMNC, route_id,
 replace(rcrd.route_collection_route_smsc_name, '_smsc', '') as 'DEFAULT',
 replace(rcrp.route_collection_route_smsc_name, '_smsc', '') as 'PORTED',
 route_currency
FROM
(SELECT p.prefix_country_name as 'COUNTRY',
 concat(p.prefix_network_name, ' (', r.route_mccmnc, ')') as 'NETWORK',
 r.route_status as 'STATUS',
 r.route_id as 'route_id',
 r.route_billing as 'RATE',
 rc.route_collection_name as 'COLLECTION',
 rc.route_collection_id,
 r.route_mccmnc as 'MCCMNC',
 r.route_currency
FROM core.core_route r
LEFT OUTER JOIN core.core_prefix p
ON r.route_mccmnc = p.prefix_mccmnc
INNER JOIN core.core_route_collection rc
ON r.route_collection_id = rc.route_collection_id
WHERE r.route_msg_type = 'MTSMS' AND r.service_id = :service_id
GROUP BY p.prefix_country_name,
 concat(p.prefix_network_name, ' (', r.route_mccmnc, ')'),
 r.route_status,
 r.route_billing,
 rc.route_collection_name
) as Route
LEFT OUTER JOIN core.core_route_collection_route rcrp
ON Route.route_collection_id = rcrp.route_collection_id
AND concat(MCCMNC, 'p') = rcrp.route_collection_route_match_mccmnc
AND rcrp.route_collection_route_match_priority = (SELECT min(route_collection_route_match_priority)
FROM core.core_route_collection_route
WHERE route_collection_id = Route.route_collection_id
AND route_collection_route_match_mccmnc = concat(MCCMNC, 'p')
LIMIT 1)
LEFT OUTER JOIN core.core_route_collection_route rcrd
ON Route.route_collection_id = rcrd.route_collection_id
AND MCCMNC = rcrd.route_collection_route_match_mccmnc
AND rcrd.route_collection_route_match_priority = (SELECT min(route_collection_route_match_priority)
FROM core.core_route_collection_route q
WHERE q.route_collection_id = Route.route_collection_id
AND q.route_collection_route_match_mccmnc = MCCMNC
LIMIT 1)
order by COUNTRY, MCCMNC;
");

   //set the fetch mode
   $stmt->setFetchMode(PDO::FETCH_ASSOC);

   //execute it
   $stmt->execute($data);

   //fetch the data
   $defaultRoutes = $stmt->fetchAll();
   return $defaultRoutes;
}

//addroute.php | addrouteadmin.php | newresellerroutes.php | newroutes.php
function getResellerDefaultRouteData($service_id, $new_service = null) {
   //open the DB object
   $conn = getPDOMasterCore();

   if (!isset($new_service)) {
      $new_service = "";
   }
   //prepare the data
   $data = array(':service_id' => $service_id, ':new_service' => $new_service);

   //prepare the query
   $stmt = $conn->prepare("SELECT COUNTRY, NETWORK, `STATUS`, RATE, COLLECTION, Route.route_collection_id, MCCMNC, route_id
, replace(rcrd.route_collection_route_smsc_name, '_smsc', '') as 'DEFAULT'
, replace(rcrp.route_collection_route_smsc_name, '_smsc', '') as 'PORTED',
 `provider_id`, `service_id`, `route_msg_type`, `route_description`, `route_display_name`, `route_code`,
 `route_match_regex`, `route_match_priority`, `route_billing`
FROM
(SELECT p.prefix_country_name as 'COUNTRY',
 concat(p.prefix_network_name, ' (', r.route_mccmnc, ')') as 'NETWORK',
 r.route_status as 'STATUS',
 r.route_id as 'route_id',
 format(r.route_billing/10000, 4) as 'RATE',
 rc.route_collection_name as 'COLLECTION',
 rc.route_collection_id,
 r.route_mccmnc as 'MCCMNC',
 `provider_id`, `service_id`, `route_msg_type`, `route_description`, `route_display_name`, `route_code`, `route_match_regex`, `route_match_priority`,
 `route_billing`
FROM core.core_route r
LEFT OUTER JOIN core.core_prefix p
ON r.route_mccmnc = p.prefix_mccmnc
INNER JOIN core.core_route_collection rc
ON r.route_collection_id = rc.route_collection_id
WHERE r.route_msg_type = 'MTSMS' AND r.service_id = :service_id
and route_mccmnc not in (select route_mccmnc FROM core.core_route where service_id = :new_service)
GROUP BY p.prefix_country_name,
 concat(p.prefix_network_name, ' (', r.route_mccmnc, ')'),
 r.route_status,
 format(r.route_billing/10000, 4),
 rc.route_collection_name
) as Route
LEFT OUTER JOIN core.core_route_collection_route rcrp
ON Route.route_collection_id = rcrp.route_collection_id
AND concat(MCCMNC, 'p') = rcrp.route_collection_route_match_mccmnc
AND rcrp.route_collection_route_match_priority = (SELECT min(route_collection_route_match_priority)
FROM core.core_route_collection_route
WHERE route_collection_id = Route.route_collection_id
AND route_collection_route_match_mccmnc = concat(MCCMNC, 'p')
LIMIT 1)
LEFT OUTER JOIN core.core_route_collection_route rcrd
ON Route.route_collection_id = rcrd.route_collection_id
AND MCCMNC = rcrd.route_collection_route_match_mccmnc
AND rcrd.route_collection_route_match_priority = (SELECT min(route_collection_route_match_priority)
FROM core.core_route_collection_route q
WHERE q.route_collection_id = Route.route_collection_id
AND q.route_collection_route_match_mccmnc = MCCMNC
LIMIT 1)");

   //set the fetch mode
   $stmt->setFetchMode(PDO::FETCH_ASSOC);

   //execute it
   $stmt->execute($data);

   //fetch the data
   $defaultRoutes = $stmt->fetchAll();
   return $defaultRoutes;
}

//resellerhome.php
function getSubAccounts($account_id) {
   //open the DB object
   $conn = getPDOMasterCore();

   //prepare the data
   $data = array(':account_id' => $account_id);

   //prepare the query
   $stmt = $conn->prepare("SELECT service_id
FROM core.core_service
WHERE account_id = :account_id");

   //set the fetch mode
   $stmt->setFetchMode(PDO::FETCH_ASSOC);

   //execute it
   $stmt->execute($data);

   //fetch the data
   $subs = $stmt->fetchAll();

   $s = '';
   foreach ($subs as $key => $value) {
      $s .= $value['service_id'] . ',';
   }
   $s = substr($s, 0, -1);
   return $s;
}

//delivery.php
function getDelivery($service_id) {
   //open the DB object
   $conn = getPDOReportingCore();

   //prepare the data
   $data = array(':service_id' => $service_id);

   //prepare the query
   $stmt = $conn->prepare("SELECT daym, prefix_country_name, prefix_network_name, mccmnc, units, sent, delivered, delper, failed, failper, MOTotal
FROM(
select date(`timestamp`) as 'daym',
 substring_index(SUBSTRING_INDEX(meta, 'mccmnc=', -1), '&', 1) as mccmnc,
 SUM(billing_units) as `units`,
 COUNT(mtsms_id) as `sent`,
 SUM(If ((dlr & 1 = 1), 1, 0)) as 'delivered', #1
FORMAT((SUM(If ((dlr & 1 = 1), 1, 0)) * 100) / COUNT(mtsms_id), 2) as 'delper',
 SUM(If ((dlr & 2 = 2), 1, 0)) as 'failed', #2
(SUM(If ((dlr & 2 = 2), 1, 0)) * 100) / COUNT(mtsms_id) as 'failper'
from core.core_mtsms sms
WHERE DATE(`timestamp`) >= DATE(DATE_SUB(NOW(), INTERVAL 4 DAY))
and service_id in (" . $cl . ")
group by date(`timestamp`), substring_index(SUBSTRING_INDEX(meta, 'mccmnc=', -1), '&', 1)
) AS DEL
LEFT JOIN
(SELECT prefix_mccmnc, prefix_network_name, prefix_country_name FROM core.core_prefix GROUP BY prefix_mccmnc)
AS P
ON mccmnc = prefix_mccmnc
LEFT JOIN
(select date(`timestamp`) as 'DAYMO', count(mosms_id) as `MOTotal`,
 substring_index(SUBSTRING_INDEX(meta, 'mccmnc=', -1), '&', 1) as MCCMNCMO, mo.service_id as service_idMO
from core.core_mosms mo
where mo.service_id in (:service_id)
AND DATE(`timestamp`) >= DATE(DATE_SUB(NOW(), INTERVAL 4 DAY))
group by date_format(`timestamp`, '%Y-%m-%d'), substring_index(SUBSTRING_INDEX(meta, 'mccmnc=', -1), '&', 1)
) as MO
on daym = MO.DAYMO
and MCCMNC = MO.MCCMNCMO");

   //set the fetch mode
   $stmt->setFetchMode(PDO::FETCH_ASSOC);

   //execute it
   $stmt->execute($data);

   //fetch the data
   $deliveryReports = $stmt->fetchAll();
   return $deliveryReports;
}

//addroute.php | addrouteadmin.php | newresellerroutes.php | newroutes.php
function removeRouteFromSubAccount($route_id) {
   //open the DB object
   $conn = getPDOMasterCore();

   //prepare the data
   $data = array(':route_id' => $route_id);

   //prepare the query
   $stmt = $conn->prepare("DELETE FROM `core`.`core_route` WHERE `route_id` = :route_id ");

   //execute it
   return $stmt->execute($data);
}

//addroute.php | addrouteadmin.php | newresellerroutes.php | newroutes.php
function getAddedDefaultRouteData($service_id) {
   //open the DB object
   $conn = getPDOMasterCore();

   //prepare the data
   $data = array(':service_id' => $service_id);

   //prepare the query
   $stmt = $conn->prepare("SELECT p.prefix_country_name as 'COUNTRY',
 concat(p.prefix_network_name, ' (', r.route_mccmnc, ')') as 'NETWORK',
 r.route_status as 'STATUS',
 format(r.route_billing/10000, 4) as 'RATE',
 r.route_id
FROM core.core_route r
LEFT OUTER JOIN core.core_prefix p
ON r.route_mccmnc = p.prefix_mccmnc
WHERE r.route_msg_type = 'MTSMS' AND r.service_id = :service_id
GROUP BY p.prefix_country_name,
 concat(p.prefix_network_name, ' (', r.route_mccmnc, ')'),
 r.route_status,
 format(r.route_billing/10000, 4)");

   //set the fetch mode
   $stmt->setFetchMode(PDO::FETCH_ASSOC);

   //execute it
   $stmt->execute($data);

   //fetch the data
   $defaultRoutes = $stmt->fetchAll();
   return $defaultRoutes;
}

//updateservice.php
function createDlrUrls($service_id, $endpoint_dlr_address, $endpoint_dlr_status, $endpoint_dlr_method, $type, $endpoint_dlr_flags) {
   //open the DB object
   $conn = getPDOMasterCore();

   if ($endpoint_dlr_method == 'smpp' || $endpoint_dlr_method == 'smpp custom') {
      //prepare the data
      $data = array(':service_id' => $service_id, ':endpoint_dlr_address' => $endpoint_dlr_address, ':endpoint_dlr_status' => $endpoint_dlr_status,
          ':endpoint_dlr_method' => $endpoint_dlr_method, ':endpoint_dlr_flags' => $endpoint_dlr_flags);

      //prepare the query
      $stmt = $conn->prepare("INSERT INTO core.push_endpoint_dlr(`service_id`, `endpoint_dlr_address`, `endpoint_dlr_status`, `endpoint_dlr_method`, `endpoint_dlr_flags`)
VALUES(:service_id, :endpoint_dlr_address, :endpoint_dlr_status, :endpoint_dlr_method, :endpoint_dlr_flags)");
   }

   if ($endpoint_dlr_method == 'http' || $endpoint_dlr_method == 'http custom') {
      $method = 'LEGACY';

      //prepare the data
      $data = array(':service_id' => $service_id, ':endpoint_dlr_address' => $endpoint_dlr_address, ':endpoint_dlr_status' => $endpoint_dlr_status,
          ':endpoint_dlr_method' => $method, ':endpoint_dlr_flags' => $endpoint_dlr_flags);

      //prepare the query
      $stmt = $conn->prepare("INSERT INTO core.push_endpoint_dlr(`service_id`, `endpoint_dlr_address`, `endpoint_dlr_status`, `endpoint_dlr_method`, `endpoint_dlr_flags`)
VALUES(:service_id, :endpoint_dlr_address, :endpoint_dlr_status, :endpoint_dlr_method, :endpoint_dlr_flags)");
   }

   //execute it
   return $stmt->execute($data);
}

//Used in the allInclusive.php
function setDlrUrls($service_id, $endpoint_dlr_address, $endpoint_dlr_status, $endpoint_dlr_method, $endpoint_dlr_flags) {
   if ($endpoint_dlr_method == 'http' || $endpoint_dlr_method == 'http custom') {
      $endpoint_dlr_method = 'LEGACY';
   }

   //open the DB object
   $conn = getPDOMasterCore();

   //prepare the data
   $data = array(':service_id' => $service_id, ':endpoint_dlr_address' => $endpoint_dlr_address, ':endpoint_dlr_status' => $endpoint_dlr_status,
       ':endpoint_dlr_method' => $endpoint_dlr_method, ':endpoint_dlr_flags' => $endpoint_dlr_flags);

   //prepare the query
   $stmt = $conn->prepare("INSERT INTO core.push_endpoint_dlr(
`service_id`,
 `endpoint_dlr_address`,
 `endpoint_dlr_status`,
 `endpoint_dlr_method`,
 `endpoint_dlr_flags`)
VALUES(:service_id, :endpoint_dlr_address, :endpoint_dlr_status, :endpoint_dlr_method, :endpoint_dlr_flags)
ON DUPLICATE KEY
UPDATE
`service_id` = :service_id,
 `endpoint_dlr_address` = :endpoint_dlr_address,
 `endpoint_dlr_status` = :endpoint_dlr_status,
 `endpoint_dlr_method` = :endpoint_dlr_method,
 `endpoint_dlr_flags` = :endpoint_dlr_flags ");

   //execute it
   return $stmt->execute($data);
}

//Used in the allInclusive.php
function setMosmsUrls($service_id, $endpoint_mosms_address, $endpoint_mosms_status, $endpoint_mosms_method, $endpoint_mosms_flags) {
   if ($endpoint_mosms_method == 'http' || $endpoint_mosms_method == 'http custom') {
      $endpoint_mosms_method = 'LEGACY';
   }

   //open the DB object
   $conn = getPDOMasterCore();

   //prepare the data
   $data = array(':service_id' => $service_id, ':endpoint_mosms_address' => $endpoint_mosms_address, ':endpoint_mosms_status' => $endpoint_mosms_status,
       ':endpoint_mosms_method' => $endpoint_mosms_method, ':endpoint_mosms_flags' => $endpoint_mosms_flags);

   //prepare the query
   $stmt = $conn->prepare("INSERT INTO core.push_endpoint_mosms(
`service_id`,
 `endpoint_mosms_address`,
 `endpoint_mosms_status`,
 `endpoint_mosms_method`,
 `endpoint_mosms_flags`)
VALUES(:service_id, :endpoint_mosms_address, :endpoint_mosms_status, :endpoint_mosms_method, :endpoint_mosms_flags)
ON DUPLICATE KEY
UPDATE
`service_id` = :service_id,
 `endpoint_mosms_address` = :endpoint_mosms_address,
 `endpoint_mosms_status` = :endpoint_mosms_status,
 `endpoint_mosms_method` = :endpoint_mosms_method,
 `endpoint_mosms_flags` = :endpoint_mosms_flags ");

   //execute it
   return $stmt->execute($data);
}

//**********************************
//Used in the allInclusive.php
function addUser($user_id, $service_id) {
   //open the DB object
   $conn = getPDOMasterCore();

   //prepare the data
   $data = array(':user_id' => $user_id, ':service_id' => $service_id);

   //prepare the query
   $stmt = $conn->prepare("INSERT INTO core.core_permission(user_id, service_id, permission_type, permission_status)
VALUES (:user_id, :service_id, 'submit.mtsms', 'ENABLED')");

   //execute it
   $stmt->execute($data);

   //prepare the data
   $data2 = array(':user_id' => $user_id, ':service_id' => $service_id);

   //prepare the query
   $stmt2 = $conn->prepare("INSERT INTO core.core_permission(user_id, service_id, permission_type, permission_status)
VALUES (:user_id, :service_id, 'SUPER', 'ENABLED')");

   //execute it
   $stmt2->execute($data2);

   return true;
}

//updateservice.php
function checkUser($nU, $users) {
   if (isset($users)) {
      foreach ($users as $key => $value) {
         if ($value['user_username'] == $nU) {
            return true;
         }
      }
   } else {
      return false;
   }
}

//addUser.php | assignroles.php + newusers.php
function getAllUsers() {
   //open the DB object
   $conn = getPDOMasterCore();

   //prepare the query
   $stmt = $conn->prepare('SELECT user_id, user_username, user_password
                FROM core.core_user
                WHERE user_status = "ENABLED"
                ORDER BY user_username ASC');

   //set the fetch mode
   $stmt->setFetchMode(PDO::FETCH_ASSOC);

   //execute it
   $stmt->execute();

   //fetch the data
   $allUsers = $stmt->fetchAll();
   return $allUsers;
}

/**
 * This function returns a user record from core_user using the user id
 * @param int $user_id - The ID of the user
 * @return Array $user_account - An associative array of the user record
 */
function getUser($user_id) {
   $conn = getPDOMasterCore();

   //prepare the data
   $data = array(':user_id' => $user_id);

   //prepare the query
   $stmt = $conn->prepare('SELECT *
                            FROM core.core_user
                            WHERE user_id = :user_id ');

   //set the fetch mode
   $stmt->setFetchMode(PDO::FETCH_ASSOC);

   //execute it
   $stmt->execute($data);

   //fetch the data
   $user_account = $stmt->fetch();

   return $user_account;
}

/**
 * This function returns a user account details record from account_details using the user id
 * @param int $user_id - The ID of the user
 * @return Array $user_account_details - An associative array of the user details record
 */
function getUserAccountDetails($user_id) {
   $conn = getPDOMasterCore();

   //prepare the data
   $data = array(':user_id' => $user_id);

   //prepare the query
   $stmt_get_deets = $conn->prepare('SELECT *
                            FROM core.account_details
                            WHERE core_user_id = :user_id ');

   //set the fetch mode
   $stmt_get_deets->setFetchMode(PDO::FETCH_ASSOC);

   //execute it
   $stmt_get_deets->execute($data);

   //fetch the data
   $user_account_details = $stmt_get_deets->fetch();

   return $user_account_details;
}

//newservice.php
function getNewAccountData($service_name) {
   //open the DB object
   $conn = getPDOMasterCore();

   //prepare the data
   $data = array(':service_name' => $service_name);

   //prepare the query
   $stmt = $conn->prepare('SELECT s.service_id, s.account_id, s.service_login, u.user_id
                FROM core.core_service s
                INNER JOIN core.core_user u
                ON s.service_login = u.user_username
                WHERE service_name = :service_name ');

   //set the fetch mode
   $stmt->setFetchMode(PDO::FETCH_ASSOC);

   //execute it
   $stmt->execute($data);

   //fetch the data
   $newServiceData = $stmt->fetchAll();
   return $newServiceData;
}

//adduser.php | newusers.php | updateservice.php | viewservice.php | viewserviceAdmin.php
function getUsers($service_id) {
   //open the DB object
   $conn = getPDOMasterCore();

   //prepare the data
   $data = array(':service_id' => $service_id);

   //prepare the query
   $stmt = $conn->prepare("SELECT u.user_id, u.user_default_service_id, u.user_username, u.user_password, u.user_status, s.service_login
FROM core.core_permission p
INNER JOIN core.core_user u
ON p.user_id = u.user_id
INNER JOIN core.core_service s
ON p.service_id = s.service_id
WHERE p.service_id = :service_id
AND user_username NOT LIKE '%.connet'
AND user_default_service_id = :service_id
GROUP BY u.user_username, u.user_password, u.user_status");

   //set the fetch mode
   $stmt->setFetchMode(PDO::FETCH_ASSOC);

   //execute it
   $stmt->execute($data);

   //fetch the data
   $users = $stmt->fetchAll();
   return $users;
}

//updateservice.php | viewservice.php | viewserviceAdmin.php
function getPayments($service_id) {
   //open the DB object
   $conn = getPDOMasterCore();

   //prepare the data
   $data = array(':service_id' => $service_id);

   //prepare the query
   $stmt = $conn->prepare("SELECT payment_timestamp, payment_description, format(payment_value / 10000, 4)
AS 'Value', format(payment_previous_balance / 10000, 4)
AS 'Previous Balance', u.user_username
FROM core.core_service_payment p
INNER JOIN core.core_user u
ON p.user_id = u.user_id
WHERE service_id = :service_id ");

   //set the fetch mode
   $stmt->setFetchMode(PDO::FETCH_ASSOC);

   //execute it
   $stmt->execute($data);

   //fetch the data
   $payments = $stmt->fetchAll();
   return $payments;
}

//updateservice.php
function getAlerts($service_id) {
   //open the DB object
   $conn = getPDOMasterCore();

   //prepare the data
   $data = array(':service_id' => $service_id);

   //prepare the query
   $stmt = $conn->prepare("SELECT service_notification_threshold, service_notification_email, service_notification_sms
FROM core.core_service_credit
WHERE service_id = :service_id ");

   //set the fetch mode
   $stmt->setFetchMode(PDO::FETCH_ASSOC);

   //execute it
   $stmt->execute($data);

   //fetch the data
   $alerts = $stmt->fetchAll();
   return $alerts;
}

//**********************************
//**********************************
//updateservice.php
function getEPDLRid($service_id, $type) {
   //open the DB object
   $conn = getPDOMasterCore();

   //prepare the data
   $data = array(':service_id' => $service_id, ':type' => $type);

   //prepare the query
   $stmt = $conn->prepare("SELECT endpoint_dlr_id
FROM core.push_endpoint_dlr
WHERE service_id = :service_id
AND endpoint_dlr_method LIKE :type ");

   //set the fetch mode
   $stmt->setFetchMode(PDO::FETCH_ASSOC);

   //execute it
   $stmt->execute($data);

   //fetch the data
   $endpoint = $stmt->fetch();
   return $endpoint['endpoint_dlr_id'];
}

//updateservice.php
function getEPMOSMSid($service_id, $type) {
   //open the DB object
   $conn = getPDOMasterCore();

   //prepare the data
   $data = array(':service_id' => $service_id, ':type' => $type);

   //prepare the query
   $stmt = $conn->prepare("SELECT endpoint_mosms_id
FROM core.push_endpoint_mosms
WHERE service_id = :service_id
AND endpoint_mosms_method LIKE :type ");

   //set the fetch mode
   $stmt->setFetchMode(PDO::FETCH_ASSOC);

   //execute it
   $stmt->execute($data);

   //fetch the data
   $endpoint = $stmt->fetch();
   return $endpoint['endpoint_mosms_id'];
}

//**********************************
//home.php
function getDashNetworkTotals($account_id = null) {
   //open the DB object
   $conn = getPDOReportingCore();

   if (isset($account_id)) {
      //prepare the data
      $data = array(':account_id' => $account_id);
   } else {
      //prepare the data
      $data = array(':account_id' => 55);
   }

   //prepare the query
   $stmt = $conn->prepare("SELECT prefix_country_name, prefix_network_name, sum(units) as 'units', sum(price) as 'price', mccmnc
FROM(
SELECT sum(cdr_record_units) as 'units',
 SPLIT_STR(cdr_provider, '|', 4) as 'mccmnc',
 (cdr_record_cost_per_unit/10000) as 'client_price',
 (cdr_record_cost_per_unit/10000) * sum(cdr_record_units) as 'price'
FROM reporting.cdr_record cr
INNER JOIN reporting.cdr c
on cr.cdr_id = c.cdr_id
WHERE DATE(`cdr_record_date`) >= DATE(DATE_SUB(NOW(), INTERVAL 30 DAY))
and SPLIT_STR(cdr_record_reference, '|', 2) in (select service_id from core.core_service where account_id = :account_id)
group by cdr_record_cost_per_unit, SPLIT_STR(cdr_provider, '|', 4)
) as CDR
LEFT JOIN
(SELECT prefix_mccmnc, prefix_network_name, prefix_country_name FROM core.core_prefix GROUP BY prefix_mccmnc) AS core_prefix
ON prefix_mccmnc = mccmnc
group by prefix_network_name, prefix_country_name, mccmnc
order by 1, 3 desc");

   //set the fetch mode
   $stmt->setFetchMode(PDO::FETCH_ASSOC);

   //execute it
   $stmt->execute($data);

   //fetch the data
   $netTots = $stmt->fetchAll();
   return $netTots;
}

//home.php
function getDashServiceTotals($account_id = null) {
   //open the DB object
   $conn = getPDOReportingCore();

   if (isset($account_id)) {
      //prepare the data
      $data = array(':account_id' => $account_id);
   } else {
      //prepare the data
      $data = array(':account_id' => 55);
   }

   //prepare the query
   $stmt = $conn->prepare("SELECT s.service_name, sum(units) as 'units', sum(price) as 'price'
FROM(
SELECT sum(cdr_record_units) as 'units',
 SPLIT_STR(cdr_record_reference, '|', 2) as `service_id`,
 (cdr_record_cost_per_unit/10000) as 'client_price',
 (cdr_record_cost_per_unit/10000) * sum(cdr_record_units) as 'price'
FROM reporting.cdr_record cr
INNER JOIN reporting.cdr c
on cr.cdr_id = c.cdr_id
WHERE DATE(`cdr_record_date`) >= DATE(DATE_SUB(NOW(), INTERVAL 30 DAY))
and SPLIT_STR(cdr_record_reference, '|', 2) in (select service_id from core.core_service where account_id = :account_id)
group by SPLIT_STR(cdr_record_reference, '|', 2), cdr_record_cost_per_unit
) as CDR
LEFT JOIN core.core_service s
ON CDR.service_id = s.service_id
group by s.service_name
order by 2 desc");

   //set the fetch mode
   $stmt->setFetchMode(PDO::FETCH_ASSOC);

   //execute it
   $stmt->execute($data);

   //fetch the data
   $serTots = $stmt->fetchAll();
   return $serTots;
}

//home.php
function getDashBarChart($account_id = null) {
   //open the DB object
   $conn = getPDOReportingCore();

   if (isset($account_id)) {
      //prepare the data
      $data = array(':account_id' => $account_id);
   } else {
      //prepare the data
      $data = array(':account_id' => 95);
   }

   //prepare the query
   $stmt = $conn->prepare("SELECT monthname(cdr_record_date) as 'month', day(cdr_record_date) as 'day', sum(cdr_record_units) as 'units', SPLIT_STR(cdr_record_reference, '|', 2)
FROM reporting.cdr_record cr
INNER JOIN reporting.cdr c
on cr.cdr_id = c.cdr_id
WHERE DATE(`cdr_record_date`) >= DATE(DATE_SUB(NOW(), INTERVAL 30 DAY))
#and SPLIT_STR(cdr_record_reference,'|',2) in  (select service_id from core.core_service where account_id= :account_id)
and SPLIT_STR(cdr_record_reference, '|', 2) = 329
group by monthname(cdr_record_date), day(cdr_record_date), SPLIT_STR(cdr_record_reference, '|', 2)");

   //set the fetch mode
   $stmt->setFetchMode(PDO::FETCH_ASSOC);

   //execute it
   $stmt->execute($data);

   //fetch the data
   $barData = $stmt->fetchAll();
   return $barData;
}

//batchlistphp
function getBatchList($service_id, $start_date, $end_date) {
   //open the DB object
   $conn = getPDOReportingCore();

   //prepare the data
   $data = array(':service_id' => $service_id, ':start_date' => $start_date, ':end_date' => $end_date);

   //prepare the query
   $stmt = $conn->prepare("SELECT distinct batch_id, batch_created as 'batch_created', client_name, ifnull(campaign_name, 'API Interface') as 'campaign_name', campaign_code, batch_reference,
 CASE batch_status WHEN 'NEW' THEN 'Draft' WHEN 'CSV'THEN 'Processing' WHEN 'WAIT'THEN batch_submit_status WHEN 'ERROR' THEN 'Error' WHEN 'BLACKLIST' THEN 'Blacklist' WHEN 'DONE' THEN 'Completed'
WHEN 'BUCKET' THEN 'None' WHEN 'SEND' THEN 'Sending' ELSE batch_submit_status END as 'batch_submit_status', batch_mtsms_total
FROM core.batch_batch b
LEFT JOIN core.campaign_batch USING (batch_id)
LEFT JOIN core.campaign_campaign USING (campaign_id)
LEFT JOIN core.campaign_client USING (client_id)
WHERE date(batch_created) >= :start_date
AND date(batch_created) <= :end_date
AND b.service_id = :service_id
AND batch_reference IS NOT NULL
ORDER BY batch_created ASC");

   //set the fetch mode
   $stmt->setFetchMode(PDO::FETCH_ASSOC);

   //execute it
   $stmt->execute($data);

   //fetch the data
   $batch_list = $stmt->fetchAll();
   return $batch_list;
}


/**
 * This function consumes an array of batches from SQL, and gathhers and adds all send specific statistics for each batch, assigning each batch by it's ID
 *
 * @param $batch_list - AN array of batch information pulled from the SQL table
 * @return array - An array of batches, using the batch IDs as keys for each batch, and all the send statistics added to each batch
 */
function getBatchlistStats($batch_list)
{
   $batch_list_with_stats = array();

   //we're gonna use CURL mutli to simulate the MO processing in parralell
   $curl_multi_data = array(array(),array());
   $count = 0;

   foreach ($batch_list as $batch)
   {
      if(isset($batch['batch_id']) && $batch['batch_id'] > 0)
      {
         $batch_list_with_stats[$batch['batch_id']] = $batch;
         //get all delivery stats from elasticsearch
         $batch_stats = Batch::getFullSendStats($batch['batch_id']);

         $batch_list_with_stats[$batch['batch_id']]['sent'] = $batch_stats['sent'] + $batch_stats['rejected'];
         $batch_list_with_stats[$batch['batch_id']]['mosms'] = 0; //default 0
         $batch_list_with_stats[$batch['batch_id']]['units'] = $batch_stats['units'] + $batch_stats['rejected_units'];
         $batch_list_with_stats[$batch['batch_id']]['pending'] = $batch_stats['pending'];
         $batch_list_with_stats[$batch['batch_id']]['pending_units'] = $batch_stats['pending_units'];
         $batch_list_with_stats[$batch['batch_id']]['pending_codes'] = implode(',', $batch_stats['pending_codes']);
         $batch_list_with_stats[$batch['batch_id']]['delivered'] = $batch_stats['delivered'];
         $batch_list_with_stats[$batch['batch_id']]['delivered_units'] = $batch_stats['delivered_units'];
         $batch_list_with_stats[$batch['batch_id']]['delivered_codes'] = implode(',', $batch_stats['delivered_codes']);
         $batch_list_with_stats[$batch['batch_id']]['failed'] = $batch_stats['failed'] + $batch_stats['rejected'];
         $batch_list_with_stats[$batch['batch_id']]['failed_units'] = $batch_stats['failed_units'] + $batch_stats['rejected_units'];
         $batch_list_with_stats[$batch['batch_id']]['failed_codes'] = implode(',', $batch_stats['failed_codes']).','.implode(',', $batch_stats['rejected_codes']);
         $batch_list_with_stats[$batch['batch_id']]['rejected'] = 0;
         $batch_list_with_stats[$batch['batch_id']]['blacklisted'] = 0;

         $curl_multi_data[$count]['url']  = "https://".$_SERVER['SERVER_NAME'].'/php/ajaxGetBatchMOTotal.php';
         $curl_multi_data[$count]['post'] = array();
         $curl_multi_data[$count]['post']['batch_id'] = $batch['batch_id'];
         $count ++;

         //$batch_list_with_stats[$batch['batch_id']]['rejected'] = $batch_stats['rejected'];
         //$batch_list_with_stats[$batch['batch_id']]['rejected_codes'] = implode(',', $batch_stats['rejected_codes']);
         //$batch_list_with_stats[$batch['batch_id']]['blacklisted'] = $batch_stats['blacklisted'];
         //$batch_list_with_stats[$batch['batch_id']]['blacklisted_codes'] = implode(',', $batch_stats['blacklisted_codes']);
      }
   }

   //dont pull MO counts if there is no batch list to pull
   if(isset($curl_multi_data[0]) && count($curl_multi_data[0]) > 0)
   {
      //run the MO query through the CURL multi to speed up getting stats for all the batches by doing parallel processing
      $mo_results = helperCurlMulti::multiRequest($curl_multi_data);

      //cycle through all the batch mos and set the value
      foreach($mo_results as $mo_result)
      {
         //convert JSON object to Array because that is how the script is responding
         $json_mo_result = json_decode($mo_result, true);
         if(isset($json_mo_result['success']) && $json_mo_result['success'] === true)
         {
            $batch_list_with_stats[$json_mo_result['batch_id']]['mosms'] = $json_mo_result['mo_total'];
         }
      }
   }

   return $batch_list_with_stats;
}

//**********************************
//campaign.php
function getCampaignClient($service_id) {
   //open the DB object
   $conn = getPDOMasterCore();

   //prepare the data
   $data = array(':service_id' => $service_id);

   //prepare the query
   $stmt = $conn->prepare("SELECT c.client_id, c.service_id, c.user_id, c.client_name, c.client_description, s.service_name, count(cc.campaign_id) as 'campaign total'
FROM core.campaign_client c
INNER JOIN core.core_service s
ON c.service_id = s.service_id
LEFT OUTER JOIN core.campaign_campaign cc
ON c.client_id = cc.client_id
WHERE c.service_id = :service_id
GROUP BY c.client_name, c.client_id, c.service_id, c.user_id, c.client_description, s.service_name
ORDER BY client_name = 'Default' desc, client_name asc");

   //set the fetch mode
   $stmt->setFetchMode(PDO::FETCH_ASSOC);

   //execute it
   $stmt->execute($data);

   //fetch the data
   $CCData = $stmt->fetchAll();
   return $CCData;
}

//**********************************
//collections.php
/*
  function getCollectionsNew2($startDate, $endDate)
  {
  $qry = "SELECT CONCAT(route_collection_name, ' (', default_route_id, ')') as route_collection_name, prefix_country_name, CONCAT(prefix_network_name, ' (', mccmnc_d, ')') as prefix_network_name, default_route, ported_route, route_collection_description, default_route_id, mccmnc_d,
  dmatrix_route_id, dmatrix_route, mccmnc_dx, pmatrix_route_id, pmatix_route, mccmnc_px, units
  FROM (
  SELECT rcrd.route_collection_id as 'default_route_id', rcrd.route_collection_route_smsc_name as 'default_route', route_collection_route_match_mccmnc as 'mccmnc_d'
  FROM core.core_route_collection_route rcrd
  WHERE rcrd.route_collection_id in (14, 37, 91, 92, 69, 86, 54, 5, 67, 42, 34, 29, 40, 74, 73, 98) #37,91,14,92,69,86,54,5,67
  AND rcrd.route_collection_route_match_mccmnc in ('65501', '65502', '65507', '65510')#,'65502','65507','65510'
  AND rcrd.route_collection_route_match_priority = (SELECT min(route_collection_route_match_priority)
  FROM core.core_route_collection_route
  WHERE route_collection_id = rcrd.route_collection_id
  AND route_collection_route_match_mccmnc = rcrd.route_collection_route_match_mccmnc
  LIMIT 1)
  ) as DR
  LEFT OUTER JOIN (
  SELECT rcrd.route_collection_id as 'dmatrix_route_id', rcrd.route_collection_route_smsc_name as 'dmatrix_route', route_collection_route_match_mccmnc as 'mccmnc_dx'
  FROM core.core_route_collection_route_options rcrd
  WHERE rcrd.route_collection_id in (14, 37, 91, 92, 69, 86, 54, 5, 67, 42, 34, 29, 40, 74, 73, 98) #37,91,14,92,69,86,54,5,67
  AND rcrd.route_collection_route_match_mccmnc in ('65501', '65502', '65507', '65510')
  AND rcrd.route_collection_route_match_priority = (SELECT min(route_collection_route_match_priority)
  FROM core.core_route_collection_route_options
  WHERE route_collection_id = rcrd.route_collection_id
  AND route_collection_route_match_mccmnc = rcrd.route_collection_route_match_mccmnc
  LIMIT 1)
  ) as DM
  on default_route_id = dmatrix_route_id
  and mccmnc_d = mccmnc_dx
  INNER JOIN (
  Select distinct prefix_mccmnc, prefix_network_name, prefix_country_name
  from core.core_prefix
  where prefix_country_name = 'South Africa'
  ) as PX
  on prefix_mccmnc = mccmnc_d
  INNER JOIN core_route_collection
  ON route_collection_id = default_route_id
  INNER JOIN (
  SELECT rcrd.route_collection_id as 'ported_route_id', rcrd.route_collection_route_smsc_name as 'ported_route', route_collection_route_match_mccmnc as 'mccmnc_p'
  FROM core.core_route_collection_route rcrd
  WHERE rcrd.route_collection_id in (14, 37, 91, 92, 69, 86, 54, 5, 67, 42, 34, 29, 40, 74, 73, 98) #37,91,14,92,69,86,54,5,67
  AND rcrd.route_collection_route_match_mccmnc in ('65501p', '65502p', '65507p', '65510p')#,'65502p','65507p','65510p'
  AND rcrd.route_collection_route_match_priority = (SELECT min(route_collection_route_match_priority)
  FROM core.core_route_collection_route
  WHERE route_collection_id = rcrd.route_collection_id
  AND route_collection_route_match_mccmnc = rcrd.route_collection_route_match_mccmnc
  LIMIT 1)
  ) AS P
  ON ported_route_id = default_route_id
  and replace(mccmnc_p, 'p', '') = prefix_mccmnc
  LEFT OUTER JOIN (
  SELECT rcrd.route_collection_id as 'pmatrix_route_id', rcrd.route_collection_route_smsc_name as 'pmatix_route', route_collection_route_match_mccmnc as 'mccmnc_px'
  FROM core.core_route_collection_route_options rcrd
  WHERE rcrd.route_collection_id in (14, 37, 91, 92, 69, 86, 54, 5, 67, 42, 34, 29, 40, 74, 73, 98) #37,91,14,92,69,86,54,5,67
  AND rcrd.route_collection_route_match_mccmnc in ('65501p', '65502p', '65507p', '65510p')#,'65502p','65507p','65510p'
  AND rcrd.route_collection_route_match_priority = (SELECT min(route_collection_route_match_priority)
  FROM core.core_route_collection_route_options
  WHERE route_collection_id = rcrd.route_collection_id
  AND route_collection_route_match_mccmnc = rcrd.route_collection_route_match_mccmnc
  LIMIT 1)
  ) AS PC
  ON ported_route_id = pmatrix_route_id
  and mccmnc_p = mccmnc_px
  LEFT OUTER JOIN (
  SELECT sum(C.units) as 'units', mccmnc, route_collection_id
  FROM (
  SELECT sum(cdr_record_units) as 'units',
  SPLIT_STR(cdr_record_reference, '|', 2) as `service_id`,
  SPLIT_STR(cdr_provider, '|', 4) as 'mccmnc'
  FROM reporting.cdr_record_archive cr
  INNER JOIN reporting.cdr c
  on cr.cdr_id = c.cdr_id
  where date(cdr_record_date) >= '".$startDate."' and date(cdr_record_date) < '".$endDate."'
  and SPLIT_STR(cdr_provider, '|', 4) in ('65501', '65502', '65507', '65510')
  and SPLIT_STR(cdr_record_reference, '|', 1) not in ('legacycore')
  #and SPLIT_STR(cdr_record_reference,'|',2)  = 536
  group by SPLIT_STR(cdr_record_reference, '|', 2), SPLIT_STR(cdr_provider, '|', 4)
  ) as C
  INNER JOIN core.core_route r
  ON C.service_id = r.service_id
  AND C.mccmnc = r.route_mccmnc
  #WHERE r.service_id = 536
  WHERE r.route_msg_type = 'MTSMS'
  and r.route_status = 'ENABLED'
  and r.route_collection_id in (14, 37, 91, 92, 69, 86, 54, 5, 67, 42, 34, 29, 40, 74, 73, 98)
  GROUP BY mccmnc, route_collection_id
  ) as Total
  ON default_route_id = Total.route_collection_id
  AND mccmnc_d = Total.mccmnc
  ORDER BY default_route_id = 14 desc, default_route_id = 92 desc, default_route_id = 37 desc, default_route_id = 91 desc, default_route_id = 86 desc, default_route_id = 98 desc, default_route_id = 69 desc, default_route_id = 54 desc,
  default_route_id, mccmnc_d = '65501' desc, mccmnc_d = '65510' desc, mccmnc_d = '65507' desc, mccmnc_d = '65502' desc";
  $colData = selectFromMasterDB($qry, 'core');

  return $colData;
  } */

//**********************************
//campaign.php
function getCampaignInfo($client_id) {
   //open the DB object
   $conn = getPDOMasterCore();

   //prepare the data
   $data = array(':client_id' => $client_id);

   //prepare the query
   $stmt = $conn->prepare("SELECT c.campaign_id, c.client_id, s.service_name, c.user_id, cc.client_name, c.campaign_name, c.campaign_code, c.campaign_description, c.campaign_status, count(template_id) as 'template total'
FROM core.campaign_campaign c
INNER JOIN core.campaign_client cc
ON c.client_id = cc.client_id
INNER JOIN core.core_service s
ON cc.service_id = s.service_id
LEFT OUTER JOIN core.campaign_template t
ON c.campaign_id = t.campaign_id
WHERE c.client_id = :client_id
GROUP BY c.campaign_id, c.client_id, s.service_name, c.user_id, cc.client_name, c.campaign_name, c.campaign_code, c.campaign_description, c.campaign_status");

   //set the fetch mode
   $stmt->setFetchMode(PDO::FETCH_ASSOC);

   //execute it
   $stmt->execute($data);

   //fetch the data
   $CAData = $stmt->fetchAll();
   return $CAData;
}

//campaign.php
function getCampaignTemp($campaign_id) {
   //open the DB object
   $conn = getPDOMasterCore();

   //prepare the data
   $data = array(':campaign_id' => $campaign_id);

   //prepare the query
   $stmt = $conn->prepare("SELECT template_id, user_id, template_content, template_description, template_status, template_optout_content
FROM core.campaign_template
WHERE campaign_id = :campaign_id ");

   //set the fetch mode
   $stmt->setFetchMode(PDO::FETCH_ASSOC);

   //execute it
   $stmt->execute($data);

   //fetch the data
   $TMData = $stmt->fetchAll();
   return $TMData;
}

//clienthome.php | home.php | resellerhome.php | addroute.php | adduser.php | allmo.php | apitraffic.php | batchlist.php
//batchreplies.php | billing.php | billingnew.php | bulksms.php | campaing.php | clients.php | confirmcredits.php | contactlists.php
//exclusionlists.php | netrep.php | networkpercentadmin.php | networktraffic.php | networktrafficsummary.php | newservice.php
//quicksms.php | search.php | searchMO.php | servicepercentadmin.php | turtorial.php | updatecredits.php | updateservice.php | newservice.php
function getCreditOnly($service_id) {
   //open the DB object
   $conn = getPDOMasterCore();

   //prepare the data
   $data = array(':service_id' => $service_id);

   //prepare the query
   $stmt = $conn->prepare("SELECT service_credit_available
FROM core.core_service_credit
WHERE service_id = :service_id ");

   //set the fetch mode
   $stmt->setFetchMode(PDO::FETCH_ASSOC);

   //execute it
   $stmt->execute($data);

   $credAvail = '0';

   //fetch the data
   $credit = $stmt->fetchAll();
   if (isset($credit) && $credit != '') {
      foreach ($credit as $key => $value) {
         $credAvail = $value['service_credit_available'];
      }
   }

   return $credAvail;
}

//cinfirmquicksms.php | confirmquicksmsold.php
function getSMSCost($service_id, $num_string) {
   //open the DB object
   $conn = getPDOReportingCore();

   //prepare the data
   $data = array(':service_id' => $service_id);

   //prepare the query
   $stmt = $conn->prepare("SELECT route_billing, prefix_network_name, prefix_country_name, prefix_mccmnc, route_currency
FROM (
SELECT route_billing, route_mccmnc, route_currency
FROM core.core_route
WHERE service_id = :service_id
AND route_mccmnc IN (" . $num_string . ")
AND route_msg_type = 'MTSMS'
) AS ROUTE
LEFT JOIN
(SELECT prefix_mccmnc, prefix_network_name, prefix_country_name FROM core.core_prefix GROUP BY prefix_mccmnc) AS core_prefix
ON prefix_mccmnc = route_mccmnc");

   //set the fetch mode
   $stmt->setFetchMode(PDO::FETCH_ASSOC);

   //execute it
   $stmt->execute($data);

   //fetch the data
   $costsArr = $stmt->fetchAll();
   return $costsArr;
}

//pause.php
function updateWithTime($batch_id, $batch_submit_status, $batch_schedule) {
   //open the DB object
   $conn = getPDOMasterCore();

   //prepare the data
   $data = array(':batch_id' => $batch_id, ':batch_submit_status' => $batch_submit_status, ':batch_schedule' => $batch_schedule);

   //prepare the query
   $stmt = $conn->prepare("UPDATE batch_batch SET batch_status = 'WAIT', batch_submit_status = :batch_submit_status, batch_schedule = :batch_schedule
      WHERE batch_id = :batch_id ");
   ;

   //execute it
   return $stmt->execute($data);
}

//pause.php
function updateWithoutTime($batch_id, $batch_submit_status) {
   //open the DB object
   $conn = getPDOMasterCore();

   //prepare the data
   $data = array(':batch_id' => $batch_id, ':batch_submit_status' => $batch_submit_status);

   //prepare the query
   $stmt = $conn->prepare("UPDATE batch_batch
SET batch_status = 'WAIT', batch_submit_status = :batch_submit_status
WHERE batch_id = :batch_id ");
   ;

   //execute it
   return $stmt->execute($data);
}

//**********************************
//successquicksms.php
function validateSMSSendNew($service_id, $user_id) {
   //open the DB object
   $conn = getPDOMasterCore();

   //prepare the data
   $data = array(':service_id' => $service_id, ':user_id' => $user_id);

   //prepare the query
   $stmt = $conn->prepare("SELECT DISTINCT service_login, user_password, user_status, service_status, user_username
FROM core.core_permission p
inner join core.core_user u
on p.user_id = u.user_id
inner join core.core_service s
on p.service_id = s.service_id
WHERE p.service_id = :service_id
AND p.user_id = :user_id
AND p.permission_status = 'ENABLED' and u.user_status = 'ENABLED' and s.service_status = 'ENABLED'");

   //set the fetch mode
   $stmt->setFetchMode(PDO::FETCH_ASSOC);

   //execute it
   $stmt->execute($data);

   //fetch the data
   $smsData = $stmt->fetchAll();
   return $smsData;
}

//confirmbulkfile.php | confirmbulkfile.php | confirmbulkfile2.php | confirmbulkfileold.php | successquicksms.php
function guid() {
   if (function_exists('com_create_guid')) {
      return com_create_guid();
   } else {
      mt_srand((double) microtime() * 10000); //optional for php 4.2.0 and up.
      $charid = strtoupper(md5(uniqid(rand(), true)));
      $hyphen = chr(45); // "-"
      $uuid = chr(123)// "{"
              . substr($charid, 0, 8) . $hyphen
              . substr($charid, 8, 4) . $hyphen
              . substr($charid, 12, 4) . $hyphen
              . substr($charid, 16, 4) . $hyphen
              . substr($charid, 20, 12)
              . chr(125); // "}"

      return $uuid;
   }
}

//exclusionlist.php
function getExNums() {
   //open the DB object
   $conn = getPDOMasterCore();

   //prepare the query
   $stmt = $conn->prepare("SELECT dnc_id, dnc_created, dnc_msisdn
FROM MNP.dnc
ORDER BY dnc_created desc");

   //set the fetch mode
   $stmt->setFetchMode(PDO::FETCH_ASSOC);

   //execute it
   $stmt->execute();

   //fetch the data
   $exNums = $stmt->fetchAll();
   return $exNums;
}

//exclusionlist.php
function removeFromEx($dnc_id) {
   //open the DB object
   $conn = getPDOMasterCore();

   //prepare the data
   $data = array(':dnc_id' => $dnc_id);

   //prepare the query
   $stmt = $conn->prepare("DELETE FROM `MNP`.`dnc`
WHERE `dnc_id` = :dnc_id ");

   //execute it
   return $stmt->execute($data);
}

// exclusionlist.php
function addToEx($dnc_msisdn) {
   //open the DB object
   $conn = getPDOMasterCore();

   //prepare the data
   $data = array(':dnc_msisdn' => $dnc_msisdn);

   //prepare the query
   $stmt = $conn->prepare("INSERT INTO MNP.dnc (`dnc_msisdn`)
VALUES (:dnc_msisdn) ");

   //execute it
   return $stmt->execute($data);
}

function getUserServicePermissions($user_id) {

   $pdo = getPDOMasterCore();
   $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
   /*      [service_id] => 1094
     [user_id] => 1190
     [user_username] => jnz.cybernew
     [user_password] => cyber837
     [user_password_hashed] => $2y$10$i3rbePyno71b74w.jgdS5.0eojgBBQ5d2IUBkVxyCaDfRhuVeNSK.
     [user_status] => ENABLED
     [user_created] => 2015-08-11 16:28:00
     [user_default_service_id] => 1094
     [user_system_admin] => 0
     [user_account_id] => 539
     [user_fullname] => CyberNew/JNZ
     [permission_id] => 28198
     [permission_type] => submit.mtsms
     [permission_status] => ENABLED
     [permission_created] => 2015-08-11 16:28:00
     [account_id] => 539
     [service_name] => CyberNew/JNZ
     [service_login] => jnz.cybernew
     [service_status] => ENABLED
     [service_created] => 2015-08-11 16:28:00
     [service_meta] => cm_slots=00-99&cm_smpp
     [service_manager_user_id] => 745
     [service_id_reseller] =>
     [service_billed_service_id] => */
   $sql = <<<SQLGOESHERE
           
   SELECT  
         account_id, account_name,
         service_id, service_login, service_name, service_id_reseller,
         user_id, user_username, 
         permission_type
      
   FROM core_user
   JOIN core_permission USING (user_id)
   JOIN core_service USING (service_id)
   JOIN core_account USING (account_id)
   WHERE core_user.user_id = :user_id        
   AND user_status = 'ENABLED'
   AND permission_status = 'ENABLED'
   AND service_status = 'ENABLED'
   AND account_status = 'ENABLED'
           
SQLGOESHERE;

   $statement = $pdo->prepare($sql);

   $statement->execute(array('user_id' => $user_id));

   $results = array();
   foreach ($statement->fetchAll(PDO::FETCH_ASSOC) as $row) {

      $service_id = (integer) $row['service_id'];

      foreach (explode(',', strtolower($row['permission_type'])) as $permission) {

         $permission = trim(strtolower($permission));

         if (!isset($results[$service_id])) {

            unset($row['permission_type']);
            $results[$service_id] = $row;
         }

         $results[$service_id]['permissions'][$permission] = $permission;
      }
   }

   return $results;
}
