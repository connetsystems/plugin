<?php
    /**
     * This script is called by the contact list page, and runs certain ajax based tasks, such as
     * adding, editing or deleting a list, as well as adding, editing, deleting and assigning a contact to a list.
     * @author - Doug Jenkinson
     */
    require_once("allInclusive.php");

    try
    {
        session_start();
        $contact_helper = new helperContactLists();

        //check if the user is logged in, if not, redirect back to login
        if(!isset($_SESSION['userId']))
        {
            die();
        }

        if(isset($_POST['task']))
        {
            $task = $_POST['task'];
            $result['success'] = false;
            switch($task)
            {
                case "save_list" :
                    //get the relevant values
                    $service_id = $_POST['serviceId'];
                    $list_id = $_POST['listId'];
                    $list_name = $_POST['listName'];
                    $list_description = $_POST['listDescription'];
                    $list_code = $_POST['listCode'];
                    $campaign_id = ($_POST['campaignId'] != '-1' ? $_POST['campaignId'] : null);
                    $client_id = ($_POST['clientId'] != '-1' ? $_POST['clientId'] : null);
                    $listStatus = $_POST['listStatus'];

                    if($list_id == "" || $list_id == -1)
                    {
                        //no ID set for the list so we are inserting
                        $result['contact_list_id'] = $contact_helper->createContactList($service_id, $list_name, $list_description, $list_code, $campaign_id, $client_id, $listStatus);
                        if($result['contact_list_id'] == 0)
                        {
                            $result['success'] = false;
                        }
                        else
                        {
                            $result['success'] = true;
                        }
                    }
                    else
                    {
                        //okay so we have an ID, so lets update
                        $result['success'] = $contact_helper->updateContactList($list_id, $service_id, $list_name, $list_description, $list_code, $campaign_id, $client_id, $listStatus);
                    }
                    break;
                case "delete_list" :
                    $list_id = $_POST['listId'];
                    $delete_all_contact_in_list = $_POST['deleteContactsInList'];
                    if($delete_all_contact_in_list == 1)
                    {
                        $result['success'] = $contact_helper->deleteContactList($list_id, true);
                    }
                    else
                    {
                        $result['success'] = $contact_helper->deleteContactList($list_id, false);
                    }
                    break;
                case "save_contact" :
                    //get the relevant values
                    $service_id = $_POST['serviceId'];
                    $contact_id = $_POST['contactId'];
                    $contact_firstname = $_POST['firstName'];
                    $contact_lastname = $_POST['lastName'];
                    $contact_msisdn = $_POST['msisdn'];

                    if($contact_id == "" || $contact_id == -1)
                    {
                        //no ID set for the list so we are inserting
                        $contact_id = $contact_helper->addNewContact($service_id, $contact_firstname, $contact_lastname, $contact_msisdn);

                        if(isset($_POST['listId']) && $_POST['listId'] != -1 && $contact_id != 0)
                        {
                            $contact_helper->addContactToList($contact_id, $_POST['listId']);
                        }

                        $result['success'] = true;
                    }
                    else
                    {
                        //okay so we have an ID, so lets update
                        $result['success'] = $contact_helper->updateContact($contact_id, $contact_firstname, $contact_lastname, $contact_msisdn);

                        if(isset($_POST['listId']) && $_POST['listId'] != -1)
                        {
                            $contact_helper->addContactToList($contact_id, $_POST['listId']);
                        }
                    }
                    break;
                case "delete_contact" :
                    $contact_id = $_POST['contactId'];
                    $result['success'] = $contact_helper->deleteContact($contact_id);
                    break;
                case "toggle_contact_list" :
                    $contact_id = $_POST['contactId'];
                    $contact_list_id = $_POST['contactListId'];

                    //check if it is in the list and toggle it appropriately
                    if(!$contact_helper->checkContactIsInList($contact_id, $contact_list_id))
                    {
                        $result['success'] = $contact_helper->addContactToList($contact_id, $contact_list_id);
                        $result['contact_list_toggle'] = "added";
                    }
                    else
                    {
                        $result['success'] = $contact_helper->removeContactFromList($contact_id, $contact_list_id);
                        $result['contact_list_toggle'] = "removed";
                    }

                    break;
                case "ajax_upload" :
                    //we are uploading a CSV file here
                    $max_file_size = 1024 * 1024 * 1024 * 1024;

                    $target_dir = "../tempFiles/";
                    if (!is_dir($target_dir))
                    {
                        mkdir($target_dir, 0777);
                    }

                    $target_file = $target_dir . basename($_FILES["import_file"]["name"]);
                    $file_type = pathinfo($target_file, PATHINFO_EXTENSION);

                    //do some validation
                    $upload_valid = true;
                    $error_reason = "";
                    if(empty($_FILES["import_file"]["tmp_name"]))
                    {
                        $error_reason .= "No file was uploaded. ";
                        $upload_valid = false;
                    }

                    if($_FILES["import_file"]["size"] > $max_file_size)
                    {
                        $error_reason .= "The file is too large. ";
                        $upload_valid = false;
                    }

                    if($file_type != "csv")
                    {
                        $error_reason .= "The file must be in a csv format. ";
                        $upload_valid = false;
                    }

                    //move the file for processing if it is valid
                    if($upload_valid)
                    {
                        move_uploaded_file($_FILES["import_file"]["tmp_name"], $target_file);
                        $result['success'] = true;
                        $result['task'] = $task;
                        $result['file_name'] = $_FILES["import_file"]["name"];
                        $result['full_path'] = $target_file;
                    }
                    else
                    {
                        $result['success'] = false;
                        $result['reason'] = $error_reason;
                    }
                    break;
                case "ajax_upload_bulk_delete" :

                    $service_id = $_POST['service_id'];

                    //we are uploading a CSV file here
                    $max_file_size = 1024 * 1024 * 1024 * 1024;

                    $target_dir = "../tempFiles/";
                    if (!is_dir($target_dir))
                    {
                        mkdir($target_dir, 0777);
                    }

                    $target_file = $target_dir . basename($_FILES["import_file"]["name"]);
                    $file_type = pathinfo($target_file, PATHINFO_EXTENSION);

                    //do some validation
                    $upload_valid = true;
                    $error_reason = "";
                    if(empty($_FILES["import_file"]["tmp_name"]))
                    {
                        $error_reason .= "No file was uploaded. ";
                        $upload_valid = false;
                    }

                    if($_FILES["import_file"]["size"] > $max_file_size)
                    {
                        $error_reason .= "The file is too large. ";
                        $upload_valid = false;
                    }

                    if($file_type != "csv")
                    {
                        $error_reason .= "The file must be in a csv format. ";
                        $upload_valid = false;
                    }

                    //move the file for processing if it is valid
                    if($upload_valid)
                    {
                        //get the delimiter of the file so we can process it correctly
                        $delimiter = helperSMSGeneral::getDelimiter($_FILES["import_file"]["tmp_name"]);

                        $delete_count = 0;

                        //open the file and delete any matching contacts
                        if (($handle = fopen($_FILES["import_file"]["tmp_name"], "r")) !== FALSE)
                        {
                            while (($data = fgetcsv($handle, 1000, $delimiter, '"')) !== false)
                            {
                                //no validation checks for speed, any non matches are simply ignored
                                if($contact_helper->deleteContactByMSISDN($service_id, $data[0]))
                                {
                                    $delete_count ++;
                                }
                            }
                        }

                        $result['success'] = true;
                        $result['task'] = $task;
                        $result['contact_delete_count'] = $delete_count;
                    }
                    else
                    {
                        $result['success'] = false;
                        $result['reason'] = $error_reason;
                    }
                    break;

            }
        }
        else
        {
            $result = array('success' => false);
        }
    }
    catch(Exception $e)
    {
        $result = array('success' => false, 'message' => $e->getMessage());
    }

    //json encode the array before returning so it can be parsed by the JS
    echo json_encode($result);

