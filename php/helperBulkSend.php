<?php
require_once('allInclusive.php');

/**
 * MAKE SURE THE LOGGER IS AVAILABLE
 */
if (!defined('ERROR_REPORTING') && !defined('LOG_FILENAME')) {
	define('ERROR_REPORTING', true);
	define('LOG_FILENAME', '/var/log/connet/plugin/' . basename(__FILE__, '.php') . '.log');
	require_once('ConnetAutoloader.php');
}

function logProcessingStatus($total_lines_processed, $redis_job_name)
{
	//goes down if not declared here
	$redis = new Redis();
	$redis->connect(PLUGIN_DAEMON_REDIS_HOST, 6379);
	$redis->select(12);

	$batch_raw = $redis->hGetAll($redis_job_name);

	$batch_raw['sends_processed'] = $total_lines_processed;

	$redis->hmSet($redis_job_name, $batch_raw);
}


/**
 * This function builds a send file for submission by using a csv file submitted by the user
 * @param $service_id - The Service ID of the sender
 * @param $filename - The name of the CSV file that was uploaded to the server
 * @param $sms_text - The text to send in each SMS (can include placeholders eg. {2} and {3} ect...
 *
 * @return String(json) $stats - A json_encoded string representing all statistics for this send
 */
function buildCsvFileAndCalculateCost($stats_array, $service_id, $filename, $sms_text, $redis_job_name)
{
	//some stats vars
	$submission_count = 0;

	$new_file_line = array();
	$file_to_read = dirname(dirname(__FILE__)) . str_replace('../', '/', $filename);

	//get the delimiter of the file so we can process it correctly
	$delimiter = helperSMSGeneral::getDelimiter($file_to_read);

	if(function_exists('updateServer')){
		clog::info('Function exists.');
	}

	//check the SMS text
	$placeholder_numbers = helperSMSGeneral::getSMSPlaceholderNumbersAsArray($sms_text);

	clog::info('File to open and process:  '.$file_to_read. ' PLACEHOLDERS: '.json_encode($placeholder_numbers). " DELIMITER: ".$delimiter);

	//open the file for writing
	$fp = fopen($stats_array['temp_file'], 'a');

	if (($handle = fopen($file_to_read, "r")) !== FALSE)
	{
		clog::info('File opened... ');
		while (($data = fgetcsv($handle, 1000, $delimiter, '"')) !== false)
		{
			clog::info('Got row '.$stats_array['total'].' - MSISDN  '.$data[0]);

			//get the maximum column count for the file
			if ($stats_array['file_col_count'] < count($data))
			{
				$result['file_col_count'] = count($data);
			}

			if (!is_numeric($data[0]))
			{
				$stats_array['invalid_count'] ++;
			}
			else
			{
				if(count($placeholder_numbers) > 0)
				{
					$reformat_line_arr = array();
					$sms_text_injected = $sms_text;

					//insert hte MSISDN first
					$reformat_line_arr[0] = $data[0];

					//go through each data column and insert the conetent into the message so we can get an idea of length
					for ($i = 0; $i < count($placeholder_numbers); $i++)
					{
						//check if the slot {1} or {2} ect exists and then populate it
						$placeholder_string = '{'.$placeholder_numbers[$i].'}';
						$data_column = $placeholder_numbers[$i] - 1;

						//check if data exists in the column for this placeholder
						if(strpos($sms_text_injected, $placeholder_string) !== false)
						{
							//check if there is data to insert in the placeholder
							if(isset($data[$data_column]) && $data[$data_column] != "")
							{
								//clean up the string to be inserted into the message
								$insert_string = helperSMSGeneral::clean_string_encoding($data[$data_column]);

								//replace the placeholder with the column string
								$sms_text_injected = str_replace($placeholder_string, $insert_string, $sms_text_injected);

								//add the string to the reformatted array
								$reformat_line_arr[intval($data_column)] = addslashes($insert_string);

								clog::info('Valid value, inserted: '.$insert_string);
							}
							else
							{
								//replace the placeholder with the column string
								$sms_text_injected = str_replace($placeholder_string, "", $sms_text_injected);

								//no data to insert so we insert a blank string for it
								$reformat_line_arr[intval($data_column)] = "";
								clog::info('Injected string not inserted, blank inserted instead.');
							}
							clog::info('>>>>> Completed Injected string: '.$sms_text_injected);
						}
					}

					//sort the array in case the keys are all in the wrong order
					ksort($reformat_line_arr);

					//build our SMS send object
					$sms_object = buildSMSMessageWithMnpLookup($reformat_line_arr[0], $sms_text_injected);

					//add to cost to send if it is valid
					if($sms_object['valid'] == true)
					{
						//check if the code already exists, and append the units
						if(array_key_exists(strval($sms_object['mccmnc']), $stats_array['unique_mccmnc_codes']))
						{
							$stats_array['unique_mccmnc_codes'][strval($sms_object['mccmnc'])]['units'] += $sms_object['units']; //add onto existing key
							$stats_array['unique_mccmnc_codes'][strval($sms_object['mccmnc'])]['numbers'] += 1; //add onto existing key
						}
						else
						{
							$stats_array['unique_mccmnc_codes'][strval($sms_object['mccmnc'])]['units'] = $sms_object['units']; //add new key with initial count
							$stats_array['unique_mccmnc_codes'][strval($sms_object['mccmnc'])]['numbers'] = 1;//add new key with initial count
						}

						//save 10 numbers for our page preview
						if($stats_array['valid_count'] < 10)
						{
							array_push($stats_array['sms_previews'], $sms_object);
						}

						$stats_array['valid_count'] ++;
					}
					else
					{
						$stats_array['unknown_count'] ++;
					}

					//check if this number is a duplicate
					if (isset($stats_array['unique_msisdns'][$data[$i]]))
					{
						$stats_array['duplicate_count'] ++;
					}
					else
					{
						$stats_array['unique_msisdns'][$data[$i]] = 1;
					}

					$new_file_line[$submission_count] = $reformat_line_arr;

				}
				else
				{
					//build our SMS send object
					$sms_object = buildSMSMessageWithMnpLookup($data[0], $sms_text);

					//add to cost to send if it is valid
					if ($sms_object['valid'] == true)
					{
						//check if the code already exists, and append the units
						if(array_key_exists(strval($sms_object['mccmnc']), $stats_array['unique_mccmnc_codes']))
						{
							$stats_array['unique_mccmnc_codes'][strval($sms_object['mccmnc'])]['units'] += $sms_object['units']; //add onto existing key
							$stats_array['unique_mccmnc_codes'][strval($sms_object['mccmnc'])]['numbers'] += 1; //add onto existing key
						}
						else
						{
							$stats_array['unique_mccmnc_codes'][strval($sms_object['mccmnc'])]['units'] = $sms_object['units']; //add new key with initial count
							$stats_array['unique_mccmnc_codes'][strval($sms_object['mccmnc'])]['numbers'] = 1;//add new key with initial count
						}

						//save 10 numbers for our page preview
						if($stats_array['valid_count'] < 10)
						{
							array_push($stats_array['sms_previews'], $sms_object);
						}

						$stats_array['valid_count'] ++;
					}
					else
					{
						$stats_array['unknown_count'] ++;
					}

					//check if this number is a duplicate
					if (isset($stats_array['unique_msisdns'][$data[0]]))
					{
						$stats_array['duplicate_count'] ++;
					}
					else
					{
						$stats_array['unique_msisdns'][$data[0]] = 1;
					}

					$reformat_line_arr = array($data[0], $sms_text);
					$new_file_line[$submission_count] = $reformat_line_arr;
				}

				//write our prepared line to file
				fputcsv($fp, $new_file_line[$submission_count]);
				$submission_count ++;
			}

			$stats_array['total'] ++;
			if ($stats_array['total'] % 10 == 0)
			{
				logProcessingStatus($stats_array['total'], $redis_job_name);
			}

			clog::info('* >>>>>>> RESULT ON PASS ['.$i.']: '.$stats_array['total']);
		}
		fclose($handle);
	}
	else
	{
		clog::info("Couldn't open file.");
	}
	fclose($fp);

	clog::info('File processed and closed.');

	return $stats_array;
}


/**
 * This function builds a send file for submission by using a string of comma separeted numbers, typically submitted by the user for smaller sends
 * @param $service_id - The Service ID of the sender
 * @param $post_msisdns - The comma separated MSISDN string submitted by the user
 * @param $sms_text - The text to send in each SMS
 *
 * @return String(json) $stats - A json_encoded string representing all statistics for this send
 */
function generateManualMSISDNFile($stats_array, $service_id, $post_msisdns, $sms_text, $redis_job_name)
{

	//open the file for writing
	$fp = fopen($stats_array['temp_file'], 'a');

	//get the SMS length and units
	$sms_length = helperSMSGeneral::getSMSCharacterCount($sms_text);
	$units = helperSMSGeneral::getSMSUnits($sms_length);

	//convert the submitted comma separated msisdn string into an array
	$post_msisdn_arr = explode(',', $post_msisdns);

	//cycle through the array, check for validity and submit to new file
	for ($i = 0; $i < count($post_msisdn_arr); $i++)
	{
		if (substr($post_msisdn_arr[$i], 0, 1) != '0')
		{

			//check for duplicates
			if(array_key_exists($post_msisdn_arr[$i], $stats_array['unique_msisdns']))
			{
				$stats_array['duplicate_count'] ++;
			}
			else
			{
				$stats_array['unique_msisdns'][$post_msisdn_arr[$i]] = 1;
			}

			//build our SMS send object
			$sms_object = buildSMSMessageWithMnpLookup($post_msisdn_arr[$i], $sms_text, $units, $sms_length);

			//add to cost to send if it is valid
			if ($sms_object['valid'] == true) {
				//check if the code already exists, and append the units
				if (array_key_exists(strval($sms_object['mccmnc']), $stats_array['unique_mccmnc_codes'])) {
					$stats_array['unique_mccmnc_codes'][strval($sms_object['mccmnc'])]['units'] += $sms_object['units']; //add onto existing key
					$stats_array['unique_mccmnc_codes'][strval($sms_object['mccmnc'])]['numbers'] += 1; //add onto existing key
				} else {
					$stats_array['unique_mccmnc_codes'][strval($sms_object['mccmnc'])]['units'] = $sms_object['units']; //add new key with initial count
					$stats_array['unique_mccmnc_codes'][strval($sms_object['mccmnc'])]['numbers'] = 1;//add new key with initial count
				}
			}
			else
			{
				$stats_array['unknown_count'] ++;
			}

			//write CSV line
			fputcsv($fp, array($post_msisdn_arr[$i], $sms_text));

			if($stats_array['valid_count'] < 10)
			{
				array_push($stats_array['sms_previews'], $sms_object);
			}

			$stats_array['valid_count'] ++;
		}
		else
		{
			$stats_array['invalid_count'] ++;
		}
		$stats_array['total'] ++;

		if ($stats_array['total'] % 10 == 0)
		{
			logProcessingStatus($stats_array['total'], $redis_job_name);
		}
	}

	fclose($fp);

	return $stats_array;
}


/**
 * This function builds a send file for submission by using numbers drawn from a contact list.
 * @param $service_id - The Service ID of the sender
 * @param $contact_list_id - THe ID of the ocntact list to use in the send
 * @param $sms_text - The text to send in each SMS
 *
 * @return String(json) $stats - A json_encoded string representing all statistics for this send
 */
function generateContactListFile($stats_array, $service_id, $contact_list_id, $sms_text, $redis_job_name)
{
	$contact_helper = new helperContactLists();

	//open the file for writing
	$fp = fopen($stats_array['temp_file'], 'a');

	//get the SMS length and units
	$sms_length = mb_strlen($sms_text, 'utf-8') + preg_match_all('/[\\^{}\\\~€|\\[\\]]/mu', $sms_text);
	$units_per_message = ceil($sms_length/160);

	//select the contacts and process
	if($contact_list_id == "All")
	{
		$contacts = $contact_helper->getAllContactInfoNumOnlyByService($service_id);
	}
	else
	{
		$contacts = $contact_helper->getContactInfoNumOnly($contact_list_id);
	}

	foreach ($contacts as $key => $value)
	{
		//build our SMS send object
		$sms_object = buildSMSMessageWithMnpLookup($value['contact_msisdn'], $sms_text, $units_per_message, $sms_length);

		//add to cost to send if it is valid
		if($sms_object['valid'] == true)
		{
			//check if the code already exists, and append the units
			if(array_key_exists(strval($sms_object['mccmnc']), $stats_array['unique_mccmnc_codes']))
			{
				$stats_array['unique_mccmnc_codes'][strval($sms_object['mccmnc'])]['units'] += $sms_object['units']; //add onto existing key
				$stats_array['unique_mccmnc_codes'][strval($sms_object['mccmnc'])]['numbers'] += 1; //add onto existing key
			}
			else
			{
				$stats_array['unique_mccmnc_codes'][strval($sms_object['mccmnc'])]['units'] = $sms_object['units']; //add new key with initial count
				$stats_array['unique_mccmnc_codes'][strval($sms_object['mccmnc'])]['numbers'] = 1;//add new key with initial count
			}

			//write to the csv file
			fputcsv($fp, array($value['contact_msisdn'], $sms_text));

			//save 10 numbers for our page preview
			if($stats_array['valid_count'] < 10)
			{
				array_push($stats_array['sms_previews'], $sms_object);
			}

			$stats_array['valid_count'] ++;
		}
		else
		{
			$stats_array['unknown_count'] ++;
		}

		$stats_array['total'] ++;

		if ($stats_array['total'] % 10 == 0)
		{
			logProcessingStatus($stats_array['total'], $redis_job_name);
		}
	}

	//close file
	fclose($fp);

	return $stats_array;
}


/**
 * Helper function, takes in our mnp code array with totals for each network, and returns a cost array
 * @param $service_id - The Service ID of the sender
 * @param $unique_mccmnc_codes - An array with the mnp codes as keys and the total amount of sends over those networks as values
 * @return Array $cost_array - An array featuring the total cost to send as well as the individual totals for each network
 */
function getCostTOSendFromMCCMNCCountArray($service_id, $unique_mccmnc_codes)
{
	$cost_array = array();
	$cost_array['total_cost'] = 0.0;
	$network_totals = array();

	//create the default stats for undefined networks
	$undefined_network = array('mccmnc' => '', 'rate' => 0, 'total_cost' => 0, 'send_count' => 0, 'numbers' => 0,
		'country' => 'Unknown Country', 'network' => 'Unknown Network', 'currency' => '?');

	//cycle through each code, get the cost for that network, as well as add it to the total
	foreach($unique_mccmnc_codes as $mccmnc => $network_count)
	{
		$billing_info = getBillingInfoByMCCMNC($service_id, $mccmnc);
		$cost_for_this_network = $billing_info['route_billing'] * $network_count['units'];

		if($billing_info['prefix_country_name'] != null && $billing_info['prefix_network_name'] != null)
		{
			//create the stats array for this network
			$this_network_totals = array('mccmnc' => $mccmnc, 'rate' => $billing_info['route_billing'], 'total_cost' => $cost_for_this_network, 'send_count' => $network_count['units'], 'numbers' => $network_count['numbers'],
				'country' => $billing_info['prefix_country_name'], 'network' => $billing_info['prefix_network_name'], 'currency' => $billing_info['route_currency']);

			array_push($network_totals, $this_network_totals);
		}
		else
		{
			//add to the default stats
			$undefined_network['send_count'] += $network_count['units'];
			$undefined_network['numbers'] += $network_count['numbers'];
		}

		//add the cost to the total cost
		$cost_array['total_cost'] += $cost_for_this_network;
	}

	//don't show an undefined network if there are no numbers that apply to it
	if($undefined_network['send_count'] > 0 || $undefined_network['numbers'] > 0)
	{
		array_push($network_totals, $undefined_network);
	}

	$cost_array['network_totals'] = $network_totals;

	//lastly check if this user can afford to send this
	$cost_array['current_credit'] = getServiceCredit($service_id);

	$service_type = getServiceType($service_id);

	//check affordability
	if($service_type == 'PREPAID' && $cost_array['current_credit'] < $cost_array['total_cost'])
	{
		//cannot afford to send
		$cost_array['can_afford'] = false;
	}
	else
	{
		//can afford to send
		$cost_array['can_afford'] = true;
	}

	return $cost_array;
}


/**
 * Helper function builds an array "object" for a send with all the relevant information
 * @param $msisdn - The number to run the MNP lookup
 * @param $sms_text - THe text to send in the message
 * @param $units - The number of units that the send requires, if left null, then it is calculated automatically
 * @param $sms_length - The sms text char count, if left null, then it is calculated automatically
 *
 * @return array $sms_object - An array with all the relevant data on this individual send
 */
function buildSMSMessageWithMnpLookup($msisdn, $sms_text, $units = null, $sms_length = null)
{
	if(substr($msisdn, 0, 1) != "0")
	{
		if(!isset($units))
		{
			//get the SMS length and units
			$sms_length = helperSMSGeneral::getSMSCharacterCount($sms_text);
			$units = helperSMSGeneral::getSMSUnits($sms_length);
		}

		//get the mccmnc lookup
		$mccmnc = helpers\mnp::mnp_prefix_lookup($msisdn);

		if(!isset($mccmnc[0]['mccmnc']))
		{
			$sms_object = array('valid' => false, 'msisdn' => $msisdn, 'sms_length' => $sms_length, 'sms_text' => $sms_text, 'units' => $units, 'mccmnc' => '');
		}
		else
		{
			//build our SMS send object
			$sms_object = array('valid' => true, 'msisdn' => $msisdn, 'sms_length' => $sms_length, 'sms_text' => $sms_text, 'units' => $units, 'mccmnc' => $mccmnc[0]['mccmnc']);
		}
	}
	else
	{
		$sms_object = array('valid' => false, 'msisdn' => $msisdn, 'sms_length' => $sms_length, 'sms_text' => $sms_text, 'units' => $units, 'mccmnc' => '');
	}

	return $sms_object;
}

