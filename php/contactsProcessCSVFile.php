<?php
    session_start();

    ini_set('display_errors', 1);

    //--------------------------------------------------------------
    // Constants
    //--------------------------------------------------------------
    define('ERROR_REPORTING', (array_search('debug', $argv) !== false));

    //--------------------------------------------------------------
    // Includes
    //--------------------------------------------------------------
    require_once('ConnetAutoloader.php');

    clog::setLogFilename("/var/log/connet/plugin/" . basename(__FILE__, '.php') . '.log');
    clog::setBacktrace();
    clog::setVerbose(ERROR_REPORTING);

    try {



        //check if the user is logged in, if not, redirect back to login
        if (!isset($_SESSION['userId']))
        {
            //nope nope nope nope nope
            header('Location: /index.php');
            die();
        }

        /**
         * This script handles the final importing of a contact list CSV file, by using the column
         * configuration selected by the user and saving or updating records from the CSV file.
         * @author - Doug Jenkinson
         */
        require_once("allInclusive.php");

        //gather the basic variables
        $service_id = $_POST['service_id'];
        $list_id = $_POST['selected_contact_list_id'];
        $file_path = $_POST['file_path'];
        $col_count = $_POST['col_count'];
        $row_count = $_POST['row_count'];

        if(isset($list_id) && $list_id != -1)
        {
            $addToList = true;
        }
        else
        {
            $addToList = false;
        }

        $success = false;

        //collect the collumn configuration sent to us by the user
        $cols_array = array();
        for($c = 0; $c < $col_count; $c ++)
        {
            //map keys of the data to keys in the array
            switch($_POST['col_select'.$c])
            {
                case 'contact_msisdn' :
                    $msisdn_key = $c;
                    break;
                case 'contact_firstname' :
                    $firstname_key = $c;
                    break;
                case 'contact_lastname' :
                    $lastname_key = $c;
                    break;
            }
        }

        $contact_helper = new helperContactLists();

        //detect the CSV delimiter
        $delimiter = helperSMSGeneral::getDelimiter($file_path);

        //declare array to track duplicate entries
        $duplicate_tracking_array = array();
        $duplicate_count = 0;
        $invalid_count = 0;

        //for MSISDN
        if(isset($msisdn_key))
        {
            //open the file for reading
            if (($handle = fopen($file_path, "r")) !== FALSE)
            {
                $row = 0;
                while (($data = fgetcsv($handle, 0, $delimiter, '"')) !== false)
                {
                    $row ++;
                    $msisdn = "";
                    $contact_valid = true;

                    clog::info('Got row '.$row.' - MSISDN  '.$data[$msisdn_key]);

                    //for MSISDN
                    if (isset($data[$msisdn_key]) && $data[$msisdn_key] != "")
                    {
                        if (!isset($duplicate_tracking_array[$data[$msisdn_key]]))
                        {
                            if (helperSMSGeneral::validateMSISDN($data[$msisdn_key]))
                            {
                                $msisdn = $data[$msisdn_key];
                                $duplicate_tracking_array[$data[$msisdn_key]] = 1;
                                array_push($duplicate_tracking_array, $data[$msisdn_key]);
                            }
                            else
                            {
                                $contact_valid = false;
                            }
                        }
                        else
                        {
                            $contact_valid = false;
                            $duplicate_count ++;
                        }
                    }
                    else
                    {
                        $contact_valid = false;
                    }

                    if ($contact_valid)
                    {
                        $firstname = "";
                        $lastname = "";

                        //for FIRSTNAME
                        if(isset($firstname_key) && isset($data[$firstname_key]))
                        {
                            $firstname = $data[$firstname_key];
                        }

                        //for FIRSTNAME
                        if(isset($lastname_key) && isset($data[$lastname_key]))
                        {
                            $lastname = $data[$lastname_key];
                        }

                        //insert or update the contact record using the data in this row
                        $contact_id = $contact_helper->addNewContact($service_id, $firstname, $lastname, $msisdn);

                        //check if we need to add it to a list
                        if(isset($contact_id) && $addToList === true)
                        {
                            $contact_helper->addContactToList($contact_id, $list_id);
                        }
                    }
                    else
                    {
                        $invalid_count++;
                    }
                }

                fclose($handle);

                $success = true;
                $conn = null;
            }

            //redirect accordingly
            if ($success)
            {
                header("Location: ../pages/contactlists.php?service_id=".$service_id."&list_id=".$list_id."&import_success=true&duplicates=".$duplicate_count."&total=".$row_count."&invalid=".$invalid_count);
            }
            else
            {
                header("Location: ../pages/contactlists_import_data.php?file_path=".urlencode($file_path)."&service_id=".$service_id."&list_id=".$list_id."&error=true");
            }
        }
        else
        {
            header("Location: ../pages/contactlists_import_data.php?file_path=".urlencode($file_path)."&service_id=".$service_id."&list_id=".$list_id."&error=true");
        }
    }
    catch (Error $e)
    {
        clog::info('Aw damn, we got an error. FILE: '.$e->getFile().' line '.$e->getLine().'. MESSAGE: '.$e->getMessage());
        clog::info('Trace:  '.$e->getTraceAsString());

        header("Location: ../pages/contactlists_import_data.php?file_path=".urlencode($file_path)."&service_id=".$service_id."&list_id=".$list_id."&error=true");
        die();
    }