<?php

    if(isset($_POST["lookupNetworks"]))
    {
        $jsonNetArr = "";

        $netSel = "";
        if(isset($_POST["networkName"]))
        {
            $_POST["networkName"] = strstr($_POST["networkName"], '(', true);
               
            $netSel = " AND p.prefix_network_name = '".$_POST["networkName"]."' ";
        }

        $qry = "SELECT CONCAT(a.account_name,' - ',s.service_name) as 'SERVICE',concat(p.prefix_network_name,' (',r.route_mccmnc,')') as 'NETWORK', prefix_country_name,
                r.route_status as 'ROUTE STATUS',
                format(r.route_billing/10000,4) as 'RATE',
                rc.route_collection_name as 'COLLECTION',
                (select replace(rcr.route_collection_route_smsc_name,'_smsc','')
                from core.core_route_collection_route rcr
                where rcr.route_collection_id=rc.route_collection_id
                and rcr.route_collection_route_match_mccmnc= r.route_mccmnc
                order by route_collection_route_match_priority
                limit 1) as 'DEFAULT ROUTE', 
                (select replace(rcr.route_collection_route_smsc_name,'_smsc','')
                from core.core_route_collection_route rcr
                where rcr.route_collection_id=rc.route_collection_id
                and rcr.route_collection_route_match_mccmnc= concat(r.route_mccmnc,'p')
                order by route_collection_route_match_priority
                limit 1) as 'PORTED ROUTE',
                r.route_id
                FROM core.core_route r
                INNER JOIN core.core_service s
                on r.service_id = s.service_id
                INNER JOIN core.core_account a
                on s.account_id = a.account_id
                LEFT OUTER JOIN core.core_prefix p
                on r.route_mccmnc = p.prefix_mccmnc
                INNER JOIN core.core_route_collection rc
                on r.route_collection_id = rc.route_collection_id
                WHERE p.prefix_country_name = '".$_POST["lookupNetworks"]."' $netSel
                GROUP BY CONCAT(a.account_name,' - ',s.service_name),concat(p.prefix_network_name,' (',r.route_mccmnc,')'),
                r.route_status,
                format(r.route_billing/10000,4),
                rc.route_collection_name,
                r.route_collection_id,r.route_mccmnc
                ORDER BY prefix_country_name ASC;";

        $countrySearch = selectFromMasterDB($qry, 'core');
        
        foreach ($countrySearch as $key => $value) 
        {
            $jsonNetArr .= $value['SERVICE'].'|+'.$value['NETWORK'].'|+'.$value['prefix_country_name'].'|+'.$value['ROUTE STATUS'].'|+'.$value['RATE'].'|+'.$value['COLLECTION'].'|+'.$value['DEFAULT ROUTE'].'|+'.$value['PORTED ROUTE'].'|+'.$value['route_id'].';';
        } 

        echo $jsonNetArr;    
    }

    /*if(isset($_POST["networkName"]))
    {
        $jsonNetArr = "";

        $netSel = "";
        
            $_POST["networkName"] = strstr($_POST["networkName"], '(', true);
               
            $netSel = " AND p.prefix_network_name = '".$_POST["networkName"]."' ";
        

        $qry = "SELECT CONCAT(a.account_name,' - ',s.service_name) as 'SERVICE',concat(p.prefix_network_name,' (',r.route_mccmnc,')') as 'NETWORK', prefix_country_name,
                r.route_status as 'ROUTE STATUS',
                format(r.route_billing/10000,4) as 'RATE',
                rc.route_collection_name as 'COLLECTION',
                (select replace(rcr.route_collection_route_smsc_name,'_smsc','')
                from core.core_route_collection_route rcr
                where rcr.route_collection_id=rc.route_collection_id
                and rcr.route_collection_route_match_mccmnc= r.route_mccmnc
                order by route_collection_route_match_priority
                limit 1) as 'DEFAULT ROUTE', 
                (select replace(rcr.route_collection_route_smsc_name,'_smsc','')
                from core.core_route_collection_route rcr
                where rcr.route_collection_id=rc.route_collection_id
                and rcr.route_collection_route_match_mccmnc= concat(r.route_mccmnc,'p')
                order by route_collection_route_match_priority
                limit 1) as 'PORTED ROUTE',
                r.route_id
                FROM core.core_route r
                INNER JOIN core.core_service s
                on r.service_id = s.service_id
                INNER JOIN core.core_account a
                on s.account_id = a.account_id
                LEFT OUTER JOIN core.core_prefix p
                on r.route_mccmnc = p.prefix_mccmnc
                INNER JOIN core.core_route_collection rc
                on r.route_collection_id = rc.route_collection_id
                WHERE p.prefix_country_name = '".$_POST["lookupNetwork"]."' $netSel
                GROUP BY CONCAT(a.account_name,' - ',s.service_name),concat(p.prefix_network_name,' (',r.route_mccmnc,')'),
                r.route_status,
                format(r.route_billing/10000,4),
                rc.route_collection_name,
                r.route_collection_id,r.route_mccmnc
                ORDER BY prefix_country_name ASC;";

        $countrySearch = selectFromDB($qry, 'core');
        
        foreach ($countrySearch as $key => $value) 
        {
            $jsonNetArr .= $value['SERVICE'].'|+'.$value['NETWORK'].'|+'.$value['prefix_country_name'].'|+'.$value['ROUTE STATUS'].'|+'.$value['RATE'].'|+'.$value['COLLECTION'].'|+'.$value['DEFAULT ROUTE'].'|+'.$value['PORTED ROUTE'].'|+'.$value['route_id'].';';
        } 

        echo $jsonNetArr;    
    
     
    }*/

    if(isset($_POST["serviceName"]))
    {
        $jsonNetArr = "";

        $qry = "SELECT service_name
                FROM core.core_service
                WHERE service_name = '".$_POST["serviceName"]."' ";
                //AND service_id =  '".$_POST["sId"]."' ";

        $nameCheck = selectFromMasterDB($qry, 'core');
         
        foreach ($nameCheck as $key => $value) 
        {
            $jsonNetArr = $value['service_name'];
        } 

        echo $jsonNetArr;
    }

    if(isset($_POST["countryName"]))
    {
    	$jsonNetArr = "";
        
        $qry = "SELECT DISTINCT prefix_mccmnc, prefix_network_name, prefix_country_name
                FROM core.core_prefix
                WHERE prefix_country_name = '".$_POST["countryName"]."' ";

        $networks = selectFromMasterDB($qry, 'core');
        
        foreach ($networks as $key => $value) 
        {
            $jsonNetArr .= $value['prefix_network_name'].'('.$value['prefix_mccmnc'].');';
        }
        echo $jsonNetArr;
    }

    if(isset($_POST["collectionName"]))
    {
        $jsonNetArr = "";
        
        $qry = "SELECT DISTINCT rcr.route_collection_route_smsc_name AS 'Default'
                FROM core.core_route_collection rc
                INNER JOIN core.core_route_collection_route rcr
                ON rc.route_collection_id = rcr.route_collection_id
                WHERE rc.route_collection_name = '".$_POST["collectionName"]."' ";

        $defaults = selectFromMasterDB($qry, 'core');
        
        foreach ($defaults as $key => $value) 
        {
            $jsonNetArr .= $value['Default'].';';
        }
        echo $jsonNetArr;
    }

    if(isset($_POST["portedName"]))
    {
        $jsonNetArr = "";
        
        $qry = "SELECT DISTINCT rcr.route_collection_route_smsc_name AS 'Ported only with p'
                FROM core.core_route_collection rc
                INNER JOIN core.core_route_collection_route rcr
                ON rc.route_collection_id = rcr.route_collection_id
                WHERE rcr.route_collection_route_smsc_name =  '".$_POST["portedName"]."' ";

        $ported = selectFromMasterDB($qry, 'core');
        
        foreach ($ported as $key => $value) 
        {
            $jsonNetArr .= $value['Ported only with p'].';';
        }
        echo $jsonNetArr;
    }

    if(isset($_POST["accountName"]))
    {
        $aName = $_POST["accountName"];
        if($aName != 'Please Select')
        {
            if($aName != 'Please Select')
            {
                $qry = "SELECT account_id  
                        FROM core.core_account
                        WHERE account_name 
                        LIKE '".$aName."' ";

                $accArr = selectFromMasterDB($qry, 'core');

                foreach ($accArr as $key => $value) 
                {
                    $accountID = $value['account_id'];
                }

                $qry2 = "SELECT DISTINCT service_id_reseller FROM core.core_service
                        WHERE account_id = '".$accountID."' ";

                $accArr2 = selectFromMasterDB($qry2, 'core');
                
                $rCount = 0;

                foreach ($accArr2 as $key2 => $value2) 
                {
                    if($value2['service_id_reseller'] != null)
                    {
                        $rCount++;
                    }
                    //$accounts = $value['account_id'];
                }
                echo $rCount;
            }
        }
        else
        {
            echo 'nope';
        }
/*
                $accArr = selectFromMasterDB($qry, 'core');

                foreach ($accArr as $key => $value) 
                {
                    $accountID = $value['account_id'];
                }

        echo $accountID;
*/

          //      return $accountID;

        /*$jsonNetArr = "";
        
        $qry = "SELECT DISTINCT rcr.route_collection_route_smsc_name AS 'Ported only with p'
                FROM core.core_route_collection rc
                INNER JOIN core.core_route_collection_route rcr
                ON rc.route_collection_id = rcr.route_collection_id
                WHERE rcr.route_collection_route_smsc_name =  '".$_POST["portedName"]."' ";

        $ported = selectFromMasterDB($qry, 'core');
        
        foreach ($ported as $key => $value) 
        {
            $jsonNetArr .= $value['Ported only with p'].';';
        }*/
    }

    function selectFromMasterDB($qry, $dbToSelect)
    {
        //$host = '10.3.0.42'; // MASTER 
	    $host = '10.3.0.48';   // TestDB
        $user = 'jacquesb';
        $pass = 'pei9ZeToh3iuvee';

        $con = mysqli_connect($host, $user, $pass, $dbToSelect);

        if(mysqli_connect_errno())
        {
            echo "Failed to connect to server. If your internet connection is working, the problem might be on the server side. Err: ".mysqli_connect_error();
            $result = "Error!";
        }
        else
        {
            $result = mysqli_query($con, $qry);
        }
        return $result;
    }

?>