<?php
require_once("helperRouting.php");
if(( isset($_GET["country"]) && !empty($_GET["country"])) && (isset($_GET["collection"]) && !empty($_GET["collection"])))
{

    $country = $_GET["country"];
    $collection = $_GET["collection"];
    $filename = 'International_Costs_Collection_('.$collection.').csv';

    header('Content-Type: application/csv');
    header('Content-Disposition: attachment; filename="'.$filename.'";');

    $output = fopen('php://output', 'w');
    $headerColumns = array("ID","Country","Network","Default Route","Price","Nexmo Price","Silver Price","Clx Price","Portal Price","Cellfind Price","Price(in Rands)","Effective Date");
    fputcsv($output, $headerColumns, ',');

    $collectionsData = getCollectionsInternationalByCountryAndCollection($country,$collection);
    if(isset($collectionsData) && count($collectionsData) > 0)
    {
        //if collectionsData is set and contains some data, get exchange rate
        $exchangeRate = get_currency('EUR', 'ZAR', 1);

        foreach($collectionsData as $collectionItem)
        {
            //parse vars
            $price =  isset($collectionItem['cdr_cost_per_unit']) ? number_format(($collectionItem['cdr_cost_per_unit'] / 10000), 4, '.', '') : 0.00;
            $nexmoPrice = isset($collectionItem['nexmo_price']) ? number_format(($collectionItem['nexmo_price'] / 10000), 4, '.', '') : 0.00;
            $silverPrice = isset($collectionItem['silverstreet_price']) ? number_format(($collectionItem['silverstreet_price'] / 10000), 4, '.', '') : 0.00;
            $clxPrice = isset($collectionItem['clx_price']) ? number_format(($collectionItem['clx_price'] / 10000), 4, '.', '') : 0.00;
            $portalPrice = isset($collectionItem['portal_price']) ? number_format(($collectionItem['portal_price'] / 10000), 4, '.', '') : 0.00;
            $cellFindPrice = isset($collectionItem['cellfind_price']) ? number_format(($collectionItem['cellfind_price'] / 10000), 4, '.', '') : 0.00;
            $priceInRands = isset($collectionItem['nexmo_price']) ? number_format(($collectionItem['nexmo_price'] / 10000), 4, '.', '') : 0.00;
            if ((strpos($collectionItem["cdr_comments"],"eur") !== false))
            {
                $priceInRands =  number_format(($collectionItem['cdr_cost_per_unit'] / 10000) * $exchangeRate, 4, '.', '');
            } else {
                $priceInRands = number_format(($collectionItem['cdr_cost_per_unit'] / 10000), 4, '.', '');
            }
            $row = array(
                $collectionItem["cdr_id"],
                $collectionItem["prefix_country_name"],
                $collectionItem["prefix_network_name"],
                $collectionItem["default_route"],
                $price,
                $nexmoPrice,
                $silverPrice,
                $clxPrice,
                $portalPrice,
                $cellFindPrice,
                $priceInRands,
                $collectionItem["cdr_effective_date"],

            );
            fputcsv($output, $row, ',');
        }
    }

}else{
    echo "Country and Collection is not set.";
}

function get_currency($from_Currency, $to_Currency, $amount) {

    $amount = urlencode($amount);
    $from_Currency = urlencode($from_Currency);
    $to_Currency = urlencode($to_Currency);

    $url = "http://www.google.com/finance/converter?a=$amount&from=$from_Currency&to=$to_Currency";

    $ch = curl_init();
    $timeout = 0;
    curl_setopt($ch, CURLOPT_URL, $url);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);

    curl_setopt($ch, CURLOPT_USERAGENT, "Mozilla/4.0 (compatible; MSIE 8.0; Windows NT 6.1)");
    curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, $timeout);
    $rawdata = curl_exec($ch);
    curl_close($ch);
    $data = explode('bld>', $rawdata);
    $data = explode($to_Currency, $data[1]);

    return round($data[0], 2);
}