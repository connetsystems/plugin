<?php
require_once('config/dbConn.php');

if(isset($_POST['action']))
{
	//echo 'Test='.$_POST['action'];
	  $startDate = mktime(0, 0, 0)*1000;
    $endDate = (time()*1000);

    /*$startDate = "1428616800000";
    $endDate = "1428703199999";*/

    $qry = '{
          "query": {
            "filtered": {
              "query": {
                "query_string": {
                  "analyze_wildcard": true,
                  "query": "(smsc:portal_smsc) OR (smsc:routesms_smsc) OR (smsc:global_smsc) OR (smsc:clxlocal_smsc) OR (smsc:jabster_smsc) OR (smsc:globalcellc_smsc) OR (smsc:portal_connetsms1_smsc) OR (smsc:vodacom_smsc) OR (smsc:mtn_smsc) OR (smsc:cellc_smsc) OR (smsc:telkom_smsc) OR (smsc:cellfindecns_smsc) OR (smsc:globalvc_smsc)"
                }
              },
              "filter": {
                "bool": {
                  "must": [
                    {
                      "range": {
                        "timestamp": {
                          "gte": '.$startDate.',
                          "lte": '.$endDate.'
                        }
                      }
                    }
                  ],
                  "must_not": []
                }
              }
            }
          },
          "size": 0,
          "aggs": {
            "data": {
              "date_histogram": {
                "field": "timestamp",
                "interval": "1d",
                "pre_zone": "+02:00",
                "pre_zone_adjust_large_interval": true,
                "min_doc_count": 1,
                "extended_bounds": {
                  "min": '.$startDate.',
                  "max": '.$endDate.'
                }
              },
              "aggs": {
                "one": {
                  "sum": {
                    "script": "doc[\'billing_units\'].value",
                    "lang": "expression"
                  }
                },
                "mccmnc": {
                  "terms": {
                    "field": "mccmnc",
                    "size": 0,
                    "order": {
                      "1": "desc"
                    }
                  },
                  "aggs": {
                    "1": {
                      "sum": {
                        "script": "doc[\'billing_units\'].value",
                        "lang": "expression"
                      }
                    },
                    "smsc": {
                      "terms": {
                        "field": "smsc",
                        "size": 0,
                        "order": {
                          "1": "desc"
                        }
                      },
                      "aggs": {
                        "1": {
                          "sum": {
                            "script": "doc[\'billing_units\'].value",
                            "lang": "expression"
                          }
                        },
                        "dlr": {
                          "terms": {
                            "field": "dlr",
                            "size": 0,
                            "order": {
                              "dlr_units": "desc"
                            }
                          },
                          "aggs": {
                            "dlr_units": {
                              "sum": {
                                "script": "doc[\'billing_units\'].value",
                                "lang": "expression"
                              }
                            },
                            "rdnc": {
                              "terms": {
                                "field": "rdnc",
                                "size": 0,
                                "order": {
                                  "1": "desc"
                                }
                              },
                              "aggs": {
                                "1": {
                                  "sum": {
                                    "script": "doc[\'billing_units\'].value",
                                    "lang": "expression"
                                  }
                                }
                              }
                            }
                          }
                        }
                      }
                    }
                  }
                }
              }
            }
          }
        }
        ';

    $output = json_encode(runRawMTSMSElasticsearchQuery($qry));

    echo $output;
}

?>