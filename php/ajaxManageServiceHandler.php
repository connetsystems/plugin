<?php
    /**
     * This script is called by the manage services page, and runs certain ajax based tasks
     * @author - Doug Jenkinson
     */
    require_once("allInclusive.php");
    require_once("../PluginAPI/PluginAPIAutoload.php");

    try
    {
        session_start();
        $service_helper = new sqlHelperServices();

        //check if the user is logged in, if not, redirect back to login
        if(!isset($_SESSION['userId']))
        {
            $result = array('success' => false, 'reason' => 'session_expired');
            echo json_encode($result);
            die();
        }

        if(isset($_POST['task']))
        {
            $task = $_POST['task'];
            $result['success'] = false;
            switch($task)
            {
                case "get_payment_history" :
                    //get the relevant values
                    $service_id = $_POST['serviceId'];
                    $offset = $_POST['offset'];
                    $limit = $_POST['limit'];

                    //no ID set for the list so we are inserting
                    $result['payments'] = $service_helper->getPaymentRecords($service_id, $offset, $limit);
                    $result['total_payment_records'] = $service_helper->getTotalPaymentRecords($service_id);
                    $result['limit'] = $limit;
                    $result['offset'] = $offset;
                    $result['success'] = true;
                    break;
                case "update_credit" :
                    //get the relevant values
                    $service_id = $_POST['serviceId'];
                    $credit_amount = $_POST['creditAmount'];
                    $credit_description = $_POST['creditDescription'];

                    //get the service object
                    $service_obj = new Service($service_id);

                    $result['success'] = $service_obj->updateCreditWithCurrency($credit_amount, $credit_description);
                    break;
                case "get_daily_send_chart_data_by_date" :
                    //get the relevant values
                    $service_id = $_POST['serviceId'];
                    $start_date = $_POST['startDate'];
                    $end_date = $_POST['endDate'];

                    //get all the data for the graph by day
                    $daily_sends = Service::getDailySends($service_id, $start_date, $end_date);

                    $result['labels'] = array();
                    $result['daily_sent'] = array();
                    foreach($daily_sends as $date => $count)
                    {
                        array_push($result['labels'],  date('d-M-y', strtotime($date)));
                        array_push($result['daily_sent'], $count);
                    }
                    $result['success'] = true;
                    break;
                case "save_service_field_value" :
                    $service_id = $_POST['serviceId'];
                    $field_name = $_POST['fieldName'];
                    $field_value = $_POST['fieldValue'];

                    //special handling to convert notification threshold number to the thousands like it should be
                    if($field_name == "service_notification_threshold")
                    {
                        $field_value = $field_value * 10000;
                    }

                    $result['success'] = $service_helper->saveServiceFieldValue($service_id, $field_name, $field_value);
                    break;
                case "save_http_mosms_endpoint_value" :
                    $service_id = $_POST['serviceId'];
                    $field_name = $_POST['fieldName'];
                    $field_value = $_POST['fieldValue'];

                    //get the service object
                    $service_obj = new Service($service_id);
                    $result['success'] = $service_obj->saveHTTPMOSMSEndpointValue($field_name, $field_value);
                    break;
                case "save_http_dlr_endpoint_value" :
                    $service_id = $_POST['serviceId'];
                    $field_name = $_POST['fieldName'];
                    $field_value = $_POST['fieldValue'];

                    $service_obj = new Service($service_id);
                    $result['success'] = $service_obj->saveHTTPDLREndpointValue($field_name, $field_value);
                    break;
                case "save_credits_value" :
                    $endpoint_id = $_POST['fieldId'];
                    $field_name = $_POST['fieldName'];
                    $field_value = $_POST['fieldValue'];

                    $result['success'] = $service_helper->saveMOSMSEndpointValue($endpoint_id, $field_name, $field_value);
                    break;
                case "toggle_low_balance_notifications_status" :
                    $service_id = $_POST['serviceId'];
                    $status = $_POST['status'];

                    //get the service object
                    $service_obj = new Service($service_id);

                    $result['success'] = $service_obj->toggleLowBalanceNotifications($status);
                    break;
                case "toggle_http_dlr_status" :
                    $service_id = $_POST['serviceId'];
                    $status = $_POST['status'];

                    //get the service object
                    $service_obj = new Service($service_id);

                    $result['success'] = $service_obj->setHTTPDLREndpointStatus($status);
                    break;
                case "toggle_smpp_dlr_status" :
                    $service_id = $_POST['serviceId'];
                    $status = $_POST['status'];

                    //get the service object
                    $service_obj = new Service($service_id);

                    $result['success'] = $service_obj->setSMPPDLREndpointStatus($status);
                    break;
                case "toggle_http_mosms_status" :
                    $service_id = $_POST['serviceId'];
                    $status = $_POST['status'];

                    //get the service object
                    $service_obj = new Service($service_id);

                    $result['success'] = $service_obj->setHTTPMOSMSEndpointStatus($status);
                    break;
                case "toggle_smpp_mosms_status" :
                    $service_id = $_POST['serviceId'];
                    $status = $_POST['status'];

                    //get the service object
                    $service_obj = new Service($service_id);

                    $result['success'] = $service_obj->setSMPPMOSMSEndpointStatus($status);
                    break;
                case "rename_service" :
                    $service_id = $_POST['serviceId'];
                    $service_name = $_POST['fieldValue'];

                    if($service_helper->isServiceNameUnique($service_name, $service_id))
                    {
                        $result['success'] = $service_helper->saveServiceFieldValue($service_id, "service_name", $service_name);
                    }
                    else
                    {
                        $result['success'] = false;
                        $result['reason'] = "Service name already exists.";
                    }

                    break;
                case "get_route_countries" :
                    $service_id = $_POST['serviceId'];

                    $route_helper = new sqlHelperRoutes();
                    $result['countries'] = $route_helper->getAllRouteCountries();
                    $result['success'] = true;
                    break;
                case "get_route_networks" :
                    $service_id = $_POST['serviceId'];
                    $country_name = $_POST['countryName'];
                    $exclude_exists = $_POST['excludeExists'];

                    $route_helper = new sqlHelperRoutes();

                    $result['networks'] = $route_helper->getAllRouteNetworksByCountry($country_name, $service_id);


                    $result['success'] = true;
                    break;
                case "get_route_collections" :
                    $service_id = $_POST['serviceId'];
                    $country_name = $_POST['countryName'];
                    $network_mccmnc = $_POST['networkMCCMNC'];

                    $route_helper = new sqlHelperRoutes();
                    if($country_name === "South Africa")
                    {
                        $result['collections'] = $route_helper->getAllRouteCollectionsRSA($network_mccmnc);
                    }
                    else
                    {
                        $result['collections'] = $route_helper->getAllRouteCollectionsInternational($network_mccmnc);
                    }

                    $result['success'] = true;
                    break;

                case "save_service_route" :
                    $service_id = $_POST['serviceId'];
                    $route_id = $_POST['routeId'];

                    $country_name = $_POST['countryName'];
                    $network_mccmnc = $_POST['networkMCCMNC'];
                    $collection_id = $_POST['collectionId'];
                    $status = $_POST['status'];
                    $currency = $_POST['currency'];
                    $rate = $_POST['rate'];
                    $rdnc = $_POST['rdnc'];

                    $route_obj = new ServiceRoute($route_id);
                    $route_obj->setRouteDataFromForm($service_id, $country_name, $network_mccmnc, $collection_id, $rate, $rdnc, $currency, $status);
                    $result['success'] = $route_obj->save();

                    break;

                case "delete_service_route" :
                    $service_id = $_POST['serviceId'];
                    $route_id = $_POST['routeId'];

                    $route_obj = new ServiceRoute($route_id);
                    $result['success'] = $route_obj->delete();

                    break;

                case "add_user_to_service" :
                    $service_id = $_POST['serviceId'];
                    $user_id = $_POST['userId'];

                    if($service_helper->checkUserHasAccessToService($service_id, $user_id) === false)
                    {
                        $permission_type = "submit.mtsms";
                        $result['success'] = $service_helper->addUserPermissionToService($service_id, $user_id, $permission_type);
                    }
                    else
                    {
                        $result['success'] = false;
                        $result['reason'] = "user_exists";
                    }
                    break;
                case "remove_user_from_service" :
                    $service_id = $_POST['serviceId'];
                    $user_id = $_POST['userId'];

                    if($service_helper->checkUserHasAccessToService($service_id, $user_id) === true)
                    {
                        $result['success'] = $service_helper->removeUserPermissionFromService($service_id, $user_id);
                    }
                    else
                    {
                        $result['success'] = false;
                        $result['reason'] = "no_permissions_exist";
                    }
                    break;
                case "create_new_service" :
                    $account_id = $_POST['accountId'];
                    $account_name = $_POST['accountName'];

                    $service_name = $_POST['serviceName'];
                    $service_login = $_POST['serviceLogin'];
                    $service_billing_type = $_POST['serviceBillingType'];
                    $service_manager_id = $_POST['serviceManagerId'];

                    $accounts_helper = new sqlHelperAccounts();
                    $users_helper = new sqlHelperUsers();

                    $input_error_array = array();
                    $input_valid = true; //assume valid until proven otherwise


                    //first check if the service name already exists
                    if($account_id == -1)
                    {
                        //do some validation first
                        if (!$accounts_helper->isAccountNameUnique($account_name))
                        {
                            $input_error_array['accountName'] = "The account name you entered already exists.";
                            $input_valid = false;
                        }
                        else
                        {
                            //create new account quickly
                            $account_id = Account::createNewAccountAdmin($account_name);
                        }
                    }

                    //validate the service info
                    if(!$service_helper->isServiceNameUnique($service_name))
                    {
                        $input_error_array['serviceName'] = "The service name you entered already exists, please enter a different one.";
                        $input_valid = false;
                    }


                    if(!$users_helper->isUsernameUnique($service_login))
                    {
                        $input_error_array['serviceLogin'] = "The user already exists with this login name, please enter a different one.";
                        $input_valid = false;
                    }


                    if($account_id && $input_valid)
                    {
                        $new_service_id = Service::createNewServiceAdmin($account_id, $service_name, $service_login, $service_billing_type, $service_manager_id);

                        if($new_service_id)
                        {
                            $result['success'] = true;
                            $result['service_id'] = $new_service_id;
                        }
                        else
                        {
                            $result['success'] = false;
                            $result['reason'] = "service_creation_failed";
                        }
                    }
                    else
                    {
                        $result['success'] = false;
                        $result['reason'] = "validation_failed";
                        $result['form_errors'] = $input_error_array;
                    }

                    break;
            }
        }
        else
        {
            $result = array('success' => false);
        }
    }
    catch(Exception $e)
    {
        $result = array('success' => false, 'message' => $e->getMessage());
    }

    //json encode the array before returning so it can be parsed by the JS
    echo json_encode($result);

