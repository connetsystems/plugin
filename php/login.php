<?php
//required to connect to the DB
require_once('php/config/dbConn.php');
require_once('php/helperPassword.php');

if( isset($_POST['check']) )
{
    //Unset the session to clear previous data stored in it.
    session_unset();

    //make sure the user actually entered something
    if(isset($_POST['un']) && !empty($_POST['pw']))
    {
        //for login status tracking
        $loginSuccess = false;
        $showTutorial = false;

        $err = "1";


        /*
        * NEW! SQL search for prefix URL to get resources for an account prefix
        */
        $conn = connToMasterCore();

        //prepare the select query so no one can insert some pesky SQL through the URL
        if(strpos($_POST['un'],'connet') !== false)
        {
            $stmt = $conn->prepare("SELECT user_id, user_username, user_password_hashed, user_account_id, user_default_service_id, user_system_admin, service_id_reseller, service_name, header_logo, tab_logo, account_name, account_colour, 'ENABLED' as account_status, 'ENABLED' as service_status, user_status
                    FROM core.core_user u
                    LEFT JOIN core.core_service s
                    ON u.user_default_service_id = s.service_id
                    LEFT JOIN core.core_account a
                    ON u.user_account_id = a.account_id
                    LEFT JOIN core.core_account_settings t
                    ON u.user_account_id = t.account_id
                    WHERE user_username = ?");
        }
        else
        {
            $stmt = $conn->prepare("SELECT user_id, user_username, user_password_hashed, user_account_id, user_default_service_id, user_system_admin, ifnull(service_id_reseller,0) as 'service_id_reseller', service_name, login_logo, header_logo, tab_logo, account_name, account_colour, service_credit_available, service_credit_billing_type, account_status, service_status, user_status
                    FROM core.core_user u
                    LEFT JOIN core.core_service s
                    ON u.user_default_service_id = s.service_id
                    LEFT JOIN core.core_account a
                    ON u.user_account_id = a.account_id
                    LEFT JOIN core.core_account_settings t
                    ON u.user_account_id = t.account_id
                    INNER JOIN core.core_service_credit c
                    ON s.service_id = c.service_id
                    WHERE user_username = ?");
        }

        //check if the statement was successfully init'd and die if it wasn't
        if(!$stmt)
        {
            header("Location: /index.php?err=1");
        }

        //set the username in the statement
        $stmt->bind_param('s', $_POST['un']);

        //run the statement
        $stmt->execute();

        //get the result set if it exists
        $result = $stmt->get_result();

        //die if no result was given, means the username is wrong or doesn't exist
        if(!$result)
        {
            //NB need better output than this
            header("Location: /index.php?err=1");
            return;
        }

        //get the username row and compare it to the password
        while($row = $result->fetch_assoc())
        {
            //compare the passwords
            if (helperPassword::validatePassword($_POST['pw'], $row['user_password_hashed']))
            {
                //check if the password requires rehashing
                if (helperPassword::checkNeedsRehash($row['user_password_hashed'])) {
                    // If so, create a new hash, and replace the old one
                    $hashed_password = helperPassword::createHashedPassword($_POST['pw']);

                    //prepare the data
                    $data_update_password = array(':user_id' => $row['user_id'], ':hashed_password' => $hashed_password);

                    //prepare the query
                    $stmt_update_password = $conn->prepare("UPDATE core_user
                        SET user_password_hashed = :hashed_password
                        WHERE user_id = :user_id ");

                    //execute it
                    $stmt_update_password->execute($data_update_password);
                }

                //excellent, lets do some extra checks on this account to see that all the levels of the account are enabled
                if ($row['account_status'] === 'ENABLED')
                {
                    if ($row['service_status'] === 'ENABLED')
                    {
                        if ($row['user_status'] === 'ENABLED')
                        {
                            //echo "enabled...";
                            //everything passed, let's save the user's details in the session for later use
                            $_SESSION['userId'] = $row['user_id'];
                            $_SESSION['userName'] = $row['user_username'];
                            $_SESSION['serviceName'] = $row['service_name'];
                            $_SESSION['accountName'] = $row['account_name'];
                            $_SESSION['accountId'] = $row['user_account_id'];
                            $_SESSION['serviceCredit'] = $row['service_credit_available'];
                            $_SESSION['serviceBillingType'] = $row['service_credit_billing_type'];
                            $_SESSION['randomSendCheck'] = rand(0, 1000);
                            $_SESSION['userSystemAdmin'] = $row['user_system_admin'];

                            //all is good, the login was successful
                            $loginSuccess = true;

                            //set the colour scheme for the user if it is configured
                            if (isset($row['account_colour']) && $row['account_colour'] != "")
                            {
                                $_SESSION['accountColour'] = $row['account_colour'];
                                // $accRGB = $_SESSION['accountColour'];
                            }
                            else
                            {
                                $_SESSION['accountColour'] = '#cc3322';
                            }

                            //set the login logo for the user if it is configured
                            if (isset($row['login_logo']) && $row['login_logo'] != "")
                            {
                                $_SESSION['loginLogo'] = $row['login_logo'];
                                //$headerLogo = $_SESSION['headerLogo'];
                            }
                            else
                            {
                                $_SESSION['loginLogo'] = '../img/skinlogos/1/1.png';
                                //$headerLogo = '../img/skinlogos/default/ConnetHeader.png';
                            }

                            //set the header logo for the user if it is configured
                            if (isset($row['header_logo']) && $row['header_logo'] != "")
                            {
                                $_SESSION['headerLogo'] = $row['header_logo'];
                                //$headerLogo = $_SESSION['headerLogo'];
                            }
                            else
                            {
                                $_SESSION['headerLogo'] = '../img/skinlogos/1/1_header.png';
                                //$headerLogo = '../img/skinlogos/default/ConnetHeader.png';
                            }

                            //set the tab logo for the user if it is configured
                            if (isset($row['tab_logo']) && $row['tab_logo'] != "")
                            {
                                $_SESSION['tabLogo'] = $row['tab_logo'];
                                //$tabLogo = $_SESSION['tabLogo'];
                            }
                            else
                            {
                                $_SESSION['tabLogo'] = '../img/skinlogos/1/1_icon.png';
                                //$tabLogo = '../img/skinlogos/default/ConnetIcon.png';
                            }

                            //does the user have an associated account ID
                            if (isset($row['user_account_id']))
                            {
                                $_SESSION['accountId'] = $row['user_account_id'];
                            }
                            else
                            {
                                header("Location: /index.php?err=2");
                            }

                            //set the users service ID
                            if (isset($row['user_default_service_id']))
                            {
                                $_SESSION['serviceId'] = $row['user_default_service_id'];
                            }
                            else
                            {
                                if ($row['user_account_id'] == 1)
                                {
                                    $_SESSION['serviceId'] = '';
                                }
                                else
                                {
                                    header("Location: /index.php?err=3");
                                }
                                $_SESSION['serviceId'] = '';
                            }

                            //set the reseller ID if applicable
                            if (isset($row['service_id_reseller']))
                            {
                                $_SESSION['resellerId'] = $row['service_id_reseller'];
                            }
                            else
                            {
                                $_SESSION['resellerId'] = '';
                            }


                            $stmt_role = $conn->prepare("SELECT r.role_id, role_name, role_url, role_icon
                                    FROM core.core_user_role r
                                    INNER JOIN core.core_role rl
                                    ON r.role_id = rl.role_id
                                    WHERE user_id = ?
                                    ORDER BY role_order,role_id");

                            if($stmt_role)
                            {
                                $stmt_role->bind_param('i', $_SESSION['userId']);
                                $stmt_role->execute();
                                //get the result set if it exists
                                $roleTmp = $stmt_role->get_result();
                                $stmt_role->fetch();
                                $stmt_role->close();
                            }

                            $rol;
                            $i = 0;
                            foreach ($roleTmp as $key => $value)
                            {
                                if ($value['role_id'] == '1')
                                {
                                    $_SESSION['settings'] = 1;
                                }
                                else
                                {
                                    $rol[$i] = $value;
                                    $i++;
                                }
                            }

                            ($rol);

                            $_SESSION['roles'] = $rol;

                            /*
                             * We just want to check if this user is a new user from the free credits initiative, and show them the tutorial
                             */

                            if ($_SESSION['accountId'] == 650)
                            {
                                //lets run an SQL query to find out if the user has seen the tutorial
                                $tutorialSeen = 0;
                                $stmt_tut = $conn->prepare("SELECT tutorial_seen FROM core.account_details WHERE core_user_id = ?");
                                if($stmt_tut)
                                {
                                    $stmt_tut->bind_param('i', $_SESSION['userId']);
                                    $stmt_tut->execute();
                                    $stmt_tut->bind_result($tutorialSeen);
                                    $stmt_tut->fetch();
                                    $stmt_tut->close();
                                }

                                /*echo '<pre>';
                                    print_r($result);
                                echo '</pre>';*/

                                //check if it was seen
                                if (isset($tutorialSeen) && $tutorialSeen != 1)
                                {
                                    //hasn't been seen, so we default to the tutorial page instead of the dashboard page for this run
                                    $showTutorial = true;

                                    //just need to update the DB to say that the user has indeed seen this tutorial
                                    $stmt_update_tut = $conn->prepare("UPDATE core.account_details SET tutorial_seen = 1 WHERE core_user_id = ?");
                                    $stmt_update_tut->bind_param('i', $_SESSION['userId']);
                                    $stmt_update_tut->execute();
                                    $stmt_update_tut->close();
                                }
                            }


                        }
                        else
                        {
                            $err = "use";
                        }
                    }
                    else
                    {
                        $err = "ser";
                    }
                }
                else
                {
                    $err = "acc&v=" . $row['account_status'] . "";
                }
            }
            else
            {
                $err = "1";
            }
        }

        $stmt->close();
        $conn->close();

        //was the login successful? We need to reroute
        if($loginSuccess)
        {
            //check which home we need to show
            if ($_SESSION['resellerId'] == '')
            {
                if ($showTutorial)
                {
                    //show the tutorial for first time free credits users
                    header("Location: /pages/tutorial.php");
                }
                else
                {
                    //this is the admin dashboard
                    header("Location: /home.php");
                }
            }
            else
            {
                if ($showTutorial)
                {
                    //show the tutorial for first time free credits users
                    header("Location: /pages/tutorial.php");
                }
                else if ($_SESSION['serviceId'] == $_SESSION['resellerId'])
                {
                    //head to reseller home if the user is a reseller
                    header("Location: /resellerhome.php");
                }
                else
                {
                    //go to the client home
                    header("Location: /clienthome.php");
                }
            }
        }
        else
        {
            //login failed!!! Show the error
            header("Location: /index.php?err=".$err);
        }
    }
    elseif(empty($_POST['pw']))
    {
       header("Location: /index.php?err=1");
    }

}


