<?php

require_once('config/dbConn.php');

if (isset($_POST['action'])) {
   //require_once("allInclusive.php");
   //echo 'Test='.$_POST['action'];
   //include("allInclusive.php");
   $startDate = mktime(0, 0, 0);
   $startDateConv = date('Y-m-d', $startDate);

   $mosTotals = getMOForDashConnet($startDateConv);

   //echo $startDateConv;
   echo $mosTotals;
   //echo number_format($sents, 0, '', ' ');
}

/*
  function getMOForDashConnet($sD)
  {
  $qry = "SELECT count(mosms_id) as id
  FROM core.core_mosms
  WHERE DATE(`timestamp`) = '".$sD."' ";

  $MOs = selectFromDB($qry, 'core');

  foreach ($MOs as $key => $value)
  {
  $MoTot = $value['id'];
  }

  return $MoTot;
  }
 */

function getMOForDashConnet($sD) {
   //open the DB object
   $conn = getPDOReportingCore();

   //prepare the data
   $data = array(':sD' => $sD);

   //prepare the query
   $stmt = $conn->prepare("SELECT count(mosms_id) as total
            FROM core.core_mosms
            WHERE DATE(`timestamp`) = :sD ");

   //set the fetch mode
   $stmt->setFetchMode(PDO::FETCH_ASSOC);

   //execute it
   $stmt->execute($data);

   //run the query
   $row = $stmt->fetch();

   return $row['total'];
}

?>