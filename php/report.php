<?php

//-----------------------------------------------
// Init
//-----------------------------------------------
ob_start();
define('ERROR_REPORTING', false);
define('ERROR_REPORTING', true);
//define('MYSQL_CONNECTION_STRING', '10.3.0.42;core;youyaiN3kooduwi;core'); //masterdb
define('MYSQL_CONNECTION_STRING', '10.3.0.41;core;youyaiN3kooduwi;core'); //waspreportdb1

require_once('bootstrap/default.bootstrap.php');
preset::plain();

//-----------------------------------------------
// Delivery Report Mask
//-----------------------------------------------
define('DLR_DELIVERED', 1); //DELIVERED: The SMS has been delivered to the subscriber.
define('DLR_FAILED', 2); //FAILED: Delivery of the SMS has failed.
define('DLR_QUEUED', 4); //QUEUED: The SMS has been queued for delivery.
define('DLR_SMSC_SUBMIT', 8); //SENT: The SMS has been sent, but not yet delivered
define('DLR_SMSC_REJECTED', 16); //REJECTED: The SMS has been rejected by the SMSC
define('DLR_CUSTOM_ACCEPTED', 32); //ACCEPTED: The SMS has been accepted by the gateway.
define('DLR_CUSTOM_REJECTED', 64); //REJECTED2: The SMS has been rejected by the gateway.
define('DLR_GATEWAY_ROUTED', 128); //GATEWAY_ROUTED: Routable
define('DLR_GATEWAY_NOT_ROUTABLE', 256); //GATEWAY_NOT_ROUTABLE: Failed to route message
define('DLR_GATEWAY_REMOTE_ACCEPTED', 512); //GATEWAY_REMOTE_ACCEPTED: Accepted for sending by remote gateway / smsc
define('DLR_GATEWAY_REMOTE_REJECTED', 1024); //GATEWAY_REMOTE_REJECTED: REJECTED by remote gateway / smsc
define('DLR_GATEWAY_REMOTE_ERROR', 2048); //DLR_GATEWAY_REMOTE_ERROR: Remote Gateway Error
define('DLR_CUSTOM_DNC_REJECTED', 4096); //DLR_CUSTOM_DNC_REJECTED: Exclusion list public
define('DLR_CUSTOM_DNC_REJECTED_PRIVATE', 8192); //DLR_CUSTOM_DNC_REJECTED_PRIVATE: Exclusion list private
//-----------------------------------------------
// Query
//-----------------------------------------------
$mysql = MySQLConnection::getInstance(MYSQL_CONNECTION_STRING);

$service_id = isset($_GET['sid']) ? (integer) $_GET['sid'] : 450;

$sql = <<<SQLGOESHERE
(
  
    

  select 'mt' as `type`, 
  0 as id, 
  date(timestamp) as `timestamp`, 
  SUM(billing_units) as `billing_units`, 
  dlr, 
  '' as `meta`,
  substring_index(SUBSTRING_INDEX(meta, 'smsc=', -1),'&',1) as SMSC,
  substring_index(SUBSTRING_INDEX(meta, 'mccmnc=', -1),'&',1) as MCCMNC    
    FROM core.core_mtsms
    WHERE mtsms_id > (SELECT mtsms_id FROM core.core_mtsms WHERE DATE(`timestamp`) >= DATE(DATE_SUB(NOW(), INTERVAL 5 DAY)) LIMIT 1)
    AND service_id = '450'
    group by date(timestamp), dlr, substring_index(SUBSTRING_INDEX(meta, 'smsc=', -1),'&',1),substring_index(SUBSTRING_INDEX(meta, 'mccmnc=', -1),'&',1)
)
               
SQLGOESHERE;




$start = microtime(true);

$stats = array();

$resource = $mysql->query($sql);


clog('[Query] Elapsed:', number_format(microtime(true) - $start, 3));
$start2 = microtime(true);

$units = 0;
while ($row = mysql_fetch_array($resource, MYSQL_ASSOC)) {


    parse_str($row['meta'], $row['meta']);
    $smsc = isset($row['meta']['smsc']) ? $row['meta']['smsc'] : 'unknown';
    $smsc1 = isset($row['SMSC']) ? $row['SMSC'] : 'SMSC';
    $mccmnc = isset($row['meta']['mccmnc']) ? $row['meta']['mccmnc'] : 'unknown';
    $mccmnc1 = isset($row['MCCMNC']) ? $row['MCCMNC'] : 'MCCMNC';
    $date = date('Y-m-d', strtotime($row['timestamp']));
    $units1 = isset($row['billing_units']) ? $row['billing_units'] : 'billing_units';

    if ($row['type'] == 'mo') {

        $type = 'MO';
    } else {

        $dlr = (integer) $row['dlr'];



        static $order = array(
            DLR_DELIVERED => 'DELIVERED',
            DLR_FAILED => 'FAILED',
            DLR_CUSTOM_DNC_REJECTED => 'FAILED-E',
            DLR_CUSTOM_DNC_REJECTED_PRIVATE => 'FAILED-E2',
            DLR_GATEWAY_NOT_ROUTABLE => 'NOT_ROUTABLE',
            DLR_GATEWAY_REMOTE_REJECTED => 'REMOTE_REJECTED',
            DLR_SMSC_REJECTED => 'SMSC_REJECTED',
            DLR_CUSTOM_DNC_REJECTED => 'FAILED',
            DLR_CUSTOM_REJECTED => 'REJECTED',
            DLR_GATEWAY_REMOTE_ERROR => 'REMOTE_ERROR',
            DLR_SMSC_SUBMIT => 'SUBMIT',
            DLR_GATEWAY_ROUTED => 'ROUTED',
            DLR_QUEUED => 'QUEUED',
            DLR_GATEWAY_REMOTE_ACCEPTED => 'REMOTE_ACCEPTED',
            DLR_CUSTOM_ACCEPTED => 'ACCEPTED',
        );

        $type = 'UNKNOWN';
        foreach ($order as $mask => $str) {

            if (((integer) $dlr & (integer) $mask) == (integer) $mask) {

                switch ($mask) {

                    case DLR_DELIVERED: $type = 'DELIVERED';
                        break;
                    case DLR_FAILED: $type = 'FAILED';
                        break;
                    case DLR_CUSTOM_DNC_REJECTED:
                    case DLR_CUSTOM_DNC_REJECTED_PRIVATE:
                        $type = 'EXCLUDED';
                        break;
                    case DLR_GATEWAY_NOT_ROUTABLE:
                    case DLR_GATEWAY_REMOTE_REJECTED:
                    case DLR_SMSC_REJECTED:
                    case DLR_CUSTOM_DNC_REJECTED:
                    case DLR_CUSTOM_REJECTED:
                    case DLR_GATEWAY_REMOTE_ERROR:
                        $type = 'REJECTED';
                        break;

                    default:

                        $type = 'SUBMIT';
                }
                break;
            }
        }
    }


    $units += $row['billing_units'];

    if (1) 
    {
        $key = $date . '|' . $smsc . '|' . $mccmnc . '|' . 'TOTAL';
        if (!isset($stats[$key])) 
        {

            $stats[$key] = array(
                'mccmnc' => $mccmnc,
                'mccmnc1' => $mccmnc1,
                'smsc' => $smsc,
                'smsc1' => $smsc1,
                'date' => $date,
                'type' => $type, 
                'units' => $row['billing_units'],
                'units1' => $units1,
            );
        } 
        else 
        {
            $stats[$key]['units'] += $row['billing_units'];
        }

        /*$key = $date . '|' . $smsc . '|' . $mccmnc . '|' . $type;
        if (!isset($stats[$key])) 
        {

            $stats[$key] = array(
                'mccmnc' => $mccmnc,
                'mccmnc2' => $mccmnc1,
                'smsc' => $smsc,
                'smsc2' => $smsc1,
                'date' => $date,
                'type' => $type,
                'units' => $row['billing_units'],
            );
        } else 
        {
            $stats[$key]['units'] += $row['billing_units'];
        }*/
    } else {
        if (!isset($stats[$smsc][$date][$type])) {

            $stats[$smsc][$date][$type] = $row['billing_units'];
        } else {
            $stats[$smsc][$date][$type] += $row['billing_units'];
        }
    }
}

ksort($stats);
clog('[Process] Elapsed:', number_format(microtime(true) - $start2, 3));


clog($stats);

if (!ERROR_REPORTING) {
    ob_clean();
    header('Content-Type: application/json');
}

die(json_encode(array(
    'query_secs' => (float) number_format($start2 - $start, 3),
    'fetch_secs' => (float) number_format(microtime(true) - $start2, 3),
    'service_id' => $service_id,
    'units' => $units,
    'data' => array_values($stats),
)));




//********************** end of file ******************************
