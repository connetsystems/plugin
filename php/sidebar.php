<?php
    //include("php/allInclusive.php");
?>
            <aside class="left-side sidebar-offcanvas">
                <!-- sidebar: style can be found in sidebar.less -->
                <section class="sidebar">
                    <ul class="sidebar-menu">
                        <li class="active">
                            <a href="/admin.php">
                                <i class="fa fa-home"></i> <span>Admin Home</span>
                            </a>
                        </li>
                        <li class="active">
                            <a href="/pages/clientrep.php">
                                <i class="fa fa-users"></i> <span>Client Report</span>
                            </a>
                        </li>
                        <li class="active">
                            <a href="/pages/netrep.php">
                                <i class="fa fa-signal"></i> <span>Network Report</span>
                            </a>
                        </li>
                        <li class="active">
                            <a href="/pages/monthcomp.php">
                                <i class="fa fa-line-chart"></i> <span>Daily Comparison</span>
                            </a>
                        </li>
                    </ul>
                </section>
                <!-- /.sidebar -->
            </aside>