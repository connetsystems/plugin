<?php

require_once('config/dbConn.php');


if ((isset($_POST["mccmnc"]) && $_POST["mccmnc"] != '') && (isset($_POST["sId"]) && $_POST["sId"] != ''))
{
    $conn = getPDOMasterCore();

    $data = array(':service_id' => $_POST["sId"], ':mccmnc' => $_POST["mccmnc"]);

    //query
    $query = 'SELECT route_billing, prefix_network_name, prefix_country_name, route_currency
            FROM (
            SELECT route_billing,route_mccmnc,route_currency
            FROM core.core_route
            WHERE service_id = :service_id
            AND route_mccmnc = :mccmnc
            ) AS ROUTE
            LEFT JOIN
            (SELECT prefix_mccmnc, prefix_network_name, prefix_country_name FROM core.core_prefix GROUP BY prefix_mccmnc) AS core_prefix
            ON prefix_mccmnc = route_mccmnc';

    //prepare the query
    $stmt = $conn->prepare($query);

    //set the fetch mode 
    $stmt->setFetchMode(PDO::FETCH_ASSOC);
    
    //execute it
    $stmt->execute($data);
    
    //fetch the data
    $mccmnc = $stmt->fetch();
    
    $conn = null;

    $mncStr = '';

    if (isset($mccmnc) && $mccmnc != '')
    {
        $mncStr = $mccmnc['prefix_country_name'] . ',' . $mccmnc['prefix_network_name'] . ',' . $mccmnc['route_currency'] . ',' . $mccmnc['route_billing'];
    }

    echo $mncStr;
}

//********************** end of file ******************************
?>