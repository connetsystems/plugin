<?php

require_once('config/dbConn.php');

function sendEmailTo($fn, $sn, $cn, $ce, $em, $lId) {
    $to = $em;
    $subject = "Registration at Connet";

    $msg = '<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w31.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
                <html>
                <head>
                    <title>' . $subject . '</title>
                </head>
                    <body style="font-family: Arial, sans-serif;    font-stretch: normal;   font-size: 12px;    font-style: normal; width:740px;">
                        <table border="0">
                            <tr>
                                <td>
                                    <img src="http://portal.connet-systems.com/img/Header.jpg"> 
                                </td>
                            </tr>   
                            <tr>
                                <td style="padding-left: 28px; padding-right: 28px;">

                                    <h1 style="color:#3c5e6e;font-size:21px;">Only one step away!</h1>

                                    <p>Your registration is almost complete.</p>
                                  
                                    <p style="color:#dd0000"><b>Click on the link below to send an OTP to your cellphone:</b></p>
                                    
                                    <a href="http://portal2.connet-systems.com/verify.php?lId=' . $lId . '">www.connet-systems.com</a></p>
                                    
                                    <p>If you have not received an OTP, please click on the link above to resend it to your cellphone.</p>
                                    
                                    <p><br>Kind Regards,</p>

                                    <p><strong>The Connet Systems Team</strong></p>

                                </td>
                            </tr>   
                            <tr>
                                <td>    
                                    <img style="margin-bottom:-100px;" src="http://portal.connet-systems.com/img/Footer.jpg"> 
                                </td>
                            </tr>   
                        </table>
                    </body>
                </html>';

    $msg = wordwrap($msg, 70);

    $final_msg = preparehtmlmail($msg); // give a function your html*

    $m = mail($to, $subject, $final_msg['multipart'], $final_msg['headers']);

    if ($m == 1) {
        return 1;
    }
}

function preparehtmlmail($html) {

    define("DEFCALLBACKMAIL", "customer@connet-systems.com"); // WIll be shown as "from".
    $headers = '';
    //clog::info('Beginning HTML mail preparation');

    preg_match_all('~<img.*?src=.([\/.a-z0-9:_-]+).*?>~si', $html, $matches);
    $i = 0;
    $paths = array();

    foreach ($matches[1] as $img) {
        $img_old = $img;

        if (strpos($img, "http://") == false) {
            $uri = parse_url($img);
            $paths[$i]['path'] = $_SERVER['DOCUMENT_ROOT'] . $uri['path'];
            $content_id = md5($img);
            $html = str_replace($img_old, 'cid:' . $content_id, $html);
            $paths[$i++]['cid'] = $content_id;
        }
    }

    //clog::info('Processing HTML');

    $boundary = "--" . md5(uniqid(time()));
    $headers .= "MIME-Version: 1.0\n";
    $headers .="Content-Type: multipart/mixed; boundary=\"$boundary\"\n";
    $headers .= "From: " . DEFCALLBACKMAIL . "\r\n";
    //$headers .= 'Cc: customer@connet-systems.com'."\r\n";
    $multipart = '';
    $multipart .= "--$boundary\n";
    $kod = 'utf-8';
    $multipart .= "Content-Type: text/html; charset=$kod\n";
    $multipart .= "Content-Transfer-Encoding: Quot-Printed\n\n";
    $multipart .= "$html\n\n";

    foreach ($paths as $path) {
        //clog::info('Processing HTML IMAGES');
        if (file_exists($path['path']))
            $fp = fopen($path['path'], "r");
        if (!$fp) {
            echo "No file";
            return false;
        }

        $imagetype = substr(strrchr($path['path'], '.'), 1);
        $file = fread($fp, filesize($path['path']));
        fclose($fp);

        $message_part = "";

        switch ($imagetype) {
            case 'png':
            case 'PNG':
                $message_part .= "Content-Type: image/png";
                break;
            case 'jpg':
            case 'jpeg':
            case 'JPG':
            case 'JPEG':
                $message_part .= "Content-Type: image/jpeg";
                break;
            case 'gif':
            case 'GIF':
                $message_part .= "Content-Type: image/gif";
                break;
        }

        /*      echo '<pre>';
          print_r($path);
          echo '</pre>';
         */
        $message_part .= "; file_name = \"$path[path]\"\n";
        $message_part .= 'Content-ID: <' . $path['cid'] . ">\n";
        $message_part .= "Content-Transfer-Encoding: base64\n";
        $message_part .= "Content-Disposition: inline; filename = \"" . basename($path['path']) . "\"\n\n";
        $message_part .= chunk_split(base64_encode($file)) . "\n";
        $multipart .= "--$boundary\n" . $message_part . "\n";
        /*
          echo '<br>---'.$path['cid'];
          echo '<br>---'.$path['cid'][0]; */
    }

    $multipart .= "--$boundary--\n";
    $multipart = wordwrap($multipart, 70);

    //clog::info('Done html email');
    return array('multipart' => $multipart, 'headers' => $headers);
}

function sendOTP($ce, $otp) {
    $success = 1;
    $type = 'single';
    $user = 'national.connet';
    $pass = 'nah3ut4E';

    $smsText = urlencode("You are about to create a bulk SMS account with Connet Systems.  Your OTP is: $otp. Enter the OTP on the webpage provided.");

    $gu = trim(guid(), '{}');
    $gu = str_replace('-', '', $gu);

    $smsUrl = "http://sms.connet-systems.com/submit/" . $type . "/?username=" . $user . "&password=" . $pass . "&account=" . $user . "&da=" . $ce . "&ud=" . $smsText . "&id=" . $gu;
    //echo '<br><br><br><br>url='.$smsUrl;
    $statusOfSMS = get_http_response_code($smsUrl);
    if ($statusOfSMS == "401") {
        $success = 2;
    } elseif ($statusOfSMS == "202") {
        $success = 0;
        //sentOTP();
    } elseif ((strpos($statusOfSMS, '700') !== false) || (strpos($statusOfSMS, 'Accepted') !== false)) {
        $success = 0;
    } elseif ($statusOfSMS == "200") {
        $success = 0;
        //sentOTP();
    } else {
        $success = 3;
    }

    //echo "<br>1string=".$statusOfSMS;
    //echo "<br>2string=".$success;
    return $success;
}

//getOtpCount($adId)

/* function sentOTP()
  {
  $sql = "INSERT INTO core.account_details (firstname, lastname, companyname, cellnumber, emailaddress, waspacode, connetterm, newsletter, password, otp) VALUES ('".$fn."', '".$sn."', '".$cn."', '".$ce."', '".$em."', ".$wa.", ".$ca.", ".$su.", ".$pw.", ".$otp.")";
  $result = mysqli_query($con, $sql);
  //echo '<br>->'.$sql.'<-<br>';
  //echo '<br>->'.$result.'<-<br>';
  if($result == 1)
  {

  }
  } */

function checkOTPSent($adId) {

    //get db ref object
    $conn = getPDOReportingCore();

//    $qry = "SELECT otp_sent
//                FROM core.account_details
//                WHERE account_details_id = " . $adId . ";";

    $vars = array(':acc_det_id' => $adId);

    $query = "SELECT otp_sent
                FROM core.account_details
                WHERE account_details_id = :acc_det_id";

    //prepare the query
    $stmt = $conn->prepare($query);

    //set the fetch mode
    $stmt->setFetchMode(PDO::FETCH_ASSOC);

    //execute query
    $stmt->execute($vars);

    //fetch results
    $data = $stmt->fetchAll();

    $conn = null;

    $otpC = '';
    if (isset($data) && $data != '') {
        foreach ($data as $key => $value) {
            $otpC = $value['otp_sent'];
        }
        return $otpC;
    } else {
        return 1;
    }
}

function checkValidatedOTP($adId) {

    //get db ref object
    $conn = getPDOReportingCore();

    $vars = array(':acc_det_id' => $adId);

    $query = 'SELECT verified
                FROM core.account_details
                WHERE account_details_id =:acc_det_id ';

    //prepare statement
    $stmt = $conn->prepare($query);

    //set fetch mode 
    $stmt->setFetchMode(PDO::FETCH_ASSOC);

    //execute query
    $stmt->execute($vars);

    //fetch results
    $data = $stmt->fetchAll();

    $conn = null;

    foreach ($data as $key => $value) {
        /* echo '<pre>';
          print_r($value);
          echo '</pre>'; */
        if ($value['verified'] == 1) {
            return 1;
        } else {
            return 0;
        }
    }
}

function verifyNewOtp($otp, $adId) {

    $conn = getPDOReportingCore();

    $vars = array(':acc_det_id' => $adId);

    $query = 'SELECT otp, otp_date 
                FROM core.account_details
                WHERE account_details_id = :acc_det_id ';

    //prepare statement
    $stmt = $conn->prepare($query);

    //set fetch mode 
    $stmt->setFetchMode(PDO::FETCH_ASSOC);

    //execute query
    $stmt->execute($vars);

    //fetch results
    $data = $stmt->fetchAll();

    $conn = null;

    if ($data->num_rows > 0) {
        foreach ($data as $key => $value) {
            if ($otp == $value['otp']) {
                $regTime = (strtotime($value['otp_date']) + (2 * 60 * 60));
                $otCheck = (((time() + (2 * 60 * 60)) - $regTime) / 3600);
                if ($otCheck < 1) {
                    return 1;
                } else {
                    return 0;
                }
            } else {
                return -1;
            }
        }
    } else {
        return -2;
    }
}

function getCellNum($adId) {

    $conn = getPDOReportingCore();

    $vars = array(':acc_det_id' => $adId);

    $query = 'SELECT cellnumber 
                FROM core.account_details
                WHERE account_details_id =  :acc_det_id ';

    //prepare statement
    $stmt = $conn->prepare($query);

    //set fetch mode 
    $stmt->setFetchMode(PDO::FETCH_ASSOC);

    //execute query
    $stmt->execute($vars);

    //fetch results
    $data = $stmt->fetchAll();

    $cellnumber = '';

    $conn = null;

    foreach ($data as $key => $value) {
        $cellnumber = $value['cellnumber'];
    }

    return $cellnumber;
}

function verifyAccount($adId) {

    $conn = getPDOMasterCore();

    $vars = array(':acc_det_id' => $adId);

    $query = 'UPDATE core.account_details 
                SET verified = 1 
                WHERE account_details_id =:acc_det_id ';

    //prepare the statement
    $stmt = $conn->prepare($query);

    //set the fetch mode 
    $stmt->setFetchMode(PDO::FETCH_ASSOC);

    //execute query
    $stmt->execute($vars);

    //fetch results
    $data = $stmt->fetchAll();

    $conn = null;

    return $data;
}

function checkVerified($adId) {

    $conn = getPDOReportingCore();
    
    $vars = array(':acc_det_id' => $adId);

    $qry = "SELECT verified 
                FROM core.account_details
                WHERE account_details_id = :acc_det_id ";

    //prepare statement
    $stmt = $conn->prepare($qry);

    //set the fetch mode
    $stmt->setFetchMode(PDO::FETCH_ASSOC);

    //execute query
    $stmt->execute($vars);

    //fetch results
    $data = $stmt->fetchAll();

    $conn = null;

    $ver = '';

    foreach ($data as $key => $value) {
        $ver = $value['verified'];
    }

    return $ver;
}

function checkServiceName($sN) {
    //echo '<br><br>looking for:'.$sN;]

    $conn = getPDOReportingCore();

    $vars = array(':serv_name' => $sN);

    $qry = "SELECT service_name 
                FROM core.core_service
                WHERE service_name =:serv_name";

    //prepare the query
    $stmt = $conn->prepare($qry);

    //set the fetch mode
    $stmt->setFetchMode(PDO::FETCH_ASSOC);

    //execute it
    $stmt->execute($vars);

    //fetch the data
    $serN = $stmt->fetch();

    $conn = null;

    //echo '<br><br>rows='.$serN->num_rows;
    if ($serN->num_rows != 0) {
        //echo '<br><br>2nd=return';
        $digits = 3;
        $rand = rand(pow(10, $digits - 1), pow(10, $digits) - 1);
        $sN = $sN . $rand;
        //echo '<br><br>newname='.$sN;
        return checkServiceName($sN);
    } else {
        //echo '<br><br>1st return='.$sN;
        return $sN;
    }
}

function creatingNewAcc($adId) {

    //database ref objects
    $conn = getPDOReportingCore();

    $vars = array(':acc_det_id' => $adId);

    $qry = "SELECT companyname, cellnumber, emailaddress, firstname, lastname, password 
                FROM core.account_details
                WHERE account_details_id = :acc_det_id ";

    //prepare the query
    $stmt = $conn->prepare($qry);

    //set the fetch mode
    $stmt->setFetchMode(PDO::FETCH_ASSOC);

    //execute it
    $stmt->execute($vars);

    //fetch the data
    $data = $stmt->fetchAll();

    $conn = null;

    //echo '<br>q0='.$qry;

    foreach ($data as $key => $value) {
        $serviceName = $value['companyname'];
        $cellNum = $value['cellnumber'];
        $serviceLogin = $value['emailaddress'];
        $fName = $value['firstname'];
        $lName = $value['lastname'];
        $pass = $value['password'];
        $userFname = $fName . " " . $lName;
    }

    //get the service name
    $checkSn = checkServiceName($serviceName);

    $serviceName = $checkSn;

    //creates a new service and returns new id
    $newService = createService($serviceName, $cellNum);

    $newServiceId = $newService;

    //update account details 
    updateAccountDetails($newServiceId, $adId);

    //insert credits for newly created account
    insertServiceCredits($newServiceId, $serviceLogin, $cellNum);

    //creates user for account
    createUser($serviceLogin, $pass, $userFname, $newServiceId);

    //pulls user id
    $userId = getUserId($serviceLogin);

    //updates user and account details
    updateUserAndAccountDetails($userId, $adId);

    //adds clients
    addCampaignClient($newServiceId, $userId);

    $id = getClientId($newServiceId);

    //adds a new campaign
    addCampaign($id, $userId);

    //adds permissions 
    addPermissions($newServiceId, $userId);

    //adds user roles
    addUserRole($userId);

    //add routes
    addRoutes($newServiceId, $serviceLogin);
}

function get_http_response_code($smsUrl) {
    $headers = get_headers($smsUrl);
    //echo '<br>so1='.$headers[0];
    $headers = substr($headers[0], 9, 3);
    //echo '<br>so2='.$headers;

    return $headers;
}

function guid() {
    if (function_exists('com_create_guid')) {
        return com_create_guid();
    } else {
        mt_srand((double) microtime() * 10000); //optional for php 4.2.0 and up.
        $charid = strtoupper(md5(uniqid(rand(), true)));
        $hyphen = chr(45); // "-"
        $uuid = chr(123)// "{"
                . substr($charid, 0, 8) . $hyphen
                . substr($charid, 8, 4) . $hyphen
                . substr($charid, 12, 4) . $hyphen
                . substr($charid, 16, 4) . $hyphen
                . substr($charid, 20, 12)
                . chr(125); // "}"

        return $uuid;
    }
}

function createService($serviceName, $cellNum) {

    $con = getPDOMasterCore();

    $qryinsert = "INSERT INTO core.core_service (account_id, service_name, service_login, service_meta, service_manager_user_id)
                    VALUES (650, ?, ?, 'cm_slots=00-99', 1179) ";

    //prepare the query
    $stmt = $con->prepare($qryinsert);

    //bind the data
    $stmt->bind_param("ss", $serviceName, $cellNum);
    //execute query
    $stmt->execute();
    //get last insert id
    $newService = $con->lastInsertId();

    $con = null;

    return $newService;
}

function updateAccountDetails($serviceId, $accountDetailsId) {

    $conn = getPDOMasterCore();

    //$newServiceId = $newService;

    $updatedata = array(':new_service_id' => $serviceId, ':acc_det_id' => $accountDetailsId);

    $update_query = "UPDATE core.account_details SET `account_id` = 650, `service_id` =:new_service_id WHERE account_details_id =:acc_det_id ";

    //prepare statement
    $statement = $conn->prepare($update_query);

    //set the fetch mode
    //execute query
    $statement->execute($updatedata);

    $conn = null;
    //
}

function insertServiceCredits($newServiceId, $serviceLogin, $cellNum) {

    $conn = getPDOMasterCore();

    //$data = array(':service_id' => $newServiceId, ':service_login' => $serviceLogin, 'cell_no' => $cellNum);

    $qry = "INSERT INTO core.core_service_credit (service_id, service_credit_msg_type, service_credit_billing_type, service_credit_available, service_iso_id, service_notification_threshold, service_notification_email, service_notification_sms)
                        VALUES ( ? , 'MTSMS', 'PREPAID', '26000', '166', '10000', ? , ? ) ";

    //prepare statement 
    $stmt = $conn->prepare($qry);

    // prepare and bind
    $stmt->bind_param("sss", $newServiceId, $serviceLogin, $cellNum);

    //execute query
    $stmt->execute();

    //check inserted rows
    //$conn->lastInsertId();

    $conn = null;
}

function createUser($serviceLogin, $pass, $userFullName, $newServiceId) {

    $conn = getPDOMasterCore();

    $qry = 'INSERT INTO core.core_user (user_username, user_password, user_fullname, user_account_id, user_default_service_id)
                        VALUES ( ? , ? , ? , "650", ? ) ';

    //prepare statement 
    $stmt = $conn->prepare($qry);

    //prepare the bind
    $stmt->bind_param("ssss", $serviceLogin, $pass, $userFullName, $newServiceId);

    $stmt->execute();

    //check inserted rows
    //$conn->lastInsertId();

    $conn = null;
}

function getUserId($serviceLogin) {

    $conn = getPDOReportingCore();

    $data = array(':serviceLogin' => $serviceLogin);

    $query = "SELECT user_id 
                        FROM core.core_user
                        WHERE user_username = :serviceLogin ";

    //echo '<br>q5='.$qry;
    //foreach ($userArr as $key => $value) {
    //    $userId = $value['user_id'];
    //}
    //prepare the query
    $stmt = $conn->prepare($query);

    //set the fetch mode
    $stmt->setFetchMode(PDO::FETCH_ASSOC);

    //execute it
    $stmt->execute($data);

    //fetch the data
    $data = $stmt->fetch();

    $userId = $data["user_id"];

    $conn = null;

    return $userId;
}

function addCampaignClient($serviceId, $userId) {

    $conn = getPDOMasterCore();

    $query = 'INSERT INTO core.campaign_client (service_id,user_id,client_name,client_status)
                            VALUES ( ? , ? , "Default", "ENABLED")';
    //prepare statement 
    $stmt = $conn->prepare($query);

    //prepare the bind
    $stmt->bind_param("ss", $serviceId, $userId);

    //execute query
    $stmt->execute();

    $lastinsert_id = $conn->lastInsertId();

    $conn = null;

    //return last insert id
    return $lastinsert_id;
}

function addCampaign($id, $userId) {

    $conn = getPDOMasterCore();
    //id = last insert id from addCampaignClient

    $query = 'INSERT INTO core.campaign_campaign (client_id,user_id,campaign_name,campaign_code,campaign_status)
                        VALUES ( ? , ? , "Default", "DEFAULT", "ENABLED") ';

    //prepare statement 
    $stmt = $conn->prepare($query);

    //prepare the bind
    $stmt->bind_param("ss", $id, $userId);

    //execute query
    $stmt->execute();

    $conn = null;
}

function addPermissions($newServiceId, $userId) {

    $conn = getPDOMasterCore();

    $query = 'INSERT INTO core.core_permission (user_id, service_id, permission_type) VALUES ';
    $query .= '(1, ? , "submit.mtsms"),(1, ? , "SUPER"),'; //Niel
    $query .= '(3, ? , "submit.mtsms"),(3, ? , "SUPER"),'; //Brendan
    $query .= '(40, ? , "submit.mtsms"),(40, ? , "SUPER"),'; //Caitrin
    $query .= '(225, ? , "submit.mtsms"),(225, ? , "SUPER"),'; //Louise
    $query .= '(92, ? , "submit.mtsms"),(92, ? , "SUPER"),'; //Thinus
    $query .= '(18, ? , "submit.mtsms"),(18, ? , "SUPER"),'; //wayne.connet
    $query .= '(521, ?  , "submit.mtsms"),(521, ? , "SUPER"),'; //michiel.connet
    $query .= '( ? , ? , "submit.mtsms"),( ? , ? , "SUPER")'; //michiel.connet
    //prepare statement 
    $stmt = $conn->prepare($query);

    //prepare the bind
    $stmt->bind_param("ssssssssssssssssss", $newServiceId, $newServiceId, $newServiceId, $newServiceId, $newServiceId, $newServiceId, $newServiceId, $newServiceId, $newServiceId, $newServiceId, $newServiceId, $newServiceId, $newServiceId, $newServiceId, $userId, $newServiceId, $userId, $newServiceId);

    //execute query
    $stmt->execute();

    $conn = null;
}

function addUserRole($userId) {

    $conn = getPDOMasterCore();

    $roleIds = [400, 401, 402, 403, 407, 404, 405, 409, 410, 200, 206, 100, 104, 212, 800, 801];

    for ($i = 0; $i < count($roleIds); $i++) {

        $query = "INSERT INTO core.core_user_role ( user_id , role_id)
                            VALUES ( ? , ? ) ";

        $stmt = $conn->prepare($query);

        //prepare the bind
        $stmt->bind_param("ss", $userId, $roleIds[$i]);

        //execute query
        $stmt->execute();
    }
    $conn = null;
}

function addRoutes($newServiceId, $serviceLogin) {

    $conn = getPDOMasterCore();

    $networks = ['OUT Vodacom Autotag ', 'OUT MTN Autotag ', 'OUT Cell C Autotag ', 'OUT 8ta Autotag '];
    $mccs = ['65501', '65510', '65507', '65502'];
    $providerIDs = ['27', '14', '21', '14'];
    $rdns = ['2782***************', '2783***************', '2784***************', '2781***************'];

    $routePrices = '2600,0,0,0,2600';

    for ($i = 0; $i < count($networks); $i++) {

        $query = "INSERT INTO core.core_route (provider_id, service_id, route_msg_type, route_description, route_display_name, route_code, route_status, 
            route_match_regex, route_match_priority, route_billing, route_mccmnc, route_collection_id)
                            VALUES ( ? ,  ? , 'MTSMS', ? , ? , ? , 'ENABLED', '/\|mtsms\|" . $mccs[$i] . "\|.*/', '50', ? , ? , '14') ";

        $stmt = $conn->prepare($query);

        $networkServiceLogin = $networks[$i] . " " . $serviceLogin;
        //prepare the bind
        $stmt->bind_param("sssssss", $providerIDs[$i], $newServiceId, $networkServiceLogin, $rdns[$i], $rdns[$i], $routePrices, $mccs[$i]);

        //execute query
        $stmt->execute();
    }
    
    $conn = null;
}

function updateUserAndAccountDetails($userId, $accountId) {

    $conn = getPDOMasterCore();

    $data = array(':userId' => $userId, ':accountId' => $accountId);

    $query = "UPDATE core.account_details SET `core_user_id` =:userId  WHERE account_details_id =:accountId ";
    //prepare statement 
    $stmt = $conn->prepare($query);

    //execute query
    $stmt->execute($data);

    $conn = null;
}

function getClientId($newServiceId) {

    $conn = getPDOReportingCore();
    $data = array(':newServiceId' => $newServiceId);

    $query = 'SELECT client_id 
                        FROM core.campaign_client
                        WHERE service_id =:newServiceId 
                        AND client_name = "Default"';
    //prepare the query
    $stmt = $conn->prepare($query);

    //set the fetch mode
    $stmt->setFetchMode(PDO::FETCH_ASSOC);

    //execute it
    $stmt->execute($data);

    //fetch the data
    $results = $stmt->fetch();

    $id = $results["client_id"];

    $conn = null;
    
    return $id;
}

?>