<?php

/**
 * SEND BULK PORCESSING JOBS TO THIS FILE FOR PROCESSING
 */
//define('ERROR_REPORTING', true);
require_once('ConnetAutoloader.php');
require_once('allInclusive.php');

try {
   /*
     preset::plain();

     set_time_limit(720);
     ini_set('memory_limit','512M');
     setlocale(LC_ALL, 'en_US.utf8');
    */

   if (isset($_POST['service_id'])) {
      $service_id = $_POST['service_id'];

      //some stats
      $result = array();
      $result['unique_msisdns'] = array();
      $result['batch_is_valid'] = true;
      $result['valid_count'] = 0;
      $result['invalid_count'] = 0;
      $result['total'] = 0;
      $result['duplicate_count'] = 0;
      $result['unknown_count'] = 0;
      $result['sms_previews'] = array();
      $result['file_col_count'] = 0;
      $result['unique_mccmnc_codes'] = array();

      //create and open a file to write our content to
      $temp_file_name = trim(guid(), '{}') . date("Y_m_d_H_i_s");
      $temp_file_name = 'cm2_' . str_replace('-', '', $temp_file_name) . '.csv';
      $result['temp_file'] = $temp_file_name;

      //check the post variables and run the appropriate task
      if (isset($_POST['csv_source_file']) && $_POST['csv_source_file'] != "") {
         //process file
         $result = buildCsvFileAndCalculateCost($result, $service_id, $_POST['csv_source_file'], $_POST['sms_text']);
      }

      //check the post variables and run the appropriate task
      if (isset($_POST['contact_list_id']) && $_POST['contact_list_id'] != '0') {
         //process contact list
         $result = generateContactListFile($result, $service_id, $_POST['contact_list_id'], $_POST['sms_text']);
      }

      //check the post variables and run the appropriate task
      if (isset($_POST['numbers_string']) && $_POST['numbers_string'] != "") {
         //process msisdn string
         $result = generateManualMSISDNFile($result, $service_id, $_POST['numbers_string'], $_POST['sms_text']);
      }

      foreach ($result['sms_previews'] as &$sms_object) {

         $sms_object['sms_text'] = htmlentities($sms_object['sms_text']);
      }
      unset($sms_object);

      //get the costing for this whole send
      $result['costing'] = getCostTOSendFromMCCMNCCountArray($service_id, $result['unique_mccmnc_codes']);

      $result['success'] = true;
      echo json_encode($result);
      exit;
   } else {
      $result = array('success' => false, 'error' => 'No service ID was passed.');
      echo json_encode($result);
      exit;
   }
} catch (Exception $e) {
   $result = array('success' => false, 'error' => $e->getMessage());
   echo json_encode($result);
   exit;
}
