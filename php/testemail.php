<?php

sendMailToUser(1, "jacques@connet-systems.com");

//define("DEFCALLBACKMAIL", "customer@connet-systems.com"); // WIll be shown as "from".

function preparehtmlmailE($html) 
{

  //clog::info('Beginning HTML mail preparation');

  preg_match_all('~<img.*?src=.([\/.a-z0-9:_-]+).*?>~si',$html,$matches);
  $i = 0;
  $paths = array();

  foreach ($matches[1] as $img) 
  {
    $img_old = $img;

    if(strpos($img, "http://") == false) 
    {
      $uri = parse_url($img);
      $paths[$i]['path'] = $_SERVER['DOCUMENT_ROOT'].$uri['path'];
      $content_id = md5($img);
      $html = str_replace($img_old,'cid:'.$content_id,$html);
      $paths[$i++]['cid'] = $content_id;
    }
  }
  
  //clog::info('Processing HTML');

  $boundary = "--".md5(uniqid(time()));
  $headers = "MIME-Version: 1.0\n";
  $headers .="Content-Type: multipart/mixed; boundary=\"$boundary\"\n";
  $headers .= "From: customer@connet-systems.com\r\n";
  
  $multipart = '';
  $multipart .= "--$boundary\n";
  $kod = 'utf-8';
  $multipart .= "Content-Type: text/html; charset=$kod\n";
  $multipart .= "Content-Transfer-Encoding: Quot-Printed\n\n";
  $multipart .= "$html\n\n";

  foreach ($paths as $path) 
  {
  //clog::info('Processing HTML IMAGES');
    if(file_exists($path['path']))
      $fp = fopen($path['path'],"r");
      if (!$fp)  
      {
        echo "No file";
        return false;
      }

    $imagetype = substr(strrchr($path['path'], '.' ),1);
    $file = fread($fp, filesize($path['path']));
    fclose($fp);

    $message_part = "";

    switch ($imagetype) 
    {
      case 'png':
      case 'PNG':
            $message_part .= "Content-Type: image/png";
            break;
      case 'jpg':
      case 'jpeg':
      case 'JPG':
      case 'JPEG':
            $message_part .= "Content-Type: image/jpeg";
            break;
      case 'gif':
      case 'GIF':
            $message_part .= "Content-Type: image/gif";
            break;
    }

    $message_part .= "; file_name = \"$path\"\n";
    $message_part .= 'Content-ID: <'.$path['cid'].">\n";
    $message_part .= "Content-Transfer-Encoding: base64\n";
    $message_part .= "Content-Disposition: inline; filename = \"".basename($path['path'])."\"\n\n";
    $message_part .= chunk_split(base64_encode($file))."\n";
    $multipart .= "--$boundary\n".$message_part."\n";
  }

  $multipart .= "--$boundary--\n";
  $multipart = wordwrap($multipart,70);
  
  return array('multipart' => $multipart, 'headers' => $headers);  
}

function sendMailToUser($sId, $email)
{
  echo "START";
    $fileatt = "../download/Connet Systems_Brochure.pdf"; // Path to the file                  
    $fileatt_type = "application/pdf"; // File Type  
    $fileatt_name = "Connet Systems_Brochure.pdf"; // Filename that will be used for the file as the attachment  
    $file = fopen($fileatt,'rb');  
    $data = fread($file,filesize($fileatt));  
    fclose($file);  
    $semi_rand = md5(time()); 
    $data = chunk_split(base64_encode($data)); 

    $to = $email;
    $subject = "Welcome to Connet Systems!";

    $msg = '<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w31.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
            <html>
            <head>
                <title>'.$subject.'</title>
            </head>
                <body style="font-family: Arial, sans-serif;    font-stretch: normal;   font-size: 12px;    font-style: normal; width:740px;">
                    <table border="0">
                        <tr>
                            <td>
                                <img src="http://portal.connet-systems.com/img/Header.jpg"> 
                            </td>
                        </tr>   
                        <tr>
                            <td style="padding-left: 28px; padding-right: 28px;">
                                <h1 style="color:#3c5e6e;font-size:21px;">Thank you for setting up a bulk SMS account with Connet Systems.</h1>
                                <p>Login details below:</p>
                                <p style="padding-left:20px;width:740px;"><b>Username:</b> '.$email.'</p>
                                
                                <p style="padding-left:20px;width:740px;"><b>SMS Rate:</b> 0.26c (ZAR) per SMS</p>
                                <p style="padding-left:20px;width:740px;"><b>Account type:</b> Prepaid</p>
                                <p style="padding-left:20px;width:740px;"><b>Payment method:</b> EFT</p>
                                <p style="width:740px;">Please keep this email as a reference.</p>
                                <p style="width:740px;">To try out the free test credits and use your bulk SMS account, please login here: www.connet-systems.com</p>
                                <p style="width:740px;">Simply add more credits by requesting an invoice with the quantity required (minimum volume is 1000 SMS credits) by sending an email to accounts@connet-systems.com.  Please send the POP to accounts@connet-systems.com and once we receive the POP your credits will be added.  Payment method is by EFT only.</p>
                                <p style="width:740px;">Please send credit requests and payments during business hours from 8am to 5pm Monday to Friday (except on Public Holidays) for prompt assistance.  The SMS credits are valid for 12 months from the date the credits were added on your SMS account.</p>

                                <p style="width:740px;"><b>Tutorials to assist you:</b></p>
                                  <p style="padding-left:20px;width:740px;">
                                    Watch our tutorial on how to use the Bulk SMS system <a href="https://youtu.be/Ki6UOmazVqw">here</a>.
                                  </p>
                                  <p style="padding-left:20px;width:740px;">
                                    Watch our tutorial on how to create a csv file <a href="https://youtu.be/MpYuKZ9QItk">here</a>.
                                  </p>
                                  <p style="padding-left:20px;width:740px;">
                                    Download our guide for the Campaign Manager <a href="portal2.connet-systems.com/download/Connet%20Systems_Brochure.pdf">here</a>.
                                  </p>
                                <p style="width:740px;">Our bulk SMS campaign manager will count the text in your SMS message to see if you are going over 160 characters and running into 2 or more SMSs.  We can also set up a low credit warning on your account that goes to your email address.  Please contact support@connet-systems.com for assistance in setting up a low balance notification.</p>
                                <p style="width:740px;">SMS Lengths:</p>
                                <p style="padding-left:20px;width:740px;">
                                  0-160 characters = 1 SMS
                                </p>
                                <p style="padding-left:20px;width:740px;">
                                  161-306 characters = 2 SMS
                                </p>
                                <p style="padding-left:20px;width:740px;">
                                  307-459 characters = 3 SMS
                                </p>

                                <p style="width:740px;">
                                  General enquiries – customer@connet-systems.com
                                </p>
                                <p style="width:740px;">
                                  Invoices – accounts@connet-systems.com
                                </p>
                                <p style="width:740px;">
                                  Technical support – support@connet-systems.com
                                </p>
                                <p style="width:740px;">
                                  To enhance your mobile marketing strategy, Connet Systems offers other mobile communication solutions such as USSD (link to website info when they click on USSD) and Short Codes (link to website info).
                                </p>
                                <p><br>Kind Regards,</p>
                                <p><strong>The Connet Systems Team</strong></p>
                            </td>
                        </tr>   
                        <tr>
                            <td>    
                                <img style="margin-bottom:-100px;" src="http://portal.connet-systems.com/img/Footer.jpg"> 
                            </td>
                        </tr>   
                    </table>
                </body>
            </html>';


  
        $msg = wordwrap($msg,70);

        /*$msg .= "--{$mime_boundary}\n" .  
                  "Content-Type: {$fileatt_type};\n" .  
                  " name=\"{$fileatt_name}\"\n" .  
                  //"Content-Disposition: attachment;\n" .  
                  //" filename=\"{$fileatt_name}\"\n" .  
                  "Content-Transfer-Encoding: base64\n\n" .  
                 $data .= "\n\n" .  
                  "--{$mime_boundary}--\n"; */
        
        $final_msg = preparehtmlmailE($msg); // give a function your html*

        $m = mail($to, $subject, $final_msg['multipart'], $final_msg['headers']);
        //$m = mail('jacquesb@connet-systems.com,wayne@connet-systems.com', $subject, $final_msg['multipart'], $final_msg['headers']);
        //$m = mail('jacqu3esb@connet-systems.com', $subject, $final_msg['multipart'], $final_msg['headers']);
        echo "<br>END";
}

?>