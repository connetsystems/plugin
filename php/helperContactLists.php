<?php
require_once("config/dbConn.php");

ini_set('display_errors', 1);

//--------------------------------------------------------------
// Constants
//--------------------------------------------------------------
//define('ERROR_REPORTING', (array_search('debug', $argv) !== false));

//--------------------------------------------------------------
// Includes
//--------------------------------------------------------------
require_once('ConnetAutoloader.php');

clog::setLogFilename("/var/log/connet/plugin/" . basename(__FILE__, '.php') . '.log');
clog::setBacktrace();
clog::setVerbose(ERROR_REPORTING);



class helperContactLists
{
    private $conn;

    public function __construct()
    {
        $this->conn = getPDOMasterCore();
    }

    public function killDbConnection()
    {
        $this->conn = null;
    }

    /**
     * This function creates a contact list aka "Group"
     * @param $service_id
     * @param $list_name
     * @param $list_description
     * @param $list_code
     * @param $campaign_id
     * @param $client_id
     * @param $status
     * @return bool - success of the update
     */
    function createContactList($service_id, $list_name, $list_description, $list_code, $campaign_id, $client_id, $status)
    {
        if (!isset($campaign_id) && !isset($client_id)) {
            //prepare the data
            $data = array(':service_id' => $service_id, ':contact_list_name' => $list_name, ':contact_list_description' => $list_description,
                ':contact_list_code' => $list_code, ':contact_list_status' => $status);

            //prepare the query
            $stmt = $this->conn->prepare("INSERT INTO core.campaign_contact_list
                    (`service_id`, `contact_list_name`, `contact_list_description`, `contact_list_code`, `contact_list_status`)
                    VALUES (:service_id, :contact_list_name, :contact_list_description, :contact_list_code, :contact_list_status)");
        }
        else
        {
            //prepare the data
            $data = array(':service_id' => $service_id, ':contact_list_name' => $list_name, ':contact_list_description' => $list_description,
                ':contact_list_code' => $list_code, ':campaign_id' => $campaign_id, ':client_id' => $client_id, ':contact_list_status' => $status);

            //prepare the query
            $stmt = $this->conn->prepare("INSERT INTO core.campaign_contact_list
                    (`service_id`, `contact_list_name`, `contact_list_description`, `contact_list_code`, `campaign_id`, `client_id`, `contact_list_status`)
                    VALUES (:service_id, :contact_list_name, :contact_list_description, :contact_list_code, :campaign_id, :client_id, :contact_list_status)");
        }


        $insert = $stmt->execute($data);
        if($insert)
        {
            $contact_id = $this->conn->lastInsertId();
        }
        else
        {
            $contact_id = 0;
        }

        return $contact_id;
    }

    /**
     * This function updates a contact list according to it's contact_list_id
     *
     * @param $list_id
     * @param $service_id
     * @param $list_name
     * @param $list_description
     * @param $list_code
     * @param $campaign_id
     * @param $client_id
     * @param $status
     * @return bool - success of the update
     */
    function updateContactList($list_id, $service_id, $list_name, $list_description, $list_code, $campaign_id, $client_id, $status)
    {

        //prepare the data
        $data = array(':contact_list_id' => $list_id, ':service_id' => $service_id, ':contact_list_name' => $list_name, ':contact_list_description' => $list_description,
            ':contact_list_code' => $list_code, ':campaign_id' => $campaign_id, ':client_id' => $client_id, ':contact_list_status' => $status,);

        //prepare the query
        $stmt = $this->conn->prepare("UPDATE core.campaign_contact_list SET
          `service_id` = :service_id, `contact_list_name` = :contact_list_name, `contact_list_description` = :contact_list_description, `contact_list_code` = :contact_list_code,
          `campaign_id` = :campaign_id, `client_id` = :client_id, `contact_list_status` = :contact_list_status
          WHERE contact_list_id = :contact_list_id ");

        $update = $stmt->execute($data);

        return $update;
    }


    //DOUG: converted to PDO - contactlists.php | contactlistadd.php
    function deleteContactList($contact_list_id, $delete_contacts = null)
    {
        clog::info("deleteContactList function called.... Contact List ID: ".$contact_list_id);
        //prepare the data
        $data = array(':contact_list_id' => $contact_list_id);

        //do handle the contact deletion within the list if it is set
        if (isset($delete_contacts) && $delete_contacts === true)
        {
            clog::info("User want's to delete contacts in this list too.");
            $contact_count = $this->countContactsInList($contact_list_id);
            $delete_passes = ceil($contact_count / 1000);

            for ($d = 1; $d <= $delete_passes; ++$d)
            {
                //cycle through and delete all contacts that are members of this list
                $contacts = $this->getContactsInContactList($contact_list_id, $d, 1000);

                foreach ($contacts as $contact)
                {
                    //prepare the data
                    $data_del_contact = array(':contact_id' => $contact['contact_id'], ':contact_list_id' => $contact_list_id);

                    //prepare the query
                    $stmt_del_contact = $this->conn->prepare("SELECT count(*) FROM core.campaign_contact_list_contact WHERE contact_id = :contact_id AND contact_list_id != :contact_list_id LIMIT 1");

                    $stmt_del_contact->execute($data_del_contact);

                    //check the row count, return true if a row exists
                    if ($stmt_del_contact->fetchColumn() < 1)
                    {
                        $this->deleteContact($contact['contact_id']);
                    }
                }
            }
        }

        //delete relational references to this list from the contacts
        $stmt = $this->conn->prepare("DELETE FROM `core`.`campaign_contact_list_contact` WHERE contact_list_id = :contact_list_id ");

        //execute it
        $stmt->execute($data);

        //delete frome the actual list table
        $stmt2 = $this->conn->prepare("DELETE FROM `core`.`campaign_contact_list` WHERE contact_list_id = :contact_list_id ");

        //execute it
        $stmt2->execute($data);

        return true;
    }

    //DOUG: converted to PDO - contactlist.php | contactlistsadd.php
    function getContactsInContactList($contact_list_id, $page = null, $limit = null)
    {
        //determine the limit and offset from the pager
        if (!isset($page)) {
            $limit = 25;
            $page = 1;
        }
        $offset = $limit * ($page - 1);

        //prepare the query
        $stmt = $this->conn->prepare('SELECT l.contact_id, l.contact_list_id, m.contact_firstname, m.contact_lastname, m.contact_msisdn
                    FROM core.campaign_contact_list_contact l
                    LEFT JOIN core.campaign_contact m
                    ON l.contact_id = m.contact_id
                    WHERE contact_list_id = :contact_list_id ORDER BY contact_lastname ASC, contact_msisdn ASC LIMIT :offset, :limit');


        $stmt->bindValue(':contact_list_id', $contact_list_id);
        $stmt->bindValue(':limit', (int)$limit, PDO::PARAM_INT);
        $stmt->bindValue(':offset', (int)$offset, PDO::PARAM_INT);

        //set the fetch mode
        $stmt->setFetchMode(PDO::FETCH_ASSOC);

        //execute it
        $stmt->execute();

        //fetch the data
        $contacts = $stmt->fetchAll();

        return $contacts;
    }

    //DOUG: converted to PDO - contactlist.php | contactlistsadd.php
    function searchContactsInContactList($contact_list_id, $search_type, $search_string, $page = null, $limit = null)
    {
        //determine the limit and offset from the pager
        if (!isset($page)) {
            $limit = 25;
            $page = 1;
        }
        $offset = $limit * ($page - 1);

        //prepare the query
        $stmt = $this->conn->prepare('SELECT l.contact_id, l.contact_list_id, m.contact_firstname, m.contact_lastname, m.contact_msisdn
                    FROM core.campaign_contact_list_contact l
                    LEFT JOIN core.campaign_contact m
                    ON l.contact_id = m.contact_id
                    WHERE contact_list_id = :contact_list_id AND ' . $search_type . ' LIKE :search_string ORDER BY contact_lastname ASC, contact_msisdn ASC LIMIT :offset, :limit');

        $stmt->bindValue(':contact_list_id', $contact_list_id);
        $stmt->bindValue(':search_string', '%' . $search_string . '%', PDO::PARAM_STR);
        $stmt->bindValue(':limit', (int)$limit, PDO::PARAM_INT);
        $stmt->bindValue(':offset', (int)$offset, PDO::PARAM_INT);

        //set the fetch mode
        $stmt->setFetchMode(PDO::FETCH_ASSOC);

        //execute it
        $stmt->execute();

        //fetch the data
        $contacts = $stmt->fetchAll();

        return $contacts;
    }

    //DOUG: converted to PDO - contactlist.php | contactlistsadd.php
    function getAllContactsInService($service_id, $page = null, $limit = null)
    {
        //determine the limit and offset from the pager
        if (!isset($page)) {
            $page = 1;
            $limit = 25;
        }
        $offset = $limit * ($page - 1);

        //prepare the query
        $stmt = $this->conn->prepare('SELECT contact_id, contact_firstname, contact_lastname, contact_msisdn
                    FROM campaign_contact
                    WHERE service_id = :service_id ORDER BY contact_lastname ASC, contact_msisdn ASC LIMIT :offset, :limit');

        $stmt->bindValue(':service_id', $service_id);
        $stmt->bindValue(':limit', (int)$limit, PDO::PARAM_INT);
        $stmt->bindValue(':offset', (int)$offset, PDO::PARAM_INT);

        //set the fetch mode
        $stmt->setFetchMode(PDO::FETCH_ASSOC);

        //execute it
        $stmt->execute();

        //fetch the data
        $contacts = $stmt->fetchAll();

        return $contacts;
    }

    //DOUG: converted to PDO - contactlist.php | contactlistsadd.php
    function searchAllContactsInService($service_id, $search_type, $search_string, $page = null, $limit = null)
    {
        //determine the limit and offset from the pager
        if (!isset($page)) {
            $page = 1;
            $limit = 25;
        }
        $offset = $limit * ($page - 1);

        //prepare the query
        $stmt = $this->conn->prepare('SELECT contact_id, contact_firstname, contact_lastname, contact_msisdn
                    FROM campaign_contact
                    WHERE service_id = :service_id AND ' . $search_type . ' LIKE :search_string ORDER BY contact_lastname ASC, contact_msisdn ASC LIMIT :offset, :limit');

        $stmt->bindValue(':service_id', $service_id);
        $stmt->bindValue(':search_string', '%' . $search_string . '%');
        $stmt->bindValue(':limit', (int)$limit, PDO::PARAM_INT);
        $stmt->bindValue(':offset', (int)$offset, PDO::PARAM_INT);

        //set the fetch mode
        $stmt->setFetchMode(PDO::FETCH_ASSOC);

        //execute it
        $stmt->execute();

        //fetch the data
        $contacts = $stmt->fetchAll();

        return $contacts;
    }

    //DOUG: converted to PDO - confirmbulkfile.php | confirmbulkfile2.php | confirmbulkfileold.php
    function getContactInfoNumOnly($contact_list_id)
    {
        //prepare the data
        $data = array(':contact_list_id' => $contact_list_id);

        //prepare the query
        $stmt = $this->conn->prepare('SELECT m.contact_msisdn
                    FROM core.campaign_contact_list_contact l
                    LEFT JOIN core.campaign_contact m
                    ON l.contact_id = m.contact_id
                    WHERE contact_list_id = :contact_list_id');

        //set the fetch mode
        $stmt->setFetchMode(PDO::FETCH_ASSOC);

        //execute it
        $stmt->execute($data);

        //fetch the data
        $contactInfo = $stmt->fetchAll();

        return $contactInfo;
    }

    //DOUG: converted to PDO - confirmbulkfile.php | confirmbulkfile2.php | confirmbulkfileold.php
    function getAllContactInfoNumOnlyByService($service_id)
    {

        //prepare the data
        $data = array(':service_id' => $service_id);

        //prepare the query
        $stmt = $this->conn->prepare('SELECT contact_msisdn FROM campaign_contact WHERE service_id = :service_id');

        //set the fetch mode
        $stmt->setFetchMode(PDO::FETCH_ASSOC);

        //execute it
        $stmt->execute($data);

        //fetch the data
        $contacts = $stmt->fetchAll();

        return $contacts;
    }

    //DOUG: converted to PDO - bulksms.php | bulksms2.php | bulksmscont.php | bulksmscontold.php | contactlists.php | contactlistadd.php
    function getContactListClient($service_id)
    {

        //prepare the data
        $data = array(':service_id' => $service_id);

        //prepare the query
        $stmt = $this->conn->prepare('SELECT contact_list_name, contact_list_description, contact_list_id, contact_list_code, service_id, contact_list_status, client_id, campaign_id
                    FROM core.campaign_contact_list
                    WHERE service_id = :service_id');

        //set the fetch mode
        $stmt->setFetchMode(PDO::FETCH_ASSOC);

        //execute it
        $stmt->execute($data);

        //fetch the data
        $cList = $stmt->fetchAll();

        return $cList;
    }

    //DOUG: converted to PDO - bulksmscont.php
    function getEnabledContactListsByClientAndCampaign($service_id, $client_id, $campaign_id)
    {

        //prepare the data
        $data = array(':service_id' => $service_id, ':client_id' => $client_id, ':campaign_id' => $campaign_id);

        //prepare the query
        $stmt = $this->conn->prepare('SELECT contact_list_name, contact_list_description, contact_list_id, contact_list_code, service_id, contact_list_status, client_id, campaign_id
                    FROM core.campaign_contact_list
                    WHERE service_id = :service_id AND client_id = :client_id AND campaign_id = :campaign_id');

        //set the fetch mode
        $stmt->setFetchMode(PDO::FETCH_ASSOC);

        //execute it
        $stmt->execute($data);

        //fetch the data
        $cList = $stmt->fetchAll();

        return $cList;
    }

    function getContactIdByMSISDN($service_id, $contact_msisdn)
    {
        //prepare the data
        $data = array(':contact_msisdn' => $contact_msisdn, ':service_id' => $service_id);

        //prepare the query
        $stmt = $this->conn->prepare('SELECT contact_id
                    FROM core.campaign_contact
                    WHERE contact_msisdn = :contact_msisdn AND service_id = :service_id');

        //set the fetch mode
        $stmt->setFetchMode(PDO::FETCH_ASSOC);

        //execute it
        $stmt->execute($data);

        //fetch the data
        $contact = $stmt->fetch();

        if (isset($contact['contact_id'])) {
            return $contact['contact_id'];
        } else {
            return 0;
        }
    }

    //DOUG: converted to PDO - contactlists.php | contactlistadd.php
    function updateContact($contact_id, $contact_firstname, $contact_lastname, $msisdn)
    {

        //prepare the data
        $data = array(':contact_firstname' => $contact_firstname, ':contact_id' => $contact_id, ':contact_lastname' => $contact_lastname, ':msisdn' => $msisdn);

        //prepare the query
        $stmt = $this->conn->prepare('UPDATE core.campaign_contact SET `contact_firstname` = :contact_firstname, `contact_lastname` = :contact_lastname, `contact_msisdn` = :msisdn WHERE contact_id = :contact_id ');

        //execute it
        $result = $stmt->execute($data);

        return $result;
    }

    /**
     * Get the total amount of contacts in this list
     * @param $contact_list_id - THe ID of the contact list to run a count query
     * @return int $row_count - The total amount of contacts contained within the list
     */
    function checkContactListExists($contact_list_id)
    {
        //prepare the data
        $data_check = array(':contact_list_id' => $contact_list_id);

        //prepare the query
        $stmt_check = $this->conn->prepare("SELECT count(*) FROM core.campaign_contact_list WHERE contact_list_id = :contact_list_id LIMIT 1");

        $stmt_check->execute($data_check);

        //if more than one row exists then it exists
        if ($stmt_check->fetchColumn() > 0) {
            return true;
        } else {
            return false;
        }

    }

    /**
     * Get the total amount of contacts in this list
     * @param $contact_list_id - THe ID of the contact list to run a count query
     * @return int $row_count - The total amount of contacts contained within the list
     */
    function countContactsInList($contact_list_id)
    {

        //prepare the data
        $data_check = array(':contact_list_id' => $contact_list_id);

        //prepare the query
        $stmt_check = $this->conn->prepare("SELECT count(*) FROM core.campaign_contact_list_contact WHERE contact_list_id = :contact_list_id");

        $stmt_check->execute($data_check);

        //return the row count as a representative of the total contacts in this list
        return $stmt_check->fetchColumn();
    }

    /**
     * Get the total amount of contacts in this list
     * @param $contact_list_id - THe ID of the contact list to run a count query
     * @param $search_type - The comlumn to search by
     * @param $search_string - The String to search for in the column
     * @return int $row_count - The total amount of contacts contained within the list
     */
    function countContactsInListSearch($contact_list_id, $search_type, $search_string)
    {

        //prepare the data
        $data_check = array(':contact_list_id' => $contact_list_id, ':search_string' => '%' . $search_string . '%');

        //prepare the query
        $stmt_check = $this->conn->prepare('SELECT count(*)
                    FROM core.campaign_contact_list_contact l
                    LEFT JOIN core.campaign_contact m
                    ON l.contact_id = m.contact_id
                    WHERE contact_list_id = :contact_list_id AND ' . $search_type . ' LIKE :search_string');

        $stmt_check->execute($data_check);

        //return the row count as a representative of the total contacts in this list
        return $stmt_check->fetchColumn();
    }

    /**
     * Get the total amount of contacts that the service has uploaded
     * @param $service_id - THe ID of the service
     * @return int $row_count - The total contacts
     */
    function countAllContactsInService($service_id)
    {
        //prepare the data
        $data_check = array(':service_id' => $service_id);

        //prepare the query
        $stmt_check = $this->conn->prepare("SELECT count(*) FROM core.campaign_contact WHERE service_id = :service_id");

        $stmt_check->execute($data_check);

        //return the row count as a representative of the total contacts in this list
        return $stmt_check->fetchColumn();
    }

    /**
     * Get the total amount of corphan contacts in this service, orphans are contacts that are not asigned to a group
     * @param $service_id - THe ID of the service
     * @return int $row_count - The total contacts
     */
    function countAllOrphanContactsInService($service_id)
    {

        //get all the contact lists (groups)
        $contact_lists = $this->getContactListClient($service_id);
        $grouped_contacts_count = 0; //track the count
        $contact_list_ids = array();

        //cycle through and count unique contacts within
        foreach ($contact_lists as $contact_list) {
            array_push($contact_list_ids, $contact_list['contact_list_id']);
        }

        //prepare the query
        $stmt_check = $this->conn->prepare("SELECT count(DISTINCT contact_id) AS count FROM core.campaign_contact_list_contact WHERE contact_list_id IN (" . implode(',', $contact_list_ids) . ")");
        $stmt_check->execute();
        $counts_in_group = $stmt_check->fetch();

        //get the total count of all contacts
        $all_contacts = $this->countAllContactsInService($service_id);

        //return the difference between assigned contacts and all contacts to get our orphan count
        return ($all_contacts - $counts_in_group['count']);
    }

    /**
     * Get the total amount of contacts that the servicre has uploaded
     * @param $service_id - THe ID of the service
     * @param $search_type - The comlumn to search by
     * @param $search_string - The String to search for in the column
     * @return int $row_count - The total contacts
     */
    function countAllContactsSearch($service_id, $search_type, $search_string)
    {

        //prepare the data
        $data_check = array(':service_id' => $service_id, ':search_string' => '%' . $search_string . '%');

        //prepare the query
        $stmt_check = $this->conn->prepare("SELECT count(*) FROM core.campaign_contact WHERE service_id = :service_id AND " . $search_type . " LIKE :search_string");

        $stmt_check->execute($data_check);

        //return the row count as a representative of the total contacts in this list
        return $stmt_check->fetchColumn();
    }

    /**
     * This function simply checks if a contact has been added to a particular contact list (AKA contact group)
     * @param $contact_id - The ID of the contact
     * @param $contact_list_id - THe ID of the contact list that we want to check
     * @return bool - Does it or doesnt it exist in the contact group
     */
    function checkContactIsInList($contact_id, $contact_list_id)
    {

        //prepare the data
        $data_check = array(':contact_id' => $contact_id, ':contact_list_id' => $contact_list_id);

        //prepare the query
        $stmt_check = $this->conn->prepare("SELECT count(*) FROM core.campaign_contact_list_contact WHERE contact_id = :contact_id AND contact_list_id = :contact_list_id LIMIT 1");

        $stmt_check->execute($data_check);

        //check the row count, return true if a row exists
        if ($stmt_check->fetchColumn() > 0) {
            return true;
        } else {
            return false;
        }
    }

    //DOUG: converted to PDO - contactlists.php | contactlistadd.php
    function addNewContact($service_id, $contact_firstname, $contact_lastname, $contact_msisdn)
    {
        //first check if it exist
        //prepare the data
        $data_check = array(':service_id' => $service_id, ':contact_msisdn' => $contact_msisdn);

        //prepare the query
        $stmt_check = $this->conn->prepare("SELECT count(*) FROM core.campaign_contact WHERE contact_msisdn = :contact_msisdn AND service_id = :service_id LIMIT 1");

        $stmt_check->execute($data_check);

        //check the row count, UPDATE THE CONTACT IF IT EXISTS, OR ADD A NEW RECORD IF IT DOES NOT
        if ($stmt_check->fetchColumn() > 0) {
            //it exists, update it
            $data1 = array(':service_id' => $service_id, ':contact_firstname' => $contact_firstname, ':contact_lastname' => $contact_lastname, ':contact_msisdn' => $contact_msisdn);

            //prepare the query
            $stmt1 = $this->conn->prepare('UPDATE core.campaign_contact SET `contact_firstname` = :contact_firstname, `contact_lastname` = :contact_lastname
                         WHERE contact_msisdn = :contact_msisdn AND service_id = :service_id');
        } else {
            //it doesnt exist, insert it
            $data1 = array(':service_id' => $service_id, ':contact_firstname' => $contact_firstname, ':contact_lastname' => $contact_lastname, ':contact_msisdn' => $contact_msisdn);

            //prepare the query
            $stmt1 = $this->conn->prepare("INSERT INTO core.campaign_contact (`service_id`, `contact_firstname`, `contact_lastname`, `contact_msisdn`)
                     VALUES (:service_id, :contact_firstname, :contact_lastname, :contact_msisdn) ");
        }

        $stmt1->execute($data1);

        $contact_id = $this->conn->lastInsertId();

        //contact_id = 0 means the record already existed and was updated, so lets get it's ID
        if ($contact_id == 0)
        {
            $contact_id = $this->getContactIdByMSISDN($service_id, $contact_msisdn);
        }

        return $contact_id;
    }

    //DOUG: converted to PDO - contactlists.php | contactlistadd.php
    function deleteContact($contact_id)
    {
        //prepare the data
        $data = array(':contact_id' => $contact_id);

        //delete relational references to this contact from any lists it was associated with
        $stmt = $this->conn->prepare("DELETE FROM core.campaign_contact_list_contact WHERE contact_id = :contact_id ");

        //execute it
        $stmt->execute($data);

        //delete the actual contact
        $stmt2 = $this->conn->prepare("DELETE FROM core.campaign_contact WHERE contact_id = :contact_id ");

        //execute it
        $stmt2->execute($data);

        return true;
    }

    function deleteContactByMSISDN($service_id, $contact_msisdn)
    {
        $contact_id = $this->getContactIdByMSISDN($service_id, $contact_msisdn);
        if($contact_id > 0)
        {
            return $this->deleteContact($contact_id);
        }
        else
        {
            return false;
        }
    }

    /**
     * Adds a contact to a list by creating a reference in the relations table, won't add if it already exists
     * @param $contact_id
     * @param $contact_list_id
     * @return bool
     */
    function addContactToList($contact_id, $contact_list_id)
    {

        //make sure the contact isn't already in the list
        if (!$this->checkContactIsInList($contact_id, $contact_list_id)) {
            //prepare the data
            $data = array(':contact_id' => $contact_id, ':contact_list_id' => $contact_list_id);

            //prepare the query
            $stmt = $this->conn->prepare("INSERT INTO core.campaign_contact_list_contact (`contact_id`, `contact_list_id`) VALUES (:contact_id, :contact_list_id) ");

            $result = $stmt->execute($data);

            return $result;
        } else {
            return false;
        }
    }

    /**
     * Removes a contact from a list by destroying the reference in the relations table
     * @param $contact_id
     * @param $contact_list_id
     * @return bool
     */
    function removeContactFromList($contact_id, $contact_list_id)
    {
        //prepare the data
        $data = array(':contact_id' => $contact_id, ':contact_list_id' => $contact_list_id);

        //prepare the query
        $stmt = $this->conn->prepare("DELETE FROM `core`.`campaign_contact_list_contact`
                    WHERE contact_id = :contact_id
                    AND contact_list_id = :contact_list_id ");

        //execute it
        $result = $stmt->execute($data);

        return $result;
    }

    //DOUG: converted to PDO - contactlist.php
    function checkContactExists($number, $name, $surname, $service_id, $contact_list_id)
    {
        //prepare the data
        $data = array(':service_id' => $service_id, ':contact_msisdn' => $number, ':contact_firstname' => $name,
            ':contact_lastname' => $surname);

        //prepare the query
        $stmt = $this->conn->prepare('SELECT contact_id, contact_msisdn, contact_firstname, contact_lastname
                    FROM core.campaign_contact
                    WHERE service_id = :service_id
                    AND contact_msisdn = :contact_msisdn
                    AND contact_firstname = :contact_firstname
                    AND contact_lastname = :contact_lastname ');

        //set the fetch mode
        $stmt->setFetchMode(PDO::FETCH_ASSOC);

        $stmt->execute($data);

        $results = $stmt->fetchAll();


        $checker = '';
        $checker2 = 0;
        if (isset($results) && $results != '')
        {
            foreach ($results as $row)
            {
                $checker .= $row['contact_id'] . ',';
            }

            $checker = substr($checker, 0, -1);

            //prepare the data
            $data2 = array(':contact_id' => $checker);

            //prepare the query
            $stmt2 = $this->conn->prepare("SELECT contact_list_id
                         FROM core.campaign_contact_list_contact
                         WHERE contact_id IN (:contact_id)");

            //set the fetch mode
            $stmt2->setFetchMode(PDO::FETCH_ASSOC);

            $stmt2->execute($data2);

            $results2 = $stmt2->fetchAll();

            if (isset($results2) && $results2 != '') {
                foreach ($results2 as $row2) {
                    if ($row2['contact_list_id'] == $contact_list_id) {
                        $checker2++;
                    }
                }
            }
        }

        return $checker2;
    }

    //DOUG: converted to PDO -  contactlists.php | contactlistadd.php
    function checkNewContact($service_id, $contact_msisdn)
    {

        //prepare the data
        $data = array(':service_id' => $service_id);

        //prepare the query
        $stmt = $this->conn->prepare('SELECT contact_id, contact_msisdn FROM core.campaign_contact WHERE service_id = :service_id ');

        //set the fetch mode
        $stmt->setFetchMode(PDO::FETCH_ASSOC);

        $stmt->execute($data);

        $results = $stmt->fetchAll();

        $checker = 0;
        foreach ($results as $row) {
            if ($contact_msisdn == $row['contact_msisdn']) {
                $checker .= $row['contact_id'] . ',';
            }
        }
        $checker = substr($checker, 0, -1);

        if ($checker == 0) {
            return 0;
        } else {
            return $checker;
        }
    }

    //DOUG: converted to PDO -  - contactlists.php
    function checkContactLinkExist($contact_id, $contact_list_id)
    {

        //prepare the data
        $data = array(':contact_id' => $contact_id);

        //prepare the query
        $stmt = $this->conn->prepare("SELECT contact_list_id FROM core.campaign_contact_list_contact WHERE contact_id IN (:contact_id)");

        //set the fetch mode
        $stmt->setFetchMode(PDO::FETCH_ASSOC);

        $stmt->execute($data);

        $results = $stmt->fetchAll();

        $checker = 0;
        if (isset($results) && $results != '') {
            foreach ($results as $row) {
                if ($row['contact_list_id'] == $contact_list_id) {
                    $checker++;
                }
            }
        }

        return $checker;
    }

    //DOUG: converted to PDO - contactlists.php | contactlistadd.php
    function checkNewContactListClient($service_id, $nName, $nDesc)
    {

        //prepare the data
        $data = array(':service_id' => $service_id);

        //prepare the query
        $stmt = $this->conn->prepare('SELECT contact_list_name, contact_list_description
                    FROM core.campaign_contact_list
                    WHERE service_id = :service_id ');

        //set the fetch mode
        $stmt->setFetchMode(PDO::FETCH_ASSOC);

        //execute it
        $stmt->execute($data);

        //fetch the data
        $row = $stmt->fetch();

        $checker = 0;
        if (isset($row['contact_list_name']) && isset($row['contact_list_description'])) {
            if ($nName == $row['contact_list_name']) {
                if ($nDesc == $row['contact_list_description']) {
                    $checker++;
                }
            }
        }

        if ($checker == 0)
        {
            return 0;
        }
        else
        {
            return -1;
        }
    }

}
