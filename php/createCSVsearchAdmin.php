<?php
    require_once('config/dbConn.php');

    if(isset($_GET['date']) && $_GET['date'] != '')
    {
        $dateArr = explode(' - ', $_GET['date']);
        $startDate = $dateArr[0];
        $startDateConv = $dateArr[0];
        $endDate = $dateArr[1];
        $endDateConv = $dateArr[1];
        $date = $_GET['date'];
    }
    else
    {
        $startDate = (strtotime('today midnight'))*1000;
        $endDate = (time()*1000);
        $startDateConv = date('Y-m-d', ($startDate/1000));
        $endDateConv = date('Y-m-d', ($endDate/1000));
        $date = $startDateConv.' - '.$endDateConv;
    }

    /*
   if(isset($_GET['dest']) && $_GET['dest'] != '')
   {
       $dest2 = $_GET['dest'];
       $dests = explode(',', $dest2);
       $dest = ' AND (';
       for($i = 0; $i < count($dests); $i++)
       {
           $dest .= 'dest:'.$dests[$i].' OR ';
       }
       $dest = substr($dest, 0, -4);
       $dest .= ')';
       // (dest:27837788774* OR dest:27833928610*)
   }
   else
   {
       $dest = "";
       $dest2 = "";
   }

   if(isset($_GET['mcc']) && $_GET['mcc'] != '')
   {
       $mcc = ' AND mccmnc:'.$_GET['mcc'];
       $mcc2 = $_GET['mcc'];
   }
   else
   {
       $mcc = "";
       $mcc2 = "";
   }

   if(isset($_GET['cont']) && $_GET['cont'] != '')
   {
       $cont = ' AND content:'.$_GET['cont'];
       $cont2 = $_GET['cont'];
   }
   else
   {
       $cont = "";
       $cont2 = "";
   }

   if(isset($_GET['serv']) && $_GET['serv'] != '')
   {
       $serv = ' AND service_login:'.$_GET['serv'];
       $serv2 = $_GET['serv'];
   }
   else
   {
       $serv = "";
       $serv2 = "";
   }

   if(isset($_GET['smsc']) && $_GET['smsc'] != '')
   {
       $smsc = ' AND smsc:'.$_GET['smsc'];
       $smsc2 = $_GET['smsc'];
   }
   else
   {
       $smsc = "";
       $smsc2 = "";
   }

   $query = $dest.$mcc.$cont.$serv.$smsc;
   if(substr($query, 0, 5) == ' AND ')
   {
       $query = substr($query, 5);
   }

   if($query == '')
   {
       $query = '*';
   }
   */

    //query now passed to this function directly from the page
    $query = $_GET['query'];

    $ordering = 'ASC';
    $units = 0;

    if(isset($_GET['sort']))
    {
        $sort = urldecode($_GET['sort']);
        $ordering = strstr($sort, ':');
        $ordering = trim($ordering, ':');
        $ordering = trim($ordering, '"');
        $sortUrl = urlencode($sort);
    }
    else
    {
        $sort = '"timestamp":"desc"';
        $ordering = strstr($sort, ':');
        $ordering = trim($ordering, ':');
        $ordering = trim($ordering, '"');
        $sortUrl = urlencode($sort);
    }

        
        
    $fileNa = 'Search_Report';

    $reportName = 'All';

    $starting = 0;
    $total = $_GET['total'];

    if(isset($_GET['admin']) && $_GET['admin'] == 1)
    {
        $admin = 1;
    }
    else
    {
        $admin = 0;
    }
    

    
    //$nl = "\n";
    $dateR = date("Y-m-d_H:i:s");
    $fileN = $fileNa."_".$dateR;
    $file = "../tempFiles/".$fileN.".csv";
    //$csvFile = fopen($file, "a+") or die("Unable to open file!");

    set_time_limit(180);

    //Download headers
    //header('Content-type: text/plain');
    header('Content-type: application/vnd.ms-excel');
    header('Content-Disposition: attachment; filename="' . basename($file)) . '"';
    header('Expires: 0');
    header('Cache-Control: must-revalidate');
    header('Pragma: public');
    //Enable implicit flush & gz compression
    ob_end_clean();
    ob_implicit_flush();
    ob_start("ob_gzhandler");
    //Open out stream
    $php_out = fopen('php://output', 'w');
    fputcsv($php_out, array('sep=,'), "\t");
    
    //Write header
    if ($admin == 1) 
    {
        //$columns = array('SMS_id', 'Time Sent', 'Time Dlr', 'MCCMNC', 'SMSC', 'Service', 'Source', 'Destination', 'Content', 'RDNC', 'Status', 'Ported');
        $columns = array('SMS_id', 'Time Sent', 'MCCMNC', 'Service', 'Source', 'Destination', 'Content', 'Status');
    }
    else
    {
        $columns = array('SMS_id', 'Time', 'MCCMNC', 'Service', 'Source', 'Destination', 'Content', 'Status');
    }
    fputcsv($php_out, $columns);
    //Flush data
    ob_flush();
    //Output results
    define('ELASTIC_CHUNK_SIZE', 10000);
    for ($starting; $starting < $total; $starting += ELASTIC_CHUNK_SIZE) 
    {
        getElastData($starting, $query, $startDateConv, $endDateConv, $admin, $php_out, ELASTIC_CHUNK_SIZE);
    }
    //Close  output stream, flush, exit
    fclose($php_out);
    ob_flush();
    flush();
    die();

    function getElastData($starting, $query, $startDateConv, $endDateConv, $admin, $handle, $chunk_size)
    {

        $qry = <<<JSONGOESHERE
{
                  "size":$chunk_size,
                  "from":$starting,
                  "sort": {"mtsms_id":"desc"},
                  "fields" : ["service_login","mtsms_id","timestamp","src","dest","content","dlr","mccmnc","smsc","rdnc","meta.ported","dlr_timestamp"],
                  "query": {
                    "filtered": {
                      "query": {
                        "bool" : {
                            "must" : [
                                {
                                 "query_string": {
                                 "query": "$query",
                                 "analyze_wildcard": true
                                    }
                                },
                                {
                                  "range": {
                                    "timestamp": {
                                      "gte": "$startDateConv",
                                      "lte": "$endDateConv"
                                    }
                                  }
                                }
                            ]
                        }
                      }
                    }
                  }  
                }
JSONGOESHERE;

        $allStats = runRawMTSMSElasticsearchQuery($qry);

        writeToFile($allStats, $admin, $handle);
    }

    //echo $qry.$nl;
    function writeToFile($allStats, $admin, $handle)
    {
        foreach ($allStats as $key => $value) 
        {
            if($key == 'hits')
            {
                $hits = count($value['hits']);
                for($i=0; $i < $hits; $i++) 
                {
                    $netw = ($value['hits'][$i]['fields']['mccmnc']['0']);
                    $smsc = $value['hits'][$i]['fields']['smsc']['0'];
                    $posSmsc = strrpos($smsc, '_');
                    $smsc = ucfirst(substr($smsc, 0, $posSmsc));
                    $time = str_replace('T', ' ', $value['hits'][$i]['fields']['timestamp']['0']);
                    $time = strstr($time, '+', true);
                    $time2 = str_replace('T', ' ', $value['hits'][$i]['fields']['dlr_timestamp']['0']);
                    $time2 = strstr($time2, '+', true);

                    $dlrPos = $value['hits'][$i]['fields']['dlr']['0'];
                    $dlrPos = DeliveryReportHelper::dlrMaskToString($dlrPos);
//                    if(($dlrPos & 1) == 1)
//                    {
//                        $dlrPos = 'Delivered';
//                    }
//                    elseif(($dlrPos & 32) == 32 && ($dlrPos & 1) == 0 && ($dlrPos & 2) == 0)
//                    {
//                        $dlrPos = 'Pending';
//                    }
//                    elseif(($dlrPos & 2) == 2)
//                    {
//                        $dlrPos = 'Failed';
//                    }
                    
                    $rdnc = $value['hits'][$i]['fields']['rdnc']['0'];

                    if($admin == 1)
        			{
                    	$csvData = array(
                            $value['hits'][$i]['fields']['mtsms_id']['0'],
                            $time,
                            //$time2,
                            $value['hits'][$i]['fields']['mccmnc']['0'],
                            //$smsc,
                            $value['hits'][$i]['fields']['service_login']['0'],
                            $value['hits'][$i]['fields']['src']['0'],
                            $value['hits'][$i]['fields']['dest']['0'],
                            preg_replace('/[^(\x20-\x7F)]*/','', $value['hits'][$i]['fields']['content']['0']),
                            //$rdnc,
                            $dlrPos,
                            //$value['hits'][$i]['fields']['meta.ported']['0']
                        );
                    }
                    else
                    {
                    	$csvData = array(
                            $value['hits'][$i]['fields']['mtsms_id']['0'],
                            $time,
                            $value['hits'][$i]['fields']['mccmnc']['0'],
                            $value['hits'][$i]['fields']['service_login']['0'],
                            $value['hits'][$i]['fields']['src']['0'],
                            $value['hits'][$i]['fields']['dest']['0'],
                            preg_replace('/[^(\x20-\x7F)]*/','', $value['hits'][$i]['fields']['content']['0']),
                            $dlrPos
                        );
                    }
                    fputcsv($handle, $csvData);
                }
            }
        }
        ob_flush();
    }

    /*for($starting; $starting < $total; $starting += 5000)
    {
        getElastData($starting, $cl, $qryStr, $searchTerm, $startDate, $endDate, $admin, $csvFile, $nl, $file);
        echo '<br>'.$starting;
        flush();
    }

    header('Content-Type: application/csv');
    header('Content-Disposition: attachment; filename='.basename($file).'.csv');
    header("Cache-Control: no-cache, no-store, must-revalidate"); // HTTP 1.1
    header("Pragma: no-cache");
    header("Expires: 30000");
    readfile("../tempFiles/".$fileN.".csv");*/

?>
