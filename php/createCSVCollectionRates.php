<?php
require_once("helperRouting.php");
if(( isset($_GET["country"]) && !empty($_GET["country"])) && (isset($_GET["collection"]) && !empty($_GET["collection"])))
{

    $country = $_GET["country"];
    $collection = $_GET["collection"];
    $filename = 'CollectionRates_('.$country.')('.$collection.').csv';

    header('Content-Type: application/csv');
    header('Content-Disposition: attachment; filename="'.$filename.'";');

    $output = fopen('php://output', 'w');
    //ID, Country, Network, WholeSale, Direct
    $headerColumns = array("ID", "Country", "Network","WholeSale_Cost(+15%)","WholeSale_Cost(R+15%)","Direct_Cost(+25%)", "Direct_Cost(R+25%)");
    fputcsv($output, $headerColumns, ',');

    $collectionsData = getCollectionsInternationalByCountryAndCollection($country,$collection);
    if(isset($collectionsData) && count($collectionsData) > 0)
    {
        //if collectinsData is set and contains some data, get exchange rate
        $exchangeRate = get_currency('EUR', 'ZAR', 1);

        foreach($collectionsData as $collectionItem)
        {
            //Wholesale cost value + 15%
            if ((strpos($collectionItem["cdr_comments"],"eur") !== false)){
                $wholeSalePrice = number_format(($collectionItem['cdr_cost_per_unit'] / 10000) * 1.20, 4, '.', '');
            }else{
                $wholeSalePrice = number_format((($collectionItem['cdr_cost_per_unit'] / 10000) * 1.20) / $exchangeRate, 4, '.', '');
            }
            //Wholesale cost value R + 15%
            if((strpos($collectionItem["cdr_comments"],"eur") !== false)){
                $wholeSaleSecondPrice = number_format((($collectionItem['cdr_cost_per_unit'] / 10000) * 1.20) * $exchangeRate, 4, '.', '');
            }else{
                $wholeSaleSecondPrice = number_format(($collectionItem['cdr_cost_per_unit'] / 10000) * 1.20, 4, '.', '');
            }

            //Direct cost value + 25%
            if((strpos($collectionItem["cdr_comments"],"eur") !== false)){
                $directSalePrice =  number_format(($collectionItem['cdr_cost_per_unit'] / 10000) * 1.30, 4, '.', '');
            }else{
                $directSalePrice =  number_format((($collectionItem['cdr_cost_per_unit'] / 10000) * 1.30) / $exchangeRate, 4, '.', '');
            }
            //Direct cost value R + 25%
            if((strpos($collectionItem["cdr_comments"],"eur") !== false)){
                $directSaleSecondPrice = number_format((($collectionItem['cdr_cost_per_unit'] / 10000) * 1.30) * $exchangeRate, 4, '.', '');
            }else{
                $directSaleSecondPrice = number_format(($collectionItem['cdr_cost_per_unit'] / 10000) * 1.30, 4, '.', '');
            }

            $row = array(
                $collectionItem["cdr_id"],
                $collectionItem["prefix_country_name"],
                $collectionItem["prefix_network_name"],
                $wholeSalePrice,
                $wholeSaleSecondPrice,
                $directSalePrice,
                $directSaleSecondPrice

            );
            fputcsv($output, $row, ',');
        }
    }

}else{
        echo "Country and Collection is not set.";
}

function get_currency($from_Currency, $to_Currency, $amount) {

    $amount = urlencode($amount);
    $from_Currency = urlencode($from_Currency);
    $to_Currency = urlencode($to_Currency);

    $url = "http://www.google.com/finance/converter?a=$amount&from=$from_Currency&to=$to_Currency";

    $ch = curl_init();
    $timeout = 0;
    curl_setopt($ch, CURLOPT_URL, $url);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);

    curl_setopt($ch, CURLOPT_USERAGENT, "Mozilla/4.0 (compatible; MSIE 8.0; Windows NT 6.1)");
    curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, $timeout);
    $rawdata = curl_exec($ch);
    curl_close($ch);
    $data = explode('bld>', $rawdata);
    $data = explode($to_Currency, $data[1]);

    return round($data[0], 2);
}