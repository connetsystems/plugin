<?php

try {
   //always start that session so we have access to it
   session_start();
   $json_response = array();
   //make sure we include our DB helper so we don't have to open our pesky DB connection ourself
   require_once('config/dbConn.php');

   //open the DB object
   $conn = getPDOMasterCore();

   //get our task
   $mimic_task = $_POST['mimic_task'];

   if (!isset($mimic_task)) {
      $json_response['success'] = false;
      $json_response['error'] = "no_task";

      //json encode the array before returning so it can be parsed by the JS
      echo json_encode($json_response);
      return;
   }

   if ($mimic_task == 'start' && isset($_SESSION['userId']) && isset($_POST['user_id_to_mimic']) && $_SESSION['userSystemAdmin'] == 1) {
      //THE USER WANTS TO MIMIC A CLIENT USER, SO LETS LOG THEM IN AS SUCH
      loginMimic($_POST['user_id_to_mimic'], $_SESSION['userId']);

      $json_response['success'] = true;

      //json encode the array before returning so it can be parsed by the JS
      echo json_encode($json_response);
      return;
   } else if ($mimic_task == 'stop' && isset($_SESSION['mimicUserId'])) {

      $json_response['user_id'] = $_SESSION['userId'];

      //THE USER WANTS TO STOP THE MIMIC PROCESS AND GO BACK TO NORMAL, SO LETS DO THAT
      revokeMimicLogin();

      $json_response['success'] = true;

      //json encode the array before returning so it can be parsed by the JS
      echo json_encode($json_response);
      return;
   }
} catch (Exception $e) {

   //return success
   $json_response = array(
       'success' => false,
       'error_string' => 'There was a server error. Please try again later. File: ' . $e->getFile() . ' Line:' . $e->getFile(),
       'trace' => $e->getTraceAsString(),
       'error' => $e->getMessage()
   );

   //json encode the array before returning so it can be parsed by the JS
   echo json_encode($json_response);
   return;
}

/**
 * This function restores an administrators session after cancelling the mimic mode
 * @param $user_id
 * @param $user_id
 */
function revokeMimicLogin() {
   $session_cache = $_SESSION['mimicCache'];
   session_unset();

   foreach ($session_cache as $key => $value) {
      if ($key != 'mimicCache' && $key != 'mimicUserId' && $key != 'mimicUserName') {
         $_SESSION[$key] = $value;
      }
   }

   return true;
}

function loginMimic($user_id_to_mimic, $user_id) {
   $_SESSION['mimicUserId'] = $user_id;
   $_SESSION['mimicUserName'] = $_SESSION['userName'];

   $session_cache = array();
   foreach ($_SESSION as $key => $value) {
      $session_cache[$key] = $value;
   }
   $_SESSION['mimicCache'] = $session_cache;

   $conn = connToMasterCore();

   //prepare the select query so no one can insert some pesky SQL through the URL

   $stmt = $conn->prepare("SELECT user_id, user_username, user_password_hashed, user_account_id, user_system_admin, user_default_service_id, ifnull(service_id_reseller,0) as 'service_id_reseller', service_name, login_logo, header_logo, tab_logo, account_name, account_colour, service_credit_available, service_credit_billing_type, account_status, service_status, user_status
                    FROM core.core_user u
                    LEFT JOIN core.core_service s
                    ON u.user_default_service_id = s.service_id
                    LEFT JOIN core.core_account a
                    ON u.user_account_id = a.account_id
                    LEFT JOIN core.core_account_settings t
                    ON u.user_account_id = t.account_id
                    INNER JOIN core.core_service_credit c
                    ON s.service_id = c.service_id
                    WHERE user_id = ?");


   //check if the statement was successfully init'd and die if it wasn't
   if (!$stmt) {
      header("Location: /index.php?err=1");
   }

   //set the username in the statement
   $stmt->bind_param('i', $user_id_to_mimic);

   //run the statement
   $stmt->execute();

   //get the result set if it exists
   $result = $stmt->get_result();

   //die if no result was given, means the username is wrong or doesn't exist
   if (!$result) {
      //NB need better output than this
      header("Location: /index.php?err=1");
      return;
   }

   //get the username row and compare it to the password
   while ($row = $result->fetch_assoc()) {
      //everything passed, let's save the user's details in the session for later use
      $_SESSION['userId'] = $row['user_id'];
      $_SESSION['userName'] = $row['user_username'];
      $_SESSION['serviceName'] = $row['service_name'];
      $_SESSION['accountName'] = $row['account_name'];
      $_SESSION['serviceCredit'] = $row['service_credit_available'];
      $_SESSION['serviceBillingType'] = $row['service_credit_billing_type'];
      $_SESSION['randomSendCheck'] = rand(0, 1000);
      $_SESSION['userSystemAdmin'] = $row['user_system_admin'];

      //set the colour scheme for the user if it is configured
      if (isset($row['account_colour'])) {
         $_SESSION['accountColour'] = $row['account_colour'];
         // $accRGB = $_SESSION['accountColour'];
      } else {
         $_SESSION['accountColour'] = '#cc3322';
      }

      //set the login logo for the user if it is configured
      if (isset($row['login_logo'])) {
         $_SESSION['loginLogo'] = $row['login_logo'];
         //$headerLogo = $_SESSION['headerLogo'];
      } else {
         $_SESSION['loginLogo'] = '../img/skinlogos/1/1.png';
         //$headerLogo = '../img/skinlogos/default/ConnetHeader.png';
      }

      //set the header logo for the user if it is configured
      if (isset($row['header_logo'])) {
         $_SESSION['headerLogo'] = $row['header_logo'];
         //$headerLogo = $_SESSION['headerLogo'];
      } else {
         $_SESSION['headerLogo'] = '../img/skinlogos/1/1_header.png';
         //$headerLogo = '../img/skinlogos/default/ConnetHeader.png';
      }

      //set the tab logo for the user if it is configured
      if (isset($row['tab_logo'])) {
         $_SESSION['tabLogo'] = $row['tab_logo'];
         //$tabLogo = $_SESSION['tabLogo'];
      } else {
         $_SESSION['tabLogo'] = '../img/skinlogos/1/1_icon.png';
         //$tabLogo = '../img/skinlogos/default/ConnetIcon.png';
      }

      //does the user have an associated account ID
      if (isset($row['user_account_id'])) {
         $_SESSION['accountId'] = $row['user_account_id'];
      } else {
         header("Location: /index.php?err=2");
      }

      //set the users service ID
      if (isset($row['user_default_service_id'])) {
         $_SESSION['serviceId'] = $row['user_default_service_id'];
      } else {
         if ($row['user_account_id'] == 1) {
            $_SESSION['serviceId'] = '';
         } else {
            header("Location: /index.php?err=3");
         }
         $_SESSION['serviceId'] = '';
      }

      //set the reseller ID if applicable
      if (isset($row['service_id_reseller'])) {
         $_SESSION['resellerId'] = $row['service_id_reseller'];
      } else {
         $_SESSION['resellerId'] = '';
      }


      $stmt_role = $conn->prepare("SELECT r.role_id, role_name, role_url, role_icon
                    FROM core.core_user_role r
                    INNER JOIN core.core_role rl
                    ON r.role_id = rl.role_id
                    WHERE user_id = ?
                    ORDER BY role_order,role_id");

      if ($stmt_role) {
         $stmt_role->bind_param('i', $_SESSION['userId']);
         $stmt_role->execute();
         //get the result set if it exists
         $roleTmp = $stmt_role->get_result();
         $stmt_role->fetch();
         $stmt_role->close();
      }

      $rol;
      $i = 0;
      foreach ($roleTmp as $key => $value) {
         if ($value['role_id'] == '1') {
            $_SESSION['settings'] = 1;
         } else {
            $rol[$i] = $value;
            $i++;
         }
      }

      ($rol);

      $_SESSION['roles'] = $rol;
   }

   $stmt->close();
   $conn->close();

   return true;
}
