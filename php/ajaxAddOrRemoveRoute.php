<?php
    require_once("allInclusive.php");

    try
    {
        session_start();

        //check if the user is logged in, if not, redirect back to login
        if(!isset($_SESSION['userId']))
        {
            die();
        }

        if(isset($_POST['task']))
        {


            if(isset($_POST['task']) && $_POST['task'] == "remove")
            {
                $remove_id = $_POST['routeId'];
                removeRouteFromSubAccount($remove_id);
            }

            if(isset($_POST['task']) && $_POST['task'] == "add")
            {
                //get the relevant values
                $service_id = $_POST['serviceId'];
                $route_id = $_POST['routeId'];
                $route_rate = $_POST['routeRate'];

                $resellerRoutes = getResellerDefaultRouteData($_SESSION['serviceId'], $service_id);
                $route_to_add = null;
                if(isset($resellerRoutes))
                {
                    foreach ($resellerRoutes as $route)
                    {
                        if($route['route_id'] == $route_id)
                        {
                            $route_to_add = $route;
                        }
                    }
                }

                if(isset($route_to_add))
                {
                    createNewClientRoutes($route_rate, $service_id, $_SESSION['serviceId'], $route_to_add['route_description'], $route_to_add['provider_id'], $route_to_add['route_msg_type'], $route_to_add['route_display_name'], $route_to_add['route_code'], $route_to_add['STATUS'], $route_to_add['route_match_regex'], $route_to_add['route_match_priority'], $route_to_add['MCCMNC'], $route_to_add['route_collection_id']);
                }
            }

            $result = array('success' => true);
        }
        else
        {
            $result = array('success' => false);
        }
    }
    catch(Exception $e)
    {
        $result = array('success' => false, 'message' => $e->getMessage());
    }

    //json encode the array before returning so it can be parsed by the JS
    echo json_encode($result);

