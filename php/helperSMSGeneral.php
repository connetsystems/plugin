<?php
/**
 * Class helperSMSGeneral
 * @author - Doug Jenkinson
 *
 * Simple class stores general tasks and functions related to our system, such as string handling
 */

class helperSMSGeneral
{

    /**
     * Checks if the number entered is a valid MSISDN according to it's formatting (NOT MNP)
     * (I'm sure this function can be improved.)
     * @param $msisdn - THe phone number we are validating
     * @return boolean - True or false for validation
     */
    static function validateMSISDN($msisdn)
    {
        return is_numeric($msisdn);
    }

    /**
     * Safely calulcates the amount of characters (length) of an SMS string.
     * @param $sms_text
     * @return int
     */
    static function getSMSCharacterCount($sms_text)
    {
        //fix the line breaks
        $sms_text = preg_replace( "/\r\n/", "\n", $sms_text );

        //get the SMS length and units
        $sms_length = mb_strlen($sms_text) + preg_match_all('/[\\^{}\\\~€|\\[\\]]/mu', $sms_text);

        return $sms_length;
    }

    /**
     * Safely calcluates how many units to send for an SMS of a particular character length.
     * @param $sms_length
     * @return float
     */
    static function getSMSUnits($sms_length)
    {
        if ($sms_length <= 160)
        {
            $units = ceil($sms_length / 160);
        }
        else
        {
            $units = ceil($sms_length / 153);
        }

        return $units;
    }

    /**
     * Helper function that cleans up the encoding of a string for a CSV file to insure it will pass through our system correctly
     * @param $string - The string to clean up
     * @return String $string - The cleaned up string.
     */
    static function clean_string_encoding($string)
    {
        //convert smart quotes and dashes
        $search = array(chr(145),
            chr(146),
            chr(147),
            chr(148),
            chr(151),
            chr(150));

        $replace = array("'",
            "'",
            '"',
            '"',
            '-',
            '-');

        $string = str_replace($search, $replace, $string);

        $string = str_replace(chr(0xE2).chr(0x82).chr(0xAc), "E", $string); //for EURO
        $string = str_replace(chr(128), 'E', $string); //for EURO
        $string = mb_convert_encoding(trim($string), "UTF-8"); //convert encoding
        return $string;
    }

    /**
     * Helper function that cleans up newlines (and other characters?) in a string so it can be better output to a CSV file
     * @param $string - The string to clean up
     * @return String $string - The cleaned up string.
     */
    static function cleanStringForCSV($string)
    {
        $string = trim(preg_replace('/\s+/', ' ', $string));
        return $string;
    }

    /**
     * This function returns an array of placeholder numbers, in their exact order, extracted from a SMS string
     * EG: array('2', '4', '3') will be the resultif a string is sent "Hello {2}, {4} then {3}"
     *
     * @param $sms_text - THe text containing placeholders to be anaylzed
     * @return array $placeholder_numbers - The array containing each placeholder number in the order that they occur in the string
     */
    static function getSMSPlaceholderNumbersAsArray($sms_text)
    {
        //check the SMS text
        $placeholder_count = 0;
        $placeholder_numbers = array(); //this holds our number order eg {2} {5} {4} ect
        if (isset($sms_text) && $sms_text != '')
        {
            $ps = strpos($sms_text, '{');
            $pe = strpos($sms_text, '}');

            if (($ps !== false) && ($pe !== false))
            {
                $psC = substr_count($sms_text, '{');
                $peC = substr_count($sms_text, '}');
                if ($psC == $peC)
                {
                    //placeholder exist
                    $placeholder_count = $psC;
                }
            }

            $last_pos = 0; //holds the position of what last placeholder we just analyzed
            //go through each data column and insert the conetent into the message so we can get an idea of length
            for ($i = 0; $i < $placeholder_count; $i++)
            {
                $pos_open = strpos($sms_text, '{', $last_pos);
                $pos_close = strpos($sms_text, '}', $last_pos);

                $placeholder_num = substr($sms_text, $pos_open + 1, ($pos_close - ($pos_open + 1)));

                //make sure it is actually a valid int
                if ((string)(int) $placeholder_num == $placeholder_num)
                {
                    //echo '<br/>'.$placeholder_num;
                    array_push($placeholder_numbers, $placeholder_num);
                }

                clog::info('Placeholders to process and replace:  '.json_encode($placeholder_numbers));
                $last_pos = $pos_close + 1;
            }
        }

        return $placeholder_numbers;
    }

    /**
     * This function simply takes sms messages and converts the placeholder numbers to be 4 more than their current values
     * This is required because post processed bulk CSV files add 4 columns of data at the beginning of each row, so the placeholders and the data to be inserted becomes incorrectly matched.
     * eg: "Hello {2} and {3}" becomes "Hello {6} and {7}"
     * @param $sms_text - The text to convert
     * @return string $sms_text - The text with the converted placeholders
     */
    static function addFourToAllPlaceholderNumbers($sms_text)
    {
        $placeholder_numbers = helperSMSGeneral::getSMSPlaceholderNumbersAsArray($sms_text);
        $replace_placeholder_array = array();

        //we need to check for placeholders and increment them by 4
        for($i = 0; $i < count($placeholder_numbers); $i ++)
        {
            //check if the slot {1} or {2} ect exists and then add 4 to it in order to compensate for niel 4 extra fields
            $placeholder_string = '{'.$placeholder_numbers[$i].'}';

            if(strpos($sms_text, $placeholder_string) !== false)
            {
                $replace_placeholder_string = '{'.($placeholder_numbers[$i] + 4).'}';
                $replace_placeholder_array[$placeholder_string] = $replace_placeholder_string;
            }
        }

        //now replace all the strings
        $sms_text = strtr($sms_text, $replace_placeholder_array);
        return $sms_text;
    }

    /**
     * This function simply takes post processed messages and converts the placeholders to be minus 4 of their current values.
     * eg: "Hello {6} and {7}" becomes "Hello {2} and {3}"
     * @param $sms_text - The text to convert
     * @return string $sms_text - The text with the converted placeholders
     */
    static function restorePlaceholderValuesForDisplay($sms_text)
    {
        $placeholder_numbers = helperSMSGeneral::getSMSPlaceholderNumbersAsArray($sms_text);
        $replace_placeholder_array = array();

        //we need to check for placeholders and increment them by 4
        for($i = 0; $i < count($placeholder_numbers); $i ++)
        {
            //check if the slot {1} or {2} ect exists and then add 4 to it in order to compensate for niel 4 extra fields
            $placeholder_string = '{'.$placeholder_numbers[$i].'}';

            if(strpos($sms_text, $placeholder_string) !== false)
            {
                $replace_placeholder_string = '{'.($placeholder_numbers[$i] - 4).'}';
                $replace_placeholder_array[$placeholder_string] = $replace_placeholder_string;
            }
        }

        //now replace all the strings
        $sms_text = strtr($sms_text, $replace_placeholder_array);
        return $sms_text;
    }

    /**
     * Helper function that determines the delimiter of a send CSV file uploaded by a user
     * @param $filename - The name of the CSV file to process
     * @return string $delimiter - The delimiter of the CSV file
     */
    static function getDelimiter($filename)
    {
        $correct_delims = array(',', ';', '\t', chr(9), '|');

        $delimiter = '';
        $handle2 = fopen($filename, "r");

        $max_chars = 0;

        if($handle2)
        {
            while (($line = fgets($handle2)) !== false)
            {
                for($i = 0; $i < strlen($line); $i++)
                {
                    //strip away the newlines as we don't need them
                    $line = trim(preg_replace('/\s+/', ' ', $line));

                    //find out what the maximum length of each line might be
                    if(mb_strlen($line) > $max_chars)
                    {
                        $max_chars = mb_strlen($line);
                    }

                    //check if the line is not a number and is a valid delimeter
                    if(!is_numeric($line[$i]) && in_array($line[$i], $correct_delims))
                    {
                        $delimiter = $line[$i];
                        break 2;
                    }
                }
            }
            fclose($handle2);
        }

        //if delimeter isn't found then assume comma, useful when processing files with only one column
        if($delimiter == '' && $max_chars >= 11)
        {
            $delimiter = ',';
        }

        return $delimiter;
    }
}