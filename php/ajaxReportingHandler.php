<?php
    /**
     * This script is called by the manage services page, and runs certain ajax based tasks
     * @author - Doug Jenkinson
     */
    require_once("allInclusive.php");
    require_once("../PluginAPI/PluginAPIAutoload.php");

    try
    {
        session_start();
        $service_helper = new sqlHelperServices();

        //check if the user is logged in, if not, redirect back to login
        if(!isset($_SESSION['userId']))
        {
            $result = array('success' => false, 'reason' => 'session_expired');
            echo json_encode($result);
            die();
        }

        if(isset($_POST['task']))
        {
            $task = $_POST['task'];
            $result['success'] = false;
            switch($task)
            {
                case "get_daily_send_chart_data_by_date" :
                    //get the relevant values
                    $service_id = $_POST['serviceId'];
                    $start_date = $_POST['startDate'];
                    $end_date = $_POST['endDate'];

                    //if the date range is 2 days or less, we want just a little more data to make the graphs look legit
                    $start_date_obj = new DateTime($start_date);
                    $end_date_obj = new DateTime($end_date);

                    $interval = 'day';
                    $days = $end_date_obj->diff($start_date_obj)->format("%a");
                    if($days < 3)
                    {
                        $start_date = $start_date_obj->modify('-1 day')->format("Y-m-d");
                        $end_date = $end_date_obj->modify('+2 day')->format("Y-m-d");

                    }
                    else if($days > 28)
                    {
                        $interval = 'week';
                    }
                    else if($days > 112)
                    {
                        $interval = 'month';
                    }

                    //get all the data for the graph by day
                    $daily_sends = Service::getSendsOverTimeWithDelivery($service_id, $start_date, $end_date, $interval);

                    $result['labels'] = array();
                    $result['daily_sent'] = array();
                    $result['daily_delivered'] = array();
                    foreach($daily_sends as $date => $stats)
                    {
                        array_push($result['labels'],  date('d-M-y', strtotime($date)));
                        array_push($result['daily_sent'], $stats['send_total']);
                        array_push($result['daily_delivered'], $stats['delivered']);
                    }

                    $result['success'] = true;
                    break;
                case "get_network_traffic_stats" :
                    //get the relevant values
                    $service_id = $_POST['serviceId'];
                    $start_date = $_POST['startDate'];
                    $end_date = $_POST['endDate'];

                    //get all the data for the graph by day
                    $network_stats = Service::getFullNetworkSendStats($service_id, $start_date, $end_date);

                    $result['network_stats'] = $network_stats;
                    $result['success'] = true;
                    break;
            }
        }
        else
        {
            $result = array('success' => false);
        }
    }
    catch(Exception $e)
    {
        $result = array('success' => false, 'message' => $e->getMessage());
    }

    //json encode the array before returning so it can be parsed by the JS
    echo json_encode($result);

