<?php
    session_start();

    //check if the user is logged in, if not, redirect back to login
    if (!isset($_SESSION['userId'])) {
        //nope nope nope nope nope
        header('Location: /index.php');
        die();
    }

    require_once("allInclusive.php");
    require_once("../PluginAPI/PluginAPIAutoload.php");

    //get the data for the csv file
    if(isset($_GET['service_id']) && isset($_GET['start_date']) && isset($_GET['end_date']))
    {
        $service_id = $_GET['service_id'];
        $start_date = $_GET['start_date'];
        $end_date = $_GET['end_date'];

        //get the service name for the file name
        if(isset($_GET['service_name']) && $_GET['service_name'] != "")
        {
            $service_name = $_GET['service_name'];
        }
        else
        {
            $service_name =  getServiceName($service_id);
        }
        $service_name = preg_replace("/[^a-zA-Z0-9\-\_]/", ' ', $service_name);

        $filename = 'API TRAFFIC MOSMS ('.$service_name.') ['.$start_date.' - '.$end_date.'].csv';

        header('Content-Type: application/csv');
        header('Content-Disposition: attachment; filename="'.$filename.'";');

        $output = fopen('php://output', 'w');
        //fputcsv($output, array('sep=,'), "\t");

        $columns = array("MOSMS ID", "Timestamp", "MSISDN", "Content");
        fputcsv($output, $columns, ',');

        //get the scroll ID so wse can scan over the data and write it out
        $scroll_id = MOSMS::getScrollIDByServiceId($service_id, 100, $start_date, $end_date);

        $still_data = true;
        while($still_data)
        {
            //get this set of results
            $scroll_results = MOSMS::getMOsByScrollId($scroll_id);

            if(count($scroll_results['objects']) > 0)
            {
                //get the new scroll ID to use on the next pass of the while loop
                $scan_scroll_id = $scroll_results['scroll_id'];

                //cycle through this set of data nad write it to file
                foreach($scroll_results['objects'] as $mo_data)
                {
                    $line = array();
                    //output the data for CSV reporter
                    array_push($line, $mo_data->mosms_id);
                    array_push($line, str_replace('T', ' ', strstr($mo_data->timestamp, '+', true)));
                    array_push($line, $mo_data->src);
                    array_push($line, helperSMSGeneral::cleanStringForCSV($mo_data->content));

                    fputcsv($output, $line, ',');
                }
            }
            else
            {
                $still_data = false;
            }
        }

        //close output stream
        fclose($output);

    }
    else {
        echo "Invalid variables.";
    }


