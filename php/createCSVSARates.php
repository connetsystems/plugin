<?php
require_once("helperRouting.php");
if(isset($_GET['mcc']) && isset($_GET['collection']) && isset($_GET['network']) && isset($_GET['range'])) {

    $country = $_GET["country"];
    $collection = $_GET["collection"];
    $filename = "South Africa - Routes/Rates.csv";
    header('Content-Type: application/csv');
    header('Content-Disposition: attachment; filename="' . $filename . '";');

    $output = fopen('php://output', 'w');
    $headerColumns = array("Service Name", "Service id", "Network", "Country", "Rate", "Units", "Cur", "Default Cost", "Default", "Ported Cost", "Ported", "Collection", "Status", "Manager");
    fputcsv($output, $headerColumns, ',');

    $serviceName = isset($_GET['serviceName']) ? $_GET['serviceName'] : "";
    $mcc = getMCCFromNet($_GET['network']);
    if ((isset($_GET['collection']) && $_GET['collection'] != 'Show All') && (isset($_GET['network']) && $_GET['network'] != 'Show All')) {
        $coll = $_GET['collection'];
        $allServices = getBillingData($mcc, $coll, $serviceName);
    } else if ((isset($_GET['network']) && $_GET['network'] != 'Show All') && (isset($_GET['collection']) && strcasecmp($_GET['collection'], "Show All") == 0)) {
        $allServices = getBillingData($mcc, null, $serviceName);
    } else if ((isset($_GET['collection']) && $_GET['collection'] != 'Show All') && (isset($_GET['network']) && strcasecmp($_GET['network'], "Show All") == 0)) {
        $coll = $_GET['collection'];
        $allServices = getBillingData(null, $coll, $serviceName);
    } else {
        $allServices = getBillingData(null, null, $serviceName);
    }
    $dateRange = explode(" - ",$_GET["range"]);
    $startDate = $dateRange[0];
    $endDate = $dateRange[1];
    $billingUnits = getBillingUnits($startDate, $endDate, $mcc);
    $networks = getNetworkViaCountryName('South Africa');
    $collectionsLimit = array();
    $uniqueSMSCArr = array();
    foreach ($allServices as $key => $value) {
        if (!in_array($value['DEFAULT'], $uniqueSMSCArr)) {
            array_push($uniqueSMSCArr, $value['DEFAULT']);
        }

        if (!in_array($value['COLLECTION'], $collectionsLimit)) {
            $collectionsLimit[] = $value['COLLECTION'];
        }
    }
    $allSmscStr = "";
    for ($i = 0; $i < count($uniqueSMSCArr); $i++) {
        $allSmscStr .= "'" . $uniqueSMSCArr[$i] . "_smsc',";
    }
    $allSmscStr = substr($allSmscStr, 0, -1);
    $allSmscStr = (string) $allSmscStr;
    $colCost = getCollectionCost($mcc, $allSmscStr);
    sort($collectionsLimit, SORT_NATURAL | SORT_FLAG_CASE);
    if (isset($allServices)) {
        foreach ($allServices as $key => $value) {
            $finalUnits = 0;
            $finalDefaultCosts = 0;
            $finalPortedCosts = 0;
            foreach ($billingUnits as $key2 => $billingUnit) {
                if ($billingUnit['service_id'] == $value['service_id']) {
                    if (isset($billingUnit['units']) && $billingUnit['units'] != '') {
                        $finalUnits = $billingUnit['units'];
                        break;
                    }
                }
            }
            foreach ($colCost as $keyX => $valueX) {
                $dv = $value['DEFAULT'] . '_smsc';
                if ($valueX['smsc'] == $dv) {
                    $finalDefaultCosts = $valueX['cost'];
                    break;
                }
            }
            foreach ($colCost as $keyX => $valueX) {
                $dv = $value['PORTED'] . '_smsc';
                if ($valueX['smsc'] == $dv) {
                    $finalPortedCosts = $valueX['cost'];
                    break;
                }
            }
            $row = array(
                $serviceName = $value['Service_Name'],
                $serviceId = $value['service_id'],
                $network = $value['NETWORK'],
                $country = $value['COUNTRY'],
                $rate = $value['RATE'],
                $units = $finalUnits,
                $currency = $value['CURRENCY'],
                $defaultCost = $finalDefaultCosts,
                $default = ucfirst($value['DEFAULT']),
                $ported = $finalPortedCosts,
                $portedCost = ucfirst($value['PORTED']),
                $collection = $value['COLLECTION'],
                $status = $value['STATUS'],
                $manager = $value['FULLNAME']
            );
            fputcsv($output, $row, ',');

        }

    }
}else{
    echo("Country and Collection is not set.");
}
?>