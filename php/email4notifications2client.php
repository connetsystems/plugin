<!DOCTYPE html>
<?php
define('ERROR_REPORTING', (isset($_REQUEST['debug']) && $_REQUEST['debug'] == 'debug132'));
define('LOG_FILENAME', '/var/www/portal/log/log.log');


//--------------------------------------------------------------
// Includes
//--------------------------------------------------------------
require_once('ConnetAutoloader.php');
require_once('config/dbConn.php');
warnlog::enable();

clog::setBacktrace();

//ENABLE LOGGING
if (ERROR_REPORTING) {
    clog::setLogLevel(0);
    clog::setVerbose();
    clog::debug('Debug enabled');
}

define("DEFCALLBACKMAIL", "customer@connet-systems.com"); // WIll be shown as "from".

function getAccountsToMail() {

    //get ref object
    $conn = getPDOMasterCore();

    $query = "SELECT concat(a.account_name,' - ',cs.service_name) as service_name,csc.service_credit_available,service_notification_email,service_notification_sms,cs.service_id
            FROM core.core_service_credit AS csc 
            JOIN core.core_service AS cs ON cs.service_id = csc.service_id 
            JOIN core.core_account AS a ON cs.account_id = a.account_id 
            WHERE (service_notification_threshold <> '')
            AND if(DATE_ADD(service_notification_lastsend, INTERVAL 10 DAY) > now(),'YES','NO') = 'NO'
            AND csc.service_credit_available < service_notification_threshold";

    //prepare the query
    $stmt = $conn->prepare($query);

    //set the fetch mode
    $stmt->setFetchMode(PDO::FETCH_ASSOC);

    //execute statement
    $stmt->execute();

    //fetch data
    $getData = $stmt->fetchAll();

    $conn = null;
    
    return $getData;
}

function preparehtmlmail($html) {

    clog::info('Beginning HTML mail preparation');

    preg_match_all('~<img.*?src=.([\/.a-z0-9:_-]+).*?>~si', $html, $matches);
    $i = 0;
    $paths = array();

    foreach ($matches[1] as $img) {
        $img_old = $img;

        if (strpos($img, "http://") == false) {
            $uri = parse_url($img);
            $paths[$i]['path'] = $_SERVER['DOCUMENT_ROOT'] . $uri['path'];
            $content_id = md5($img);
            $html = str_replace($img_old, 'cid:' . $content_id, $html);
            $paths[$i++]['cid'] = $content_id;
        }
    }

    clog::info('Processing HTML');

    $boundary = "--" . md5(uniqid(time()));
    $headers = "";
    $headers .= "MIME-Version: 1.0\n";
    $headers .="Content-Type: multipart/mixed; boundary=\"$boundary\"\n";
    $headers .= "From: " . DEFCALLBACKMAIL . "\r\n";
    $headers .= 'Cc: customer@connet-systems.com' . "\r\n";
    $multipart = '';
    $multipart .= "--$boundary\n";
    $kod = 'utf-8';
    $multipart .= "Content-Type: text/html; charset=$kod\n";
    $multipart .= "Content-Transfer-Encoding: Quot-Printed\n\n";
    $multipart .= "$html\n\n";

    foreach ($paths as $path) {
        clog::info('Processing HTML IMAGES');
        if (file_exists($path['path']))
            $fp = fopen($path['path'], "r");
        if (!$fp) {
            echo "No file";
            return false;
        }

        $imagetype = substr(strrchr($path['path'], '.'), 1);
        $file = fread($fp, filesize($path['path']));
        fclose($fp);

        $message_part = "";

        switch ($imagetype) {
            case 'png':
            case 'PNG':
                $message_part .= "Content-Type: image/png";
                break;
            case 'jpg':
            case 'jpeg':
            case 'JPG':
            case 'JPEG':
                $message_part .= "Content-Type: image/jpeg";
                break;
            case 'gif':
            case 'GIF':
                $message_part .= "Content-Type: image/gif";
                break;
        }

        $message_part .= "; file_name = \"$path\"\n";
        $message_part .= 'Content-ID: <' . $path['cid'] . ">\n";
        $message_part .= "Content-Transfer-Encoding: base64\n";
        $message_part .= "Content-Disposition: inline; filename = \"" . basename($path['path']) . "\"\n\n";
        $message_part .= chunk_split(base64_encode($file)) . "\n";
        $multipart .= "--$boundary\n" . $message_part . "\n";
    }

    $multipart .= "--$boundary--\n";
    $multipart = wordwrap($multipart, 70);

    clog::info('Done html email');
    return array('multipart' => $multipart, 'headers' => $headers);
}

$dataToSend = getAccountsToMail();

if (isset($dataToSend))
{

    //get db ref object
    $conn = getPDOMasterCore();

    foreach ($dataToSend as $key => $value)
    {
        $sId = $value['service_id'];
        $sN = $value['service_name'];
        $emails = $value['service_notification_email'].',customer@connet-systems.com';
        $creditAvail = number_format($value['service_credit_available'] / 10000, 2, '.', ' ');

        $to = $emails;
        $subject = " Account balance for " . $sN . " is low at Connet";

        $msg = '<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w31.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
                <html>
                <head>
                    <title>' . $subject . '</title>
                </head>
                    <body style="font-family: Arial, sans-serif;    font-stretch: normal;   font-size: 12px;    font-style: normal; width:740px;">
                        <table border="0">
                            <tr>
                                <td>
                                    <img src="http://portal.connet-systems.com/img/Header.jpg"> 
                                </td>
                            </tr>   
                            <tr>
                                <td style="padding-left: 28px; padding-right: 28px;">
                                    <h1 style="color:#3c5e6e;font-size:21px;">Low balance alert from Connet Systems</h1>
                                    <p> The balance for your account is currently R ' . $creditAvail . '.</p>
                                    <p style="width:740px;"> To top up your account please send an email with the amount of credits required and your username to <br>accounts@connet-systems.com for an invoice or please contact your account manager.</p>
                                    <p><br>Kind Regards,</p>
                                    <p><strong>The Connet Systems Team</strong></p>
                                </td>
                            </tr>   
                            <tr>
                                <td>    
                                    <img style="margin-bottom:-100px;" src="http://portal.connet-systems.com/img/Footer.jpg"> 
                                </td>
                            </tr>   
                        </table>
                    </body>
                </html>';


        $msg = wordwrap($msg, 70);

        $final_msg = preparehtmlmail($msg); // give a function your html*
       // $m = mail('jacquesb@connet-systems.com,wayne@connet-systems.com', $subject, $final_msg['multipart'], $final_msg['headers']);
        $m = mail($to, $subject, $final_msg['multipart'], $final_msg['headers']);

        if ($m == 1)
        {
            $data = array(':service_id' => $sId, ':date' => date("Y-m-d H:i:s"));

            $updatequery = "UPDATE core.core_service_credit
                   SET `service_notification_lastsend` =:date
                   WHERE service_id =:service_id ";

            //prepare the query
            $stmt = $conn->prepare($updatequery);

            //execute it
            $stmt->execute($data);
        }
    }
    $conn = null;
}


