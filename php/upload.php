<?php
/**
 * Multiple file upload with progress bar php and jQuery
 * 
 * @author Resalat Haque
 * @link http://www.w3bees.com
 * 
 */

$max_size = 1024*1024*10; // 10 485 760 kb
$extensions = array('png', 'jpg', 'jpeg');
$dir = 'uploads/';
$count = 0;
//echo '<br>start';
if ($_SERVER['REQUEST_METHOD'] == 'GET' and isset($_FILES['files']))
{
//echo '<br>1';
	// loop all files
	foreach ( $_FILES['files']['name'] as $i => $name )
	{
		//echo '<br>FOR -'.$i;
		// if file not uploaded then skip it
		if ( !is_uploaded_file($_FILES['files']['tmp_name'][$i]) )
			continue;

	    // skip large files
		if ( $_FILES['files']['size'][$i] >= $max_size )
		{
			echo '<br>3too big file-'.$i;
		
			continue;
		}
		
		// skip unprotected files
		/*if( !in_array(pathinfo($name, PATHINFO_EXTENSION), $extensions) )
		{
			echo '<br>4-'.$i;
			continue;
		}*/

		// now we can move uploaded files
	    if( move_uploaded_file($_FILES["files"]["tmp_name"][$i], $dir . $name) )
	    {
	    	$count++;
			//echo '<br>UL '.' - '.$count.' = '.count($_FILES["files"]);
	    	if(count($_FILES["files"]) == $count)
	    	{
	    		$totSubmitted = $_FILES["files"];
	    		//successUpload($totSubmitted, $count);
	    	}
	    }
	}
}

/*function successUpload($totSubmitted, $count)
{
	echo "Total Files Submitted = ".count($_FILES["files"])."<br> ";
	//(3);
	echo json_encode(array('Successfully uploaded ' => $count.' files.'));
	// include ("index.php");

	header("Location: index.php?status='success'");
	die();
}*/


?>