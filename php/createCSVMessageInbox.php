<?php
session_start();

//check if the user is logged in, if not, redirect back to login
if (!isset($_SESSION['userId'])) {
   //nope nope nope nope nope
   header('Location: /index.php');
   die();
}

require_once("allInclusive.php");
require_once("../PluginAPI/PluginAPIAutoload.php");

//get the data for the csv file
if(isset($_GET['service_id']) && isset($_GET['start_date']) && isset($_GET['end_date']))
{
   $service_id = $_GET['service_id'];
   $start_date = $_GET['start_date'];
   $end_date = $_GET['end_date'];

   //get the service name for the file name
   if (isset($_GET['service_name']) && $_GET['service_name'] != "")
   {
      $service_name = $_GET['service_name'];
   }
   else
   {
      $service_name = getServiceName($service_id);
   }

   //check for a search query
   if (isset($_GET['search_type']) && isset($_GET['search_string']) && $_GET['search_string'] != "")
   {
      $search_type = $_GET['search_type'];
      $search_string = $_GET['search_string'];
      $is_search = true;
   }
   else
   {
      $is_search = false;
      $search_type = "mosms_content";
      $search_type_display = 'MSOSMS Content';
      $search_string = "";
   }

   $service_name = preg_replace("/[^a-zA-Z0-9\-\_]/", ' ', $service_name);
   $filename = 'Message Inbox ' . $start_date . ' - ' . $end_date . ' (' . $service_name . ').csv';

   header('Content-Type: application/csv');
   header('Content-Disposition: attachment; filename="' . $filename . '";');

   $output = fopen('php://output', 'w');
   //fputcsv($output, array('sep=,'), "\t");

   $columns = array("MOSMS ID", "Time Received", "MOSMS Source", "MOSMS Content",  "Original MTSMS", "MTSMS Source", "Time Sent",  "Campaign Code", "Batch Reference", "Batch ID");
   fputcsv($output, $columns, ',');

   $from = 0;
   $limit = 50;


   //get the initial count
   if($is_search)
   {
      $mo_results = getMOResultsSearch($search_type, $search_string, $service_id, $from, $limit, $start_date, $end_date);
   }
   else
   {
      $mo_results = getMOResults($service_id, $from, $limit, $start_date, $end_date);
   }
   $total_mos_in_search = $mo_results['total_search_hits'];

   if ($total_mos_in_search > 0)
   {
      //cycle through the MOs until all have been output
      $mos_remain = true;
      while ($mos_remain)
      {
         //run this search cycle
         if($is_search)
         {
            $mo_results = getMOResultsSearch($search_type, $search_string, $service_id, $from, $limit, $start_date, $end_date);
         }
         else
         {
            $mo_results = getMOResults($service_id, $from, $limit, $start_date, $end_date);
         }

         $mo_list = $mo_results['objects'];

         //cycle through the MOs and get the associated MTSMS records
         foreach ($mo_list as $key => $mo_sms)
         {
            //create  the line that we will write to CSV
            $line = array();
            array_push($line, $mo_sms->mosms_id);
            array_push($line, str_replace('T', ' ', strstr($mo_sms->timestamp, '+', true)));
            array_push($line, $mo_sms->src);
            array_push($line, helperSMSGeneral::cleanStringForCSV($mo_sms->content));

            //get hte MTSMS information and attach it to the MOSMS
            $mo_mtsms = MTSMS::find($mo_sms->meta['mtsms_id']);

            //write the data out if it exists
            if(isset($mo_mtsms))
            {
               array_push($line, helperSMSGeneral::cleanStringForCSV($mo_mtsms->content));
               array_push($line, $mo_mtsms->src);
               array_push($line, str_replace('T', ' ', strstr($mo_mtsms->timestamp, '+', true)));
            }
            else
            {
               array_push($line, 'n/a');
               array_push($line, 'n/a');
               array_push($line, 'n/a');
            }

            //get the batch data
            $mo_batch = getBatchData($mo_mtsms->meta['batch_id']);

            //write out batch data if exists, or just write out the defaults
            if(isset($mo_batch))
            {
               array_push($line, $mo_batch['campaign_code']);
               array_push($line, $mo_batch['batch_reference']);
               array_push($line, $mo_batch['batch_id']);

            }
            else
            {
               array_push($line, 'n/a');
               array_push($line, 'n/a');
               array_push($line, 'n/a');
            }

            fputcsv($output, $line, ',');
         }

         //here we check if the records returned are smaller than our size limit, it means we have reached the end of our available data and we can stop the loop
         if(count($mo_list) < $limit)
         {
            $mos_remain = false;
         }
         else
         {
            $from += $limit;
         }
      }
   }

   //close output stream
   fclose($output);

}
else {
   echo "Invalid variables.";
}


function getMOResultsSearch($search_type, $search_string, $service_id, $from, $limit, $start_date, $end_date)
{
   switch($search_type)
   {
      case 'mosms_content' :
         $mo_results = MOSMS::searchMOSMSForArray('content', $search_string, $service_id, $from, $limit, $start_date, $end_date);
         break;
      case 'mosms_source' :
         $mo_results = MOSMS::searchMOSMSForArray('src', $search_string, $service_id, $from, $limit, $start_date, $end_date);
         break;
   }
   return $mo_results;
}

function getMOResults($service_id, $from, $limit, $start_date, $end_date)
{
   $mo_results = MOSMS::getAllByServiceId($service_id, $from, $limit, $start_date, $end_date);
   return $mo_results;
}

