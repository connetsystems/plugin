<?php
    /**
     * This script is called by the collections page, and runs certain ajax based tasks
     * @author - Doug Jenkinson
     */
    require_once("allInclusive.php");

    try
    {
        session_start();
        $contact_helper = new helperContactLists();

        //check if the user is logged in, if not, redirect back to login
        if(!isset($_SESSION['userId']))
        {
            die();
        }

        if(isset($_POST['task']))
        {
            $task = $_POST['task'];
            $result['success'] = false;
            switch($task)
            {
                case "change_collection_route" :
                    //get the relevant values
                    $collection_id = $_POST['selectedCollectionId'];
                    $collection_mccmnc = $_POST['selectedMCCMNC'];
                    $route_collection_route_id = $_POST['selectedRouteId'];

                    //no ID set for the list so we are inserting
                    $result['success'] = switchRouteOnCollection($collection_id, $collection_mccmnc, $route_collection_route_id);

                    break;
            }
        }
        else
        {
            $result = array('success' => false);
        }
    }
    catch(Exception $e)
    {
        $result = array('success' => false, 'message' => $e->getMessage());
    }

    //json encode the array before returning so it can be parsed by the JS
    echo json_encode($result);

