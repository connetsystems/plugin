<?php

define('SSH_HOST', 'server2');
define('SSH_PORT', 22);
define('SSH_USER', 'php');
define('SSH_PASS', '$2y$10$CsbC1tOgp.j85Bz0aEjYke4ev5sTBqmJVP/7S7GdJbd5MA2WORePe');
//ini_set('memory_limit', '8192M');
set_time_limit(3000);

function ssh_upload_file($local_filename, $remote_filename) 
{

     if (!function_exists('ssh2_connect')) {
     return false;
     }

     if (false === ($local_filename = realpath($local_filename))) {
     return false;
     }

     if (false === ($connection = ssh2_connect(SSH_HOST, SSH_PORT))) {
     return false;
     }

     if (!ssh2_auth_password($connection, SSH_USER, SSH_PASS)) {

     unset($connection);
     return false;
     }

     if (!ssh2_scp_send($connection, $local_filename, $remote_filename, 0644)) {

     unset($connection);
     return false;
     }

     unset($connection);
     return true;
}

?>
