<?php
    require_once('config/dbConn.php');
    /*if(isset($_SESSION['resellerId']) && $_SESSION['resellerId'] != '')
    {
        if($_SESSION['serviceId'] == $_SESSION['resellerId'])
        {
            //this is a reseller;
            $serviceVar = ' AND ('.getAllServicesInReseller($_SESSION['serviceId']).')';
            //echo "<br><br><br>==========================================================".$serviceVar;

        }
        else
        {
            //this is a client
            $serviceVar = ' AND service_id:'.$_SESSION['serviceId'];
            //echo "<br><br><br>==========================================================".$serviceVar;
        }
    }
    else
    {
        $serviceVar = '';
        //this is a connet user
    }*/

    if(isset($_GET['date']) && $_GET['date'] != '')
    {
        $dateArr = explode(' - ', $_GET['date']);
        $startDate = $dateArr[0];
        $startDateConv = $dateArr[0];
        $endDate = $dateArr[1];
        $endDateConv = $dateArr[1];
        $date = $_GET['date'];
    }
    else
    {
        $startDate = (strtotime('today midnight'))*1000;
        $endDate = (time()*1000);
        $startDateConv = date('Y-m-d', ($startDate/1000));
        $endDateConv = date('Y-m-d', ($endDate/1000));
        $date = $startDateConv.' - '.$endDateConv;
    }

    if(isset($_GET['dest']) && $_GET['dest'] != '')
    {
        $dest2 = $_GET['dest'];
        $dests = explode(',', $dest2);
        $dest = ' AND (';
        for($i = 0; $i < count($dests); $i++) 
        { 
            $dest .= 'src:'.$dests[$i].' OR ';
        }
        $dest = substr($dest, 0, -4);
        $dest .= ')';
        // (dest:27837788774* OR dest:27833928610*)
    }
    else
    {
        $dest = "";
        $dest2 = "";
    }

    if(isset($_GET['cont']) && $_GET['cont'] != '')
    {
        $cont = ' AND content:'.$_GET['cont'];
        $cont2 = $_GET['cont'];
    }
    else
    {
        $cont = "";
        $cont2 = "";
    }

    if(isset($_GET['serv']) && $_GET['serv'] != '')
    {
        //$serv = ' AND service_id:'.$_GET['serv'];
        $serv2 = $_GET['serv'];
    }
    else
    {
        //$serv = "";
        $serv2 = "";
    }

    if(isset($_GET['smsc']) && $_GET['smsc'] != '')
    {
        $smsc = ' AND smsc:'.$_GET['smsc'];
        $smsc2 = $_GET['smsc'];
    }
    else
    {
        $smsc = "";
        $smsc2 = "";
    }

    $query = $dest.$cont.$serv2.$smsc;
    if(substr($query, 0, 5) == ' AND ')
    {
        $query = substr($query, 5);
    }

    if($query == '')
    {
        $query = '*';
    }

    $ordering = 'ASC';
    $units = 0;

    if(isset($_GET['sort']))
    {
        $sort = urldecode($_GET['sort']);
        $ordering = strstr($sort, ':');
        $ordering = trim($ordering, ':');
        $ordering = trim($ordering, '"');
        $sortUrl = urlencode($sort);
    }
    else
    {
        $sort = '"timestamp":"desc"';
        $ordering = strstr($sort, ':');
        $ordering = trim($ordering, ':');
        $ordering = trim($ordering, '"');
        $sortUrl = urlencode($sort);
    }

        
        
        $fileNa = 'Search_Replies_Report';
        
        $reportName = 'All';
        
        $starting = 0;
        $total = $_GET['total'];

        if(isset($_GET['admin']) && $_GET['admin'] == 1)
        {
            $admin = 1;
        }
        else
        {
            $admin = 0;
        }
    

    
    //$nl = "\n";
    $dateR = date("Y-m-d_H:i:s");
    $fileN = $fileNa."_".$dateR;
    $file = "../tempFiles/".$fileN.".csv";
    //$csvFile = fopen($file, "a+") or die("Unable to open file!");

    set_time_limit(180);

    //Download headers
    //header('Content-type: text/plain');
    header('Content-type: application/vnd.ms-excel');
    header('Content-Disposition: attachment; filename="' . basename($file)) . '"';
    header('Expires: 0');
    header('Cache-Control: must-revalidate');
    header('Pragma: public');
    //Enable implicit flush & gz compression
    ob_end_clean();
    ob_implicit_flush();
    ob_start("ob_gzhandler");
    //Open out stream
    $php_out = fopen('php://output', 'w');
    fputcsv($php_out, array('sep=,'), "\t");
    
    //Write header
    if ($admin == 1) 
    {
        $columns = array('SMS_id', 'Date', 'MCCMNC', 'SMSC', 'Service', 'Source', 'Destination', 'Content', 'MT_id');
    }
    else
    {
        //$columns = array('SMS_id', 'Date', 'MCCMNC', 'Service', 'From', 'To', 'Content', 'Status');
    }
    fputcsv($php_out, $columns);
    //Flush data
    ob_flush();
    //Output results
    define('ELASTIC_CHUNK_SIZE', 1000);
    for ($starting; $starting < $total; $starting += ELASTIC_CHUNK_SIZE) 
    {
        //fputcsv($php_out, 'Start');
        getElastData($starting, $query, $startDateConv, $endDateConv, $admin, $php_out, ELASTIC_CHUNK_SIZE);
    }
    //Close  output stream, flush, exit
    fclose($php_out);
    ob_flush();
    flush();
    die();

    function getElastData($starting, $query, $startDateConv, $endDateConv, $admin, $handle, $chunk_size)
    {

        $qry = <<<JSONGOESHERE
{
                  "size":$chunk_size,
                  "from":$starting,
                  "sort": {"mosms_id":"desc"},
                  "fields" : ["service_id","meta.mtsms_id","mosms_id","timestamp","src","dest","content","meta.smsc","meta.mccmnc"],
                  "query": {
                    "filtered": {
                      "query": {
                        "bool" : {
                            "must" : [
                                {
                                 "query_string": {
                                 "query": "$query",
                                 "analyze_wildcard": true
                                    }
                                },
                                {
                                  "range": {
                                    "timestamp": {
                                      "gte": "$startDateConv",
                                      "lte": "$endDateConv"
                                    }
                                  }
                                }
                            ]
                        }
                      }
                    }
                  }  
                }

JSONGOESHERE;

        $allStats = runRawMOSMSElasticsearchQuery($qry);

        writeToFile($allStats, $admin, $handle);
    }

    //echo $qry.$nl;
    function writeToFile($allStats, $admin, $handle)
    {
        foreach ($allStats as $key => $value) 
        {
            if($key == 'hits')
            {
                $hits = count($value['hits']);
                for($i=0; $i < $hits; $i++) 
                {
                    //$netw = ($value['hits'][$i]['fields']['meta.mccmnc']['0']);
                    //$smsc = $value['hits'][$i]['fields']['smsc']['0'];
                    /*$posSmsc = strrpos($smsc, '_');
                    $smsc = ucfirst(substr($smsc, 0, $posSmsc));*/
                    $smsc = $value['hits'][$i]['fields']['meta.smsc']['0'];
                    $time = str_replace('T', ' ', $value['hits'][$i]['fields']['timestamp']['0']);
                    $time = strstr($time, '+', true);
                    $timeSec = strrpos($time, ':');
                    $time = substr($time, 0, $timeSec);
                    if(isset($value['hits'][$i]['fields']['meta.mtsms_id']['0']))
                    {
                        $mtsms_id = $value['hits'][$i]['fields']['meta.mtsms_id']['0'];
                    }
                    else
                    {
                        $mtsms_id = '';
                    }
                    /*$dlrPos = $value['hits'][$i]['fields']['dlr']['0'];
                    if(($dlrPos & 1) == 1)
                    {
                        $dlrPos = 'Delivered';
                    }
                    elseif(($dlrPos & 32) == 32 && ($dlrPos & 1) == 0 && ($dlrPos & 2) == 0)
                    {
                        $dlrPos = 'Pending';
                    }
                    elseif(($dlrPos & 2) == 2)
                    {
                        $dlrPos = 'Failed';
                    }
                    
                    $rdnc = $value['hits'][$i]['fields']['rdnc']['0'];
                    */
                    if($admin == 1)
                    {
                        $csvData = array(
                            $value['hits'][$i]['fields']['mosms_id']['0'],
                            $time,
                            $value['hits'][$i]['fields']['meta.mccmnc']['0'],
                            $smsc,
                            $value['hits'][$i]['fields']['service_id']['0'],
                            $value['hits'][$i]['fields']['src']['0'],
                            $value['hits'][$i]['fields']['dest']['0'],
                            preg_replace('/[^(\x20-\x7F)]*/','', $value['hits'][$i]['fields']['content']['0']),
                            $mtsms_id
                        );
                    }
                    else
                    {
                        $csvData = array(
                            $value['hits'][$i]['fields']['mosms_id']['0'],
                            $time,
                            $value['hits'][$i]['fields']['meta.mccmnc']['0'],
                            $value['hits'][$i]['fields']['service_id']['0'],
                            $value['hits'][$i]['fields']['src']['0'],
                            $value['hits'][$i]['fields']['dest']['0'],
                            preg_replace('/[^(\x20-\x7F)]*/','', $value['hits'][$i]['fields']['content']['0']),
                            $mtsms_id
                            
                        );
                    }
                    fputcsv($handle, $csvData);
                }
            }
        }
        ob_flush();
    }

    /*for($starting; $starting < $total; $starting += 5000)
    {
        getElastData($starting, $cl, $qryStr, $searchTerm, $startDate, $endDate, $admin, $csvFile, $nl, $file);
        echo '<br>'.$starting;
        flush();
    }

    header('Content-Type: application/csv');
    header('Content-Disposition: attachment; filename='.basename($file).'.csv');
    header("Cache-Control: no-cache, no-store, must-revalidate"); // HTTP 1.1
    header("Pragma: no-cache");
    header("Expires: 30000");
    readfile("../tempFiles/".$fileN.".csv");*/

?>
