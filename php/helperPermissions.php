<?php

/**
 * Class helperPermissions
 * @author - Doug Jenkinson
 *
 * This class handles all permissions related functions
 */

//collections.php - Mater DB
function checkAccountHasAccessToService($account_id, $service_id)
{
    //open the DB object
    $conn = getPDOMasterCore();

    //prepare the data
    $data = array(':service_id' => $service_id, ':account_id' => $account_id);

    //prepare the query
    $stmt = $conn->prepare('SELECT count(*) FROM core.core_service
                    WHERE account_id = :account_id AND service_id = :service_id');

    //execute it
    $stmt->execute($data);

    //fetch the data
    $rows = $stmt->fetch(PDO::FETCH_NUM);

    if($rows[0] > 0)
    {
        return true;
    }
    else
    {
        return false;
    }
}