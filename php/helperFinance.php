<?php

//salesstats.php - Mater DB
function getSalesPersonStats($startDate, $endDate) {
    //open the DB object
    $conn = getPDOReportingCore();

    //prepare the data
    $data = array(':start_date' => $startDate, ':end_date' => $endDate);

    $stmt = $conn->prepare("SELECT a.account_name,s.service_name,s.service_id,units,service_status,sc.service_credit_billing_type,sc.service_credit_available
            FROM (SELECT sum(cdr_record_units) as 'units',
                SPLIT_STR(cdr_record_reference,'|',2) as `service_id`
                FROM reporting.cdr_record_archive cr
                INNER JOIN reporting.cdr c
                on cr.cdr_id = c.cdr_id
                where date(cdr_record_date) >= :start_date and date(cdr_record_date) < :end_date
                and SPLIT_STR(cdr_record_reference,'|',1) not in ('legacycore')
                group by SPLIT_STR(cdr_record_reference,'|',2)
            ) AS C
            RIGHT OUTER JOIN core.core_service s
            ON C.service_id = s.service_id
            INNER JOIN core.core_account a
            ON s.account_id = a.account_id
            INNER JOIN core.core_user u
            on s.service_manager_user_id = u.user_id
            INNER JOIN core.core_service_credit sc
            ON s.service_id = sc.service_id
            WHERE u.user_id = 563
            order by 1;");


    //set the fetch mode
    $stmt->setFetchMode(PDO::FETCH_ASSOC);

    //execute it
    $stmt->execute($data);

    //fetch the data
    $colData = $stmt->fetchAll();

    return $colData;
}

//DOUG: converted to PDO - clienthomeOld.php | home.php | homeOld.php | resellerhomeOld.php
function getMonthTotalsSalesPerson($start_date, $end_date) {
    //open the DB object
    $conn = getPDOReportingCore();

    //prepare the data
    $data = array(':start_date' => $start_date, ':end_date' => $end_date);

    //prepare the query
    $stmt = $conn->prepare("SELECT SUM(units) as 'cdr_record_units',Year,month,monthnum
                            FROM (SELECT sum(cdr_record_units) as 'units',
                                SPLIT_STR(cdr_record_reference,'|',2) as `service_id`,
                                MONTHNAME(cdr_record_date) as 'month',
                                YEAR(cdr_record_date) as 'year',
                                month(cdr_record_date) as 'monthnum'
                                FROM reporting.cdr_record_archive cr
                                INNER JOIN reporting.cdr c
                                on cr.cdr_id = c.cdr_id
                                where date(cdr_record_date) >= :start_date and date(cdr_record_date) < :end_date
                                and SPLIT_STR(cdr_record_reference,'|',1) not in ('legacycore')
                                group by SPLIT_STR(cdr_record_reference,'|',2),YEAR(cdr_record_date), MONTHNAME(cdr_record_date)
                            ) AS C
                            INNER JOIN core.core_service s
                            ON C.service_id = s.service_id
                            WHERE s.service_manager_user_id = 92
                            GROUP BY year,month
                            order by Year,monthnum");

    //set the fetch mode
    $stmt->setFetchMode(PDO::FETCH_ASSOC);

    //execute it
    $stmt->execute($data);

    //fetch the data
    $networkReport = $stmt->fetchAll();

    return $networkReport;
}

function getSalesPersonProfit() {
    //open the DB object
    $conn = getPDOReportingCore();

    $qry = "SELECT account_name,service_name,SUM(units) as 'units',SUM(client_zar) as 'client_zar',SUM(profit_zar) as 'profit_zar'
            FROM(
            SELECT account_name,service_name,service_id,prefix_country_name,prefix_network_name,mccmnc,smsc,units,
            if(client_cost_price='0.0000',format(route_billing/10000,4),client_cost_price) as 'service_rate',service_cur,
            connect_cost_price as 'connet rate',connet_cur,
            (units * if(client_cost_price='0.0000',format(route_billing/10000,4),client_cost_price)) as 'client',
            (units * connect_cost_price) as 'connet',
            (units * if(client_cost_price='0.0000',format(route_billing/10000,4),client_cost_price)) - (units * connect_cost_price) as 'profit',
            if(service_cur='ZAR',(units * if(client_cost_price='0.0000',format(route_billing/10000,4),client_cost_price)),(units * if(client_cost_price='0.0000',format(route_billing/10000,4),client_cost_price))*15.0) as 'client_zar',
            if(connet_cur='ZAR',(units * connect_cost_price),(units * connect_cost_price)*15.0) as 'connet_zar',
            (if(service_cur='ZAR',(units * if(client_cost_price='0.0000',format(route_billing/10000,4),client_cost_price)),(units * if(client_cost_price='0.0000',format(route_billing/10000,4),client_cost_price))*15.0)-if(connet_cur='ZAR',(units * connect_cost_price),(units * connect_cost_price)*15.0)) as 'profit_zar',
            user_fullname,`type`
            FROM (
            SELECT a.account_name,s.service_name,s.service_id,
            prefix_country_name,prefix_network_name,mccmnc,smsc,client_cost_price,
            #Exclusion list = 0 or price in core_route table
            if(`type`='rdnc',0,connect_cost_price) as 'connect_cost_price',
            C.units,connet_cur,user_fullname,`type`,
                (select r.route_currency
                from core.core_route r
                where route_msg_type='MTSMS'
                and route_status='ENABLED'
                and route_collection_id is not null
                and service_id=C.service_id
                and route_mccmnc=C.mccmnc
                limit 1) as 'service_cur',
                (select SPLIT_STR(r.route_billing,',',1)
                from core.core_route r
                where route_msg_type='MTSMS'
                and route_status='ENABLED'
                and route_collection_id is not null
                and service_id=C.service_id
                and route_mccmnc=C.mccmnc
                limit 1) as 'route_billing'
            FROM (SELECT sum(cdr_record_units) as 'units',
                SPLIT_STR(cdr_record_reference,'|',2) as `service_id`,
                (cdr_record_cost_per_unit/10000) as 'client_cost_price',
                (cdr_cost_per_unit/10000) as 'connect_cost_price',
                SPLIT_STR(cdr_provider,'|',4) as 'mccmnc',
                SPLIT_STR(cdr_provider,'|',3) as 'smsc',
                if(c.cdr_comments like '%EUR%','EUR','ZAR') as connet_cur ,
                SPLIT_STR(cdr_record_reference,'|',1) as 'type'
                FROM reporting.cdr_record_archive cr
                INNER JOIN reporting.cdr c
                on cr.cdr_id = c.cdr_id
                where date(cdr_record_date) >= '2015-12-01' and date(cdr_record_date) < '2016-01-01'
                #and SPLIT_STR(cdr_record_reference,'|',2) in  (905)
                and SPLIT_STR(cdr_record_reference,'|',1) not in ('legacycore')
                group by SPLIT_STR(cdr_record_reference,'|',2),(cdr_cost_per_unit/10000),(cdr_cost_per_unit/10000),
                SPLIT_STR(cdr_provider,'|',4),SPLIT_STR(cdr_provider,'|',3),
                SPLIT_STR(cdr_record_reference,'|',1)
            ) AS C
            INNER join
                ( SELECT prefix_mccmnc,prefix_network_name,prefix_country_name FROM core.core_prefix GROUP BY prefix_mccmnc)
                AS core_prefix
                ON mccmnc=prefix_mccmnc
            INNER JOIN core.core_service s
            ON C.service_id = s.service_id
            INNER JOIN core.core_account a
            ON s.account_id = a.account_id
            INNER JOIN core.core_user u
            on s.service_manager_user_id = u.user_id
            WHERE s.service_manager_user_id = 92
            ) AS TDATA
            ) AS Profit
            GROUP BY account_name,service_name;";

    //prepare the query
    $stmt = $conn->prepare($qry);

    //set the fetch mode
    $stmt->setFetchMode(PDO::FETCH_ASSOC);

    //execute it
    $stmt->execute();

    //run the query
    $colData = $stmt->fetchAll();

    return $colData;
}

//financialstats.php
function getFinancialProfit($start_date, $end_date,$user_id) {

    //open the DB object
    $conn = getWaspdb1();


    //prepare the data
    $data = array(':start_date' => $start_date, ':end_date' => $end_date);

    if ($user_id != 'Show All') {
        $data[':user_id'] = $user_id;
    }


    $qry = "select account_name,service_name,p.service_id,prefix_country_name,prefix_network_name,mccmnc,smsc,units,user_fullname,`type`,day,eur_zar,
            if(client_cost_price='0.0000',format(route_billing/10000,4),format(client_cost_price,4)) as 'service_rate',service_cur,
            connect_cost_price as 'connet rate',connet_cur,
            (units * if(client_cost_price='0.0000',format(route_billing/10000,4),format(client_cost_price,4))) as 'client',
            (units * connect_cost_price) as 'connet',
            (units * if(client_cost_price='0.0000',format(route_billing/10000,4),format(client_cost_price,4))) - (units * connect_cost_price) as 'profit',
            if(service_cur='ZAR',(units * if(client_cost_price='0.0000',format(route_billing/10000,4),format(client_cost_price,4))),(units * if(client_cost_price='0.0000',format(route_billing/10000,4),format(client_cost_price,4)))*eur_zar) as 'client_zar',
            if(connet_cur='ZAR',(units * connect_cost_price),(units * connect_cost_price)*eur_zar) as 'connet_zar',
            (if(service_cur='ZAR',(units * if(client_cost_price='0.0000',format(route_billing/10000,4),format(client_cost_price,4))),(units * if(client_cost_price='0.0000',format(route_billing/10000,4),format(client_cost_price,4)))*eur_zar)-if(connet_cur='ZAR',(units * connect_cost_price),(units * connect_cost_price)*eur_zar)) as 'profit_zar',
            sc.service_credit_billing_type,user_id
            from profit_report p
            inner join core.core_service_credit sc
            on p.service_id = sc.service_id
            where date(`day`) >= :start_date and date(`day`) < :end_date";
            if ($user_id != "Show All") {
                $qry .= " and user_id = :user_id order by service_name,mccmnc ";
            }
            else
            {
                $qry .= " order by service_name,mccmnc";
            }

    //prepare the query
    $stmt = $conn->prepare($qry);

    //set the fetch mode
    $stmt->setFetchMode(PDO::FETCH_ASSOC);

    //execute it
    $stmt->execute($data);

    //fetch the data
    $colData = $stmt->fetchAll();

    return $colData;
}

function organizeSalesPerson($finData,$user_id)
{
    $reformat_sales = array();
    foreach ($finData as $key => $value) {
        //sales

            if ($user_id == 'Show All' || $value['user_id'] == $user_id){
                if (!array_key_exists($value['user_fullname'], $reformat_sales)) {
                    $salesman = array();
                    $salesman['units'] = $value['units'];
                    $salesman['client'] = $value['client_zar'];
                    $salesman['connet'] = $value['connet_zar'];
                    $salesman['profit'] = $value['profit_zar'];
                    $salesman['service_ids'] = array($value['service_id'] => 1);

                    $reformat_sales[$value['user_fullname']] = $salesman;
                } else {
                    $salesman = $reformat_sales[$value['user_fullname']];
                    $salesman['units'] += $value['units'];
                    $salesman['client'] += $value['client_zar'];
                    $salesman['connet'] += $value['connet_zar'];
                    $salesman['profit'] += $value['profit_zar'];

                    if(!array_key_exists($value['service_id'], $salesman['service_ids'])) {
                        $salesman['service_ids'][$value['service_id']] = 1;
                    }

                    $reformat_sales[$value['user_fullname']] = $salesman;
                }
            }

    }
    //sort array
    foreach ($reformat_sales as $key => $row)
    {
        $sortarray[$key] = $row['profit'];
    }
    array_multisort($sortarray, SORT_DESC, $reformat_sales);
    return $reformat_sales;
}

function organizeExclusion($finData)
{
    $reformat_type = array();
    foreach ($finData as $key => $value) {
        //Exclusion
        if (!array_key_exists($value['type'], $reformat_type)) {
            $type = array();
            $type['units'] = $value['units'];
            $type['client'] = $value['client_zar'];
            $type['connet'] = $value['connet_zar'];
            $type['profit'] = $value['profit_zar'];

            $reformat_type[$value['type']] = $type;
        } else {
            $type = $reformat_type[$value['type']];
            $type['units'] += $value['units'];
            $type['client'] += $value['client_zar'];
            $type['connet'] += $value['connet_zar'];
            $type['profit'] += $value['profit_zar'];

            $reformat_type[$value['type']] = $type;
        }
    }
    //Sort Array
    foreach ($reformat_type as $key => $row)
    {
        $tarray[$key] = $row['profit'];
    }
    array_multisort($tarray, SORT_DESC, $reformat_type);
    return $reformat_type;
}

function organizeAccount($finData,$user_id)
{
    $reformat_account = array();
    foreach ($finData as $key=>$value) {
        //Account

        if ($user_id == 'Show All' || $value['user_id'] == $user_id) {
            if (!array_key_exists($value['account_name'], $reformat_account)) {
                $acc = array();
                $acc['units'] = $value['units'];
                $acc['client'] = $value['client_zar'];
                $acc['connet'] = $value['connet_zar'];
                $acc['profit'] = $value['profit_zar'];

                $acc['service_ids'] = array($value['service_id'] => 1);

                if($value['profit_zar'] < -0)
                {
                    $acc['is_profit_error'] = true;
                }
                else
                {
                    $acc['is_profit_error'] = false;
                }

                $reformat_account[$value['account_name']] = $acc;
            } else {
                $acc = $reformat_account[$value['account_name']];
                $acc['units'] += $value['units'];
                $acc['client'] += $value['client_zar'];
                $acc['connet'] += $value['connet_zar'];
                $acc['profit'] += $value['profit_zar'];

                if(!array_key_exists($value['service_id'], $acc['service_ids'])) {
                    $acc['service_ids'][$value['service_id']] = 1;
                }

                if($value['profit_zar'] < -0)
                {
                    $acc['is_profit_error'] = true;
                }

                $reformat_account[$value['account_name']] = $acc;
            }
        }

    }
    //Sort Account
    foreach ($reformat_account as $key => $row)
    {
        $accarray[$key] = $row['units'];
    }
    array_multisort($accarray, SORT_DESC, $reformat_account);
    return $reformat_account;
}

function organizeNetworkCharts($finData)
{
    $reformat_networkcharts = array();
    foreach ($finData as $key=>$value) {
        //Network
        if(!array_key_exists($value['prefix_network_name'], $reformat_networkcharts))
        {
            $network = array();
            $network['units'] = $value['units'];
            $network['client'] = $value['client_zar'];
            $network['connet'] = $value['connet_zar'];
            $network['profit'] = $value['profit_zar'];
            $network['mccmnc'] = $value['mccmnc'];

            $reformat_networkcharts[$value['prefix_network_name']] = $network;
        }
        else
        {
            $network = $reformat_networkcharts[$value['prefix_network_name']];
            $network['units'] += $value['units'];
            $network['client'] += $value['client_zar'];
            $network['connet'] += $value['connet_zar'];
            $network['profit'] += $value['profit_zar'];
            $network['mccmnc'] = $value['mccmnc'];

            $reformat_networkcharts[$value['prefix_network_name']] = $network;
        }
    }
//Sort Network by profit total and unit total
    foreach ($reformat_networkcharts as $key => $row)
    {
        //$narray[$key] = $row['profit'];
        $narray[$key] = $row['units'];
    }
    array_multisort($narray, SORT_DESC, $reformat_networkcharts);
    return $reformat_networkcharts;

}

function organizeNetwork($start_date, $end_date) {

    //open the DB object
    $conn = getWaspdb1();

    //prepare the data
    $data = array(':start_date' => $start_date, ':end_date' => $end_date);

    $qry = "Select prefix_network_name,mccmnc,units,connet_zar,connet_eur,profit_zar,units_rdnc,profit_zar_rdnc
            from (
                select prefix_network_name,mccmnc,sum(units) as 'units',
                sum(if(connet_cur='ZAR',(units * connect_cost_price),(units * connect_cost_price)*eur_zar)) as 'connet_zar',
                sum(if(connet_cur='EUR',(units * connect_cost_price),(units * connect_cost_price))) as 'connet_eur',
                sum((if(service_cur='ZAR',(units * if(client_cost_price='0.0000',format(route_billing/10000,4),format(client_cost_price,4))),(units * if(client_cost_price='0.0000',format(route_billing/10000,4),format(client_cost_price,4)))*eur_zar)-if(connet_cur='ZAR',(units * connect_cost_price),(units * connect_cost_price)*eur_zar))) as 'profit_zar'
                from profit_report
                where date(`day`) >= :start_date and date(`day`) < :end_date
                and type='core'
                group by mccmnc
            ) as D
            LEFT OUTER JOIN (
                select mccmnc as 'mccmnc_rdnc',sum(units) as 'units_rdnc',
                sum((if(service_cur='ZAR',(units * if(client_cost_price='0.0000',format(route_billing/10000,4),format(client_cost_price,4))),(units * if(client_cost_price='0.0000',format(route_billing/10000,4),format(client_cost_price,4)))*eur_zar)-if(connet_cur='ZAR',(units * connect_cost_price),(units * connect_cost_price)*eur_zar))) as 'profit_zar_rdnc'
                from profit_report
                where date(`day`) >= :start_date and date(`day`) < :end_date
                and type='rdnc'
                group by mccmnc
            ) as DD on D.mccmnc = DD.mccmnc_rdnc";

    //prepare the query
    $stmt = $conn->prepare($qry);

    //set the fetch mode
    $stmt->setFetchMode(PDO::FETCH_ASSOC);

    //execute it
    $stmt->execute($data);

    //fetch the data
    $colData = $stmt->fetchAll();

    return $colData;

}

function organizeProvider($start_date, $end_date,$user_id) {

    //open the DB object
    $conn = getWaspdb1();

    //prepare the data
    $data = array(':start_date' => $start_date, ':end_date' => $end_date);

    $qry = "Select smsc,units,connet_zar,connet_cur,connet_eur,profit_zar,smsc_rdnc,units_rdnc,profit_zar_rdnc
            from (
                select smsc,connet_cur,sum(units) as 'units',
                sum(if(connet_cur='ZAR',(units * connect_cost_price),(units * connect_cost_price)*eur_zar)) as 'connet_zar',
                sum(if(connet_cur='EUR',(units * connect_cost_price),(units * connect_cost_price))) as 'connet_eur',
                sum((if(service_cur='ZAR',(units * if(client_cost_price='0.0000',format(route_billing/10000,4),format(client_cost_price,4))),(units * if(client_cost_price='0.0000',format(route_billing/10000,4),format(client_cost_price,4)))*eur_zar)-if(connet_cur='ZAR',(units * connect_cost_price),(units * connect_cost_price)*eur_zar))) as 'profit_zar'
                from profit_report
                where date(`day`) >= :start_date and date(`day`) < :end_date
                and type='core'
                group by smsc,connet_cur
            ) as D
            LEFT OUTER JOIN (
                select smsc as 'smsc_rdnc',sum(units) as 'units_rdnc',
                sum((if(service_cur='ZAR',(units * if(client_cost_price='0.0000',format(route_billing/10000,4),format(client_cost_price,4))),(units * if(client_cost_price='0.0000',format(route_billing/10000,4),format(client_cost_price,4)))*eur_zar)-if(connet_cur='ZAR',(units * connect_cost_price),(units * connect_cost_price)*eur_zar))) as 'profit_zar_rdnc'
                from profit_report
                where date(`day`) >= :start_date and date(`day`) < :end_date
                and type='rdnc'
                group by smsc
            ) as DD on D.smsc = DD.smsc_rdnc";

    //prepare the query
    $stmt = $conn->prepare($qry);

    //set the fetch mode
    $stmt->setFetchMode(PDO::FETCH_ASSOC);

    //execute it
    $stmt->execute($data);

    //fetch the data
    $colData = $stmt->fetchAll();

    return $colData;
}

//finacialtotals.php - get montly units based on date range
function getMonthly_Units_Totals($start_date, $end_date) {
    //open the DB object
    $conn = getWaspdb1();

    //prepare the data
    $data = array(':start_date' => $start_date, ':end_date' => $end_date);

    //prepare the query
    $stmt = $conn->prepare("SELECT YEAR(cdr_record_date) as 'Year', MONTHNAME(cdr_record_date) as 'month',sum(cdr_record_units) as 'cdr_record_units'
                           FROM reporting.cdr_record_archive cr
                           INNER JOIN reporting.cdr c
                           on cr.cdr_id = c.cdr_id
                           where date(cdr_record_date) >= :start_date and date(cdr_record_date) < :end_date
                           and SPLIT_STR(cdr_record_reference,'|',1) not in ('legacycore')
                           group by YEAR(cdr_record_date),MONTHNAME(cdr_record_date)
                           ORDER BY cdr_record_date");

    //set the fetch mode
    $stmt->setFetchMode(PDO::FETCH_ASSOC);

    //execute it
    $stmt->execute($data);

    //fetch the data
    $montly_data_totals = $stmt->fetchAll();

    return $montly_data_totals;
}
//finacialtotals.php - get daily units based on date range
function getDay_Units_Totals($start_date, $end_date)
{
//open the DB object
    $conn = getWaspdb1();

    //prepare the data
    $data = array(':start_date' => $start_date, ':end_date' => $end_date);

    //prepare the query
    $stmt = $conn->prepare("SELECT *
                            FROM(
                                SELECT IfNull(concat(account_name,' (',account_id,')'),'Totals') account_name,IfNull(concat(service_name,' (',service_id,')'),'Totals') service_name,
                                service_credit_billing_type,service_status,
                                sum(CASE WHEN day= 1 THEN units ELSE 0 END) AS '1',sum(CASE WHEN day= 2 THEN units ELSE 0 END) AS '2',sum(CASE WHEN day= 3 THEN units ELSE 0 END) AS '3',
                                sum(CASE WHEN day= 4 THEN units ELSE 0 END) AS '4',sum(CASE WHEN day= 5 THEN units ELSE 0 END) AS '5',sum(CASE WHEN day= 6 THEN units ELSE 0 END) AS '6',
                                sum(CASE WHEN day= 7 THEN units ELSE 0 END) AS '7',sum(CASE WHEN day= 8 THEN units ELSE 0 END) AS '8',sum(CASE WHEN day= 9 THEN units ELSE 0 END) AS '9',
                                sum(CASE WHEN day= 10 THEN units ELSE 0 END) AS '10',sum(CASE WHEN day= 11 THEN units ELSE 0 END) AS '11',sum(CASE WHEN day= 12 THEN units ELSE 0 END) AS '12',
                                sum(CASE WHEN day= 13 THEN units ELSE 0 END) AS '13',sum(CASE WHEN day= 14 THEN units ELSE 0 END) AS '14',sum(CASE WHEN day= 15 THEN units ELSE 0 END) AS '15',
                                sum(CASE WHEN day= 16 THEN units ELSE 0 END) AS '16',sum(CASE WHEN day= 17 THEN units ELSE 0 END) AS '17',sum(CASE WHEN day= 18 THEN units ELSE 0 END) AS '18',
                                sum(CASE WHEN day= 19 THEN units ELSE 0 END) AS '19',sum(CASE WHEN day= 20 THEN units ELSE 0 END) AS '20',sum(CASE WHEN day= 21 THEN units ELSE 0 END) AS '21',
                                sum(CASE WHEN day= 22 THEN units ELSE 0 END) AS '22',sum(CASE WHEN day= 23 THEN units ELSE 0 END) AS '23',sum(CASE WHEN day= 24 THEN units ELSE 0 END) AS '24',
                                sum(CASE WHEN day= 25 THEN units ELSE 0 END) AS '25',sum(CASE WHEN day= 26 THEN units ELSE 0 END) AS '26',sum(CASE WHEN day= 27 THEN units ELSE 0 END) AS '27',
                                sum(CASE WHEN day= 28 THEN units ELSE 0 END) AS '28',sum(CASE WHEN day= 29 THEN units ELSE 0 END) AS '29',sum(CASE WHEN day= 30 THEN units ELSE 0 END) AS '30',
                                sum(CASE WHEN day= 31 THEN units ELSE 0 END) AS '31'
                                FROM(
                                    SELECT a.account_name,a.account_id,s.service_name,s.service_id,sc.service_credit_billing_type,s.service_status,if(C.units is null,0,C.units) as 'units',day
                                    FROM (
                                        SELECT sum(cdr_record_units) as 'units',
                                        SPLIT_STR(cdr_record_reference,'|',2) as `service_id`,
                                        day(cdr_record_date) as 'day'
                                        FROM reporting.cdr_record_archive cr
                                        INNER JOIN reporting.cdr c
                                        on cr.cdr_id = c.cdr_id
                                        where date(cdr_record_date) >= :start_date and date(cdr_record_date) < :end_date
                                        and SPLIT_STR(cdr_record_reference,'|',3) not in (1,586)
                                        and SPLIT_STR(cdr_record_reference,'|',1) not in ('legacycore')
                                        group by SPLIT_STR(cdr_record_reference,'|',2),day(cdr_record_date)
                                    ) as C
                                    RIGHT OUTER JOIN core.core_service s
                                    ON C.service_id = s.service_id
                                    RIGHT OUTER JOIN core.core_account a
                                    ON s.account_id = a.account_id
                                    RIGHT OUTER JOIN core.core_service_credit sc
                                    ON s.service_id = sc.service_id
                                    WHERE a.account_id not in (1,586)
                            ) as PivotData
                            GROUP BY service_name WITH ROLLUP
                        ) as DTotals
                        ORDER BY -1 ASC");

    //set the fetch mode
    $stmt->setFetchMode(PDO::FETCH_ASSOC);

    //execute it
    $stmt->execute($data);

    //fetch the data
    $daily_data_totals = $stmt->fetchAll();

    return $daily_data_totals;
}
//financialstats_network.php
function getAccount_Service_Network_Profit($start_date, $end_date,$mccmnc,$account_name) {
    //open the DB object
    $conn = getWaspdb1();

    //prepare the data
    $data = array(':start_date' => $start_date, ':end_date' => $end_date);

    if ($mccmnc != 'Show All') {
        $data[':mccmnc'] = $mccmnc;
    }
    if ($account_name != 'Show All') {
        $data[':account_name'] = $account_name;
    }

    $qry = "select account_name,service_name,prefix_country_name,concat(prefix_network_name,' (',mccmnc,')') as prefix_network_name,client_cost_price,connect_cost_price,sum(units) as 'units',
            sum(if(service_cur='ZAR',(units * if(client_cost_price='0.0000',format(route_billing/10000,4),format(client_cost_price,4))),(units * if(client_cost_price='0.0000',format(route_billing/10000,4),format(client_cost_price,4)))*eur_zar)) as 'client_zar',
            sum(if(connet_cur='ZAR',(units * connect_cost_price),(units * connect_cost_price)*eur_zar)) as 'connet_zar',
            sum((if(service_cur='ZAR',(units * if(client_cost_price='0.0000',format(route_billing/10000,4),format(client_cost_price,4))),(units * if(client_cost_price='0.0000',format(route_billing/10000,4),format(client_cost_price,4)))*eur_zar)-if(connet_cur='ZAR',(units * connect_cost_price),(units * connect_cost_price)*eur_zar))) as 'profit_zar'
            from profit_report
            where date(`day`) >= :start_date and date(`day`) < :end_date";
    if (($mccmnc != "Show All") && ($account_name == "Show All")) {
        $qry .= " and mccmnc = :mccmnc ";
    }
    if (($account_name != "Show All") && ($mccmnc == "Show All")) {
        $qry .= " and account_name = :account_name ";
    }

    $qry .= " group by account_name,service_name,prefix_country_name,prefix_network_name order by 1,2;";

    $stmt = $conn->prepare($qry);

    //set the fetch mode
    $stmt->setFetchMode(PDO::FETCH_ASSOC);

    //execute it
    $stmt->execute($data);

    //fetch the data
    $networkData = $stmt->fetchAll();

    return $networkData;
}

function getSmsc_Popup_Country_Network($start_date, $end_date,$smsc) {
    //open the DB object
    $conn = getWaspdb1();

    //prepare the data
    $data = array(':start_date' => $start_date, ':end_date' => $end_date, ':smsc' => $smsc);

    $stmt = $conn->prepare("Select prefix_country_name,prefix_network_name,mccmnc,connet_cur,connect_cost_price,units,connet_zar,connet_cur,connet_eur,profit_zar,units_rdnc,profit_zar_rdnc
                    from (
                    select prefix_country_name,prefix_network_name,mccmnc,connet_cur,sum(units) as 'units',connect_cost_price,
                    sum(if(connet_cur='ZAR',(units * connect_cost_price),(units * connect_cost_price)*eur_zar)) as 'connet_zar',
                    sum(if(connet_cur='EUR',(units * connect_cost_price),(units * connect_cost_price))) as 'connet_eur',
                    sum((if(service_cur='ZAR',(units * if(client_cost_price='0.0000',format(route_billing/10000,4),format(client_cost_price,4))),(units * if(client_cost_price='0.0000',format(route_billing/10000,4),format(client_cost_price,4)))*eur_zar)-if(connet_cur='ZAR',(units * connect_cost_price),(units * connect_cost_price)*eur_zar))) as 'profit_zar'
                    from profit_report
                    where date(`day`) >= :start_date and date(`day`) < :end_date
                    and type='core'
                    and smsc = :smsc
                    group by prefix_country_name,prefix_network_name,mccmnc,connet_cur,connect_cost_price
                    ) as D
                    LEFT OUTER JOIN (
                    select prefix_country_name as 'prefix_country_name_rdnc',prefix_network_name as 'prefix_network_name_rdnc',mccmnc as 'mccmnc_rdnc',sum(units) as 'units_rdnc',connect_cost_price as 'connect_cost_price_rdnc',
                    sum((if(service_cur='ZAR',(units * if(client_cost_price='0.0000',format(route_billing/10000,4),format(client_cost_price,4))),(units * if(client_cost_price='0.0000',format(route_billing/10000,4),format(client_cost_price,4)))*eur_zar)-if(connet_cur='ZAR',(units * connect_cost_price),(units * connect_cost_price)*eur_zar))) as 'profit_zar_rdnc'
                    from profit_report
                    where date(`day`) >= :start_date and date(`day`) < :end_date
                    and type='rdnc'
                    and smsc = :smsc
                    group by prefix_country_name,prefix_network_name,mccmnc,connet_cur,connect_cost_price
                    ) as DD on D.mccmnc = DD.mccmnc_rdnc");

    //set the fetch mode
    $stmt->setFetchMode(PDO::FETCH_ASSOC);

    //execute it
    $stmt->execute($data);

    //fetch the data
    $smscData = $stmt->fetchAll();

    return $smscData;
}

function getSmsc_Popup_Account_Network($start_date, $end_date,$mccmnc) {
    //open the DB object
    $conn = getWaspdb1();

    //prepare the data
    $data = array(':start_date' => $start_date, ':end_date' => $end_date, ':mccmnc' => $mccmnc);

    $stmt = $conn->prepare("Select account_name,service_name,prefix_country_name,prefix_network_name,mccmnc,smsc,connet_cur,connect_cost_price,client_cost_price,service_cur,units,connet_zar,connet_cur,connet_eur,profit_zar,units_rdnc,profit_zar_rdnc
                                        from (
                                        select account_name,service_name,prefix_country_name,prefix_network_name,mccmnc,smsc,connet_cur,sum(units) as 'units',connect_cost_price,client_cost_price,service_cur,
                                        sum(if(connet_cur='ZAR',(units * connect_cost_price),(units * connect_cost_price)*eur_zar)) as 'connet_zar',
                                        sum(if(connet_cur='EUR',(units * connect_cost_price),(units * connect_cost_price))) as 'connet_eur',
                                        sum((if(service_cur='ZAR',(units * if(client_cost_price='0.0000',format(route_billing/10000,4),format(client_cost_price,4))),(units * if(client_cost_price='0.0000',format(route_billing/10000,4),format(client_cost_price,4)))*eur_zar)-if(connet_cur='ZAR',(units * connect_cost_price),(units * connect_cost_price)*eur_zar))) as 'profit_zar'
                                        from profit_report
                                        where date(`day`) >= :start_date and date(`day`) < :end_date
                                        and type='core'
                                        and mccmnc = :mccmnc
                                        group by account_name,service_name,prefix_country_name,prefix_network_name,mccmnc,connet_cur,connect_cost_price,client_cost_price,service_cur
                                        ) as D
                                        LEFT OUTER JOIN (
                                        select account_name as 'account_name_rdnc',service_name as 'service_name_rdnc',prefix_country_name as 'prefix_country_name_rdnc',prefix_network_name as 'prefix_network_name_rdnc',mccmnc as 'mccmnc_rdnc',smsc as 'smsc_rdnc',sum(units) as 'units_rdnc',connect_cost_price as 'connect_cost_price_rdnc',
                                        sum((if(service_cur='ZAR',(units * if(client_cost_price='0.0000',format(route_billing/10000,4),format(client_cost_price,4))),(units * if(client_cost_price='0.0000',format(route_billing/10000,4),format(client_cost_price,4)))*eur_zar)-if(connet_cur='ZAR',(units * connect_cost_price),(units * connect_cost_price)*eur_zar))) as 'profit_zar_rdnc'
                                        from profit_report
                                        where date(`day`) >= :start_date and date(`day`) < :end_date
                                        and type='rdnc'
                                        and mccmnc = :mccmnc
                                        group by account_name,service_name,prefix_country_name,prefix_network_name,mccmnc,connet_cur,connect_cost_price
                                        ) as DD on D.mccmnc = DD.mccmnc_rdnc
                                        and D.smsc = DD.smsc_rdnc
                                        and D.service_name = DD.service_name_rdnc");

    //set the fetch mode
    $stmt->setFetchMode(PDO::FETCH_ASSOC);

    //execute it
    $stmt->execute($data);

    //fetch the data
    $smscData = $stmt->fetchAll();

    return $smscData;
}