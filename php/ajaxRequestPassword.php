<?php

//error_reporting(E_ALL);
//ini_set('display_errors', '1');
try {
   //always start that session so we have access to it
   session_start();

   $json_response = array();

   //make sure we include our DB helper so we don't have to open our pesky DB connection ourself
   require_once('config/dbConn.php');

   //open the DB object
   $conn = getPDOMasterCore();

   //get our POST var for the email address
   $email = $_POST['email'];

   if (!isset($email)) {
      $json_response['success'] = false;
      $json_response['error'] = "no_email";

      //json encode the array before returning so it can be parsed by the JS
      echo json_encode($json_response);
      return;
   }

   //track errors
   $error = false;
   $error_type = "";

   //get the users account_details
   //prepare the data
   $data = array(':email' => $email);

   //prepare the query
   $stmt = $conn->prepare('SELECT * FROM account_details WHERE emailaddress = :email');

   //set the fetch mode
   $stmt->setFetchMode(PDO::FETCH_ASSOC);

   //execute it
   $stmt->execute($data);

   //fetch the data
   $user_account_details = $stmt->fetch();

   //check if there are any results
   if(isset($user_account_details['account_details_id']))
   {
      $token = generateUniqueEmailToken($conn);
      //insure the token was generated
      if($token != "")
      {
         $expiry = date("Y-m-d h:i:s", strtotime("+30 minutes"));
         //prepare the data
         $data_save_token = array(':token' => $token, ':expiry' => $expiry, ':account_details_id' => $user_account_details['account_details_id']);

         //prepare the query
         $stmt = $conn->prepare("UPDATE account_details
                SET password_reset_token = :token, password_reset_expiry = :expiry
                WHERE account_details_id = :account_details_id ");

         //execute it
         if ($stmt->execute($data_save_token))
         {
            sendResetEmail($user_account_details['emailaddress'], $token);

            $error = false;
         }
         else
         {
            $error = true;
            $error_type = "server_error";
         }
      }
      else
      {
         $error = true;
         $error_type = "server_error";
      }
   }
   else
   {
      $error = true;
      $error_type = "not_registered";
   }

   //now we create a json_formatted response so our Javscript on the form can parse it and find out how we did
   if (isset($error) && $error == true)
   {
      $json_response['success'] = false;
      $json_response['error'] = $error_type;
   }
   else
   {
      $json_response['success'] = true;
      $json_response['response_string'] = "An SMS has been sent with the new password.";
   }

   //json encode the array before returning so it can be parsed by the JS
   echo json_encode($json_response);
}
catch (Exception $e)
{

   //return success
   $json_response = array(
       'success' => false,
       'error_string' => 'There was a server error. Please try again later. File: ' . $e->getFile() . ' Line:' . $e->getFile(),
       'trace' => $e->getTraceAsString(),
       'error' => $e->getMessage()
   );

   //json encode the array before returning so it can be parsed by the JS
   echo json_encode($json_response);
}

function sendResetEmail($target_email, $token) {
   $to = $target_email;
   $subject = "Connet Systems - Forgot Password";

   $msg = '<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w31.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
                <html>
                <head>
                    <title>' . $subject . '</title>
                </head>
                    <body style="font-family: Arial, sans-serif;    font-stretch: normal;   font-size: 12px;    font-style: normal; width:740px;">
                        <table border="0">
                            <tr>
                                <td>
                                    <img src="http://portal.connet-systems.com/img/Header.jpg" />
                                </td>
                            </tr>
                            <tr>
                                <td style="padding-left: 28px; padding-right: 28px;">

                                    <h1 style="color:#3c5e6e;font-size:21px;">Greetings!</h1>

                                    <p>This is an automated response to your request to reset your password.</p>

                                    <p style="color:#dd0000"><b>You can follow the link below to continue:</b></p>

                                    <a href="https://' . $_SERVER['SERVER_NAME'] . '/reset_password.php?token=' . $token . '">[RESET MY PASSWORD]</a></p>

                                    <p>If you did not submit this request, kindly ignore this email. If you receive this email repeatedly, please contact us a support@connet-systems.com</p>

                                    <p><br>Kind Regards,</p>

                                    <p><strong>The Connet Systems Team</strong></p>

                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <img style="margin-bottom:-100px;" src="http://portal.connet-systems.com/img/Footer.jpg" />
                                </td>
                            </tr>
                        </table>
                    </body>
                </html>';

   $msg = wordwrap($msg, 70);

   $final_msg = preparehtmlmailE($msg); // give a function your html*

   $m = mail($to, $subject, $final_msg['multipart'], $final_msg['headers']);

   if ($m == 1) {
      return 1;
   }
}

function preparehtmlmailE($html) {

   //clog::info('Beginning HTML mail preparation');

   preg_match_all('~<img.*?src=.([\/.a-z0-9:_-]+).*?>~si', $html, $matches);
   $i = 0;
   $paths = array();

   foreach ($matches[1] as $img) {
      $img_old = $img;

      if (strpos($img, "http://") == false) {
         $uri = parse_url($img);
         $paths[$i]['path'] = $_SERVER['DOCUMENT_ROOT'] . $uri['path'];
         $content_id = md5($img);
         $html = str_replace($img_old, 'cid:' . $content_id, $html);
         $paths[$i++]['cid'] = $content_id;
      }
   }

   //clog::info('Processing HTML');

   $boundary = "--" . md5(uniqid(time()));
   $headers = "MIME-Version: 1.0\n";
   $headers .="Content-Type: multipart/mixed; boundary=\"$boundary\"\n";
   $headers .= "From: customer@connet-systems.com\r\n";

   $multipart = '';
   $multipart .= "--$boundary\n";
   $kod = 'utf-8';
   $multipart .= "Content-Type: text/html; charset=$kod\n";
   $multipart .= "Content-Transfer-Encoding: Quot-Printed\n\n";
   $multipart .= "$html\n\n";

   foreach ($paths as $path) {
      //clog::info('Processing HTML IMAGES');
      if (file_exists($path['path']))
         $fp = fopen($path['path'], "r");
      if (!$fp) {
         echo "No file";
         return false;
      }

      $imagetype = substr(strrchr($path['path'], '.'), 1);
      $file = fread($fp, filesize($path['path']));
      fclose($fp);

      $message_part = "";

      switch ($imagetype) {
         case 'png':
         case 'PNG':
            $message_part .= "Content-Type: image/png";
            break;
         case 'jpg':
         case 'jpeg':
         case 'JPG':
         case 'JPEG':
            $message_part .= "Content-Type: image/jpeg";
            break;
         case 'gif':
         case 'GIF':
            $message_part .= "Content-Type: image/gif";
            break;
      }

      $message_part .= "; file_name = \"" . basename($path['path']) . "\"\n";
      $message_part .= 'Content-ID: <' . $path['cid'] . ">\n";
      $message_part .= "Content-Transfer-Encoding: base64\n";
      $message_part .= "Content-Disposition: inline; filename = \"" . basename($path['path']) . "\"\n\n";
      $message_part .= chunk_split(base64_encode($file)) . "\n";
      $multipart .= "--$boundary\n" . $message_part . "\n";
   }

   $multipart .= "--$boundary--\n";
   $multipart = wordwrap($multipart, 70);

   return array('multipart' => $multipart, 'headers' => $headers);
}

function generateUniqueEmailToken($conn) {
   $token = "";
   $is_token_unique = false;
   $retry_count = 1000;
   $count = 0;
   while (!$is_token_unique)
   {
      $token = makeRandomToken();

      //prepare the data
      $data = array(':password_reset_token' => $token);

      //prepare the query
      $stmt = $conn->prepare('SELECT * FROM account_details WHERE password_reset_token = :password_reset_token');

      //set the fetch mode
      $stmt->setFetchMode(PDO::FETCH_ASSOC);

      //execute it
      $stmt->execute($data);

      //fetch the data
      $user_account_details = $stmt->fetch();

      //check if there are any results
      if (!isset($user_account_details['password_reset_token']))
      {
         $is_token_unique = true;
      }

      $count ++;

      if ($count > $retry_count)
      {
         break;
      }
   }

   return $token;
}

function makeRandomToken()
{
   //generate our password reset code and set an expiry date
   $token = bin2hex(openssl_random_pseudo_bytes(12));

   return $token;
}
