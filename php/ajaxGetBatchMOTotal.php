<?php
/*
 * Simply pulls the MO totals of a batch. This script exists so we can call it from a curl_multi situation and speed up queries accross large sets of batches,
 *
 * predominantly used in batchlist.php
 */
//define('ERROR_REPORTING', true);
require_once('ConnetAutoloader.php');
require_once('allInclusive.php');
require_once("../PluginAPI/PluginAPIAutoload.php");

try
{
   if(isset($_REQUEST['batch_id']))
   {
      //$mo_total = MOSMS::getMOCountInBatchOldAndVerySlow($_POST['batch_id']);
      $mo_total = MOSMS::getMOCountInBatchByBatchId($_REQUEST['batch_id']);
      $result = array('success' => true, 'batch_id' => $_REQUEST['batch_id'], 'mo_total' => $mo_total);
   }
   else
   {
      $result = array('success' => false, 'error' => 'No batch ID was passed.');
   }
}
catch (Exception $e)
{
   $result = array('success' => false, 'error' => $e->getMessage());
}

echo json_encode($result);
exit;
