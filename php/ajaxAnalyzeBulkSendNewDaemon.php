<?php
session_start();

ini_set('display_errors', 1);

//--------------------------------------------------------------
// Constants
//--------------------------------------------------------------
//define('ERROR_REPORTING', (array_search('debug', $argv) !== false));

//--------------------------------------------------------------
// Includes
//--------------------------------------------------------------
require_once('ConnetAutoloader.php');
require_once('../daemon/daemon_config.php');

clog::setLogFilename("/var/log/connet/plugin/" . basename(__FILE__, '.php') . '.log');
clog::setBacktrace();
clog::setVerbose(ERROR_REPORTING);


try
{

   if (isset($_POST['service_id']))
   {

      //goes down if not declared here
      $redis = new Redis();
      $redis->connect(PLUGIN_DAEMON_REDIS_HOST, 6379);
      $redis->select(12);

      if(isset($_POST['job_name']) && $_POST['job_name'] != "")
      {
         clog::info('Checking status of job '. $_POST['job_name'].'  for service '. $_POST['service_id']);

         $batch_raw = $redis->hGetAll($_POST['job_name']);
         $result = array('success' => true,
             'action' => 'status', 'job_name' => $_POST['job_name'],
             'completed' => (isset($batch_raw['completed']) ? $batch_raw['completed']  : 0),
             'sends_processed' => (isset($batch_raw['sends_processed']) ? $batch_raw['sends_processed']  : 0),
             'job_data' => $batch_raw['result']);

         echo json_encode($result);
         exit;
      }
      else
      {
         clog::info('We have a job for service ID to queue: '. $_POST['service_id']);

         $redis_batch_info = array();
         $redis_batch_info['service_id'] = $_POST['service_id'];
         $redis_batch_info['sms_text'] = $_POST['sms_text'];

         //check the post variables and run the appropriate task
         if (isset($_POST['csv_source_file']) && $_POST['csv_source_file'] != "") {
            $redis_batch_info['csv_source_file'] = $_POST['csv_source_file'];
         }

         //check the post variables and run the appropriate task
         if (isset($_POST['contact_list_id']) && $_POST['contact_list_id'] != '0') {
            $redis_batch_info['contact_list_id'] = $_POST['contact_list_id'];
         }

         //check the post variables and run the appropriate task
         if (isset($_POST['numbers_string']) && $_POST['numbers_string'] != "") {
            $redis_batch_info['numbers_string'] = $_POST['numbers_string'];
         }

         //send it to redis
         $job_name = 'batch_job:'.$_POST['service_id'].':'.microtime();

         $redis->hmSet($job_name, $redis_batch_info);
         $redis->setTimeout($job_name, 86400); //24 hour expiry
         $redis->lPush(PLUGIN_DAEMON_REDIS_KEY_BATCH_JOB_LIST, $job_name);

         $result = array('success' => true, 'action' => 'queued', 'job_name' => $job_name);
         echo json_encode($result);
         exit;
      }
   }
   else
   {
      $result = array('success' => false, 'error' => 'No service ID was passed.');
      echo json_encode($result);
      exit;
   }
}
catch (Exception $e)
{
   $result = array('success' => false, 'error' => $e->getMessage());
   echo json_encode($result);
   exit;
}
