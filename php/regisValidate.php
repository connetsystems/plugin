<?php
	/**
	 * @author: Doug Jenkinson 
	 * This script allows us to save credentials on redis, and then sending a unqiue code to Kingfisher, 
	 * who can then check the redis server locally to verify authentication. Through this, we can avoid
	 * forcing the user to login again when using Kingfisher.
	 * 
	 * This can presumably be used for any seperate project or product, provided they live on the same server.
	 */ 
	
	/**
	 * SET ALL THE RELEVENT DETAILS HERE
	 */



	$account_id = $_GET['aId']; //set the account ID here
	$account_name = $_GET['aName']; //accoutn name here
	
	$service_id = $_GET['sId']; //set the service ID here
	$service_name = $_GET['sName']; //service name here
	
	$user_id = $_GET['uId']; //set the user ID here
	$user_name = $_GET['uName']; //user name here
	
	//generate a random key using the acount ID number
	$auth_key = md5(uniqid($account_id, true));
	
	/** 
	 * NOW SET THE REDIS HASH RECORD HERE, BY ASSIGNING THE RELEVENT KEY/VALUES, AND AN EXPIRY OF 2 MINS
	 */
	$auth_hmap = array('account_id' => ''.$account_id,
			'account_name' => $account_name, 
			'service_id' => $service_id, 
			'service_name' => $service_name, 
			'user_id' => $user_id, 
			'user_name' => $user_name);
	 
	$redis = new Redis();
 	$redis->connect('redismaster', 6379); //open the conection
 	$redis->select(9);
	$redis->hmset('auth_key:'.$auth_key, $auth_hmap); //set the hash map to the record
	$redis->expire('auth_key:'.$auth_key, 120); //set expiry to 2 minutes
	
	/**
	 * FOR TESTING
	 */
 	echo "Connection to server sucessfully<br/>";
 	//check whether server is running or not
 	echo "Server is running: ". $redis->ping()."<br/>";
	echo "Saved data: auth_key:".$auth_key . " - account_id: ". $account_id . " - account_name: ". $account_name;
	echo "<br/><br/>".$redis->hget("user:".$auth_key, "account_name");
	
	/**
	 * NOW REDIRECT TO KINGFISHER HERE
	 */
	 header("Location: http://kingfisher.connet-systems.com/client/auth?auth_key=".$auth_key); //can be sent via POST var too

?>