<?php

//collections.php - Mater DB
function getSACollections() {
    //open the DB object
    $conn = getPDOMasterCore();

    $qry = "SELECT CONCAT(route_collection_name,' (',collection_id,')') as route_collection_name,prefix_country_name,CONCAT(prefix_network_name,' (',mccmnc_d,')') as prefix_network_name,default_route,ported_route,route_collection_description,collection_id,selected_collection_route_id,mccmnc_d,
                dmatrix_route_id,dmatrix_route,mccmnc_dx,pmatrix_route_id,pmatix_route,mccmnc_px,units
                FROM (
                    SELECT rcrd.route_collection_id as 'collection_id',rcrd.route_collection_route_id as 'selected_collection_route_id', rcrd.route_collection_route_smsc_name as 'default_route',route_collection_route_match_mccmnc as 'mccmnc_d'
                    FROM core.core_route_collection_route rcrd
                    WHERE rcrd.route_collection_id in (14,37,91,92,69,86,54,5,67,42,34,40,74,73,98) #37,91,14,92,69,86,54,5,67
                    AND rcrd.route_collection_route_match_mccmnc in ('65501','65502','65507','65510')#,'65502','65507','65510'
                    AND rcrd.route_collection_route_match_priority = (SELECT min(route_collection_route_match_priority)
                    FROM core.core_route_collection_route
                    WHERE route_collection_id = rcrd.route_collection_id
                    AND route_collection_route_match_mccmnc = rcrd.route_collection_route_match_mccmnc
                    LIMIT 1)
                ) as DR
                LEFT OUTER JOIN (
                    SELECT rcrd.route_collection_id as 'dmatrix_route_id',rcrd.route_collection_route_smsc_name as 'dmatrix_route',route_collection_route_match_mccmnc as 'mccmnc_dx'
                    FROM core.core_route_collection_route_options rcrd
                    WHERE rcrd.route_collection_id in (14,37,91,92,69,86,54,5,67,42,34,40,74,73,98) #37,91,14,92,69,86,54,5,67
                    AND rcrd.route_collection_route_match_mccmnc in ('65501','65502','65507','65510')
                    AND rcrd.route_collection_route_match_priority = (SELECT min(route_collection_route_match_priority)
                    FROM core.core_route_collection_route_options
                    WHERE route_collection_id = rcrd.route_collection_id
                    AND route_collection_route_match_mccmnc = rcrd.route_collection_route_match_mccmnc
                    LIMIT 1)
                ) as DM
                on collection_id = dmatrix_route_id
                and mccmnc_d = mccmnc_dx
                INNER JOIN (
                    Select distinct prefix_mccmnc,prefix_network_name,prefix_country_name
                    from core.core_prefix
                    where prefix_country_name='South Africa'
                ) as PX
                on prefix_mccmnc = mccmnc_d
                INNER JOIN core_route_collection
                ON route_collection_id = collection_id
                INNER JOIN (
                    SELECT rcrd.route_collection_id as 'ported_route_id',rcrd.route_collection_route_smsc_name as 'ported_route',route_collection_route_match_mccmnc as 'mccmnc_p'
                    FROM core.core_route_collection_route rcrd
                    WHERE rcrd.route_collection_id in (14,37,91,92,69,86,54,5,67,42,34,40,74,73,98) #37,91,14,92,69,86,54,5,67
                    AND rcrd.route_collection_route_match_mccmnc in ('65501p','65502p','65507p','65510p')#,'65502p','65507p','65510p'
                    AND rcrd.route_collection_route_match_priority = (SELECT min(route_collection_route_match_priority)
                    FROM core.core_route_collection_route
                    WHERE route_collection_id = rcrd.route_collection_id
                    AND route_collection_route_match_mccmnc = rcrd.route_collection_route_match_mccmnc
                    LIMIT 1)
                ) AS P
                ON ported_route_id = collection_id
                and replace(mccmnc_p,'p','') = prefix_mccmnc
                LEFT OUTER JOIN (
                    SELECT rcrd.route_collection_id as 'pmatrix_route_id',rcrd.route_collection_route_smsc_name as 'pmatix_route',route_collection_route_match_mccmnc as 'mccmnc_px'
                    FROM core.core_route_collection_route_options rcrd
                    WHERE rcrd.route_collection_id in (14,37,91,92,69,86,54,5,67,42,34,40,74,73,98) #37,91,14,92,69,86,54,5,67
                    AND rcrd.route_collection_route_match_mccmnc in ('65501p','65502p','65507p','65510p')#,'65502p','65507p','65510p'
                    AND rcrd.route_collection_route_match_priority = (SELECT min(route_collection_route_match_priority)
                    FROM core.core_route_collection_route_options
                    WHERE route_collection_id = rcrd.route_collection_id
                    AND route_collection_route_match_mccmnc = rcrd.route_collection_route_match_mccmnc
                    LIMIT 1)
                ) AS PC
                ON ported_route_id = pmatrix_route_id
                and mccmnc_p = mccmnc_px
                LEFT OUTER JOIN (
                    SELECT sum(C.units) as 'units',mccmnc,route_collection_id
                    FROM (
                        SELECT sum(cdr_record_units) as 'units',
                        SPLIT_STR(cdr_record_reference,'|',2) as `service_id`,
                        SPLIT_STR(cdr_provider,'|',4) as 'mccmnc'
                        FROM reporting.cdr_record_archive cr
                        INNER JOIN reporting.cdr c
                        on cr.cdr_id = c.cdr_id
                        where date(cdr_record_date) >= DATE_FORMAT(now(), '%Y-%m-%d 00:00:00') and date(cdr_record_date) < DATE_FORMAT(now() + INTERVAL 1 DAY, '%Y-%m-%d 00:00:00')
                        and SPLIT_STR(cdr_provider,'|',4) in ('65501','65502','65507','65510')
                        and SPLIT_STR(cdr_record_reference,'|',1) not in ('legacycore')
                        #and SPLIT_STR(cdr_record_reference,'|',2)  = 536
                        group by SPLIT_STR(cdr_record_reference,'|',2),SPLIT_STR(cdr_provider,'|',4)
                        ) as C
                    INNER JOIN core.core_route r
                    ON C.service_id = r.service_id
                    AND C.mccmnc = r.route_mccmnc
                    #WHERE r.service_id = 536
                    WHERE r.route_msg_type = 'MTSMS'
                    and r.route_status = 'ENABLED'
                    and r.route_collection_id in (14,37,91,92,69,86,54,5,67,42,34,40,74,73,98)
                    GROUP BY mccmnc,route_collection_id
                ) as Total
                ON collection_id = Total.route_collection_id
                AND mccmnc_d = Total.mccmnc
                ORDER BY collection_id=14  desc,collection_id=92  desc,collection_id=37  desc,collection_id=91 desc,collection_id=86 desc,collection_id=98 desc,collection_id= 69 desc,collection_id= 54 desc,
                collection_id,mccmnc_d='65501' desc,mccmnc_d='65510' desc,mccmnc_d='65507' desc,mccmnc_d='65502' desc";

    //prepare the query
    $stmt = $conn->prepare($qry);

    //set the fetch mode
    $stmt->setFetchMode(PDO::FETCH_ASSOC);

    //execute it
    $stmt->execute();

    //run the query
    $colData = $stmt->fetchAll();

    return $colData;
}

/**
 * Returns all available routes configured for a collection via it's network ID
 *
 * @param $collection_id - ID of the collection to query
 * @param $mccmnc - the mccmnc code to filter the results by
 * @return mixed - THe resulting rows of the query
 */
function getAllNetworkRoutesOnCollection($collection_id, $mccmnc)
{
    $conn = getPDOMasterCore();

    //prepare the data
    $data = array(':collection_id' => $collection_id, ':mccmnc' => $mccmnc);

    $qry = "SELECT route_collection_route_id,route_collection_id,route_collection_route_smsc_name,route_collection_route_match_mccmnc,route_collection_route_match_priority
        FROM core.core_route_collection_route
        WHERE route_collection_id = :collection_id
        AND route_collection_route_match_mccmnc = :mccmnc
        AND route_collection_route_status = \"ENABLED\"
        ORDER BY route_collection_route_match_priority";

    //prepare the query
    $stmt = $conn->prepare($qry);

    //set the fetch mode
    $stmt->setFetchMode(PDO::FETCH_ASSOC);

    //execute it
    $stmt->execute($data);

    //run the query
    $route_data = $stmt->fetchAll();

    return $route_data;
}

function switchRouteOnCollection($collection_id, $mccmnc, $route_collection_route_id)
{
    $success = true;

    $routes_on_collection = getAllNetworkRoutesOnCollection($collection_id, $mccmnc);

    $conn = getPDOMasterCore();
    $priority_stepper = 10; //increments by 10
    foreach($routes_on_collection as $route)
    {
        //echo "GOT HERE ".$route['route_collection_route_id']." </br> ";
        if($route['route_collection_route_id'] == $route_collection_route_id)
        {
            //this is the route we are switching to, so make it highest priority
            $priority = 5;
        }
        else
        {
            //not this one, default to 10
            $priority = $priority_stepper;
            $priority_stepper += 5;
        }

        //prepare the data
        $data = array(':route_collection_route_id' => $route['route_collection_route_id'], ':priority' => $priority);

        //prepare the query
        $stmt = $conn->prepare("UPDATE core.core_route_collection_route
            SET route_collection_route_match_priority = :priority
            WHERE route_collection_route_id = :route_collection_route_id");

        $stmt->execute($data);
    }

    return $success;
}

///collectionsAfrica.php | collectionsEurope.php - Reporting DB
function getCollectionsInternational($collection_id) {
    //open the DB object Intense query changed to Reporting DB
    //$conn = getPDOReportingCore();
    $conn = getPDOMasterCore();

    //prepare the data
    $data = array(':collection_id' => $collection_id);

    $qry = "SELECT route_collection_name,prefix_country_name,prefix_network_name,default_route,route_collection_description,default_route_id,mccmnc_d,
            cdr_cost_per_unit,cdr_comments,cdr_type,cdr_id,cdr_effective_date,silverstreet_price,silverstreet_cur,clx_price,clx_cur,nexmo_price,nexmo_cur,
            cellfind_price,cellfind_cur,portal_price,portal_cur,
            CASE WHEN default_route = 'silverstreet_smsc' THEN IF(cdr_cost_per_unit <> silverstreet_price,'1','0')
                WHEN default_route = 'clx_smsc' THEN IF(cdr_cost_per_unit <> clx_price,'1','0')
                WHEN default_route = 'nexmo_smsc' THEN IF(cdr_cost_per_unit <> nexmo_price,'1','0')
                WHEN default_route = 'cellfindint_smsc' THEN IF(cdr_cost_per_unit <> cellfind_price,'1','0')
                WHEN default_route = 'portal_smsc' THEN IF(cdr_cost_per_unit <> portal_price,'1','0')
            END AS 'price_change'
            FROM (
            SELECT CONCAT(route_collection_name,' (',default_route_id,')') as route_collection_name,prefix_country_name,TRIM(SUBSTRING_INDEX(CONCAT(prefix_network_name,' (',mccmnc_d,')'),'/',-1)) as prefix_network_name,
            default_route,route_collection_description,default_route_id,mccmnc_d,
            (SELECT cdr_cost_per_unit
            from reporting.cdr c
            where SPLIT_STR(c.cdr_provider,'|',4) = mccmnc_d
            and SPLIT_STR(c.cdr_provider,'|',3) =  default_route
            order by cdr_effective_date desc,cdr_cost_per_unit desc limit 1) as 'cdr_cost_per_unit',
            (SELECT cdr_comments
            from reporting.cdr c
            where SPLIT_STR(c.cdr_provider,'|',4) = mccmnc_d
            and SPLIT_STR(c.cdr_provider,'|',3) =  default_route
            order by cdr_effective_date desc,cdr_cost_per_unit desc limit 1) as 'cdr_comments',
            (SELECT cdr_type
            from reporting.cdr c
            where SPLIT_STR(c.cdr_provider,'|',4) = mccmnc_d
            and SPLIT_STR(c.cdr_provider,'|',3) =  default_route
            order by cdr_effective_date desc,cdr_cost_per_unit desc limit 1) as 'cdr_type',
            (SELECT cdr_id
            from reporting.cdr c
            where SPLIT_STR(c.cdr_provider,'|',4) = mccmnc_d
            and SPLIT_STR(c.cdr_provider,'|',3) =  default_route
            order by cdr_effective_date desc,cdr_cost_per_unit desc limit 1) as 'cdr_id',
            (SELECT date(cdr_effective_date)
            from reporting.cdr c
            where SPLIT_STR(c.cdr_provider,'|',4) = mccmnc_d
            and SPLIT_STR(c.cdr_provider,'|',3) =  default_route
            order by cdr_effective_date desc,cdr_cost_per_unit desc limit 1) as 'cdr_effective_date',
            ROUND(s.price*10000) as 'silverstreet_price',s.price as 'silverstreet_cur',(replace(c.gw0,',','.')*10000) as 'clx_price','EUR' as 'clx_cur',
            ROUND(n.price*10000) as 'nexmo_price','EUR' as 'nexmo_cur',(cn.price) as 'cellfind_price',cn.curr as 'cellfind_cur',(p.price) as 'portal_price',p.curr as 'portal_cur'
            FROM (
            SELECT rcrd.route_collection_id as 'default_route_id',rcrd.route_collection_route_smsc_name as 'default_route',route_collection_route_match_mccmnc as 'mccmnc_d'
            FROM core.core_route_collection_route rcrd
            WHERE rcrd.route_collection_id = :collection_id
            AND rcrd.route_collection_route_match_priority = (SELECT min(route_collection_route_match_priority)
            FROM core.core_route_collection_route
            WHERE route_collection_id = rcrd.route_collection_id
            AND route_collection_route_match_mccmnc = rcrd.route_collection_route_match_mccmnc
            LIMIT 1)
            ) as DR
            INNER JOIN (
            Select distinct prefix_mccmnc,prefix_network_name,prefix_country_name
            from core.core_prefix
            ) as PX
            on prefix_mccmnc = mccmnc_d
            INNER JOIN core_route_collection
            ON route_collection_id = default_route_id
            LEFT OUTER JOIN provider_silverstreet s
            ON s.mccmnc = mccmnc_d
            LEFT OUTER JOIN provider_clx c
            ON concat(c.mcc,'',LPAD(`mnc`, 2, '0')) = mccmnc_d
            LEFT OUTER JOIN provider_nexmo n
            ON n.mccmnc = mccmnc_d
            LEFT OUTER JOIN provider_cellfind cn
            ON cn.mccmnc = mccmnc_d
            LEFT OUTER JOIN provider_portal p
            ON p.mccmnc = mccmnc_d
            Order by prefix_country_name,mccmnc_d
            ) as CD";

    //prepare the query
    $stmt = $conn->prepare($qry);

    //set the fetch mode
    $stmt->setFetchMode(PDO::FETCH_ASSOC);

    //execute it
    $stmt->execute($data);

    //run the query
    $colData = $stmt->fetchAll();

    return $colData;
}

function getCollectionsInternationalByCountryAndCollection($country, $collection_id) {

    $conn = getPDOMasterCore();

    $data = array();
    //prepare the data
    if ($country != 'Show All') {
        $data[':country'] = $country;
    }
    if ($collection_id != 'Show All') {
        $data[':collection_id'] = $collection_id;
    }

    $qry = "SELECT route_collection_name,prefix_country_name,prefix_network_name,default_route,route_collection_description,default_route_id,mccmnc_d,
            cdr_cost_per_unit,cdr_comments,cdr_type,cdr_id,cdr_effective_date,silverstreet_price,silverstreet_cur,clx_price,clx_cur,nexmo_price,nexmo_cur,
            cellfind_price,cellfind_cur,portal_price,portal_cur,
            CASE WHEN default_route = 'silverstreet_smsc' THEN IF(cdr_cost_per_unit <> silverstreet_price,'1','0')
                WHEN default_route = 'clx_smsc' THEN IF(cdr_cost_per_unit <> clx_price,'1','0')
                WHEN default_route = 'nexmo_smsc' THEN IF(cdr_cost_per_unit <> nexmo_price,'1','0')
                WHEN default_route = 'cellfindint_smsc' THEN IF(cdr_cost_per_unit <> cellfind_price,'1','0')
                WHEN default_route = 'portal_smsc' THEN IF(cdr_cost_per_unit <> portal_price,'1','0')
            END AS 'price_change'
            FROM (
            SELECT CONCAT(route_collection_name,' (',default_route_id,')') as route_collection_name,prefix_country_name,TRIM(SUBSTRING_INDEX(CONCAT(prefix_network_name,' (',mccmnc_d,')'),'/',-1)) as prefix_network_name,
            default_route,route_collection_description,default_route_id,mccmnc_d,
            (SELECT cdr_cost_per_unit
            from reporting.cdr c
            where SPLIT_STR(c.cdr_provider,'|',4) = mccmnc_d
            and SPLIT_STR(c.cdr_provider,'|',3) =  default_route
            order by cdr_effective_date desc,cdr_cost_per_unit desc limit 1) as 'cdr_cost_per_unit',
            (SELECT cdr_comments
            from reporting.cdr c
            where SPLIT_STR(c.cdr_provider,'|',4) = mccmnc_d
            and SPLIT_STR(c.cdr_provider,'|',3) =  default_route
            order by cdr_effective_date desc,cdr_cost_per_unit desc limit 1) as 'cdr_comments',
            (SELECT cdr_type
            from reporting.cdr c
            where SPLIT_STR(c.cdr_provider,'|',4) = mccmnc_d
            and SPLIT_STR(c.cdr_provider,'|',3) =  default_route
            order by cdr_effective_date desc,cdr_cost_per_unit desc limit 1) as 'cdr_type',
            (SELECT cdr_id
            from reporting.cdr c
            where SPLIT_STR(c.cdr_provider,'|',4) = mccmnc_d
            and SPLIT_STR(c.cdr_provider,'|',3) =  default_route
            order by cdr_effective_date desc,cdr_cost_per_unit desc limit 1) as 'cdr_id',
            (SELECT date(cdr_effective_date)
            from reporting.cdr c
            where SPLIT_STR(c.cdr_provider,'|',4) = mccmnc_d
            and SPLIT_STR(c.cdr_provider,'|',3) =  default_route
            order by cdr_effective_date desc,cdr_cost_per_unit desc limit 1) as 'cdr_effective_date',
            ROUND(s.price*10000) as 'silverstreet_price',s.price as 'silverstreet_cur',(replace(c.gw0,',','.')*10000) as 'clx_price','EUR' as 'clx_cur',
            ROUND(n.price*10000) as 'nexmo_price','EUR' as 'nexmo_cur',(cn.price) as 'cellfind_price',cn.curr as 'cellfind_cur',(p.price) as 'portal_price',p.curr as 'portal_cur'
            FROM (
            SELECT rcrd.route_collection_id as 'default_route_id',rcrd.route_collection_route_smsc_name as 'default_route',route_collection_route_match_mccmnc as 'mccmnc_d'
            FROM core.core_route_collection_route rcrd";

    if ($collection_id != "Show All" && is_numeric($collection_id)) {
        $qry .= " WHERE rcrd.route_collection_id =:collection_id ";
    } else {
        $qry .= " WHERE rcrd.route_collection_id IN (83,96,97,100) ";
    }


    $qry .= " AND rcrd.route_collection_route_match_priority = (SELECT min(route_collection_route_match_priority)
            FROM core.core_route_collection_route
            WHERE route_collection_id = rcrd.route_collection_id
            AND route_collection_route_match_mccmnc = rcrd.route_collection_route_match_mccmnc
            LIMIT 1)
            ) as DR
            INNER JOIN (
            Select distinct prefix_mccmnc,prefix_network_name,prefix_country_name
            from core.core_prefix ";

    if (isset($country) && ($country != "Show All")) {
        $qry .= " WHERE prefix_country_name =:country ";
    }

    $qry .= ") as PX
            on prefix_mccmnc = mccmnc_d
            INNER JOIN core_route_collection
            ON route_collection_id = default_route_id
            LEFT OUTER JOIN provider_silverstreet s
            ON s.mccmnc = mccmnc_d
            LEFT OUTER JOIN provider_clx c
            ON concat(c.mcc,'',LPAD(`mnc`, 2, '0')) = mccmnc_d
            LEFT OUTER JOIN provider_nexmo n
            ON n.mccmnc = mccmnc_d
            LEFT OUTER JOIN provider_cellfind cn
            ON cn.mccmnc = mccmnc_d
            LEFT OUTER JOIN provider_portal p
            ON p.mccmnc = mccmnc_d
            Order by prefix_country_name,mccmnc_d
            ) as CD";

    //prepare the query
    $stmt = $conn->prepare($qry);

    //set the fetch mode
    $stmt->setFetchMode(PDO::FETCH_ASSOC);

    //execute it
    if ($country != "Show All" || $collection_id != "Show All") {
        $stmt->execute($data);
    } else {
        $stmt->execute();
    }

    //run the query
    $colData = $stmt->fetchAll();

    return $colData;
}

//billingnew.php - Reporting DB
function getInternationalRoutesRates($prefix_country_name, $prefix_network_mccmnc, $route_collection_id, $startDate, $endDate) {
    //open the DB object
    $conn = getPDOReportingCore();

    //prepare the data
    $data = array(':startDate' => $startDate, ':endDate' => $endDate);
    //,  => $prefix_network_mccmnc,':route_collection_id' => $route_collection_id,':startDate' => $startDate,':endDate' => $endDate );

    if (isset($prefix_country_name) && ($prefix_country_name != "Show All")) {
        $prefix_country_name_statement = " AND p.prefix_country_name = :prefix_country_name ";
        $data[':prefix_country_name'] = $prefix_country_name;
    } else {
        $prefix_country_name_statement = "";
    }

    if (isset($prefix_network_mccmnc) && ($prefix_network_mccmnc != "Show All")) {
        $prefix_network_mccmnc_statement = " AND r.route_mccmnc = :prefix_network_mccmnc ";
        $data[':prefix_network_mccmnc'] = $prefix_network_mccmnc;
    } else {
        $prefix_network_mccmnc_statement = "";
    }

    if (isset($route_collection_id) && ($route_collection_id != "Show All")) {
        $route_collection_id_statement = " AND r.route_collection_id = :route_collection_id ";
        $data[':route_collection_id'] = $route_collection_id;
    } else {
        $route_collection_id_statement = "";
    }


    $qry = "SELECT concat(`Account Name`,' - ',`Service Name`) as 'Service_Name',service_id,COUNTRY,TRIM(SUBSTRING_INDEX(NETWORK,'/',-1)) as `NETWORK`,`STATUS`,RATE,CURRENCY,COLLECTION,route_collection_id,MCCMNC
                ,DEFAULTR,FULLNAME,
                (SELECT sum(cdr_record_units)
                FROM reporting.cdr_record_archive cr
                INNER JOIN reporting.cdr c
                on cr.cdr_id = c.cdr_id
                where date(cdr_record_date) >= :startDate and date(cdr_record_date) <= :endDate
                AND SPLIT_STR(cdr_record_reference,'|',2) = service_id
                and SPLIT_STR(cdr_provider,'|',4) = MCCMNC
                #and SPLIT_STR(c.cdr_provider,'|',3) =  concat(DEFAULTR,'_smsc')
                and SPLIT_STR(cdr_record_reference,'|',1) not in ('legacycore')) as 'DEFAULTTOTAL',
                (SELECT format(cdr_cost_per_unit/10000,4)
                from reporting.cdr c
                where SPLIT_STR(c.cdr_provider,'|',4) = MCCMNC
                and SPLIT_STR(c.cdr_provider,'|',3) =  concat(DEFAULTR,'_smsc')
                order by cdr_effective_date desc,cdr_cost_per_unit desc limit 1) as 'DEFAULTCOST',
                (SELECT cdr_comments
                from reporting.cdr c
                where SPLIT_STR(c.cdr_provider,'|',4) = MCCMNC
                and SPLIT_STR(c.cdr_provider,'|',3) =  concat(DEFAULTR,'_smsc')
                order by cdr_effective_date desc,cdr_cost_per_unit desc limit 1) as 'cdr_comments'
                FROM (
                SELECT `Account Name`,`Service Name`,service_id,COUNTRY,NETWORK,`STATUS`,RATE,CURRENCY,COLLECTION,Route.route_collection_id,MCCMNC
                ,replace(rcrd.route_collection_route_smsc_name,'_smsc','') as 'DEFAULTR',FULLNAME
                FROM
                (
                SELECT p.prefix_country_name as 'COUNTRY',
                concat(p.prefix_network_name,' (',r.route_mccmnc,')') as 'NETWORK',
                r.route_status as 'STATUS',
                format(r.route_billing/10000,4) as 'RATE',
                rc.route_collection_name as 'COLLECTION',
                rc.route_collection_id,
                r.route_mccmnc as 'MCCMNC',
                a.account_name as 'Account Name',
                s.service_name as 'Service Name',
                s.service_id,
                r.route_currency as 'CURRENCY',
                u.user_fullname as 'FULLNAME'
                FROM core.core_route r
                INNER JOIN core.core_service s
                ON r.service_id = s.service_id
                INNER JOIN core.core_account a
                ON s.account_id = a.account_id
                LEFT OUTER JOIN core.core_prefix p
                ON r.route_mccmnc = p.prefix_mccmnc
                INNER JOIN core.core_route_collection rc
                ON r.route_collection_id = rc.route_collection_id
                LEFT JOIN core.core_user u
                ON s.service_manager_user_id = u.user_id
                WHERE r.route_msg_type = 'MTSMS' AND s.service_status='ENABLED' $prefix_country_name_statement $prefix_network_mccmnc_statement $route_collection_id_statement
                GROUP BY p.prefix_country_name,
                r.route_status,
                format(r.route_billing/10000,4),
                rc.route_collection_name,
                r.route_mccmnc,
                a.account_name,
                s.service_name,
                r.route_currency,
                u.user_fullname
                ) as Route
                LEFT OUTER JOIN core.core_route_collection_route rcrd
                ON Route.route_collection_id = rcrd.route_collection_id
                AND MCCMNC = rcrd.route_collection_route_match_mccmnc
                AND rcrd.route_collection_route_match_priority = (SELECT min(route_collection_route_match_priority)
                FROM core.core_route_collection_route q
                WHERE q.route_collection_id = Route.route_collection_id
                AND q.route_collection_route_match_mccmnc = MCCMNC
                LIMIT 1)
                ) as SDATA order by 13 desc";
    
    //echo $qry;

    //prepare the query
    $stmt = $conn->prepare($qry);

    //set the fetch mode
    $stmt->setFetchMode(PDO::FETCH_ASSOC);

    //execute it
    $stmt->execute($data);

    //run the query
    $result = $stmt->fetchAll();

    return $result;
}

//billingnew.php - Reporting DB
function getListOfCountries() {
    //open the DB object
    $conn = getPDOReportingCore();

    $qry = "SELECT DISTINCT prefix_country_name
				FROM core.core_prefix
				ORDER BY prefix_country_name ASC";

    //prepare the query
    $stmt = $conn->prepare($qry);

    //set the fetch mode
    $stmt->setFetchMode(PDO::FETCH_ASSOC);

    //execute it
    $stmt->execute();

    //run the query
    $colData = $stmt->fetchAll();

    return $colData;
}

//billingnew.php - Reporting DB
function getListOfNetworks() {
    //open the DB object
    $conn = getPDOReportingCore();

    $qry = "SELECT DISTINCT prefix_mccmnc, prefix_network_name
                FROM core.core_prefix
                ORDER BY prefix_network_name";

    //prepare the query
    $stmt = $conn->prepare($qry);

    //set the fetch mode
    $stmt->setFetchMode(PDO::FETCH_ASSOC);

    //execute it
    $stmt->execute();

    //run the query
    $colData = $stmt->fetchAll();

    return $colData;
}

//billing.php | billingnew.php - Reporting DB
function getNetworkViaCountryName($prefix_country_name) {
    //open the DB object
    $conn = getPDOReportingCore();

    //prepare the data
    $data = array(':prefix_country_name' => $prefix_country_name);

    $qry = "SELECT DISTINCT prefix_mccmnc, prefix_network_name, prefix_country_name
               FROM core.core_prefix
               WHERE prefix_country_name = :prefix_country_name
               ORDER BY prefix_network_name ASC";

    //prepare the query
    $stmt = $conn->prepare($qry);

    //set the fetch mode
    $stmt->setFetchMode(PDO::FETCH_ASSOC);

    //execute it
    $stmt->execute($data);

    //run the query
    $colData = $stmt->fetchAll();

    return $colData;
}

//billing.php | billingnew.php - Reporting DB
function getListOfCollections() {
    //open the DB object
    $conn = getPDOReportingCore();

    $qry = "SELECT DISTINCT route_collection_name, route_collection_id
                FROM core.core_route_collection
                ORDER BY route_collection_name";

    //prepare the query
    $stmt = $conn->prepare($qry);

    //set the fetch mode
    $stmt->setFetchMode(PDO::FETCH_ASSOC);

    //execute it
    $stmt->execute();

    //run the query
    $colData = $stmt->fetchAll();

    return $colData;
}

function getListOfCollectionsInt() {
    //open the DB object
    $conn = getPDOReportingCore();

    $qry = "SELECT DISTINCT route_collection_name, route_collection_id
                FROM core.core_route_collection
                WHERE route_collection_id in (83,100,97,96)    
                ORDER BY route_collection_name";

    //prepare the query
    $stmt = $conn->prepare($qry);

    //set the fetch mode
    $stmt->setFetchMode(PDO::FETCH_ASSOC);

    //execute it
    $stmt->execute();

    //run the query
    $colData = $stmt->fetchAll();

    return $colData;
}

//billingnew.php - Reporting DB
function getCollectionsMCCMNC($prefix_network_mccmnc) {
    //open the DB object
    $conn = getPDOReportingCore();

    //prepare the data
    $data = array(':prefix_network_mccmnc' => $prefix_network_mccmnc);


    $qry = "SELECT DISTINCT route_collection_name, route_collection_id
                    FROM core.core_route_collection
                    WHERE route_collection_id in (select distinct route_collection_id
                                                    from core.core_route
                                                    where route_collection_id is not null and route_mccmnc in (:prefix_network_mccmnc))
                    ORDER BY route_collection_name";

    //prepare the query
    $stmt = $conn->prepare($qry);

    //set the fetch mode
    $stmt->setFetchMode(PDO::FETCH_ASSOC);

    //execute it
    $stmt->execute($data);

    //run the query
    $colData = $stmt->fetchAll();

    return $colData;
}

//billing.php
function getBillingData($mcc, $collOpt, $serviceName) {

    //open the DB object
    $conn = getPDOReportingCore();

    //prepare the data
    //if mccmnc is provided then add to the data array
    if (isset($mcc) && !empty($mcc)) {
        $data = array(':route_mccmnc' => $mcc);
    }
    //if collection_id is provided, then add it to the data array
    if (!empty($collOpt) && is_numeric($collOpt)) {
        $data[":collection_id"] = $collOpt;
    }

    //query 
    $query = "SELECT concat(`Account Name`,' - ',`Service Name`) as 'Service_Name',service_id,
                COUNTRY,NETWORK,`STATUS`,RATE,CURRENCY,COLLECTION,Route.route_collection_id,MCCMNC
                ,replace(rcrd.route_collection_route_smsc_name,'_smsc','') as 'DEFAULT'
                ,replace(rcrp.route_collection_route_smsc_name,'_smsc','') as 'PORTED',FULLNAME
                FROM
                (
                SELECT p.prefix_country_name as 'COUNTRY',
                concat(p.prefix_network_name,' (',r.route_mccmnc,')') as 'NETWORK',
                r.route_status as 'STATUS',
                format(r.route_billing/10000,4) as 'RATE',
                rc.route_collection_name as 'COLLECTION',
                rc.route_collection_id,
                r.route_mccmnc as 'MCCMNC',
                a.account_name as 'Account Name',
                s.service_name as 'Service Name',
                s.service_id,
                r.route_currency as 'CURRENCY',
                u.user_fullname as 'FULLNAME'
                FROM core.core_route r
                INNER JOIN core.core_service s
                ON r.service_id = s.service_id
                INNER JOIN core.core_account a
                ON s.account_id = a.account_id
                LEFT OUTER JOIN core.core_prefix p
                ON r.route_mccmnc = p.prefix_mccmnc
                INNER JOIN core.core_route_collection rc
                ON r.route_collection_id = rc.route_collection_id
                LEFT JOIN core.core_user u
                ON s.service_manager_user_id = u.user_id
                WHERE r.route_msg_type = 'MTSMS'  AND p.prefix_country_name = 'South Africa' ";

    //if route mccmnc is provided, append the following the following query and bind data
    if (isset($mcc) && !empty($mcc)) {
        $query .=" AND r.route_mccmnc =:route_mccmnc ";
    }
    //if route collection id is provided, append the following the following query and bind data
    if (!empty($collOpt) && is_numeric($collOpt)) {

        $query .= " AND rc.route_collection_id =:collection_id ";
    }
    //if serviceName is provided, append the following query
    if (isset($serviceName) && !empty($serviceName)) {
        $query .=" AND (s.service_name  LIKE  '%" . $serviceName . "%' OR a.account_name LIKE '%" . $serviceName . "%')";
    }

    $query .= "GROUP BY p.prefix_country_name,
                r.route_status,
                format(r.route_billing/10000,4),
                rc.route_collection_name,
                r.route_mccmnc,
                a.account_name,
                s.service_name,
                r.route_currency,
                u.user_fullname
                ) as Route
                LEFT OUTER JOIN core.core_route_collection_route rcrp
                ON Route.route_collection_id = rcrp.route_collection_id
                AND concat(MCCMNC,'p') = rcrp.route_collection_route_match_mccmnc
                AND rcrp.route_collection_route_match_priority = (SELECT min(route_collection_route_match_priority)
                             FROM core.core_route_collection_route
                             WHERE route_collection_id = Route.route_collection_id
                             AND route_collection_route_match_mccmnc = concat(MCCMNC,'p')
                             LIMIT 1)
                LEFT OUTER JOIN core.core_route_collection_route rcrd
                ON Route.route_collection_id = rcrd.route_collection_id
                AND MCCMNC = rcrd.route_collection_route_match_mccmnc
                AND rcrd.route_collection_route_match_priority = (SELECT min(route_collection_route_match_priority)
                             FROM core.core_route_collection_route q
                             WHERE q.route_collection_id = Route.route_collection_id
                             AND q.route_collection_route_match_mccmnc = MCCMNC
                             LIMIT 1);
                ";

    //prepare the query
    $stmt = $conn->prepare($query);

    //execute it
    //first check if atleast one variable is added to then bind, else execute without binding
    if (!empty($mcc) || !empty($collOpt)) {
        $stmt->execute($data);
    } else {
        $stmt->execute();
    }

    //fetch the data
    $billingData = $stmt->fetchAll();

    $conn = null;

    return $billingData;
}

//billing.php
function getCollectionCost($mcc, $distinct_smsc) {

    //open the DB object
    $conn = getPDOReportingCore();

    //prepare the data
    //$data = array(":distinct_smsc" => $distinct_smsc, ":mcc" => $mcc);

    $qry = "SELECT mccmnc, smsc, cost, cdr_comments
            FROM (
            select SPLIT_STR(cdr_provider, '|', 4) AS mccmnc, SPLIT_STR(c.cdr_provider, '|', 3)
            AS smsc, (cdr_cost_per_unit /10000) as cost, cdr_comments, cdr_effective_date
            from reporting.cdr c
            where SPLIT_STR(cdr_provider, '|', 4) ";

    if (!empty($mcc) && is_numeric($mcc)) {
        $qry .= " = " . $mcc;
    } else {
        $qry .= " in ('65502', '65510', '65501', '65507') ";
    }

    $qry .= " AND SPLIT_STR(c.cdr_provider, '|', 3) in ( " . $distinct_smsc . " )

    order by SPLIT_STR(c.cdr_provider, '|', 3), cdr_effective_date desc
    ) AS DA
    INNER JOIN (
    select SPLIT_STR(cdr_provider, '|', 4) AS dmccmnc, SPLIT_STR(c.cdr_provider, '|', 3) AS dsmsc, max(cdr_effective_date) as 'mdate'
    from reporting.cdr c
    where SPLIT_STR(cdr_provider, '|', 4) ";

    if (!empty($mcc) && is_numeric($mcc)) {
        $qry .= " = " . $mcc;
    } else {
        $qry .= " IN ('65502', '65510', '65501', '65507') ";
    }

    $qry .= " AND SPLIT_STR(c.cdr_provider, '|', 3) in ( " . $distinct_smsc . " )
    group by SPLIT_STR(cdr_provider, '|', 4), SPLIT_STR(c.cdr_provider, '|', 3)
    ) AS DD
    ON dmccmnc = mccmnc
    AND dsmsc = smsc
    AND mdate = cdr_effective_date";
    
    //echo $qry;
    //prepare the query
    $stmt = $conn->prepare($qry);

    //set the fetch mode
    $stmt->setFetchMode(PDO::FETCH_ASSOC);

    //execute it
    $stmt->execute();

    //fetch the data
    $billingUnits = $stmt->fetchAll();

    return $billingUnits;
}

//billing.php
function getCollections() {
    //open the DB object
    $conn = getPDOReportingCore();

    //prepare the data
    //No data to prepare
    //prepare the query
    $stmt = $conn->prepare("SELECT DISTINCT route_collection_name, route_collection_id
                FROM core.core_route_collection
                ORDER BY route_collection_name");

    //set the fetch mode
    $stmt->setFetchMode(PDO::FETCH_ASSOC);

    //execute it
    $stmt->execute();
    //
    //fetch the data
    $countryData = $stmt->fetchAll();

    //echo "Affected rows: ".$conn->affected_rows;

    return $countryData;
}

//billing.php
function getBillingUnits($start_date, $end_date, $mcc) {
    //open the DB object
    $conn = getPDOReportingCore();

    //prepare the data
    $data = array(':start_date' => $start_date, ':end_date' => $end_date);

    if(isset($mcc) && is_numeric($mcc)){
        $data[':mcc'] = $mcc;

    }

    $query = "SELECT sum(cdr_record_units) as 'units', SPLIT_STR(cdr_record_reference,'|',2) as 'service_id'
                FROM reporting.cdr_record_archive cr
                INNER JOIN reporting.cdr c
                on cr.cdr_id = c.cdr_id
                where date(cdr_record_date) >= :start_date and date(cdr_record_date) <= :end_date ";


    if(isset($mcc) && is_numeric($mcc)) {
        $query .= " and SPLIT_STR(cdr_provider,'|',4) = :mcc ";
    }else{
        $query .= " and SPLIT_STR(cdr_provider,'|',4)  IN ('65502','65510','65507','65501') ";
    }

    $query .= " and SPLIT_STR(cdr_record_reference,'|',1) not in ('legacycore')
                group by SPLIT_STR(cdr_record_reference,'|',2) ";

    //prepare the query
    $stmt = $conn->prepare($query);

    //set the fetch mode
    $stmt->setFetchMode(PDO::FETCH_ASSOC);

    //execute it
    $stmt->execute($data);

    //fetch the data
    $billingUnits = $stmt->fetchAll();

    return $billingUnits;
}
