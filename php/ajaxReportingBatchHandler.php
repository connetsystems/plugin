<?php
    /**
     * This script is called by the manage services page, and runs certain ajax based tasks
     * @author - Doug Jenkinson
     */
    require_once("allInclusive.php");
    require_once("../PluginAPI/PluginAPIAutoload.php");

    try
    {
        session_start();
        $service_helper = new sqlHelperServices();

        //check if the user is logged in, if not, redirect back to login
        if(!isset($_SESSION['userId']))
        {
            $result = array('success' => false, 'reason' => 'session_expired');
            echo json_encode($result);
            die();
        }

        if(isset($_POST['task']))
        {
            $task = $_POST['task'];
            $result['success'] = false;
            switch($task)
            {
                case "change_batch_status" :
                    //get the relevant values
                    $service_id = $_POST['serviceId'];
                    $batch_id = $_POST['batchId'];
                    $batch_status = $_POST['batchStatus'];

                    //get the batch replies
                    $result['success'] = Batch::setBatchStatus($batch_id, $batch_status);
                    break;
                case "get_batch_reply_count" :
                    //get the relevant values
                    $service_id = $_POST['serviceId'];
                    $batch_id = $_POST['batchId'];

                    //get the batch replies
                    $result['reply_total'] = MOSMS::getMOCountInBatchByBatchId($batch_id);
                    $result['success'] = true;
                    break;
                case "get_batch_overview_chart_data" :
                    //get the relevant values
                    $service_id = $_POST['serviceId'];
                    $batch_id = $_POST['batchId'];

                    //get all the data for the graph by day
                    $chart_data = Batch::getSendRateChartData($batch_id);

                    $temp_date = new DateTime();
                    $result['charts'] = array();
                    $send_rate_chart = array();
                    $send_rate_chart['labels'] = array();
                    $send_rate_chart['values'] = array();
                    foreach($chart_data as $timestamp => $count)
                    {
                        array_push($send_rate_chart['labels'], $temp_date->setTimestamp($timestamp)->format('H:i'));
                        array_push($send_rate_chart['values'], $count);
                    }
                    $result['charts']['send_rate_chart'] = $send_rate_chart;


                    //get all the data for the graph by day
                    $chart_data_dlr = Batch::getDeliveryOverIntervalChartData($batch_id);

                    $dlr_and_mo_chart = array();
                    $dlr_and_mo_chart['labels'] = array();
                    $dlr_and_mo_chart['delivered'] = array();
                    $dlr_and_mo_chart['replies'] = array();
                    foreach($chart_data_dlr as $timestamp => $send_stats)
                    {
                        array_push($dlr_and_mo_chart['labels'], $temp_date->setTimestamp($timestamp)->format('H:i'));
                        array_push($dlr_and_mo_chart['delivered'], $send_stats['delivered']);
                        array_push($dlr_and_mo_chart['replies'], $send_stats['replies']);
                    }
                    $result['charts']['dlr_and_mo_chart'] = $dlr_and_mo_chart;

                    $result['success'] = true;
                    break;

                case "get_batch_network_send_chart_data" :
                    //get the relevant values
                    $service_id = $_POST['serviceId'];
                    $batch_id = $_POST['batchId'];

                    //get all the data for the graph by day
                    $result['networks'] = Batch::getNetworkSendData($service_id, $batch_id);
                    $result['success'] = true;
                    break;
            }
        }
        else
        {
            $result = array('success' => false);
        }
    }
    catch(Exception $e)
    {
        $result = array('success' => false, 'message' => $e->getMessage());
    }

    //json encode the array before returning so it can be parsed by the JS
    echo json_encode($result);

