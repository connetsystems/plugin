<?php
    require_once("allInclusive.php");
    require_once("../PluginAPI/PluginAPIAutoload.php");

    try
    {
        session_start();

        //check if the user is logged in, if not, redirect back to login
        if(!isset($_SESSION['userId']))
        {
            die();
        }

        //for pagination, get the page number for services, or set to default 1
        if(isset($_POST['page']))
        {
            $page = $_POST['page'];
        }
        else
        {
            $page = 1;
        }

        if(isset($_POST['limit']))
        {
            $limit = $_POST['limit'];
        }
        else
        {
            $limit = 25; //default
        }

        //for search, detect search variable
        if(isset($_POST['search_type']) && $_POST['search_type'] != "" && isset($_POST['search_term']) && $_POST['search_term'] != "")
        {
            $run_search = true;
            $search_type = $_POST['search_type'];
            $search_term = $_POST['search_term'];
        }
        else
        {
            $run_search = false;
        }

        //declare the service helper
        $service_helper = new sqlHelperServices();
        $page_services = array();

        if($_SESSION['userSystemAdmin'] == 1)
        {
            //run a search or just show the general list
            if(!$run_search)
            {
                $page_services = $service_helper->getAllServicesAdmin($page, $limit);
            }
            else
            {
                $page_services = $service_helper->getAllServicesAdminSearch($page, $limit, $search_type, $search_term);
            }
        }
        else
        {
            //get the user permissions
            $perm_service_ids = array();
            $permissions = PermissionsHelper::getAllPermissions();

            //filter out the service IDs, that's all we need
            foreach($permissions as $perm_service_id => $permission)
            {
                array_push($perm_service_ids, $perm_service_id);
            }

            //run a search or just show the general list
            if(!$run_search)
            {
                $page_services = $service_helper->getAllServicesClient($perm_service_ids, $page, $limit);
            }
            else
            {
                $page_services = $service_helper->getAllServicesClientSearch($perm_service_ids , $page, $limit, $search_type, $search_term);
            }
        }



        $result = array('success' => true, 'page' => $page, 'is_search' => $run_search, 'services' => $page_services);
    }
    catch(Exception $e)
    {
        $result = array('success' => false, 'message' => $e->getMessage());
    }

    //json encode the array before returning so it can be parsed by the JS
    echo json_encode($result);

