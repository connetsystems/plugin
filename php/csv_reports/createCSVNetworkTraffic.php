<?php
    session_start();

    //check if the user is logged in, if not, redirect back to login
    if (!isset($_SESSION['userId']))
    {
        //nope nope nope nope nope
        header('Location: /index.php');
        die();
    }

    require_once("../allInclusive.php");
    require_once("../../PluginAPI/PluginAPIAutoload.php");

    //get the data for the csv file
    if(isset($_GET['service_id']))
    {
        $service_id = $_GET['service_id'];
        if(isset($_GET['start_date']))
        {
            $start_date = $_GET['start_date'];
        }
        else
        {
            $start_date = date('Y-m-d');
        }

        if(isset($_GET['end_date']))
        {
            $end_date = $_GET['end_date'];
        }
        else
        {
            $end_date = date('Y-m-d', strtotime(date('Y-m-d') . ' +1 day')); //elasticsearch includes the "from" date in the aggregation, but not the "to"
        }

        //our holder for all the services
        $all_service_data = array();
        $service_name = "";
        if($service_id == -1 && $_SESSION['serviceId'] == $_SESSION['resellerId'])
        {
            //show all services
            if(isset($_GET['postpaid']) && $_GET['postpaid'] == "true")
            {
                $service_name = "All Services POSTPAID";
                if(isset($_SESSION['accountId']))
                {
                    $account_id = $_SESSION['accountId'];
                }
                else
                {
                    $account_id = null;
                }

                $all_reseller_services = getServicesInAccountPostPaid($account_id);
            }
            else
            {
                $service_name = "All Services ALL";
                $all_reseller_services = getServicesInAccount($_SESSION['accountId']);
            }

            foreach($all_reseller_services as $service)
            {
                $service_stats = Service::getFullNetworkSendStats($service['service_id'], $start_date, $end_date);
                array_push($all_service_data, $service_stats);
            }
        }
        else if($service_id == -1 && $_SESSION['serviceId'] != $_SESSION['resellerId'])
        {
            $service_name = "Service ID ".$_SESSION['serviceId'];
            //user has selected one service to view
            $service_stats = Service::getFullNetworkSendStats($_SESSION['serviceId'], $start_date, $end_date);
            array_push($all_service_data, $service_stats);
        }
        else
        {
            $service_name = "Service ID ".$service_id;

            //user has selected one service to view
            $service_stats = Service::getFullNetworkSendStats($service_id, $start_date, $end_date);
            array_push($all_service_data, $service_stats);
        }

        $filename = 'Network Traffic ('.$service_name.') ['.$start_date.' - '.$end_date.'].csv';

        header('Content-Type: application/csv');
        header('Content-Disposition: attachment; filename="'.$filename.'";');

        $output = fopen('php://output', 'w');
        fputcsv($output, array('sep=,'), "\t");

        $columns = array("Service Name", "Network", "Country", "Sent", "Units", "Cost per Unit", "Total Cost", "Delivered", "Pending", "Failed", "Rejected");
        fputcsv($output, $columns, ',');

        foreach($all_service_data as $service_data)
        {
            foreach($service_data['networks'] as $network_data)
            {
                //get some info
                $service_name = getServiceName($service_data['service_id']);
                $network_name = getNetFromMCC($network_data['mccmnc']);
                $country_name = getCountryFromMCC($network_data['mccmnc']);

                //now cycle through any potential rates, as a certain date range may feature a rate change, in which case there will be multiple records
                foreach($network_data['rates'] as $rate_data)
                {
                    $line = array();
                    //output the data for CSV reporter
                    array_push($line, $service_name.' ('.$service_data['service_id'].')');
                    array_push($line, $network_name.' ('.$network_data['mccmnc'].')');
                    array_push($line, $country_name);
                    array_push($line, $rate_data['total_sends']);
                    array_push($line, $rate_data['total_units']);
                    array_push($line, number_format(($rate_data['rate']/10000), 2, '.', ' '));
                    array_push($line, number_format(($rate_data['total_cost']/10000), 2, '.', ' '));
                    array_push($line, (isset($network_data['delivered']) ? $network_data['delivered'] : 0));
                    array_push($line, (isset($network_data['pending']) ? $network_data['pending'] : 0));
                    array_push($line, (isset($network_data['failed']) ? $network_data['failed'] : 0));
                    array_push($line, (isset($network_data['rejected']) ? $network_data['rejected'] : 0));

                    fputcsv($output, $line, ',');
                }
            }
        }
    }
    else {
        echo "Invalid variables.";
    }


