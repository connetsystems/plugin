<?php
session_start();

//check if the user is logged in, if not, redirect back to login
if (!isset($_SESSION['userId'])) {
    //nope nope nope nope nope
    header('Location: /index.php');
    die();
}

require_once("../allInclusive.php");
require_once("../../PluginAPI/PluginAPIAutoload.php");

//get the data for the csv file
if(isset($_GET['service_id']) && isset($_GET['batch_id']))
{
    $service_id = $_GET['service_id'];
    $batch_id = $_GET['batch_id'];

    //get the batch info and check if we can acutally view this batch using permissions
    $batch_obj = Batch::getBatchData($batch_id);
    $service_id = $batch_obj['service_id'];

    if (!PermissionsHelper::hasServicePermission($service_id))
    {
        echo "You do not have permission to view this data.";
        die();
    }

    //get the service object
    $service_obj = new Service($service_id);

    //get the send type to display
    if (isset($_GET['send_type']))
    {
        $send_type = $_GET['send_type'];
    }
    else
    {
        //default to all sends
        $send_type = "all";
    }


    $filename = 'Batch ('.$service_obj->service_name.') - Ref '.$batch_obj['batch_reference'].'] - '.$batch_obj['batch_scheduled'].'.csv';

    header('Content-Type: application/csv');
    header('Content-Disposition: attachment; filename="'.$filename.'";');

    $output = fopen('php://output', 'w');
    fputcsv($output, array('sep=,'), "\t");

    $columns = array("Message ID", "Date", "Number", "Content", "Network Code", "Network Name", "Country", "Cost", "Units", "Status");
    fputcsv($output, $columns, ',');

    //getting the records

    //need to load up the network index for speed
    $batch_helper = new sqlHelperBatches();
    $networks = $batch_helper->getAllNetworks();
    $network_index = array();
    foreach($networks as $network)
    {
        $temp_net = array();
        $temp_net['name'] = $network['prefix_network_name'];
        $temp_net['country'] = $network['prefix_network_country'];
        $network_index[$network['prefix_mccmnc']] = $temp_net;
    }

    //need to get all the objects by batch ID
    $mtsms_size = 500; //this number will be divided by 5 automatically to cater for the shards
    $pass_count = 1;
    $scroll_id = null;

    $keep_searching = true;
    while($keep_searching)
    {
        $mtsms_arr = Batch::getMTSMSArrayWithScrollId($batch_id, $mtsms_size, $scroll_id, $send_type);

        $scroll_id = $mtsms_arr['scroll_id'];

        if(count($mtsms_arr['objects']) < $mtsms_size)
        {
            $keep_searching = false;
        }

        foreach($mtsms_arr['objects'] as $send_obj)
        {
            $line = array();

            //output the data for CSV reporter
            array_push($line, $send_obj->mtsms_id);
            array_push($line, $send_obj->timestamp);
            array_push($line, $send_obj->dest);
            array_push($line, $send_obj->content); //helperSMSGeneral::restorePlaceholderValuesForDisplay(utf8_decode($send_obj->content)));
            array_push($line, $send_obj->mccmnc);
            array_push($line, $network_index[$send_obj->mccmnc]['name']);
            array_push($line, $network_index[$send_obj->mccmnc]['country']);
            array_push($line, $send_obj->route_billing);
            array_push($line, $send_obj->billing_units);
            array_push($line, DeliveryReportHelper::dlrMaskToString($send_obj->dlr, DeliveryReportHelper::DLR_STRING_PRETTY));

            fputcsv($output, $line, ',');
        }

        $pass_count ++;
    }

}
else
{
    echo "Invalid variables.";
}


