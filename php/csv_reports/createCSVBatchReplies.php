<?php
session_start();

//check if the user is logged in, if not, redirect back to login
if (!isset($_SESSION['userId'])) {
    //nope nope nope nope nope
    header('Location: /index.php');
    die();
}

require_once("../allInclusive.php");
require_once("../../PluginAPI/PluginAPIAutoload.php");

//get the data for the csv file
if(isset($_GET['service_id']) && isset($_GET['batch_id']))
{
    $service_id = $_GET['service_id'];
    $batch_id = $_GET['batch_id'];

    //get the batch info and check if we can acutally view this batch using permissions
    $batch_obj = Batch::getBatchData($batch_id);
    $service_id = $batch_obj['service_id'];

    if (!PermissionsHelper::hasServicePermission($service_id))
    {
        echo "You do not have permission to view this data.";
        die();
    }

    //get the service object
    $service_obj = new Service($service_id);

    $filename = 'Batch Replies ('.$service_obj->service_name.') [Batch Ref '.$batch_obj['batch_reference'].'].csv';

    header('Content-Type: application/csv');
    header('Content-Disposition: attachment; filename="'.$filename.'";');

    $output = fopen('php://output', 'w');
    fputcsv($output, array('sep=,'), "\t");

    $columns = array("Reply ID", "Date", "Source", "Content");
    fputcsv($output, $columns, ',');

    $mo_sms_objs = MOSMS::getAllByBatchId($batch_id);

    foreach($mo_sms_objs as $reply_obj)
    {
        $line = array();

        //output the data for CSV reporter
        array_push($line, $reply_obj->mosms_id);
        array_push($line, $reply_obj->timestamp);
        array_push($line, $reply_obj->src);
        array_push($line, $reply_obj->content); //helperSMSGeneral::restorePlaceholderValuesForDisplay(utf8_decode($send_obj->content)));

        fputcsv($output, $line, ',');
    }



}
else
{
    echo "Invalid variables.";
}


