<?php
    /**
     * This script is called by the manage services page, and runs certain ajax based tasks
     * @author - Doug Jenkinson
     */
    require_once("allInclusive.php");
    require_once("../PluginAPI/PluginAPIAutoload.php");

    try
    {
        session_start();
        $user_helper = new sqlHelperUsers();

        //check if the user is logged in, if not, redirect back to login
        if(!isset($_SESSION['userId']))
        {
            $result = array('success' => false, 'reason' => 'session_expired');
            echo json_encode($result);
            die();
        }

        if(isset($_POST['task']))
        {
            $task = $_POST['task'];
            $result['success'] = false;
            switch($task)
            {
                case "set_user_status" :
                    //get the relevant values
                    $user_id = $_POST['userId'];
                    $status = $_POST['status'];

                    $user_obj = new User($user_id);
                    $result['success'] = $user_obj->setStatus($status);
                    break;
                case "save_account_details" :
                    //get the relevant values
                    $user_id = $_POST['userId'];

                    $user_obj = new User($user_id);

                    $user_obj->first_name = $_POST['firstName'];
                    $user_obj->last_name = $_POST['lastName'];
                    $user_obj->company_name = $_POST['companyName'];
                    $user_obj->email_address = $_POST['emailAddress'];
                    $user_obj->cell_number = $_POST['cellNumber'];

                    $result['success'] = $user_obj->save();
                    break;
                case "save_new_password" :
                    //get the relevant values
                    $user_id = $_POST['userId'];
                    $new_password = $_POST['newPassword'];

                    $user_obj = new User($user_id);
                    $result['success'] = $user_obj->saveNewPassword($new_password);
                    break;
                case "toggle_role" :
                    //get the relevant values
                    $user_id = $_POST['userId'];
                    $role_id = $_POST['roleId'];
                    $checked = $_POST['checked'];

                    $user_obj = new User($user_id);
                    if($checked === "true")
                    {
                        $result['success'] = $user_obj->addRoleToUser($role_id);
                    }
                    else
                    {
                        $result['success'] = $user_obj->removeRoleFromUser($role_id);
                    }
                    break;
                case "assign_role_template" :
                    //get the relevant values
                    $user_id = $_POST['userId'];
                    $role_type = $_POST['roleTemplate'];

                    $user_obj = new User($user_id);
                    $result['success'] = $user_obj->assignAllRolesByType($role_type);

                    break;
                case "add_service_permission" :
                    //get the relevant values
                    $user_id = $_POST['userId'];
                    $service_id = $_POST['serviceId'];

                    $user_obj = new User($user_id);
                    if($user_obj->addServicePermission($service_id))
                    {
                        $result['success'] = true;
                    }
                    else
                    {
                        $result['success'] = false;
                        $result['reason'] = "permission_exists";
                    }

                    break;
                case "remove_service_permission" :
                    //get the relevant values
                    $user_id = $_POST['userId'];
                    $service_id = $_POST['serviceId'];

                    $user_obj = new User($user_id);
                    if($user_obj->removeServicePermission($service_id))
                    {
                        $result['success'] = true;
                    }
                    else
                    {
                        $result['success'] = false;
                        $result['reason'] = "permission_doesnt_exist";
                    }
                    break;

                case "delete_user" :
                    //get the relevant values
                    $user_id = $_POST['userId'];

                    $user_obj = new User($user_id);
                    if($user_obj->checkCanDeleteUser() === true)
                    {
                        $user_obj->delete();
                        $result['success'] = true;
                    }
                    else
                    {
                        $result['success'] = false;
                        $result['reason'] = "must_preserve";
                    }
                    break;

                case "create_new_user" :
                    $service_id = $_POST['serviceId'];

                    $user_is_admin = $_POST['userIsAdmin'];
                    $user_username = $_POST['userName'];
                    $user_first_name = $_POST['firstName'];
                    $user_last_name = $_POST['lastName'];
                    $user_password = $_POST['setPassword'];

                    $service_helper = new sqlHelperServices();

                    $input_error_array = array();
                    $input_valid = true; //assume valid until proven otherwise

                    //validate the user info
                    if(!$user_helper->isUsernameUnique($user_username))
                    {
                        $input_error_array['userName'] = "The user already exists with this username, please enter a different one.";
                        $input_valid = false;
                    }

                    //we need the account id
                    $service_data = $service_helper->getServiceData($service_id);
                    $account_id = $service_data['account_id'];

                    if(isset($service_id) && $service_id > 0 && $input_valid)
                    {
                        $new_user_id = User::createNewUser($service_id, $account_id, $user_username, $user_password, $user_first_name, $user_last_name, $user_is_admin);

                        if($new_user_id)
                        {
                            $result['success'] = true;
                            $result['user_id'] = $new_user_id;
                        }
                        else
                        {
                            $result['success'] = false;
                            $result['reason'] = "user_creation_failed";
                        }
                    }
                    else
                    {
                        $result['success'] = false;
                        $result['reason'] = "validation_failed";
                        $result['form_errors'] = $input_error_array;
                    }
                    break;
            }
        }
        else
        {
            $result = array('success' => false);
        }
    }
    catch(Exception $e)
    {
        $result = array('success' => false, 'message' => $e->getMessage());
    }

    //json encode the array before returning so it can be parsed by the JS
    echo json_encode($result);

