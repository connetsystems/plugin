<?php

require_once('config/dbConn.php');

$starting = 0;
$total = $_GET['total'];
$sd = $_GET['sd'];
$ed = $_GET['ed'];
$sId = $_GET['sId'];

$dateR = date("Y-m-d_H:i:s");
$fileN = "Batchlist_" . $type . "_" . $dateR;
$file = "../tempFiles/" . $fileN . ".csv";

set_time_limit(180);

//Download headers
//header('Content-type: text/plain');
header('Content-type: application/vnd.ms-excel');
header('Content-Disposition: attachment; filename="' . basename($file)) . '"';
header('Expires: 0');
header('Cache-Control: must-revalidate');
header('Pragma: public');
//Enable implicit flush & gz compression
ob_end_clean();
ob_implicit_flush();
ob_start("ob_gzhandler");
//Open out stream
$php_out = fopen('php://output', 'w');
fputcsv($php_out, array('sep=,'), "\t");

//Write header
$columns = array("Batch Id", "Id", "Date", "From", "Client", "Campaign", "Code", "Reference", "Content");
fputcsv($php_out, $columns);

//Flush data
ob_flush();
//Output results
define('ELASTIC_CHUNK_SIZE', 1000);
for ($starting; $starting < $total; $starting += ELASTIC_CHUNK_SIZE) {
   //fputcsv($php_out, 'Start');
   //getElastData($starting, $query, $startDateConv, $endDateConv, $admin, $php_out, ELASTIC_CHUNK_SIZE);
   getSQLData($sId, $sd, $ed, $starting, ELASTIC_CHUNK_SIZE, $php_out);
   //$modata = getBatchlistExtraMOs($bId, $type);
}
//Close  output stream, flush, exit
fclose($php_out);
ob_flush();
flush();
die();

function getSQLData($sId, $sd, $ed, $starting, $chunk_size, $handle) {
   $modata = getBatchlistAllMOsSQL($sId, $sd, $ed, $starting, $chunk_size);

   writeToFile($modata, $handle);
}

function getBatchlistAllMOsSQL($sId, $sd, $ed, $starting, $chunk_size) {
   //db object
   $conn = getPDOReportingCore();
   $conn->setAttribute(PDO::ATTR_EMULATE_PREPARES, FALSE);

   $data = array(':sId' => $sId, ':start_date' => $sd, ':end_date' => $ed, ':starting' => $starting, ':chunk_size' => $chunk_size);

   $query = "SELECT batch_id as 'batch_id',sms_id as 'Id',sms_timestamp as 'Date',sms_src as 'From',client_name as 'Client',ifnull(campaign_name,'API Interface') as 'Campaign',
                campaign_code as 'Campaign Code',batch_reference as 'Reference',sms_content as 'Content'
                FROM core.batch_sms s
                LEFT JOIN core.campaign_batch USING (batch_id)
                LEFT JOIN core.campaign_campaign USING (campaign_id)
                LEFT JOIN core.campaign_client USING (client_id)
                WHERE s.service_id =:sId
                AND date(`sms_timestamp`) >=:start_date AND date(`sms_timestamp`) <=:end_date
                AND s.sms_status = 'MOSMS'
                LIMIT :starting, :chunk_size";

   //prepare the query
   $stmt = $conn->prepare($query);

   //set fetch mode
   $stmt->setFetchMode(PDO::FETCH_ASSOC);

   //execute query
   $stmt->execute($data);

   //fetch data
   $extras = $stmt->fetchAll();

   //set connection object to null
   $conn = null;

   return $extras;
}

//echo $qry.$nl;
function writeToFile($modata, $handle) {
   if (isset($modata) && $modata != '') {
      foreach ($modata as $key => $value) {
         $csvData = array(
             $value['batch_id'],
             $value['Id'],
             $value['Date'],
             $value['From'],
             $value['Client'],
             $value['Campaign'],
             $value['Campaign Code'],
             $value['Reference'],
             $value['Content']

                 //preg_replace("/[^a-zA-Z0-9\/_|+ -]/", '', $value['content'])
         );

         fputcsv($handle, $csvData);
      }
   }
   ob_flush();
}

?>
