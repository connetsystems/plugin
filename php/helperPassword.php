<?php

/**
 * Class helperPassword
 * @author - Doug Jenkinson
 *
 * This class simply helps us out with password hashing and comparison in a centralized location.
 */

class helperPassword
{
    //private $options = array('cost' => 11); //not using any defaults at this moment

    static function createRandomPassword($char_length)
    {
        //source - http://stackoverflow.com/questions/5438760/generate-random-5-characters-string
        return $rand = substr(md5(microtime()),rand(0,26),8);
    }

    static function createHashedPassword($password)
    {
        return password_hash($password, PASSWORD_DEFAULT);
    }

    static function validatePassword($password, $hashed_comparison)
    {
        return password_verify($password , $hashed_comparison);
    }

    static function checkNeedsRehash($hashed_password)
    {
        return password_needs_rehash($hashed_password, PASSWORD_DEFAULT);
    }

}
