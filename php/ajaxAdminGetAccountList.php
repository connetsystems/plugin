<?php
    require_once("allInclusive.php");
    require_once("../PluginAPI/PluginAPIAutoload.php");

    try
    {
        session_start();

        //check if the user is logged in, if not, redirect back to login
        if(!isset($_SESSION['userId']))
        {
            die();
        }

        //for pagination, get the page number for services, or set to default 1
        if(isset($_POST['page']))
        {
            $page = $_POST['page'];
        }
        else
        {
            $page = 1;
        }

        if(isset($_POST['limit']))
        {
            $limit = $_POST['limit'];
        }
        else
        {
            $limit = 25; //default
        }

        //for search, detect search variable
        if(isset($_POST['search_type']) && $_POST['search_type'] != "" && isset($_POST['search_term']) && $_POST['search_term'] != "")
        {
            $run_search = true;
            $search_type = $_POST['search_type'];
            $search_term = $_POST['search_term'];
        }
        else
        {
            $run_search = false;
        }

        //declare the account helper
        $account_helper = new sqlHelperAccounts();

        //run a search or just show the general list
        if(!$run_search)
        {
            $page_accounts = $account_helper->getAllAccountsAdmin($page, $limit);
        }
        else
        {
            $page_accounts = $account_helper->getAllAccountsAdminSearch($page, $limit, $search_type, $search_term);
        }


        $result = array('success' => true, 'page' => $page, 'is_search' => $run_search, 'accounts' => $page_accounts);
    }
    catch(Exception $e)
    {
        $result = array('success' => false, 'message' => $e->getMessage());
    }

    //json encode the array before returning so it can be parsed by the JS
    echo json_encode($result);

