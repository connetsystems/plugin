<?php
require_once('config/dbConn.php');

function getMonthElastic($monthStart, $monthEnd)
{
	$monthStartConv = strtotime($monthStart);
	$monthStartConv = $monthStartConv * 1000;
	$monthEndConv = strtotime($monthEnd);
	$monthEndConv = $monthEndConv * 1000;

    $qry = '{
			  "query": {
			    "filtered": {
			      "query": {
			        "query_string": {
			          "query": "*",
			          "analyze_wildcard": true
			        }
			      },
			      "filter": {
			        "bool": {
			          "must": [
			            {
			              "range": {
			                "timestamp": {
			                  "gte": '.$monthStartConv.',
			                  "lte": '.$monthEndConv.'
			                }
			              }
			            }
			          ],
			          "must_not": []
			        }
			      }
			    }
			  },
			  "size": 0,
			  "aggs": {
			    "2": {
			      "date_histogram": {
			        "field": "timestamp",
			        "interval": "1M",
			        "pre_zone": "+02:00",
			        "pre_zone_adjust_large_interval": true,
			        "min_doc_count": 1,
			        "extended_bounds": {
			          "min": '.$monthStartConv.',
			          "max": '.$monthEndConv.'
			        }
			      },
			      "aggs": {
			        "3": {
			          "terms": {
			            "field": "mccmnc",
			            "size": 0,
			            "order": {
			              "1": "desc"
			            }
			          },
			          "aggs": {
			            "1": {
			              "sum": {
			                "script": "doc[\'billing_units\'].value",
			                "lang": "expression"
			              }
			            }
			          }
			        }
			      }
			    }
			  }
			}
    ';

    $allStats = runRawMCoreElasticsearchQuery($qry);

    return $allStats;
}

?>