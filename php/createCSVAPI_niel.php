<?php

require_once('config/dbConn.php');

//ini_set('memory_limit', '1024M');
//$tl = set_time_limit(30000);
//echo '<br><br><br><br><br>Tl='.$tl;
if (isset($_GET['cl']) && $_GET['cl'] != '') {
    $cl = 'service_id:' . $_GET['cl'];
    $range = $_GET['range'];
    $dateArr = explode(' - ', $_GET['range']);
    $startDate = $dateArr[0];
    $endDate = $dateArr[1];
    $reportName = $cl;
    $total = $_GET['total'];
    $starting = 0;

    if (isset($_GET['admin']) && $_GET['admin'] == 1) {
        $admin = 1;
    } else {
        $admin = 0;
    }
} else {
    $cl = 'service_id:*';
    $range = $_GET['range'];
    $dateArr = explode(' - ', $_GET['range']);
    $startDate = $dateArr[0];
    $endDate = $dateArr[1];
    if (!isset($_GET['net']) && $_GET['net'] != '1') {
        $startDate = strtotime($dateArr[0]);
        $endDate = strtotime($dateArr[1]);
    }
    if ($startDate == $endDate) {
        $endDate = $endDate + (24 * 60 * 60);
    }
    $startDate = $startDate * 1000;
    $endDate = $endDate * 1000;
    $reportName = 'All';
    $total = $_GET['total'];
    $starting = 0;

    if (isset($_GET['admin']) && $_GET['admin'] == 1) {
        $admin = 1;
    } else {
        $admin = 0;
    }
}

if (isset($_GET['query']) && $_GET['query'] != '') {
    $qryArr = explode('.', $_GET['query']);
    $qryStr = 'AND (';
    for ($i = 0; $i < count($qryArr); $i++) {
        $qryStr .= ' dlr:' . $qryArr[$i] . ' OR';
    }
    $qryStr = substr($qryStr, 0, -3) . ')';
} else {
    $_GET['query'] = '';
    $qryStr = '';
}

if (isset($_GET['searchTerm']) && $_GET['searchTerm'] != '') {
    $searchT = $_GET['searchTerm'];

    $searchTerm = ',{
                         "multi_match" : {
                            "query":    "' . $searchT . '",
                            "fields": ["smsc", "src", "dest", "content", "mccmnc"]
                          }   
                        }';
} else {
    $searchT = '';
    $searchTerm = '';
}


$chunk = 5000;
$nl = "\n";
$date = date("Y-m-d_H:i:s");
$fileN = "AllReport_" . $cl . "_" . $date;
$file = "../tempFiles/" . $fileN . ".csv";
$csvFile = fopen($file, "a+") or die("Unable to open file!");



//Ignore User Abort (the chunk loop will check for this)
ignore_user_abort(true);

//Time limit (3min)
set_time_limit(180);

//Download headers
//header('Content-type: text/plain');
header('Content-type: application/vnd.ms-excel');
header('Content-Disposition: attachment; filename="' . basename($file) . '"');
header('Expires: 0');
header('Cache-Control: must-revalidate');
header('Pragma: public');
//Enable implicit flush & gz compression
ob_end_clean();
ob_implicit_flush();
ob_start("ob_gzhandler");

//Open out stream
$php_out = fopen('php://output', 'w');
fputcsv($php_out, array('sep=,'), "\t");

//Write header
if ($admin == 1) {
    $columns = array('SMS_id', 'Date', 'From', 'To', 'Content', 'Network', 'SMSC', 'Status', 'RDNC');
} else {
    $columns = array('SMS_id', 'Date', 'From', 'To', 'Content', 'Network', 'Status');
}
fputcsv($php_out, $columns);

//Flush data
ob_flush();


//Output results
define('ELASTIC_CHUNK_SIZE', 10000);
for ($starting; $starting < $total; $starting += ELASTIC_CHUNK_SIZE) {
    getElastData($starting, $cl, $qryStr, $searchTerm, $startDate, $endDate, $admin, $php_out, ELASTIC_CHUNK_SIZE);

    if (connection_aborted()) {

        break;
    }
}

//Close  output stream, flush, exit
fclose($php_out);
ob_flush();
flush();
die();

function getElastData($starting, $cl, $qryStr, $searchTerm, $startDate, $endDate, $admin, $handle, $chunk_size) {

    $qry = <<<JSONGOESHERE
{

     "size": $chunk_size,
     "from": $starting,
      "fields" : ["mtsms_id","timestamp","src","dest","content","dlr","mccmnc","smsc","rdnc"],
      "query": {
        "filtered": {
          "query": {
            "bool" : {
                "must" : [
                    {
                     "query_string": {
                     "query": "$cl $qryStr",
                     "analyze_wildcard": true
                        }
                    }
                    $searchTerm
                ]
            }
          },
          "filter": {
                "bool": {
                  "must": [
                    {
                      "range": {
                        "timestamp": {
                          "gte": $startDate,
                          "lte": $endDate
                        }
                      }
                    }
                  ],
                  "must_not": []
                }
              }
        }
      }  
    }
JSONGOESHERE;

    $allStats = runRawMCoreElasticsearchQuery($qry);


    writeToFile($allStats, $admin, $handle);
}

function writeToFile($allStats, $admin, $handle) {

    foreach ($allStats as $key => $value) {
        if ($key == 'hits') {
            $hits = count($value['hits']);
            for ($i = 0; $i < $hits; $i++) {
                $netw = ($value['hits'][$i]['fields']['mccmnc']['0']);
                $smsc = $value['hits'][$i]['fields']['smsc']['0'];
                $posSmsc = strrpos($smsc, '_');
                $smsc = ucfirst(substr($smsc, 0, $posSmsc));
                $time = str_replace('T', ' ', $value['hits'][$i]['fields']['timestamp']['0']);
                $time = strstr($time, '+', true);
                $timeSec = strrpos($time, ':');
                $time = substr($time, 0, $timeSec);

                $dlrPos = $value['hits'][$i]['fields']['dlr']['0'];
                if (($dlrPos & 1) == 1) {
                    $dlrPos = 'Delivered';
                } elseif (($dlrPos & 16) == 16) {
                    $dlrPos = 'Rejected';
                } elseif (($dlrPos & 32) == 32 && ($dlrPos & 1) == 0 && ($dlrPos & 2) == 0) {
                    $dlrPos = 'Pending';
                } elseif (($dlrPos & 2) == 2) {
                    $dlrPos = 'Failed';
                }

                $rdnc = $value['hits'][$i]['fields']['rdnc']['0'];

                if ($admin == 1) {
                    $csvData = array(
                        $value['hits'][$i]['fields']['mtsms_id']['0'],
                        $time,
                        $value['hits'][$i]['fields']['src']['0'],
                        $value['hits'][$i]['fields']['dest']['0'],
                        preg_replace('/[^(\x20-\x7F)]*/', '', $value['hits'][$i]['fields']['content']['0']),
                        $value['hits'][$i]['fields']['mccmnc']['0'],
                        $smsc,
                        $dlrPos,
                        $rdnc
                    );
                } else {
                    $csvData = array(
                        $value['hits'][$i]['fields']['mtsms_id']['0'],
                        $time,
                        $value['hits'][$i]['fields']['src']['0'],
                        $value['hits'][$i]['fields']['dest']['0'],
                        preg_replace('/[^(\x20-\x7F)]*/', '', $value['hits'][$i]['fields']['content']['0']),
                        $value['hits'][$i]['fields']['mccmnc']['0'],
                        $dlrPos);
                }

                fputcsv($handle, $csvData);
            }
        }
    }

    ob_flush();
}
