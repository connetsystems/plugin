<?php

require_once('config/dbConn.php');
require_once('allInclusive.php');

if (isset($_POST['sId'])) {
   //require_once("allInclusive.php");
   //echo 'Test='.$_POST['action'];
   //include("allInclusive.php");
   $startDate = mktime(0, 0, 0);
   $startDateConv = date('Y-m-d', $startDate);

   $mosTotals = getMOForDashConnetReseller($_POST['accountId'], $startDateConv);

   //echo $startDateConv;
   //echo '1';
   echo $mosTotals;
   //echo number_format($sents, 0, '', ' ');
} else {
   echo '0';
}

function getMOForDashConnet($account_id, $sD)
{
   $total_mos = 0;

   //open the DB object
   $conn = getPDOReportingCore();

   $services_in_reseller = getServicesInAccount($account_id);

   foreach($services_in_reseller as $service)
   {
      //prepare the data
      $data = array(':sD' => $sD, ':sId' => $service['service_id']);

      //prepare the query
      $stmt = $conn->prepare("SELECT count(mosms_id) as total
                FROM core.core_mosms
                WHERE DATE(`timestamp`) = :sD
                AND service_id in (:sId)");

      //set the fetch mode
      $stmt->setFetchMode(PDO::FETCH_ASSOC);

      //execute it
      $stmt->execute($data);

      //run the query
      $row = $stmt->fetch();

      $total_mos += $row['total'];
   }

   return $total_mos;
}
