<?php

//include("allInclusive.php");
require_once('config/dbConn.php');

$conn = getPDOReportingCore();

if (isset($_POST["sdate"]) && isset($_POST["edate"])) {

   $jsonNetArr = "";

   $data = array(':start_date' => $_POST["sdate"], ':end_date' => $_POST["edate"]);

   $query = "SELECT YEAR(cdr_record_date) as 'Year',MONTHNAME(cdr_record_date) as 'month',prefix_country_name,SUM(cdr_record_units) AS cdr_record_units
                FROM
                (SELECT SUBSTRING_INDEX(SUBSTRING_INDEX(cdr_provider, '|', -2), '|', 1) AS `smsc`,SUBSTRING_INDEX(SUBSTRING_INDEX(cdr_provider, '|', -1), '|', 1) AS `mccmnc`,cdr.*
                FROM reporting.cdr WHERE cdr_provider LIKE 'core|mtsms|%' OR cdr_provider LIKE 'legacy|mtsms|%' OR cdr_provider LIKE 'rdnc|%'  ORDER BY cdr_id DESC) as cdr
                LEFT JOIN
                (SELECT prefix_mccmnc, prefix_network_name, prefix_country_name FROM core.core_prefix GROUP BY prefix_mccmnc) AS core_prefix
                ON prefix_mccmnc=mccmnc
                LEFT JOIN reporting.cdr_record_archive USING (cdr_id)
                WHERE date(cdr_record_date) >=:start_date and date(cdr_record_date) < :end_date
                GROUP BY YEAR(cdr_record_date),MONTHNAME(cdr_record_date),prefix_country_name
                ORDER BY cdr_record_units ASC";

   //prepare the query
   $stmt = $conn->prepare($query);

   //set fetch method 
   $stmt->setFetchMode(PDO::FETCH_ASSOC);

   //execute statement
   $stmt->execute($data);

   //fetch the data
   $countryData = $stmt->fetchAll();

   //set connection object to null
   $conn = null;

   foreach ($countryData as $key => $value) {
      $jsonNetArr .= $value['prefix_country_name'] . '+' . $value['cdr_record_units'] . '|';
   }

   echo $jsonNetArr;
}
?>