<?php
    session_start();
    require_once('allInclusive.php');

    if(isset($_POST['serviceId']))
    {
        $service_id = $_POST['serviceId'];
        $prevFund = getFundsOnly($service_id);

        if(isset($_POST['ammount']) && ($_POST['ammount'] != 0) && (isset($_POST['desc']) && $_POST['desc'] != ''))
        {
            $amount = ($_POST['ammount']*10000);
            if(isset($_POST['confir']) && $_POST['confir'] == 1)
            {
                createPayment($service_id, $_SESSION['userId'], $amount, $_POST['desc'], $prevFund);
                echo "DONE;";
                deleteNotificationData($service_id);
                if(isset($_SESSION['resellerId']) && $_SESSION['resellerId'] != '')
                {
                    createPayment($_SESSION['resellerId'], $_SESSION['userId'], (-1*$amount), $_POST['desc'], $prevFund);
                }
                $height = 72;
            }
        }

        header("Location: http://".$_SERVER['HTTP_HOST']."/pages/confirmcredits.php?success=1");
        die();
    }
    else
    {
        header("Location: http://".$_SERVER['HTTP_HOST']."/pages/confirmcredits.php?success=0");
        die();
    }