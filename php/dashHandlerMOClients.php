<?php

require_once('config/dbConn.php');

if (isset($_POST['sId'])) {
   //require_once("allInclusive.php");
   //echo 'Test='.$_POST['action'];
   //include("allInclusive.php");
   $startDate = mktime(0, 0, 0);
   $startDateConv = date('Y-m-d', $startDate);

   $mosTotals = getMOForDashConnet($_POST['sId'], $startDateConv);

   //echo $startDateConv;
   //echo '1';
   echo $mosTotals;
   //echo number_format($sents, 0, '', ' ');
} else {
   echo '0';
}

function getMOForDashConnet($sId, $sD) {
   //open the DB object
   $conn = getPDOReportingCore();

   //prepare the data
   $data = array(':sD' => $sD);

   //prepare the query
   $stmt = $conn->prepare("SELECT count(mosms_id) as total
                FROM core.core_mosms
                WHERE DATE(`timestamp`) = :sD
                AND service_id in (".$sId.")");

   //set the fetch mode
   $stmt->setFetchMode(PDO::FETCH_ASSOC);

   //execute it
   $stmt->execute($data);

   //run the query
   $row = $stmt->fetch();

   return $row['total'];
}
