<?php
require_once('config/dbConn.php');

if(isset($_POST['action']))
{
    //echo 'Test='.$_POST['action'];
      $startDate = mktime(0, 0, 0)*1000;
    $endDate = (time()*1000);

    /*$startDate = "1428616800000";
    $endDate = "1428703199999";*/

    $qry = '{
              "size": 0,
                  "aggs": {
                    "service_id": {
                      "terms": {
                        "field": "service_id",
                        "size": 0,
                        "order": {
                          "1": "asc"
                        }
                      },
                      "aggs": {
                        "1": {
                          "sum": {
                            "script": "doc[\'billing_units\'].value",
                            "lang": "expression"
                          }
                        },
                    
                        "dlr": {
                              "terms": {
                                "field": "dlr",
                                "size": 0,
                                "order": {
                                  "dlr_units": "asc"
                                }
                              },
                              "aggs": {
                                "dlr_units": {
                                  "sum": {
                                    "script": "doc[\'billing_units\'].value",
                                    "lang": "expression"
                                  }
                                }
                            }
                          }
                      
                  
                      }
                    
                  
                }
              },
              "query": {
                "filtered": {
                  "query": {
                    "query_string": {
                      "query": "(service_id:*)",
                      "analyze_wildcard": true
                    }
                  },
                  "filter": {
                    "bool": {
                      "must": [
                        {
                          "range": {
                            "timestamp": {
                                "gte": '.$startDate.',
                                "lte": '.$endDate.'
                            }
                          }
                        }
                      ],
                      "must_not": []
                    }
                  }
                }
              }
            }

    
        ';

    $allStats = runRawMCoreElasticsearchQuery($qry);

    //echo $output;
    if(isset($allStats))
    {
        $returnString = '';  
        foreach ($allStats as $key => $value) 
        {
            /*if($key == 'hits')
            {
                echo $value['total'];       
            }*/

                                        //&& isset($value['5']['buckets'])
            if($key == 'aggregations')
            {
                $bucks = count($value['service_id']['buckets']);
                for($a = 0; $a < $bucks; $a++)
                {
                    //ALL the service IDs -> $value['service_id']['buckets'][$a]['key'];
                    $returnString .= $value['service_id']['buckets'][$a]['key'].':';
                    $dlrSent = $value['service_id']['buckets'][$a]['doc_count'];
                    $dlr680 = 0;
                    $dlr681 = 0;
                    $dlr682 = 0;
                    $dlr4288 = 0;

                    $dlrBucks = count($value['service_id']['buckets'][$a]['dlr']['buckets']);
                    for($b = 0; $b < $dlrBucks; $b++)
                    {
                        //ALL the dlrs/service -> $value['service_id']['buckets'][$a]['dlr']['buckets'][$b]['key'];
                        $dlrPos = $value['service_id']['buckets'][$a]['dlr']['buckets'][$b]['key'];
                        if(($dlrPos & 32) == 32 && ($dlrPos & 1) == 0 && ($dlrPos & 2) == 0)
                        {
                            $dlr680 += $value['service_id']['buckets'][$a]['dlr']['buckets'][$b]['doc_count'];
                        }
                        if(($dlrPos & 1) == 1) 
                        {
                            $dlr681 += $value['service_id']['buckets'][$a]['dlr']['buckets'][$b]['doc_count'];
                        }
                        if(($dlrPos & 2) == 2) 
                        {
                            $dlr682 += $value['service_id']['buckets'][$a]['dlr']['buckets'][$b]['doc_count'];
                        }
                        if(($dlrPos & 16) == 16) 
                        {
                            $dlr4288 += $value['service_id']['buckets'][$a]['dlr']['buckets'][$b]['doc_count'];
                        }
                        //$returnString .= $value['service_id']['buckets'][$a]['key'];
                    }
                    $returnString .= $dlrSent.','.$dlr680.','.$dlr681.','.$dlr682.','.$dlr4288.';';
                }
                //$returnString = $returnString['key'];
                /*if(count($value['5']['buckets']) > 0)
                {

                }*/
            }
        }
        echo $returnString;
    }    
   /* if(isset($allStats))
    {
        $sents = 0;
        $units = 0;
        $dlrArr681 = array();
        $dlrArr680 = array();
        $dlrArr682 = array();
  //      $dlrArr4288 = array();
    //    $dlrArrREJ = array();
        foreach ($allStats as $key => $value) 
        {
            if($key = 'aggregations' && isset($value['5']['buckets']))
            {
                if(count($value['5']['buckets']) > 0)
                {
                    echo '<td>'.strstr($value['5']['buckets']['0']['key_as_string'], 'T', true).'</td>';
                    echo '<td>Traffic</td>';
                    $sents = $value['5']['buckets']['0']['doc_count'];
                    $units = $value['5']['buckets']['0']['1']['value'];
                    $dlrArr680['5']['buckets']['0']['dlr']['buckets'] = 0;
                    $dlrArr681['5']['buckets']['0']['dlr']['buckets'] = 0;
                    $dlrArr682['5']['buckets']['0']['dlr']['buckets'] = 0;
                    //$dlrArr4288['5']['buckets']['0']['dlr']['buckets'] = 0;
                    $dlrArrREJ['5']['buckets']['0']['dlr']['buckets'] = 0;

                    for($i=0; $i < count($value['5']['buckets']['0']['dlr']['buckets']); $i++) 
                    {
                        $dlrPos = (integer)$value['5']['buckets']['0']['dlr']['buckets'][$i]['key'];

                        if(($dlrPos & 32) == 32 && ($dlrPos & 1) == 0 && ($dlrPos & 2) == 0 && ($dlrPos & 16) == 0)
                        {
                            $countRdnc = count($value['5']['buckets']['0']['dlr']['buckets'][$i]);
                            for ($j=0; $j < $countRdnc; $j++) 
                            {
                                if($value['5']['buckets']['0']['dlr']['buckets'][$i] == 0) 
                                {
                                    //echo "<br>pending=".$dlrPos."- num=".$value['5']['buckets']['0']['dlr']['buckets'][$i]['doc_count'];
                                    if($dlrArr680['5']['buckets']['0']['dlr']['buckets'] == 0)
                                    {
                                        $dlrArr680['5']['buckets']['0']['dlr']['buckets'] = $value['5']['buckets']['0']['dlr']['buckets'][$i]['doc_count'];
                                        //$dlrArr680['5']['buckets']['0']['dlr']['buckets'] = $value['5']['buckets']['0']['dlr']['buckets'][$i]['dlr_units']['value'];
                                    }
                                    else
                                    {
                                        $dlrArr680['5']['buckets']['0']['dlr']['buckets'] += $value['5']['buckets']['0']['dlr']['buckets'][$i]['doc_count'];
                                        //$dlrArr680['5']['buckets']['0']['dlr']['buckets'] += $value['5']['buckets']['0']['dlr']['buckets'][$i]['dlr_units']['value'];
                                    }
                                }
                            }
                            $countRdnc = count($value['5']['buckets']['0']['dlr']['buckets'][$i]['rdnc']['buckets']);
                            for ($j=0; $j < $countRdnc; $j++) 
                            {
                                if($value['5']['buckets']['0']['dlr']['buckets'][$i]['rdnc']['buckets'][$j]['key'] == 0) 
                                {
                                    //echo "<br>pending=".$dlrPos."- num=".$value['5']['buckets']['0']['dlr']['buckets'][$i]['rdnc']['buckets'][$j]['doc_count'];
                                    if($dlrArr680['5']['buckets']['0']['dlr']['buckets'] == 0)
                                    {
                                        $dlrArr680['5']['buckets']['0']['dlr']['buckets'] = $value['5']['buckets']['0']['dlr']['buckets'][$i]['rdnc']['buckets'][$j]['doc_count'];
                                        //$dlrArr680['5']['buckets']['0']['dlr']['buckets'] = $value['5']['buckets']['0']['dlr']['buckets'][$i]['dlr_units']['value'];
                                    }
                                    else
                                    {
                                        $dlrArr680['5']['buckets']['0']['dlr']['buckets'] += $value['5']['buckets']['0']['dlr']['buckets'][$i]['rdnc']['buckets'][$j]['doc_count'];
                                        //$dlrArr680['5']['buckets']['0']['dlr']['buckets'] += $value['5']['buckets']['0']['dlr']['buckets'][$i]['dlr_units']['value'];
                                    }
                                }
                            }
                        }
                        if(($dlrPos & 1) == 1) 
                        {
                            if($dlrArr681['5']['buckets']['0']['dlr']['buckets'] == 0)
                            {
                                $dlrArr681['5']['buckets']['0']['dlr']['buckets'] = $value['5']['buckets']['0']['dlr']['buckets'][$i]['doc_count'];
                            }
                            else
                            {
                                $dlrArr681['5']['buckets']['0']['dlr']['buckets'] += $value['5']['buckets']['0']['dlr']['buckets'][$i]['doc_count'];
                            }
                        }
                        if(($dlrPos & 2) == 2) 
                        {
                            if($dlrArr682['5']['buckets']['0']['dlr']['buckets'] == 0)
                            {
                                $dlrArr682['5']['buckets']['0']['dlr']['buckets'] = $value['5']['buckets']['0']['dlr']['buckets'][$i]['doc_count'];
                            }
                            else
                            {
                                $dlrArr682['5']['buckets']['0']['dlr']['buckets'] += $value['5']['buckets']['0']['dlr']['buckets'][$i]['doc_count'];
                            }
                        }
                        if(($dlrPos & 16) == 16) 
                        {
                            if($dlrArrREJ['5']['buckets']['0']['dlr']['buckets'] == 0)
                            {
                                $dlrArrREJ['5']['buckets']['0']['dlr']['buckets'] = $value['5']['buckets']['0']['dlr']['buckets'][$i]['doc_count'];
                            }
                            else
                            {
                                $dlrArrREJ['5']['buckets']['0']['dlr']['buckets'] += $value['5']['buckets']['0']['dlr']['buckets'][$i]['doc_count'];
                            }
                        }
                    }
                }

                $sendCent = round(($sents/$sents*100),2);

                $pendings = $dlrArr680['5']['buckets']['0']['dlr']['buckets'];
                $pendCent = round(($pendings/$sents*100),2);

                $delivered = $dlrArr681['5']['buckets']['0']['dlr']['buckets'];
                $deliCent = round(($delivered/$sents*100),2);
                
                $failed = $dlrArr682['5']['buckets']['0']['dlr']['buckets'];
                $failCent = round(($failed/$sents*100),2);

                $rejected = $dlrArrREJ['5']['buckets']['0']['dlr']['buckets'];
                $rejeCent = round(($rejected/$sents*100),2);
            }
        }
    }
    $totals = $units.','.$pendings.','.$delivered.','.$failed.','.$rejected.','.$sendCent.','.$pendCent.','.$deliCent.','.$failCent.','.$rejeCent;
    echo $totals;*/
    //echo number_format($sents, 0, '', ' ');
}

?>