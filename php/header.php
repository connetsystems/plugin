<?php
/* * ****************************************************************
 * THIS IS THE OLD HEADER FILE, THIS IS DEPRECATED AND
 * MUST BE REPLACED WITH "pages/template_header.php" ON ALL PAGES
 * **************************************************************** */

LoginHelper::assertHttps();

//check if the user is logged in, if not, redirect back to login
if (!isset($_SESSION['userId'])) {
   header('Location: /index.php');
}

//setting some display variables according to the users account customization
$accRGB = $_SESSION['accountColour'];
$tabLogo = $_SESSION['tabLogo'];
$headerLogo = $_SESSION['headerLogo'];
$pathLogout = "/index.php?logged=out";
?>
<!DOCTYPE html>
<head>
   <link rel="icon" type="image/png" href="<?php echo $tabLogo; ?>">
</head>

<header class="header">

   <!--link rel="shortcut icon" href="../img/windowIcons/Small.png"-->


   <a style="background-image: url('<?php echo $headerLogo; ?>'); background-color:<?php echo $accRGB; ?>; background-repeat: no-repeat; background-position: center center;" href="<?php echo $_SERVER['SERVER_NAME'] . '/'; ?>home.php" class="logo">
      <!-- Add the class icon to your logo image or logo icon to add the margining -->   
   </a>

   <nav class="navbar navbar-static-top" role="navigation" style="background-color:<?php echo $accRGB; ?> ">
      <!--a href="#" class="navbar-btn sidebar-toggle" data-toggle="offcanvas" role="button">
          <span class="sr-only">Toggle navigation</span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>

          style="font-size:20px; text-align:center;"
      </a-->
      <div class="navbar-left" >
         <ul class="nav navbar-nav" data-toggle="offcanvas">

            <li class="active">
               <a href="#" class="navbar-link">
                  <i class="fa fa-bars"></i>
                  Menu
               </a>
            </li>

         </ul>
      </div>

      <div class="navbar-right">
         <ul class="nav navbar-nav">

            <?php
            if (isset($_SESSION['settings'])) {
               echo '<li>';
               echo '<a href="../pages/settings.php" class="navbar-link">';
               echo '<i class="fa fa-cog"></i>';
               echo 'Settings';
               echo '</a>';
               echo '</li>';
            }
            ?>
            <li>
               <a href="<?php echo $pathLogout; ?>">
                  <i class="glyphicon glyphicon-user"></i>
                  Logout
               </a>
            </li>

         </ul>
      </div>
   </nav>
</header>
