<?php
    require_once('config/dbConn.php');
    /*if(isset($_SESSION['resellerId']) && $_SESSION['resellerId'] != '')
    {
        if($_SESSION['serviceId'] == $_SESSION['resellerId'])
        {
            //this is a reseller;
            $serviceVar = ' AND ('.getAllServicesInReseller($_SESSION['serviceId']).')';
            //echo "<br><br><br>==========================================================".$serviceVar;

        }
        else
        {
            //this is a client
            $serviceVar = ' AND service_id:'.$_SESSION['serviceId'];
            //echo "<br><br><br>==========================================================".$serviceVar;
        }
    }
    else
    {
        $serviceVar = ''; 
        //this is a connet user
    }*/

    if(isset($_GET['serv']) && $_GET['serv'] != '')
    {
        $serviceVar = $_GET['serv'];
    }

    if(isset($_GET['date']) && $_GET['date'] != '')
    {
        $dateArr = explode(' - ', $_GET['date']);
        $startDate = $dateArr[0];
        $startDateConv = $dateArr[0];
        $endDate = $dateArr[1];
        $endDateConv = $dateArr[1];
        $date = $_GET['date'];
    }
    else
    {
        $startDate = (strtotime('01 January 2015'))*1000;
        $endDate = (time()*1000);
        $startDateConv = date('Y-m-d', ($startDate/1000));
        $endDateConv = date('Y-m-d', ($endDate/1000));
        $date = $startDateConv.' - '.$endDateConv;
    }

    if(isset($_GET['dest']) && $_GET['dest'] != '')
    {
        $dest2 = $_GET['dest'];
        $dests = explode(',', $dest2);
        $dest = ' AND (';
        for($i = 0; $i < count($dests); $i++) 
        { 
            $dest .= 'src:'.$dests[$i].' OR ';
        }
        $dest = substr($dest, 0, -4);
        $dest .= ')';
    }
    else
    {
        $dest = "";
        $dest2 = "";
    }

    if(isset($_GET['cont']) && $_GET['cont'] != '')
    {
        $cont = ' AND content:'.$_GET['cont'];
        $cont2 = $_GET['cont'];
    }
    else
    {
        $cont = "";
        $cont2 = "";
    }

    $query = $dest.$cont.$serviceVar;
    if(substr($query, 0, 5) == ' AND ')
    {
        $query = substr($query, 5);
    }

    if($query == '')
    {
        $query = '*';
    }

    $fileNa = 'Search_Replies_Report';

    $reportName = 'All';

    $starting = 0;
    $total = $_GET['total'];

    if(isset($_GET['admin']) && $_GET['admin'] == 1)
    {
        $admin = 1;
    }
    else
    {
        $admin = 0;
    }
    

    $dateR = date("Y-m-d_H:i:s");
    $fileN = $fileNa."_".$dateR;
    $file = "../tempFiles/".$fileN.".csv";

    set_time_limit(180);

    //Download headers
    //header('Content-type: text/plain');
    header('Content-type: application/vnd.ms-excel');
    header('Content-Disposition: attachment; filename="' . basename($file)) . '"';
    header('Expires: 0');
    header('Cache-Control: must-revalidate');
    header('Pragma: public');
    //Enable implicit flush & gz compression
    ob_end_clean();
    ob_implicit_flush();
    ob_start("ob_gzhandler");
    //Open out stream
    $php_out = fopen('php://output', 'w');
    fputcsv($php_out, array('sep=,'), "\t");
    
    //Write header
    if ($admin == 1) 
    {
        $columns = array('SMS_id', 'Date', 'SMSC', 'Service', 'From', 'Content', 'MT_id');
    }
    else
    {
        $columns = array('SMS_id', 'Date', 'Service', 'From', 'Content', 'MT_id');
    }
    fputcsv($php_out, $columns);
    //Flush data
    ob_flush();
    //Output results
    define('ELASTIC_CHUNK_SIZE', 1000);
    for ($starting; $starting < $total; $starting += ELASTIC_CHUNK_SIZE) 
    {
        //fputcsv($php_out, 'Start');
        getElastData($starting, $query, $startDateConv, $endDateConv, $admin, $php_out, ELASTIC_CHUNK_SIZE);
    }
    //Close  output stream, flush, exit
    fclose($php_out);
    ob_flush();
    flush();
    die();

    function getElastData($starting, $query, $startDateConv, $endDateConv, $admin, $handle, $chunk_size)
    {

        $qry = <<<JSONGOESHERE
{
                  "size":$chunk_size,
                  "from":$starting,
                  "sort": {"mosms_id":"desc"},
                  "fields" : ["service_id","meta.mtsms_id","mosms_id","timestamp","src","dest","content","meta.mccmnc"],
                  "query": {
                    "filtered": {
                      "query": {
                        "bool" : {
                            "must" : [
                                {
                                 "query_string": {
                                 "query": "$query",
                                 "analyze_wildcard": true
                                    }
                                },
                                {
                                  "range": {
                                    "timestamp": {
                                      "gte": "$startDateConv",
                                      "lte": "$endDateConv"
                                    }
                                  }
                                }
                            ]
                        }
                      }
                    }
                  }  
                }

JSONGOESHERE;

        $allStats = runRawMOSMSElasticsearchQuery($qry);

        writeToFile($allStats, $admin, $handle);
    }

    //echo $qry.$nl;
    function writeToFile($allStats, $admin, $handle)
    {
        foreach ($allStats as $key => $value) 
        {
            if($key == 'hits')
            {
                $hits = count($value['hits']);
                for($i=0; $i < $hits; $i++) 
                {
                    $time = str_replace('T', ' ', $value['hits'][$i]['fields']['timestamp']['0']);
                    $time = strstr($time, '+', true);
                    $timeSec = strrpos($time, ':');
                    $time = substr($time, 0, $timeSec);
                    if(isset($value['hits'][$i]['fields']['meta.mtsms_id']['0']))
                    {
                        $mtsms_id = $value['hits'][$i]['fields']['meta.mtsms_id']['0'];
                    }
                    else
                    {
                        $mtsms_id = '';
                    }

                    if($admin == 1)
                    {
                        //WTF WHY IS THIS ALL COMMENTED
                        $csvData = array(
                            /*$value['hits'][$i]['fields']['mosms_id']['0'],
                            $time,
                            $value['hits'][$i]['fields']['meta.mccmnc']['0'],
                            $smsc,
                            $value['hits'][$i]['fields']['service_id']['0'],
                            $value['hits'][$i]['fields']['src']['0'],
                            $value['hits'][$i]['fields']['dest']['0'],*/
                            //preg_replace('/[^(\x20-\x7F)]*/','', $value['hits'][$i]['fields']['content']['0'])*/
                        );
                    }
                    else
                    {
                        $csvData = array(
                            $value['hits'][$i]['fields']['mosms_id']['0'],
                            $time,
                            //$value['hits'][$i]['fields']['meta.mccmnc']['0'],
                            getServiceName($value['hits'][$i]['fields']['service_id']['0']).' ('.$value['hits'][$i]['fields']['service_id']['0'].')',
                            $value['hits'][$i]['fields']['src']['0'],
                            //$value['hits'][$i]['fields']['dest']['0'],
                            preg_replace('/[^(\x20-\x7F)]*/','', $value['hits'][$i]['fields']['content']['0']),
                            $mtsms_id
                            
                        );
                    }
                    fputcsv($handle, $csvData);
                }
            }
        }
        ob_flush();
    }

    /*for($starting; $starting < $total; $starting += 5000)
    {
        getElastData($starting, $cl, $qryStr, $searchTerm, $startDate, $endDate, $admin, $csvFile, $nl, $file);
        echo '<br>'.$starting;
        flush();
    }

    header('Content-Type: application/csv');
    header('Content-Disposition: attachment; filename='.basename($file).'.csv');
    header("Cache-Control: no-cache, no-store, must-revalidate"); // HTTP 1.1
    header("Pragma: no-cache");
    header("Expires: 30000");
    readfile("../tempFiles/".$fileN.".csv");*/

?>
