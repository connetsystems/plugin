<?php

define('ERROR_REPORTING', true);
require_once('ConnetAutoloader.php');
preset::plain();

//ini_set('memory_limit', '8192M');
set_time_limit(3000);

if(isset($_POST['fName']) && $_POST['fName'] != '')
{
	if(isset($_POST['dedup']) && $_POST['dedup'] != '')
	{
		$deDups = $_POST['dedup'];	
	}
	else
	{
		$deDups = 0;	
	}
	$filename = dirname( dirname(__FILE__) ).'/'.substr($_POST['fName'], 3);
	$filters = array('duplicate' => $deDups, 'unknown' => 1, 'invalid' => 1);

	//clog('Filename:',$filename);
	if (false === ($info = helpers\MnpBulkLookup::process($filename, $filters))) 
	{
		clog::error("An error");
		die();
	}

	//clog($info);
	echo json_encode($info, JSON_FORCE_OBJECT);

	if ($info['status'] == 'COMPLETED') 
	{
		$new_filename = $filename . '.downloaded';
		if (false === (helpers\MnpBulkLookup::fetch($filename, $new_filename))) 
		{
			clog::error("An error");
			die();
		}
		//clog::info("Fetched file:",$new_filename,"Exists:",(file_exists($filename) ? 'Yes': 'No'));
	} 
	else 
	{
		//clog::info('Not Completed. Status:',$info['status']);
	}

	//clog::info('* Exit');
	die();
}
else
{
	echo 'ERROR';
}

//********************** end of file ******************************
?>
