<?php
    session_start();

    //check if the user is logged in, if not, redirect back to login
    if (!isset($_SESSION['userId'])) {
        //nope nope nope nope nope
        header('Location: /index.php');
        die();
    }

    require_once("allInclusive.php");
    require_once("../PluginAPI/PluginAPIAutoload.php");

    //get the data for the csv file
    if(isset($_GET['service_id']) && isset($_GET['start_date']) && isset($_GET['end_date']))
    {
        $service_id = $_GET['service_id'];
        $start_date = $_GET['start_date'];
        $end_date = $_GET['end_date'];

        //get the service name for the file name
        if(isset($_GET['service_name']) && $_GET['service_name'] != "")
        {
            $service_name = $_GET['service_name'];
        }
        else
        {
            $service_name =  getServiceName($service_id);
        }
        $service_name = preg_replace("/[^a-zA-Z0-9\-\_]/", ' ', $service_name);

        $batch_list = getBatchList($service_id, $start_date, $end_date);

        $sorted_batch_data = array();

        //we're gonna use CURL mutli to simulate the MO processing in parralell
        $curl_multi_data = array(array(),array());
        $count = 0;

        foreach ($batch_list as $key => $value)
        {
            $sorted_batch_data[$value['batch_id']]['DATE'] = $value['batch_created'];
            $sorted_batch_data[$value['batch_id']]['CLIENT'] = $value['client_name'];
            $sorted_batch_data[$value['batch_id']]['CAMPAIGN'] = $value['campaign_name'];
            $sorted_batch_data[$value['batch_id']]['CODE'] = $value['campaign_code'];
            $sorted_batch_data[$value['batch_id']]['REF'] = $value['batch_reference'];
            $sorted_batch_data[$value['batch_id']]['STATUS'] = $value['batch_submit_status'];
            $sorted_batch_data[$value['batch_id']]['TOTAL'] = $value['batch_mtsms_total'];

            $eStartDate = strtotime($value['batch_created']) * 1000;
            $eEndDate = (strtotime($value['batch_created'] . ' + 1 day') - 1) * 1000;

            //get all delivery stats from elasticsearch
            $batch_stats = Batch::getFullSendStats($value['batch_id']);

            $sorted_batch_data[$value['batch_id']]['sent'] = $batch_stats['sent'] + $batch_stats['rejected'];
            $sorted_batch_data[$value['batch_id']]['mosms'] = 0;
            $sorted_batch_data[$value['batch_id']]['units'] = $batch_stats['units'] + $batch_stats['rejected_units'];
            $sorted_batch_data[$value['batch_id']]['pending'] = $batch_stats['pending'];
            $sorted_batch_data[$value['batch_id']]['pending_units'] = $batch_stats['pending_units'];
            $sorted_batch_data[$value['batch_id']]['delivered'] = $batch_stats['delivered'];
            $sorted_batch_data[$value['batch_id']]['delivered_units'] = $batch_stats['delivered_units'];
            $sorted_batch_data[$value['batch_id']]['failed'] = $batch_stats['failed'] + $batch_stats['rejected'];
            $sorted_batch_data[$value['batch_id']]['failed_units'] = $batch_stats['failed_units'] + $batch_stats['rejected_units'];
            $sorted_batch_data[$value['batch_id']]['rejected'] = 0;
            $sorted_batch_data[$value['batch_id']]['blacklisted'] = 0;

            $curl_multi_data[$count]['url']  = "https://".$_SERVER['SERVER_NAME'].'/php/ajaxGetBatchMOTotal.php';
            $curl_multi_data[$count]['post'] = array();
            $curl_multi_data[$count]['post']['batch_id'] = $value['batch_id'];
            $count ++;
        }

        //dont pull MO counts if there is no batch list to pull
        if(isset($curl_multi_data[0]) && count($curl_multi_data[0]) > 0)
        {
            //run the MO query through the CURL multi to speed up getting stats for all the batches by doing parallel processing
            $mo_results = helperCurlMulti::multiRequest($curl_multi_data);

            //cycle through all the batch mos and set the value
            foreach($mo_results as $mo_result)
            {
                //convert JSON object to Array because that is how the script is responding
                $json_mo_result = json_decode($mo_result, true);
                if(isset($json_mo_result['success']) && $json_mo_result['success'] === true)
                {
                    $sorted_batch_data[$json_mo_result['batch_id']]['mosms'] = $json_mo_result['mo_total'];
                }
            }
        }


        $filename = 'Batch List ('.$service_name.') ['.$start_date.' - '.$end_date.'].csv';

        header('Content-Type: application/csv');
        header('Content-Disposition: attachment; filename="'.$filename.'";');

        $output = fopen('php://output', 'w');
        fputcsv($output, array('sep=,'), "\t");

        $columns = array("Batch Id", "Date", "Client", "Campaign", "Code", "Reference", "Submitted", "Sent",
            "Units", "Pending", "Pending Units", "Delivered", "Delivered Units", "Failed", "Failed Units", "Replies", "Status");
        fputcsv($output, $columns, ',');

        foreach($sorted_batch_data as $batch_id => $batch_data)
        {
            $line = array();
            //output the data for CSV reporter
            array_push($line, $batch_id);
            array_push($line, $batch_data['DATE']);
            array_push($line, $batch_data['CLIENT']);
            array_push($line, $batch_data['CAMPAIGN']);
            array_push($line, $batch_data['CODE']);
            array_push($line, $batch_data['REF']);
            array_push($line, number_format($batch_data['TOTAL'], 0, ',', ' '));
            array_push($line, number_format($batch_data['sent'], 0, ',', ' '));
            array_push($line, number_format($batch_data['units'], 0, ',', ' '));
            array_push($line, number_format($batch_data['pending'], 0, ',', ' '));
            array_push($line, number_format($batch_data['pending_units'], 0, ',', ' '));
            array_push($line, number_format($batch_data['delivered'], 0, ',', ' '));
            array_push($line, number_format($batch_data['delivered_units'], 0, ',', ' '));
            array_push($line, number_format($batch_data['failed'], 0, ',', ' '));
            array_push($line, number_format($batch_data['failed_units'], 0, ',', ' '));
            //array_push($line, number_format($batch_data['rejected'], 0, ',', ' '));
            //array_push($line, number_format($batch_data['blacklisted'], 0, ',', ' '));
            array_push($line, number_format($batch_data['mosms'], 0, ',', ' '));
            array_push($line, $batch_data['STATUS']);

            fputcsv($output, $line, ',');
        }
    }
    else {
        echo "Invalid variables.";
    }


