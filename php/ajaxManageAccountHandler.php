<?php
    /**
     * This script is called by the manage accounts page, and runs certain ajax based tasks
     * @author - Doug Jenkinson
     */
    require_once("allInclusive.php");
    require_once("../PluginAPI/PluginAPIAutoload.php");

    try
    {
        session_start();

        //check if the user is logged in, if not, redirect back to login
        if(!isset($_SESSION['userId']))
        {
            $result = array('success' => false, 'reason' => 'session_expired');
            echo json_encode($result);
            die();
        }

        if(isset($_POST['task']))
        {
            $account_helper = new sqlHelperAccounts();

            $task = $_POST['task'];
            $result['success'] = false;
            switch($task)
            {
                case "ajax_image_upload" :
                    //get the relevant values
                    $account_id = $_POST['accountId'];
                    $field_name = $_POST['fieldName'];
                    $image_file_upload_obj  = $_FILES["imageFile"];

                    if(!empty($image_file_upload_obj["tmp_name"]))
                    {
                        $account_obj = new Account($account_id);
                        $result['success'] = $account_obj->moveAndSaveLogo($field_name, $image_file_upload_obj);
                    }
                    else
                    {
                        $result['success'] = false;
                        $result['reason'] = "No image file.";
                    }
                    break;
                case "save_account_field_value" :
                    //get the relevant values
                    $account_id = $_POST['accountId'];
                    $field_name = $_POST['fieldName'];
                    $field_value = $_POST['fieldValue'];

                    $result['success'] = $account_helper->saveAccountFieldValue($account_id, $field_name, $field_value);
                    break;
            }
        }
        else
        {
            $result = array('success' => false);
        }
    }
    catch(Exception $e)
    {
        $result = array('success' => false, 'message' => $e->getMessage());
    }

    //json encode the array before returning so it can be parsed by the JS
    echo json_encode($result);

