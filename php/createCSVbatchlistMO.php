<?php
session_start();

//check if the user is logged in, if not, redirect back to login
if (!isset($_SESSION['userId'])) {
   //nope nope nope nope nope
   header('Location: /index.php');
   die();
}

require_once("allInclusive.php");
require_once("../PluginAPI/PluginAPIAutoload.php");

//get the data for the csv file
if(isset($_GET['service_id']) && isset($_GET['batch_id']))
{
   $service_id = $_GET['service_id'];
   $batch_id = $_GET['batch_id'];

   //get the service name for the file name
   if(isset($_GET['service_name']) && $_GET['service_name'] != "")
   {
      $service_name = $_GET['service_name'];
   }
   else
   {
      $service_name =  getServiceName($service_id);
   }

   $service_name = preg_replace("/[^a-zA-Z0-9\-\_]/", ' ', $service_name);

   $batch = getBatchData($batch_id);
   $campaign_name = $batch['campaign_name'];
   $filename = 'BATCHLIST MOSMS (Campaign - '.$campaign_name.') ('.$service_name.').csv';

   header('Content-Type: application/csv');
   header('Content-Disposition: attachment; filename="'.$filename.'";');

   $output = fopen('php://output', 'w');
   //fputcsv($output, array('sep=,'), "\t");

   $columns = array("MOSMS ID", "Timestamp", "MSISDN", "Content");
   fputcsv($output, $columns, ',');

   //get the MOS and write them out
   $mo_array = MOSMS::getAllByBatchId($batch_id);

   //cycle through this set of data nad write it to file
   foreach($mo_array as $mo_data)
   {
      $line = array();
      //output the data for CSV reporter
      array_push($line, $mo_data->mosms_id);
      array_push($line, str_replace('T', ' ', strstr($mo_data->timestamp, '+', true)));
      array_push($line, $mo_data->src);
      array_push($line, helperSMSGeneral::cleanStringForCSV($mo_data->content));

      fputcsv($output, $line, ',');
   }

   //close output stream
   fclose($output);

}
else {
   echo "Invalid variables.";
}


