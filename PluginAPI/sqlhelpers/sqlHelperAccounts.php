<?php

class sqlHelperAccounts
{
    const SQL_TABLE_CORE_ACCOUNT = "core_account";
    const SQL_TABLE_CORE_ACCOUNT_SETTINGS = "core_account_settings";
    const SQL_TABLE_CORE_USER = "core_user";
    const SQL_TABLE_CORE_SERVICE = "core_service";

    private $table_fields_core_account = array('account_id' => 1,
        'account_name' => 1,
        'account_status' => 1,
        'account_created' => 1,
        'parent_account_id' => 1);

    private $table_fields_core_account_settings = array('account_settings_id' => 1,
        'account_id' => 1,
        'url_prefix' => 1,
        'url_suffix' => 1,
        'header_logo' => 1,
        'login_logo' => 1,
        'tab_logo' => 1,
        'account_colour' => 1,
        'email_header' => 1,
        'email_footer' => 1,
        'email_from' => 1);

    private $conn;


    public function __construct()
    {
        $this->conn = dbAdapter::getPDOMasterCore();
    }

    public function killDbConnection()
    {
        $this->conn = null;
    }

    /**
     * This function returns all account data from the core_account table
     * @param int $account_id - The ID of the account
     * @return Array $account - An associative array of the account record
     */
    function getAccount($account_id)
    {
        //prepare the data
        $data = array(':account_id' => $account_id);

        //prepare the query
        $stmt = $this->conn->prepare('SELECT *
                            FROM '.self::SQL_TABLE_CORE_ACCOUNT.'
                            WHERE account_id = :account_id ');

        //set the fetch mode
        $stmt->setFetchMode(PDO::FETCH_ASSOC);

        //execute it
        $stmt->execute($data);

        //fetch the data
        $account = $stmt->fetch();

        return $account;
    }

    /**
     * This function just returns the account name for a particular ID
     * @param int $account_id - The ID of the account
     * @return String $account_name
     */
    function getAccountName($account_id)
    {
        //prepare the data
        $data = array(':account_id' => $account_id);

        //prepare the query
        $stmt = $this->conn->prepare('SELECT account_name
                            FROM '.self::SQL_TABLE_CORE_ACCOUNT.'
                            WHERE account_id = :account_id ');

        //set the fetch mode
        $stmt->setFetchMode(PDO::FETCH_ASSOC);

        //execute it
        $stmt->execute($data);

        //fetch the data
        $account = $stmt->fetch();

        return $account['account_name'];
    }

    /**
     * This function pulls settings and styling information
     * @param int $account_id - The ID of the account
     * @return String $account_name
     */
    function getAccountSettings($account_id)
    {
        //prepare the data
        $data = array(':account_id' => $account_id);

        //prepare the query
        $stmt = $this->conn->prepare('SELECT *
                            FROM '.self::SQL_TABLE_CORE_ACCOUNT_SETTINGS.'
                            WHERE account_id = :account_id ');

        //set the fetch mode
        $stmt->setFetchMode(PDO::FETCH_ASSOC);

        //execute it
        $stmt->execute($data);

        //fetch the data
        $account_settings = $stmt->fetch();

        return $account_settings;
    }

    /**
     * Collects all service IDs and service names for this account
     * @param $account_id
     *
     * @return Array $service_list
     */
    function getAccoutServices($account_id)
    {
        //prepare the data
        $data = array(':account_id' => $account_id);

        //prepare the query
        $stmt = $this->conn->prepare('SELECT service_id, service_name
                            FROM '.self::SQL_TABLE_CORE_SERVICE.'
                            WHERE account_id = :account_id');

        //set the fetch mode
        $stmt->setFetchMode(PDO::FETCH_ASSOC);

        //execute it
        $stmt->execute($data);

        //fetch the data
        $service_list = $stmt->fetchAll();

        return $service_list;
    }

    /**
     * Collects all user IDs and user names directly associated with this account
     * @param $account_id
     *
     * @return Array $user_list
     */
    function getAccountUsers($account_id)
    {
        //prepare the data
        $data = array(':account_id' => $account_id);

        //prepare the query
        $stmt = $this->conn->prepare('SELECT user_id, user_username
                            FROM '.self::SQL_TABLE_CORE_USER.'
                            WHERE user_account_id = :account_id');

        //set the fetch mode
        $stmt->setFetchMode(PDO::FETCH_ASSOC);

        //execute it
        $stmt->execute($data);

        //fetch the data
        $user_list = $stmt->fetchAll();

        return $user_list;
    }

    /**
     * Pulls all accounts for the list accounts page
     * @param $page
     * @param $limit
     * @return array
     */
    public function getAllAccountsAdmin($page, $limit)
    {
        //determine the limit and offset from the pager
        if (!isset($page)) {
            $limit = 25;
            $page = 1;
        }
        $offset = $limit * ($page - 1);

        //prepare the query
        $stmt = $this->conn->prepare('SELECT a.account_id, a.account_name, s.header_logo
                    FROM '.self::SQL_TABLE_CORE_ACCOUNT.' a
                    LEFT JOIN core.core_account_settings s USING (account_id)
                    GROUP BY a.account_name
                    ORDER BY a.account_name ASC LIMIT :offset, :limit');

        $stmt->bindValue(':limit', (int) $limit, PDO::PARAM_INT);
        $stmt->bindValue(':offset', (int) $offset, PDO::PARAM_INT);

        //set the fetch mode
        $stmt->setFetchMode(PDO::FETCH_ASSOC);

        //execute it
        $stmt->execute();

        //fetch the data
        $accounts = $stmt->fetchAll();

        return $accounts;
    }

    /**
     * Pulls all accounts for the list accounts page
     * @param $page
     * @param $limit
     * @param $search_type
     * @param $search_term
     * @return array
     */
    public function getAllAccountsAdminSearch($page, $limit, $search_type, $search_term)
    {
        //determine the limit and offset from the pager
        if (!isset($page)) {
            $limit = 25;
            $page = 1;
        }
        $offset = $limit * ($page - 1);

        $where_clause = "";
        if($search_type == "" || $search_type == "all")
        {
            $where_clause = ' a.account_id LIKE \'%' . $search_term . '%\' OR a.account_name LIKE \'%' . $search_term . '%\' ';
        }
        else if($search_type == "account_id")
        {
            $where_clause = ' a.account_id = ' . $search_term . ' ';
        }
        else if($search_type == "account_name")
        {
            $where_clause = ' a.account_name LIKE \'%' . $search_term . '%\' ';
        }

        //prepare the query
        $stmt = $this->conn->prepare('SELECT a.account_id, a.account_name, s.header_logo
                    FROM '.self::SQL_TABLE_CORE_ACCOUNT.' a
                    LEFT JOIN core.core_account_settings s USING (account_id)
                    WHERE '.$where_clause.'
                    GROUP BY a.account_name
                    ORDER BY account_name ASC LIMIT :offset, :limit');

        $stmt->bindValue(':limit', (int) $limit, PDO::PARAM_INT);
        $stmt->bindValue(':offset', (int) $offset, PDO::PARAM_INT);


        //set the fetch mode
        $stmt->setFetchMode(PDO::FETCH_ASSOC);

        //execute it
        $stmt->execute();

        //fetch the data
        $accounts = $stmt->fetchAll();

        return $accounts;
    }

    /**
     * This basic function saves a value to a particular column in the accounts tables
     * @param $account_id
     * @param $field_name
     * @param $field_value
     * @return bool success
     */
    public function saveAccountFieldValue($account_id, $field_name, $field_value = null)
    {
        $table_name = $this->getAccountFieldTable($field_name);

        //prepare the query
        $stmt = $this->conn->prepare("UPDATE ".$table_name." SET ".$field_name." = :field_value WHERE account_id = :account_id");

        $stmt->bindValue(':account_id', $account_id, PDO::PARAM_INT);
        if($field_value === null)
        {
            $stmt->bindValue(':field_value', null, PDO::PARAM_INT);
        }
        else
        {
            $stmt->bindValue(':field_value', $field_value);
        }

        //execute it
        return $stmt->execute();
    }

    /**
     * Send a column name to this function to determine which table owns the column name in the context of an account
     * @param $field_name - The name of the field or column for which we want to know the owning table.
     * @return string - The Name of the table that has the column.
     */
    public function getAccountFieldTable($field_name)
    {
        $table_name= "";
        if(array_key_exists($field_name, $this->table_fields_core_account))
        {
            $table_name = self::SQL_TABLE_CORE_ACCOUNT;
        }
        else if(array_key_exists($field_name, $this->table_fields_core_account_settings))
        {
            $table_name = self::SQL_TABLE_CORE_ACCOUNT_SETTINGS;
        }

        return $table_name;
    }

    /**
     * Create a basic record for an account
     * @param $account_name
     * @return string
     */
    function createNewAccount($account_name) {

        //prepare the data
        $data = array(':account_name' => $account_name);

        //prepare the query
        $stmt = $this->conn->prepare("INSERT INTO core.core_account (account_name, account_status)
                VALUES (:account_name, 'ENABLED') ");

        //execute it
        $stmt->execute($data);

        //return the insert ID
        return $this->conn->lastInsertId('account_id');
    }

    /**
     * Check if an account name exists
     * @param $account_name
     * @return string
     */
    function isAccountNameUnique($account_name)
    {
        //prepare the data
        $data = array(':account_name' => $account_name);

        //prepare the query
        $stmt = $this->conn->prepare("SELECT count(*) FROM core.core_account
                WHERE account_name LIKE :account_name ");

        //execute it
        $stmt->execute($data);

        //fetch the data
        $number_of_rows = $stmt->fetchColumn();

        if ($number_of_rows == 0)
        {
            return true;
        }
        else
        {
            return false;
        }
    }

    /**
     * Generates a default account settings record for accounts
     * @param $account_id
     * @return bool
     */
    function createDefaultAccountSettings($account_id)
    {
        //prepare the data
        $data = array(':account_id' => $account_id);

        //prepare the query
        $stmt = $this->conn->prepare("INSERT INTO core.core_account_settings (account_id, header_logo, login_logo, tab_logo, account_colour, email_header, email_footer, email_from)
                VALUES (:account_id, '/img/skinlogos/1/1.png', '/img/skinlogos/1/1_header.png', '/img/skinlogos/1/1_icon.png', '#3c5d6d',
                'http://portal.connet-systems.com/img/Header.jpg', 'http://portal.connet-systems.com/img/Footer.jpg', 'support@connet-systems.com') ");

        return $stmt->execute($data);
    }


}