<?php

class sqlHelperRoutes
{
    const SQL_TABLE_CORE_ROUTES = "core_route";
    private $conn;


    public function __construct()
    {
        $this->conn = dbAdapter::getPDOMasterCore();
    }

    public function killDbConnection()
    {
        $this->conn = null;
    }

    /**
     * Gets the default routing data for a service
     *
     * @param $route_id - THe ID of the route
     * @return array - an object containing all routes for this service
     */
    function getRouteData($route_id)
    {
        //prepare the data
        $data = array(':route_id' => $route_id);

        //prepare the query
        $stmt = $this->conn->prepare("SELECT *
            FROM core.".self::SQL_TABLE_CORE_ROUTES."
            WHERE route_id = :route_id");

        //set the fetch mode
        $stmt->setFetchMode(PDO::FETCH_ASSOC);

        //execute it
        $stmt->execute($data);

        //fetch the data
        $route = $stmt->fetch();

        return $route;
    }

    /**
     * Collects all route countries uniquely
     *
     * @return Array $country_list
     */
    function getAllRouteCountries()
    {
        //prepare the query
        $stmt = $this->conn->prepare('SELECT distinct prefix_country_name
            FROM core.core_prefix
            where prefix_status=\'ENABLED\'
            order by 1;');

        //set the fetch mode
        $stmt->setFetchMode(PDO::FETCH_ASSOC);

        //execute it
        $stmt->execute();

        //fetch the data
        $country_list = $stmt->fetchAll();

        return $country_list;
    }

    /**
     * Gets all route networks within a country
     * @param $country_name
     * @param $service_id - If service ID is set, no networks already added to that service will be returned
     * @return Array $network_list
     */
    function getAllRouteNetworksByCountry($country_name, $service_id = null)
    {
        if(isset($service_id))
        {
            //prepare the data
            $data = array(':country_name' => $country_name, ':service_id' => $service_id);

            $query_string = "SELECT prefix_mccmnc,prefix_network_name,prefix_country_name
                FROM core.core_prefix cp
                LEFT JOIN core.core_route cr ON cr.route_mccmnc = cp.prefix_mccmnc AND cr.service_id = :service_id
                where cr.route_mccmnc IS NULL
                        AND prefix_status='ENABLED'
                        AND prefix_country_name=:country_name
                GROUP BY prefix_mccmnc;";
        }
        else
        {
            //prepare the data
            $data = array(':country_name' => $country_name);

            $query_string = "SELECT prefix_mccmnc,prefix_network_name,prefix_country_name
                FROM core.core_prefix
                where prefix_country_name=:country_name
                and prefix_status='ENABLED'
                GROUP BY prefix_mccmnc;";
        }

        //prepare the query
        $stmt = $this->conn->prepare($query_string);

        //set the fetch mode
        $stmt->setFetchMode(PDO::FETCH_ASSOC);

        //execute it
        $stmt->execute($data);

        //fetch the data
        $network_list = $stmt->fetchAll();

        return $network_list;
    }

    /**
     * Gets all collections for SOuth Africa only
     * @param $mccmnc
     *
     * @return Array $collections_list
     */
    function getAllRouteCollectionsRSA($mccmnc)
    {
        //prepare the data
        $data = array(':mccmnc' => $mccmnc);

        //prepare the query
        $stmt = $this->conn->prepare('SELECT DISTINCT rc.route_collection_name,rc.route_collection_id
            FROM core.core_route_collection_route rcr
            INNER JOIN core_route_collection rc
            ON rcr.route_collection_id = rc.route_collection_id
            WHERE rcr.route_collection_route_match_mccmnc = :mccmnc
            AND rcr.route_collection_route_status=\'ENABLED\'
            AND rcr.route_collection_id IN (14,92,69,86,54,37,91,5)');

        //set the fetch mode
        $stmt->setFetchMode(PDO::FETCH_ASSOC);

        //execute it
        $stmt->execute($data);

        //fetch the data
        $collections_list = $stmt->fetchAll();

        return $collections_list;
    }

    /**
     * Gets all collections for International countries
     * @param $mccmnc
     *
     * @return Array $collections_list
     */
    function getAllRouteCollectionsInternational($mccmnc)
    {
        //prepare the data
        $data = array(':mccmnc' => $mccmnc);

        //prepare the query
        $stmt = $this->conn->prepare('SELECT DISTINCT rc.route_collection_name,rc.route_collection_id
            FROM core.core_route_collection_route rcr
            INNER JOIN core_route_collection rc
            ON rcr.route_collection_id = rc.route_collection_id
            WHERE rcr.route_collection_route_match_mccmnc = :mccmnc
            AND rcr.route_collection_route_status=\'ENABLED\'
            AND rcr.route_collection_id IN (83,100,97,96)');

        //set the fetch mode
        $stmt->setFetchMode(PDO::FETCH_ASSOC);

        //execute it
        $stmt->execute($data);

        //fetch the data
        $collections_list = $stmt->fetchAll();

        return $collections_list;
    }


    function saveNewRoute($service_id, $provider_id, $route_msg_type, $route_description, $route_collection_id, $route_currency, $route_mccmnc, $route_match_regex, $route_status, $route_billing, $route_match_priority)
    {
        //prepare the data
        $data = array(':provider_id' => $provider_id, ':service_id' => $service_id, ':route_msg_type' => $route_msg_type, ':route_description' => $route_description, ':route_collection_id' => $route_collection_id,
            ':route_currency' => $route_currency, ':route_mccmnc' => $route_mccmnc, ':route_match_regex' => $route_match_regex, ':route_status' => $route_status, ':route_billing' => $route_billing,
            ':route_match_priority' => $route_match_priority);

        //prepare the query
        $stmt = $this->conn->prepare("INSERT INTO core.core_route (provider_id, service_id, route_msg_type, route_description, route_collection_id, route_currency,
                route_mccmnc, route_match_regex, route_status, route_billing, route_match_priority )
                VALUES (:provider_id, :service_id, :route_msg_type, :route_description, :route_collection_id, :route_currency,
                :route_mccmnc, :route_match_regex, :route_status, :route_billing, :route_match_priority) ");

        //execute it
        $stmt->execute($data);

        //return the insert ID
        return $this->conn->lastInsertId();
    }


    function updateRoute($route_id, $provider_id, $route_msg_type, $route_description, $route_collection_id, $route_currency, $route_mccmnc, $route_match_regex, $route_status, $route_billing, $route_match_priority)
    {
        //prepare the data
        $data = array(':route_id' => $route_id, ':provider_id' => $provider_id, ':route_msg_type' => $route_msg_type, ':route_description' => $route_description,
            ':route_collection_id' => $route_collection_id, ':route_currency' => $route_currency, ':route_mccmnc' => $route_mccmnc,
            ':route_match_regex' => $route_match_regex, ':route_status' => $route_status, ':route_billing' => $route_billing,
            ':route_match_priority' => $route_match_priority);


        //prepare the query
        $stmt = $this->conn->prepare("UPDATE core.".self::SQL_TABLE_CORE_ROUTES." SET provider_id = :provider_id,
                route_msg_type = :route_msg_type, route_description = :route_description,
                route_collection_id = :route_collection_id, route_currency = :route_currency,
                route_mccmnc = :route_mccmnc, route_match_regex = :route_match_regex, route_status = :route_status,
                route_billing = :route_billing, route_match_priority = :route_match_priority
                WHERE route_id = :route_id");

        //execute it and save
        return $stmt->execute($data);
    }

    function checkServiceRouteExists($service_id, $route_mccmnc)
    {
        //prepare the data
        $data = array(':service_id' => $service_id, ':route_mccmnc' => $route_mccmnc);

        //prepare the query
        $stmt = $this->conn->prepare('SELECT count(*)
                    FROM core.core_route
                    WHERE service_id = :service_id AND route_mccmnc = :route_mccmnc');

        //set the fetch mode
        $stmt->setFetchMode(PDO::FETCH_ASSOC);

        //execute it
        $stmt->execute($data);

        //fetch the data
        $number_of_rows = $stmt->fetchColumn();

        //if no matches do insert
        if ($number_of_rows == 0)
        {
            return false;
        }
        else
        {
            return true;
        }
    }

    function deleteRoute($route_id)
    {
        //prepare the data
        $data = array(':route_id' => $route_id);

        //prepare the query
        $stmt = $this->conn->prepare('DELETE FROM core.core_route WHERE route_id = :route_id');

        //execute it
        return $stmt->execute($data);

    }
}