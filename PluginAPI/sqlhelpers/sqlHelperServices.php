<?php


class sqlHelperServices
{
    const SERVICE_TABLE_PUSH_ENDPOINT_DLR = 'push_endpoint_dlr';
    const SERVICE_TABLE_PUSH_ENDPOINT_MOSMS = 'push_endpoint_mosms';
    const SERVICE_TABLE_CORE_SERVICE_CREDIT = 'core_service_credit';
    const SERVICE_TABLE_CORE_SERVICE = 'core_service';

    private $conn;
    private $table_fields_core_service_credit = array('service_credit_msg_type',
        'service_credit_billing_type' => 1,
        'service_credit_available' => 1,
        'service_credit_created' => 1,
        'service_iso_id' => 1,
        'service_notification_threshold' => 1,
        'service_notification_email' => 1,
        'service_notification_sms' => 1,
        'service_notification_triggered' => 1,
        'service_notification_lastsend' => 1);

    private $table_fields_core_service = array('account_id' => 1,
        'service_name' => 1,
        'service_login' => 1,
        'service_status' => 1,
        'service_created' => 1,
        'service_meta' => 1,
        'service_manager_user_id' => 1,
        'service_id_reseller' => 1,
        'service_billed_service_id' => 1);

    private $table_fields_push_endpoint_mosms = array('endpoint_mosms_id' => 1,
        'endpoint_mosms_address' => 1,
        'endpoint_mosms_status' => 1,
        'endpoint_mosms_created' => 1,
        'endpoint_mosms_method' => 1,
        'endpoint_mosms_flags' => 1);

    private $table_fields_push_endpoint_dlr = array('endpoint_dlr_id' => 1,
        'endpoint_dlr_address' => 1,
        'endpoint_dlr_status' => 1,
        'endpoint_dlr_created' => 1,
        'endpoint_dlr_method' => 1,
        'endpoint_dlr_flags' => 1,
        'endpoint_dlr_comment' => 1);


    public function __construct()
    {
        $this->conn = dbAdapter::getPDOMasterCore();
    }

    public function killDbConnection()
    {
        $this->conn = null;
    }

    public function getAllServicesAdmin($page, $limit)
    {
        //determine the limit and offset from the pager
        if (!isset($page)) {
            $limit = 25;
            $page = 1;
        }
        $offset = $limit * ($page - 1);

        //prepare the query
        $stmt = $this->conn->prepare('SELECT a.account_name, a.account_status,
                    s.service_name, s.service_id, s.service_id_reseller, s.service_login,
                    c.header_logo, c.account_colour, c.url_prefix, c.url_suffix
                    FROM core.core_service s
                    LEFT JOIN core.core_account a USING (account_id)
                    LEFT JOIN core.core_account_settings c USING (account_id)
                    GROUP BY s.service_name
                    ORDER BY s.service_name, a.account_name ASC LIMIT :offset, :limit');

        $stmt->bindValue(':limit', (int) $limit, PDO::PARAM_INT);
        $stmt->bindValue(':offset', (int) $offset, PDO::PARAM_INT);

        //set the fetch mode
        $stmt->setFetchMode(PDO::FETCH_ASSOC);

        //execute it
        $stmt->execute();

        //fetch the data
        $services = $stmt->fetchAll();

        return $services;
    }

    public function getAllServicesClient($permission_ids, $page, $limit)
    {
        //determine the limit and offset from the pager
        if (!isset($page)) {
            $limit = 25;
            $page = 1;
        }
        $offset = $limit * ($page - 1);

        $permission_ids_string = implode(',', $permission_ids);

        //prepare the query
        $stmt = $this->conn->prepare('SELECT a.account_name, a.account_status,
                    s.service_name, s.service_id, s.service_id_reseller, s.service_login,
                    c.header_logo, c.account_colour, c.url_prefix, c.url_suffix
                    FROM core.core_service s
                    LEFT JOIN core.core_account a USING (account_id)
                    LEFT JOIN core.core_account_settings c USING (account_id)
                    WHERE s.service_id IN ('.$permission_ids_string.')
                    GROUP BY s.service_name
                    ORDER BY s.service_name, a.account_name ASC LIMIT :offset, :limit');

        $stmt->bindValue(':limit', (int) $limit, PDO::PARAM_INT);
        $stmt->bindValue(':offset', (int) $offset, PDO::PARAM_INT);

        //set the fetch mode
        $stmt->setFetchMode(PDO::FETCH_ASSOC);

        //execute it
        $stmt->execute();

        //fetch the data
        $services = $stmt->fetchAll();

        return $services;
    }

    public function getAllServicesAdminSearch($page, $limit, $search_type, $search_term)
    {
        //determine the limit and offset from the pager
        if (!isset($page)) {
            $limit = 25;
            $page = 1;
        }
        $offset = $limit * ($page - 1);

        if($search_type == "" || $search_type == "all")
        {
            //for searching all
            $stmt = $this->conn->prepare('SELECT a.account_name, a.account_status,
                    s.service_name, s.service_id, s.service_id_reseller, s.service_login,
                    c.header_logo, c.account_colour, c.url_prefix, c.url_suffix
                    FROM core.core_service s
                    LEFT JOIN core.core_account a USING (account_id)
                    LEFT JOIN core.core_account_settings c USING (account_id)
                    WHERE s.service_id LIKE \'%'.$search_term.'%\' OR s.service_name LIKE \'%'.$search_term.'%\' OR a.account_name LIKE \'%'.$search_term.'%\'
                    GROUP BY s.service_name
                    ORDER BY s.service_name, a.account_name ASC LIMIT :offset, :limit');
        }
        else if($search_type == "service_id")
        {
            //prepare the query
            $stmt = $this->conn->prepare('SELECT DISTINCT a.account_name, a.account_status,
                    s.service_name, s.service_id, s.service_id_reseller, s.service_login,
                    c.header_logo, c.account_colour, c.url_prefix, c.url_suffix
                    FROM core.core_service s
                    LEFT JOIN core.core_account a USING (account_id)
                    LEFT JOIN core.core_account_settings c USING (account_id)
                    WHERE s.service_id = '.$search_term.'
                    GROUP BY s.service_name
                    ORDER BY s.service_name, a.account_name ASC LIMIT :offset, :limit');
        }
        else if($search_type == "service_name")
        {
            //prepare the query
            $stmt = $this->conn->prepare('SELECT DISTINCT a.account_name, a.account_status,
                    s.service_name, s.service_id, s.service_id_reseller, s.service_login,
                    c.header_logo, c.account_colour, c.url_prefix, c.url_suffix
                    FROM core.core_service s
                    LEFT JOIN core.core_account a ON a.account_id = s.account_id
                    LEFT JOIN core.core_account_settings c ON c.account_id = s.account_id
                    WHERE s.service_name LIKE \'%'.$search_term.'%\'
                    GROUP BY s.service_name
                    ORDER BY s.service_name ASC LIMIT :offset, :limit');
        }
        else if($search_type == "account_name")
        {
            //prepare the query
            $stmt = $this->conn->prepare('SELECT DISTINCT a.account_name, a.account_status,
                    s.service_name, s.service_id, s.service_id_reseller, s.service_login,
                    c.header_logo, c.account_colour, c.url_prefix, c.url_suffix
                    FROM core.core_service s
                    LEFT JOIN core.core_account a USING (account_id)
                    LEFT JOIN core.core_account_settings c USING (account_id)
                    WHERE a.account_name LIKE \'%'.$search_term.'%\'
                    GROUP BY s.service_name
                    ORDER BY s.service_name, a.account_name ASC LIMIT :offset, :limit');
        }
        else if($search_type == "account_id")
        {
            //prepare the query
            $stmt = $this->conn->prepare('SELECT DISTINCT a.account_name, a.account_status,
                    s.service_name, s.service_id, s.service_id_reseller, s.service_login,
                    c.header_logo, c.account_colour, c.url_prefix, c.url_suffix
                    FROM core.core_service s
                    LEFT JOIN core.core_account a USING (account_id)
                    LEFT JOIN core.core_account_settings c USING (account_id)
                    WHERE a.account_id = '.$search_term.'
                    GROUP BY s.service_name
                    ORDER BY s.service_name, a.account_name ASC LIMIT :offset, :limit');
        }


        $stmt->bindValue(':limit', (int) $limit, PDO::PARAM_INT);
        $stmt->bindValue(':offset', (int) $offset, PDO::PARAM_INT);

        //set the fetch mode
        $stmt->setFetchMode(PDO::FETCH_ASSOC);

        //execute it
        $stmt->execute();

        //fetch the data
        $services = $stmt->fetchAll();

        return $services;
    }

    public function getAllServicesClientSearch($permission_ids, $page, $limit, $search_type, $search_term)
    {
        //determine the limit and offset from the pager
        if (!isset($page)) {
            $limit = 25;
            $page = 1;
        }
        $offset = $limit * ($page - 1);

        $permission_ids_string = implode(',', $permission_ids);

        if($search_type == "" || $search_type == "all")
        {
            //for searching all
            $stmt = $this->conn->prepare('SELECT a.account_name, a.account_status,
                    s.service_name, s.service_id, s.service_id_reseller, s.service_login,
                    c.header_logo, c.account_colour, c.url_prefix, c.url_suffix
                    FROM core.core_service s
                    LEFT JOIN core.core_account a USING (account_id)
                    LEFT JOIN core.core_account_settings c USING (account_id)
                    WHERE s.service_id IN ('.$permission_ids_string.') AND (s.service_id LIKE \'%'.$search_term.'%\' OR s.service_name LIKE \'%'.$search_term.'%\' OR a.account_name LIKE \'%'.$search_term.'%\')
                    GROUP BY s.service_name
                    ORDER BY s.service_name, a.account_name ASC LIMIT :offset, :limit');
        }
        else if($search_type == "service_id")
        {
            //prepare the query
            $stmt = $this->conn->prepare('SELECT DISTINCT a.account_name, a.account_status,
                    s.service_name, s.service_id, s.service_id_reseller, s.service_login,
                    c.header_logo, c.account_colour, c.url_prefix, c.url_suffix
                    FROM core.core_service s
                    LEFT JOIN core.core_account a USING (account_id)
                    LEFT JOIN core.core_account_settings c USING (account_id)
                    WHERE s.service_id IN ('.$permission_ids_string.') AND s.service_id = '.$search_term.'
                    GROUP BY s.service_name
                    ORDER BY s.service_name, a.account_name ASC LIMIT :offset, :limit');
        }
        else if($search_type == "service_name")
        {
            //prepare the query
            $stmt = $this->conn->prepare('SELECT DISTINCT a.account_name, a.account_status,
                    s.service_name, s.service_id, s.service_id_reseller, s.service_login,
                    c.header_logo, c.account_colour, c.url_prefix, c.url_suffix
                    FROM core.core_service s
                    LEFT JOIN core.core_account a ON a.account_id = s.account_id
                    LEFT JOIN core.core_account_settings c ON c.account_id = s.account_id
                    WHERE s.service_id IN ('.$permission_ids_string.') AND s.service_name LIKE \'%'.$search_term.'%\'
                    GROUP BY s.service_name
                    ORDER BY s.service_name ASC LIMIT :offset, :limit');
        }
        else if($search_type == "account_name")
        {
            //prepare the query
            $stmt = $this->conn->prepare('SELECT DISTINCT a.account_name, a.account_status,
                    s.service_name, s.service_id, s.service_id_reseller, s.service_login,
                    c.header_logo, c.account_colour, c.url_prefix, c.url_suffix
                    FROM core.core_service s
                    LEFT JOIN core.core_account a USING (account_id)
                    LEFT JOIN core.core_account_settings c USING (account_id)
                    WHERE s.service_id IN ('.$permission_ids_string.') AND a.account_name LIKE \'%'.$search_term.'%\'
                    GROUP BY s.service_name
                    ORDER BY s.service_name, a.account_name ASC LIMIT :offset, :limit');
        }
        else if($search_type == "account_id")
        {
            //prepare the query
            $stmt = $this->conn->prepare('SELECT DISTINCT a.account_name, a.account_status,
                    s.service_name, s.service_id, s.service_id_reseller, s.service_login,
                    c.header_logo, c.account_colour, c.url_prefix, c.url_suffix
                    FROM core.core_service s
                    LEFT JOIN core.core_account a USING (account_id)
                    LEFT JOIN core.core_account_settings c USING (account_id)
                    WHERE s.service_id IN ('.$permission_ids_string.') AND a.account_id = '.$search_term.'
                    GROUP BY s.service_name
                    ORDER BY s.service_name, a.account_name ASC LIMIT :offset, :limit');
        }


        $stmt->bindValue(':limit', (int) $limit, PDO::PARAM_INT);
        $stmt->bindValue(':offset', (int) $offset, PDO::PARAM_INT);

        //set the fetch mode
        $stmt->setFetchMode(PDO::FETCH_ASSOC);

        //execute it
        $stmt->execute();

        //fetch the data
        $services = $stmt->fetchAll();

        return $services;
    }

    //**********************************
    //updateservice.php | viewservice.php | viewserviceAdmin.php
    function getServiceData($service_id) {

        //prepare the data
        $data = array(':service_id' => $service_id);

        //prepare the query
        $stmt = $this->conn->prepare("SELECT account_name,
            s.account_id,
            service_name,
            service_status,
            service_login,
            service_created,
            us.user_password as 'service_password',
            sc.service_credit_billing_type,
            sc.service_credit_available,
            service_notification_threshold,
            service_notification_email,
            service_notification_sms,
            u.user_fullname as 'service_manager',
            c.header_logo,
            c.account_colour,
            c.url_prefix,
            c.url_suffix
            FROM core.core_service s
            INNER JOIN core.core_account a
            ON s.account_id = a.account_id
            INNER JOIN core_service_credit sc
            ON s.service_id = sc.service_id
            LEFT OUTER JOIN core.core_user u
            ON s.service_manager_user_id = u.user_id
            LEFT OUTER JOIN core.core_user us
            ON s.service_login = us.user_username
            LEFT OUTER JOIN core.core_account_settings c
            ON s.account_id = c.account_id
            WHERE s.service_id = :service_id");

        //set the fetch mode
        $stmt->setFetchMode(PDO::FETCH_ASSOC);

        //execute it
        $stmt->execute($data);

        //fetch the data
        $service_data = $stmt->fetch();

        return $service_data;
    }

    function getServiceDLRConfig($service_id)
    {
        //prepare the data
        $data = array(':service_id' => $service_id);

        //prepare the query
        $stmt = $this->conn->prepare("SELECT endpoint_dlr_id,endpoint_dlr_address,endpoint_dlr_status,endpoint_dlr_method
            FROM core.push_endpoint_dlr
            WHERE service_id = :service_id");

        //set the fetch mode
        $stmt->setFetchMode(PDO::FETCH_ASSOC);

        //execute it
        $stmt->execute($data);

        //fetch the data
        $dlr_config_data = $stmt->fetchAll();

        return $dlr_config_data;
    }


    function getServiceMOSMSConfig($service_id)
    {
        //prepare the data
        $data = array(':service_id' => $service_id);

        //prepare the query
        $stmt = $this->conn->prepare("SELECT endpoint_mosms_id,endpoint_mosms_address,endpoint_mosms_status,endpoint_mosms_method
            FROM core.push_endpoint_mosms
            WHERE service_id = :service_id");

        //set the fetch mode
        $stmt->setFetchMode(PDO::FETCH_ASSOC);

        //execute it
        $stmt->execute($data);

        //fetch the data
        $mosms_config_data = $stmt->fetchAll();

        return $mosms_config_data;
    }

    /**
     * Insert a new DLR Endpoint record for a service.
     * @param $service_id
     * @param $endpoint_dlr_address
     * @param $endpoint_dlr_status
     * @param $endpoint_dlr_method
     * @param $endpoint_dlr_flags
     * @return bool
     */
    function createDLREndpoint($service_id, $endpoint_dlr_address, $endpoint_dlr_status, $endpoint_dlr_method, $endpoint_dlr_flags)
    {
        $data = array(':service_id' => $service_id, ':endpoint_dlr_address' => $endpoint_dlr_address, ':endpoint_dlr_status' => $endpoint_dlr_status,
                ':endpoint_dlr_method' => $endpoint_dlr_method, ':endpoint_dlr_flags' => $endpoint_dlr_flags);

        //prepare the query
        $stmt = $this->conn->prepare("INSERT INTO core.push_endpoint_dlr(`service_id`, `endpoint_dlr_address`, `endpoint_dlr_status`, `endpoint_dlr_method`, `endpoint_dlr_flags`)
          VALUES(:service_id, :endpoint_dlr_address, :endpoint_dlr_status, :endpoint_dlr_method, :endpoint_dlr_flags)");

        //execute it
        if($stmt->execute($data))
        {
            return $this->conn->lastInsertId();
        }
        else
        {
            return false;
        }

    }

    /**
     * Insert a new MOSMS Endpoint for a service.
     * @param $service_id
     * @param $endpoint_mosms_address
     * @param $endpoint_mosms_status
     * @param $endpoint_mosms_method
     * @param $endpoint_mosms_flags
     * @return bool
     */
    function createMOSMSEndpoint($service_id, $endpoint_mosms_address, $endpoint_mosms_status, $endpoint_mosms_method, $endpoint_mosms_flags)
    {
        $data = array(':service_id' => $service_id, ':endpoint_mosms_address' => $endpoint_mosms_address, ':endpoint_mosms_status' => $endpoint_mosms_status,
            ':endpoint_mosms_method' => $endpoint_mosms_method, ':endpoint_mosms_flags' => $endpoint_mosms_flags);

        //prepare the query
        $stmt = $this->conn->prepare("INSERT INTO core.push_endpoint_mosms (`service_id`, `endpoint_mosms_address`, `endpoint_mosms_status`, `endpoint_mosms_method`, `endpoint_mosms_flags`)
          VALUES(:service_id, :endpoint_mosms_address, :endpoint_mosms_status, :endpoint_mosms_method, :endpoint_mosms_flags)");

        //execute it
        //execute it
        if($stmt->execute($data))
        {
            return $this->conn->lastInsertId();
        }
        else
        {
            return false;
        }
    }

    /**
     * Does as it says, checks if a service name exists and returns an appropriate boolean value
     * @param $service_name - THe name of the service
     * @param $service_id_to_ignore - The service ID that should not be included in the duplicate search
     * @return int
     */
    function isServiceNameUnique($service_name, $service_id_to_ignore = null)
    {
        if(isset($service_id_to_ignore))
        {
            //prepare the data
            $data = array(':service_id' => $service_id_to_ignore, ':service_name' => $service_name);

            //prepare the query
            $stmt = $this->conn->prepare('SELECT count(*) FROM core.core_service WHERE service_name = :service_name AND service_id != :service_id');
        }
        else
        {
            //prepare the data
            $data = array(':service_name' => $service_name);

            //prepare the query
            $stmt = $this->conn->prepare('SELECT count(*) FROM core.core_service WHERE service_name = :service_name');
        }

        //execute it
        $stmt->execute($data);

        //fetch the data
        $number_of_rows = $stmt->fetchColumn();

        if ($number_of_rows == 0)
        {
            return true;
        }
        else
        {
            return false;
        }
    }

    /**
     * Gets the default routing data for a service
     *
     * @param $service_id - THe ID of the service to query
     * @return array - an object containing all routes for this service
     */
    function getDefaultRouteData($service_id)
    {

        //prepare the data
        $data = array(':service_id' => $service_id);

        //prepare the query
        $stmt = $this->conn->prepare("SELECT COUNTRY, NETWORK, `STATUS`, RATE, COLLECTION, Route.route_collection_id, MCCMNC, route_id, route_meta,
             replace(rcrd.route_collection_route_smsc_name, '_smsc', '') as 'DEFAULT',
             replace(rcrp.route_collection_route_smsc_name, '_smsc', '') as 'PORTED',
             route_currency
            FROM
            (SELECT p.prefix_country_name as 'COUNTRY',
             concat(p.prefix_network_name, ' (', r.route_mccmnc, ')') as 'NETWORK',
             r.route_status as 'STATUS',
             r.route_id as 'route_id',
             r.route_meta,
             r.route_billing as 'RATE',
             rc.route_collection_name as 'COLLECTION',
             rc.route_collection_id,
             r.route_mccmnc as 'MCCMNC',
             r.route_currency
            FROM core.core_route r
            LEFT OUTER JOIN core.core_prefix p
            ON r.route_mccmnc = p.prefix_mccmnc
            INNER JOIN core.core_route_collection rc
            ON r.route_collection_id = rc.route_collection_id
            WHERE r.route_msg_type = 'MTSMS' AND r.service_id = :service_id
            GROUP BY p.prefix_country_name,
             concat(p.prefix_network_name, ' (', r.route_mccmnc, ')'),
             r.route_status,
             r.route_billing,
             rc.route_collection_name
            ) as Route
            LEFT OUTER JOIN core.core_route_collection_route rcrp
            ON Route.route_collection_id = rcrp.route_collection_id
            AND concat(MCCMNC, 'p') = rcrp.route_collection_route_match_mccmnc
            AND rcrp.route_collection_route_match_priority = (SELECT min(route_collection_route_match_priority)
            FROM core.core_route_collection_route
            WHERE route_collection_id = Route.route_collection_id
            AND route_collection_route_match_mccmnc = concat(MCCMNC, 'p')
            LIMIT 1)
            LEFT OUTER JOIN core.core_route_collection_route rcrd
            ON Route.route_collection_id = rcrd.route_collection_id
            AND MCCMNC = rcrd.route_collection_route_match_mccmnc
            AND rcrd.route_collection_route_match_priority = (SELECT min(route_collection_route_match_priority)
            FROM core.core_route_collection_route q
            WHERE q.route_collection_id = Route.route_collection_id
            AND q.route_collection_route_match_mccmnc = MCCMNC
            LIMIT 1)
            order by COUNTRY, MCCMNC;
        ");

        //set the fetch mode
        $stmt->setFetchMode(PDO::FETCH_ASSOC);

        //execute it
        $stmt->execute($data);

        //fetch the data
        $routes = $stmt->fetchAll();

        //do a bit of post processing for the page
        foreach ($routes as $key => $route)
        {
            //for the flag image path
            if(substr($route['COUNTRY'], 0, 3) == "USA")
            {
                $flag = "USA";
            }
            else
            {
                $flag = $route['COUNTRY'];
            }

            $flag = str_replace('/', '&', $flag);
            $route['flag_path'] = 'img/flags/' . $flag . '.png';

            //for the network logo image path
            $s1 = strrpos($route['NETWORK'], '/');
            $s2 = strrpos($route['NETWORK'], '(');

            $logo = substr($route['NETWORK'], $s1, $s2);
            $logo = trim($logo);

            if ($logo != "Cell C")
            {  // | $logo != 'Vodacom SA' | $logo != 'MTN-SA'
                if ($logo != 'Vodacom SA')
                {
                    if ($logo != 'MTN-SA')
                    {
                        if ($logo != 'Telkom Mobile')
                        {
                            if ($logo != 'MTN (Nigeria)')
                            {
                                $logo = substr($route['NETWORK'], $s1 + 1, $s2 - $s1 - 2);
                                $logo = trim($logo);
                            }
                        }
                    }
                }
            }

            $route['logo_path'] = 'img/serviceproviders/' . $logo . '.png';

            if (strpos($route['RATE'], ','))
            {
                $route_config = explode(',', $route['RATE']);
                $route['real_rate'] = $route_config[0];
                $route['rdnc'] = $route_config[4];
            }
            else
            {
                $route['real_rate'] = $route['RATE'];
                $route['rdnc'] = 0;
            }

            $routes[$key] = $route;
        }

        return $routes;
    }

    /**
     * Pulls all users associated with this service
     * @param $service_id
     * @return array
     */
    function getServiceUsers($service_id) {
        //open the DB object
        $conn = getPDOMasterCore();

        //prepare the data
        $data = array(':service_id' => $service_id);

        //prepare the query
        $stmt = $conn->prepare("SELECT u.user_id, u.user_default_service_id, u.user_username, u.user_password, u.user_status, s.service_login
            FROM core.core_permission p
            INNER JOIN core.core_user u
            ON p.user_id = u.user_id
            INNER JOIN core.core_service s
            ON p.service_id = s.service_id
            WHERE p.service_id = :service_id
            AND user_username NOT LIKE '%.connet'
            AND user_default_service_id = :service_id
            GROUP BY u.user_username, u.user_password, u.user_status");

        //set the fetch mode
        $stmt->setFetchMode(PDO::FETCH_ASSOC);

        //execute it
        $stmt->execute($data);

        //fetch the data
        $users = $stmt->fetchAll();

        return $users;
    }

    /**
     * Checks if a user has access to a service.
     * @param $service_id
     * @param $user_id
     * @return array
     */
    function checkUserHasAccessToService($service_id, $user_id) {

        //prepare the data
        $data = array(':service_id' => $service_id, ':user_id' => $user_id);

        //prepare the query
        $stmt = $this->conn->prepare("SELECT COUNT(*)
            FROM core.core_permission
            WHERE service_id = :service_id AND user_id = :user_id");

        //set the fetch mode
        $stmt->setFetchMode(PDO::FETCH_ASSOC);

        //execute it
        $stmt->execute($data);

        //fetch the data
        $count = $stmt->fetchColumn();

        if($count == 0)
        {
            return false;
        }
        else
        {
            return true;
        }
    }

    /**
     * Add user permissions to a service.
     * @param $service_id
     * @param $user_id
     * @param $permission_type
     * @return array
     */
    function addUserPermissionToService($service_id, $user_id, $permission_type)
    {

        //prepare the data
        $data = array(':service_id' => $service_id, ':user_id' => $user_id, ':permission_type' => $permission_type);

        //prepare the query
        $stmt = $this->conn->prepare("INSERT INTO core.core_permission (`service_id`, `user_id`, `permission_type`) VALUES (:service_id, :user_id, :permission_type)");

        //execute it
        return $stmt->execute($data);
    }

    /**
     * Removes user permissions from a service.
     * @param $service_id
     * @param $user_id
     * @return array
     */
    function removeUserPermissionFromService($service_id, $user_id)
    {
        //prepare the data
        $data = array(':service_id' => $service_id, ':user_id' => $user_id);

        //prepare the query
        $stmt = $this->conn->prepare("DELETE FROM core.core_permission WHERE service_id = :service_id AND user_id = :user_id");

        //execute it
        return $stmt->execute($data);
    }

    /**
     * Pulls all users associated with this service
     * @param $service_id
     * @return array
     */
    function getServiceUserIds($service_id) {
        //open the DB object
        $conn = getPDOMasterCore();

        //prepare the data
        $data = array(':service_id' => $service_id);

        //prepare the query
        $stmt = $conn->prepare("SELECT u.user_id
            FROM core.core_permission p
            INNER JOIN core.core_user u
            ON p.user_id = u.user_id
            INNER JOIN core.core_service s
            ON p.service_id = s.service_id
            WHERE p.service_id = :service_id
            AND user_username NOT LIKE '%.connet'
            GROUP BY u.user_id");

        //set the fetch mode
        $stmt->setFetchMode(PDO::FETCH_ASSOC);

        //execute it
        $stmt->execute($data);

        //fetch the data
        $user_ids = $stmt->fetchAll();

        return $user_ids;
    }

    /**
     * Pulls all users associated with this service
     * @param $service_id
     * @return array
     */
    function getServiceUserAdminIds($service_id)
    {
        //prepare the data
        $data = array(':service_id' => $service_id);

        //prepare the query
        $stmt = $this->conn->prepare("SELECT u.user_id
            FROM core.core_permission p
            INNER JOIN core.core_user u
            ON p.user_id = u.user_id
            INNER JOIN core.core_service s
            ON p.service_id = s.service_id
            WHERE p.service_id = :service_id AND u.user_system_admin = 1
            GROUP BY u.user_id");

        //set the fetch mode
        $stmt->setFetchMode(PDO::FETCH_ASSOC);

        //execute it
        $stmt->execute($data);

        //fetch the data
        $user_ids = $stmt->fetchAll();

        return $user_ids;
    }

    function newPaymentRecord($service_id, $user_id, $payment_value, $payment_description, $payment_previous_balance) {

        //prepare the data
        $data = array(':service_id' => $service_id, ':user_id' => $user_id, ':payment_value' => $payment_value,
            ':payment_description' => $payment_description, ':payment_previous_balance' => $payment_previous_balance);

        //prepare the query
        $stmt = $this->conn->prepare("INSERT INTO core.core_service_payment(`service_id`, `user_id`, `payment_value`, `payment_description`, `payment_previous_balance`)
        VALUES(:service_id, :user_id, :payment_value, :payment_description, :payment_previous_balance)");

        //execute it
        $stmt->execute($data);
        return true;
    }


    /**
     * @param $service_id - THe ID of the relevant service
     * @param $offset - The record offset for pagination
     * @param $limit - The limit of records to return per page
     * @return array of payment records
     */
    function getPaymentRecords($service_id, $offset, $limit)
    {
        //prepare the query
        $stmt = $this->conn->prepare("SELECT p.payment_value, p.payment_description, p.payment_timestamp, p.payment_previous_balance, p.user_id, u.user_username
                FROM core.core_service_payment p
                INNER JOIN core.core_user u
                ON u.user_id = p.user_id
                WHERE p.service_id = :service_id ORDER BY p.payment_timestamp DESC LIMIT :offset, :limit");

        $stmt->bindValue(':service_id', $service_id);
        $stmt->bindValue(':limit', (int) $limit, PDO::PARAM_INT);
        $stmt->bindValue(':offset', (int) $offset, PDO::PARAM_INT);

        //set the fetch mode
        $stmt->setFetchMode(PDO::FETCH_ASSOC);

        //execute it
        $stmt->execute();

        //fetch the data
        $payments = $stmt->fetchAll();

        foreach($payments as $key => $payment)
        {
            $payment['payment_previous_balance_display'] = number_format($payment['payment_previous_balance'] / 10000, 2, '.', ' ');
            $payment['payment_value_display'] = number_format($payment['payment_value'] / 10000, 2, '.', ' ');
            $payment['new_balance_display'] = number_format(($payment['payment_previous_balance'] + $payment['payment_value']) / 10000, 2, '.', ' ');
            $payments[$key] = $payment;
        }

        return $payments;
    }

    /**
     * @param $service_id - THe ID of the relevant service
     * @return int total records
     */
    function getTotalPaymentRecords($service_id)
    {
        //prepare the query
        $stmt = $this->conn->prepare("SELECT COUNT(*)
                FROM core.core_service_payment p
                INNER JOIN core.core_user u
                ON u.user_id = p.user_id
                WHERE p.service_id = :service_id");

        $stmt->bindValue(':service_id', $service_id);

        //set the fetch mode
        $stmt->setFetchMode(PDO::FETCH_ASSOC);

        //execute it
        $stmt->execute();

        //return the count
        return $stmt->fetchColumn();
    }


    function updateCredit($service_id, $user_id, $payment_value, $payment_description, $payment_previous_balance)
    {
        //prepare the data
        $data = array(':service_id' => $service_id, ':user_id' => $user_id, ':payment_value' => $payment_value,
            ':payment_description' => $payment_description, ':payment_previous_balance' => $payment_previous_balance);

        //prepare the query
        $stmt = $this->conn->prepare("INSERT INTO core.core_service_payment(`service_id`, `user_id`, `payment_value`, `payment_description`, `payment_previous_balance`)
        VALUES(:service_id, :user_id, :payment_value, :payment_description, :payment_previous_balance)");

        //execute it
        $update_credit_success = $stmt->execute($data);

        if($update_credit_success === true)
        {
            //prepare the data
            $data = array(':service_id' => $service_id);

            //prepare the query
            $stmt = $this->conn->prepare("UPDATE core.core_service_credit
              SET service_notification_lastsend = null
              WHERE service_id = :service_id ");

            return $stmt->execute($data);
        }
        else
        {
            return false;
        }
    }


    function getCurrentCredit($service_id) {

        //prepare the data
        $data = array(':service_id' => $service_id);

        //prepare the query
        $stmt = $this->conn->prepare('SELECT *
                FROM core.core_service_credit
                WHERE service_id = :service_id');

        //set the fetch mode
        $stmt->setFetchMode(PDO::FETCH_ASSOC);

        //execute it
        $stmt->execute($data);

        //fetch the data
        $funds = $stmt->fetch();

        return $funds;
    }

    /**
     * This basic function saves a value to a particular column in the services table
     * @param $service_id
     * @param $field_name
     * @param $field_value
     * @return bool success
     */
    public function saveServiceFieldValue($service_id, $field_name, $field_value = null)
    {
        $table_name = $this->getServiceFieldTable($field_name);

        //prepare the query
        $stmt = $this->conn->prepare("UPDATE ".$table_name." SET ".$field_name." = :field_value WHERE service_id = :service_id");

        $stmt->bindValue(':service_id', $service_id, PDO::PARAM_INT);
        if($field_value === null)
        {
            $stmt->bindValue(':field_value', null, PDO::PARAM_INT);
        }
        else
        {
            $stmt->bindValue(':field_value', $field_value);
        }

        //execute it
        return $stmt->execute();
    }

    /**
     * This basic function saves a DLR endpoint table value
     * @param $endpoint_id
     * @param $field_name
     * @param $field_value
     * @return bool success
     */
    public function saveDLREndpointValue($endpoint_id, $field_name, $field_value)
    {
        //prepare the data
        $data = array(':endpoint_dlr_id' => $endpoint_id, ':field_value' => $field_value);

        //prepare the query
        $stmt = $this->conn->prepare("UPDATE ".self::SERVICE_TABLE_PUSH_ENDPOINT_DLR." SET ".$field_name." = :field_value WHERE endpoint_dlr_id = :endpoint_dlr_id");

        //execute it
        return $stmt->execute($data);
    }

    /**
     * This basic function saves a MOSMS endpoint table value
     * @param $endpoint_id
     * @param $field_name
     * @param $field_value
     * @return bool success
     */
    public function saveMOSMSEndpointValue($endpoint_id, $field_name, $field_value)
    {
        //prepare the data
        $data = array(':endpoint_mosms_id' => $endpoint_id, ':field_value' => $field_value);

        //prepare the query
        $stmt = $this->conn->prepare("UPDATE ".self::SERVICE_TABLE_PUSH_ENDPOINT_MOSMS." SET ".$field_name." = :field_value WHERE endpoint_mosms_id = :endpoint_mosms_id");

        //execute it
        return $stmt->execute($data);
    }


    /**
     * Create a basic record for a service
     * @param $account_id
     * @param $service_name
     * @param $service_login_name
     * @param $manager_user_id
     * @return string
     */
    public function createNewService($account_id, $service_name, $service_login_name, $manager_user_id)
    {
        //old code for old CM
        if (stripos(" " . $service_name . " ", 'smpp')) {
            $service_meta = 'cm_slots=00-99&cm_smpp&smpp_queued_submit';
        } else {
            $service_meta = 'cm_slots=00-99';
        }

        //prepare the data
        $data = array(
            ':account_id' => $account_id,
            ':service_name' => $service_name,
            ':service_login' => $service_login_name,
            ':service_meta' => service_meta,
            ':service_manager_user_id' => $manager_user_id);

        //prepare the query
        $stmt = $this->conn->prepare("INSERT INTO core.core_service (account_id, service_name, service_login, service_meta, service_manager_user_id)
                VALUES (:account_id, :service_name, :service_login, :service_meta, :service_manager_user_id)");

        //execute it
        $stmt->execute($data);

        //return the insert ID
        return $this->conn->lastInsertId('service_id');
    }

    /**
     * Create a new service record for a newly created service
     * @param $service_id
     * @param $billing_type
     * @return string
     */
    function createServiceCreditRecord($service_id, $billing_type)
    {
        $data = array(':service_id' => $service_id, ':billing_type' => $billing_type);

        $stmt = $this->conn->prepare("INSERT INTO core.core_service_credit
                (service_id, service_credit_msg_type, service_credit_billing_type, service_credit_available,
                service_iso_id)VALUES (:service_id, 'MTSMS', :billing_type, '0', '166' ) ");

        $stmt->execute($data);

        return $this->conn->lastInsertId();
    }

    /**
     * Create a new service record for a newly created service
     * @param $service_id
     * @param $user_id
     * @return string
     */
    function createServiceAdminPermissions($service_id, $user_id)
    {
        $data = array(':service_id' => $service_id, ':user_id' => $user_id);

        //add sending permission
        $stmt = $this->conn->prepare("INSERT INTO core.core_permission (user_id, service_id, permission_type) VALUES (:user_id, :service_id, 'submit.mtsms') ");
        $stmt->execute($data);

        //add SUPER user permissions
        $stmt = $this->conn->prepare("INSERT INTO core.core_permission (user_id, service_id, permission_type) VALUES (:user_id, :service_id, 'SUPER') ");
        $stmt->execute($data);

        return true;
    }

    /**
     * Create a new service record for a newly created service
     * @param $service_id
     * @param $user_id
     * @return string
     */
    function createServiceClientPermissions($service_id, $user_id)
    {
        $data = array(':service_id' => $service_id, ':user_id' => $user_id);

        //add sending permission
        $stmt = $this->conn->prepare("INSERT INTO core.core_permission (user_id, service_id, permission_type) VALUES (:user_id, :service_id, 'submit.mtsms') ");
        $stmt->execute($data);

        return true;
    }

    /**
     * Create a new campaign record for our service, attached to a client via client_id
     * @param $client_id
     * @param $user_id
     * @param $campaign_name
     * @param $campaign_code
     * @param $campaign_status
     * @return string
     */
    function createCampaign($client_id, $user_id, $campaign_name, $campaign_code, $campaign_status)
    {
        //prepare the data
        $data = array(':client_id' => $client_id, ':user_id' => $user_id, ':campaign_name' => $campaign_name, ':campaign_code' => $campaign_code, ':campaign_status' => $campaign_status);

        //prepare the query
        $stmt = $this->conn->prepare('INSERT INTO core.campaign_campaign(client_id, user_id, campaign_name, campaign_code, campaign_status)
                VALUES (:client_id, :user_id, :campaign_name, :campaign_code, :campaign_status) ');

        //execute it
        $stmt->execute($data);

        //return the insert ID
        return $this->conn->lastInsertId();
    }

    /**
     * Creates a new client record for our service
     * @param $service_id
     * @param $user_id
     * @param $client_name
     * @param $client_status
     * @return string
     */
    function createClient($service_id, $user_id, $client_name, $client_status)
    {
        //prepare the data
        $data = array(':service_id' => $service_id, ':user_id' => $user_id, ':client_name' => $client_name, ':client_status' => $client_status);

        //prepare the query
        $stmt = $this->conn->prepare('INSERT INTO core.campaign_client (service_id, user_id, client_name, client_status)
                VALUES (:service_id, :user_id, :client_name, :client_status)');

        //execute it
        $stmt->execute($data);

        //return the insert ID
        return $this->conn->lastInsertId('client_id');
    }

    /**
     * Send a column name to this function to determine which table owns the column name in the context of a service
     * @param $field_name - The name of the field or column for which we want to know the owning table.
     * @return string - The Name of the table that has the column.
     */
    public function getServiceFieldTable($field_name)
    {
        $table_name= "";
        if(array_key_exists($field_name, $this->table_fields_core_service))
        {
            $table_name = self::SERVICE_TABLE_CORE_SERVICE;
        }
        else if(array_key_exists($field_name, $this->table_fields_core_service_credit))
        {
            $table_name = self::SERVICE_TABLE_CORE_SERVICE_CREDIT;
        }
        elseif(array_key_exists($field_name, $this->table_fields_push_endpoint_mosms))
        {
            $table_name = self::SERVICE_TABLE_PUSH_ENDPOINT_MOSMS;
        }
        elseif(array_key_exists($field_name, $this->table_fields_push_endpoint_dlr))
        {
            $table_name = self::SERVICE_TABLE_PUSH_ENDPOINT_DLR;
        }

        return $table_name;
    }
}