<?php


class sqlHelperBatches
{
    const BATCH_TABLE_BATCH_BATCH = 'batch_batch';

    private $conn;

    public function __construct()
    {
        $this->conn = dbAdapter::getPDOMasterCore();
    }

    public function killDbConnection()
    {
        $this->conn = null;
    }

    public function getBatchList($service_id, $start_date, $end_date)
    {
        //prepare the data
        $data = array(':service_id' => $service_id, ':start_date' => $start_date, ':end_date' => $end_date);

        //prepare the query
        $stmt = $this->conn->prepare("SELECT distinct batch_id, batch_schedule, client_name, ifnull(campaign_name, 'API Interface') as 'campaign_name', client_id, campaign_id, campaign_code, batch_reference, batch_csv_content,
                CASE batch_status WHEN 'NEW' THEN 'Draft' WHEN 'CSV' THEN 'Processing' WHEN 'WAIT'THEN batch_submit_status WHEN 'ERROR' THEN 'Error' WHEN 'BLACKLIST' THEN 'Blacklist' WHEN 'DONE' THEN 'Completed'
                WHEN 'BUCKET' THEN 'None' WHEN 'SEND' THEN 'Sending' ELSE batch_submit_status END as 'batch_submit_status', batch_mtsms_total
                FROM ".self::BATCH_TABLE_BATCH_BATCH." b
                LEFT JOIN core.campaign_batch USING (batch_id)
                LEFT JOIN core.campaign_campaign USING (campaign_id)
                LEFT JOIN core.campaign_client USING (client_id)
                WHERE date(batch_schedule) >= :start_date
                AND date(batch_schedule) <= :end_date
                AND b.service_id = :service_id
                AND batch_reference IS NOT NULL
                ORDER BY batch_schedule DESC");

        //set the fetch mode
        $stmt->setFetchMode(PDO::FETCH_ASSOC);

        //execute it
        $stmt->execute($data);

        //fetch the data
        $batch_list = $stmt->fetchAll();

        return $batch_list;
    }

    function getAllNetworks() {

        //prepare the query
        $stmt = $this->conn->prepare('SELECT *
                FROM core.core_prefix');

        //set the fetch mode
        $stmt->setFetchMode(PDO::FETCH_ASSOC);

        //execute it
        $stmt->execute();

        //fetch the data
        $networks = $stmt->fetchAll();
        return $networks;
    }

    function getBatchData($batch_id)
    {

        //prepare the data
        $data = array(':batch_id' => $batch_id);

        //prepare the query
        $stmt = $this->conn->prepare("SELECT b.service_id, batch_id, batch_schedule, batch_created as 'batch_created', client_name, ifnull(campaign_name, 'API Interface') as 'campaign_name', client_id, campaign_id, campaign_code, batch_reference,
          CASE batch_status WHEN 'NEW' THEN 'Draft' WHEN 'CSV'THEN 'Processing' WHEN 'WAIT'THEN batch_submit_status WHEN 'ERROR' THEN 'Error' WHEN 'BLACKLIST' THEN 'Blacklist' WHEN 'DONE' THEN 'Completed'
         WHEN 'BUCKET' THEN 'None' WHEN 'SEND' THEN 'Sending' ELSE batch_submit_status END as 'batch_submit_status', batch_mtsms_total
         FROM core.batch_batch b
         LEFT JOIN core.campaign_batch USING (batch_id)
         LEFT JOIN core.campaign_campaign USING (campaign_id)
         LEFT JOIN core.campaign_client USING (client_id)
         WHERE b.batch_id = :batch_id
         AND batch_reference IS NOT NULL");


        //set the fetch mode
        $stmt->setFetchMode(PDO::FETCH_ASSOC);

        //execute it
        $stmt->execute($data);

        //fetch the data
        $batch_data = $stmt->fetch();

        return $batch_data;
    }

    function setBatchStatus($batch_id, $batch_submit_status, $batch_schedule = null)
    {
        if(isset($batch_schedule))
        {
            //prepare the data
            $data = array(':batch_id' => $batch_id, ':batch_submit_status' => $batch_submit_status, ':batch_schedule' => $batch_schedule);

            //prepare the query
            $sql_string = "UPDATE batch_batch SET batch_status = 'WAIT', batch_submit_status = :batch_submit_status, batch_schedule = :batch_schedule WHERE batch_id = :batch_id ";
        }
        else
        {
            //prepare the data
            $data = array(':batch_id' => $batch_id, ':batch_submit_status' => $batch_submit_status);

            //prepare the query
            $sql_string = "UPDATE batch_batch SET batch_status = 'WAIT', batch_submit_status = :batch_submit_status WHERE batch_id = :batch_id ";
        }

        //prepare the query
        $stmt = $this->conn->prepare($sql_string);

        //execute it
        return $stmt->execute($data);
    }

}