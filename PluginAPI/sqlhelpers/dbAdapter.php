<?php

class dbAdapter
{
   const PLUGIN_DB_REPORT_DB_HOST = "waspreportdb1";
   const PLUGIN_DB_REPORT_DB_NAME = "core";
   const PLUGIN_DB_REPORT_DB_USER = "portal";
   const PLUGIN_DB_REPORT_DB_PASS = "phaeXe7Kee6OhZi";

   const PLUGIN_DB_MASTER_DB_HOST = "waspmasterdb";
   const PLUGIN_DB_MASTER_DB_NAME = "core";
   const PLUGIN_DB_MASTER_DB_USER = "portal";
   const PLUGIN_DB_MASTER_DB_PASS = "phaeXe7Kee6OhZi";


   /**
    * This method connects to the main core DB, with a PDO object for prepared statements.
    * @return PDO|bool
    */
   public static function getPDOMasterCore() {
      try {

         if (defined('USE_PDO_EXCEPTIONS') && USE_PDO_EXCEPTIONS) {

            $con = new PDO("mysql:host=" . self::PLUGIN_DB_MASTER_DB_HOST . ";dbname=" . self::PLUGIN_DB_MASTER_DB_NAME . "", self::PLUGIN_DB_MASTER_DB_USER, self::PLUGIN_DB_MASTER_DB_PASS, array(
                PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION));
         } else {
            $con = new PDO("mysql:host=" . self::PLUGIN_DB_MASTER_DB_HOST . ";dbname=" . self::PLUGIN_DB_MASTER_DB_NAME . "", self::PLUGIN_DB_MASTER_DB_USER, self::PLUGIN_DB_MASTER_DB_PASS);
         }
      } catch (PDOException $e) {
         clog::info('* PDO - MYSQL error: ' . $e->getMessage());
      }

      return $con;
   }

   /**
    * This method connects to the main core DB, with a PDO object for prepared statements.
    * @return bool|mysqli
    */
   public static function getPDOReportingCore() {
      try {
         $con = new PDO("mysql:host=" . self::PLUGIN_DB_REPORT_DB_HOST . ";dbname=" . self::PLUGIN_DB_REPORT_DB_NAME . "", self::PLUGIN_DB_REPORT_DB_USER, self::PLUGIN_DB_REPORT_DB_PASS);
      } catch (PDOException $e) {
         clog::info('* PDO - MYSQL error: ' . $e->getMessage());
      }

      return $con;
   }
}
