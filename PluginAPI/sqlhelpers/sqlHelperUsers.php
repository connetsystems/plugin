<?php

class sqlHelperUsers
{
    const SQL_TABLE_CORE_USER = "core_user";
    const SQL_TABLE_CORE_USER_PERMISSION = "core_permission";
    const SQL_TABLE_CORE_USER_ROLE = "core_user_role";
    const SQL_TABLE_CORE_USER_DETAILS = "account_details";
    const SQL_TABLE_CORE_SERVICES = "core_service";
    const SQL_TABLE_CORE_ACCOUNTS = "core_account";
    private $conn;


    public function __construct()
    {
        $this->conn = dbAdapter::getPDOMasterCore();
    }

    public function killDbConnection()
    {
        $this->conn = null;
    }

    public function getAllAdministratorIds()
    {
        //prepare the query
        $stmt = $this->conn->prepare('SELECT user_id
                    FROM core.core_user
                    WHERE user_system_admin = 1');

        //set the fetch mode
        $stmt->setFetchMode(PDO::FETCH_ASSOC);

        //execute it
        $stmt->execute();

        //fetch the data
        $admin_ids = $stmt->fetchAll();

        return $admin_ids;
    }

    public function getAllUsersAdmin($page, $limit)
    {
        //determine the limit and offset from the pager
        if (!isset($page)) {
            $limit = 25;
            $page = 1;
        }
        $offset = $limit * ($page - 1);

        //prepare the query
        $stmt = $this->conn->prepare('SELECT u.user_id, u.user_username, a.firstname, a.lastname, a.companyname, a.cellnumber, a.emailaddress
                    FROM core.core_user u
                    LEFT JOIN '.self::SQL_TABLE_CORE_USER_DETAILS.' a
                    ON u.user_id = a.core_user_id
                    GROUP BY u.user_username
                    ORDER BY u.user_username ASC LIMIT :offset, :limit');

        $stmt->bindValue(':limit', (int) $limit, PDO::PARAM_INT);
        $stmt->bindValue(':offset', (int) $offset, PDO::PARAM_INT);

        //set the fetch mode
        $stmt->setFetchMode(PDO::FETCH_ASSOC);

        //execute it
        $stmt->execute();

        //fetch the data
        $services = $stmt->fetchAll();

        return $services;
    }

    public function getAllUsersAdminSearch($page, $limit, $search_type, $search_term)
    {
        //determine the limit and offset from the pager
        if (!isset($page)) {
            $limit = 25;
            $page = 1;
        }
        $offset = $limit * ($page - 1);

        if($search_type == "" || $search_type == "all")
        {
            $where_clause = ' u.user_id LIKE \'%' . $search_term . '%\' OR u.user_username LIKE \'%' . $search_term . '%\' ';
        }
        else if($search_type == "user_id")
        {
            $where_clause = ' u.user_id = ' . $search_term . ' ';
        }
        else if($search_type == "user_username")
        {
            $where_clause = ' u.user_username LIKE \'%' . $search_term . '%\' ';
        }

        //prepare the query
        $stmt = $this->conn->prepare('SELECT u.user_id, u.user_username, a.firstname, a.lastname, a.companyname, a.cellnumber, a.emailaddress
                    FROM core.core_user u
                    LEFT JOIN '.self::SQL_TABLE_CORE_USER_DETAILS.' a
                    ON u.user_id = a.core_user_id
                    WHERE '.$where_clause.'
                    GROUP BY u.user_username
                    ORDER BY u.user_username ASC LIMIT :offset, :limit');

        $stmt->bindValue(':limit', (int) $limit, PDO::PARAM_INT);
        $stmt->bindValue(':offset', (int) $offset, PDO::PARAM_INT);

        //set the fetch mode
        $stmt->setFetchMode(PDO::FETCH_ASSOC);

        //execute it
        $stmt->execute();

        //fetch the data
        $services = $stmt->fetchAll();

        return $services;
    }




    /**
     * This function returns a user record from core_user using the user id
     * @param int $user_id - The ID of the user
     * @return Array $user_account - An associative array of the user record
     */
    function getUser($user_id)
    {
        //prepare the data
        $data = array(':user_id' => $user_id);

        //prepare the query
        $stmt = $this->conn->prepare('SELECT *
                            FROM '.self::SQL_TABLE_CORE_USER.'
                            WHERE user_id = :user_id ');

        //set the fetch mode
        $stmt->setFetchMode(PDO::FETCH_ASSOC);

        //execute it
        $stmt->execute($data);

        //fetch the data
        $user_account = $stmt->fetch();

        return $user_account;
    }

    /**
     * Collects all service IDs and service names that a user has permissions on
     * @param $user_id
     *
     * @return Array $service_list
     */
    function getUserServices($user_id)
    {
        //prepare the data
        $data = array(':user_id' => $user_id);

        //prepare the query
        $stmt = $this->conn->prepare('SELECT p.service_id, s.service_name, s.service_login, p.permission_type, p.permission_created, p.permission_status, a.account_name
                            FROM '.self::SQL_TABLE_CORE_USER_PERMISSION.' p
                            INNER JOIN '.self::SQL_TABLE_CORE_SERVICES.' s
                            ON p.service_id = s.service_id
                            INNER JOIN '.self::SQL_TABLE_CORE_ACCOUNTS.' a
                            ON s.account_id = a.account_id
                            WHERE user_id = :user_id
                            GROUP BY p.service_id');

        //set the fetch mode
        $stmt->setFetchMode(PDO::FETCH_ASSOC);

        //execute it
        $stmt->execute($data);

        //fetch the data
        $service_list = $stmt->fetchAll();

        return $service_list;
    }

    /**
     * Collects all service IDs and service names that a user has permissions on
     * @param $user_id
     *
     * @return Array $service_list
     */
    function getUserDefaultServiceInfo($user_id)
    {
        //prepare the data
        $data = array(':user_id' => $user_id);

        //prepare the query
        $stmt = $this->conn->prepare('SELECT u.user_default_service_id, s.service_id, s.service_name
                            FROM '.self::SQL_TABLE_CORE_USER.' u
                            INNER JOIN '.self::SQL_TABLE_CORE_SERVICES.' s
                            ON u.user_default_service_id = s.service_id
                            WHERE user_id = :user_id
                            GROUP BY u.user_default_service_id');

        //set the fetch mode
        $stmt->setFetchMode(PDO::FETCH_ASSOC);

        //execute it
        $stmt->execute($data);

        //fetch the data
        $service = $stmt->fetch();

        return $service;
    }

    /**
     * Gets athe main account data for a particular user
     * @param $user_id
     *
     * @return Array $account_info
     */
    function getUserMainAccount($user_id)
    {
        //prepare the data
        $data = array(':user_id' => $user_id);

        //prepare the query
        $stmt = $this->conn->prepare('SELECT u.user_id, a.account_id, a.account_name, a.account_status, a.account_created
                            FROM '.self::SQL_TABLE_CORE_USER.' u
                            INNER JOIN '.self::SQL_TABLE_CORE_ACCOUNTS.' a
                            ON u.user_account_id = a.account_id
                            WHERE user_id = :user_id');

        //set the fetch mode
        $stmt->setFetchMode(PDO::FETCH_ASSOC);

        //execute it
        $stmt->execute($data);

        //fetch the data
        $account = $stmt->fetch();

        return $account;
    }

    /**
     * GETS the total batches sent by a user on each service
     * @param $user_id
     * @return Array $batch_activity - 'total', 'service_id', 'service_name'
     */
    function getBatchActivity($user_id)
    {
        //prepare the data
        $data = array(':user_id' => $user_id);

        //prepare the query
        $stmt = $this->conn->prepare('SELECT COUNT(*) as "total", b.service_id, s.service_name
                            FROM `batch_batch` b
                            LEFT JOIN '.self::SQL_TABLE_CORE_SERVICES .' s
                            ON b.service_id = s.service_id
                            WHERE user_id = :user_id GROUP BY service_id');

        //set the fetch mode
        $stmt->setFetchMode(PDO::FETCH_ASSOC);

        //execute it
        $stmt->execute($data);

        //fetch the data
        $batch_activity = $stmt->fetchAll();

        return $batch_activity;
    }

    /**
     * GETS the total batches sent by a user on each service
     * @param $user_id
     * @return Array $batch_activity - 'total', 'service_id', 'service_name'
     */
    function getPaymentActivity($user_id)
    {
        //prepare the data
        $data = array(':user_id' => $user_id);

        //prepare the query
        $stmt = $this->conn->prepare('SELECT COUNT(*) as "total", p.service_id, s.service_name
                            FROM `core_service_payment` p
                            LEFT JOIN '.self::SQL_TABLE_CORE_SERVICES .' s
                            ON p.service_id = s.service_id
                            WHERE user_id = :user_id GROUP BY service_id');

        //set the fetch mode
        $stmt->setFetchMode(PDO::FETCH_ASSOC);

        //execute it
        $stmt->execute($data);

        //fetch the data
        $batch_activity = $stmt->fetchAll();

        return $batch_activity;
    }

    /**
     * Counts the amount of batches that a user has made
     * @param $user_id
     * @return int Total number of batches
     */
    function getBatchCount($user_id)
    {
        //prepare the data
        $data = array(':user_id' => $user_id);

        //prepare the query
        $stmt = $this->conn->prepare("SELECT COUNT(*)
            FROM core.batch_batch
            WHERE user_id = :user_id");

        //set the fetch mode
        $stmt->setFetchMode(PDO::FETCH_ASSOC);

        //execute it
        $stmt->execute($data);

        //fetch the data
        return $stmt->fetchColumn();
    }

    /**
     * Counts the amount of credit transactions
     * @param $user_id
     * @return int Total number of batches
     */
    function getPaymentCount($user_id)
    {
        //prepare the data
        $data = array(':user_id' => $user_id);

        //prepare the query
        $stmt = $this->conn->prepare("SELECT COUNT(*)
            FROM core.core_service_payment
            WHERE user_id = :user_id");

        //set the fetch mode
        $stmt->setFetchMode(PDO::FETCH_ASSOC);

        //execute it
        $stmt->execute($data);

        //fetch the data
        return $stmt->fetchColumn();
    }

    /**
     * Counts the amount of credit transactions
     * @param $user_id
     * @return int Total number of batches
     */
    function getServiceCountAsManager($user_id)
    {
        //prepare the data
        $data = array(':user_id' => $user_id);

        //prepare the query
        $stmt = $this->conn->prepare("SELECT COUNT(*)
            FROM core.core_service
            WHERE service_manager_user_id = :user_id");

        //set the fetch mode
        $stmt->setFetchMode(PDO::FETCH_ASSOC);

        //execute it
        $stmt->execute($data);

        //fetch the data
        return $stmt->fetchColumn();
    }



    /**
     * Get all the roles for this user.
     * @param $user_id
     * @return array
     */
    function getUserRoles($user_id)
    {
        //prepare the data
        $data = array(':user_id' => $user_id);

        //prepare the query
        $stmt = $this->conn->prepare('SELECT r.role_id, role_name, role_url, role_icon, role_type
                FROM '.self::SQL_TABLE_CORE_USER_ROLE.' r
                INNER JOIN core.core_role rl
                ON r.role_id = rl.role_id
                WHERE r.user_id = :user_id
                ORDER BY role_order');

        //set the fetch mode
        $stmt->setFetchMode(PDO::FETCH_ASSOC);

        //execute it
        $stmt->execute($data);

        //fetch the data
        $roles_list = $stmt->fetchAll();

        return $roles_list;
    }

    /**
     * Gets a role by the role ID
     * @param $role_id
     * @return array
     */
    function getUserRole($role_id)
    {
        //prepare the data
        $data = array(':role_id' => $role_id);

        //prepare the query
        $stmt = $this->conn->prepare('SELECT role_id, role_name, role_url, role_icon, role_type
                FROM '.self::SQL_TABLE_CORE_USER_ROLE.'
                WHERE role_id = :role_id');

        //set the fetch mode
        $stmt->setFetchMode(PDO::FETCH_ASSOC);

        //execute it
        $stmt->execute($data);

        //fetch the data
        $role = $stmt->fetch();

        return $role;
    }

    /**
     * This function returns a user account details record from account_details using the user id
     * @param int $user_id - The ID of the user
     * @return Array $user_account_details - An associative array of the user details record
     */
    function getUserAccountDetails($user_id) {

        //prepare the data
        $data = array(':user_id' => $user_id);

        //prepare the query
        $stmt_get_deets = $this->conn->prepare('SELECT *
                            FROM '.self::SQL_TABLE_CORE_USER_DETAILS.'
                            WHERE core_user_id = :user_id ');

        //set the fetch mode
        $stmt_get_deets->setFetchMode(PDO::FETCH_ASSOC);

        //execute it
        $stmt_get_deets->execute($data);

        //fetch the data
        $user_account_details = $stmt_get_deets->fetch();

        return $user_account_details;
    }

    /**
     * Creates or edits an account details record to save the details of a user
     *
     * @param $account_details_id
     * @param $user_id
     * @param $account_id
     * @param $service_id
     * @param $first_name
     * @param $last_name
     * @param $company_name
     * @param $email_address
     * @param $cell_number
     * @return string
     */
    function saveUserAccountDetails($account_details_id, $user_id, $account_id, $service_id, $first_name, $last_name, $company_name, $email_address, $cell_number)
    {
        if (!isset($account_details_id) || $account_details_id == -1)
        {
            //Doesn't exist, prepare an insert statement
            $data_details = array(':user_id' => $user_id, ':firstname' => $first_name, ':lastname' => $last_name, ':companyname' => $company_name, ':emailaddress' => $email_address,
                ':cellnumber' => $cell_number, ':tutorial_seen' => 1, ':verified' => 1, ':account_id' => $account_id, ':service_id' => $service_id); //tutorial seen and verified on by default because this must be an old account

            //prepare the query
            $stmt_details = $this->conn->prepare('INSERT INTO '.self::SQL_TABLE_CORE_USER_DETAILS.' (core_user_id, firstname, lastname, companyname, emailaddress, cellnumber, tutorial_seen, verified, account_id, service_id)
                    VALUES (:user_id, :firstname, :lastname, :companyname, :emailaddress, :cellnumber, :tutorial_seen, :verified, :account_id, :service_id) ');
        }
        else
        {
            //Does exist, prepare an UPDATE statement
            $data_details = array(':account_details_id' => $account_details_id, ':firstname' => $first_name, ':lastname' => $last_name, ':companyname' => $company_name, ':emailaddress' => $email_address,
                ':cellnumber' => $cell_number);

            //prepare the query
            $stmt_details = $this->conn->prepare('UPDATE '.self::SQL_TABLE_CORE_USER_DETAILS.' SET firstname = :firstname, lastname = :lastname, companyname = :companyname, emailaddress = :emailaddress, cellnumber = :cellnumber
                    WHERE account_details_id = :account_details_id ');

        }

        //execute it
        if($stmt_details->execute($data_details))
        {
            if (!isset($account_details_id) || $account_details_id == -1)
            {
                $account_details_id = $this->conn->lastInsertId();
            }
        }

        //now save the nae in the user field
        if((isset($first_name) && $first_name != "") || (isset($last_name) && $last_name != ""))
        {
            $data_user = array(':user_id' => $user_id, ':user_fullname' => $first_name.' '.$last_name);

            //prepare the query
            $stmt_user = $this->conn->prepare('UPDATE '.self::SQL_TABLE_CORE_USER.' SET user_fullname = :user_fullname WHERE user_id = :user_id ');

            $stmt_user->execute($data_user);
        }

        return $account_details_id;
    }

    function saveNewHashedPassword($user_id, $hashed_password)
    {
        //prepare the data
        $data_update_password = array(':user_id' => $user_id, ':hashed_password' => $hashed_password);

        //prepare the query
        $stmt_update_password = $this->conn->prepare("UPDATE core_user SET user_password_hashed = :hashed_password WHERE user_id = :user_id ");

        //execute it
        return $stmt_update_password->execute($data_update_password);
    }

    function addRoleToUser($user_id, $role_id)
    {
        //prepare the data
        $data_role = array(':user_id' => $user_id, ':role_id' => $role_id);

        //prepare the query
        $stmt_role = $this->conn->prepare('INSERT INTO '.self::SQL_TABLE_CORE_USER_ROLE.' (user_id, role_id) VALUES (:user_id, :role_id)');

        //execute it
        return $stmt_role->execute($data_role);
    }

    function removeRoleFromUser($user_id, $role_id)
    {
        //prepare the data
        $data_role = array(':user_id' => $user_id, ':role_id' => $role_id);

        //prepare the query
        $stmt_role = $this->conn->prepare('DELETE FROM '.self::SQL_TABLE_CORE_USER_ROLE.' WHERE user_id = :user_id AND role_id = :role_id');

        //execute it
        return $stmt_role->execute($data_role);
    }


    /**
     * This function gets all roles from the rolle table, simple dude
     * @return array
     */
    function getAllRoles()
    {
        //prepare the query
        $stmt = $this->conn->prepare('SELECT * FROM core.core_role;');

        //set the fetch mode
        $stmt->setFetchMode(PDO::FETCH_ASSOC);

        //execute it
        $stmt->execute();

        //fetch the data
        $allRoles = $stmt->fetchAll();

        return $allRoles;
    }

    /**
     * This function gets all roles from the rolle table, simple dude
     * @param $role_type - Valid types are "client", "reseller", "admin"
     * @return array
     */
    function getAllRolesByType($role_type)
    {
        //prepare the data
        $data = array(':role_type' => '%'.$role_type.'%' );

        //prepare the query
        $stmt = $this->conn->prepare('SELECT * FROM core.core_role WHERE LOWER(role_type) LIKE :role_type;');

        //set the fetch mode
        $stmt->setFetchMode(PDO::FETCH_ASSOC);

        //execute it
        $stmt->execute($data);

        //fetch the data
        $allRoles = $stmt->fetchAll();

        return $allRoles;
    }

    /**
     * Checks if a user has access to a service.
     * @param $service_id
     * @param $user_id
     * @return array
     */
    function checkUserHasAccessToService($service_id, $user_id) {

        //prepare the data
        $data = array(':service_id' => $service_id, ':user_id' => $user_id);

        //prepare the query
        $stmt = $this->conn->prepare('SELECT COUNT(*)
            FROM '.self::SQL_TABLE_CORE_USER_PERMISSION.'
            WHERE service_id = :service_id AND user_id = :user_id');

        //set the fetch mode
        $stmt->setFetchMode(PDO::FETCH_ASSOC);

        //execute it
        $stmt->execute($data);

        //fetch the data
        $count = $stmt->fetchColumn();

        if($count == 0)
        {
            return false;
        }
        else
        {
            return true;
        }
    }

    /**
     * Checks if a user has access to a service.
     * @param $service_id
     * @param $user_id
     * @return array
     */
    function checkIsAPIUser($user_name) {

        //prepare the data
        $data = array(':service_id' => $service_id, ':user_id' => $user_id);

        //prepare the query
        $stmt = $this->conn->prepare('SELECT COUNT(*)
            FROM '.self::SQL_TABLE_CORE_USER_PERMISSION.'
            WHERE service_id = :service_id AND user_id = :user_id');

        //set the fetch mode
        $stmt->setFetchMode(PDO::FETCH_ASSOC);

        //execute it
        $stmt->execute($data);

        //fetch the data
        $count = $stmt->fetchColumn();

        if($count == 0)
        {
            return false;
        }
        else
        {
            return true;
        }
    }

    /**
     * Does as it says, checks if a user name exists and returns an appropriate boolean value
     * @param $user_name - THe name of the user to check
     * @param $user_id_to_ignore - The user ID that should not be included in the duplicate search
     * @return int
     */
    function isUserNameUnique($user_name, $user_id_to_ignore = null)
    {
        if(isset($service_id_to_ignore))
        {
            //prepare the data
            $data = array(':user_id' => $user_id_to_ignore, ':user_username' => $user_name);

            //prepare the query
            $stmt = $this->conn->prepare('SELECT count(*) FROM '.self::SQL_TABLE_CORE_USER.' WHERE user_username = :user_username AND user_id != :user_id');
        }
        else
        {
            //prepare the data
            $data = array(':user_username' => $user_name);

            //prepare the query
            $stmt = $this->conn->prepare('SELECT count(*) FROM '.self::SQL_TABLE_CORE_USER.' WHERE user_username = :user_username');
        }

        //execute it
        $stmt->execute($data);

        //fetch the data
        $number_of_rows = $stmt->fetchColumn();

        if ($number_of_rows == 0)
        {
            return true; //it is unique
        }
        else
        {
            return false;
        }
    }

    /**
     * This basic function saves a value to a particular column in the users table
     * @param $user_id
     * @param $field_name
     * @param $field_value
     * @return bool success
     */
    public function saveUserFieldValue($user_id, $field_name, $field_value = null)
    {
        //prepare the query
        $stmt = $this->conn->prepare("UPDATE ".self::SQL_TABLE_CORE_USER." SET ".$field_name." = :field_value WHERE user_id = :user_id");

        $stmt->bindValue(':user_id', $user_id, PDO::PARAM_INT);
        if($field_value === null)
        {
            $stmt->bindValue(':field_value', null, PDO::PARAM_INT);
        }
        else
        {
            $stmt->bindValue(':field_value', $field_value);
        }

        //execute it
        return $stmt->execute();
    }


    function addServicePermissionToUser($user_id, $service_id, $permission_type)
    {
        //prepare the data
        $data = array(':service_id' => $service_id, ':user_id' => $user_id, ':permission_type' => $permission_type);

        //prepare the query
        $stmt = $this->conn->prepare('INSERT INTO '.self::SQL_TABLE_CORE_USER_PERMISSION.' (`service_id`, `user_id`, `permission_type`) VALUES (:service_id, :user_id, :permission_type)');

        //execute it
        return $stmt->execute($data);
    }

    function removeServicePermissionFromUser($user_id, $service_id)
    {
        //prepare the data
        $data_role = array(':user_id' => $user_id, ':service_id' => $service_id);

        //prepare the query
        $stmt_role = $this->conn->prepare('DELETE FROM '.self::SQL_TABLE_CORE_USER_PERMISSION.' WHERE user_id = :user_id AND service_id = :service_id');

        //execute it
        return $stmt_role->execute($data_role);
    }

    function deleteUser($user_id)
    {
        //prepare the data
        $data_user = array(':user_id' => $user_id);

        //prepare the query
        $stmt_del_role = $this->conn->prepare('DELETE FROM '.self::SQL_TABLE_CORE_USER_ROLE.' WHERE user_id = :user_id');

        //execute it
        $stmt_del_role->execute($data_user);

        //prepare the query
        $stmt_del_permission = $this->conn->prepare('DELETE FROM '.self::SQL_TABLE_CORE_USER_PERMISSION.' WHERE user_id = :user_id');

        //execute it
        $stmt_del_permission->execute($data_user);

        //prepare the query
        $stmt_del_user_details = $this->conn->prepare('DELETE FROM '.self::SQL_TABLE_CORE_USER_DETAILS.' WHERE core_user_id = :user_id');

        //execute it
        $stmt_del_user_details->execute($data_user);

        //prepare the query
        $stmt_del_user = $this->conn->prepare('DELETE FROM '.self::SQL_TABLE_CORE_USER.' WHERE user_id = :user_id');

        //execute it
        return $stmt_del_user->execute($data_user);
    }

    /**
     * Creates a new API user for a service
     * @param $user_username
     * @param $user_password
     * @param $account_id
     * @param $service_id
     * @return string
     */
    function createNewServiceAPIUser($user_username, $user_password, $account_id, $service_id)
    {
        //new, HASH THAT PASSWORD
        $hashed_password = helperPassword::createHashedPassword($user_password);

        //prepare the data
        $data = array(':user_username' => $user_username, ':user_password' => $user_password,
            ':user_account_id' => $account_id, ':user_default_service_id' => $service_id, ':user_password_hashed' => $hashed_password);

        //prepare the query
        $stmt = $this->conn->prepare('INSERT INTO core.core_user (user_username, user_password, user_account_id, user_default_service_id, user_password_hashed)
                    VALUES (:user_username, :user_password, :user_account_id, :user_default_service_id, :user_password_hashed) ');

        //execute it
        $stmt->execute($data);

        //return the insert ID
        return $this->conn->lastInsertId('user_id');
    }

    /**
     * Creates a new API user for a service
     * @param $account_id
     * @param $service_id
     * @param $user_username
     * @param $user_password
     * @param $user_first_name
     * @param $user_last_name
     * @param $user_system_admin
     * @return int the new user ID
     */
    function createNewUser($service_id, $account_id, $user_username, $user_password, $user_first_name, $user_last_name, $user_system_admin)
    {
        //new, HASH THAT PASSWORD
        $hashed_password = helperPassword::createHashedPassword($user_password);

        //prepare the data
        $data = array(':user_account_id' => $account_id, ':user_default_service_id' => $service_id, ':user_username' => $user_username, ':user_password' => $user_password,
            ':user_password_hashed' => $hashed_password, ':user_system_admin' => $user_system_admin, ':user_fullname' => $user_first_name.' '.$user_last_name);

        //prepare the query
        $stmt = $this->conn->prepare('INSERT INTO core.core_user (user_username, user_password, user_account_id, user_default_service_id, user_password_hashed, user_system_admin, user_fullname)
                    VALUES (:user_username, :user_password, :user_account_id, :user_default_service_id, :user_password_hashed, :user_system_admin, :user_fullname) ');

        //execute it
        $stmt->execute($data);

        //return the insert ID
        return $this->conn->lastInsertId('user_id');
    }
}