<?php

ini_set('display_errors', 1);

//--------------------------------------------------------------
// Constants
//--------------------------------------------------------------
//define('ERROR_REPORTING', (array_search('debug', $argv) !== false));

//--------------------------------------------------------------
// Includes
//--------------------------------------------------------------
require_once('ConnetAutoloader.php');

clog::setLogFilename("/var/log/connet/plugin/" . basename(__FILE__, '.php') . '.log');
clog::setBacktrace();
clog::setVerbose(ERROR_REPORTING);

/**
 * This model handles all tasks based on Account level information.
 *
 * @author Doug Jenkinson
 */

class Account
{
	const ACCOUNT_MAIN_REAL_PATH_FROM_HERE = "../img/skinlogos/";
	const ACCOUNT_MAIN_RELATIVE_PATH = "/img/skinlogos/";

	//--------------------------------
	// DATA VARS FOR OUR ACCOUNTS
	//--------------------------------
	public $helper_accounts;

	public $account_id;
	public $account_name;
	public $account_status;
	public $account_created;
	public $parent_account_id;
	public $parent_account_name;

	//--------------------------------
	// ACCOUNT SETTINGS VARS
	//--------------------------------

	public $account_settings_id;
	public $url_prefix;
	public $url_suffix;
	public $header_logo;
	public $login_logo;
	public $tab_logo;
	public $account_colour;
	public $email_header;
	public $email_footer;
	public $email_from;


	public $account_services_simple;
	public $account_service_objects;

	public $account_users_simple;
	public $account_user_objects;

	/**
	 * Account constructor.
	 * @param $account_id - The account ID, if isset, all the account info will automatically be loaded, else if it is null, no data is loaded, but can be assigned and saved.
	 */
	public function __construct($account_id)
	{
		$this->helper_accounts = new sqlHelperAccounts();

		if(isset($account_id))
		{
			$this->account_id = $account_id;

			$account_data = $this->helper_accounts->getAccount($account_id);

			$this->account_name = $account_data['account_name'];
			$this->account_status = $account_data['account_status'];
			$this->account_created = $account_data['account_created'];
			$this->parent_account_id = $account_data['parent_account_id'];
			$this->parent_account_name = $this->helper_accounts->getAccountName($this->parent_account_id);

			$this->buildAccountSettings();
			$this->buildAccountServices();
			$this->buildAccountUsers();
		}
	}

	/**
	 * ASSEMBLES ALL ACCOUNT SETTINGS INFOR FOR THIS SERVICE
	 */
	private function buildAccountSettings()
	{
		$account_settings = $this->helper_accounts->getAccountSettings($this->account_id);

		if(isset($account_settings))
		{
			$this->account_settings_id = $account_settings['account_settings_id'];
			$this->url_prefix = $account_settings['url_prefix'];
			$this->url_suffix = $account_settings['url_suffix'];
			$this->header_logo = $account_settings['header_logo'];
			$this->login_logo = $account_settings['login_logo'];
			$this->tab_logo = $account_settings['tab_logo'];
			$this->account_colour = $account_settings['account_colour'];
			$this->email_header = $account_settings['email_header'];
			$this->email_footer = $account_settings['email_footer'];
			$this->email_from = $account_settings['email_from'];
		}
	}

	/**
	 * GET ALL SERVICE NAMES AND IDs THAT BELONG TO THIS ACCOUNT
	 */
	private function buildAccountServices()
	{
		$this->account_services_simple = $this->helper_accounts->getAccoutServices($this->account_id);
	}

	/**
	 * GET ALL SERVICE NAMES AND IDs THAT BELONG TO THIS ACCOUNT
	 */
	private function buildAccountUsers()
	{
		$this->account_users_simple = $this->helper_accounts->getAccountUsers($this->account_id);
	}

	/**
	 * Get all services as SERVICE objects, not run by default to save on processing when initializing the Account object
	 */
	public function getAllAccountServicesAsObjects()
	{
		if(!isset($this->account_services_simple))
		{
			$this->buildAccountServices();
		}

		$this->account_service_objects = array();
		foreach($this->account_services_simple as $service_simple)
		{
			$service_obj = new Service($service_simple['service_id']);

			array_push($this->account_service_objects, $service_obj);
		}

		return $this->account_service_objects;
	}

	/**
	 * Get all Users as USER objects, not run by default to save on processing when initializing the Account object
	 */
	public function getAllAccountUsersAsObjects()
	{
		if(!isset($this->account_users_simple))
		{
			$this->buildAccountUsers();
		}

		$this->account_user_objects = array();
		foreach($this->account_users_simple as $user_simple)
		{
			$user_obj = new User($user_simple['user_id']);

			array_push($this->account_user_objects, $user_obj);
		}

		return $this->account_user_objects;
	}

	/****************************************************
	 * LOGOS AND SETTINGS HANDLING
	 ****************************************************/

	/**
	 * Takes a $_FILES object form a form, and the filed name, and properly moves the logos into the correct file locations and saves the path
	 * @param $field_name
	 * @param $image_file_upload_obj
	 *
	 * @return Boolean success
	 */
	public function moveAndSaveLogo($field_name, $image_file_upload_obj)
	{
		//build and move the file into it's new location

		$target_dir = self::ACCOUNT_MAIN_REAL_PATH_FROM_HERE . $this->account_id."/";
		if (!is_dir($target_dir))
		{
			mkdir($target_dir, 0777);
		}


		$new_file_name = $this->account_id . "_" . $field_name . "." . pathinfo($image_file_upload_obj["name"], PATHINFO_EXTENSION);
		$real_path = $target_dir . $new_file_name;
		if(move_uploaded_file($image_file_upload_obj["tmp_name"], $real_path))
		{
			//emails have URL paths, images have relative paths in the internal filesystem
			if($field_name == 'email_header' || $field_name == 'email_footer') {
				//save dat data mmmmmmm
				$plugin_path = "https://".$_SERVER['SERVER_NAME'].self::ACCOUNT_MAIN_RELATIVE_PATH.$this->account_id."/".$new_file_name;
			}
			else
			{
				//save dat data mmmmmmm
				$plugin_path = self::ACCOUNT_MAIN_RELATIVE_PATH.$this->account_id."/".$new_file_name;
			}


			//echo "Path: ". $plugin_path . " FIELD: " . $field_name . " ID: ".$this->account_id;
			return $this->helper_accounts->saveAccountFieldValue($this->account_id, $field_name, $plugin_path);

		}
		else
		{
			return false;
		}
	}

	/****************************************************
	 * SAVING AND DELETING THE ACCOUNT
	 ****************************************************/
	public function save()
	{
		return false;
	}


	public function delete()
	{
		return false;
	}

	/**
	 * Manufactures a new default account with just the account name.
	 * @param $account_name
	 * @return bool|int The ID of the newly created account, or false if unsuccessful.
	 */
	public static function createNewAccountAdmin($account_name)
	{
		$helper_accounts = new sqlHelperAccounts();

		$create_success = true;
		if($helper_accounts->isAccountNameUnique($account_name))
		{
			$account_id = $helper_accounts->createNewAccount($account_name);

			if(isset($account_id))
			{
				$create_success = $helper_accounts->createDefaultAccountSettings($account_id);
			}
			else
			{
				$create_success = false;
			}
		}
		else
		{
			$create_success = false;
		}

		if($create_success)
		{
			return $account_id;
		}
		else
		{
			return false;
		}
	}
}
