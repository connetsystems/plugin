<?php
/**
 * This model handles all tasks based on MTSMS objects
 *
 * @author Doug Jenkinson
 */

class Reseller
{

	public function __construct(){}

	/**
	 * Returns an array of objects that represents a MTSMS elasticsearch document, that you can consume.
	 *
	 * @param String $batch_id - The ID of the batch we are filtering by
	 *
	 * @return ElasticsearchPluginObj $mtsms_obj - An ElasticsearchPluginObj that has all keys defined as this document.
	 */
	public static function getFullNetworkSendStats($reseller_id)
	{
		//do some initial setup for values
		$batch_stats = array();
		$batch_stats['service_id'] = $reseller_id;
		$batch_stats['sent'] = -1;
		$batch_stats['units'] = -1;
		$batch_stats['pending'] = -1;
		$batch_stats['delivered'] = -1;
		$batch_stats['failed'] = -1;
		$batch_stats['rejected'] = -1;
		$batch_stats['blacklisted'] = -1;
		$batch_stats['mo_count'] = -1;

		//get status count of messages
		$filter_array = array('meta.batch_id' => $reseller_id);

		$batch_stats['sent'] = ElasticsearchPluginObject::count(CONFIG_PLUG_MTSMS_TYPE, $filter_array);

		$aggregation_keys = array('terms' => 'dlr', 'sum' => 'meta.parts');

		$aggregations = ElasticsearchPluginObject::getKeyUniqueAggregations(CONFIG_PLUG_MTSMS_TYPE, $aggregation_keys, $filter_array);

		$batch_stats['units'] = $aggregations['meta.parts'];

		//echo json_encode($aggregations['dlr']);
		//we need to process each aggregated status to see what it is!
		foreach($aggregations['dlr'] as $status_key => $status_value)
		{
			if(($status_key & 32) == 32 && ($status_key & 1) == 0 && ($status_key & 2) == 0 && ($status_key & 16) == 0)
			{
				//pending
				$batch_stats['pending'] = $status_value;
			}

			if(($status_key & 1) == 1)
			{
				//delivered
				$batch_stats['delivered'] = $status_value;
			}
			elseif(($status_key & 2) == 2)
			{
				//failed
				$batch_stats['failed'] = $status_value;
			}

			if(($status_key & 16) == 16)
			{
				//rejected
				$batch_stats['rejected'] = $status_value;
			}
		}

		//get all the MOs receieved for this batch
		$batch_stats['mo_count'] = MOSMS::getMOCountInBatch($batch_id);

		return $batch_stats;
	}


}
