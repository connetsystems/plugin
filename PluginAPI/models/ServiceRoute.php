<?php

ini_set('display_errors', 1);

//--------------------------------------------------------------
// Constants
//--------------------------------------------------------------
//define('ERROR_REPORTING', (array_search('debug', $argv) !== false));

//--------------------------------------------------------------
// Includes
//--------------------------------------------------------------
require_once('ConnetAutoloader.php');

clog::setLogFilename("/var/log/connet/plugin/" . basename(__FILE__, '.php') . '.log');
clog::setBacktrace();
clog::setVerbose(ERROR_REPORTING);

/**
 * This model handles all tasks based on Service Route level information.
 *
 * @author Doug Jenkinson
 */

class ServiceRoute
{

	//--------------------------------
	// DATA VARS FOR OUR USERS
	//--------------------------------
	public $route_id;
	public $provider_id;
	public $service_id;
	public $route_msg_type;
	public $route_description;
	public $route_display_name;
	public $route_code;
	public $route_code_range;
	public $route_status;
	public $route_match_regex;
	public $route_match_priority;
	public $route_meta;
	public $route_billing;
	public $route_created;
	public $route_mccmnc;
	public $route_collection_id;
	public $route_currency;

	public $helper_routes;

	/**
	 * Route constructor.
	 * @param $r_id - The routes ID, if isset, all the user info will automatically be loaded, else if it is null, no data is loaded, but can be assigned and saved.
	 */
	public function __construct($r_id = null)
	{
		$this->helper_routes = new sqlHelperRoutes();

		if(isset($r_id))
		{
			$this->route_id = $r_id;

			if($r_id != -1)
			{
				//build the route object!
				$route_data = $this->helper_routes->getRouteData($r_id);

				$this->service_id = $route_data['service_id'];
				$this->provider_id = $route_data['provider_id'];
				$this->route_msg_type = $route_data['route_msg_type'];
				$this->route_description = $route_data['route_description'];
				$this->route_collection_id = $route_data['route_collection_id'];
				$this->route_currency = $route_data['route_currency'];
				$this->route_mccmnc = $route_data['route_mccmnc'];
				$this->route_match_regex = $route_data['route_match_regex'];
				$this->route_status = $route_data['route_status'];
				$this->route_billing = $route_data['route_billing'];
				$this->route_match_priority = $route_data['route_match_priority'];

			}
		}
		else
		{
			$this->route_id = -1;
		}
	}

	/**
	 * This function specially sets the Route match regex along with the normal regex
	 * @param $mccmnc
	 */
	public function buildRouteMCCMNCFromCode($mccmnc)
	{
		$this->route_mccmnc = $mccmnc;
		$this->route_match_regex = "/\|mtsms\|" . $mccmnc . "\|.*/";
	}

	/**
	 * This function specially sets the ROute match regex along with the normal regex
	 * @param $rate
	 * @param $rdnc
	 */
	public function setRouteBilling($rate, $rdnc)
	{
		$this->route_billing = $rate.',0,0,0,'.$rdnc;
	}

	/**
	 * Custom function to specially build a new route object from a form
	 * @param $service_id
	 * @param $country_name
	 * @param $network_mccmnc
	 * @param $collection_id
	 * @param $rate
	 * @param $rdnc
	 * @param $currency
	 * @param $status
	 */
	public function setRouteDataFromForm($service_id, $country_name, $network_mccmnc, $collection_id, $rate, $rdnc, $currency, $status)
	{
		$this->service_id = $service_id;
		$this->provider_id = 42;
		$this->route_msg_type = "MTSMS";
		if(!isset($this->route_description) || $this->route_description == "")
		{
			$this->route_description = $country_name. " (".$network_mccmnc.")";
		}
		$this->route_collection_id = $collection_id;
		$this->route_currency = $currency;
		$this->buildRouteMCCMNCFromCode($network_mccmnc);
		$this->route_status = $status;
		$this->setRouteBilling($rate, $rdnc);
		$this->route_match_priority = 50;
	}

	public function save()
	{
		$save_success = false;
		if($this->route_id == -1)
		{
			if($this->helper_routes->checkServiceRouteExists($this->service_id, $this->route_mccmnc) === false)
			{
				$this->route_id = $this->helper_routes->saveNewRoute($this->service_id, $this->provider_id, $this->route_msg_type, $this->route_description,
					$this->route_collection_id, $this->route_currency, $this->route_mccmnc, $this->route_match_regex, $this->route_status, $this->route_billing,
					$this->route_match_priority);

				//check that saving was successful
				if(isset($this->route_id) && $this->route_id != -1)
				{
					$save_success = true;
				}
			}
		}
		else
		{
			$save_success = $this->helper_routes->updateRoute($this->route_id, $this->provider_id, $this->route_msg_type, $this->route_description,
				$this->route_collection_id, $this->route_currency, $this->route_mccmnc, $this->route_match_regex, $this->route_status, $this->route_billing,
				$this->route_match_priority);
		}

		return $save_success;
	}

	public function delete()
	{
		$delete_success = false;
		if($this->route_id != -1)
		{
			$delete_success = $this->helper_routes->deleteRoute($this->route_id);
		}

		return $delete_success;
	}

}
