<?php
/**
 * This model handles all tasks based on MTSMS objects
 *
 * @author Doug Jenkinson
 */

class MOSMS
{
	public function __construct(){}

	/**
	 * Returns an object that represents a single MTSMS elasticsearch document, that you can consume.
	 *
	 * @param String $doc_id - The Elasticsearch ID of the document
	 * @return ElasticsearchPluginObj $mtsms_obj - An ElasticsearchPluginObj that has all keys defined as this document.
	 */
	public static function find($doc_id)
	{
		$mosms_obj = ElasticsearchPluginObject::find(CONFIG_PLUG_MOSMS_TYPE, $doc_id);

		return $mosms_obj;
	}

	/**
	 * Returns an object that represents a single MTSMS elasticsearch document, that you can consume.
	 *
	 * @param String $doc_id - The Elasticsearch ID of the document
	 * @return ElasticsearchPluginObj $mtsms_obj - An ElasticsearchPluginObj that has all keys defined as this document.
	 */
	public static function findByMTSMSId($doc_id)
	{
		$mosms_objs = ElasticsearchPluginObject::findByKey(CONFIG_PLUG_MOSMS_TYPE, 'meta.mtsms_id', $doc_id);

		return $mosms_objs;
	}

	/**
	 * Returns a list of MOSMS objects by querying multiple MTSMS IDs and getting all matches
	 *
	 * @param array $mtsms_ids - The array of MTSMS IDs to use to find the MOSMS ids
	 * @param int $size - The amount of records to return
	 * @return Array of ElasticsearchPluginObj $mtsms_objs - An ElasticsearchPluginObj that has all keys defined as this document.
	 */
	public static function getAllMOSMSbyMTSMSIdArray($mtsms_ids, $size)
	{

		//build the basics of our search params for Elasticsearch specifically to handle this case
		$search_params = ElasticsearchPluginObject::getBaseFormattedElasticParams(CONFIG_PLUG_MOSMS_TYPE);
		$search_params['body']['query']['filtered'] = array('query' => array('match_all' => array()),
			'filter' => array('bool' =>
				array('must' => array('terms' => array('meta.mtsms_id' => $mtsms_ids)))
			)
		);
		$search_params['size'] = $size;

		$mosms_objs = ElasticsearchPluginObject::runElasticSearchForArray(CONFIG_PLUG_MOSMS_TYPE, $search_params);

		return $mosms_objs['objects'];
	}

	/**
	 * Returns an array of objects that represents a MOSMS elasticsearch document, that you can consume.
	 *
	 * @param String $service_id - The ID of the service we are filtering by
	 * @param int $from - The start document key to return, 0 returns from the beginning
	 * @param int $size - The amount of objects to return
	 *	@param int $start_date - The start date to limit the search
	 * @param int $end_date - The end date to limit the search
	 *
	 * @return ElasticsearchPluginObj $mtsms_obj - An ElasticsearchPluginObj that has all keys defined as this document.
	 */
	public static function getAllByServiceId($service_id, $from = null, $size = null, $start_date = null, $end_date = null)
	{
		$filter_array = array('service_id' => $service_id);

		$mosms_objs = ElasticsearchPluginObject::getAllObjByFilter(CONFIG_PLUG_MOSMS_TYPE, $from, $size, $filter_array, null, $start_date, $end_date);

		return $mosms_objs;
	}

	/**
	 * Returns an array of objects that represents a MTSMS elasticsearch document, filter by a search term applied on a search column
	 *
	 * @param String $field - The field in the MOSMS object to search for
	 * @param String $search_string - The string to search for
	 * @param String $service_id - The ID of the service we are filtering by (if left null or less than 1, then it won't be filtered
	 * @param int $from - The start document key to return, 0 returns from the beginning
	 * @param int $size - The amount of objects to return
	 * @param int $start_date - The start date to limit the search
	 * @param int $end_date - The end date to limit the search
	 *
	 * @return Array(ElasticsearchPluginObj) $mtsms_obj - An array of ElasticsearchPluginObjs
	 */
	public static function searchMOSMSForArray($field, $search_string, $service_id = null, $from = null, $size = null, $start_date = null, $end_date = null)
	{
		$filter_array = array();
		if(isset($service_id) && $service_id > 0)
		{
			$filter_array = array('service_id' => $service_id);
		}

		$mosms_objs = ElasticsearchPluginObject::searchByPartialFilterMatch(CONFIG_PLUG_MOSMS_TYPE, $field, $search_string, $from, $size, $filter_array, null, null, $start_date, $end_date);
		return $mosms_objs;
	}

	/**
	 * Returns an scroll ID for a set of MOSMS results that belong to specific service ID
	 *
	 * @param String $service_id - The ID of the service we are filtering by
	 * @param int $size - The amount of objects to return
	 * @param int $start_date - The start date to limit the search
	 * @param int $end_date - The end date to limit the search
	 *
	 * @return ElasticsearchPluginObj $mtsms_obj - An ElasticsearchPluginObj that has all keys defined as this document.
	 */
	public static function getScrollIDByServiceId($service_id, $size = null, $start_date = null, $end_date = null)
	{
		$filter_array = array('service_id' => $service_id);

		$mosms_objs = ElasticsearchPluginObject::getScrollIDByFilter(CONFIG_PLUG_MOSMS_TYPE, $size, $filter_array, null, $start_date, $end_date);

		return $mosms_objs;
	}

	/**
	 * Returns an array of objects that represents a MTSMS elasticsearch document, that you can consume.
	 *
	 * @param String $scroll_id - The ID of the scroll object to return the next set of results on
	 *
	 * @return ElasticsearchPluginObj $mtsms_obj - An ElasticsearchPluginObj that has all keys defined as this document.
	 */
	public static function getMOsByScrollId($scroll_id)
	{
		$mosms_objs = ElasticsearchPluginObject::getResultsByScrollID(CONFIG_PLUG_MOSMS_TYPE, $scroll_id);

		return $mosms_objs;
	}

	/**
	 * Returns an array of objects that represents a MTSMS elasticsearch document, that you can consume.
	 * THis function is very hacky, there is no current way to link an MO to a batch, so we have to wrok through MTSMS first and gather our list,
	 * then split upp appropriately. Hopefully will be replaced by something much better when CRABI comes online.
	 *
	 * @param String $batch_id - The ID of the batch we are filtering by
	 * @param int $from - The start document key to return, 0 returns from the beginning
	 * @param int $size - The amount of objects to return
	 *
	 * @return ElasticsearchPluginObj $mtsms_obj - An ElasticsearchPluginObj that has all keys defined as this document.
	 */
	public static function getAllByBatchId($batch_id, $from = null, $size = null)
	{
		clog::info('* >>>>>>> <<<<<<<<<<<<');
		clog::info('* >>>>>>> STARTING GET BATCH <<<<<<<<<<<<');
		clog::info('* >>>>>>> <<<<<<<<<<<<');
		$mosms_arr = array();

		//need to get all the objects by batch ID
		$mtsms_size = 500; //this number will be divided by 5 automatically to cater for the shards
		$total_mtsms = 0;
		$pass_count = 1;
		$scroll_id = null;

		$keep_searching = true;
		while($keep_searching)
		{

			$mtsms_arr = MTSMS::getAllMTSMSIDArrayByBatchId($batch_id, $mtsms_size, $scroll_id);

			$scroll_id = $mtsms_arr['scroll_id'];

			clog::info('* TOTAL MTSMS HITS = '.count($mtsms_arr['ids']));

			$mosms_arr = array_merge($mosms_arr, self::getAllMOSMSbyMTSMSIdArray($mtsms_arr['ids'], $mtsms_size));;

			clog::info('* MOSMS ARRAY SIZE IS NOW === '.count($mosms_arr));

			if(count($mtsms_arr['ids']) < $mtsms_size)
			{
				$keep_searching = false;
			}

			$total_mtsms += count($mtsms_arr['ids']);

			clog::info('* ------------ PASS: '.$pass_count. ' TOTAL MTSMS SCANNED [ '.$total_mtsms.' ] -------------------------------');

			$pass_count ++;
		}

		//return the subsection requested (Hate this part, incredibly inefficient)
		if(isset($from) && isset($size))
		{
			return array_slice($mosms_arr, $from, $size);
		}
		else
		{
			return $mosms_arr;
		}
	}


	/**
	 * Returns just a count of MOs for a particular batch
	 *
	 * @param String $batch_id - The ID of the batch we are filtering by
	 * @param int $from - The start document key to return, 0 returns from the beginning
	 * @param int $size - The amount of objects to return
	 *
	 * @return ElasticsearchPluginObj $mtsms_obj - An ElasticsearchPluginObj that has all keys defined as this document.
	 */
	public static function getMOCountInBatchOldAndVerySlow($batch_id)
	{
		//$time_start = microtime(true);

		//get the elasticlient
		$elasticClient = ElasticsearchLoader::getElasticClient();

		//need to get all the objects by batch ID
		$mo_count = 0;
		$break_count = 1000; // we have to break searches up into batches of a 100 to avoid memory problems and timeouts


		//using more manual searching seeing as this is a time sensitive task
		$search_params = ElasticsearchLoader::getElasticsearchMTSMSParams();
		$search_params['search_type'] = 'scan';
		$search_params['scroll'] = '1m';
		$search_params['size'] = $break_count;
		$search_params['body']['query']['filtered'] = array('query' => array('match_all' => array()),
			'filter' => array('bool' =>
				array('must' => array(array('term' => array('meta.batch_id' => $batch_id))))
			)
		);

		//run the query to get the scroll ID
		$result = $elasticClient->search($search_params);
		$scroll_id = $result['_scroll_id'];

		//cycle through each scroll batch and get the count of MOs
		$no_more_results = false;
		while (!$no_more_results)
		{
			$mtsms_id_list = array();
			$scroll_params = array();
			$scroll_params['scroll_id'] = $scroll_id;
			$scroll_params['scroll'] = '1m';
			$result_scroll = $elasticClient->scroll($scroll_params);

			$scroll_id = $result_scroll['_scroll_id'];

			if(empty($result_scroll['hits']['hits']))
			{
				$no_more_results = true;
			}
			else {
				foreach ($result_scroll['hits']['hits'] as $doc)
				{
					array_push($mtsms_id_list, $doc['_id']);
				}
			}

			//we have to build a manual array for this one
			$query['query']['filtered'] = array('filter' => array('terms' => array('meta.mtsms_id' => $mtsms_id_list)));

			//now count all results that match the MTSMS Ids
			$mo_count += ElasticsearchPluginObject::countByManualQueryArray(CONFIG_PLUG_MOSMS_TYPE, $query);
		}

		//$time_end = microtime(true);
		//$execution_time = $time_end - $time_start;
		//echo '<hr/><b>Total Execution Time FOR MO:</b> '.$execution_time . "<hr/><br/>";

		return $mo_count;
	}

	/**
	 * Counts the amount of MOs in a batch
	 * This function is very hacky, there is no current way to link an MO to a batch, so we have to work through MTSMS first and gather our list,
	 * then split up appropriately. Hopefully will be replaced by something much better when CRABI comes online.
	 *
	 * @param String $batch_id - The ID of the batch we are filtering by
	 *
	 * @return ElasticsearchPluginObj $mtsms_obj - An ElasticsearchPluginObj that has all keys defined as this document.
	 */
	public static function getMOCountInBatchByBatchId($batch_id)
	{
		clog::info('* >>>>>>> <<<<<<<<<<<<');
		clog::info('* >>>>>>> STARTING GET MOSMS COUNT IN BATCH <<<<<<<<<<<<');
		clog::info('* >>>>>>> <<<<<<<<<<<<');
		try
		{
			//need to get all the objects by batch ID
			$mtsms_size = 10000; //this number will be divided by 5 automatically to cater for the shards
			$total_mtsms = 0;
			$total_mosms = 0;
			$pass_count = 1;
			$scroll_id = null;

			$keep_searching = true;
			while($keep_searching)
			{
				$mtsms_arr = MTSMS::getAllMTSMSIDArrayByBatchId($batch_id, $mtsms_size, $scroll_id);

				$scroll_id = $mtsms_arr['scroll_id'];

				clog::info('* TOTAL MTSMS HITS = '.$mtsms_arr['total_search_hits']);

				if(count($mtsms_arr['ids']) < $mtsms_size)
				{
					$keep_searching = false;
				}

				$total_mtsms += count($mtsms_arr['ids']);

				//we have to build a manual array for this one
				$query['query']['filtered'] = array('filter' => array('terms' => array('meta.mtsms_id' => $mtsms_arr['ids'])));

				//now count all results that match the MTSMS Ids
				$total_mosms += ElasticsearchPluginObject::countByManualQueryArray(CONFIG_PLUG_MOSMS_TYPE, $query);

				clog::info('* ----------------- PASS: '.$pass_count. ' - TOTAL MOs = '.$total_mosms.' TOTAL MTSMS SCANNED [ '.$total_mtsms.' ] -------------------------------');

				$pass_count ++;
			}

			return $total_mosms;
		}
		catch(Exception $e)
		{
			clog::info('* MOBatch MO stats error: '.$e->getMessage());
			return 0;
		}
	}

	/**
	 * Returns an array of objects that represents a MTSMS elasticsearch document, that you can consume.
	 *
	 * @param String $service_id - The ID of the service we are filtering by (if left null or less than 1, then it won't be filtered
	 * @param String $message_content - The message text to search for.
	 * @param int $from - The start document key to return, 0 returns from the beginning
	 * @param int $size - The amount of objects to return
	 *
	 * @return Array(ElasticsearchPluginObj) $mtsms_obj - An array of ElasticsearchPluginObjs
	 */
	public static function searchByMessageContent($message_content, $service_id = null, $from = null, $size = null)
	{
		$filter_array = array();
		if(isset($service_id) && $service_id > 0)
		{
			$filter_array = array('service_id' => $service_id);
		}

		$mtsms_objs = ElasticsearchPluginObject::searchByStringContent(CONFIG_PLUG_MOSMS_TYPE, 'content', $message_content, $from, $size, $filter_array);

		return $mtsms_objs['objects'];

	}



}
