<?php

ini_set('display_errors', 1);

//--------------------------------------------------------------
// Constants
//--------------------------------------------------------------
//define('ERROR_REPORTING', (array_search('debug', $argv) !== false));

//--------------------------------------------------------------
// Includes
//--------------------------------------------------------------
require_once('ConnetAutoloader.php');

clog::setLogFilename("/var/log/connet/plugin/" . basename(__FILE__, '.php') . '.log');
clog::setBacktrace();
clog::setVerbose(ERROR_REPORTING);

/**
 * This model handles all tasks based on User level information.
 *
 * Due to the nature of users, there is no join into Elasticsearch with this model at this time.
 *
 * Available holders are all the general service data, plus $this->routing_data for routes, and $this->users for associated users.
 *
 * @author Doug Jenkinson
 */

class User
{

	//--------------------------------
	// DATA VARS FOR OUR USERS
	//--------------------------------
	public $helper_users;

	public $user_id;
	public $user_username;
	public $user_password; //this needs to be phased out
	private $user_password_hashed; //private, function internally should do comparison
	public $user_status;
	public $user_created;
	public $user_default_service_id;
	public $default_service;
	public $user_system_admin;
	public $user_account_id;
	public $user_fullname;

	//--------------------------------
	// ACCOUNT DETAILS
	//--------------------------------
	public $account_details_id;
	public $first_name;
	public $last_name;
	public $email_address;
	public $cell_number;
	public $company_name;

	public $user_services;
	public $default_account;

	public $roles;
 	public $role_index;
	/**
	 * User constructor.
	 * @param $u_id - The user ID, if isset, all the user info will automatically be loaded, else if it is null, no data is loaded, but can be assigned and saved.
	 */
	public function __construct($u_id)
	{
		$this->helper_users = new sqlHelperUsers();

		if(isset($u_id))
		{
			$this->user_id = $u_id;

			$user_data = $this->helper_users->getUser($u_id);

			$this->user_username = $user_data['user_username'];
			$this->user_password_hashed = $user_data['user_password_hashed'];
			$this->user_status = $user_data['user_status'];
			$this->user_created = $user_data['user_created'];
			$this->user_default_service_id = $user_data['user_default_service_id'];
			$this->user_system_admin = $user_data['user_system_admin'];
			$this->user_account_id = $user_data['user_account_id'];
			$this->user_fullname = $user_data['user_fullname'];

			$account_details = $this->helper_users->getUserAccountDetails($u_id);
			$this->account_details_id = $account_details['account_details_id'];
			$this->first_name = $account_details['firstname'];
			$this->last_name = $account_details['lastname'];
			$this->email_address = $account_details['emailaddress'];
			$this->cell_number = $account_details['cellnumber'];
			$this->company_name = $account_details['companyname'];

			$this->user_services = $this->helper_users->getUserServices($u_id);
			$this->default_service = $this->helper_users->getUserDefaultServiceInfo($u_id);
			$this->default_account = $this->helper_users->getUserMainAccount($u_id);
			$this->buildUserRoles();
		}
	}

	/**
	 * STATIC FUNCTION RETURNS ALL PLUGIN ADMINS
	 */
	public static function getAllAdministrators()
	{
		$admins = array();
		$sql_helper = new sqlHelperUsers();
		$admin_ids = $sql_helper->getAllAdministratorIds();

		foreach($admin_ids as $admin_id)
		{
			$admin = new User($admin_id['user_id']);
			array_push($admins, $admin);
		}

		return $admins;

	}

	public function setStatus($statusString)
	{
		if($statusString == "ENABLED" || $statusString == "DISABLED")
		{
			$this->user_status = $statusString;
			return $this->helper_users->saveUserFieldValue($this->user_id, 'user_status', $statusString);
		}
		else
		{
			return false;
		}
	}

	public function getBatchActivity()
	{
		return $this->helper_users->getBatchActivity($this->user_id);
	}

	public function getPaymentActivity()
	{
		return $this->helper_users->getPaymentActivity($this->user_id);
	}

	public function isAPIUser()
	{
		$is_api_user = false;
		foreach($this->user_services as $service)
		{
			if($this->user_username == $service['service_login'])
			{
				$is_api_user = true;
			}
		}

		return $is_api_user;
	}


	/****************************************************
	 * FUNCTIONS FOR ROLE HANDLING OF THIS USER
	 ****************************************************/
	public function buildUserRoles()
	{
		$this->roles = $this->helper_users->getUserRoles($this->user_id);

		//to speed up roles searches using array index
		$this->role_index = array();
		foreach($this->roles as $role)
		{
			$this->role_index[$role['role_id']] = $role;
		}

	}

	public function checkUserHasRole($role_id)
	{
		if(array_key_exists($role_id, $this->role_index))
		{
			return true;
		}
		else
		{
			return false;
		}
	}

	public function getUserRoleByRoleId($role_id)
	{
		return $this->role_index[$role_id];
	}

	public function addRoleToUser($role_id)
	{
		if(!$this->checkUserHasRole($role_id))
		{
			$this->role_index[$role_id] = $this->helper_users->getUserRole($role_id);
			return $this->helper_users->addRoleToUser($this->user_id, $role_id);
		}
		else
		{
			return false;
		}
	}

	public function removeRoleFromUser($role_id)
	{
		if($this->checkUserHasRole($role_id))
		{
			unset($this->role_index[$role_id]);
			return $this->helper_users->removeRoleFromUser($this->user_id, $role_id);
		}
		else
		{
			return false;
		}
	}

	/**
	 * Does as it says, gets rid of all roles associated for a user, useful for applying templates
	 * @return boolD
	 */
	public function removeAllRolesFromUser()
	{
		foreach($this->role_index as $role_id => $role)
		{
			$this->removeRoleFromUser($role_id);
		}

		return true;
	}

	/**
	 * This function cycles through all roles, and assigns them to a user if the substring
	 * of their type matches the type string passed ot this function.
	 * @param $role_type - Type string, valid ones are "client", "reseller" or "admin"
	 *
	 * @return Boolean success
	 */
	public function assignAllRolesByType($role_type)
	{
		$this->removeAllRolesFromUser();

		$roles = $this->helper_users->getAllRolesByType($role_type);

		foreach($roles as $role)
		{
			$this->addRoleToUser($role['role_id']); //checks and adds the role for us
		}

		return true;
	}



	/****************************************************
	 * FUNCTIONS FOR PASSWORD MANAGEMENT
	 ****************************************************/

	public function checkUserHasPermission($service_id)
	{
		if(array_key_exists($service_id, $this->permission_index))
		{
			return true;
		}
		else
		{
			return false;
		}
	}

	public function saveNewPassword($new_password)
	{
		//hash and save the new password for the user
		$hashed_password = helperPassword::createHashedPassword($new_password);

		return $this->helper_users->saveNewHashedPassword($this->user_id, $hashed_password);
	}



	/****************************************************
	 * FUNCTIONS FOR SERVICE PERMISSIONS MANAGEMENT
	 ****************************************************/

	public function addServicePermission($service_id)
	{
		if(!$this->helper_users->checkUserHasAccessToService($service_id, $this->user_id))
		{
			$permission_type = "submit.mtsms";

			return $this->helper_users->addServicePermissionToUser($this->user_id, $service_id, $permission_type);
		}
		else
		{
			return false;
		}
	}

	public function removeServicePermission($service_id)
	{
		if($this->helper_users->checkUserHasAccessToService($service_id, $this->user_id))
		{
			return $this->helper_users->removeServicePermissionFromUser($this->user_id, $service_id);
		}
		else
		{
			return false;
		}
	}


	/****************************************************
	 * SAVING THE USER
	 ****************************************************/
	public function save()
	{
		if(isset($this->user_id) && $this->user_id != -1)
		{
			$this->account_details_id = $this->helper_users->saveUserAccountDetails($this->account_details_id, $this->user_id, $this->user_account_id, $this->user_default_service_id, $this->first_name, $this->last_name, $this->company_name, $this->email_address, $this->cell_number);

			return true;
		}
		else
		{
			return false;
		}
	}

	/**
	 * This function checks to see if this user has performed any actions, and if not, authorizes deletion
	 * @return bool
	 */
	public function checkCanDeleteUser()
	{
		//assume true until it isnt
		$can_delete = true;

		//count batches
		$batch_count = $this->helper_users->getBatchCount($this->user_id);
		if($batch_count > 0)
		{
			$can_delete = false;
		}

		//check credit transactions
		$pay_count = $this->helper_users->getPaymentCount($this->user_id);
		if($pay_count > 0)
		{
			$can_delete = false;
		}

		//check credit transactions
		$service_manager_count = $this->helper_users->getServiceCountAsManager($this->user_id);
		if($service_manager_count > 0)
		{
			$can_delete = false;
		}

		return $can_delete;
	}

	/**
	 * Does as it says, dummy
	 * @return bool
	 */
	public function delete()
	{
		if(isset($this->user_id) && $this->user_id != -1)
		{
			$this->helper_users->deleteUser($this->user_id);

			return true;
		}
		else
		{
			return false;
		}
	}

	/**
	 * Creates a new user using basic parameters
	 * @param $service_id
	 * @param $account_id
	 * @param $user_username
	 * @param $user_password
	 * @param $user_first_name
	 * @param $user_last_name
	 * @param $user_system_admin
	 * @return bool
	 */
	public static function createNewUser($service_id, $account_id, $user_username, $user_password, $user_first_name, $user_last_name, $user_system_admin)
	{
		try
		{
			$helper_users = new sqlHelperUsers();
			$helper_services = new sqlHelperServices();

			$user_id = $helper_users->createNewUser($service_id, $account_id, $user_username, $user_password, $user_first_name, $user_last_name, $user_system_admin);

			if(isset($user_id) && $user_id > 0)
			{
				$helper_users->saveUserAccountDetails(-1, $user_id, $account_id, $service_id, $user_first_name, $user_last_name, "", "", "");

				//check if the user is an admin and assign roles
				if($user_system_admin == 1)
				{
					//give the new user the correct default roles for the service, so if he logs in, he can see what is happening
					$roles = $helper_users->getAllRolesByType('admin');
					foreach($roles as $role)
					{
						$helper_users->addRoleToUser($user_id, $role['role_id']); //checks and adds the role for us
					}

					//give the new API user administrative permissions over it's service
					$helper_services->createServiceAdminPermissions($service_id, $user_id); //make the user an admin
				}
				else
				{
					//give the new user the correct default roles for the service, so if he logs in, he can see what is happening
					$roles = $helper_users->getAllRolesByType('client');
					foreach($roles as $role)
					{
						$helper_users->addRoleToUser($user_id, $role['role_id']); //checks and adds the role for us
					}

					//give the new API user administrative permissions over it's service
					$helper_services->createServiceClientPermissions($service_id, $user_id); //make the user a client user of the service
				}

				return $user_id;
			}
			else
			{
				return false;
			}
		}
		catch(Exception $e)
		{
			return false;
			//log error $e->getMessage());
		}

	}

}
