<?php
/**
 * This model handles all tasks based on MTSMS objects
 *
 * @author Doug Jenkinson
 */

class MTSMS
{
	private $obj_type = "MTSMS";

	public function __construct(){}

	/**
	 * Returns an object that represents a single MTSMS elasticsearch document, that you can consume.
	 *
	 * @param String $doc_id - The Elasticsearch ID of the document
	 * @return ElasticsearchPluginObj $mtsms_obj - An ElasticsearchPluginObj that has all keys defined as this document.
	 */
	public static function find($doc_id)
	{
		$mtsms_obj = ElasticsearchPluginObject::find(CONFIG_PLUG_MTSMS_TYPE, $doc_id);

		return $mtsms_obj;
	}

	/**
	 * Returns an array of objects that represents a MTSMS elasticsearch document, that you can consume.
	 *
	 * @param String $service_id - The ID of the service we are filtering by
	 * @param int $from - The start document key to return, 0 returns from the beginning
	 * @param int $size - The amount of objects to return
	 *
	 * @return ElasticsearchPluginObj $mtsms_obj - An ElasticsearchPluginObj that has all keys defined as this document.
	 */
	public static function getAllByServiceId($service_id, $from = null, $size = null)
	{
		$filter_array = array('service_id' => $service_id);

		$mtsms_objs = ElasticsearchPluginObject::getAllObjByFilter(CONFIG_PLUG_MTSMS_TYPE, $from, $size, $filter_array);

		return $mtsms_objs;
	}

	/**
	 * Returns an array of objects that represents a MTSMS elasticsearch document, that you can consume.
	 *
	 * @param String $batch_id - The ID of the batch we are filtering by
	 * @param int $from - The start document key to return, 0 returns from the beginning
	 * @param int $size - The amount of objects to return
	 *
	 * @return ElasticsearchPluginObj $mtsms_obj - An ElasticsearchPluginObj that has all keys defined as this document.
	 */
	public static function getAllByBatchId($batch_id, $from = null, $size = null)
	{
		$filter_array = array('meta.batch_id' => $batch_id);

		$mtsms_objs = ElasticsearchPluginObject::getAllObjByFilter(CONFIG_PLUG_MTSMS_TYPE, $from, $size, $filter_array);

		return $mtsms_objs['objects'];
	}

	/**
	 * Returns an array of objects that represents a MTSMS elasticsearch document, that you can consume.
	 *
	 * @param String $batch_id - The ID of the batch we are filtering by
	 * @param int $size - The amount of objects to return
	 * @param String $scroll_id - The scrol ID for pagination, batch ID will be ignored if this is passed
	 *
	 * @return Array $mtsms_ids - An assoc array that contains the array of IDs and a new scroll_id
	 */
	public static function getAllMTSMSIDArrayByBatchId($batch_id, $size, $scroll_id = null)
	{
		//if no scroll ID, we need to issue one
		if(!isset($scroll_id))
		{
			//we have to build a manual array for this one
			$filter_array = array('meta.batch_id' => $batch_id);

			$scroll_id = ElasticsearchPluginObject::getScrollIDByFilter(CONFIG_PLUG_MTSMS_TYPE, $size, $filter_array, null, null, null, array('mtsms_id')); //size divide by 5 to cater for all 5 shards
		}

		$mtsms_arr = ElasticsearchPluginObject::getAllIDsByScrollID($scroll_id);

		return $mtsms_arr;
	}

	/**
	 * Returns an array of objects that represents a MTSMS elasticsearch document, that you can consume.
	 *
	 * @param String $batch_id - The ID of the batch we are filtering by
	 * @param int $size - The amount of objects to return
	 * @param String $scroll_id - The scrol ID for pagination, batch ID will be ignored if this is passed
	 *
	 * @return Array $mtsms_ids - An assoc array that contains the array of IDs and a new scroll_id
	 */
	public static function getAllMTSMSObjArrayByBatchId($batch_id, $size, $scroll_id = null)
	{
		//if no scroll ID, we need to issue one
		if(!isset($scroll_id))
		{
			//we have to build a manual array for this one
			$filter_array = array('meta.batch_id' => $batch_id);

			$scroll_id = ElasticsearchPluginObject::getScrollIDByFilter(CONFIG_PLUG_MTSMS_TYPE, $size, $filter_array); //size / 5 is to cater for all 5 shards
		}

		$mtsms_arr = ElasticsearchPluginObject::getResultsByScrollID(CONFIG_PLUG_MTSMS_TYPE, $scroll_id);

		return $mtsms_arr;
	}


	/**
	 * Returns an array of objects that represents a MTSMS elasticsearch document, that you can consume.
	 *
	 * @param String $service_id - The ID of the service we are filtering by (if left null or less than 1, then it won't be filtered
	 * @param String $message_content - The message text to search for.
	 * @param int $from - The start document key to return, 0 returns from the beginning
	 * @param int $size - The amount of objects to return
	 *
	 * @return Array(ElasticsearchPluginObj) $mtsms_obj - An array of ElasticsearchPluginObjs
	 */
	public static function searchByMessageContent($message_content, $service_id = null, $from = null, $size = null)
	{
		$filter_array = array();
		if(isset($service_id) && $service_id > 0)
		{
			$filter_array = array('service_id' => $service_id);
		}

		$mtsms_objs = ElasticsearchPluginObject::searchByStringContent(CONFIG_PLUG_MTSMS_TYPE, 'content', $message_content, $from, $size, $filter_array);

		return $mtsms_objs['objects'];

	}

	/**
	 * Gets the bootstrap text css class for the display of a DLR
	 * @param $dlr_code
	 * @return string
	 */
	public static function getDLRDisplayClass($dlr_code)
	{
		//get the styling for the status
		$dlr_string = DeliveryReportHelper::dlrMaskToString($dlr_code, DeliveryReportHelper::DLR_STRING_PRETTY);
		$text_class = "";
		switch(strtolower($dlr_string))
		{
			case 'pending' :
				$text_class = 'text-warning';
				break;
			case 'delivered' :
				$text_class = 'text-success';
				break;
			case 'failed' :
				$text_class = 'text-danger';
				break;
			case 'rejected' :
				$text_class = 'text-danger';
				break;
		}

		return $text_class;
	}

	/**
	 * Gets the bootstrap ICON css class for the display of a DLR icon
	 * @param $dlr_code
	 * @return string
	 */
	public static function getDLRDisplayIcon($dlr_code)
	{
		//get the styling for the status
		$dlr_string = DeliveryReportHelper::dlrMaskToString($dlr_code, DeliveryReportHelper::DLR_STRING_PRETTY);
		$icon_class = "";
		switch(strtolower($dlr_string))
		{
			case 'pending' :
				$icon_class = 'fa fa-question-circle';
				break;
			case 'delivered' :
				$icon_class = 'fa fa-check-circle';
				break;
			case 'failed' :
				$icon_class = 'fa fa-times-circle';
				break;
			case 'rejected' :
				$icon_class = 'fa fa-minus-circle';
				break;
		}

		return $icon_class;
	}

}
