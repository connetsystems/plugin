<?php

ini_set('display_errors', 1);

//--------------------------------------------------------------
// Constants
//--------------------------------------------------------------
//define('ERROR_REPORTING', (array_search('debug', $argv) !== false));

//--------------------------------------------------------------
// Includes
//--------------------------------------------------------------
require_once('ConnetAutoloader.php');

clog::setLogFilename("/var/log/connet/plugin/" . basename(__FILE__, '.php') . '.log');
clog::setBacktrace();
clog::setVerbose(ERROR_REPORTING);

/**
 * This model handles all tasks based on Service level information.
 *
 * This is the first model in the Connet API that introduces the use of SQL as well as elasticsearch, I hope to build on this.
 *
 * Available holders are all the general service data, plus $this->routing_data for routes, and $this->users for associated users.
 *
 * @author Doug Jenkinson
 */

class Service
{
	const SERVICE_ENDPOINT_METHOD_HTTP = "LEGACY";
	const SERVICE_ENDPOINT_METHOD_SMPP = "SMPP";
	const SERVICE_STATUS_ENABLED = "ENABLED";
	const SERVICE_STATUS_DISABLED = "DISABLED";


	//for SMPP ENDPOINTS
	const SERVICE_BASE_SMPP_DLR_URL = "http://server2:3001/core_sms/interfaces/smpp/smpp_dlr.php?debug=hallodaar&system_id=";
	const SERVICE_BASE_SMPP_MOSMS_URL = "http://server2:3001/core_sms/interfaces/smpp/smpp_mosms.php?debug=hallodaar&system_id=";


	//----------------------------------
	// GENERAL SERVICE DATA
	//----------------------------------
	public $account_id;
	public $account_name;
	public $service_name;
	public $service_status;
	public $service_login;
	public $service_created;
	public $service_password;
	public $service_manager;
	public $header_logo;
	public $account_colour;
	public $url_prefix;
	public $url_suffix;

	private $_data;
	public $service_id;
	public $service_helper;

	//setup default vars
	//----------------------------------
	// ROUTES CONFIG
	//----------------------------------
	public $routing_data = null;
	public $local_routes = null;
	public $international_routes = null;

	//----------------------------------
	// USERS CONFIG
	//----------------------------------
	public $users = null;
	public $admin_users = null;

	//----------------------------------
	// SERVICE CREDIT VARS
	//----------------------------------
	public $service_credit = null;
	public $service_credit_billing_type;
	public $service_credit_available = null;

	//----------------------------------
	// LOW BALANCE NOTIFICATION VARS
	//----------------------------------
	public $service_notification_enabled = false;
	public $service_notification_threshold = null;
	public $service_notification_email = null;
	public $service_notification_sms = null;

	//----------------------------------
	// DLR AND MOSMS ENDPOINT VARS
	//----------------------------------
	public $dlr_raw_sql_endpoints = array();
	public $msosms_raw_sql_endpoints = array();

	public $http_dlr_enabled = false;
	public $http_dlr_status = 'DISABLED';
	public $http_dlr_address = 'Not set';
	public $http_dlr_id = -1;

	public $smpp_dlr_enabled = false;
	public $smpp_dlr_status = 'DISABLED';
	public $smpp_dlr_address = null;
	public $smpp_dlr_id = -1;

	public $http_mosms_enabled = false;
	public $http_mosms_status = 'DISABLED';
	public $http_mosms_address = 'Not set';
	public $http_mosms_id = -1;

	public $smpp_mosms_enabled = false;
	public $smpp_mosms_status = 'DISABLED';
	public $smpp_mosms_address = null;
	public $smpp_mosms_id = -1;

	/**
	 * Service constructor.
	 * @param $s_id - The service ID, if isset, all the service info will automatically be loaded, else if it is null, no data is loaded, but can be assigned and saved.
	 */
	public function __construct($s_id)
	{
		$this->service_helper = new sqlHelperServices();

		if(isset($s_id))
		{
			$this->service_id = $s_id;

			//for general service properties
			$service_data = $this->service_helper->getServiceData($this->service_id);

			$this->account_id = $service_data['account_id'];
			$this->account_name = $service_data['account_name'];
			$this->service_name = $service_data['service_name'];
			$this->service_status = $service_data['service_status'];
			$this->service_login = $service_data['service_login'];
			$this->service_created = $service_data['service_created'];
			$this->service_password = $service_data['service_password'];
			$this->service_manager = $service_data['service_manager'];
			$this->header_logo = $service_data['header_logo'];
			$this->account_colour = $service_data['account_colour'];
			$this->url_prefix = $service_data['url_prefix'];
			$this->url_suffix = $service_data['url_suffix'];
			$this->service_credit_billing_type = $service_data['service_credit_billing_type'];

			//for low balance notifications
			$this->service_notification_enabled = (isset($service_data['service_notification_threshold']) && $service_data['service_notification_threshold'] != "" ? true : false);
			$this->service_notification_threshold = $service_data['service_notification_threshold'];
			$this->service_notification_email = $service_data['service_notification_email'];
			$this->service_notification_sms = $service_data['service_notification_sms'];

			$this->service_credit_available = $service_data['service_credit_available'];

			//right, construct some of the other complicated properties of a service
			$this->buildUserData();
			$this->buildEndpointData();
			$this->buildRoutingData();
			$this->buildCredit();
		}
	}

	/**
	 * This function grabs all the users that belong to this service, and generates an array of user objects within this service.
	 */
	public function buildUserData()
	{
		$user_ids = $this->service_helper->getServiceUserIds($this->service_id);
		$this->users = array();

		foreach($user_ids as $user_id)
		{
			array_push($this->users, new User($user_id['user_id']));
		}
	}

	/**
	 * Pulls all admin users ONLY and builds them into the object
	 */
	public function getAdminUsers()
	{
		$user_ids = $this->service_helper->getServiceUserAdminIds($this->service_id);

		if(!isset($this->users))
		{
			$this->users = array();
		}

		foreach($user_ids as $user_id)
		{
			array_push($this->users, new User($user_id['user_id']));
		}
	}

	/****************************************************
	 * FUNCTIONS FOR HANDLING CREDIT
	 ****************************************************/
	/**
	 * This function pulls and processes credit data for this service
	 */
	private function buildCredit()
	{
		$this->service_credit = $this->service_helper->getCurrentCredit($this->service_id);
	}

	/**
	 * Updates the credit of this account, by adding the credit_amount sent to this function with the service credit, and saves a transaction recrod with the descripti.
	 * Negative credit values are obviously subtracted from the service credit, whereas positive credit values are added.
	 * @param $credit_amount
	 * @param $credit_description
	 * @return bool
	 */
	public function updateCreditWithCurrency($credit_amount, $credit_description)
	{
		$credit_amount = ($credit_amount*10000);
		if(!isset($_SESSION['userId']))
		{
			$user_id = -1;
		}
		else
		{
			$user_id = $_SESSION['userId'];
		}

		$success = $this->service_helper->updateCredit($this->service_id, $user_id, $credit_amount, $credit_description, $this->service_credit_available);

		if($success === true) {
			$this->service_credit_available += $credit_amount;
		}

		return $success;
	}

	/****************************************************
	 * FUNCTIONS FOR HANDLING ROUTING FOR THIS SERVICE
	 ****************************************************/

	/**
	 * This function pulls and processes all the routing data for this object.
	 */
	private function buildRoutingData()
	{
		$this->routing_data = $this->service_helper->getDefaultRouteData($this->service_id);
		$this->local_routes = array();
		$this->international_routes = array();

		//separate the local and international routes
		foreach ($this->routing_data as $route)
		{
			if ($route['MCCMNC'] == "65501" || $route['MCCMNC'] == "65502" || $route['MCCMNC'] == "65507" || $route['MCCMNC'] == "65510")
			{
				array_push($this->local_routes, $route);
			}
			else
			{
				array_push($this->international_routes, $route);
			}
		}
	}

	/****************************************************
	 * FUNCTIONS FOR HANDLING OF LOW BALANCE NOTIFICATIONS
	 ****************************************************/

	/**
	 * Either activates or deactivates Low Balance Notifications, by setting or not setting values
	 * @param $status
	 * @return bool
	 */
	public function toggleLowBalanceNotifications($status)
	{
		try
		{
			if(self::checkEnabled($status))
			{
				//update the object as well
				$this->service_notification_enabled = true;
				$this->service_notification_threshold = 0;

				//set default blank addresses if they are not set already
				if(!isset($this->service_notification_email))
				{
					$this->service_notification_email = "";
				}
				if(!isset($this->service_notification_sms))
				{
					$this->service_notification_sms = "";
				}

				//first, let's update the DB to make sure this endpoint is now enabled
				$this->service_helper->saveServiceFieldValue($this->service_id, "service_notification_threshold", 0);
			}
			else
			{
				//update the object as well
				$this->service_notification_enabled = true;
				$this->service_notification_threshold = null;

				//let's update the DB to make sure this endpoint is now disabled
				$this->service_helper->saveServiceFieldValue($this->service_id, "service_notification_threshold", null);
			}

			return true;
		}
		catch(Error $e)
		{
			clog::info('* Low Balance Notifications Error: '.$e->getMessage());
			return false;
		}
	}

	/****************************************************
	 * FUNCTIONS FOR HANDLING OF ENDPOINTS IN A SERVICE
	 ****************************************************/

	/**
	 * This function pulls all the DLR and MOSMS endpoints from the database and reformats the information into
	 * useful variables for easy display and editing.
	 */
	private function buildEndpointData()
	{
		//get the raw data from the database
		$this->dlr_raw_sql_endpoints = $this->service_helper->getServiceDLRConfig($this->service_id);
		$this->msosms_raw_sql_endpoints = $this->service_helper->getServiceMOSMSConfig($this->service_id);


		//process the DLR and MOSMS enpoints into something more tangible for this object
		//start with the DLR enpoints, determine if it is SMPP or HTTP
		foreach($this->dlr_raw_sql_endpoints as $dlr_config_record)
		{
			if($dlr_config_record['endpoint_dlr_method'] === self::SERVICE_ENDPOINT_METHOD_HTTP)
			{
				$this->http_dlr_enabled = self::checkEnabled($dlr_config_record['endpoint_dlr_status']);
				$this->http_dlr_status = $dlr_config_record['endpoint_dlr_status'];
				$this->http_dlr_address = $dlr_config_record['endpoint_dlr_address'];
				$this->http_dlr_id = $dlr_config_record['endpoint_dlr_id'];
			}

			if($dlr_config_record['endpoint_dlr_method'] === self::SERVICE_ENDPOINT_METHOD_SMPP)
			{
				$this->smpp_dlr_enabled = self::checkEnabled($dlr_config_record['endpoint_dlr_status']);
				$this->smpp_dlr_status = $dlr_config_record['endpoint_dlr_status'];
				$this->smpp_dlr_address = $dlr_config_record['endpoint_dlr_address'];
				$this->smpp_dlr_id = $dlr_config_record['endpoint_dlr_id'];
			}
		}

		//now the MOSMS endpoints, determine if it is SMPP or HTTP
		foreach($this->msosms_raw_sql_endpoints as $mosms_config_record)
		{
			if($mosms_config_record['endpoint_mosms_method'] === self::SERVICE_ENDPOINT_METHOD_HTTP)
			{
				$this->http_mosms_enabled = self::checkEnabled($mosms_config_record['endpoint_mosms_status']);
				$this->http_mosms_status = $mosms_config_record['endpoint_mosms_status'];
				$this->http_mosms_address = $mosms_config_record['endpoint_mosms_address'];
				$this->http_mosms_id = $mosms_config_record['endpoint_mosms_id'];
			}

			if($mosms_config_record['endpoint_mosms_method'] === self::SERVICE_ENDPOINT_METHOD_SMPP)
			{
				$this->smpp_mosms_enabled = self::checkEnabled($mosms_config_record['endpoint_mosms_status']);
				$this->smpp_mosms_status = $mosms_config_record['endpoint_mosms_status'];
				$this->smpp_mosms_address = $mosms_config_record['endpoint_mosms_address'];
				$this->smpp_mosms_id = $mosms_config_record['endpoint_mosms_id'];
			}
		}

		//if the SMPP address is not set in the DB, then we set it manually here for display purposes only
		if(!isset($this->smpp_mosms_address))
		{
			$this->smpp_mosms_address = self::SERVICE_BASE_SMPP_MOSMS_URL.$this->service_login;
		}

		//if the SMPP address is not set in the DB, then we set it manually here for display purposes only
		if(!isset($this->smpp_dlr_address))
		{
			$this->smpp_dlr_address = self::SERVICE_BASE_SMPP_DLR_URL.$this->service_login;
		}
	}

	/**
	 * ENABLING AND DISABLING THE ENDPOINTS!
	 */
	function enableHTTPDLREndpoint() { $this->setHTTPDLREndpointStatus(self::SERVICE_STATUS_ENABLED); }
	function enableHTTPMOSMSEndpoint() { $this->setHTTPMOSMSEndpointStatus(self::SERVICE_STATUS_ENABLED); }
	function disableHTTPDLREndpoint() { $this->setHTTPDLREndpointStatus(self::SERVICE_STATUS_DISABLED); }
	function disableHTTPMOSMSEndpoint() { $this->setHTTPMOSMSEndpointStatus(self::SERVICE_STATUS_DISABLED); }

	function enableSMPPDLREndpoint() { $this->setSMPPDLREndpointStatus(self::SERVICE_STATUS_ENABLED); }
	function enableSMPPMOSMSEndpoint() { $this->setSMPPMOSMSEndpointStatus(self::SERVICE_STATUS_ENABLED); }
	function disableSMPPDLREndpoint() { $this->setSMPPDLREndpointStatus(self::SERVICE_STATUS_DISABLED); }
	function disableSMPPMOSMSEndpoint() { $this->setSMPPMOSMSEndpointStatus(self::SERVICE_STATUS_DISABLED); }

	/**
	 * This function updates the object as well as the database entry for the status of an endpoint
	 * @param $status - (string) Either DISABLED or ENABLED
	 * @return bool
	 */
	function setHTTPDLREndpointStatus($status)
	{
		$this->checkAndCreateBlankHTTPDLREndpoint();

		//first, let's update the DB to make sure this endpoint is now enabled
		$this->service_helper->saveDLREndpointValue($this->http_dlr_id, "endpoint_dlr_status", $status);

		//update the object as well
		$this->http_dlr_enabled = true;
		$this->http_dlr_status = $status;

		return true;
	}

	/**
	 * This function updates the object as well as the database entry for the status of an endpoint
	 * @param $status - (string) Either DISABLED or ENABLED
	 * @return bool
	 */
	function setSMPPDLREndpointStatus($status)
	{
		$this->checkAndCreateBlankSMPPDLREndpoint();

		//first, let's update the DB to make sure this endpoint is now enabled
		$this->service_helper->saveDLREndpointValue($this->smpp_dlr_id, "endpoint_dlr_status", $status);

		//update the object as well
		$this->smpp_dlr_enabled = true;
		$this->smpp_dlr_status = $status;

		return true;
	}

	/**
	 * This function updates the object as well as the database entry for the status of an endpoint
	 * @param $status - (string) Either DISABLED or ENABLED
	 * @return bool
	 */
	function setHTTPMOSMSEndpointStatus($status)
	{
		$this->checkAndCreateBlankHTTPMOSMSEndpoint();

		//first, let's update the DB to make sure this endpoint is now enabled
		$this->service_helper->saveMOSMSEndpointValue($this->http_mosms_id, "endpoint_mosms_status", $status);

		//update the object as well
		$this->http_mosms_enabled = true;
		$this->http_mosms_status = $status;

		return true;
	}

	/**
	 * This function updates the object as well as the database entry for the status of an endpoint
	 * @param $status - (string) Either DISABLED or ENABLED
	 * @return bool
	 */
	function setSMPPMOSMSEndpointStatus($status)
	{
		$this->checkAndCreateBlankSMPPMOSMSEndpoint();

		//first, let's update the DB to make sure this endpoint is now enabled
		$this->service_helper->saveMOSMSEndpointValue($this->smpp_mosms_id, "endpoint_mosms_status", $status);

		//update the object as well
		$this->smpp_mosms_enabled = true;
		$this->smpp_mosms_status = $status;

		return true;
	}

	/**
	 * This handy function checks if an endpoint record actually exists, and creates a new blank one if it does not,
	 * useful for setting "ENABLED" to endpoint records that haven't actually been created in the DB yet
	 */
	function checkAndCreateBlankHTTPDLREndpoint()
	{
		if($this->http_dlr_id == -1)
		{
			$this->http_dlr_id = $this->service_helper->createDLREndpoint($this->service_id, "", self::SERVICE_STATUS_ENABLED, self::SERVICE_ENDPOINT_METHOD_HTTP, "strict=1");
		}
	}

	/**
	 * This handy function checks if an endpoint record actually exists, and create a new blank one if it does not,
	 * useful for setting "ENABLED" to endpoint records that haven't been created yet
	 */
	function checkAndCreateBlankHTTPMOSMSEndpoint()
	{
		if($this->http_mosms_id == -1)
		{
			$this->http_mosms_id = $this->service_helper->createMOSMSEndpoint($this->service_id, "", self::SERVICE_STATUS_ENABLED, self::SERVICE_ENDPOINT_METHOD_HTTP, "strict=1");
		}
	}

	/**
	 * This handy function checks if an endpoint record actually exists, and create a new blank one if it does not,
	 * useful for setting "ENABLED" to endpoint records that haven't been created yet
	 */
	function checkAndCreateBlankSMPPDLREndpoint()
	{
		if($this->smpp_dlr_id == -1)
		{
			$this->service_helper->createDLREndpoint($this->service_id, self::SERVICE_BASE_SMPP_DLR_URL.$this->service_login, self::SERVICE_STATUS_ENABLED, self::SERVICE_ENDPOINT_METHOD_SMPP, "");
		}
	}

	/**
	 * This handy function checks if an endpoint record actually exists, and create a new blank one if it does not,
	 * useful for setting "ENABLED" to endpoint records that haven't been created yet
	 */
	function checkAndCreateBlankSMPPMOSMSEndpoint()
	{
		if($this->smpp_mosms_id == -1)
		{
			$this->service_helper->createMOSMSEndpoint($this->service_id, self::SERVICE_BASE_SMPP_MOSMS_URL.$this->service_login, self::SERVICE_STATUS_ENABLED, self::SERVICE_ENDPOINT_METHOD_SMPP, "");
		}
	}

	/**
	 * This basic function saves a DLR endpoint to the table
	 * @param $field_name
	 * @param $field_value
	 * @return bool success
	 */
	public function saveHTTPDLREndpointValue($field_name, $field_value)
	{
		$this->checkAndCreateBlankHTTPDLREndpoint();

		return $this->service_helper->saveDLREndpointValue($this->http_dlr_id, $field_name, $field_value);
	}

	/**
	 * This basic function saves a MOSMS endpoint to the table
	 * @param $field_name
	 * @param $field_value
	 * @return bool success
	 */
	public function saveHTTPMOSMSEndpointValue($field_name, $field_value)
	{
		$this->checkAndCreateBlankHTTPMOSMSEndpoint();

		return $this->service_helper->saveMOSMSEndpointValue($this->http_mosms_id, $field_name, $field_value);
	}


	/**
	 * Simple helper method returns boolean TRUE for "ENABLED" and false for any other string value (assumed "DISABLED")
	 * @param $status_string
	 * @return bool
	 */
	public static function checkEnabled($status_string)
	{
		if($status_string === self::SERVICE_STATUS_ENABLED)
		{
			return true;
		}
		else
		{
			return false;
		}
	}


	/**
	 * This function handles the annying and complicated process of creating a new service within the core environment,
	 * I've tried to comment as best I could with what or why it is doing things, but in the end programming is an exercise of love
	 * and hate. Don't hate the player, hate the game.
	 *
	 * No seriously it's not that complicated.
	 *
	 * @param $account_id
	 * @param $service_name
	 * @param $service_login_name
	 * @param $billing_type
	 * @param $manager_user_id
	 * @return bool|string
	 */
	public static function createNewServiceAdmin($account_id, $service_name, $service_login_name, $billing_type, $manager_user_id)
	{
		try
		{
			//need our SQL helpers to help us build this thing
			$service_helper = new sqlHelperServices();
			$user_helper = new sqlHelperUsers();

			//create the nw service record xso we have a Service ID to attach all the rest of the crap to
			$service_id = $service_helper->createNewService($account_id, $service_name, $service_login_name, $manager_user_id);

			//if we have an ID, we can proceed
			if(isset($service_id) && $service_id > 0)
			{
				//must create a new blank credit record for this service, which stores the services credit balance
				$service_helper->createServiceCreditRecord($service_id, $billing_type);

				//create the new API user for the service
				$user_password = helperPassword::createRandomPassword(8);
				$user_id = $user_helper->createNewServiceAPIUser($service_login_name, $user_password, $account_id, $service_id);

				//give the new API user the correct default roles for the service, so if he logs in, he can see what is happening
				$roles = $user_helper->getAllRolesByType('client');
				foreach($roles as $role)
				{
					$user_helper->addRoleToUser($user_id, $role['role_id']); //checks and adds the role for us
				}

				//give the new API user administrative permissions over it's service
				$service_helper->createServiceAdminPermissions($service_id, $user_id); //make the user an admin

				//we need to add proper permissions for all admin users in Connet as well
				$admin_ids = $user_helper->getAllAdministratorIds();
				foreach($admin_ids as $admin_id)
				{
					$service_helper->createServiceAdminPermissions($service_id, $admin_id['user_id']);
				}

				//lastly, we create a default client and default campaign for the service, used for organizing bulk sends
				$client_id = $service_helper->createClient($service_id, $user_id, $service_name, "ENABLED");
				$service_helper->createCampaign($client_id, $user_id, "Default", "DEFAULT", "ENABLED");

				//and with that, we have built a whole new service, lets return an ID
				return $service_id;
			}
			else
			{
				return false;
			}
		}
		catch(Exception $e)
		{
			return false;
			//log the msseage
			//$e->getMessage());
		}
	}




	//-----------------------------------------------
	// FOR SERVICE STATS (ORIGINAL SERVICE OBJECT CODE HERE)
	// Mostly relates to elasticsearch
	//-----------------------------------------------
	/**
	 * Returns an array of objects that represents a MTSMS elasticsearch document, that you can consume.
	 *
	 * @param String $service_id - The ID of the service for which we want send stats
	 * @param String $start_date - The start of the date range for the stats (UNIX timestamp)
	 * @param String $end_date - The end of the date range for the stats (UNIX timestamp)
	 *
	 * @return Array $service_send_stats - An array of all the stats
	 */
	public static function getBatchStats($service_id, $start_date = null, $end_date = null)
	{
		$batch_list = getBatchList($service_id, $start_date, $end_date);

		$count = 0;

		foreach ($batch_list as $key => $value)
		{
			if(isset($value['batch_id']) && $value['batch_id'] > 0)
			{
				$batch_send_stats = array();

				$batch_send_stats['DATE'] = $value['batch_created'];
				$batch_send_stats['CLIENT'] = $value['client_name'];
				$batch_send_stats['CAMPAIGN'] = $value['campaign_name'];
				$batch_send_stats['CODE'] = $value['campaign_code'];
				$batch_send_stats['REF'] = $value['batch_reference'];
				$batch_send_stats['STATUS'] = $value['batch_submit_status'];
				$batch_send_stats['TOTAL'] = $value['batch_mtsms_total'];

				//get all delivery stats from elasticsearch
				$batch_stats = Batch::getFullSendStats($value['batch_id']);

				$batch_send_stats['sent'] = $batch_stats['sent'] + $batch_stats['rejected'];
				$batch_send_stats['mosms'] = 0; //default 0
				$batch_send_stats['units'] = $batch_stats['units'] + $batch_stats['rejected_units'];
				$batch_send_stats['pending'] = $batch_stats['pending'];
				$batch_send_stats['pending_units'] = $batch_stats['pending_units'];
				$batch_send_stats['pending_codes'] = implode(',', $batch_stats['pending_codes']);
				$batch_send_stats['delivered'] = $batch_stats['delivered'];
				$batch_send_stats['delivered_units'] = $batch_stats['delivered_units'];
				$batch_send_stats['delivered_codes'] = implode(',', $batch_stats['delivered_codes']);
				$batch_send_stats['failed'] = $batch_stats['failed'] + $batch_stats['rejected'];
				$batch_send_stats['failed_units'] = $batch_stats['failed_units'] + $batch_stats['rejected_units'];
				$batch_send_stats['failed_codes'] = implode(',', ($batch_stats['failed_codes'] + $batch_stats['rejected_codes']));
				$batch_send_stats['rejected'] = 0;
				$batch_send_stats['blacklisted'] = 0;

				$batch_list[$key]['send_stats'] = $batch_send_stats;

				$count ++;
			}
		}

		$service_batch_stats = array();
		$service_batch_stats['total'] = count($batch_list);
		$service_batch_stats['batch_list'] = $batch_list;

		return $service_batch_stats;
	}

	/**
	 * Returns full totals for all sending on a service through a particular date range
	 *
	 * @param String $service_id - The ID of the service for which we want send stats
	 * @param String $start_date - The start of the date range for the stats
	 * @param String $end_date - The end of the date range for the stats
	 *
	 * @return Array $service_send_stats - An array of all the stats
	 */
	public static function getFullSendStats($service_id, $start_date = null, $end_date = null)
	{
		//do some initial setup for values
		$service_send_stats = array();
		$service_send_stats['service_id'] = $service_id;
		$service_send_stats['sent'] = 0;
		$service_send_stats['units'] = 0;
		$service_send_stats['pending'] = 0;
		$service_send_stats['pending_units'] = 0;
		$service_send_stats['pending_codes'] = array();
		$service_send_stats['delivered'] = 0;
		$service_send_stats['delivered_units'] = 0;
		$service_send_stats['delivered_codes'] = array();
		$service_send_stats['failed'] = 0;
		$service_send_stats['failed_units'] = 0;
		$service_send_stats['failed_codes'] = array();
		$service_send_stats['rejected'] = 0;
		$service_send_stats['rejected_units'] = 0;
		$service_send_stats['rejected_codes'] = array();
		$service_send_stats['blacklisted'] = 0;
		$service_send_stats['mo_count'] = 0;


		//get status count of messages
		$filter_array = array('service_id' => $service_id);

		//set the aggregation keys we want to collect
		$aggregations = array(
			'dlr' => array(
				'terms' => array('field' => 'dlr', 'size' => 100000),
				'aggs' => array (
					'meta.parts' => array(
						'sum' => array('field' => 'meta.parts')
					)
				)
			)
		);

		$aggregations = ElasticsearchPluginObject::getKeyUniqueAggregations(CONFIG_PLUG_MTSMS_TYPE, $aggregations, $filter_array, null, $start_date, $end_date);

		//we need to process each aggregated status to see what it is!
		foreach($aggregations['dlr']['buckets'] as $dlr_bucket)
		{

			$dlr_string = strtolower(DeliveryReportHelper::dlrMaskToString($dlr_bucket['key']));
			if(!isset($service_send_stats[$dlr_string]))
			{
				$service_send_stats[$dlr_string] = 0;
				$service_send_stats[$dlr_string.'_codes'] = array();
			}

			$service_send_stats[$dlr_string] += $dlr_bucket['doc_count'];
			$service_send_stats[$dlr_string.'_units'] += $dlr_bucket['meta.parts']['value'];
			array_push($service_send_stats[$dlr_string.'_codes'], $dlr_bucket['key']);

		}

		//a little hacky, but calculate the sent value using the actual sent codes
		$service_send_stats['sent'] = $service_send_stats['failed'] + $service_send_stats['pending'] + $service_send_stats['delivered'];
		$service_send_stats['units'] = $service_send_stats['failed_units'] + $service_send_stats['pending_units'] + $service_send_stats['delivered_units'];


		return $service_send_stats;
	}

	/**
	 * Returns a daily send count for a service over a date range
	 *
	 * @param String $service_id - The ID of the service for which we want send stats
	 * @param String $start_date - The start of the date range for the stats
	 * @param String $end_date - The end of the date range for the stats
	 *
	 * @return Array $service_send_stats - An array of all the stats
	 */
	public static function getDailySends($service_id, $start_date = null, $end_date = null)
	{
		//get status count of messages
		$filter_array = array('service_id' => $service_id);

		//set the aggregation keys we want to collect
		$aggregations = array(
			'daily_sends' => array(
				'date_histogram' => array('field' => 'timestamp', 'interval' => 'day', 'min_doc_count' => 0, 'format' => 'yyyy-MM-dd'),
			)
		);

		$aggregations = ElasticsearchPluginObject::getKeyUniqueAggregations(CONFIG_PLUG_MTSMS_TYPE, $aggregations, $filter_array, null, $start_date, $end_date);

		$daily_stats = array();

		//reformat the buckets so that the dates are keys and the counts are values
		foreach($aggregations['daily_sends']['buckets'] as $stats)
		{
			$daily_stats[$stats['key_as_string']] = $stats['doc_count'];
		}

		//we need to fill in the blank days, as 0 doc values are not returned by ES
		$start_date_obj = new DateTime($start_date);
		$end_date_obj = new DateTime($end_date);

		$days_difference = $end_date_obj->diff($start_date_obj)->format("%a");
		$key_date = $start_date_obj->format('Y-m-d');
		for($i = 0; $i < $days_difference; ++ $i)
		{
			if(!array_key_exists($key_date, $daily_stats))
			{
				$daily_stats[$key_date] = 0;
			}
			$key_date = $start_date_obj->modify('+1 day')->format('Y-m-d');
		}

		ksort($daily_stats);

		return $daily_stats;
	}

	/**
	 * Returns a daily send count for a service over a date range
	 *
	 * @param String $service_id - The ID of the service for which we want send stats
	 * @param String $start_date - The start of the date range for the stats
	 * @param String $end_date - The end of the date range for the stats
	 * @param String $interval - 'day, 'month' year
	 * @return Array $service_send_stats - An array of all the stats
	 */
	public static function getSendsOverTimeWithDelivery($service_id, $start_date = null, $end_date = null, $interval = null)
	{
		if(!isset($interval))
		{
			$interval = 'day';
		}

		//get status count of messages
		$filter_array = array('service_id' => $service_id);

		//set the aggregation keys we want to collect
		$aggregations = array(
			'daily_sends' => array(
				'date_histogram' => array('field' => 'timestamp', 'interval' => $interval, 'min_doc_count' => 0, 'format' => 'yyyy-MM-dd'),
				'aggs' => array(
					'dlr' => array(
						'terms' => array('field' => 'dlr', 'size' => 100000),
					)
				)

			)
		);

		$aggregations = ElasticsearchPluginObject::getKeyUniqueAggregations(CONFIG_PLUG_MTSMS_TYPE, $aggregations, $filter_array, null, $start_date, $end_date);

		$daily_stats = array();

		//reformat the buckets so that the dates are keys and the counts are values
		foreach($aggregations['daily_sends']['buckets'] as $stats)
		{
			$send_stats = array();
			$send_stats['send_total'] = $stats['doc_count'];
			$send_stats['delivered'] = 0;
			$send_stats['pending'] = 0;
			$send_stats['failed'] = 0;
			$send_stats['rejected'] = 0;
			foreach($stats['dlr']['buckets'] as $dlr_stat)
			{
				$dlr_key_string = strtolower(DeliveryReportHelper::dlrMaskToString($dlr_stat['key']));
				$send_stats[$dlr_key_string] += $dlr_stat['doc_count'];
			}
			$daily_stats[$stats['key_as_string']] = $send_stats;
		}
		//echo json_encode($daily_stats). " ----- STOP ----------------------------";

		/*
		//we need to fill in the blank days, as 0 doc values are not returned by ES
		$start_date_obj = new DateTime($start_date);
		$end_date_obj = new DateTime($end_date);

		$days_difference = $end_date_obj->diff($start_date_obj)->format("%a");
		$key_date = $start_date_obj->format('Y-m-d');
		for($i = 0; $i < $days_difference; ++ $i)
		{
			if(!array_key_exists($key_date, $daily_stats))
			{
				$send_stats = array();
				$send_stats['send_total'] = 0;
				$send_stats['delivered'] = 0;
				$send_stats['pending'] = 0;
				$send_stats['failed'] = 0;
				$send_stats['rejected'] = 0;
				$daily_stats[$key_date] = $send_stats;
			}
			$key_date = $start_date_obj->modify('+1 day')->format('Y-m-d');
		}
		*/
		ksort($daily_stats);

		return $daily_stats;
	}


	/**
	 * Returns an assoc array of send stats for a service ID, basically networks, sends, units, cost of send (unique network records if there is a rate change)
	 * and totals per those networks
	 *
	 * @param int $service_id - The ID of the service we need stats for
	 * @param int (optional) $start_date - A unix datetime of the start date range (will return over all dates if not set)
	 * @param int (optional) $end_date - A unix datetime of the end date range (will return over all dates if not set)
	 *
	 * @return Array $service_send_stats - An associative array of keys and values representing send stats over a particular date range
	 */
	public static function getFullNetworkSendStats($service_id, $start_date = null, $end_date = null)
	{
		//do some initial setup for values
		$service_send_stats = array();
		$service_send_stats['service_id'] = $service_id;
		$service_send_stats['networks'] = array();


		//get status count of messages
		$filter_array = array('service_id' => $service_id);

		//set the aggregation keys we want to collect
		$aggregations['results'] = array(
			'date_range' => array(
				'field' => 'timestamp',
				'format' => 'YYYY-MM-dd',
				'ranges' => array(
					array('from' => $start_date, 'to' => $end_date)
				)
			),
			'aggs' => array(
				'mccmnc' => array(
					'terms' => array('field' => 'meta.mccmnc', 'size' => 1000),
					'aggs' => array(
						'rate' => array(
							'terms' => array('field' => 'route_billing'),
							'aggs' => array(
								'units' => array('terms' => array('field' => 'billing_units')),
								'total_units' => array('sum' => array('field' => 'billing_units')),
								'total_cost' => array('sum' => array('field' => 'route_billing'))
							)
						),
						'dlr' => array(
							'terms' => array('field' => 'dlr', 'size' => 1000),
							'aggs' => array (
								'meta.parts' => array(
									'sum' => array('field' => 'meta.parts')
								)
							)
						)
					)
				)
			)
		);

		$mccmnc_aggregations = ElasticsearchPluginObject::getKeyUniqueAggregations(CONFIG_PLUG_MTSMS_TYPE, $aggregations, $filter_array, null, $start_date, $end_date);

		//echo "PERCENTS ----------------------- " . json_encode($mccmnc_aggregations);

		//get the total results
		$total_results = $mccmnc_aggregations['results']['buckets'][0]['doc_count'];

		//right lets process and reorganize this data
		foreach($mccmnc_aggregations['results']['buckets'][0]['mccmnc']['buckets'] as $mccmnc_bucket)
		{
			$network = array();

			//extract the network code
			$network['mccmnc'] = $mccmnc_bucket['key'];
			$network_name = getNetFromMCC($mccmnc_bucket['key']);
			$split_pos = strpos($network_name, "/");
			if($split_pos > 0 ) {
				$split_pos ++;
			}
			$network['network_name'] = trim(substr($network_name, $split_pos));
			$network['network_country'] = getCountryFromMCC($mccmnc_bucket['key']);

			$network['network_costs_per_unit'] = array();
			$network['network_total_units'] = 0;
			$network['network_total_sends'] = 0;
			$network['network_total_cost'] = 0.0;

			$network['rates'] = array();

			$unique_rates = array(); //for tracking unique rate keys

			//process for each rate
			foreach($mccmnc_bucket['rate']['buckets'] as $rate_agg)
			{
				//determine the actual rate for this bucket
				$actual_rate_value = $rate_agg['total_cost']['value'] / $rate_agg['total_units']['value'];

				//determine if we already have a rate of this value
				if(!in_array($actual_rate_value, $unique_rates))
				{
					//build the rate information
					$rate = array();
					$rate['rate'] = $actual_rate_value;
					$rate['total_units'] = $rate_agg['total_units']['value'];
					$rate['total_sends'] = $rate_agg['doc_count'];
					$rate['units'] = $rate_agg['units']['buckets'][0]['key']; //units are always be unique according to our query
					$rate['total_cost'] = $rate_agg['total_cost']['value'];

					array_push($network['rates'], $rate);
					array_push($unique_rates, $actual_rate_value);
				}
				else
				{
					//it exists, so add the totals an update the rate data
					$key = array_search($actual_rate_value, $unique_rates);
					$rate = $network['rates'][$key];

					$rate['total_units'] += $rate_agg['total_units']['value'];
					$rate['total_sends'] += $rate_agg['doc_count'];
					$rate['total_cost'] += $rate_agg['total_cost']['value'];

					$network['rates'][$key] = $rate;

				}

				if(!in_array(number_format(($actual_rate_value / 10000), 3, '.', ' '), $network['network_costs_per_unit']))
				{
					array_push($network['network_costs_per_unit'], number_format(($actual_rate_value / 10000), 3, '.', ' '));
				}

				$network['network_total_units'] += $rate_agg['total_units']['value'];
				$network['network_total_sends'] += $rate_agg['doc_count'];
				$network['network_total_cost'] += $rate_agg['total_cost']['value'];
				$network['network_total_send_percent'] = number_format($network['network_total_sends']/$total_results * 100, 2, '.', ' ');
			}

			//process for each delivery stat!
			foreach($mccmnc_bucket['dlr']['buckets'] as $dlr_bucket)
			{
				$dlr_string = strtolower(DeliveryReportHelper::dlrMaskToString($dlr_bucket['key']));
				if(!isset($network[$dlr_string]))
				{
					$network[$dlr_string] = 0;
					$network[$dlr_string.'_codes'] = array();
				}

				$network[$dlr_string] += $dlr_bucket['doc_count'];
				$network[$dlr_string.'_units'] += $dlr_bucket['meta.parts']['value'];
				array_push($network[$dlr_string.'_codes'], $dlr_bucket['key']);
			}

			array_push($service_send_stats['networks'], $network);
		}

		return $service_send_stats;
	}
}
