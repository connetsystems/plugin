<?php
/**
 * This model handles all tasks based on MTSMS objects
 *
 * @author Doug Jenkinson
 */

class Batch
{
	private $obj_type = "MTSMS";

	public function __construct(){}

	/**
	 * Returns an object that represents a single MTSMS elasticsearch document, that you can consume.
	 *
	 * @param String $doc_id - The Elasticsearch ID of the document
	 * @return ElasticsearchPluginObj $mtsms_obj - An ElasticsearchPluginObj that has all keys defined as this document.
	 */
	public static function find($doc_id)
	{
		$mtsms_obj = ElasticsearchPluginObject::find(CONFIG_PLUG_MTSMS_TYPE, $doc_id);

		return $mtsms_obj;
	}

	/**
	 * Returns an array of objects that represents a MTSMS elasticsearch document, that you can consume.
	 *
	 * @param String $service_id - The ID of the service we want the batch list for
	 * @param String $start_date - The start date
	 * @param String $end_date - The end date
	 *
	 * @return ElasticsearchPluginObj $mtsms_obj - An ElasticsearchPluginObj that has all keys defined as this document.
	 */
	public static function getServiceBatchListWithStats($service_id, $start_date, $end_date)
	{
		try
		{
			$batch_helper = new sqlHelperBatches();

			$batch_list = $batch_helper->getBatchList($service_id, $start_date, $end_date);

			foreach ($batch_list as $key => $batch)
			{
				//get all delivery stats from elasticsearch
				$batch['send_stats'] = self::getFullSendStats($batch['batch_id']);

				//get the styling for the status
				switch($batch['batch_submit_status'])
				{
					case 'Processing' :
					case 'Sending' :
					case 'SEND' :
						$batch['batch_submit_status_icon'] = 'fa fa-cog fa-spin';
						$batch['batch_submit_status_text_class'] = 'text-warning';
						break;
					case 'Paused' :
					case 'PAUSE' :
						$batch['batch_submit_status_icon'] = 'fa fa-warning';
						$batch['batch_submit_status_text_class'] = 'text-warning';
						break;
					case 'None' :
					case 'Error' :
					case 'Blacklist' :
					case 'ABORT' :
						$batch['batch_submit_status_icon'] = 'fa fa-exclamation-circle';
						$batch['batch_submit_status_text_class'] = 'text-danger';
						break;
					case 'SCHEDULE' :
						$batch['batch_submit_status_icon'] = 'fa fa-clock-o';
						$batch['batch_submit_status_text_class'] = 'text-info';
						break;
					case 'Completed' :
						$batch['batch_submit_status_icon'] = 'fa fa-check-square';
						$batch['batch_submit_status_text_class'] = 'text-success';
						break;
				}

				$batch_list[$key] = $batch;
			}

			return $batch_list;
		}
		catch(Exception $e)
		{
			return false;
			//log the msseage
			//$e->getMessage());
		}
	}

	/**
	 * Returns the basic batch data for display on a page
	 * @param $batch_id
	 * @return bool|mixed
	 */
	public static function getBatchData($batch_id)
	{
		try
		{
			$batch_helper = new sqlHelperBatches();

			$batch = $batch_helper->getBatchData($batch_id);

			//get all delivery stats from elasticsearch
			$batch['send_stats'] = self::getFullSendStats($batch['batch_id'], $batch['service_id']);

			//get the styling for the status
			switch($batch['batch_submit_status'])
			{
				case 'Processing' :
				case 'Sending' :
				case 'SEND' :
					$batch['batch_submit_status_icon'] = 'fa fa-cog fa-spin';
					$batch['batch_submit_status_text_class'] = 'text-warning';
					$batch['batch_submit_status_btn_class'] = 'btn-warning';
					$batch['batch_submit_status_can_change'] = false;
					break;
				case 'Paused' :
				case 'PAUSE' :
					$batch['batch_submit_status_icon'] = 'fa fa-warning';
					$batch['batch_submit_status_text_class'] = 'text-warning';
					$batch['batch_submit_status_btn_class'] = 'btn-warning';
					$batch['batch_submit_status_can_change'] = true;
					break;
				case 'None' :
				case 'Error' :
				case 'Blacklist' :
				case 'ABORT' :
					$batch['batch_submit_status_icon'] = 'fa fa-exclamation-circle';
					$batch['batch_submit_status_text_class'] = 'text-danger';
					$batch['batch_submit_status_btn_class'] = 'btn-warning';
					$batch['batch_submit_status_can_change'] = false;
					break;
				case 'SCHEDULE' :
					$batch['batch_submit_status_icon'] = 'fa fa-clock-o';
					$batch['batch_submit_status_text_class'] = 'text-info';
					$batch['batch_submit_status_btn_class'] = 'btn-info';
					$batch['batch_submit_status_can_change'] = true;
					break;
				case 'Completed' :
					$batch['batch_submit_status_icon'] = 'fa fa-check-square';
					$batch['batch_submit_status_text_class'] = 'text-success';
					$batch['batch_submit_status_btn_class'] = 'btn-success';
					$batch['batch_submit_status_can_change'] = false;
					break;
			}

			return $batch;
		}
		catch(Exception $e)
		{
			return false;
			//log the msseage
			//$e->getMessage());
		}
	}

	/**
	 * Allows us to change the status of a batch, such as pausing or aborting
	 * @param $batch_id
	 * @param $batch_submit_status
	 * @param $schedule - Optional
	 * @return bool
	 */
	public static function setBatchStatus($batch_id, $batch_submit_status, $schedule = null)
	{
	   try
	   {
		   $batch_helper = new sqlHelperBatches();
		   return $batch_helper->setBatchStatus($batch_id, $batch_submit_status, $schedule);
	   }
	   catch(Exception $e)
	   {
		   return false;
		   //log the msseage
		   //$e->getMessage());
	   }
	}


	/**
	 * Returns chart data breakdown of how sends went over time.
	 * @param $batch_id
	 * @return Array $chart_data
	 */
	public static function getSendRateChartData($batch_id)
	{
		//get status count of messages
		$filter_array = array('meta.batch_id' => $batch_id);

		$batch_obj = Batch::getBatchData($batch_id);

		$start_date = date('Y-m-d', strtotime($batch_obj['batch_schedule']));
		$end_date = date('Y-m-d', strtotime($batch_obj['batch_schedule']));

		//set the aggregation keys we want to collect
		$aggregations = array(
			'sends_over_interval' => array(
				'date_histogram' => array('field' => 'timestamp', 'interval' => 'minute', 'min_doc_count' => 0, 'format' => 'yyyy-MM-dd HH:mm:ss')
			)
		);

		$aggregations = ElasticsearchPluginObject::getKeyUniqueAggregations(CONFIG_PLUG_MTSMS_TYPE, $aggregations, $filter_array, null, $start_date, $end_date);

		$chart_data = array();
		//echo " DATA ---------------------- " . json_encode($aggregations['hour_sends']['buckets']);

		//reformat the buckets so that the dates are keys and the counts are values
		foreach($aggregations['sends_over_interval']['buckets'] as $stats)
		{
			$chart_data[$stats['key']/1000] = $stats['doc_count'];
		}

		ksort($chart_data);

		//want to add a blank record at the start and end
		reset($chart_data);
		$arr_start_date = key($chart_data);
		end($chart_data);
		$arr_end_date = key($chart_data);

		//set the new 0 records at the beginning and end
		$start_date_obj = new DateTime();
		$start_date_obj->setTimestamp($arr_start_date);
		$chart_data[$start_date_obj->modify('-1 minute')->getTimestamp()] = 0;

		$end_date_obj = new DateTime();
		$end_date_obj->setTimestamp($arr_end_date);
		$chart_data[$end_date_obj->modify('+1 minute')->getTimestamp()] = 0;

		ksort($chart_data);

		return $chart_data;
	}

	/**
	 * Returns chart data breakdown of how sends went over time.
	 * @param $batch_id
	 * @return Array $chart_data
	 */
	public static function getDeliveryOverIntervalChartData($batch_id)
	{
		//get status count of messages
		$filter_array = array('meta.batch_id' => $batch_id);

		$batch_obj = Batch::getBatchData($batch_id);

		$start_date = date('Y-m-d', strtotime($batch_obj['batch_schedule']));
		$end_date = date('Y-m-d', strtotime($batch_obj['batch_schedule']));

		//set the aggregation keys we want to collect
		$aggregations = array(
			'dlr_over_interval' => array(
				'date_histogram' => array('field' => 'dlr_timestamp', 'interval' => '1h', 'min_doc_count' => 0, 'format' => 'yyyy-MM-dd HH:mm:ss'),
				'aggs' => array(
					'dlr' => array(
						'terms' => array('field' => 'dlr', 'size' => 100000)
					)
				)
			)
		);

		$aggregations = ElasticsearchPluginObject::getKeyUniqueAggregations(CONFIG_PLUG_MTSMS_TYPE, $aggregations, $filter_array, null, $start_date, $end_date);

		$chart_data = array();
		//echo " DATA ---------------------- " . json_encode($aggregations);

		//reformat the buckets so that the dates are keys and the counts are values
		foreach($aggregations['dlr_over_interval']['buckets'] as $stats)
		{
			$send_stats = array();
			$send_stats['send_total'] = $stats['doc_count'];
			$send_stats['delivered'] = 0;
			$send_stats['pending'] = 0;
			$send_stats['failed'] = 0;
			$send_stats['rejected'] = 0;
			$send_stats['replies'] = 0;
			foreach($stats['dlr']['buckets'] as $dlr_stat)
			{
				$dlr_key_string = strtolower(DeliveryReportHelper::dlrMaskToString($dlr_stat['key']));
				$send_stats[$dlr_key_string] += $dlr_stat['doc_count'];
			}
			$chart_data[($stats['key']/1000)] = $send_stats;
			//echo '----------------------' . json_encode($stats);
		}


		ksort($chart_data);

		//want to add a blank record at the start and end
		reset($chart_data);
		$arr_start_date = key($chart_data);
		end($chart_data);
		$arr_end_date = key($chart_data);

		//set the new 0 records at the beginning and end
		$blank_stats = array();
		$blank_stats['send_total'] = 0;
		$blank_stats['delivered'] = 0;
		$blank_stats['pending'] = 0;
		$blank_stats['failed'] = 0;
		$blank_stats['rejected'] = 0;
		$blank_stats['replies'] = 0;

		$start_date_obj = new DateTime();
		$start_date_obj->setTimestamp($arr_start_date);
		$chart_data[$start_date_obj->modify('-1 hour')->getTimestamp()] = $blank_stats;

		$end_date_obj = new DateTime();
		$end_date_obj->setTimestamp($arr_end_date);
		$chart_data[$end_date_obj->modify('+1 hour')->getTimestamp()] = $blank_stats;

		ksort($chart_data);

		//get the MO incomming over time
		$mo_sms_ids = MOSMS::getAllIdsByBatchId($batch_id);

		//get status count of messages
		$filter_array_replies = array('mosms_id' => $mo_sms_ids);

		//set the aggregation keys we want to collect
		$aggregations_replies_def = array(
			'mo_count_interval' => array(
				'date_histogram' => array('field' => 'timestamp', 'interval' => '1h', 'min_doc_count' => 0, 'format' => 'yyyy-MM-dd HH:mm:ss'),
			)
		);

		$aggregations_replies = ElasticsearchPluginObject::getKeyUniqueAggregations(CONFIG_PLUG_MOSMS_TYPE, $aggregations_replies_def, $filter_array_replies, null, $start_date, $end_date);

		//echo " DATA ---------------------- " . json_encode($aggregations);

		//reformat the buckets so that the dates are keys and the counts are values
		foreach($aggregations_replies['mo_count_interval']['buckets'] as $stats)
		{
			$chart_data[($stats['key']/1000)]['replies'] = $stats['doc_count'];
		}

		return $chart_data;
	}

	/**
	 * Returns chart data breakdown of how sends went over time.
	 * @param $batch_id
	 * @param $service_id
	 * @return Array $chart_data
	 */
	public static function getNetworkSendData($service_id, $batch_id)
	{
		//do some initial setup for values
		$networks = array();


		//get status count of messages
		$filter_array = array('meta.batch_id' => $batch_id);

		//set the aggregation keys we want to collect
		$aggregations['mccmnc'] = array(
			'terms' => array('field' => 'meta.mccmnc', 'size' => 1000),
			'aggs' => array(
				'units' => array('terms' => array('field' => 'billing_units')),
				'total_units' => array('sum' => array('field' => 'billing_units')),
				'total_cost' => array('sum' => array('field' => 'route_billing'))
			)
		);

		$mccmnc_aggregations = ElasticsearchPluginObject::getKeyUniqueAggregations(CONFIG_PLUG_MTSMS_TYPE, $aggregations, $filter_array);

		//right lets process and reorganize this data
		foreach($mccmnc_aggregations['mccmnc']['buckets'] as $mccmnc_bucket)
		{
			$network = array();
			$network['network_name'] = getNetFromMCC($mccmnc_bucket['key']);
			$network['network_icon'] = "img/serviceproviders/". trim(substr($network['network_name'], strpos($network['network_name'], "/") + 1)).".png";
			$network['country_name'] = getCountryFromMCC($mccmnc_bucket['key']);
			$billing_info = getBillingInfoByMCCMNC($service_id, $mccmnc_bucket['key']);
			$network['currency'] = $billing_info['route_currency'];
			$network['country_icon'] = "img/flags/". (str_replace(' / ', '', $network['country_name']) == "" ? 'Special' : $network['country_name']).".png";
			$network['mccmnc'] = $mccmnc_bucket['key'];
			$network['send_total'] = $mccmnc_bucket['doc_count'];
			$network['cost'] = number_format($mccmnc_bucket['total_cost']['value'] / 10000, 2, '.', ' ');
			$network['cost_raw'] = $mccmnc_bucket['total_cost']['value'];

			//push to the collection array
			array_push($networks, $network);
		}


		return $networks;
	}


	/**
	 * Returns an array of objects that represents a MTSMS elasticsearch document, that you can consume.
	 *
	 * @param String $batch_id - The ID of the batch we are filtering by
	 * @param int $from - The start document key to return, 0 returns from the beginning
	 * @param int $size - The amount of objects to return
	 * @param str $send_type - The send type to search as string ("pending", "delivered", "failed", "blacklisted")
	 *
	 * @return ElasticsearchPluginObj $mtsms_obj - An ElasticsearchPluginObj that has all keys defined as this document.
	 */
	public static function getMTSMSArray($batch_id, $from = null, $size = null, $send_type = null)
	{
		$filter_array = array('meta.batch_id' => $batch_id);

		if(isset($send_type) && $send_type != "all")
		{
			$dlr_mask_array = self::convertSendTypeToDLRMaskByBatch($batch_id, $send_type);
			$filter_array['dlr'] = $dlr_mask_array;
		}

		$mtsms_objs = ElasticsearchPluginObject::getAllObjByFilter(CONFIG_PLUG_MTSMS_TYPE, $from, $size, $filter_array);

		return $mtsms_objs;
	}

	/**
	 * Returns an array of objects that represents a MTSMS elasticsearch document, that you can consume.
	 *
	 * @param String $batch_id - The ID of the batch we are filtering by
	 * @param int $scroll_id - The scroll ID to control position of the search
	 * @param str $send_type - The send type to search as string ("pending", "delivered", "failed", "blacklisted")
	 *
	 * @return ElasticsearchPluginObj $mtsms_obj - An ElasticsearchPluginObj that has all keys defined as this document.
	 */
	public static function getMTSMSArrayWithScrollId($batch_id, $size, $scroll_id = null, $send_type = null)
	{

		//if no scroll ID, we need to issue one
		if(!isset($scroll_id))
		{
			//we have to build a manual array for this one
			$filter_array = array('meta.batch_id' => $batch_id);

			if(isset($send_type) && $send_type != "all")
			{
				$dlr_mask_array = self::convertSendTypeToDLRMaskByBatch($batch_id, $send_type);
				$filter_array['dlr'] = $dlr_mask_array;
			}

			$scroll_id = ElasticsearchPluginObject::getScrollIDByFilter(CONFIG_PLUG_MTSMS_TYPE, $size, $filter_array); //size / 5 is to cater for all 5 shards
		}

		$mtsms_objs = ElasticsearchPluginObject::getResultsByScrollID(CONFIG_PLUG_MTSMS_TYPE, $scroll_id);

		return $mtsms_objs;
	}

	/**
	 * Gets all dlr codes available in
	 * @param $batch_id
	 * @param $send_type
	 * @return array
	 */
	public static function convertSendTypeToDLRMaskByBatch($batch_id, $send_type)
	{
		$dlr_codes = array();

		//get all dlr codes for this batch
		$filter_array = array('meta.batch_id' => $batch_id);

		//set the aggregation keys we want to collect
		$aggregations = array(
			'dlr' => array(
				'terms' => array('field' => 'dlr', 'size' => 1000),
			)
		);

		$aggregations = ElasticsearchPluginObject::getKeyUniqueAggregations(CONFIG_PLUG_MTSMS_TYPE, $aggregations, $filter_array);

		//we need to process each aggregated status to see what it is!
		foreach($aggregations['dlr']['buckets'] as $dlr_bucket)
		{
			$dlr_string = DeliveryReportHelper::dlrMaskToString($dlr_bucket['key'], DeliveryReportHelper::DLR_STRING_PRETTY);

			if(strtolower($dlr_string) == strtolower($send_type))
			{
				array_push($dlr_codes, $dlr_bucket['key']);
			}
		}

		return $dlr_codes;
	}

	/**
	 * Returns an array of objects that represents a MTSMS elasticsearch document, that you can consume.
	 *
	 * @param Int $batch_id - The ID of the batch we are filtering by
	 * @param Int $service_id
	 *
	 * @return ElasticsearchPluginObj $mtsms_obj - An ElasticsearchPluginObj that has all keys defined as this document.
	 */
	public static function getFullSendStats($batch_id, $service_id = null)
	{
		//do some initial setup for values
		$batch_stats = array();
		$batch_stats['batch_id'] = $batch_id;
		$batch_stats['sent'] = 0;
		$batch_stats['units'] = 0;
		$batch_stats['pending'] = 0;
		$batch_stats['pending_units'] = 0;
		$batch_stats['pending_codes'] = array();
		$batch_stats['delivered'] = 0;
		$batch_stats['delivered_units'] = 0;
		$batch_stats['delivered_codes'] = array();
		$batch_stats['failed'] = 0;
		$batch_stats['failed_units'] = 0;
		$batch_stats['failed_codes'] = array();
		$batch_stats['rejected'] = 0;
		$batch_stats['rejected_units'] = 0;
		$batch_stats['rejected_codes'] = array();
		$batch_stats['blacklisted'] = 0;
		$batch_stats['mo_count'] = 0;


		//get status count of messages
		$filter_array = array('meta.batch_id' => $batch_id);

		//set the aggregation keys we want to collect
		$aggregations = array(
			'dlr' => array(
				'terms' => array('field' => 'dlr', 'size' => 1000),
				'aggs' => array (
					'meta.parts' => array(
						'sum' => array('field' => 'meta.parts')
					)
				)
			),
			'total_cost' => array('sum' => array('field' => 'route_billing'))
		);

		$aggregations = ElasticsearchPluginObject::getKeyUniqueAggregations(CONFIG_PLUG_MTSMS_TYPE, $aggregations, $filter_array);

		//we need to process each aggregated status to see what it is!
		foreach($aggregations['dlr']['buckets'] as $dlr_bucket)
		{

			$dlr_string = strtolower(DeliveryReportHelper::dlrMaskToString($dlr_bucket['key']));
			if(!isset($batch_stats[$dlr_string]))
			{
				$batch_stats[$dlr_string] = 0;
				$batch_stats[$dlr_string.'_codes'] = array();
			}

			$batch_stats[$dlr_string] += $dlr_bucket['doc_count'];
			$batch_stats[$dlr_string.'_units'] += $dlr_bucket['meta.parts']['value'];
			array_push($batch_stats[$dlr_string.'_codes'], $dlr_bucket['key']);

		}

		//a little hacky, but calculate the sent value using the actual sent codes
		$batch_stats['sent'] = $batch_stats['failed'] + $batch_stats['pending'] + $batch_stats['delivered'];
		$batch_stats['units'] = $batch_stats['failed_units'] + $batch_stats['pending_units'] + $batch_stats['delivered_units'];
		$batch_stats['total_cost'] =  array();

		if(isset($service_id))
		{
			$cost_array = array();
			$network_data = self::getNetworkSendData($service_id, $batch_id);
			foreach($network_data as $network)
			{
				if(!array_key_exists($network['currency'], $cost_array))
				{
					$cost_array[$network['currency']] = 0;
				}
				$cost_array[$network['currency']] += $network['cost_raw'];
			}

			$batch_stats['total_cost'] = $cost_array;
		}

		return $batch_stats;
	}


}
