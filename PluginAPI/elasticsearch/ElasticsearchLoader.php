<?php
//require_once('../vendor/autoload.php');
//require_once('../config/config_core.php');

class ElasticsearchLoader {
	/**
	 * Helper for Elastic Search Connections
	 *
	 * @var string
	 */
	
	public static function getElasticClient()
	{
		$hosts = array (
			CONFIG_PLUG_ELASTIC_HOST
		);

		$defaultHandler  = Elasticsearch\ClientBuilder::defaultHandler();

		$client = Elasticsearch\ClientBuilder::create()
			->setHosts($hosts)      // Set the hosts
			->setHandler($defaultHandler)
			->build();

    	return $client;
  	}

	public static function killClient($client)
	{
		unset($client);
	}


	 /**
	 * Returns an array of parameters that only features the general client INDEX
	 * 
	 * @return $params - Array of parameters
	 */
	public static function getElasticsearchIndexParams() {
		$params = array();
		$params['index'] = CONFIG_PLUG_ELASTIC_CORE_INDEX;

		return $params;
	}
	
	public static function getElasticsearchMTSMSParams() {

		$params = array();
		$params['index'] = CONFIG_PLUG_ELASTIC_CORE_INDEX;
		$params['type']  = CONFIG_PLUG_ELASTIC_CORE_DOC_MTSMS;
		
		return $params;
	}
	
	public static function getElasticsearchMOSMSParams() {

		$params = array();
		$params['index'] = CONFIG_PLUG_ELASTIC_CORE_INDEX;
		$params['type']  = CONFIG_PLUG_ELASTIC_CORE_DOC_MOSMS;
		
		return $params;
	}
}
