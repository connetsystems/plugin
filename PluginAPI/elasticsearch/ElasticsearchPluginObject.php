<?php
/**
 * This class was built to try simplify the handling of Elasticsearch objects, particularly for MTSMS and MOSMS, based on an earlier class iteration that was built for another project.
 *
 * Please note this class is incomplete.
 *
 * @author Doug Jenkinson
 */

class ElasticsearchPluginObject
{
	private $elasticClient;
	private $raw_json_doc;
	private $_data;
	private $obj_type;
	private $base_elast_params;

	/**
	 * Inits everything needed to force this object to be exactly waht it implies, a PHP object
	 *
	 * @param String $obj_type - The type of object we are searching ("MTSMS" or "MOSMS")
	 */
	public function __construct($obj_type)
	{
		//we need to do some elasticsearch stuff
		$this->elasticClient = ElasticsearchLoader::getElasticClient();
		$this->_data = array();
		$this->obj_type = $obj_type;
		$this->base_elast_params = self::getBaseFormattedElasticParams($obj_type);
	}
	
	// magic methods!
  	public function &__set($index, $value)
	{
    	return $this->_data[$index] = & $value;
  	}

  	public function &__get($index)
	{
    	if (!array_key_exists( $index, $this->_data ))
    	{
        	// Use whatever default value is appropriate here
        	$this->_data[ $index ] = null;
    	}

    	return $this->_data[ $index ];
  	}
	
	/**  As of PHP 5.1.0  */
  	public function __isset($index)
  	{
  		return isset($this->_data [$index]);
  	}

	public static function getBaseFormattedElasticParams($obj_type)
	{
		switch ($obj_type) {
			case CONFIG_PLUG_MOSMS_TYPE:
				$base_params = ElasticsearchLoader::getElasticsearchMOSMSParams();
				break;
			case CONFIG_PLUG_MTSMS_TYPE:
				$base_params = ElasticsearchLoader::getElasticsearchMTSMSParams();
				break;
			default:
				throw new Exception("You must set a type for the elastic search object, options are 'MTSMS' or 'MOSMS'");
				return;
		}

		return $base_params;
	}
	
	/**
	 * General function that takes in Elasticsearch formatted params and returns an object
	 * @param Array $params - The elasticsearch formatted search or get params (is GET when $params['id'] is set, otherwise will search and return the first object)
	 * @return Object $mtsms - Returns an ElastObjMTSMS object located using the params
	 */
	public static function runElasticSearchForObject($obj_type, $params)
	{
		//get the elasticlient
		$elasticClient = ElasticsearchLoader::getElasticClient();

		//declare a new object of this class type
		$obj = new ElasticsearchPluginObject($obj_type);
		
		//if ID is set, then we are just getting an object directly from it's ID, otherwise we have to search for the object and return the first record
		if(isset($params['id']))
		{
			if($elasticClient->exists($params))
			{
				//get the raw json doc from elasticsearch and save it in the raw holder
				$obj->raw_json_doc = $elasticClient->get($params);

				foreach($obj->raw_json_doc['_source'] as $key => $value)
				{
					$obj->{$key} = $value;
		    	}

				$obj->id = $obj->raw_json_doc['_id'];
			}
		}
		else
		{
			//run the update in elasticsearch
			$elasticAssetGroup = $elasticClient->search($params);

			if(isset($elasticAssetGroup['hits']['hits'][0]))
			{
				foreach ($elasticAssetGroup['hits']['hits'][0]['_source'] as $key => $value)
				{
					$obj->{$key} = $value;
				}

				$obj->id = $elasticAssetGroup['hits']['hits'][0]['_id'];

			}
		}
		
		return $obj;
	}
	
	/**
	 * General function that takes in Elasticsearch formatted params and returns an array of objects as well as the total count.
	 * It also optionally returns a scroll ID if one is set.
	 *
	 * @param String $obj_type - The type of object we are searching ("MTSMS" or "MOSMS")
	 * @param $params - The elasticsearch formatted search params
	 *
	 * @return Array(ElasticsearchPluginObject) - A full array of these objects
	 */
	public static function runElasticSearchForArray($obj_type, $params)
	{
		//get the elasticlient
		$elasticClient = ElasticsearchLoader::getElasticClient();

		//run a scroll query, or run a normal query depending on if its set
		if(isset($params['scroll_id']))
		{
			$elasticResult = $elasticClient->scroll($params);
		}
		else
		{
			$elasticResult = $elasticClient->search($params);
		}

		$objArray = array();

		//build and array of objects and return it
		foreach($elasticResult['hits']['hits'] as $elasticHit)
		{
			//create an instance
			$object = new ElasticsearchPluginObject($obj_type);

			//build all the variables within the object using the actual documents variables
			foreach($elasticHit['_source'] as $key => $value)
			{
				$object->{$key} = $value;
	    	}
			
			//set the id
			$object->id = $elasticHit['_id'];

			array_push($objArray, $object);
		}
		
		$return_array = array();
		$return_array['objects'] = $objArray;


		//check if scroll ID is set, and send it if it is
		if(isset($elasticResult['_scroll_id']))
		{
			$return_array['scroll_id'] = $elasticResult['_scroll_id'];
		}
		$return_array['total_search_hits'] = $elasticResult['hits']['total'];

		return $return_array;
	}

	/**
	 * General function that takes in Elasticsearch formatted params and returns an array of objects as well as the total count.
	 * It also optionally returns a scroll ID if one is set.
	 *
	 * @param String $obj_type - The type of object we are searching ("MTSMS" or "MOSMS")
	 * @param $params - The elasticsearch formatted search params
	 *
	 * @return Array(ElasticsearchPluginObject) - A full array of these objects
	 */
	public static function runElasticSearchForIDs($params)
	{
		$return_array = array();
		try
		{
			//get the elasticlient
			$elasticClient = ElasticsearchLoader::getElasticClient();

			clog::info('* Query to run: ' . json_encode($params));

			//run a scroll query, or run a normal query depending on if its set
			if (isset($params['scroll_id']))
			{
				$elasticResult = $elasticClient->scroll($params);
			}
			else
			{
				$elasticResult = $elasticClient->search($params);
			}

			$idArray = array();

			//build and array of asset groups and return it
			foreach ($elasticResult['hits']['hits'] as $elasticHit)
			{
				array_push($idArray, $elasticHit['_id']);
			}

			$return_array['ids'] = $idArray;

			//check if scroll ID is set, and send it if it is
			if (isset($elasticResult['_scroll_id']))
			{
				$return_array['scroll_id'] = $elasticResult['_scroll_id'];
			}
			$return_array['total_search_hits'] = $elasticResult['hits']['total'];
		}
		catch(Exception $e)
		{
			$return_array['total_search_hits'] = 0;
		}

		return $return_array;
	}

	/**
	 * Gets a scroll ID for a particular scan scroll query
	 *
	 * @param String $obj_type - The type of object we are searching ("MTSMS" or "MOSMS")
	 * @param $params - The elasticsearch formatted search params
	 *
	 * @return String - The scroll ID for the scan scroll search
	 */
	public static function runElasticSearchForScrollID($params)
	{

		clog::info('* SCROLL QUERY = '.json_encode($params));

		//get the elasticlient
		$elasticClient = ElasticsearchLoader::getElasticClient();

		//run the update in elasticsearch
		$elasticResults = $elasticClient->search($params);

		return $elasticResults['_scroll_id'];
	}

	
	/**
	 * Finds an MTSMS document object by the Elasticsearch document ID.
	 *
	 * @param String $obj_type - The type of object we are searching ("MTSMS" or "MOSMS")
	 * @param String $doc_id - The Elasticsearch ID of the document
	 * @return Object mtsmsObj - The object holding the data of the particular MTSMS document
	 */
	public static function find($obj_type, $doc_id) {
		//we need to fetch the record
		$get_params = self::getBaseFormattedElasticParams($obj_type);
		$get_params['id'] = $doc_id;

		$mtsmsObj = self::runElasticSearchForObject($obj_type, $get_params);

		return $mtsmsObj;
	}
	
	
	
	/**
	 * Finds a core object by a certain value (Note this only works by matching exact values)
	 *
	 * @param String $obj_type - The type of object we are searching ("MTSMS" or "MOSMS")
	 * @param String $key - The "column" or name data name that we want to match via filter.
	 * @param String $value - The value to match for a particular column
	 *
	 * @return ElastObjPlugin $object - The Elasticsearch document as a handy PHP object
	 */
	public static function findByKey($obj_type, $key, $value)
	{
		//build the basics of our MTSMS search params for Elasticsearch
		$get_params = self::getBaseFormattedElasticParams($obj_type);
		
		//set the filter params
		$musts = array();
		$musts[0]['term'] = array($key => $value);

		$get_params['body']['query']['filtered'] = array('query' => array('match_all' => array()),
				'filter' => array('bool' => 
					array('must' => $musts)
			)
		);
		
		return self::runElasticSearchForObject($obj_type, $get_params);
	}
	
	
	
	/**
	 * This function returns an array of MTSMS objects for consumption by hungry Plugin
	 *
	 * @param String $obj_type - The type of object we are searching ("MTSMS" or "MOSMS")
	 * @param int $from - The key to pull from
	 * @param int $size - The number of records to pull
	 * @param Array $must_filters - The filters that the query must match [eg array('batch_id', '9999') means the batch_id must match 9999]
	 * @param Array $must_not_filters - The filters that the query must not match [eg array('batch_id', '9999') means the batch_id must NOT match 9999]
	 * @param String $start_date - The start of the date range that the records must match
	 * @param String $end_date - The end of the date range that the records must match
	 * @param String $sort - What key to sort by
	 *
	 * @return Array $mtsms_array - An array of MTSMS objects that match all the requirements
	 */
	public static function getAllObjByFilter($obj_type, $from = null, $size = null, $must_filters = null, $must_not_filters = null, $start_date = null, $end_date = null, $sort = null)
	{

		//build the basics of our search params for Elasticsearch
		$search_params = self::getBaseFormattedElasticParams($obj_type);

		//set the musts for elasticsearch
		$musts = array();
		if(isset($must_filters))
		{
			foreach($must_filters as $key => $value)
			{
				if(is_array($value))
				{
					array_push($musts, array('terms' => array($key => $value)));
				}
				else
				{
					array_push($musts, array('term' => array($key => $value)));
				}
			}
		}

		//set the must nots for elasticsearch
		$must_nots = array();
		
		if(isset($must_not_filters)) {
			foreach($must_not_filters as $key => $value) {
				if(is_array($value))
				{
					array_push($must_nots, array('terms' => array($key => implode(',', $value))));
				}
				else
				{
					array_push($must_nots, array('term' => array($key => $value)));
				}
			}
		}

		$search_params['body']['query']['filtered'] = array('query' => array('match_all' => array()),
				'filter' => array('bool' =>
					array('must' => $musts, 'must_not' => $must_nots)
			)
		);

		//set the date range for the query as well
		if(isset($start_date) && $start_date != "" && isset($end_date) && $end_date != "")
		{
			$search_params['body']['filter']['range']['timestamp'] = array('gte' => $start_date, 'lte' => $end_date);
		}
		
		//for paging
		if(isset($from) && isset($size)) {
			//set the size and count
			$search_params['size'] = $size;
			$search_params['from'] = $from;
		}
		
		if(isset($last_date) && $last_date != "") {
			//set the date threshold, we want to ignore any assets that expire after the threshold date
			//$search_params['body']['filter']['not']['range']['updated_at'] = array('gte' => $last_date);
		}
		
		//set the sorting params
		if(isset($sort) && $sort != "")
		{
			$search_params['body']['sort'] = array($sort => array('order'=> 'desc', 'ignore_unmapped' => true));
		}
		else
		{
			$search_params['body']['sort'] = array('timestamp' => array('order'=> 'desc', 'missing' => PHP_INT_MAX -1));
		}
		
		
		return self::runElasticSearchForArray($obj_type, $search_params);
	}

	/**
	 * This function returns an array of IDs for a particular document typefor consumption by hungry Plugin
	 *
	 * @param String $obj_type - The type of object we are searching ("MTSMS" or "MOSMS")
	 * @param int $scroll_id - The scroll ID to use to return the next set of results
	 *
	 * @return Array - Returns  array of IDs as well as the new scroll ID that can be used to scroll through data
	 */
	public static function getAllIDsByScrollID($scroll_id)
	{

		//build the basics of our search params for Elasticsearch
		$search_params = array();
		$search_params['scroll_id'] = $scroll_id;
		$search_params['scroll'] = "5m";


		return self::runElasticSearchForIDs($search_params);
	}

	/**
	 * A faster version that returns IDs of the objects only rather than full objects
	 *
	 * @param String $obj_type - The type of object we are searching ("MTSMS" or "MOSMS")
	 * @param int $from - The key to pull from
	 * @param int $size - The number of records to pull
	 * @param Array $must_filters - The filters that the query must match [eg array('batch_id', '9999') means the batch_id must match 9999]
	 * @param Array $must_not_filters - The filters that the query must not match [eg array('batch_id', '9999') means the batch_id must NOT match 9999]
	 * @param String $start_date - The start of the date range that the records must match
	 * @param String $end_date - The end of the date range that the records must match
	 * @param String $sort - What key to sort by
	 *
	 * @return Array $mtsms_array - An array of MTSMS objects that match all the requirements
	 */
	public static function getAllIDsByFilter($obj_type, $from = null, $size = null, $must_filters = null, $must_not_filters = null, $start_date = null, $end_date = null, $sort = null)
	{

		//build the basics of our search params for Elasticsearch
		$search_params = self::getBaseFormattedElasticParams($obj_type);
		$search_params['fields'] = array('mtsms_id'); //set this so it only returns IDs and no document data

		//set the musts for elasticsearch
		$musts = array();
		if(isset($must_filters))
		{
			foreach($must_filters as $key => $value)
			{
				array_push($musts, array('term' => array($key => $value)));
			}
		}

		//set the must nots for elasticsearch
		$must_nots = array();

		if(isset($must_not_filters)) {
			foreach($must_not_filters as $key => $value) {
				array_push($must_nots, array('term' => array($key => $value)));
			}
		}

		$search_params['body']['query']['filtered'] = array('query' => array('match_all' => array()),
			'filter' => array('bool' =>
				array('must' => $musts, 'must_not' => $must_nots)
			)
		);

		//set the date range for the query as well
		if(isset($start_date) && $start_date != "" && isset($end_date) && $end_date != "")
		{
			$search_params['body']['filter']['range']['timestamp'] = array('gte' => $start_date, 'lte' => $end_date);
		}

		//for paging
		if(isset($from) && isset($size)) {
			//set the size and count
			$search_params['size'] = $size;
			$search_params['from'] = $from;
		}

		if(isset($last_date) && $last_date != "") {
			//set the date threshold, we want to ignore any assets that expire after the threshold date
			//$search_params['body']['filter']['not']['range']['updated_at'] = array('gte' => $last_date);
		}

		//set the sorting params
		if(isset($sort) && $sort != "") {
			$search_params['body']['sort'] = array($sort => array('order'=> 'desc', 'ignore_unmapped' => true));
		}
		else {
			$search_params['body']['sort'] = array('timestamp' => array('order'=> 'desc', 'missing' => PHP_INT_MAX -1));
		}

		return self::runElasticSearchForIDs($search_params);
	}

	/**
	 * This function returns an array of MTSMS objects for consumption by hungry Plugin
	 *
	 * @param String $obj_type - The type of object we are searching ("MTSMS" or "MOSMS")
	 * @param int $size - The number of records to pull per scroll call
	 * @param Array $must_filters - The filters that the query must match [eg array('batch_id', '9999') means the batch_id must match 9999]
	 * @param Array $must_not_filters - The filters that the query must not match [eg array('batch_id', '9999') means the batch_id must NOT match 9999]
	 * @param String $start_date - The start of the date range that the records must match
	 * @param String $end_date - The end of the date range that the records must match
	 * @param Array $fields_to_return - Restrict the document fields that are returned.
	 *
	 * @return String $scroll_id - Returns an ID that can be used to scroll through data
	 */
	public static function getScrollIDByFilter($obj_type, $size = null, $must_filters = null, $must_not_filters = null, $start_date = null, $end_date = null, $fields_to_return = null)
	{

		//build the basics of our search params for Elasticsearch
		$search_params = self::getBaseFormattedElasticParams($obj_type);
		$search_params['search_type'] = "scan";
		$search_params['scroll'] = "5m";
		$search_params['size'] = ($size / CONFIG_PLUG_ELASTIC_SHARD_COUNT);

		if(isset($fields_to_return))
		{
			$search_params['fields'] = $fields_to_return;  //set this so it only returns IDs and no document data
		}

		//set the musts for elasticsearch
		$musts = array();
		if(isset($must_filters))
		{
			foreach($must_filters as $key => $value)
			{
				array_push($musts, array('term' => array($key => $value)));
			}
		}

		//set the must nots for elasticsearch
		$must_nots = array();
		if(isset($must_not_filters)) {
			foreach($must_not_filters as $key => $value) {
				array_push($must_nots, array('term' => array($key => $value)));
			}
		}

		//build the total json query (as array) for elasticsearch using our previous arrays
		$search_params['body']['query']['filtered'] = array('query' => array('match_all' => array()),
			'filter' => array('bool' =>
				array('must' => $musts, 'must_not' => $must_nots)
			)
		);

		//set the date range for the query as well
		if(isset($start_date) && $start_date != "" && isset($end_date) && $end_date != "")
		{
			$search_params['body']['filter']['range']['timestamp'] = array('gte' => $start_date, 'lte' => $end_date);
		}

		return self::runElasticSearchForScrollID($search_params);
	}

	/**
	 * This function returns an array of MTSMS objects for consumption by hungry Plugin
	 *
	 * @param String $obj_type - The type of object we are searching ("MTSMS" or "MOSMS")
	 * @param int $scroll_id - The scroll ID to use to return the next set of results
	 *
	 * @return String $scroll_id - Returns an ID that can be used to scroll through data
	 */
	public static function getResultsByScrollID($obj_type, $scroll_id)
	{

		//build the basics of our search params for Elasticsearch
		$search_params = array();
		$search_params['scroll_id'] = $scroll_id;
		$search_params['scroll'] = "5m";

		return self::runElasticSearchForArray($obj_type, $search_params);
	}

	/**
	 * This function uses elasticsearch aggreegations to return an array with keys and value counts according to certain filtering conditions
	 *
	 * @param String $obj_type - The type of object we are searching ("MTSMS" or "MOSMS")
	 * @param Array $aggregations (optional) - An array structured like an aggregations query in elasticsearch
	 * @param Array $must_filters (optional) - The filters that the query must match [eg array('batch_id', '9999') means the batch_id must match 9999]
	 * @param Array $must_not_filters (optional) - The filters that the query must not match [eg array('batch_id', '9999') means the batch_id must NOT match 9999]
	 * @param String $start_date (optional) - The start of the date range that the records must match
	 * @param String $end_date (optional) - The end of the date range that the records must match
	 *
	 *
	 * @return Array $mtsms_array - An array of MTSMS objects that match all the requirements
	 */
	public static function getKeyUniqueAggregations($obj_type, $aggregations = null, $must_filters = null, $must_not_filters = null, $start_date = null, $end_date = null)
	{
		try
		{
			//build the basics of our Object search params for Elasticsearch
			$search_params = self::getBaseFormattedElasticParams($obj_type);

			//set the musts for elasticsearch so we filter in all results that match these conditions
			$musts = array();
			if(isset($must_filters))
			{
				foreach($must_filters as $key => $value)
				{
					if(is_array($value))
					{
						array_push($musts, array('terms' => array($key => $value)));
					}
					else
					{
						array_push($musts, array('term' => array($key => $value)));
					}
				}
			}

			//set the must nots for elasticsearch so we filter out results that match these constraints
			$must_nots = array();
			if(isset($must_not_filters))
			{
				foreach($must_not_filters as $key => $value)
				{
					if(is_array($value))
					{
						array_push($must_nots, array('terms' => array($key => $value)));
					}
					else
					{
						array_push($must_nots, array('term' => array($key => $value)));
					}

				}
			}

			//set the date range to limit the results accordingly
			if(isset($start_date) && $start_date != "" && isset($end_date) && $end_date != "")
			{
				array_push($musts, array('range' => array('timestamp' => array('from' => $start_date, 'to' => $end_date))));
				//$search_params['body']['filter']['range']['timestamp'] = array('gte' => $start_date, 'lte' => $end_date);
			}

			//add the musts and must nots to a properly formatted Elasticsearch query array
			$search_params['body']['query']['filtered'] = array('query' => array('match_all' => array()),
				'filter' => array('bool' =>
					array('must' => $musts, 'must_not' => $must_nots)
				)
			);

			//set size to zero because we only want the aggregation values, not the damn records
			$search_params['size'] = 0;

			//add the aggregation query to the full query
			$search_params['body']['aggs'] = $aggregations;

			//now lets get the Elasticsearch client object and run our search
			$elasticClient = ElasticsearchLoader::getElasticClient();

			//run the query in elasticsearch
			$elasticResults = $elasticClient->search($search_params);

			if(isset($elasticResults['aggregations']))
			{
				//return the aggregation for processing
				return $elasticResults['aggregations'];
			}
			else
			{
				return array();
			}
		}
		catch(Error $e)
		{
			clog::info('* We had an error: '.$e->getMessage());
			return array();
		}
	}

	/**
	 * This function does a partial match on not analyzed fields, and is useful for getting partial matches on things such as MSISDNs or IDs
	 *
	 * @param String $obj_type - The type of object we are searching ("MTSMS" or "MOSMS")
	 * @param String $search_key - The key or "column" to run a search on
	 * @param String $search_string - The message content we are searching for, can be partial string, does a LIKE match
	 * @param int $from - The key to pull from
	 * @param int $size - The number of records to pull
	 * @param Array $must_filters - The filters that the search MUST match exactly
	 * @param Array $must_not_filters - The filters that the search MUST NOT match
	 * @param String $sort - A string key (column) to sort by
	 * @param String $start_date (optional) - The start of the date range that the records must match
	 * @param String $end_date (optional) - The end of the date range that the records must match
	 *
	 * @return Array(ElastObjPlugin) $objects - A full array for objects of this type
	 */
	public static function searchByPartialFilterMatch($obj_type, $search_key, $search_string, $from = null, $size = null, $must_filters = null, $must_not_filters = null, $sort = null, $start_date = null, $end_date = null)
	{
		//build the basics of our MTSMS search params for Elasticsearch
		$search_params = self::getBaseFormattedElasticParams($obj_type);

		//set the musts for elasticsearch
		$musts = array();
		if(isset($must_filters)) {
			foreach($must_filters as $key => $value) {
				array_push($musts, array('term' => array($key => $value)));
			}
		}

		//set the must nots for elasticsearch
		$must_nots = array();
		if(isset($must_not_filters)) {
			foreach($must_not_filters as $key => $value) {
				array_push($must_nots, array('term' => array($key => $value)));
			}
		}

		$search_params['body']['query']['filtered'] = array('query' => array('match' => array($search_key => $search_string)),
				'filter' => array('bool' => 
					array('must' => $musts, 'must_not' => $must_nots)
			)
		);

		//set the date range for the query as well
		if(isset($start_date) && $start_date != "" && isset($end_date) && $end_date != "")
		{
			$search_params['body']['filter']['range']['timestamp'] = array('gte' => $start_date, 'lte' => $end_date);
		}
		
		//for paging
		if(isset($from) && isset($size)) {
			//set the size and count
			$search_params['size'] = $size;
			$search_params['from'] = $from;
		}

		//set the sorting params
		if(isset($sort) && $sort != "") {
			$search_params['body']['sort'] = array($sort => array('order'=> 'desc', 'ignore_unmapped' => true));
		}
		else {
			$search_params['body']['sort'] = array('timestamp' => array('order'=> 'desc', 'missing' => PHP_INT_MAX -1));
		}

		return self::runElasticSearchForArray($obj_type, $search_params);
	}

	/**
	 * This function counts the amount of records that match the particular search filters
	 *
	 * @param String $obj_type - The type of object we are searching ("MTSMS" or "MOSMS")
	 * @param Array $must_filters - The filters that the search MUST match exactly
	 * @param Array $must_not_filters - The filters that the search MUST NOT match
	 *
	 * @return int $count - The number fo records that match your search filters
	 */
	public static function count($obj_type, $must_filters = null, $must_not_filters = null)
	{
		//we need to update a record
		$search_params = self::getBaseFormattedElasticParams($obj_type);

		//set the musts for elasticsearch
		$musts = array();
		if(isset($must_filters))
		{
			foreach($must_filters as $key => $value)
			{
				array_push($musts, array('term' => array($key => $value)));
			}
		}

		//set the must nots for elasticsearch
		$must_nots = array();
		if(isset($must_not_filters))
		{
			foreach($must_not_filters as $key => $value)
			{
				array_push($must_nots, array('term' => array($key => $value)));
			}
		}
		
		$search_params['body']['query']['filtered'] = array('query' => array('match_all' => array()),
				'filter' => array('bool' => 
					array('must' => $musts, 'must_not' => $must_nots)
			)
		);
		
		//get the elasticlient
		$elasticClient = ElasticsearchLoader::getElasticClient();
		
		//run the update in elasticsearch
		$elasticCount = $elasticClient->count($search_params);

		return $elasticCount['count'];
	}

	/**
	 * This function counts the amount of records that match a customized elasticsearch query submitted as a PHP array
	 *
	 * @param String $obj_type - The type of object we are searching ("MTSMS" or "MOSMS")
	 * @param Array $query_array - The full elasticsearch query (Not including the initial "body" clause) that the search will be run on
	 *
	 * @return int $count - The number fo records that match your search filters
	 */
	public static function countByManualQueryArray($obj_type, $query_array)
	{
		try {
			//we need to update a record
			$search_params = self::getBaseFormattedElasticParams($obj_type);
			$search_params['body'] = $query_array;

			//get the elasticlient
			$elasticClient = ElasticsearchLoader::getElasticClient();

			//run the update in elasticsearch
			$elasticCount = $elasticClient->count($search_params);

			return $elasticCount['count'];
		}
		catch (Exception $e)
		{
			return 0;
		}
	}
	
	/*************************************************
	 * 
	 * NON STATIC FUNCTIONS, OBJECT FUNCTIONS
	 * 
	 *************************************************/

	/**
	 * Saves an editted or new object!
	 * 
	 * @return $asset_group_id - The ID of the AssetGroup

	public function save($user_id) {
		//we need to do some elasticsearch stuff
		$elasticClient = ElasticsearchLoader::getElasticClientNoDBCheck();
		
		//are we creating an ew one or editting one. -1 means it's brand new
		if((isset($this->api_is_new) && $this->api_is_new == true) || !isset($this->id)) {
			/**
			 * WE ARE SAVING A NEW OBJECT, SO LETS DO SOME INIT WORK

			 
			 
			//build the asset group
			$insert_params = ElasticsearchLoader::generateBasicAssetGroupDocument($this->client_id);
			
			foreach($this->_data as $key => $value ) {
				if($key != 'initial_take' && $key != 'api_is_new' && $key != 'id' && $key != 'audit_string') { //dont wanna save that
					$insert_params['body'][$key] = $value;
				}
			}
			
			$this->audit_string = "CREATED this asset group record. ".$this->audit_string;
			$audit_arr = array();
			$audit_arr['user_id'] = $user_id;
			$audit_arr['actions'] = $this->audit_string;
			$audit_arr['created_at'] = date('Y-m-d H:i:s');
			$insert_params['body']['audit'][0] = $audit_arr;
			
			//bolt on the ID if it was set
			if(isset($this->id)) {
				$insert_params['id'] = $this->id;
			}
			
			//save it
			$save_result = $elasticClient->index($insert_params);
			$this->id = $save_result['_id'];
				
			//dont automatically generate the asset objects if this rtecord is coming in new from the ANdroid application, they are generated and saved from there
			if(!isset($this->api_is_new) || $this->api_is_new == false) {
				//we must automatically generate enough asset objects for this asset groups quanitity
				$asset_obj_ids = array();
				for($i = 0; $i < $this->quantity; ++$i) {
					
					
					/*******************************
					 * 
				 	 * We need to do some calculation for the end of this assets useful life
					 * 
				 	 ********************************
					$client_subcat_config = ClientUsefulLife::where('client_id', $this->client_id)->where('subcat_id', $this->subcategory_id)->first();
					//check if exists, else use the default
					if(isset($client_subcat_config)) {
						$useful_life_years = $client_subcat_config->useful_life;
					}
					else {
						$useful_life_years = ASSET_DEFAULT_USEFUL_LIFE;
					}
					
					//useful life calcualtions
					$now_date = new DateTime();
					$lifetime_end_date = new DateTime($this->purchased_at);
					$lifetime_end_date->modify('+ '.$useful_life_years.' years');
					
					$new_barcode = "";
					//finally, lets save the new asset object
					$insert_obj_params = ElasticsearchLoader::generateBasicAssetObjectDocument($this->client_id, $this->user_id, $this->id, $lifetime_end_date->format('Y-m-d H:i:s'));
					if(isset($this->initial_take)) {
						$new_barcode = $this->initial_take['barcode'];
						$insert_obj_params['body']['barcode'] = $this->initial_take['barcode'];
						$insert_obj_params['body']['grade'] = $this->initial_take['grade'];
						$insert_obj_params['body']['usage'] = $this->initial_take['usage'];
						$insert_obj_params['body']['captures'][0] = $this->initial_take;
					}
					
					$audit_arr = array();
					$audit_arr['user_id'] = $user_id;
					$audit_arr['actions'] = "CREATED this asset object record.";
					$audit_arr['created_at'] = date('Y-m-d H:i:s');
					$insert_obj_params['body']['audit'][0] = $audit_arr;
					
					$save_obj_result = $elasticClient->index($insert_obj_params);
					
					$asset_obj = array('barcode' => $new_barcode, 'asset_id' => $save_obj_result['_id']);
					array_push($asset_obj_ids, $asset_obj);
				}
				
				$update_barcode_params = ElasticsearchLoader::generateBasicAssetGroupDocument($this->client_id);
				$update_barcode_params['id'] = $this->id;
				//the reply data to update				
				$update_barcode_params['body'] = array(
					'doc' => array(
	    			'assets' => $asset_obj_ids
					)
				);
				//run the update in elasticsearch
				$elasticClient->update($update_barcode_params);
			}
			
		}	
		else {
			/**
			 * WE ARE UPDATING A PREVIOUSLY SAVED OBJECT, SO NO WORRIES
			 *
		
			//we need to update a record
			$update_params = ElasticsearchLoader::getElasticsearchAssetGroupParams();
			$update_params['id'] = $this->id;
			
			//set the updated variables
			foreach($this->_data as $key => $value ) {
				if($key != 'id' && $key != 'audit_string') {
					$update_params['body']['doc'][$key] = $value;
				}
			}
			
			//set the audit record for this change
			if(!isset($this->audit)) {
				$this->audit = array();
			}
			$audit_arr = array();
			$audit_arr['user_id'] = $user_id;
			$audit_arr['actions'] = $this->audit_string;
			$audit_arr['created_at'] = date('Y-m-d H:i:s');
			array_push($this->audit, $audit_arr);
			
			//set the updated at field
			$update_params['body']['doc']['updated_at'] = date('Y-m-d H:i:s');
			//run the update in elasticsearch
			$elasticClient->update($update_params);
			
			//get all this asset groups objects to update their expiry dates
			$asset_objs_result = AssetObject::getAllByAssetGroup($this->id);
			$asset_list = $asset_objs_result['objects'];
			
			foreach($asset_list as $asset_obj) {
				/*******************************
				 * 
			 	 * We need to do some calculation for the end of this assets useful life
				 * 
			 	 ********************************
			 	//now calculate some stats for the asset life and depreciation and save it to the actual object
				if(isset($asset_obj->useful_life_correction_years) && $asset_obj->useful_life_correction_years != 0 && $asset_obj->useful_life_correction_years != "") {
					$useful_life_years = $asset_obj->useful_life_correction_years;
				}
				else {
					$client_subcat_config = ClientUsefulLife::where('client_id', $this->client_id)->where('subcat_id', $this->subcategory_id)->first();
					//check if exists, else use the default
					if(isset($client_subcat_config)) {
						$useful_life_years = $client_subcat_config->useful_life;
					}
					else {
						$useful_life_years = ASSET_DEFAULT_USEFUL_LIFE;
					}
				}

				//useful life calcualtions
				$now_date = new DateTime();
				$lifetime_end_date = new DateTime($this->purchased_at);
				$lifetime_end_date->modify('+ '.$useful_life_years.' years');
				
				//finally, lets save the new asset object
				$asset_obj->useful_life_end_at = $lifetime_end_date->format('Y-m-d H:i:s');
				$asset_obj->save($user_id);
			}

		}	
		
		return $this->id;
	}
	 */

}
