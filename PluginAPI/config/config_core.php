<?php

if(gethostname() == 'appstaging1') {
    $test = true;
} else {
    $test = false;
}


define('CONFIG_PLUG_PREFIX_URL', ($test ? 'https://portal2.connet-systems.com/' : 'https://plugin.connet-systems.com/'));

////////////////////////////
// ELASTICS
////////////////////////////
define('CONFIG_PLUG_ELASTIC_DB_VERSION', 1);
define('CONFIG_PLUG_ELASTIC_SHARD_COUNT', 5);

define('CONFIG_PLUG_REDIS_HOST', ($test ? 'appstaging1' : 'redismaster'));

//define('CONFIG_AKH_REDIS_HOST', ($test ? 'appstaging1' : 'redismaster'));
define('CONFIG_PLUG_ELASTIC_HOST', 'elasticmaster:9200');

define('CONFIG_PLUG_ELASTIC_CORE_INDEX', 'core'); //main index
define('CONFIG_PLUG_ELASTIC_CORE_DOC_MTSMS', 'core_mtsms'); //mtsms document type
define('CONFIG_PLUG_ELASTIC_CORE_DOC_MOSMS', 'core_mosms'); //mo document type

define('CONFIG_PLUG_MTSMS_TYPE', 'MTSMS');
define('CONFIG_PLUG_MOSMS_TYPE', 'MOSMS');