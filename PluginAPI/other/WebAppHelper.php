<?php
/**
 * This model handles all tasks based on MTSMS objects
 *
 * @author Doug Jenkinson
 */

class WebAppHelper
{

	public function __construct(){}

	public static function getBaseServerURL()
	{
		if (!isset($_SERVER["HTTPS"]) && strpos($_SERVER['SERVER_ADDR'], '10.') !== 0)
		{
			$protocol = "http://";
		}
		else
		{
			$protocol = "https://";
		}


		$server_url = $protocol.$_SERVER['HTTP_HOST'];

		return $server_url;
	}

}
