<?php
/**
 * This is an Autoloader file for Plugin, it loads PHP models that can be used within any PHP project located on the CONNET servers.
 *
 * Please note that this autoloader
 * @author Doug Jenkinson
 */

if (!class_exists('ConnetAutoloader', false)) {

    define('ERROR_REPORTING', true);
    define('LOG_FILENAME', '/var/log/connet/plugin/' . basename(__FILE__, '.php') . '.log');
    require_once('ConnetAutoloader.php');
}

clog::setLogFilename("/var/log/connet/plugin/" . basename(__FILE__, '.php') . '.log');
clog::setBacktrace();
clog::setVerbose(ERROR_REPORTING);

/***********************************************
 * Load main requirements and helpers
 ***********************************************/

//we need composer installed on the project, with Elasticsearch PHP installed through autoloader, this links to the vendor/autoload.php file
require_once(__DIR__ . '/../vendor/autoload.php');
require_once(__DIR__ . '/config/config_core.php');

//basic helpers
require_once(__DIR__ . '/other/WebAppHelper.php');
//for MYSQL
require_once(__DIR__ . '/sqlhelpers/dbAdapter.php');
require_once(__DIR__ . '/sqlhelpers/sqlHelperAccounts.php');
require_once(__DIR__ . '/sqlhelpers/sqlHelperUsers.php');
require_once(__DIR__ . '/sqlhelpers/sqlHelperRoutes.php');
require_once(__DIR__ . '/sqlhelpers/sqlHelperServices.php');
require_once(__DIR__ . '/sqlhelpers/sqlHelperBatches.php');
require_once(__DIR__ . '/../helpers/DeliveryReportHelper.php');


//elasticsearch handling
require_once(__DIR__ . '/elasticsearch/ElasticsearchLoader.php');
require_once(__DIR__ . '/elasticsearch/ElasticsearchPluginObject.php');



/***********************************************
 * Load the models
 ***********************************************/

require_once(__DIR__ . '/models/MTSMS.php');
require_once(__DIR__ . '/models/MOSMS.php');
require_once(__DIR__ . '/models/Batch.php');
require_once(__DIR__ . '/models/Account.php');
require_once(__DIR__ . '/models/Service.php');
require_once(__DIR__ . '/models/ServiceRoute.php');
require_once(__DIR__ . '/models/User.php');

