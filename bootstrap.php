<?php

//session_start();

define('SITE_BASE_URL', '/');
define('SITE_BASE_PATH', __DIR__ . '/');

//--------------------------------------------------------------
// Framework
//--------------------------------------------------------------   
define('ERROR_REPORTING', false ); //(gethostname() == 'appstaging1'));
require_once('ConnetAutoloader.php');
preset::base();

clog::setHtml();


require_once 'vendor/autoload.php';

//--------------------------------------------------------------
// Cache Control
//--------------------------------------------------------------     
header("Cache-Control: no-store, no-cache, must-revalidate, max-age=0");
header("Cache-Control: post-check=0, pre-check=0", false);
header("Pragma: no-cache");

//--------------------------------------------------------------
// Script
//--------------------------------------------------------------     
$_SERVER['SCRIPT_FILENAME'] = realpath(__DIR__ . '/' . $_SERVER['SCRIPT_NAME']);

if ($_SERVER['SCRIPT_FILENAME'] == false || $_SERVER['SCRIPT_FILENAME'] == __FILE__ || strpos($_SERVER['SCRIPT_FILENAME'], __DIR__) === false) {

   require_once(__DIR__ . '/404page.php');
   die();
}

//--------------------------------------------------------------
// Helpers
//--------------------------------------------------------------  

require_once('helpers/PermissionsHelper.php');
require_once('helpers/LoginHelper.php');
require_once('helpers/TemplateHelper.php');
require_once('helpers/DeliveryReportHelper.php');

//--------------------------------------------------------------
// Includes
//--------------------------------------------------------------  
require_once('php/config/dbConn.php');
require_once('php/allInclusive.php');


//--------------------------------------------------------------
// Include Script
//--------------------------------------------------------------  
ob_start();
chdir(dirname($_SERVER['SCRIPT_FILENAME']));
require($_SERVER['SCRIPT_FILENAME']);

//********************** end of file ******************************
