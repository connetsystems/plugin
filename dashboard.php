<?php
//-------------------------------------
// NB THIS IS INCOMPLETE, WORK IN PROGRESS (DOUG)
//--------------------------------------


/**
 * Include the header tempalte which sets up the HTML
 *
 * Don't forget to include template_import_script before any Javascripting
 * Don't forget to include template footer.php at the bottom of the page as well
 */
//-------------------------------------------------------------
// Template
//-------------------------------------------------------------
TemplateHelper::setPageTitle('Dashboard');
TemplateHelper::initialize();

//-------------------------------------------------------------
// Permissions / Sub-Accounts & Routes
//-------------------------------------------------------------
$defaultRouteData = getDefaultRouteData($_SESSION['serviceId']);
$clients = PermissionsHelper::getAllServicesWithPermissions();

$split = explode(',', getSubAccounts($_SESSION['accountId']));
foreach ($split as $index => $_service_id) {

   if (!isset($clients[(integer) $_service_id])) {

      //clog('Filtered:', $_service_id);
      unset($split[$index]);
   }
}
$subAccounts = implode(',', $split);

//-------------------------------------------------------------
// Elasticsearch Queries (Nightmare...)
//-------------------------------------------------------------
// <editor-fold defaultstate="collapsed">
$startDate = mktime(0, 0, 0) * 1000;
$endDate = (time() * 1000);

$batch_list = getBatchList($cl, $startDate, $endDate);
$batch_list_with_stats = getBatchlistStats($batch_list);

?>
<!-- Right side column. Contains the navbar and content of the page -->
<aside class="right-side">
   <!-- Content Header (Page header) -->
   <section class="content-header">
      <h1>
         Dashboard <small id="loading_status"></small>
         <div style="display:inline-block;float:right;font-size:10pt;padding-top:7px;">
            Time since last update: <div style="display:inline-block;" id="timing"></div> s
         </div>
      </h1>
   </section>
   <!-- Small buttons at Top -->
   <section class="content">
      <div class="row">
         <div class="col-lg-9 col-xs-12">


         </div>
         <div class="col-lg-3 col-xs-12">
            <div class="box box-connet" style="padding-bottom:1px; border-top-color:<?php echo $accRGB; ?>;">
               <div class="box-header">
                  <h3 class="box-title">Latest Sends</h3>
               </div><!-- /.box-header -->
               <div class="box-body table-responsive">
                  <?php foreach($batch_list_with_stats as $batch) { ?>
                  <div class="row">
                     <div class="col-lg-12">
                        <?php echo $value['batch_reference']; ?>
                     </div>
                  </div>

                  <?php } ?>



               </div>
            </div>
         </div>
      </div>

      <div class="row">
         <div class="col-lg-2 col-xs-6">
            <!-- small box -->
            <div class="small-box bg-connetBlue">
               <div class="inner">
                  <h3>
                     <div id="sentDiv2" style="color:#ffffff;font-size:20pt;">
                        &nbsp;
                     </div>
                  </h3>
                  <!--p style="color:#ffffff">
                      &nbsp;
                  </p-->
               </div>
               <!--div class="icon" id="sendDiv2" style="color:#ffffff;font-size:26pt;padding-bottom:15px;display:block;">

               </div-->
               <a class="small-box-footer">
                  SENT
               </a>
            </div>
         </div><!-- ./col -->
         <div class="col-lg-2 col-xs-6">
            <!-- small box -->
            <div class="small-box bg-connetYellow">
               <div class="inner">
                  <h3>
                     <div id="pendDiv2" style="color:#ffffff;font-size:20pt;">
                        &nbsp;
                     </div>
                  </h3>
                  <!--p style="color:#ffffff">
                     &nbsp;
                  </p-->
               </div>
               <!--div class="icon" id="pendDiv2" style="color:#ffffff;font-size:26pt;padding-bottom:15px;display:block;">
                   0%
               </div-->
               <a class="small-box-footer">
                  PENDING
               </a>
            </div>
         </div><!-- ./col -->
         <div class="col-lg-2 col-xs-6">
            <!-- small box -->
            <div class="small-box bg-connetGreen">
               <div class="inner">
                  <h3>
                     <div id="deliDiv2" style="color:#ffffff;font-size:20pt;">
                        &nbsp;
                     </div>
                  </h3>
                  <!--p style="color:#ffffff">
                     &nbsp;
                  </p-->
               </div>
               <!--div class="icon" id="deliDiv2" style="color:#ffffff;font-size:26pt;padding-bottom:15px;display:block;">
                   0%
               </div-->
               <a class="small-box-footer">
                  DELIVERED
               </a>
            </div>
         </div><!-- ./col -->
         <div class="col-lg-2 col-xs-6">
            <!-- small box -->
            <div class="small-box bg-red">
               <div class="inner">
                  <h3>
                     <div id="failDiv2" style="color:#ffffff;font-size:20pt;">
                        &nbsp;
                     </div>
                  </h3>
                  <!--p style="color:#ffffff">
                      &nbsp;
                  </p-->
               </div>
               <!--div class="icon" id="failDiv2" style="color:#ffffff;font-size:26pt;padding-bottom:15px;display:block;">
                   0%
               </div-->
               <a class="small-box-footer">
                  FAILED
               </a>
            </div>
         </div><!-- ./col -->
         <div class="col-lg-2 col-xs-6">
            <!-- small box -->
            <div class="small-box bg-connetRed">
               <div class="inner">
                  <h3>
                     <div id="rejeDiv2" style="color:#ffffff;font-size:20pt;">
                        &nbsp;
                     </div>
                  </h3>
                  <!--p style="color:#ffffff">
                      &nbsp;
                  </p-->
               </div>
               <!--div class="icon" id="rejeDiv2" style="color:#ffffff;font-size:26pt;padding-bottom:15px;display:block;">
                   0%
               </div-->
               <a class="small-box-footer">
                  REJECTED
               </a>
            </div>
         </div>
         <div class="col-lg-2 col-xs-6">
            <!-- small box -->
            <div class="small-box bg-aqua">
               <div class="inner">
                  <h3>
                     <div id="moDiv" style="color:#ffffff;font-size:20pt;">
                        &nbsp;
                     </div>
                  </h3>
                  <!--p style="color:#ffffff">
                      &nbsp;
                  </p-->
               </div>
               <!--div class="icon" id="rejeDiv2" style="color:#ffffff;font-size:26pt;padding-bottom:15px;display:block;">
                   0%
               </div-->
               <a class="small-box-footer">
                  REPLIES
               </a>
            </div>
         </div><!-- ./col -->
      </div><!-- /.row -->
   </section><!-- /.content -->

   <section class="content">
      <div class="box box-connet" style="padding-bottom:1px; border-top-color:<?php echo $accRGB; ?>;">
         <div class="box-header">
            <h3 class="box-title">Network Traffic</h3>
         </div><!-- /.box-header -->
         <div class="box-body table-responsive">
            <table id="example2" class="table table-bordered table-striped dataTable table-hover">
               <thead>
                  <tr>
                     <th>Network</th>
                     <th>Country</th>
                     <th style="text-align:right;">Sent&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</th>
                     <th style="text-align:right;">Units&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</th>
                     <th style="text-align:right;">Cost per Unit&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</th>
                     <th style="text-align:right;">Total Cost&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</th>
                  </tr>
               </thead>
               <tbody>
                  <?php
                  /* $finalCost = 0;
                    $totalSent = 0; */
                  /* echo '<pre>';
                    print_r($netName);
                    echo '</pre>'; */
                  foreach ($netName as $key => $value) {
                     if (strpos($value['0'], 'Special')) {
                        $logo = trim('Special');
                     } else {
                        $logo = $value['0'];
                        $logoP = strpos($value['0'], ' / ');
                        //echo '<br>logoP='.$logoP.'<-';
                        if ($logoP != '') {
                           $logo = substr($logo, $logoP + 3, strlen($logo));
                        }
                        $logo = trim($logo);
                     }

                     if ($logo == '') {
                        $logo = 'Special';
                     }

                     $flag = str_replace(' / ', '', $value['1']);
                     if ($flag == '') {
                        $flag = 'Special';
                     }

                     echo '<tr>';
                     //echo '<td id="'.$value['mccmnc'].'name">'.$value['service_name'].'</td>';
                     if (strpos($value['0'], 'Special')) {
                        echo '<td><img src="../img/serviceproviders/Special.png">&nbsp;&nbsp;' . $value['0'] . ' (' . $key . ')' . '</td>';
                     } else {
                        echo '<td><img src="../img/serviceproviders/' . $logo . '.png">&nbsp;&nbsp;' . $value['0'] . ' (' . $key . ')' . '</td>';
                     }

                     echo '<td><img src="../img/flags/' . $flag . '.png">&nbsp;&nbsp;' . $value['1'] . '</td>';

                     echo '<td style="text-align:right;" id="' . $key . 'sent">' . number_format($value['5'], 0, '.', ' ') . '</td>';
                     echo '<td style="text-align:right;" id="' . $key . 'units">' . number_format($value['2'], 0, '.', ' ') . '</td>';
                     $cCost = getResellerNetCost($_SESSION['serviceId'], $key);
                     echo '<td style="text-align:right;" id="' . $key . 'costs">' . number_format(($cCost / 10000), 4, '.', ' ') . '</td>';
                     //echo '<td style="text-align:right;" id="'.$key.'costs">'.number_format(($value['3']/10000), 4, '.', ' ').'</td>';
                     /* if($value['route_currency'] == 'ZAR')
                       {
                       $u = 'R ';
                       }
                       elseif($value['route_currency'] == 'EUR')
                       {
                       $u = '&euro; ';
                       }
                       else
                       {
                       $u = '';
                       } */

                     $sNaC = getServiceNameAndCurrency($_SESSION['serviceId'], $key);

                     if (isset($sNaC)) {

                        if (isset($sNaC->num_rows)) {
                           /* if($sNaC->num_rows == 0)
                             {
                             echo '<br>';
                             print_r($sNaC);
                             echo '<br>sn='.$value['0'];
                             echo '<br>mc='.$value['4'];

                             } */
                           foreach ($sNaC as $key2 => $va) {
                              $sN = $va['service_name'];
                           }
                        } else {
                           $sN = '--';
                        }
                     } else {
                        $sN = '-';
                     }

                     echo '<td style="text-align:right;" id="' . $key . 'total">' . number_format(($value['2'] * $cCost / 10000), 2, '.', ' ') . '</td>';
                     echo '</tr>';
                  }
                  ?>
               </tbody>
               <tfoot>
                  <tr>
                     <th></th>
                     <th></th>
                     <th style="text-align:right;" id="totalSentsCell"></th>
                     <th style="text-align:right;" id="totalUnitsCell"></th>
                     <th style="text-align:right;"></th>
                     <th style="text-align:right;" id="totalSentsCost"></th>
                  </tr>
               </tfoot>
            </table>
         </div><!-- /.box-body -->
      </div>
   </section>


   <section class="content">
      <div class="box box-connet" style="padding-bottom:1px; border-top-color:<?php echo $accRGB; ?>;">
         <div class="box-header">
            <h3 class="box-title">Service Traffic</h3>
         </div><!-- /.box-header -->
         <div class="box-body table-responsive">
            <table id="example2" class="table table-bordered table-striped dataTable table-hover">
               <thead>
                  <tr>
                     <th>Service Name</th>
                     <th style="text-align:right;">Sent</th>
                     <th style="text-align:right;">Units</th>
                     <!--th style="text-align:right;">Total Cost</th-->
                  </tr>
               </thead>
               <tbody>
                  <?php
                  foreach ($serviceData as $key => $value) {
                     /* echo '<pre>';
                       print_r($value);
                       echo '</pre>'; */
                     echo '<tr>';
                     echo '<td>';
                     echo getServiceName($key);
                     echo '</td>';
                     echo '<td style="text-align:right;" id="' . $key . 'units">';
                     echo number_format($value['2'], 0, '.', ' ');
                     echo '</td>';
                     echo '<td style="text-align:right;" id="' . $key . 'sents">';
                     echo number_format($value['0'], 0, '.', ' ');
                     echo '</td>';
                     echo '</tr>';
                  }
                  ?>
               </tbody>
               <tfoot>
                  <tr>
                     <th></th>
                     <th style="text-align:right;" id="totalSentsCellService"></th>
                     <th style="text-align:right;" id="totalUnitsCellService"></th>
                     <!--th style="text-align:right;" id="totalSentsCostService"></th-->
                  </tr>
               </tfoot>
            </table>
         </div>
      </div>
   </section>

   <section class="content">
      <div class="panel box box-warning" style="padding-bottom:1px; border-top-color:<?php echo $accRGB; ?>;">
         <div class="box-header">
            <h4 class="box-title">

               Routes & Rates

            </h4>
         </div>
         <div id="collapse2" class="box-body panel-collapse">
            <div class="callout callout-info" style='height:auto;margin-bottom:-0px;'>
               <div style="width:100%;height:auto;" >
                  <!--h4>Routes Details</h4-->
                  <p>These are the countries, routes and rates associated with this account.
                  </p>
                  <!--a class="btn btn-block btn-social btn-dropbox " style="background-color: #367fa9; color: #ffffff; border: 2px solid; border-radius: 6px !important; width: 200px;" onclick="toggleContent('addRoute');">
                      <i class="fa fa-link"></i>Add New Routes
                  </a-->
               </div>
            </div>
            <div class="box-body">
               <div class="box-body table-responsive no-padding">
                  <table id="routesTable" class="table table-hover table-bordered table-striped">
                     <thead>
                        <tr role="row">
                           <th class="sorting_asc" role="columnheader" tabindex="0" aria-controls="routesTable" rowspan="1" colspan="1" aria-sort="ascending" aria-label="Country: activate to sort column descending">Country</th>
                           <th>Network</th>
                           <th>Status</th>
                           <th style="text-align:center;">Rate</th>
                           <th style="text-align:center;">Cur</th>
                        </tr>
                     </thead>
                     <tbody>
                        <?php
                        if (isset($defaultRouteData)) {

                           foreach ($defaultRouteData as $key => $value) {
                              if (substr($value['COUNTRY'], 0, 3) == "USA") {
                                 $flag = "USA";
                              } else {
                                 $flag = $value['COUNTRY'];
                              }
                              $flag = str_replace('/', '&', $flag);

                              $s1 = strrpos($value['NETWORK'], '/');
                              $s2 = strrpos($value['NETWORK'], '(');

                              $logo = substr($value['NETWORK'], $s1, $s2);
                              $logo = trim($logo);

                              if ($logo != "Cell C") {  // | $logo != 'Vodacom SA' | $logo != 'MTN-SA'
                                 if ($logo != 'Vodacom SA') {
                                    if ($logo != 'MTN-SA') {
                                       if ($logo != 'Telkom Mobile') {
                                          if ($logo != 'MTN (Nigeria)') {
                                             $logo = substr($value['NETWORK'], $s1 + 1, $s2 - $s1 - 2);
                                             $logo = trim($logo);
                                          }
                                       }
                                    }
                                 }
                              }

                              echo '<tr >';
                              echo '<td style="vertical-align:middle;"><img src="../img/flags/' . $flag . '.png">&nbsp;&nbsp;' . $value['COUNTRY'] . '</td>';
                              echo '<td style="vertical-align:middle;"><img src="../img/serviceproviders/' . $logo . '.png">&nbsp;&nbsp;' . $value['NETWORK'] . '</td>';
                              if ($value['STATUS'] == 'ENABLED') {
                                 echo '<td style="vertical-align:middle;"><i class="fa fa-check" style="color:#00ff00;"></i>&nbsp;&nbsp;' . $value['STATUS'] . '</td>';
                              } else {
                                 echo '<td style="vertical-align:middle;"><i class="fa fa-times" style="color:#ff0000;"></i>&nbsp;&nbsp;' . $value['STATUS'] . '</td>';
                              }

                              if (strpos($value['RATE'], ',')) {
                                 $rateV = strstr($value['RATE'], ',', true);
                              } else {
                                 $rateV = $value['RATE'];
                              }

                              $rdncVp = strrpos($value['RATE'], ',');
                              $rdncV = substr($value['RATE'], $rdncVp + 1);
                              if ($rdncVp == '') {
                                 $rateV = $value['RATE'];
                                 $rdncV = '0';
                              }
                              if (strlen($rdncV) < 4) {
                                 $rdncVn = '';
                                 for ($c = 0; $c < (4 - strlen($rdncV)); $c++) {
                                    $rdncVn .= '0';
                                 }
                                 $rdncV = $rdncVn . $rdncV;
                              }
                              if (strlen($rateV) < 4) {
                                 $rateVn = '';
                                 for ($c = 0; $c < (4 - strlen($rateV)); $c++) {
                                    $rateVn .= '0';
                                 }
                                 $rateV = $rateVn . $rateV;
                              }

                              echo '<td style="vertical-align:middle;"><center><div class="fakey1" style="border:0px;padding-left:10px;width:60px;"><div style="display: inline-table;">0.</div>' . (string) $rateV . '</div></center></td>';
                              echo '<td style="text-align:center;">' . $value['route_currency'] . '</td>';
                              echo '</tr>';
                           }
                        }
                        ?>
                     </tbody>
                  </table>
               </div>
            </div>
         </div>
      </div>
   </section>
</aside><!-- /.right-side -->

<?php include("pages/template_import_script.php"); //must import all scripts first  ?>


<script type="text/javascript">
   var counter = 0;
   var counter2 = 0
   var counter3 = 0

   function showTimer(c)
   {
      document.getElementById('timing').innerHTML = c;
   }

   function refreshMOData(sId)
   {
      $("#loading_status").html('<span class="glyphicon glyphicon-refresh glyphicon-spin"></span> Refreshing...');
      //counter++;
      $.ajax({url: 'php/dashHandlerMOClients.php',
         data: {sId: sId},
         type: 'post',
         success: function (output)
         {
            //console.log(output);
            if (output == '')
            {
               output = '0';
            }
            document.getElementById('moDiv').innerHTML = numberWithCommas(output);
            //var barVars = output.split(",");
            //document.getElementById('sentDiv').innerHTML = numberWithCommas(barVars[0]);
            $("#loading_status").html('');
         }
      });
   }

   function refreshDataServiceNew(reCount)
   {
      $("#loading_status").html('<span class="glyphicon glyphicon-refresh glyphicon-spin"></span> Refreshing...');
      counter3++;
      var sId = <?php echo '"' . $processedSubAccounts . '"'; ?>;
      $.ajax({url: 'php/dashHandlerResellerServicesNew.php',
         data: {action: reCount, sId: sId},
         //data: {sId: sId},
         type: 'post',
         success: function (output)
         {
            console.log(output);
            var serviceSentTot = 0;
            var serviceCostTot = 0;
            var tempCost = 0;

            p = JSON.parse(output);
            for (var key in p)
            {
               if (p.hasOwnProperty(key))
               {
                  var tempTot = 0;
                  for (var key2 in p[key])
                  {
                     if (p[key].hasOwnProperty(key2))
                     {
                        if (key2 == 'total')
                        {
                           var toters = p[key][key2].split(";");
                           serviceSentTot += parseInt(toters[0]);
                           tempTot += parseInt(toters[0]);
                           serviceCostTot += parseInt(toters[1]);
                           //serviceSentTot += p[key][key2];
                           //tempTot = p[key][key2];
                           //console.log(key2);
                           var idunits = key + 'units';
                           var idsents = key + 'sents';
                           document.getElementById(idsents).innerHTML = numberWithCommas(toters[1]);
                           document.getElementById(idunits).innerHTML = numberWithCommas(toters[0]);
                           //document.getElementById(idunits).innerHTML = numberWithCommas(p[key][key2]);
                           //document.getElementById(idsents).innerHTML = numberWithCommas(p[key][key2]);
                           /*
                            var idtotal = key+'total';
                            
                            //console.log(key2 + " -> " + p[key][key2]);
                            // console.log(key);
                            
                            
                            var cost = document.getElementById(idcosts).innerHTML;
                            costTot += (p[key][key2] * cost);
                            document.getElementById(idtotal).innerHTML = numberWithCommas((Math.round((p[key][key2] * cost) * 100) / 100).toFixed(2));
                            */
                        } else
                        {
                           /*if(key2 != 0)
                            {
                            tempCost = key2;
                            tempTot += p[key][key2];
                            }
                            else
                            {
                            tempCost = 0;
                            tempTot += p[key][key2];
                            }
                            var idcosts = key+'total';
                            console.log('sCT=' + idcosts);
                            document.getElementById(idcosts).innerHTML = numberWithCommas((Math.round((tempCost * tempTot/10000) * 100) / 100).toFixed(2));
                            serviceCostTot += (Math.round((tempCost * tempTot/10000) * 100) / 100);*/
                           /*console.log('sCT=' + (Math.round((tempCost * tempTot/10000) * 100) / 100));
                            console.log('sCT=' + serviceCostTot);*/
                        }
                     }
                  }
               }
            }
            document.getElementById('totalSentsCellService').innerHTML = numberWithCommas(serviceSentTot);
            document.getElementById('totalUnitsCellService').innerHTML = numberWithCommas(serviceCostTot);
            //document.getElementById('totalSentsCostService').innerHTML = numberWithCommas(parseFloat(serviceCostTot).toFixed(2));

            $("#loading_status").html('');
         }
      });
   }

   function refreshDataNew(reCount)
   {
      $("#loading_status").html('<span class="glyphicon glyphicon-refresh glyphicon-spin"></span> Refreshing...');
      counter++;
      var sId = <?php echo '"' . $processedSubAccounts . '"'; ?>;
      //alert('sId='+sId);
      //"query": "service_id:450 or service_id:451",
      $.ajax({url: 'php/dashHandlerResellerNew.php',
         data: {action: reCount, sId: sId},
         //data: {sId: sId},
         type: 'post',
         success: function (output)
         {
            console.log(output);
            p = JSON.parse(output);

            var mccmncTot = 0;
            var unitsTot = 0;
            var costTot = 0;

            var pendBox = 0;
            var deliBox = 0;
            var failBox = 0;
            var rejeBox = 0;

            for (var key in p)
            {
               if (p.hasOwnProperty(key))
               {
                  for (var key2 in p[key])
                  {
                     if (p[key].hasOwnProperty(key2))
                     {
                        if (key2 == 'total')
                        {
                           var toters = p[key][key2].split(";");
                           mccmncTot += parseInt(toters[0]);
                           unitsTot += parseInt(toters[1]);
                           var idsents = key + 'sent';
                           var idunits = key + 'units';
                           var idcosts = key + 'costs';
                           var idtotal = key + 'total';

                           console.log(key2 + " -> " + p[key][key2]);
                           console.log(key);
                           console.log(toters);

                           document.getElementById(idsents).innerHTML = numberWithCommas(toters[0]);
                           document.getElementById(idunits).innerHTML = numberWithCommas(toters[1]);
                           var cost = document.getElementById(idcosts).innerHTML;
                           costTot += (toters[1] * cost);
                           document.getElementById(idtotal).innerHTML = numberWithCommas((Math.round((toters[1] * cost) * 100) / 100).toFixed(2));
                        } else
                        {
                           if ((key2 & 32) == 32 && (key2 & 1) == 0 && (key2 & 2) == 0 && (key2 & 16) == 0)
                           {
                              pendBox += p[key][key2];
                           } else if ((key2 & 1) == 1)
                           {
                              deliBox += p[key][key2];
                           } else if ((key2 & 2) == 2)
                           {
                              failBox += p[key][key2];
                           }
                           if ((key2 & 16) == 16)
                           {
                              rejeBox += p[key][key2];
                           }
                        }
                     }
                  }
               }
            }

            document.getElementById('totalSentsCell').innerHTML = numberWithCommas(mccmncTot);
            document.getElementById('totalUnitsCell').innerHTML = numberWithCommas(unitsTot);
            document.getElementById('totalSentsCost').innerHTML = numberWithCommas((Math.round((costTot) * 100) / 100).toFixed(2));

            document.getElementById('sentDiv2').innerHTML = numberWithCommas(mccmncTot);
            document.getElementById('pendDiv2').innerHTML = numberWithCommas(pendBox);
            document.getElementById('deliDiv2').innerHTML = numberWithCommas(deliBox);
            document.getElementById('failDiv2').innerHTML = numberWithCommas(failBox);
            document.getElementById('rejeDiv2').innerHTML = numberWithCommas(rejeBox);

            counter2 = 0;
            document.getElementById('timing').innerHTML = counter2;

            /*var barVars = output.split(",");
             document.getElementById('sentDiv').innerHTML = numberWithCommas(barVars[0]);
             
             document.getElementById('totalSentsCell').innerHTML = numberWithCommas(barVars[0]);
             document.getElementById('pendDiv').innerHTML = numberWithCommas(barVars[1]);
             document.getElementById('deliDiv').innerHTML = numberWithCommas(barVars[2]);
             document.getElementById('failDiv').innerHTML = numberWithCommas(barVars[3]);
             document.getElementById('rejeDiv').innerHTML = numberWithCommas(barVars[4]);*/

            $("#loading_status").html('');
         }
      });
   }

   function capitalize(s)
   {
      return s[0].toUpperCase() + s.slice(1);
   }

   function numberWithCommas(x)
   {
      return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, " ");
   }
   //refreshData(counter);
</script>

<script type="text/javascript">
   $(function ()
   {
      $('#example2').dataTable({
         "bPaginate": false,
         "bLengthChange": false,
         "bFilter": false,
         "bSort": true,
         "bInfo": true,
         "bAutoWidth": false,
         "iDisplayLength": 100,
         "aoColumns": [
            null,
            null,
            {"sType": 'formatted-num', targets: 0},
            {"sType": 'formatted-num', targets: 0},
            {"sType": 'formatted-num', targets: 0},
            {"sType": 'formatted-num', targets: 0}
         ],
         "aaSorting": [[2, "desc"]],
      });
   });

   jQuery.extend(jQuery.fn.dataTableExt.oSort,
           {
              "title-numeric-pre": function (a) {
                 var x = a.match(/title="*(-?[0-9\.]+)/)[1];
                 return parseFloat(x);
              },
              "title-numeric-asc": function (a, b) {
                 return ((a < b) ? -1 : ((a > b) ? 1 : 0));
              },
              "title-numeric-desc": function (a, b) {
                 return ((a < b) ? 1 : ((a > b) ? -1 : 0));
              }
           });

   jQuery.extend(jQuery.fn.dataTableExt.oSort,
           {
              "formatted-num-pre": function (a) {
                 a = (a === "-" || a === "") ? 0 : a.replace(/[^\d\-\.]/g, "");
                 return parseFloat(a);
              },
              "formatted-num-asc": function (a, b) {
                 return a - b;
              },
              "formatted-num-desc": function (a, b) {
                 return b - a;
              }
           });
</script>

<script type="text/javascript">
   $(window).load(function () {
      $(".loader").fadeOut("slow");
      $(".loaderIcon").fadeOut("slow");
      //setInterval(refreshData(counter), 1000);
      //refreshDataTables(counter);

      //show loading spiners in the main stats blocks
      $("#moDiv").html('<span class="glyphicon glyphicon-refresh glyphicon-spin"></span>');
      $("#sentDiv").html('<span class="glyphicon glyphicon-refresh glyphicon-spin"></span>');
      $("#pendDiv").html('<span class="glyphicon glyphicon-refresh glyphicon-spin"></span>');
      $("#deliDiv").html('<span class="glyphicon glyphicon-refresh glyphicon-spin"></span>');
      $("#rejeDiv").html('<span class="glyphicon glyphicon-refresh glyphicon-spin"></span>');
      $("#failDiv").html('<span class="glyphicon glyphicon-refresh glyphicon-spin"></span>');

      var sId = "<?php echo $subAccounts; ?>";
      refreshDataNew(counter);
      refreshMOData(sId);

      var d = new Date();
      var month = d.getMonth();
      var year = d.getFullYear();
      var initDateStart = String(year) + '-' + String(month + 1) + '-' + '01'
      var initDateEnd = String(year) + '-' + String(month + 2) + '-' + '01'
      /*alert(initDateStart);
       alert(initDateEnd);*/
      //getCountryData(initDateStart, initDateEnd, String(month+1));

      setInterval(function ()
      {
         //refreshData(counter);
         counter2++;
         showTimer(counter2);
         //refreshDataTables(counter)
      }, 1000);

      setInterval(function ()
      {
         //refreshData(counter);
         refreshDataNew(counter);

         refreshDataServiceNew(counter3);
         //refreshDataTables(counter)
      }, 10000);

      setInterval(function ()
      {
         refreshMOData(sId);
         //refreshData(counter);
         //refreshDataTables(counter)
      }, 120000);
   })
</script>
<!-- Template Footer -->