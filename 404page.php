<?php
//What if the person is not logged in?
//set the image URL
$accountId = 1; //this can be changed to later handle resellers
$imageUrl = '/img/skinlogos/' . $accountId . '/' . $accountId . '.png';
?>

<!DOCTYPE html>
<html class="bg-black">
   <head>
      <meta charset="UTF-8">
      <title>Page Not Found</title>
      <meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
      <!-- bootstrap 3.0.2 -->
      <link href="/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
      <!-- font Awesome -->
      <link href="/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
      <!-- Theme style -->
      <link href="/css/AdminLTE.css" rel="stylesheet" type="text/css" />
      <link rel="icon" type="image/png" href="../img/windowIcons/Small.png">
      <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
      <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
      <![endif]-->
   </head>
   <body class="bg-black">
      <div class="form-box" id="login-box">
<!--         <div class="header">
            <img src="<?php echo $imageUrl ?>" />
         </div>-->
         <div class="body bg-gray">

            <h1>Page Not Found</h1>
            <hr>
            <p>
               Click <a href="/home.php">here</a> to return to the main page.
            </p>                

         </div>
      </div>
   </body>
</html>