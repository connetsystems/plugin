/*
 * Author: Abdullah A Almsaeed
 * Date: 4 Jan 2014
 * Description:
 *      This is a demo file used only for the main dashboard (index.html)
 **/

$(function() {
    "use strict";

    //Make the dashboard widgets sortable Using jquery UI
    $(".connectedSortable").sortable({
        placeholder: "sort-highlight",
        connectWith: ".connectedSortable",
        handle: ".box-header, .nav-tabs",
        forcePlaceholderSize: true,
        zIndex: 999999
    }).disableSelection();
    $(".connectedSortable .box-header, .connectedSortable .nav-tabs-custom").css("cursor", "move");
    //jQuery UI sortable for the todo list
    $(".todo-list").sortable({
        placeholder: "sort-highlight",
        handle: ".handle",
        forcePlaceholderSize: true,
        zIndex: 999999
    }).disableSelection();
    ;

    //bootstrap WYSIHTML5 - text editor
    $(".textarea").wysihtml5();

    $('.daterange').daterangepicker(
            {
                ranges: {
                    'Today': [moment(), moment()],
                    'Yesterday': [moment().subtract('days', 1), moment().subtract('days', 1)],
                    'Last 7 Days': [moment().subtract('days', 6), moment()],
                    'Last 30 Days': [moment().subtract('days', 29), moment()],
                    'This Month': [moment().startOf('month'), moment().endOf('month')],
                    'Last Month': [moment().subtract('month', 1).startOf('month'), moment().subtract('month', 1).endOf('month')]
                },
                startDate: moment().subtract('days', 29),
                endDate: moment()
            },
    function(start, end) {
        alert("You chose: " + start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'));
    });

    /* jQueryKnob */
    $(".knob").knob();

    $('#world-map').vectorMap({
                map: 'world_mill_en',
                backgroundColor: "transparent",
                regionStyle: {
                    initial: {
                        fill: '#e4e4e4',
                        "fill-opacity": 1,
                        stroke: 'none',
                        "stroke-width": 0,
                        "stroke-opacity": 1
                    }
                },
                series: {
                    regions: [{
                            values: visitorsData,
                            scale: ["#92c1dc", "#ebf4f9"],
                            normalizeFunction: 'polynomial'
                        }]
                },
                onRegionLabelShow: function(e, el, code) {
                    if (typeof visitorsData[code] != "undefined")
                        el.html(el.html() + ': ' + visitorsData[code] + " SMS's sent");
                }
            });

    //jvectormap data
    /*var visitorsData = {
        "US": 398, //USA
        "SA": 400, //Saudi Arabia
        "CA": 1000, //Canada
        "DE": 500, //Germany
        "FR": 760, //France
        "CN": 300, //China
        "AU": 700, //Australia
        "BR": 600, //Brazil
        "IN": 800, //India
        "GB": 320, //Great Britain
        "RU": 3000 //Russia
    };
    //World map by jvectormap
    $('#world-map').vectorMap({
        map: 'world_mill_en',
        backgroundColor: "transparent",
        regionStyle: {
            initial: {
                fill: '#e4e4e4',
                "fill-opacity": 1,
                stroke: 'none',
                "stroke-width": 0,
                "stroke-opacity": 1
            }
        },
        series: {
            regions: [{
                    values: visitorsData,
                    scale: ["#92c1dc", "#ebf4f9"],
                    normalizeFunction: 'polynomial'
                }]
        },
        onRegionLabelShow: function(e, el, code) {
            if (typeof visitorsData[code] != "undefined")
                el.html(el.html() + ': ' + visitorsData[code] + ' new visitors');
        }
    });*/

    //Sparkline charts
    var myvalues = [1000, 1200, 920, 927, 931, 1027, 819, 930, 1021];
    $('#sparkline-1').sparkline(myvalues, {
        type: 'line',
        lineColor: '#92c1dc',
        fillColor: "#ebf4f9",
        height: '50',
        width: '80'
    });
    myvalues = [515, 519, 520, 522, 652, 810, 370, 627, 319, 630, 921];
    $('#sparkline-2').sparkline(myvalues, {
        type: 'line',
        lineColor: '#92c1dc',
        fillColor: "#ebf4f9",
        height: '50',
        width: '80'
    });
    myvalues = [15, 19, 20, 22, 33, 27, 31, 27, 19, 30, 21];
    $('#sparkline-3').sparkline(myvalues, {
        type: 'line',
        lineColor: '#92c1dc',
        fillColor: "#ebf4f9",
        height: '50',
        width: '80'
    });    

    //The Calender
    $("#calendar").datepicker();

    //SLIMSCROLL FOR CHAT WIDGET
    $('#chat-box').slimScroll({
        height: '250px'
    });

    /* Morris.js Charts */
    // Sales chart

                    // LINE CHART
                /*var line2 = new Morris.Line(
                {
                    element: 'line-chart',
                    resize: true,
                    data: 
                    [
                        {y: '2011 Q1', item1: 1000, item2: 800, item3: 200},
                        {y: '2011 Q2', item1: 2778, item2: 2700, item3: 78},
                        {y: '2011 Q3', item1: 3000, item2: 2800, item3: 200},
                        {y: '2011 Q4', item1: 3500, item2: 3400, item3: 100},
                        {y: '2012 Q1', item1: 4200, item2: 4000, item3: 200},
                        {y: '2012 Q2', item1: 5000, item2: 4900, item3: 100},
                        {y: '2012 Q3', item1: 6500, item2: 6300, item3: 200},
                        {y: '2012 Q4', item1: 7200, item2: 7100, item3: 100},
                        {y: '2013 Q1', item1: 7800, item2: 7700, item3: 100},
                        {y: '2013 Q2', item1: 8200, item2: 8000, item3: 200}
                    ],
                    xkey: 'y',
                    ykeys: ['item1', 'item2', 'item3'],
                    labels: ['Sent', 'Delivered', 'Failed'],
                    lineColors: ['#2222ff', '#22ff22', '#ff2222'],
                    hideHover: 'auto'
                });*/


    /*var area = new Morris.Area({
        element: 'revenue-chart',
        resize: true,
        data: [
            {y: '2014-09-3 05:00', item1: 1000, item2: 900, item3: 100},
            {y: '2014-09-3 06:42', item1: 2000, item2: 1900, item3: 100},
            {y: '2014-09-3 07:21', item1: 12, item2: 1969},
            {y: '2014-09-3 08:10', item1: 67, item2: 3597},
            {y: '2014-09-3 09:34', item1: 10, item2: 1914},
            {y: '2014-09-3 10:00', item1: 70, item2: 4293},
            {y: '2014-09-3 12:16', item1: 20, item2: 3795},
            {y: '2014-09-3 15:00', item1: 73, item2: 5967},
            {y: '2014-09-3 16:20', item1: 87, item2: 4460},
            {y: '2014-09-3 17:40', item1: 32, item2: 5713}
        ],
        xkey: 'y',
        ykeys: ['item1', 'item2', 'item3'],
        labels: ['Sent', 'Delivered', 'Failed'],
        lineColors: ['#3c8dbc', '#11ff22', '#ff2222'],
        hideHover: 'auto'
    });*/

    /*var line = new Morris.Line({
        element: 'line-chart',
        resize: true,
        data: [
            {y: '2014-09-3 05:00', item1: 2668},
            {y: '2014-09-3 06:42', item1: 2294},
            {y: '2014-09-3 07:21', item1: 1969},
            {y: '2014-09-3 08:10', item1: 3597},
            {y: '2014-09-3 09:34', item1: 1914},
            {y: '2014-09-3 10:00', item1: 4293},
            {y: '2014-09-3 12:16', item1: 3795},
            {y: '2014-09-3 15:00', item1: 5967},
            {y: '2014-09-3 16:20', item1: 4460},
            {y: '2014-09-3 17:40', item1: 5713}
        ],
        xkey: 'y',
        ykeys: ['item1'],
        labels: ['Item 1'],
        lineColors: ['#efefef'],
        lineWidth: 2,
        hideHover: 'auto',
        gridTextColor: "#fff",
        gridStrokeWidth: 0.4,
        pointSize: 4,
        pointStrokeColors: ["#efefef"],
        gridLineColor: "#efefef",
        gridTextFamily: "Open Sans",
        gridTextSize: 10
    });*/

 

    /*var line = new Morris.Bar(
    {
        element: 'bar-chart',
        resize: true,
        data: 
        [
            {y: 'Jan', item1: 1000, item2: 1080, item3: 8000, item4: 200},
            {y: 'Feb', item1: 2778, item2: 2700, item3: 6000, item4: 160},
            {y: 'Mar', item1: 3100, item2: 2800, item3: 5000, item4: 200},
            {y: 'Apr', item1: 3700, item2: 3400, item3: 4500, item4: 100},
            {y: 'May', item1: 4300, item2: 4000, item3: 4300, item4: 200},
            {y: 'Jun', item1: 5100, item2: 4900, item3: 3800, item4: 100},
            {y: 'Jul', item1: 6700, item2: 6300, item3: 2400, item4: 200},
            {y: 'Aug', item1: 7320, item2: 7100, item3: 1620, item4: 100},
            {y: 'Sep', item1: 7980, item2: 7700, item3: 1380, item4: 100},
            {y: 'Oct', item1: 8330, item2: 8000, item3: 230, item4: 200}
        ],
        xkey: 'y',
        ykeys: ['item1', 'item2', 'item3', 'item4'],
        labels: ['Sent', 'Delivered', 'Pending','Failed'],
        lineColors: ['#2222ff', '#22ff22', '#ffcc00','#ff2222'],
        hideHover: 'auto'
    });*/

    //Donut Chart
    var donut = new Morris.Donut({
        element: 'sales-chart',
        resize: true,
        colors: ["#3c8dbc", "#f56954", "#00a65a"],
        data: [
            {label: "Download Sales", value: 12},
            {label: "In-Store Sales", value: 30},
            {label: "Mail-Order Sales", value: 20}
        ],
        hideHover: 'auto'
    });
    /*Bar chart
    var bar = new Morris.Bar({
        element: 'bar-chart',
        resize: true,
        data: [
            {y: '2006', a: 100, b: 90},
            {y: '2007', a: 75, b: 65},
            {y: '2008', a: 50, b: 40},
            {y: '2009', a: 75, b: 65},
            {y: '2010', a: 50, b: 40},
            {y: '2011', a: 75, b: 65},
            {y: '2012', a: 100, b: 90}
        ],
        barColors: ['#00a65a', '#f56954'],
        xkey: 'y',
        ykeys: ['a', 'b'],
        labels: ['CPU', 'DISK'],
        hideHover: 'auto'
    });*/
    //Fix for charts under tabs
    $('.box ul.nav a').on('shown.bs.tab', function(e) {
        line.redraw();
        donut.redraw();
    });


    /* BOX REFRESH PLUGIN EXAMPLE (usage with morris charts) */
    $("#loading-example").boxRefresh({
        source: "ajax/dashboard-boxrefresh-demo.php",
        onLoadDone: function(box) {
            bar = new Morris.Bar({
                element: 'bar-chart',
                resize: true,
                data: [
                    {y: '2006', a: 100, b: 90},
                    {y: '2007', a: 75, b: 65},
                    {y: '2008', a: 50, b: 40},
                    {y: '2009', a: 75, b: 65},
                    {y: '2010', a: 50, b: 40},
                    {y: '2011', a: 75, b: 65},
                    {y: '2012', a: 100, b: 90}
                ],
                barColors: ['#00a65a', '#f56954'],
                xkey: 'y',
                ykeys: ['a', 'b'],
                labels: ['CPU', 'DISK'],
                hideHover: 'auto'
            });
        }
    });

    /* The todo list plugin */
    $(".todo-list").todolist({
        onCheck: function(ele) {
            //console.log("The element has been checked")
        },
        onUncheck: function(ele) {
            //console.log("The element has been unchecked")
        }
    });

});