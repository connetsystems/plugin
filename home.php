<?php
/**
 * Include the header template which sets up the HTML
 *
 * Don't forget to include template_import_script before any Javascripting
 * Don't forget to include template footer.php at the bottom of the page as well
 */

TemplateHelper::setPageTitle('Dashboard');
TemplateHelper::initialize();


include("php/homeMonths.php");

//NIEL $dir = 'img/serviceproviders';
//NIEL $serProviders = scandir($dir);

$smscCountV = array();
$smscCountM = array();
$smscCountC = array();
$smscCountT = array();

$startDate = mktime(0, 0, 0) * 1000;
$endDate = (time() * 1000);



//DOUG N.B. still trying to figure out what this does
$singleView = 'block;';
if ($_SESSION['serviceId'] != '' && $_SESSION['resellerId'] != '') {
   if ($_SESSION['serviceId'] != $_SESSION['resellerId']) {
      //if(!isset($_GET['client']))
      {
         $_GET['client'] = $_SESSION['accountName'] . ' - ' . $_SESSION['serviceName'] . " - " . $_SESSION['serviceId'];
         //echo "<br><br><br><br>+---------------------->".$_GET['client'];
         $singleView = 'none;';
         // $noShow = 1;
      }
   } elseif ($_SESSION['serviceId'] == $_SESSION['resellerId']) {
      $singleView = 'none;';
      //$noShow = 1;
   }
}

$qry = '{
          "size": 0,
              "aggs": {
                "service_id": {
                  "terms": {
                    "field": "service_id",
                    "size": 0,
                    "order": {
                      "1": "desc"
                    }
                  },
                  "aggs": {
                    "1": {
                      "sum": {
                        "script": "doc[\'billing_units\'].value",
                        "lang": "expression"
                      }
                    }
                  }
            }
          },
          "query": {
            "filtered": {
              "query": {
                "query_string": {
                  "query": "(service_id:*)",
                  "analyze_wildcard": true
                }
              },
              "filter": {
                "bool": {
                  "must": [
                    {
                      "range": {
                        "timestamp": {
                            "gte": ' . $startDate . ',
                            "lte": ' . $endDate . '
                        }
                      }
                    }
                  ],
                  "must_not": []
                }
              }
            }
          }
        }
    ';

$allStats = runRawMCoreElasticsearchQuery($qry);

$sId = array();
foreach ($allStats as $key => $value) {
   if ($key == 'aggregations') {
      $bucks = count($value['service_id']['buckets']);
      for ($a = 0; $a < $bucks; $a++) {
         array_push($sId, $value['service_id']['buckets'][$a]['key']);
      }
      /* echo '<pre>';
        print_r($value['service_id']['buckets']);
        echo '</pre>'; */
   }
}

$sIdNames = array();
foreach ($sId as $key => $value) {
   $serNs = getServiceName($value);
   $sIdNames[$value] = $serNs;
}


$year = date('Y');

$monthsStart = '01';
$monthsEnd = ['31', '28', '31', '30', '31', '30', '31', '31', '30', '31', '30', '31'];
$monthsArr = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'];

$sD = "2013-01-01";
$eD = date('Y-01-01', strtotime('+1 year'));

$sqlMonthData = getMonthTotalsForDash($sD, $eD);
$yearArr = array();

foreach ($sqlMonthData as $key => $value) {
   if (!in_array($value['Year'], $yearArr)) {
      array_push($yearArr, $value['Year']);
   }
}

$startDateConv = date('Y-m-d', ($startDate / 1000));
$endDateConv = date('Y-m-d', ($endDate / 1000));
//echo 'start='.$startDateConv;
//$mosTotals = getMOForDashConnet($startDateConv);
?>
<!-- Right side column. Contains the navbar and content of the page -->
<aside class="right-side">
   <!-- Content Header (Page header) -->
   <section class="content-header">
      <h1>
         Dashboard
      </h1>
   </section>
   <!-- Small buttons at Top -->
   <section class="content" style="display:<?php echo $singleView; ?>">
      <div class="row">
         <div class="col-lg-12 col-xs-12">
            <h4>Today's Stats <small id="loading_status"></small></h4>
         </div>
      </div>

      <div class="row">
         <div class="col-lg-2 col-xs-6">
            <!-- small box -->
            <div class="small-box bg-connetBlue">
               <div class="inner">
                  <h3>
                     <div id="sentDiv" style="color:#ffffff;font-size:20pt;">
                        &nbsp;
                     </div>
                  </h3>
                  <!--p style="color:#ffffff">
                      &nbsp;
                  </p-->
               </div>
               <!--div class="icon" id="sendDiv2" style="color:#ffffff;font-size:26pt;padding-bottom:15px;display:block;">

               </div-->
               <a href="#" class="small-box-footer">
                  SENT <!--div id="sendAve"></div-->
               </a>
            </div>
         </div><!-- ./col -->
         <div class="col-lg-2 col-xs-6">
            <!-- small box -->
            <div class="small-box bg-connetYellow">
               <div class="inner">
                  <h3>
                     <div id="pendDiv" style="color:#ffffff;font-size:20pt;">
                        &nbsp;
                     </div>
                  </h3>
                  <!--p style="color:#ffffff">
                     &nbsp;
                  </p-->
               </div>
               <!--div class="icon" id="pendDiv2" style="color:#ffffff;font-size:26pt;padding-bottom:15px;display:block;">
                   0%
               </div-->
               <a href="#" class="small-box-footer">
                  PENDING <!--div id="pendAve"></div-->
               </a>
            </div>
         </div><!-- ./col -->
         <div class="col-lg-2 col-xs-6">
            <!-- small box -->
            <div class="small-box bg-connetGreen">
               <div class="inner">
                  <h3>
                     <div id="deliDiv" style="color:#ffffff;font-size:20pt;">
                        &nbsp;
                     </div>
                  </h3>
                  <!--p style="color:#ffffff">
                     &nbsp;
                  </p-->
               </div>
               <!--div class="icon" id="deliDiv2" style="color:#ffffff;font-size:26pt;padding-bottom:15px;display:block;">
                   0%
               </div-->
               <a href="#" class="small-box-footer">
                  DELIVERED <!--div id="deliAve"></div-->
               </a>
            </div>
         </div><!-- ./col -->
         <div class="col-lg-2 col-xs-6">
            <!-- small box -->
            <div class="small-box bg-red">
               <div class="inner">
                  <h3>
                     <div id="failDiv" style="color:#ffffff;font-size:20pt;">
                        &nbsp;
                     </div>
                  </h3>
                  <!--p style="color:#ffffff">
                      &nbsp;
                  </p-->
               </div>
               <!--div class="icon" id="failDiv2" style="color:#ffffff;font-size:26pt;padding-bottom:15px;display:block;">
                   0%
               </div-->
               <a href="#" class="small-box-footer">
                  FAILED <!--div id="failAve"></div-->
               </a>
            </div>
         </div><!-- ./col -->
         <div class="col-lg-2 col-xs-6">
            <!-- small box -->
            <div class="small-box bg-connetRed">
               <div class="inner">
                  <h3>
                     <div id="rejeDiv" style="color:#ffffff;font-size:20pt;">
                        &nbsp;
                     </div>
                  </h3>
                  <!--p style="color:#ffffff">
                      &nbsp;
                  </p-->
               </div>
               <!--div class="icon" id="rejeDiv2" style="color:#ffffff;font-size:26pt;padding-bottom:15px;display:block;">
                   0%
               </div-->
               <a href="#" class="small-box-footer">
                  REJECTED <!--div id="rejeAve"></div-->
               </a>
            </div>
         </div>
         <div class="col-lg-2 col-xs-6">
            <!-- small box -->
            <div class="small-box bg-aqua">
               <div class="inner">
                  <h3>
                     <div id="moDiv" style="color:#ffffff;font-size:20pt;">
                        &nbsp;
                     </div>
                  </h3>
                  <!--p style="color:#ffffff">
                      &nbsp;
                  </p-->
               </div>
               <!--div class="icon" id="rejeDiv2" style="color:#ffffff;font-size:26pt;padding-bottom:15px;display:block;">
                   0%
               </div-->
               <a href="#" class="small-box-footer">
                  MOs
               </a>
            </div>
         </div><!-- ./col -->
      </div><!-- /.row -->
   </section><!-- /.content -->

   <section class="content col-lg-12" style="display:<?php echo $singleView; ?>">
      <!-- Nav tabs -->
      <ul class="nav nav-tabs" role="tablist">
         <li role="presentation" class="active"><a href="#networks" aria-controls="networks" role="tab" data-toggle="tab">Network Stats</a></li>
         <li role="presentation"><a href="#client_stats" aria-controls="client_stats" role="tab" data-toggle="tab">Client Stats</a></li>
         <li role="presentation"><a href="#history" aria-controls="history" role="tab" data-toggle="tab">Send History</a></li>
      </ul>

      <!-- Tab panes -->
      <div class="tab-content">
         <div role="tabpanel" class="tab-pane active" id="networks">
            <section class="content col-lg-3" style="display:<?php echo $singleView; ?>">
               <div class="box box-connet" style="padding-top:1px; border-top-color:<?php echo $accRGB; ?>;">
                  <div class="box-header">
                     <h3 class="box-title" style="width:100%;">
                        <div style="display:inline-block;width:50%;">
                           <img src="[template-base-url]/img/serviceproviders/Vodacom.png"> Vodacom
                        </div>
                        <div id="vodacomTot" style="display:inline-block;padding-left:4px;width:48%;text-align:right;">
                           0
                        </div>
                     </h3>
                  </div>
                  <div class="box-body">
                     <div class="box-body table-responsive no-padding">
                        <table id="vodacomTable"  class="table table-hover">
                           <thead>
                              <tr>
                                 <th>SMSC</th>
                                 <th style="background-color:#DEA52F"><center>PEND&nbsp;%</center></th>
                           <th style="background-color:#8BA808"><center>DEL&nbsp;%</center></th>
                           <th style="background-color:#f56954"><center>FAIL&nbsp;%</center></th>
                           </tr>
                           </thead>
                           <tbody id="vodaTable">

                           </tbody>
                        </table>
                     </div>
                  </div>
               </div>
            </section>

            <section class="content col-lg-3" style="display:<?php echo $singleView; ?>">
               <div class="box box-connet" style="padding-top:1px; border-top-color:<?php echo $accRGB; ?>;">
                  <div class="box-header">
                     <h3 class="box-title" style="width:100%;">
                        <div style="display:inline-block;width:50%;">
                           <img src="[template-base-url]/img/serviceproviders/MTN.png"> MTN
                        </div>
                        <div id="mtnTot" style="display:inline-block;padding-left:4px;width:48%;text-align:right;">
                           0
                        </div>
                     </h3>
                  </div>
                  <div class="box-body">
                     <div class="box-body table-responsive no-padding">
                        <table id="mtnTable"  class="table table-hover">
                           <thead>
                              <tr>
                                 <th>SMSC</th>
                                 <th style="background-color:#DEA52F"><center>PEND&nbsp;%</center></th>
                           <th style="background-color:#8BA808"><center>DEL&nbsp;%</center></th>
                           <th style="background-color:#f56954"><center>FAIL&nbsp;%</center></th>
                           </tr>
                           </thead>
                           <tbody>
                           </tbody>
                        </table>
                     </div>
                  </div>
               </div>
            </section>

            <section class="content col-lg-3" style="display:<?php echo $singleView; ?>">
               <div class="box box-connet" style="padding-top:1px; border-top-color:<?php echo $accRGB; ?>;">
                  <div class="box-header">
                     <h3 class="box-title" style="width:100%;">
                        <div style="display:inline-block;width:50%;">
                           <img src="[template-base-url]/img/serviceproviders/cellc.png"> Cell C
                        </div>
                        <div id="cellcTot" style="display:inline-block;padding-left:4px;width:48%;text-align:right;">
                           0
                        </div>
                     </h3>
                  </div>
                  <div class="box-body">
                     <div class="box-body table-responsive no-padding">
                        <table id="cellcTable"  class="table table-hover">
                           <thead>
                              <tr>
                                 <th>SMSC</th>
                                 <th style="background-color:#DEA52F"><center>PEND&nbsp;%</center></th>
                           <th style="background-color:#8BA808"><center>DEL&nbsp;%</center></th>
                           <th style="background-color:#f56954"><center>FAIL&nbsp;%</center></th>
                           </tr>
                           </thead>
                           <tbody>
                           </tbody>
                        </table>
                     </div>
                  </div>
               </div>
            </section>

            <section class="content col-lg-3" style="display:<?php echo $singleView; ?>">
               <div class="box box-connet" style="padding-top:1px; border-top-color:<?php echo $accRGB; ?>;">
                  <div class="box-header">
                     <h3 class="box-title" style="width:100%;">
                        <div style="display:inline-block;width:50%;">
                           <img src="[template-base-url]/img/serviceproviders/telkom.png"> Telkom
                        </div>
                        <div id="telkomTot" style="display:inline-block;padding-left:4px;width:48%;text-align:right;">
                           0
                        </div>
                     </h3>
                  </div>
                  <div class="box-body">
                     <div class="box-body table-responsive no-padding">
                        <table id="telkomTable"  class="table table-hover">
                           <thead>
                              <tr>
                                 <th>SMSC</th>
                                 <th style="background-color:#DEA52F"><center>PEND&nbsp;%</center></th>
                           <th style="background-color:#8BA808"><center>DEL&nbsp;%</center></th>
                           <th style="background-color:#f56954"><center>FAIL&nbsp;%</center></th>
                           </tr>
                           </thead>
                           <tbody>
                           </tbody>
                        </table>
                     </div>
                  </div>
               </div>
            </section>
         </div>

         <!--//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////// -->
         <!--//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////// -->
         <!--//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////// -->
         <!--//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////// -->
         <!--//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////// -->
         <div role="tabpanel" class="tab-pane" id="client_stats">
            <section class="content col-lg-12" style="display:<?php echo $singleView; ?>">
               <div class="box box-connet" style="padding-top:1px; border-top-color:<?php echo $accRGB; ?>;">
                  <div class="box-header">
                     <h3 class="box-title" style="width:100%;">
                        <div style="display:inline-block;width:50%;">
                           Client Traffic
                        </div>
                        <div id="dailyTot" style="display:inline-block;padding-left:4px;width:49%;text-align:right;">
                           0
                        </div>
                     </h3>
                  </div>
                  <div class="box-body">
                     <div class="box-body table-responsive no-padding">
                        <table id="dailyTable"  class="table table-hover">
                           <thead>
                              <tr>
                                 <th>Client</th>
                                 <th style="text-align:right;background-color:#737AA6;color:#ffffff;">Sent</th>
                                 <th style="text-align:right;background-color:#DEA52F;color:#ffffff;">Pending</th>
                                 <th style="text-align:right;background-color:#8BA808;color:#ffffff;">Delivered</th>
                                 <th style="text-align:right;background-color:#f56954;color:#ffffff;">Failed</th>
                                 <th style="text-align:right;background-color:#B8252B;color:#ffffff;">Rejected</th>
                              </tr>
                           </thead>
                           <tbody id="sentTable">

                           </tbody>
                           <!--tfoot>
                               <tr>
                                   <th>Client</th>
                                   <th style="text-align:right;background-color:#737AA6">Sent</th>
                                   <th style="text-align:right;background-color:#DEA52F">Pending</th>
                                   <th style="text-align:right;background-color:#8BA808">Delivered</th>
                                   <th style="text-align:right;background-color:#f56954">Failed</th>
                                   <th style="text-align:right;background-color:#B8252B">Rejected</th>
                               </tr>
                           </tfoot-->
                        </table>
                     </div>
                  </div>
               </div>
            </section>
         </div>
<?php
/* echo '<section class="content col-lg-12" style="margin-top:0px;">';
  echo '<div class="box box-connet" style="padding-top:1px; border-top-color:<?php echo $accRGB; ?>;">';
  echo '<div class="box-body">';
  echo '<div class="box-body table-responsive no-padding">';
  echo '<table id="example2"  class="table table-hover">';
  echo '<thead>';
  echo '<tr>';
  echo '<th>';
  echo 'Year';
  echo '</th>';
  for ($i=0; $i < count($monthsArr); $i++)
  {
  echo '<th><center>'.$monthsArr[$i].'</center></th>';
  }
  echo '</tr>';

  $year = '2015';
  echo '<tr>';
  echo '<td>';
  echo $year;
  echo '</td>';
  for ($i=0; $i < count($monthsArr); $i++)
  {
  $j = $i + 1;
  $startingDate = $year.'-'.$j.'-'.$monthsStart;
  $endingDate = $year.'-'.$j.'-'.$monthsEnd[$i];

  $monData = getMonthElastic($startingDate, $endingDate);

  foreach ($monData as $key => $value)
  {
  if($key == 'hits')
  {
  $mTot = $value['total'];
  }
  }

  echo '<td><center>'.number_format($mTot, 0, '', ' ').'</center></td>';
  }                          //  echo '</tr>';
  echo '</tr>';

  $year = '2014';
  echo '<tr>';
  echo '<td>';
  echo $year;
  echo '</td>';
  for ($i=0; $i < count($monthsArr); $i++)
  {
  $j = $i + 1;
  $startingDate = $year.'-'.$j.'-'.$monthsStart;
  $endingDate = $year.'-'.$j.'-'.$monthsEnd[$i];

  $monData = getMonthElastic($startingDate, $endingDate);

  foreach ($monData as $key => $value)
  {
  if($key == 'hits')
  {
  $mTot = $value['total'];
  }
  }

  echo '<td><center>'.number_format($mTot, 0, '', ' ').'</center></td>';
  }                          //  echo '</tr>';
  echo '</tr>';

  echo '</tbody>';
  echo '</table>';
  echo '</div>';
  echo '</div>';
  echo '</div>';
  echo '</section>'; */
?>
         <div role="tabpanel" class="tab-pane" id="history">
         <?php
         echo '<section class="content col-lg-12" style="margin-top:0px;display:' . $singleView . '">';
         echo '<div class="box box-connet" style="padding-top:1px; border-top-color:<?php echo $accRGB; ?>;">';
         echo '<div class="box-body">';
         echo '<div class="box-body table-responsive no-padding">';
         echo '<table id="example2"  class="table table-hover">';
         echo '<thead>';
         echo '<tr>';
         echo '<th>';
         echo 'Year';
         echo '</th>';
         for ($i = 0; $i < count($monthsArr); $i++) {
            echo '<th style="text-align:right">' . $monthsArr[$i] . '</th>';
         }
         echo '<th style="text-align:right">Year Total</th>';
         echo '<th style="text-align:right">Month Average</th>';
         echo '</tr>';
         echo '</thead>';
         //echo '<td><center>'.$monthsArr[$i].'</center></td>';
         echo '<tbody>';

         for ($i = 0; $i < count($yearArr); $i++) {
            echo '<tr>';
            echo '<td>';
            echo $yearArr[$i];
            echo '</td>';
            $yearTotal = 0;
            for ($ij = 0; $ij < count($monthsArr); $ij++) {
               $cont = 0;
               foreach ($sqlMonthData as $key => $value) {
                  if ((substr($value['month'], 0, 3) == $monthsArr[$ij]) && ($value['Year'] == $yearArr[$i])) {
                     $yearTotal += $value['cdr_record_units'];
                     $monthTemp = (($key + 1) - ($i * 12)) + 1;
                     echo '<td style="cursor: pointer;text-align:right" onclick="getCountryData(`' . $value['Year'] . '-' . $monthTemp . '-' . '01' . '`,`' . $value['Year'] . '-' . ($monthTemp + 1) . '-' . '01' . '`, `' . $monthTemp . '`)">' . number_format($value['cdr_record_units'], 0, '.', ' ') . '</td>';
                     $cont++;
                  }
               }
               if ($cont == 0) {
                  echo '<td style="text-align:right">0</td>';
                  //echo '';
               }
            }
            echo '<td style="text-align:right">' . number_format($yearTotal, 0, '.', ' ') . '</td>';
            if (date('n') != 12 && $yearArr[$i] == date('Y')) {
               echo '<td style="text-align:right">' . number_format(($yearTotal / date('n')), 0, '.', ' ') . '</td>';
            } else {
               echo '<td style="text-align:right">' . number_format(($yearTotal / 12), 0, '.', ' ') . '</td>';
            }
            echo '<tr>';
         }
         //echo '<tr>';
         //echo '</tr>';

         echo '</tbody>';
         echo '</table>';
         echo '</div>';
         echo '</div>';
         echo '</div>';
         echo '</section>';
         ?>


            <section class="content col-lg-3" style="display:<?php echo $singleView; ?>">
               <div class="box box-connet" style="padding-top:1px; border-top-color:<?php echo $accRGB; ?>;">
                  <div class="box-header">
                     <h3 class="box-title">
                        <div style="display:inline;padding-right:180px;">
                           <img src="../img/flags/globe.png"> Total Sent:&nbsp;&nbsp;&nbsp;<div style='display:inline;' id='dispMonth'>MONTH</div> <div style='display:inline;' id='dispYear'>YEAR</div>
                        </div>
                        <!--div id="telkomTot" style="display:inline;padding-left:10px">
                            0
                        </div-->
                     </h3>
                  </div>
                  <div class="box-body">
                     <div class="box-body table-responsive no-padding">
                        <table id="countryTable"  class="table table-hover">
                           <thead>
                              <tr>
                                 <th>Country</th>
                                 <th>Sent</th>
                              </tr>
                           </thead>
                           <tbody>
                           </tbody>
                        </table>
                     </div>
                  </div>
               </div>
            </section>
         </div>
      </div>
   </section>
</aside><!-- /.right-side -->

<?php include("pages/template_import_script.php"); //must import all scripts first  ?>

<script type="text/javascript">

   var template_base_url = [json:template-base-url];
   var counter = 0;

   function refreshMOData(reCount)
   {
      $("#loading_status").html('<span class="glyphicon glyphicon-refresh glyphicon-spin"></span> Refreshing...');
      counter++;
      $.ajax({url: 'php/dashHandlerMOs.php',
         data: {action: reCount},
         type: 'post',
         success: function (output)
         {
            //console.log(output);
            document.getElementById('moDiv').innerHTML = numberWithCommas(output);
            //var barVars = output.split(",");
            //document.getElementById('sentDiv').innerHTML = numberWithCommas(barVars[0]);
            $("#loading_status").html('');
         }
      });
   }

   var oldSnt;
   var oldPnd;
   var oldDel;
   var oldFai;
   var oldRej;

   function refreshData(reCount)
   {
      $("#loading_status").html('<span class="glyphicon glyphicon-refresh glyphicon-spin"></span> Refreshing...');
      counter++;
      $.ajax({url: 'php/dashHandler.php',
         data: {action: reCount},
         type: 'post',
         success: function (output)
         {
            var barVars = output.split(",");

            /*
             var newSnt = barVars[0];
             var aveSnt = ((newSnt - oldSnt) / 10);
             oldSnt = newSnt;
             document.getElementById('sendAve').innerHTML = aveSnt + " SMS/s";
             
             var newPnd = barVars[1];
             var avePnd = ((newPnd - oldPnd) / 10);
             oldPnd = newPnd;
             document.getElementById('pendAve').innerHTML = avePnd + " SMS/s";
             
             var newDel = barVars[2];
             var aveDel = ((newDel - oldDel) / 10);
             oldDel = newDel;
             document.getElementById('deliAve').innerHTML = aveDel + " SMS/s";
             
             var newFai = barVars[3];
             var aveFai = ((newFai - oldFai) / 10);
             oldFai = newFai;
             document.getElementById('failAve').innerHTML = aveFai + " SMS/s";
             
             var newRej = barVars[4];
             var aveRej = ((newRej - oldRej) / 10);
             oldRej = newRej;
             document.getElementById('rejeAve').innerHTML = aveRej + " SMS/s";
             */

            document.getElementById('sentDiv').innerHTML = numberWithCommas(barVars[0]);
            document.getElementById('dailyTot').innerHTML = numberWithCommas(barVars[0]);
            //document.getElementById('sendDiv2').innerHTML = barVars[5] + ' %';
            document.getElementById('pendDiv').innerHTML = numberWithCommas(barVars[1]);
            //document.getElementById('pendDiv2').innerHTML = barVars[6] + ' %';
            document.getElementById('deliDiv').innerHTML = numberWithCommas(barVars[2]);
            //document.getElementById('deliDiv2').innerHTML = barVars[7] + ' %';
            document.getElementById('failDiv').innerHTML = numberWithCommas(barVars[3]);
            //document.getElementById('failDiv2').innerHTML = barVars[8] + ' %';
            document.getElementById('rejeDiv').innerHTML = numberWithCommas(barVars[4]);
            //document.getElementById('rejeDiv2').innerHTML = barVars[9] + ' %';

            $("#loading_status").html('');
         }
      });
   }
   function refreshDataTables(reCount)
   {
      counter++;
      $.ajax({url: 'php/dashHandler2.php',
         data: {action: reCount},
         type: 'post',
         success: function (output)
         {
            obj = JSON.parse(output);
            //console.log(output);
            /*console.log(obj.aggregations.data.buckets[0].mccmnc.buckets[0].key);
             console.log(obj.aggregations.data.buckets[0].mccmnc.buckets[1].key);
             console.log(obj.aggregations.data.buckets[0].mccmnc.buckets[2].key);
             console.log(obj.aggregations.data.buckets[0].mccmnc.buckets[3].key);*/
            var tableVodacom = document.getElementById("vodacomTable");
            var rows = tableVodacom.getElementsByTagName("tr").length;
            rows = rows - 1;
            if (rows > 1)
            {
               for (var i = 0; i < rows; i++)
               {
                  tableVodacom.deleteRow(1);
                  //rows[`i]
               }
            }

            var tableMtn = document.getElementById("mtnTable");
            var rows = tableMtn.getElementsByTagName("tr").length;
            rows = rows - 1;
            if (rows > 1)
            {
               for (var i = 0; i < rows; i++)
               {
                  tableMtn.deleteRow(1);
                  //rows[`i]
               }
            }
            var tableCellc = document.getElementById("cellcTable");
            var rows = tableCellc.getElementsByTagName("tr").length;
            rows = rows - 1;
            if (rows > 1)
            {
               for (var i = 0; i < rows; i++)
               {
                  tableCellc.deleteRow(1);
                  //rows[`i]
               }
            }
            var tableTelkom = document.getElementById("telkomTable");
            var rows = tableTelkom.getElementsByTagName("tr").length;
            rows = rows - 1;
            if (rows > 1)
            {
               for (var i = 0; i < rows; i++)
               {
                  tableTelkom.deleteRow(1);
                  //rows[`i]
               }
            }

            //var row = table.insertRow(0);
            /*var header = table.createTHead();
             var rowH = header.insertRow(0);
             var cell1 = rowH.insertCell(0);
             cell1.innerHTML = 'SMSC';
             var cell2 = rowH.insertCell(1);
             cell2.innerHTML = 'PEND ';
             cell2.style.textAlign = 'right';
             cell2.style.backgroundColor = '#ecebc0';
             var cell3 = rowH.insertCell(2);
             cell3.innerHTML = 'DEL %';
             cell3.style.textAlign = 'right';
             cell3.style.backgroundColor = '#c0eccd';
             var cell4 = rowH.insertCell(3);
             cell4.innerHTML = 'FAIL %';
             cell4.style.textAlign = 'right';
             cell4.style.backgroundColor = '#ecc0c0';*/

            // Create an empty <tr> element and add it to the first position of <thead>:

            // Insert a new cell (<td>) at the first position of the "new" <tr> element:
            //var cellH = rowH.insertCell(0);

            //cellH.innerHTML = "<b>This is a table header</b>";
            // Add some bold text in the new cell:

            for (var i in obj.aggregations.data.buckets[0].mccmnc.buckets)
            {
               //console.log(obj.aggregations.data.buckets[0].mccmnc.buckets[i].key);
               if (obj.aggregations.data.buckets[0].mccmnc.buckets[i].key == '65501')
               {
                  var vodacomTot = obj.aggregations.data.buckets[0].mccmnc.buckets[i].doc_count;
               }
               if (obj.aggregations.data.buckets[0].mccmnc.buckets[i].key == '65510')
               {
                  var mtnTot = obj.aggregations.data.buckets[0].mccmnc.buckets[i].doc_count;
               }
               if (obj.aggregations.data.buckets[0].mccmnc.buckets[i].key == '65507')
               {
                  var cellcTot = obj.aggregations.data.buckets[0].mccmnc.buckets[i].doc_count;
               }
               if (obj.aggregations.data.buckets[0].mccmnc.buckets[i].key == '65502')
               {
                  var telkomTot = obj.aggregations.data.buckets[0].mccmnc.buckets[i].doc_count;
               }

               for (var j in obj.aggregations.data.buckets[0].mccmnc.buckets[i].smsc.buckets)
               {
                  var total = obj.aggregations.data.buckets[0].mccmnc.buckets[i].smsc.buckets[j].doc_count;
                  var smscName = obj.aggregations.data.buckets[0].mccmnc.buckets[i].smsc.buckets[j].key;
                  var dlr680 = 0;
                  var dlr681 = 0;
                  var dlr682 = 0;

                  //console.log(obj.aggregations.data.buckets[0].mccmnc.buckets[i].smsc.buckets[j].key);
                  for (var k in obj.aggregations.data.buckets[0].mccmnc.buckets[i].smsc.buckets[j].dlr.buckets)
                  {
                     //console.log('---> '+k);
                     dlrPos = obj.aggregations.data.buckets[0].mccmnc.buckets[i].smsc.buckets[j].dlr.buckets[k].key;
                     if ((dlrPos & 32) == 32 && (dlrPos & 1) == 0 && (dlrPos & 2) == 0 && (dlrPos & 16) == 0)
                     {
                        //countRdnc = count($value['5']['buckets'][$i]['mccmnc']['buckets'][$j]['smsc']['buckets'][$l]['dlr']['buckets'][$k]['rdnc']['buckets']);
                        for (var l in obj.aggregations.data.buckets[0].mccmnc.buckets[i].smsc.buckets[j].dlr.buckets[k].rdnc.buckets)
                        {
                           if (obj.aggregations.data.buckets[0].mccmnc.buckets[i].smsc.buckets[j].dlr.buckets[k].rdnc.buckets[l].key == 0)
                           {
                              if (dlr680 == 0)
                              {
                                 //$dlrArr680['5']['buckets']['0']['dlr']['buckets'] = $value['5']['buckets']['0']['dlr']['buckets'][$i]['rdnc']['buckets'][$j]['1']['value'];
                                 dlr680 = obj.aggregations.data.buckets[0].mccmnc.buckets[i].smsc.buckets[j].dlr.buckets[k].rdnc.buckets[l].doc_count;
                              } else
                              {
                                 dlr680 += obj.aggregations.data.buckets[0].mccmnc.buckets[i].smsc.buckets[j].dlr.buckets[k].rdnc.buckets[l].doc_count;
                              }
                           }
                        }
                        //console.log(obj.aggregations.data.buckets[0].mccmnc.buckets[i].smsc.buckets[j].dlr.buckets[k].key + "<--pending");
                     }
                     if ((dlrPos & 1) == 1)
                     {
                        if (dlr681 == 0)
                        {
                           //$dlrArr680['5']['buckets']['0']['dlr']['buckets'] = $value['5']['buckets']['0']['dlr']['buckets'][$i]['rdnc']['buckets'][$j]['1']['value'];
                           dlr681 = obj.aggregations.data.buckets[0].mccmnc.buckets[i].smsc.buckets[j].dlr.buckets[k].doc_count;
                        } else
                        {
                           dlr681 += obj.aggregations.data.buckets[0].mccmnc.buckets[i].smsc.buckets[j].dlr.buckets[k].doc_count;
                        }
                        //console.log(obj.aggregations.data.buckets[0].mccmnc.buckets[i].smsc.buckets[j].dlr.buckets[k].key + "<--delivered");
                     }
                     if ((dlrPos & 2) == 2)
                     {
                        /*if(dlr682 == 0)
                         {
                         //$dlrArr680['5']['buckets']['0']['dlr']['buckets'] = $value['5']['buckets']['0']['dlr']['buckets'][$i]['rdnc']['buckets'][$j]['1']['value'];
                         dlr682 = obj.aggregations.data.buckets[0].mccmnc.buckets[i].smsc.buckets[j].dlr.buckets[k].doc_count;
                         }
                         else*/
                        {
                           dlr682 += obj.aggregations.data.buckets[0].mccmnc.buckets[i].smsc.buckets[j].dlr.buckets[k].doc_count;
                        }
                        //console.log(obj.aggregations.data.buckets[0].mccmnc.buckets[i].smsc.buckets[j].dlr.buckets[k].key + "<--failed");
                     }
                     //console.log(obj.aggregations.data.buckets[0].mccmnc.buckets[i].key);

                  }

                  if (obj.aggregations.data.buckets[0].mccmnc.buckets[i].key == '65501')
                  {
                     var perc680 = Math.round(dlr680 / total * 100);
                     var perc681 = Math.round(dlr681 / total * 100);
                     var perc682 = Math.round(dlr682 / total * 100);
                     /*console.log(smscName + perc680);
                      console.log(smscName + perc681);
                      console.log(smscName + perc682);*/

                     var row = tableVodacom.insertRow(1);

                     var cell1 = row.insertCell(0);
                     //cell1.innerHTML = ' ';
                     var ind = smscName.lastIndexOf("_");
                     var shtName = smscName.substring(0, ind);
                     //var img = document.createElement('img');
                     /*img.src = "img/serviceproviders/"+ shtName +".png";
                      cell1.appendChild(img);*/
                     if (shtName == 'portal_connetsms1')
                     {
                        shtName = shtName.substring(0, 10);
                     }
                     cell1.innerHTML = "<img src='" + template_base_url + "/img/serviceproviders/" + smscName.substring(0, ind) + ".png'>&nbsp;&nbsp;" + capitalize(shtName);

                     var cell2 = row.insertCell(1);
                     cell2.innerHTML = perc680;
                     cell2.style.textAlign = 'right';
                     cell2.style.backgroundColor = '#e9cb8c';

                     var cell3 = row.insertCell(2);
                     cell3.innerHTML = perc681;
                     cell3.style.textAlign = 'right';
                     cell3.style.backgroundColor = '#c0d26e';

                     var cell4 = row.insertCell(3);
                     cell4.innerHTML = perc682;
                     cell4.style.textAlign = 'right';
                     cell4.style.backgroundColor = '#f8b6ac';


                     // Insert new cells (<td> elements) at the 1st and 2nd position of the "new" <tr> element:

                     // Add some text to the new cells:
                  }

                  if (obj.aggregations.data.buckets[0].mccmnc.buckets[i].key == '65510')
                  {
                     var perc680 = Math.round(dlr680 / total * 100);
                     var perc681 = Math.round(dlr681 / total * 100);
                     var perc682 = Math.round(dlr682 / total * 100);

                     var row = tableMtn.insertRow(1);

                     var cell1 = row.insertCell(0);
                     //cell1.innerHTML = ' ';
                     var ind = smscName.lastIndexOf("_");
                     var shtName = smscName.substring(0, ind);
                     //var img = document.createElement('img');
                     /*img.src = "img/serviceproviders/"+ shtName +".png";
                      cell1.appendChild(img);*/
                     if (shtName == 'portal_connetsms1')
                     {
                        shtName = shtName.substring(0, 10);
                     }
                     cell1.innerHTML = "<img src='" + template_base_url + "/img/serviceproviders/" + smscName.substring(0, ind) + ".png'>&nbsp;&nbsp;" + capitalize(shtName);

                     var cell2 = row.insertCell(1);
                     cell2.innerHTML = perc680;
                     cell2.style.textAlign = 'right';
                     cell2.style.backgroundColor = '#e9cb8c';

                     var cell3 = row.insertCell(2);
                     cell3.innerHTML = perc681;
                     cell3.style.textAlign = 'right';
                     cell3.style.backgroundColor = '#c0d26e';

                     var cell4 = row.insertCell(3);
                     cell4.innerHTML = perc682;
                     cell4.style.textAlign = 'right';
                     cell4.style.backgroundColor = '#f8b6ac';
                     //console.log('Voda = ' + perc681);
                  }

                  if (obj.aggregations.data.buckets[0].mccmnc.buckets[i].key == '65507')
                  {
                     var perc680 = Math.round(dlr680 / total * 100);
                     var perc681 = Math.round(dlr681 / total * 100);
                     var perc682 = Math.round(dlr682 / total * 100);
                     //console.log('Voda = ' + perc681);
                     var row = tableCellc.insertRow(1);

                     var cell1 = row.insertCell(0);
                     //cell1.innerHTML = ' ';
                     var ind = smscName.lastIndexOf("_");
                     var shtName = smscName.substring(0, ind);
                     //var img = document.createElement('img');
                     /*img.src = "img/serviceproviders/"+ shtName +".png";
                      cell1.appendChild(img);*/
                     if (shtName == 'portal_connetsms1')
                     {
                        shtName = shtName.substring(0, 10);
                     }
                     cell1.innerHTML = "<img src='" + template_base_url + "/img/serviceproviders/" + smscName.substring(0, ind) + ".png'>&nbsp;&nbsp;" + capitalize(shtName);

                     var cell2 = row.insertCell(1);
                     cell2.innerHTML = perc680;
                     cell2.style.textAlign = 'right';
                     cell2.style.backgroundColor = '#e9cb8c';

                     var cell3 = row.insertCell(2);
                     cell3.innerHTML = perc681;
                     cell3.style.textAlign = 'right';
                     cell3.style.backgroundColor = '#c0d26e';

                     var cell4 = row.insertCell(3);
                     cell4.innerHTML = perc682;
                     cell4.style.textAlign = 'right';
                     cell4.style.backgroundColor = '#f8b6ac';
                  }

                  if (obj.aggregations.data.buckets[0].mccmnc.buckets[i].key == '65502')
                  {
                     var perc680 = Math.round(dlr680 / total * 100);
                     var perc681 = Math.round(dlr681 / total * 100);
                     var perc682 = Math.round(dlr682 / total * 100);
                     //console.log('Voda = ' + perc681);
                     var row = tableTelkom.insertRow(1);

                     var cell1 = row.insertCell(0);
                     //cell1.innerHTML = ' ';
                     var ind = smscName.lastIndexOf("_");
                     var shtName = smscName.substring(0, ind);
                     //var img = document.createElement('img');
                     /*img.src = "img/serviceproviders/"+ shtName +".png";
                      cell1.appendChild(img);*/
                     if (shtName == 'portal_connetsms1')
                     {
                        shtName = shtName.substring(0, 10);
                     }
                     cell1.innerHTML = "<img src='" + template_base_url + "/img/serviceproviders/" + smscName.substring(0, ind) + ".png'>&nbsp;&nbsp;" + capitalize(shtName);

                     var cell2 = row.insertCell(1);
                     cell2.innerHTML = perc680;
                     cell2.style.textAlign = 'right';
                     cell2.style.backgroundColor = '#e9cb8c';

                     var cell3 = row.insertCell(2);
                     cell3.innerHTML = perc681;
                     cell3.style.textAlign = 'right';
                     cell3.style.backgroundColor = '#c0d26e';

                     var cell4 = row.insertCell(3);
                     cell4.innerHTML = perc682;
                     cell4.style.textAlign = 'right';
                     cell4.style.backgroundColor = '#f8b6ac';
                     //cell1.style.backgroundColor = '#ff0000';
                  }
               }
            }

            //console.log(obj.aggregations.data.buckets[0].mccmnc.buckets[3].smsc.buckets[0].dlr.buckets[0].key);

            /*console.log(obj.aggregations.data.buckets[2].mccmnc.buckets[0].key);
             console.log(obj.aggregations.data.buckets[3].mccmnc.buckets[0].key);*/
            //alert(obj.took);
            //alert(obj.hits.total);
            /*var barVars = output.split(",");
             document.getElementById('sentDiv').innerHTML = barVars[0];
             //document.getElementById('sendDiv2').innerHTML = barVars[5] + ' %';
             document.getElementById('pendDiv').innerHTML = barVars[1];
             document.getElementById('pendDiv2').innerHTML = barVars[6] + ' %';
             document.getElementById('deliDiv').innerHTML = barVars[2];
             document.getElementById('deliDiv2').innerHTML = barVars[7] + ' %';
             document.getElementById('failDiv').innerHTML = barVars[3];
             document.getElementById('failDiv2').innerHTML = barVars[8] + ' %';
             document.getElementById('rejeDiv2').innerHTML = barVars[9] + ' %';*/
            document.getElementById('vodacomTot').innerHTML = numberWithCommas(vodacomTot);
            document.getElementById('mtnTot').innerHTML = numberWithCommas(mtnTot);
            document.getElementById('cellcTot').innerHTML = numberWithCommas(cellcTot);
            document.getElementById('telkomTot').innerHTML = numberWithCommas(telkomTot);
         }
      });
   }

   function getCountryData(sdateV, edateV, monthDisp)
   {
      //alert("YEAH!=" + sdateV + ' to ' + edateV);
      var yInd = sdateV.indexOf("-");
      var monthsArr = ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'];
      //alert(integer(monthDisp-1));
      var monthDisp = monthsArr[parseInt(monthDisp - 1)];

      var yearDisp = sdateV.substring(0, yInd);
      document.getElementById("dispMonth").innerHTML = monthDisp;
      document.getElementById("dispYear").innerHTML = yearDisp;

      $.ajax({url: 'php/dashHandler3.php',
         data: {sdate: sdateV, edate: edateV},
         type: 'post',
         success: function (output)
         {
            var splitArr = output.split("|");
            //console.log(splitArr);
            var tableCountry = document.getElementById("countryTable");
            var rows = tableCountry.getElementsByTagName("tr").length;
            rows = rows - 1;

            if (rows > 1)
            {
               for (var i = 0; i < rows; i++)
               {
                  tableCountry.deleteRow(1);
                  //rows[`i]
               }
            }

            for (var i = 0; i < splitArr.length; i++)
            {
               if (splitArr[i] != "")
               {
                  var row = tableCountry.insertRow(1);

                  var countryNamePng = '';
                  var ind = splitArr[i].lastIndexOf("+");
                  var totalSent = splitArr[i].substring(ind + 1);
                  var countryName = splitArr[i].substring(0, ind);
                  var countryInd = countryName.indexOf(" /");
                  if (countryInd > -1)
                  {
                     countryNamePng = countryName.substring(0, countryInd);
                  } else
                  {
                     countryNamePng = countryName;
                  }

                  var cell1 = row.insertCell(0);
                  cell1.innerHTML = "<img src='img/flags/" + countryNamePng + ".png'>&nbsp;&nbsp;" + countryName;

                  var cell2 = row.insertCell(1);
                  cell2.innerHTML = numberWithCommas(totalSent);
                  cell2.style.textAlign = 'right';
                  //cell2.style.backgroundColor = '#ecebc0';
               }
            }
            //var rows = tableCountry.getElementsByTagName("tr").length;
         }
      });
   }

   var numsOld = null;
   function getClientBreakdown()
   {
      var reCount = 1;
      //counter++;
      $.ajax({url: 'php/dashHandler4.php',
         data: {action: reCount},
         type: 'post',
         success: function (output)
         {
            //obj = JSON.parse(output);
            //console.log(output);
            var splitArr = output.split(";");
            if (numsOld != null)
            {
               var splitArr3 = numsOld.split(";");
               //alert('YES');
            }
            //console.log(splitArr);
            var tableCountry = document.getElementById("dailyTable");
            var rows = tableCountry.getElementsByTagName("tr").length;
            rows = rows - 1;

            if (rows > 1)
            {
               for (var i = 0; i < rows; i++)
               {
                  tableCountry.deleteRow(1);
                  //rows[`i]
               }
            }

            for (var i = 0; i < splitArr.length; i++)
            {
               if (splitArr[i] != "")
               {
                  var row = tableCountry.insertRow(1);
                  //var countryNamePng = '';
                  //var totalSent = splitArr[i].substring(ind+1);
                  var ind = splitArr[i].lastIndexOf(":");
                  var service = splitArr[i].substring(0, ind);
                  var nums = splitArr[i].substring(ind + 1, splitArr[i].length);
                  var splitArr2 = nums.split(",");

                  if (numsOld != null)
                  {
                     var ind3 = splitArr3[i].lastIndexOf(":");
                     var nums3 = splitArr3[i].substring(ind3 + 1, splitArr3[i].length);
                     var splitArr4 = nums3.split(",");
                  }



                  //var countryInd = service.indexOf(" /");
                  /*if(countryInd > -1)
                   {
                   servicePng = service.substring(0, countryInd);
                   }
                   else
                   {
                   servicePng = service;
                   }*/

                  var arrayName = <?php echo json_encode($sIdNames); ?>;
                  //console.log(nums);
                  /*$(".elementClass").each(function(index, id)
                   {
                   $(id).html(arrayName[index-1]);
                   });*/

                  var cell1 = row.insertCell(0);
                  cell1.style.textAlign = 'left';
                  if (numsOld != null && (splitArr2[0] != splitArr4[0] || splitArr2[1] != splitArr4[1] || splitArr2[2] != splitArr4[2] || splitArr2[3] != splitArr4[3] || splitArr2[4] != splitArr4[4]))
                  {
                     cell1.style.backgroundColor = '#dddddd';
                     //cell2.style.color = '#ffffff';
                     cell1.innerHTML = arrayName[service];
                  } else
                  {
                     cell1.innerHTML = arrayName[service];
                  }

                  var cell2 = row.insertCell(1);
                  cell2.style.textAlign = 'right';
                  if (numsOld != null && splitArr2[0] != splitArr4[0])
                  {
                     cell2.style.backgroundColor = '#737AA6';
                     cell2.style.color = '#ffffff';
                     cell2.innerHTML = numberWithCommas(splitArr2[0]);
                  } else
                  {
                     cell2.style.backgroundColor = '#adb1cb';
                     cell2.innerHTML = numberWithCommas(splitArr2[0]);
                  }

                  var cell3 = row.insertCell(2);
                  cell3.style.textAlign = 'right';
                  if (numsOld != null && splitArr2[1] != splitArr4[1])
                  {
                     cell3.style.backgroundColor = '#DEA52F';
                     cell3.style.color = '#ffffff';
                     cell3.innerHTML = numberWithCommas(splitArr2[1]);
                  } else
                  {
                     cell3.style.backgroundColor = '#e9cb8c';
                     cell3.innerHTML = numberWithCommas(splitArr2[1]);
                  }

                  var cell4 = row.insertCell(3);
                  cell4.style.textAlign = 'right';
                  if (numsOld != null && splitArr2[2] != splitArr4[2])
                  {
                     cell4.style.backgroundColor = '#8BA808';
                     cell4.style.color = '#ffffff';
                     cell4.innerHTML = numberWithCommas(splitArr2[2]);
                  } else
                  {
                     cell4.style.backgroundColor = '#c0d26e';
                     cell4.innerHTML = numberWithCommas(splitArr2[2]);
                  }

                  var cell5 = row.insertCell(4);
                  cell5.style.textAlign = 'right';
                  if (numsOld != null && splitArr2[3] != splitArr4[3])
                  {
                     cell5.style.backgroundColor = '#f56954';
                     cell5.style.color = '#ffffff';
                     cell5.innerHTML = numberWithCommas(splitArr2[3]);
                  } else
                  {
                     cell5.style.backgroundColor = '#f8b6ac';
                     cell5.innerHTML = numberWithCommas(splitArr2[3]);
                  }

                  var cell6 = row.insertCell(5);
                  cell6.style.textAlign = 'right';
                  if (numsOld != null && splitArr2[4] != splitArr4[4])
                  {
                     cell5.style.backgroundColor = '#B8252B';
                     cell5.style.color = '#ffffff';
                     cell5.innerHTML = numberWithCommas(splitArr2[4]);
                  } else
                  {
                     cell6.style.backgroundColor = '#df8c8f';
                     cell6.innerHTML = numberWithCommas(splitArr2[4]);
                  }
                  //cell2.style.textAlign = 'right';
                  //cell2.style.backgroundColor = '#ecebc0';


               }
            }
            numsOld = output;
         }
      });
   }

   function capitalize(s)
   {
      return s[0].toUpperCase() + s.slice(1);
   }

   function numberWithCommas(x)
   {
      return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, " ");
   }
   //refreshData(counter);
</script>

<script type="text/javascript">
   $(document).ready(function (e) {

      $(window).load(function () {
         $(".loader").fadeOut("slow");
         $(".loaderIcon").fadeOut("slow");

         //show loading spiners in the main stats blocks
         $("#moDiv").html('<span class="glyphicon glyphicon-refresh glyphicon-spin"></span>');
         $("#sentDiv").html('<span class="glyphicon glyphicon-refresh glyphicon-spin"></span>');
         $("#pendDiv").html('<span class="glyphicon glyphicon-refresh glyphicon-spin"></span>');
         $("#deliDiv").html('<span class="glyphicon glyphicon-refresh glyphicon-spin"></span>');
         $("#rejeDiv").html('<span class="glyphicon glyphicon-refresh glyphicon-spin"></span>');
         $("#failDiv").html('<span class="glyphicon glyphicon-refresh glyphicon-spin"></span>');

         //setInterval(refreshData(counter), 1000);
         refreshMOData(counter);
         refreshDataTables(counter);
         refreshData(counter);
         getClientBreakdown();

         var d = new Date();
         var month = d.getMonth();
         var year = d.getFullYear();
         var initDateStart = String(year) + '-' + String(month + 1) + '-' + '01'
         var initDateEnd = String(year) + '-' + String(month + 2) + '-' + '01'
         /*alert(initDateStart);
          alert(initDateEnd);*/
         getCountryData(initDateStart, initDateEnd, String(month + 1));

         setInterval(function ()
         {
            refreshData(counter);
            getClientBreakdown();
            //refreshDataTables(counter)
         }, 10000);

         setInterval(function ()
         {
            //refreshData(counter);
            refreshMOData(counter);
            refreshDataTables(counter)
         }, 600000);
      });
   });
</script>

<!-- Template Footer -->