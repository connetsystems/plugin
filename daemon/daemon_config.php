<?php

if(gethostname() == 'appstaging1')
{
    $test = true;
}
else
{
    $test = false;
}

define('PLUGIN_DAEMON_REDIS_HOST', ($test ? 'appstaging1' : 'redismaster'));
define('PLUGIN_DAEMON_REDIS_KEY_BATCH_JOB_LIST', 'plugin_batch_jobs');

define('PLUGIN_DAEMON_RUN_BATCH_PROCESSOR', '/usr/local/sbin/phpdaemon start /var/run/plugin_batch_processor.pid '.dirname(__FILE__).'/plugin_batch_processor.php');