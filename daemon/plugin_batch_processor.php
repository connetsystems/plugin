<?php

// for handling of mac, unix and windows line breaks
ini_set("auto_detect_line_endings", true);

//--------------------------------------------------------------
// Constants
//--------------------------------------------------------------
define('ERROR_REPORTING', (array_search('debug', $argv) !== false));

//--------------------------------------------------------------
// Includes
//--------------------------------------------------------------
require_once('ConnetAutoloader.php');
require_once('daemon_config.php');
require_once(dirname(dirname(__FILE__)). '/php/allInclusive.php');

preset::daemon();


clog::setLogFilename("/var/log/connet/plugin/" . basename(__FILE__, '.php') . '.log');
clog::setBacktrace();
clog::setVerbose(ERROR_REPORTING);

clog::info('Listening on '.CONFIG_REDIS_HOST);

//--------------------------------------------------------------
// Fork
//--------------------------------------------------------------
//daemon::managed_fork(CONFIG_REDIS_QUEUE_WORKERS, array('blocking' => true, 'restart' => false, 'nowait' => false));
daemon::managed_fork(1, array('blocking' => false, 'nowait' => false));
clog::setTrace(array(posix_getpid(), 'CHILD'));



//--------------------------------------------------------------
// Main Loop
//--------------------------------------------------------------
clog::info('* Entering Main Loop');

while (!daemon::killed()) {
		
		
	 /**
	 * Plugin Batch Processor
	 * @author Doug Jenkinson - Connet Systems
	 * ---
	 * This processor processes connet batch jobs
	 */

	try
	{
		//--------------------------------------------------------------
		// Initialzation
		//--------------------------------------------------------------
		//goes down if not declared here
		$redis = new Redis();
		$redis->connect(PLUGIN_DAEMON_REDIS_HOST, 6379);
		$redis->select(12);

		//check if there are REDIS RECORDS in the queue list
		$batch_job_count = $redis->lSize(PLUGIN_DAEMON_REDIS_KEY_BATCH_JOB_LIST);

		if($batch_job_count == 0)
		{
			daemon::terminate();
		}

		//--------------------------------------------------------------
		// Shutdown check for DLRs, if there are none, we are done
		//--------------------------------------------------------------
		if(daemon::terminated())
		{
			if ($batch_job_count == 0)
			{
				clog::info('* No batches available, shutting down...');
				break;
			}
			continue;
		}

		//--------------------------------------------------------------
		// IF this is a child and the parent has suicided, the child must too
		//--------------------------------------------------------------
		if($batch_job_count == 0 && daemon::is_child() && !daemon::parent_running()) {
			clog::info('* Parent is dead, I must die too. Farewell...');
			break;
		}

		//If there are DLRs, lets do some processing
		if($batch_job_count > 0)
		{
			$successful_updates = 0;
			//cycle through redis keys for this run
			for($i = 0; $i < $batch_job_count; $i ++)
			{
				//pop out a job for this thread to handle
				$job_name = $redis->lpop(PLUGIN_DAEMON_REDIS_KEY_BATCH_JOB_LIST);

				//don't do anything if the job name is not set
				if($job_name)
				{
					$batch_raw = $redis->hGetAll($job_name);
					$dlr_success = false;
					if(isset($batch_raw) && isset($batch_raw['service_id']))
					{

						$service_id = $batch_raw['service_id'];

						clog::info('We have a job for service ID: '. $service_id);

						//some stats
						$result = array();
						$result['unique_msisdns'] = array();
						$result['batch_is_valid'] = true;
						$result['valid_count'] = 0;
						$result['invalid_count'] = 0;
						$result['total'] = 0;
						$result['duplicate_count'] = 0;
						$result['unknown_count'] = 0;
						$result['sms_previews'] = array();
						$result['file_col_count'] = 0;
						$result['unique_mccmnc_codes'] = array();

						//create and open a file to write our content to
						$temp_file_name = trim(guid(), '{}') . date("Y_m_d_H_i_s");
						$temp_file_name = 'cm2_' . str_replace('-', '', $temp_file_name) . '.csv';
						$result['temp_file'] = dirname(dirname(__FILE__)). '/tempFiles/' . $temp_file_name;
						clog::info('* **** INIT RESULT: '.json_encode($result));

						//check the post variables and run the appropriate task
						if (isset($batch_raw['csv_source_file']) && $batch_raw['csv_source_file'] != "") {
							//process file
							$result = buildCsvFileAndCalculateCost($result, $service_id, $batch_raw['csv_source_file'], $batch_raw['sms_text'], $job_name);
							clog::info('* Result for file: '.json_encode($result));
						}

						//check the post variables and run the appropriate task
						if (isset($batch_raw['contact_list_id']) && $batch_raw['contact_list_id'] != "" && $batch_raw['contact_list_id'] != "0") {
							//process contact list
							$result = generateContactListFile($result, $service_id, $batch_raw['contact_list_id'], $batch_raw['sms_text'], $job_name);
							clog::info('* Result for contact list ID '.$batch_raw['contact_list_id'].': '.json_encode($result));
						}

						//check the post variables and run the appropriate task
						if (isset($batch_raw['numbers_string']) && $batch_raw['numbers_string'] != "") {
							//process msisdn string
							$result = generateManualMSISDNFile($result, $service_id, $batch_raw['numbers_string'], $batch_raw['sms_text'], $job_name);
							clog::info('* Result for numbers string: '.json_encode($result));
						}

						foreach ($result['sms_previews'] as &$sms_object)
						{
							$sms_object['sms_text'] = htmlentities($sms_object['sms_text']);
						}
						unset($sms_object);

						unset($result['unique_msisdns']);

						//swap tempfile out for a more relative link that our JS uses
						$result['temp_file'] = $temp_file_name;

						//get the costing for this whole send
						$result['costing'] = getCostTOSendFromMCCMNCCountArray($service_id, $result['unique_mccmnc_codes']);
						$batch_raw['completed'] = 1;

						$batch_raw['result'] = json_encode($result);

						clog::info('* Our raw batch data to be sent back to the client: '.json_encode($result));
						//send it back to redis
						$redis->hmSet($job_name, $batch_raw);
						//$redis->lPush(PLUGIN_DAEMON_REDIS_KEY_BATCH_JOB_LIST, $job_name);

						$successful_updates ++;

						clog::info('* Done with JOB (Job name: '.$job_name.')');
						unset($job_name);
					}
					else
					{
						clog::info('* No full hashmap was found for the DLR report. Aborting... (Job name: '.$job_name.')');
						unset($job_name);
					}
				}
			}

			clog::info('* Job completed. Processed '.$batch_job_count.' batches. ('.($batch_job_count - $successful_updates).' failed) <DONE>');
		}
	}
	catch(Exception $e)
	{
		clog::info('* We had an error on batch job processing: '.$e->getMessage());

		if(isset($redis) && isset($job_name))
		{
			//pop out a job for this thread to handle
			$redis->rpush(PLUGIN_DAEMON_REDIS_KEY_BATCH_JOB_LIST, $job_name);
		}
	}

	continue;
}

//--------------------------------------------------------------
// Finalization
//--------------------------------------------------------------


//--------------------------------------------------------------
// Exit
//--------------------------------------------------------------
clog::info('* Exit');

//********************** end of file ******************************
