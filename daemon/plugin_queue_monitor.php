<?php
/**
 * Plugin queue monitor - This Daemon checks REDIS for outstanding jobs and launches the correct scripts to process them.
 * @author Doug Jenkinson - Connet Systems
 */

 
//--------------------------------------------------------------
// Constants
//--------------------------------------------------------------
define('ERROR_REPORTING', (array_search('debug', $argv) !== false));


//--------------------------------------------------------------
// Includes
//--------------------------------------------------------------
require_once('ConnetAutoloader.php');
require_once('daemon_config.php');
require_once(dirname(__FILE__). '/../php/allInclusive.php');

preset::daemon();


clog::setLogFilename("/var/log/connet/plugin/" . basename(__FILE__, '.php') . '.log');
clog::setBacktrace();
clog::setVerbose(ERROR_REPORTING);

clog::info('Listening on '.PLUGIN_DAEMON_REDIS_HOST);

//--------------------------------------------------------------
// Fork
//--------------------------------------------------------------
//daemon::managed_fork(CONFIG_REDIS_QUEUE_WORKERS, array('blocking' => true, 'restart' => false, 'nowait' => false));
//daemon::managed_fork(0, array('blocking' => false, 'nowait' => false));
clog::setTrace(array(posix_getpid(), 'CHILD'));

//--------------------------------------------------------------
// Main Loop
//--------------------------------------------------------------
clog::info('* Entering Main Loop');

while (!daemon::killed())
{
	//--------------------------------------------------------------
	// Shutdown check for BATCHES, if there are none, we are done
	//--------------------------------------------------------------
	if(daemon::terminated())
	{
	  clog::info('* Script terminated, shutting down.');
	  break;
	}
	else if(daemon::is_child() && !daemon::parent_running())
	{
		clog::info('* Parent is dead, I must die too. Farewell...');
      	break;
	}

	try
	{
		//goes down if not declared here
		$redis = new Redis();
		$redis->connect(PLUGIN_DAEMON_REDIS_HOST, 6379);
		$redis->select(12);

    	/*********************************
		 * FOR THE BATCH PROCESSOR
		 *********************************/
			 
  		//pop out a job for this thread to handle
  		$batch_jobs = $redis->lSize(PLUGIN_DAEMON_REDIS_KEY_BATCH_JOB_LIST);

      	//clog::info('* A campaign needs to go out...'.$job_name);
  		if($batch_jobs > 0)
  		{
			clog::info('* Got batch jobs!');
			clog::info('* Launching batch processor!');
			//start the batch sender
			exec(PLUGIN_DAEMON_RUN_BATCH_PROCESSOR);
  		}
	}
    catch(Exception $e) 
    {
      	clog::info('* The script failed! Error: '.$e->getMessage());
    }
    
	sleep(5);
	continue;
}

//--------------------------------------------------------------
// Finalization
//--------------------------------------------------------------


//--------------------------------------------------------------
// Exit
//--------------------------------------------------------------
clog::info('* Exit');

//********************** end of file ******************************
