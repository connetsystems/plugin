<?php
error_reporting(E_ALL);
ini_set('display_errors', 1);

class db {
    private $hostname = 'appdb1';
    private $username = 'root';
    private $password = 'mysql';
    private $database = 'playout';
    private $dbh;
    private $stmt;

    function __construct() {
        $dsn = "mysql:host=$this->hostname;dbname=$this->database";
        $options = [
            PDO::ATTR_EMULATE_PREPARES => false,
            PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION
        ];

        $this->dbh = new PDO( $dsn, $this->username, $this->password, $options );
    }

    public function query($query) {
        $this->stmt = $this->dbh->prepare($query);

        return $this;
    }

    public function bind($pos, $value, $type = null) {

        if( is_null($type) ) {
            switch( true ) {
                case is_int($value):
                    $type = PDO::PARAM_INT;
                    break;
                case is_bool($value):
                    $type = PDO::PARAM_BOOL;
                    break;
                case is_null($value):
                    $type = PDO::PARAM_NULL;
                    break;
                default:
                    $type = PDO::PARAM_STR;
            }
        }

        $this->stmt->bindValue($pos, $value, $type);
        return $this;
    }

    public function execute() {
        $this->stmt->setFetchMode(PDO::FETCH_ASSOC);
        return $this->stmt->execute();
    }

    public function resultset() {
        $this->execute();
        return $this->stmt->fetchAll();
    }

    public function single() {
        $this->execute();
        return $this->stmt->fetch();
    }
}

/** API **/
if (isset($_REQUEST['function'])) {
    function sendResult($status, $message, $code = '', $payload = []) {
        header('Content-Type: application/json');
        die(json_encode(['result' => $status, 'message' => $message, 'code' => $code, 'payload' => $payload]));
    }

    session_start();

    switch ($_REQUEST['function']) {
        case 'getTable':
            /** Initialize Twig */
            $loader = new Twig_Loader_Filesystem('../views/playout/');
            $twig = new Twig_Environment($loader, ['debug' => true]);
            $twig->addExtension(new Twig_Extension_Debug());

            $pd = new Db();

            $data = $pd->query("SELECT * FROM ussd_content")->resultset();

            echo $twig->render('ussd_content_table.html.twig', [
                'table_title' => 'USSD Content',
                'data' => $data
            ]);

            die();
        case 'getEditContent':
            /** Initialize Twig */
            $loader = new Twig_Loader_Filesystem('../views/playout/');
            $twig = new Twig_Environment($loader, ['debug' => true]);
            $twig->addExtension(new Twig_Extension_Debug());

            if (isset($_POST['content_id'])) {
                $pd = new Db();
                $content_id = htmlentities($_POST['content_id']);

                $pd->query("SELECT * FROM ussd_content WHERE id = :id")
                    ->bind(':id', $content_id);

                $row = $pd->single();

                echo $twig->render('edit_content.html.twig', [
                    'modal' => [
                        'title' => 'Edit content text',
                        'description' => 'You can chenge the content text here. You can also delete this template.',
                        'delete_button_title' => 'Delete',
                        'save_button_title' => 'Save'
                    ],
                    'content' => $row
                ]);
            } else {
                echo $twig->render('edit_content.html.twig', [
                    'modal' => [
                        'title' => 'Edit content text',
                        'description' => 'You can change the content text here. You can also delete this template.',
                        'delete_button_title' => 'Delete',
                        'save_button_title' => 'Save'
                    ],
                    'content' => [
                        'id' => 'new',
                        'content_text' => ''
                    ]
                ]);
            }

            die();
        case 'saveContent':
            if (empty($_POST['content_id'])) sendResult(false, 'No content id specified!', 'content_id');

            if (empty($_POST['content_text'])) sendResult(false, 'Content text may not be empty!', 'content_text');

            $pd = new Db();

            if ($_POST['content_id'] == 'new') {
                $result = $pd->query("INSERT INTO ussd_content (content_text) VALUES (:content_text)")
                    ->bind(':content_text', $_POST['content_text'])->execute();

                if ($result)
                    sendResult($result, 'Row added', 'success');
            } else {
                $result = $pd->query("UPDATE ussd_content SET content_text = :content_text WHERE id = :id")
                    ->bind(':content_text', $_POST['content_text'])
                    ->bind(':id', $_POST['content_id'])
                    ->execute();

                if ($result)
                    sendResult($result, 'Row updated', 'success');
            }

            sendResult(false, 'Error! Something went wrong', 'unknown_error', [ 'result' => $result ]);
        case 'deleteContent':
            if (empty($_POST['content_id'])) sendResult(false, 'No content id specified!', 'content_id');

            $pd = new Db();

            $result = $pd->query("DELETE FROM ussd_content WHERE id = :id")
                ->bind(':id', $_POST['content_id'])
                ->execute();

            if ($result)
                sendResult($result, 'Row deleted', 'success');

            sendResult(false, 'Error! Something went wrong', 'unknown_error', [ 'result' => $result ]);

        case 'getStats':
            if (empty($_POST['date'])) sendResult(false, 'No date specified!', 'date');

            /** Initialize Twig */
            $loader = new Twig_Loader_Filesystem('../views/playout/');
            $twig = new Twig_Environment($loader, ['debug' => true]);
            $twig->addExtension(new Twig_Extension_Debug());

            $pd = new Db();

            $rows = $pd->query("
                SELECT
                  hour(created_timestamp) AS `hour`,
                  count(id) AS `cnt`
                FROM playout.ussd_session_log 
                where date(created_timestamp) = :startdate
                group by hour(created_timestamp);
           ")
                ->bind(':startdate', $_POST['date'])
                ->resultset();

            $hours = [];
            for ($i = 0; $i < 24; $i++) {
                $hours[$i] = 0;
                foreach ($rows as $row) {
                    if ($row['hour'] == $i) {
                        $hours[$i] += $row['cnt'];
                        break;
                    }
                }
            }

            $data = [
                'labels' => ["0:00", "1:00", "2:00", "3:00", "4:00", "5:00", "6:00", "7:00", "8:00", "9:00", "10:00", "11:00",
                             "12:00","13:00","14:00","15:00","16:00","17:00","18:00","19:00","20:00","21:00","22:00","23:00"],
                'datasets' => [
//                    [
//                        'label' => "USSD Session",
//                        'fill' => false,                            // Boolean - if true fill the area under the line
//                        'lineTension' => "0.1",                     // Tension - bezier curve tension of the line. Set to 0 to draw straight lines connecting points
//                        'backgroundColor' => "#4BC0C0",             // String - the color to fill the area under the line with if fill is true
//                        'borderColor' => "#4BC0C0",                 // String - Line color
//                        'borderCapStyle' => "butt",                 // String - cap style of the line. See https://developer.mozilla.org/en-US/docs/Web/API/CanvasRenderingContext2D/lineCap
//                        'borderDash' => [],                         // Array - Length and spacing of dashes. See https://developer.mozilla.org/en-US/docs/Web/API/CanvasRenderingContext2D/setLineDash
//                        'borderDashOffset' => "0.0",                // Number - Offset for line dashes. See https://developer.mozilla.org/en-US/docs/Web/API/CanvasRenderingContext2D/lineDashOffset
//                        'borderJoinStyle' => "miter",               // String - line join style. See https://developer.mozilla.org/en-US/docs/Web/API/CanvasRenderingContext2D/lineJoin
//                        // The properties below allow an array to be specified to change the value of the item at the given index
//                        'pointBorderColor' => "#4BC0C0",            // String or Array - Point stroke color
//                        'pointBackgroundColor' => "#fff",           // String or Array - Point fill color
//                        'pointBorderWidth' => "1",                  // Number or Array - Stroke width of point border
//                        'pointHoverRadius' => "5",                  // Number or Array - Radius of point when hovered
//                        'pointHoverBackgroundColor' => "#4BC0C0",   // String or Array - point background color when hovered
//                        'pointHoverBorderColor' => "#4BC0C0",       // String or Array - Point border color when hovered
//                        'pointHoverBorderWidth' => "2",             // Number or Array - border width of point when hovered
//                        'pointRadius' => "1",                       // Number or Array - the pixel size of the point shape. Can be set to 0 to not render a circle over the point
//                        'pointHitRadius' => "10",                   // Number or Array - the pixel size of the non-displayed point that reacts to mouse hover events
//                        'data' => [65, 59, 80, 81, 56, 55, 40],     // The actual data
//                        'yAxisID' => "y-axis-0"                     // String - If specified, binds the dataset to a certain y-axis. If not specified, the first y-axis is used. First id is y-axis-0
//                    ],
                    [
                        'label' => "USSD Sessions",
                        'fill' => false,
                        'backgroundColor' => "#cdcd56",
                        'borderColor' => "#cdcd56",
                        'pointBorderColor' => "#cdcd56",
                        'pointBackgroundColor' => "#fff",
                        'pointBorderWidth' => "1",
                        'pointHoverRadius' => "5",
                        'pointHoverBackgroundColor' => "#cdcd56",
                        'pointHoverBorderColor' => "#cdcd56",
                        'pointHoverBorderWidth' => "2",
                        'oData' => $hours,
                        'data' => $hours
                    ]
                ]
            ];

            header('Content-Type: application/json');


            die(json_encode($data));
        default:
            sendResult(false,'Method '.$_POST['function'].' not implemented!', '404');
    }
}

/** Page rendering **/
$headerPageTitle = 'Playout Admin';
TemplateHelper::setPageTitle($headerPageTitle);
TemplateHelper::initialize();


/** Initialize Twig */
$loader = new Twig_Loader_Filesystem('../views/playout/');
$twig = new Twig_Environment($loader, ['debug' => true]);
$twig->addExtension(new Twig_Extension_Debug());

$pd = new Db();

$startDate = $pd->query("
    SELECT 
       created_timestamp as `date`
    FROM ussd_session_log
    ORDER BY created_timestamp ASC
    LIMIT 1
")->single()['date'];

$endDate = $pd->query("
    SELECT 
       created_timestamp as `date`
    FROM ussd_session_log
    ORDER BY created_timestamp DESC
    LIMIT 1
")->single()['date'];

$phpStartDate = strtotime( $startDate );
$phpEndDate = strtotime( $endDate );

?>

<link href="../css/playout.css" rel="stylesheet" type="text/css" />

<?php
echo $twig->render('page.html.twig', [
    'page_title' => $headerPageTitle,
    'startdate' => date( 'Y-m-d', $phpStartDate )
]);

include("template_import_script.php");

?>

<link href="../js/plugins/jquery_gritter/css/jquery.gritter.css" type="text/css" rel="stylesheet">
<script src="../js/plugins/jquery_gritter/js/jquery.gritter.js" type="text/javascript"></script>


<script type="text/javascript">
    $(function() {
        var canvas = document.getElementById('ussd_stats1'),
            canvas_parent = $(canvas).parent(),
            selected_date = "<?=date( 'Y-m-d', $phpStartDate )?>";

        var context = canvas.getContext('2d');

        var my_chart;

        function resizeCanvas() {
            canvas.width = canvas_parent.width();
//            context = canvas.getContext('2d');
            getStats();
        }

        // resize the canvas to fill browser window dynamically
        window.addEventListener('resize', resizeCanvas, false);


        // Stats tab
        function getStats() {
            if (my_chart)
                my_chart.destroy();

            $.ajax({
                url: window.location.href,
                type: 'POST',
                data: {
                    'function': 'getStats',
                    'date': selected_date
                },
                dataType: 'json',
                success: function(data) {
                    console.log(data);

                    $('#refresh_stats1').unbind('click').click(function() {
                        resizeCanvas();
                    });

                    my_chart = new Chart(context).Line( data,{ segmentShowStroke : true, segmentStrokeColor : "#fff" } );

                }
            });
        }
        
        // Content tab
        function getTable(callback) {
            $.ajax({
                url: window.location.href,
                type: 'POST',
                data: {'function': 'getTable'},
                dataType: 'html',
                success: function(html) {

                    $('#ussd_content').html(html);
                    var inner = $('#ussd_content #ussd_inner_container');

                    $('#ussd_content_table', inner).dataTable({
                        "bProcessing": true,
                        "bServerSide": false,
                        "paging": true
                    });

                    $('#refresh_table', inner).unbind('click').click(function() {
                        getTable();
                    });

                    $('#add_row', inner).unbind('click').click(function() {
                        getContentEditScreen();
                    })

                    $('.content_row', inner).click(function() {
                        console.log(this);
                        getContentEditScreen($(this).data('content-id'));
                    });
                    if (callback)
                        callback();
                }
            });
        }

        function getContentEditScreen(content_id) {
            var data = {};
            data['function'] = 'getEditContent';
            if (content_id)
                data.content_id = content_id;

            $.ajax({
                url: window.location.href,
                type: 'POST',
                data: data,
                dataType: 'html',
                success: function(html) {
                    $(html).appendTo($('.playout-body'));
                    var modal = $('#edit_client_modal', $('.playout-body')).modal('hide');
                    modal.on('hidden.bs.modal', function () {
                        $(this).remove();
                    });

                    var save_but = $('#modal_save_button', modal).unbind('click').click(function() {
                        saveContent(modal);
                    });

                    var delete_but = $('#modal_delete_button', modal).unbind('click').click(function() {
                        deleteContent(modal);
                    });

                    modal.modal('show');
                }
            });
        }

        function saveContent(modal) {
            var content_id = $('#contentId', modal).val();
            var content_text = $('#inputContentText', modal).val();

            var data = {
                'function':'saveContent',
                content_id: (content_id ? content_id:'new'),
                content_text: content_text
            };

            $.ajax({
                url: window.location.href,
                type: 'POST',
                data: data,
                dataType: 'json',
                success: function(result) {
                    var gritter = {
                        text: result.message,
                        sticky: false,
                        time: 2000
                    };

                    if (result.result) {
                        gritter.title = 'Success!';
                        gritter.class_name = 'gritter-green';
                        gritter.image = '../img/success.png';
                        gritter.before_open = function() {
                            getTable(function() {
                                modal.modal('hide');
                            });
                        }
                    }
                    else {
                        gritter.title = 'Error!';
                        gritter.class_name = 'gritter-red';
                        gritter.image = '../img/error.png';
                        gritter.before_open = function() {

                        }
                    }

                    $.gritter.add(gritter);
                }
            });
        }

        function deleteContent(modal) {
            var content_id = $('#contentId', modal).val();

            var data = {
                'function':'deleteContent',
                content_id: content_id
            };

            $.ajax({
                url: window.location.href,
                type: 'POST',
                data: data,
                dataType: 'json',
                success: function(result) {
                    var gritter = {
                        text: result.message,
                        sticky: false,
                        time: 2000
                    };

                    if (result.result) {
                        gritter.title = 'Success!';
                        gritter.class_name = 'gritter-green';
                        gritter.image = '../img/success.png';
                        gritter.before_open = function() {
                            getTable(function() {
                                modal.modal('hide');
                            });
                        }
                    }
                    else {
                        gritter.title = 'Error!';
                        gritter.class_name = 'gritter-red';
                        gritter.image = '../img/error.png';
                        gritter.before_open = function() {

                        }
                    }

                    $.gritter.add(gritter);
                }
            });
        }

        
        
        $(".nav-tabs a").click(function(){
            $(this).tab('show');
        });

        // Stats tab
        resizeCanvas();
        
        // Content tab
        getTable();

        $('#reportrange').daterangepicker(
            {
                "singleDatePicker": true,
                "showDropdowns": true,
                "startDate":    "<?=date( 'm/d/Y', $phpStartDate )?>",
                "endDate":      "<?=date( 'm/d/Y', $phpEndDate )?>",
                "minDate":      "<?=date( 'm/d/Y', $phpStartDate )?>",
                "maxDate":      "<?=date( 'm/d/Y', $phpEndDate )?>"
            }, function(start, end, label) {
                $('#daterange_label').html(start.format('YYYY-MM-DD'));
                selected_date = start.format('YYYY-MM-DD')
                resizeCanvas()
            }
        );

//        $('#reportrange').on('apply.daterangepicker', function(ev, picker) {
//
//        });
    });

</script>
