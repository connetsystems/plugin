<?php
   /**
    * Include the header template which sets up the HTML
    *
    * Don't forget to include template_import_script before any Javascripting
    * Don't forget to include template footer.php at the bottom of the page as well
    */
   //-------------------------------------------------------------
   // Template
   //-------------------------------------------------------------
   TemplateHelper::setPageTitle('Reports');
   TemplateHelper::initialize();

   //-------------------------------------------------------------
   // MANAGE SERVICE
   //-------------------------------------------------------------
   //get the service

   if(isset($_GET['service_id']))
   {
      $service_id = $_GET['service_id'];
   }
   else if(LoginHelper::getCurrentServiceId())
   {
      $service_id = LoginHelper::getCurrentServiceId();
   }
   else
   {
      $permissions = PermissionsHelper::getAllServicesWithPermissions();
      $service_id = key($permissions);
   }

   if(!PermissionsHelper::hasServicePermission($service_id))
   {
      echo "You do not have permission to view reports on this service.";
      die();
   }

   //get the service object
   $service_obj = new Service($service_id);


   //pull the report date range
   if(isset($_GET['start_date']) && isset($_GET['end_date']))
   {
      $start_date = new DateTime($_GET['start_date']);
      $end_date = new DateTime($_GET['end_date']);
   }
   else
   {
      $start_date = new DateTime('first day of this week');
      $end_date = new DateTime('today');
   }

   //get the dates as strings for display
   $start_date_string = $start_date->format('Y-m-d');
   $end_date_string = $end_date->format('Y-m-d');
?>

<aside class="right-side">
   <section class="content-header">
      <h1>
         Reports
      </h1>
   </section>

   <section class="content">
      <div class="row">
         <div class="col-lg-12">
            <p class="lead">
               Welcome to your reports page. Please select which report you would like to view using the tabs below.
            </p>

         </div>
      </div>
      <div class="row">
         <div class="col-lg-9">
            <?php include_once('modules/module_service_selector_inline.php'); ?>
         </div>
         <div class="col-lg-3">
            <label>Select Report Range:</label>
            <button id="reports_date_picker" class="btn btn-default btn-block btn-sm " style="padding:5px; font-size:15px">
               <p class="no-margin pull-left"><i class="fa fa-calendar fa-lg"></i> <?php echo htmlentities($start_date_string).' - '.htmlentities($end_date_string); ?></p>
               <p class="no-margin pull-right"><b class="caret"></b>&nbsp;</p>
            </button>
         </div>
      </div>

      <hr/>

      <!-- THE TABS THAT HOLD THE CONFIG SECTIONS -->
      <div class="row">
         <div class="col-lg-12">
            <div class="nav-tabs-custom">
               <!-- Nav tabs -->
               <ul class="nav nav-tabs" id="service_tabs">
                  <li role="presentation">
                     <a href="#tab_overview" aria-controls="tab_overview" role="tab" data-toggle="tab"><i class="fa fa-eye"></i> Reporting Overview</a>
                  </li>
                  <li role="presentation">
                     <a href="#tab_net_traffic" aria-controls="tab_net_traffic" role="tab" data-toggle="tab"><i class="fa fa-server"></i> Network Traffic</a>
                  </li>
                  <li role="presentation">
                     <a href="#tab_batches" aria-controls="tab_batches" role="tab" data-toggle="tab"><i class="fa fa-envelope"></i> Batch Reports</a>
                  </li>
               </ul>

               <!-- Tab panes -->
               <div class="tab-content">
                  <!-- THE OVERVIEW TAB -->
                  <div role="tabpanel" class="tab-pane active" id="tab_overview">

                  </div>

                  <!-- THE NETWORK TRAFFIC TAB -->
                  <div role="tabpanel" class="tab-pane active" id="tab_net_traffic">

                  </div>

                  <!-- THE BATCHES TAB -->
                  <div role="tabpanel" class="tab-pane" id="tab_batches">

                  </div>

               </div>
            </div>
         </div>
      </div>
   </section>

   <!-- MODALS HERE -->
   <div id="modalEditField" class="modal fade" tabindex="-1" role="dialog" data-backdrop="static">
      <div class="modal-dialog">
         <div class="modal-content">
            <div class="modal-header" >
               <button type="button" class="close" data-dismiss="modal" aria-label="Close" id="btnModalCloseTop"><span aria-hidden="true">&times;</span></button>
               <h4 class="modal-title" id="modalTitle">Add A New Contact Group</h4>
            </div>
            <div class="modal-body">
               <div class="alert alert-danger" style="display:none;" id="modAlert">
                  <h4><i class="icon fa fa-ban"></i> Error!</h4>
                  <p id="modAlertText">Unfortunately there was an error, please try again.</p>
               </div>
               <div class="form-group">
                  <label for="modalInputValue" id="modalInputLabel">Label</label>
                  <input type="text" class="form-control" id="modalInputValue" placeholder="" value="" />
               </div>
               <small>Please edit the value and click save to commit, or close to cancel.</small>
            </div>
            <div class="modal-footer">
               <button type="button" class="btn btn-default" data-dismiss="modal" id="btnModalCloseBottom">Close</button>
               <button type="button" class="btn btn-primary" id="btnModalSave" style="background-color:<?php echo $_SESSION['accountColour']; ?>;border: 2px solid; border-radius: 6px !important;">Save</button>
               <input type="hidden" class="form-control" id="modalInputField" value=""/>
               <input type="hidden" class="form-control" id="modalInputId" value=""/>
               <input type="hidden" class="form-control" id="modalInputTask" value=""/>
            </div>
         </div><!-- /.modal-content -->
      </div><!-- /.modal-dialog -->
   </div><!-- /.modal -->
</aside>


<!--  THE JAVASCRIPT IMPORT ALWAYS APPEARS BEFORE LOCAL JAVASCRIPT AT THE BOTTOM OF THE PAGE -->
<?php include("template_import_script.php"); //must import all scripts first   ?>
<!-- END JAVASCRIPT IMPORT -->

<script type="text/javascript">
   var serviceId = <?php echo (isset($service_id) ? $service_id : '-1') ?>;
   var activeTab = "";


   $(document).ready(function (e)
   {
      //on load of the page: switch to the currently selected tab
      var hash = window.location.hash;
      if(hash == "") {
         hash = "#tab_overview";
      }

      console.log(hash);

      $('#service_tabs a').click(function(e) {
         e.preventDefault();
         $(this).tab('show');
         $(window).scrollTop(0);
      });

      //store the currently selected tab in the hash value
      $("ul.nav-tabs > li > a").on("shown.bs.tab", function(e) {
         var id = $(e.target).attr("href").substr(1);
         window.location.hash = id;

         loadTabFragment(id);
      });

      //for the update credits shortcut
      $('#btnShortcutAddCredits').click(function(e) {
         e.preventDefault();
         showTab('#tab_payments');
      });

      //show the default loading tab for the page after init
      showTab(hash);


      //setup the date range picker
      $('#reports_date_picker').daterangepicker(
          {
             ranges: {
                'Today': [moment(), moment()],
                'Yesterday': [moment().subtract('days', 1), moment().subtract('days', 1)],
                'Last 7 Days': [moment().subtract('days', 6), moment()],
                'Last 30 Days': [moment().subtract('days', 29), moment()],
                'This Month': [moment().startOf('month'), moment().endOf('month')],
                'Last Month': [moment().subtract('month', 1).startOf('month'), moment().subtract('month', 1).endOf('month')]
             },
             startDate: moment('<?php echo isset($start_date_string) ? $start_date_string : ''; ?>'),
             endDate: moment('<?php echo isset($end_date_string) ? $end_date_string : ''; ?>')
          },
          function (start, end) {
             $('#reports_date_picker span').html(start.format('YYYY-MM-DD') + ' - ' + end.format('YYYY-MM-DD'));

             var startDateCal = start.format('YYYY-MM-DD');

             var endDateCal = end.format('YYYY-MM-DD');

             window.location = "../pages/client_reports.php?service_id=" + encodeURIComponent(serviceId) + "&start_date=" + startDateCal + "&end_date=" + endDateCal + "#" + activeTab;
          }
      );

   });

   //set the tab and start the tab load process according to the hash string passed to it
   function showTab(hashTab)
   {
      $('#service_tabs a[href="' + hashTab + '"]').tab('show');
      $(window).scrollTop(0);
   }

   //this locks the modal so the user can't do anything, useful when runing ajax calls off of it
   var prevButtonContent = "";
   function lockModalBusy(busyMessage)
   {
      prevButtonContent = $('#btnModalSave').html();
      $('#btnModalCloseTop').prop("disabled", true);
      $('#btnModalCloseBottom').prop("disabled", true);
      $('#btnModalSave').prop("disabled", false);
      $('#btnModalSave').html('<span class="glyphicon glyphicon-refresh glyphicon-spin"></span> ' + busyMessage);
   }

   //unlocks the modal so the user can close it
   function unlockModalBusy()
   {
      $('#btnModalCloseTop').prop("disabled", false);
      $('#btnModalCloseBottom').prop("disabled", false);
      $('#btnModalSave').prop("disabled", false);
      $('#btnModalSave').html(prevButtonContent);
      prevButtonContent = "";
   }

   //shows the error in the MODAL with a custom error message
   function showModalError(message)
   {
      $("#modAlertText").html(message);
      $("#modAlert").show();
   }

   //hides the modal error and resets the message
   function hideModalError()
   {
      $("#modAlertText").html();
      $("#modAlert").hide();
   }

   //cleans up all the child elements in a tab, for optimization
   function cleanupTabData(tabId)
   {
      if(tabId != "")
      {
         $(tabId).empty();
      }
   }

   function loadTabFragment(tabName)
   {
      cleanupTabData(activeTab);
      showTabLoading(tabName);
      activeTab = tabName;
      var getVariableString = window.location.search.substring(1);

      var scriptPath = "";
      switch (tabName) {
         case "tab_overview":
            scriptPath = "client_reports/frag_reports_overview.php?"+getVariableString;
            break;
         case "tab_net_traffic":
            scriptPath = "client_reports/frag_reports_network_traffic.php?"+getVariableString;
            break;
         case "tab_batches":
            scriptPath = "client_reports/frag_reports_batch_list.php?"+getVariableString;
            break;
      }

      //load up the page tab we have selected
      $.post(scriptPath, {service_id:serviceId}).done(function (data) {
         $('#'+tabName).html(data);
      }).fail(function (data) {
         showTabLoadingError(tabName, "A server error occurred and we could not load this list, please contact technical support.");
      });
   }

   function showTabLoading(elementId)
   {
      $('#'+elementId).html('<div style="width:100%; text-align:center; padding:20px; font-size:20px;"><span class="glyphicon glyphicon-refresh glyphicon-spin"></span><br/>Loading... Please wait.</div>');
   }

   function showTabLoadingError(elementId, message)
   {
      $('#'+elementId).html('<h5 class="text-warning">' + message + '</h5>');
   }

   function showLoading(elementId)
   {
      $('#'+elementId).html('<div style="width:100%; text-align:center; padding:20px;"><span class="glyphicon glyphicon-refresh glyphicon-spin"></span> Loading...</div>');
   }

   function showLoadingError(elementId, message)
   {
      $('#'+elementId).html('<h5 class="text-warning">' + message + '</h5>');
   }
</script>

<!-- Template Footer -->

