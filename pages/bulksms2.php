<?php
/**
 * Include the header template which sets up the HTML
 *
 * Don't forget to include template_import_script before any Javascripting
 * Don't forget to include template footer.php at the bottom of the page as well
 */
$headerPageTitle = "Bulk SMS"; //set the page title for the template import
TemplateHelper::initialize();

set_time_limit(360);

$singleView = 'block;';

if ($_SESSION['serviceId'] != '' && $_SESSION['resellerId'] != '') {
   if ($_SESSION['serviceId'] != $_SESSION['resellerId']) {
      //if(!isset($_GET['client']))
      {
         $_GET['client'] = $_SESSION['accountName'] . ' - ' . $_SESSION['serviceName'] . " - " . $_SESSION['serviceId'];
         //echo "<br><br><br><br>+---------------------->".$_GET['client'];
         $singleView = 'none;';
         // $noShow = 1;
      }
   } elseif ($_SESSION['serviceId'] == $_SESSION['resellerId']) {
      $singleView = 'block;';
      //$noShow = 1;
   }
   /* elseif($_SESSION['resellerId'] == '')
     {

     $noShow = 1;
     } */
}

/* if(isset($_POST['client']))
  {
  $_GET['client'] = $_POST['client'];

  echo "<pre>";
  print_r($_POST);
  echo "</pre>";

  if(isset($_FILES["uploadCSV"]))
  {
  //echo "<br>YES!";
  }
  } */

if (isset($_GET['client'])) {
   $cl = $_GET['client'];
   $clp = strrpos($cl, ' ');
   $cl = trim(substr($cl, $clp, strlen($cl)));
   $dsp = 'block;';
   $camps = getCampaignData($cl);

   if (isset($_GET['campId']) && $_GET['campId'] != "") {
      //echo "<br>this test";
      $dsp2 = 'block;';
      $temps = getTempData($_GET['campId']);
      $conts = getContactListClient($cl);
   } else {
      $dsp2 = 'none;';
   }


   /* foreach ($camps as $key => $value) 
     {
     echo '<pre>';
     print_r($value);
     echo '</pre>';
     # code...
     } */
   //$dir = '../img/routes';
   //$serProviders = scandir($dir);
} else {
   $_GET['client'] = "0 - Please select a client...";
   $cl = strstr($_GET['client'], ' ', true);
   $dsp = 'none;';
   $dsp2 = 'none;';
}

if (isset($_SESSION['accountId'])) {
   $acId = $_SESSION['accountId'];
} else {
   $acId = null;
}
$clients = getServicesInAccount($acId);

////////////////////////////////////////////////
////////////////////////////////////////////////
////////////////////////////////////////////////



/* $sql = 'INSERT INTO `core_service_payment` (service_id, user_id, payment_value, payment_description,payment_previous_balance) 
  VALUES (:service_id, :user_id, :payment_value, :payment_description,:payment_previous_balance)';

  $sql = mysql_encode_param_array($sql, array(
  'service_id' => $selected_service->service_id,
  'user_id' => $_SESSION['auth']['user']->user_id,
  'payment_value' => $request->payment_value,
  'payment_description' => $request->description,
  'payment_previous_balance' => $selected_service->service_credit_available,
  )); */
?>
<aside class="right-side">
   <section class="content-header">
      <h1>
         Bulk SMS
         <!--small>Control panel</small-->
      </h1>
   </section>

   <section class="content">
      <div class="row col-lg-12">
         <div class="box box-solid" style="height:66px; margin-top:-15px;display:<?php echo $singleView; ?>">
            <form action="" method="get" id="clientList" class="sidebar-form" style="border:0px;">
               <div class="form-group">
                  <label>Service List</label>
                  <select class="form-control" name="client" id="clientSel" form="clientList" OnChange="reloadOnSelect(this.value);">
                     <?php
                     if ($cl == '0') {
                        echo '<option SELECTED>Please select a client...</option>';
                     }
                     foreach ($clients as $key => $value) {
                        $sId = $value['service_id'];
                        if ($cl == $sId) {
                           $accountName = $value['account_name'];
                           $serviceName = $value['service_name'];
                           echo "<option name='" . $sId . "' SELECTED>" . $accountName . " - " . $serviceName . " - " . $sId . "</option>";
                        } else {
                           echo "<option name='" . $sId . "'>" . $value['account_name'] . " - " . $value['service_name'] . " - " . $sId . "</option>";
                        }
                     }
                     ?>
                  </select>
               </div>
               <br>
            </form>
         </div>
         <div class="box box-warning" style="border-top-color:<?php echo $accRGB; ?>;display:<?php echo $dsp; ?>">
            <div class="box-body" style="margin-bottom:0px;padding-bottom:1px;">
               <form role="form" id="bulkData" form="batch" enctype="multipart/form-data" action="bulksmscont.php" method="POST">

                  <div class="callout callout-info" style="margin-bottom:10px;">
                     <h4>Bulk SMS</h4>
                     <p>You can upload a CSV file -AND/OR- enter a list of numbers(comma separated) manually.</p>
                  </div>

                  <?php
                  if (isset($_GET['error']) && $_GET['error'] == 1) {
                     echo '<div class="callout callout-danger" style="margin-top:-10px;margin-bottom:10px;">';
                     echo '<h4>Error</h4>';
                     echo '<p>Invalid CSV file.</p>';
                     echo '</div>';
                  }
                  if (isset($_GET['error']) && $_GET['error'] == 2) {
                     echo '<div class="callout callout-danger" style="margin-top:-10px;margin-bottom:10px;">';
                     echo '<h4>Error</h4>';
                     echo '<p>Invalid {} format.</p>';
                     echo '</div>';
                  }
                  ?>

                  <div class="box box-warning" style="border-top-color:<?php echo $accRGB; ?>;display:<?php echo $dsp; ?>;margin-bottom:10px;">
                     <div class="box-body" style="margin-bottom:0px;">
                        <div class="form-group" style="margin-bottom:0px;">
                           <label>Campaign List</label>
                           <select class="form-control" name="campaign" id="camp" form="bulkData" OnChange="getCamp()">
                              <?php
                              echo "<option>Please Select</option>";
                              foreach ($camps as $key => $value) {
                                 if (isset($_GET['campId']) && $_GET['campId'] == $value['campaign_id']) {
                                    $camCode = $value['campaign_code'];
                                    $camId = $value['campaign_id'];
                                    echo "<option SELECTED id='" . $value['campaign_id'] . "'>Client: '" . $value['client_name'] . "'  -  Campaign: '" . $value['campaign_name'] . "'  -  Code: '" . $value['campaign_code'] . "' </option>";
                                 } else {
                                    echo "<option id='" . $value['campaign_id'] . "'>Client: '" . $value['client_name'] . "'  -  Campaign: '" . $value['campaign_name'] . "'  -  Code: '" . $value['campaign_code'] . "' </option>";
                                 }
                              }
                              ?>
                           </select>
                        </div>
                     </div>
                  </div>

                  <div class="box box-warning" style="border-top-color:<?php echo $accRGB; ?>;display:<?php echo $dsp2; ?>">

                     <div class="box-body">
                        <p><b>Reference</b></p>
                        <div class="callout callout-danger" id="referError" style="display:none;">
                           <p>Reference cannot be empty!</p>
                        </div>
                        <input type="text" class="form-control" name="refer" id="refer" placeholder="Provide a reference for this batch" value="" style="border:1px solid #aaaaaa;" >
                     </div>

                     <div class="box-body">
                        <label>Delivery Status</label>
                        <select class="form-control" name="status" id="temper" form="bulkData" OnChange="checkDelStatus(this.value)">
                           <option selected>Paused</option>
                           <option>Scheduled</option>
                           <option>Send Immediately</option>
                        </select>
                     </div>

                     <div class="box-body" id="schedulerBox" style="padding-bottom:35px;display:none;">
                        <label>Delivery Time</label>
                        <br>
                        <div id="scheduledTimer" class="select pull-left" style="cursor: pointer;padding-left:10px;">
                           <i class="fa fa-calendar fa-lg"></i>
                           <span style="font-size:15px" form="bulkData" id="sTime"><?php echo date("Y-d-m"); ?></span><b class="caret"></b>
                        </div>
                     </div>


                     <div class="box-body" style="padding-bottom:1px;padding-top:5px;">

                        <div class="form-group" style="margin-bottom:10px;">
                           <label for="uploadCSV">File upload (You can select a single CSV file, see examples below)</label>

                           <!--div>
                               <img src="../img/examples/example2.png" />
                                   <p style="display:inline-block;padding-left:10px;padding-right:10px;">Or</p>
                               <img src="../img/examples/example1.png" />
                           </div-->
                           <br>
                           <div style="padding-left:20px;padding-top:10px;">

                              <div style="display:inline-block;">
                                 27841234567<br>
                                 27813214567<br>
                                 27837654321<br>
                                 ...
                              </div>

                              <div style="display:inline-block;padding-left:10px;padding-right:10px;padding-bottom:50px;vertical-align:middle;">or</div>

                              <div style="display:inline-block;">
                                 27841234567,"Content for SMS 1 here"<br>
                                 27813214567,"Content for SMS 2 here"<br>
                                 27837654321,"Content for SMS 3 here"<br>
                                 ...
                              </div>

                              <div style="display:inline-block;padding-left:10px;padding-right:10px;padding-bottom:50px;vertical-align:middle;">or</div>

                              <div style="display:inline-block;">
                                 27841234567,"John","Smith","..."<br>
                                 27813214567,"Jane","Doe","..."<br>
                                 27837654321,"Eve","Adams","..."<br>
                                 ...
                              </div>

                           </div>
                           <div class="callout callout-danger" id="fileError" style="display:none;">
                              <p>You must choose a file and/or a contact list!</p>
                           </div>
                           <input type="file" name="uploadCSV" id="uploadCSV">

                           <div class="box-body" style="margin-bottom:0px;">
                              <div class="form-group" style="margin-bottom:0px;">
                                 <label>Delimiter (This is the separator that delimits the data in your csv file.)</label>
                                 <select class="form-control" name="separ" id="separ" form="bulkData" OnChange="">
                                    <option value="comma">, (comma delimited)</option>
                                    <option value="semi">; (semi-colon delimited)</option>
                                 </select>
                              </div>
                           </div>

                        </div>
                     </div>
                  </div>
                  <?php
                  if (isset($conts)) {
                     $z = $conts->num_rows;
                     if ($z == '0') {
                        $contsDsp = 'none;';
                     } else {
                        $contsDsp = 'block;';
                     }
                  } else {
                     $contsDsp = 'none;';
                  }
                  ?>
                  <div class="form-group" id="contactListForm" style="display:<?php echo $contsDsp; ?>">
                     <div class="box box-warning" style="border-top-color:<?php echo $accRGB; ?>;display:<?php echo $contsDsp; ?>;" >

                        <label style="padding-left:10px;padding-top:10px;">
                           Add Contact List
                        </label>

                        <div class="box-body" id="contactList" style="padding-bottom:1px;display:block;">
                           <div class="form-group">
                              <select class="form-control" name="contactList" id="contactListId" form="bulkData" >
                                 <?php
                                 echo '<option>Please Select (optional)</option>';
                                 foreach ($conts as $key => $value) {
                                    echo "<option value=" . $value['contact_list_id'] . " >" . $value['contact_list_code'] . "-" . $value['contact_list_name'] . ": " . $value['contact_list_description'] . "</option>";
                                 }
                                 ?>
                              </select>
                           </div>
                        </div>
                     </div>
                  </div>

                  <input type="hidden" name="client" id="client" value="<?php echo $_GET['client']; ?>">
                  <input type="hidden" name="serviceId" value="<?php echo $cl; ?>">
                  <input type="hidden" name="camCode" value="<?php echo $camCode; ?>">
                  <input type="hidden" name="camId" value="<?php echo $camId; ?>">
               </form>
            </div>
            <div class="box-body" style="display:<?php echo $dsp2; ?>;padding-bottom:10px;margin-top:-5px;">
               <div class="form-group" style="margin-bottom:20px;">
                  <a onclick="cont()" class="btn btn-block btn-social btn-dropbox " style="background-color:<?php echo $accRGB; ?>; border: 2px solid; border-radius: 6px !important; float:left; width: 120px; margin-bottom:10px;margin-top:-15px;">
                     <i class="fa fa-arrow-right"></i>Continue
                  </a>
               </div>
            </div>
         </div>
      </div>
   </section>
</aside>

<!--  THE JAVASCRIPT IMPORT ALWAYS APPEARS BEFORE LOCAL JAVASCRIPT AT THE BOTTOM OF THE PAGE -->
<?php include("template_import_script.php"); //must import all scripts first ?>
<!-- END JAVASCRIPT IMPORT -->

<script type="text/javascript">
   $('#scheduledTimer').daterangepicker(
           {
              singleDatePicker: true,
              timePicker: true,
              timePickerIncrement: 5,
              timePicker12Hour: false,
              format: 'YYYY-MM-DD h:mm A',
              timePickerSeconds: false
                      //minDate:Date.now

           },
           function (start, end, label)
           {
              /*alert(label);
               alert(moment(end, 'MMM DD YYYY, h:mmA'));
               var years = moment(start, 'MMM DD YYYY, h:mmA');
               alert(years);*/
              //alert(Date.now);
              $('#scheduledTimer').html(end.format("YYYY-MM-DD HH:mm"));
              //$('#scheduledTimer').html(moment(start));
           }
   );
</script>

<script type="text/javascript">
   /*var area = document.getElementById("smsText");
    var message = document.getElementById("counter");
    //var maxLength = 50;
    var checkLength = function() 
    {
    if(area.value.length == 0) 
    {
    message.innerHTML = "0 characters, 0 SMS's";
    }
    else
    {
    var tot = 0;
    if(area.value.length <= 160)
    {
    tot = Math.ceil((area.value.length-1)/160);
    }
    else
    {
    tot = Math.ceil((area.value.length)/153);
    }
    message.innerHTML = area.value.length + " characters, " + tot + " SMS's";
    }
    }
    setInterval(checkLength, 10);*/
</script>

<script type="text/javascript">
   function isNumberKey(evt)
   {
      var charCode = (evt.which) ? evt.which : evt.keyCode;

      if (charCode == 44)
         return true;

      /*alert(charCode);*/
      if (charCode == 46)
         return false;

      if (charCode != 46 && charCode > 31
              && (charCode < 48 || charCode > 57))
         return false;

      return true;
   }

   function getCamp()
   {
      var clientOp = clientSel.options;
      var name = clientOp[clientOp.selectedIndex].value;

      var options = camp.options;
      var id = options[options.selectedIndex].id;

      window.location = "bulksms.php?client=" + name + "&campId=" + id;
      //var value   = options[options.selectedIndex].value;
   }

   function checkDelStatus(v)
   {
      if (v == 'Scheduled')
      {
         document.getElementById('schedulerBox').style.display = 'block';
      } else
      {
         document.getElementById('schedulerBox').style.display = 'none';
      }
   }

   /*function checkTemplate(tempVal)
    {
    var freeTextBox = document.getElementById('freeText');
    if(tempVal == 'Enter SMS text')
    {
    freeTextBox.style.display = 'block';
    }
    else
    {
    freeTextBox.style.display = 'none';
    }
    }*/

   function cont()
   {
      var fileInput = document.getElementById('uploadCSV').value;
      var fileErr = document.getElementById('fileError');
      var referInput = document.getElementById('refer').value;
      var referErr = document.getElementById('referError');
      var contList = document.getElementById('contactListId').value;
      var validater = 0;

      if (fileInput == '' && contList == 'Please Select (optional)')
      {
         //alert("hi+" + fileInput + "+");
         fileErr.style.display = 'block';
      } else
      {
         //alert("hi+" + fileInput + "+");
         fileErr.style.display = 'none';
         validater++;
      }

      if (referInput == '')
      {
         //alert("hi+" + fileInput + "+");
         referErr.style.display = 'block';
      } else
      {
         //alert("hi+" + fileInput + "+");
         referErr.style.display = 'none';
         validater++;
      }

      if (validater == 2)
      {
         var sTime = document.getElementById('scheduledTimer').innerHTML;
         //alert(sTime);
         var hiddenField = document.createElement("input");
         hiddenField.setAttribute("type", "hidden");
         hiddenField.setAttribute("name", "sTime");
         hiddenField.setAttribute("value", sTime);

         bulkData.appendChild(hiddenField);
         bulkData.submit();
      }
   }

   /*function validateBulk()
    {
    var freeTextBox = document.getElementById('freeText').style.display;
    var smsErr = document.getElementById('smsError');
    var smstext = document.getElementById('smsText').value;
    var fileInput = document.getElementById('uploadCSV').value;
    var fileErr = document.getElementById('fileError');
    var tempErr = document.getElementById('tempError');
    var tempSel = temper.options;
    var tempSelect = tempSel[tempSel.selectedIndex].value;
    var validSubmit = 0;
    
    if(tempSelect == "Please Select")
    {
    tempErr.style.display = 'block';
    }
    else
    {
    tempErr.style.display = 'none';
    validSubmit++;
    }
    
    if(tempSelect != "Please Select" && tempSelect != "Enter SMS text" )
    {
    document.getElementById('smsText').value = '';
    //alert('12' + document.getElementById('smsText').value);
    }
    
    if(fileInput == '')
    {
    //alert("hi+" + fileInput + "+");
    fileErr.style.display = 'block';
    }
    else
    {
    //alert("hi+" + fileInput + "+");
    fileErr.style.display = 'none';
    validSubmit++;
    //bulkData.submit();      
    }
    
    if(freeTextBox == 'block' && smstext.length > 0)
    {
    //alert("hi+" + fileInput);
    smsErr.style.display = 'none';
    validSubmit++
    //bulkData.submit();
    }
    else
    {
    smsErr.style.display = 'block';
    //error
    }
    
    //alert("hi+" + validSubmit);
    if(validSubmit >= 2)
    {
    $(".loader").fadeIn("slow");
    $(".loaderIcon").fadeIn("slow");
    bulkData.submit();
    }
    }*/
</script>
<script type="text/javascript">
   function reloadOnSelect(name)
   {
      $(".loader").fadeIn("slow");
      $(".loaderIcon").fadeIn("slow");
      window.location = "bulksms.php?client=" + name;
   }
</script>
<script type="text/javascript">
   $(window).load(function () {
      $(".loader").fadeOut("slow");
      $(".loaderIcon").fadeOut("slow");
   })
</script>

<!-- Template Footer -->

