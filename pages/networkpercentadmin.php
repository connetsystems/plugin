<?php
/**
 * Include the header template which sets up the HTML
 *
 * Don't forget to include template_import_script before any Javascripting
 * Don't forget to include template footer.php at the bottom of the page as well
 */
$headerPageTitle = "Global Network Summary"; //set the page title for the template import
TemplateHelper::initialize();


$dir = '../img/serviceproviders';
$serProviders = scandir($dir);


if (isset($_GET['range'])) {
   $dateArr = explode(' - ', $_GET['range']);
   $startDate = $dateArr[0];
   $endDate = $dateArr[1];
} else {

   //$startDate = strtotime('today', time())*1000;
   $startDate = (strtotime('today midnight')) * 1000;
   $endDate = (time() * 1000);
}

$startDateConv = date('Y-m-d', ($startDate / 1000));
$endDateConv = date('Y-m-d', ($endDate / 1000));


$elasticGrouping = "1d";

if (isset($_GET['elastic_grouping'])) {
   $elasticGrouping = $_GET['elastic_grouping'];
}

$qryMo = '{
                      "size": 0,
                      "aggs": {
                        "2": {
                          "date_histogram": {
                            "field": "timestamp",
                            "interval": "' . $elasticGrouping . '",
                            "pre_zone": "+02:00",
                            "pre_zone_adjust_large_interval": false,
                            "min_doc_count": 1,
                            "extended_bounds": {
                              "min": ' . $startDate . ',
                              "max": ' . $endDate . '
                            }
                          },
                          "aggs": {
                            "3": {
                              "terms": {
                                "field": "meta.mccmnc",
                                "size": 0,
                                "order": {
                                  "_count": "desc"
                                }
                              },
                              "aggs": {
                                "4": {
                                  "terms": {
                                    "field": "meta.smsc",
                                    "size": 0,
                                    "order": {
                                      "_count": "desc"
                                    }
                                  }
                                }
                              }
                            }
                          }
                        }
                      },
                      "query": {
                        "filtered": {
                          "query": {
                            "query_string": {
                              "query": "_type:core_mosms",
                              "analyze_wildcard": true
                            }
                          },
                          "filter": {
                            "bool": {
                              "must": [
                                {
                                  "range": {
                                    "timestamp": {
                                      "gte": ' . $startDate . ',
                                      "lte": ' . $endDate . '
                                    }
                                  }
                                }
                              ],
                              "must_not": []
                            }
                          }
                        }
                      }
                    }';


$allStats2 = runRawMOSMSElasticsearchQuery($qryMo);

$qry = '{
            "size": 0,
              "aggs": {
                "5": {
                  "date_histogram": {
                    "field": "timestamp",
                    "interval": "' . $elasticGrouping . '",
                    "pre_zone": "+02:00",
                    "pre_zone_adjust_large_interval": false,
                    "min_doc_count": 1,
                    "extended_bounds": {
                      "min": ' . $startDate . ',
                      "max": ' . $endDate . '
                    }
                  },
                  "aggs": {
                    "mccmnc": {
                      "terms": {
                        "field": "mccmnc",
                        "size": 0,
                        "order": {
                          "1": "desc"
                        }
                      },
                      "aggs": {
                        "1": {
                          "sum": {
                            "script": "doc[\'billing_units\'].value",
                            "lang": "expression"
                          }
                        },
                        "smsc": {
                          "terms": {
                            "field": "smsc",
                            "size": 0,
                            "order": {
                              "1": "desc"
                            }
                          },
                          "aggs": {
                            "1": {
                              "sum": {
                                "script": "doc[\'billing_units\'].value",
                                "lang": "expression"
                              }
                            },
                            "dlr": {
                              "terms": {
                                "field": "dlr",
                                "size": 0,
                                "order": {
                                  "dlr_units": "desc"
                                }
                              },
                              "aggs": {
                                "dlr_units": {
                                  "sum": {
                                    "script": "doc[\'billing_units\'].value",
                                    "lang": "expression"
                                  }
                                 },
                    "rdnc": {
                      "terms": {
                        "field": "rdnc",
                        "size": 0,
                        "order": {
                          "1": "desc"
                        }
                      },
                      "aggs": {
                        "1": {
                          "sum": {
                            "script": "doc[\'billing_units\'].value",
                            "lang": "expression"
                          }
                        }
                      }
                    }
                  }
                }
              }
            }
          }
        }
      }
    }
  
},
              "query": {
                "filtered": {
                  "filter": {
                    "bool": {
                      "must": [
                        {
                          "range": {
                            "timestamp": {
                              "gte": ' . $startDate . ',
                              "lte": ' . $endDate . '
                            }
                          }
                        }
                      ],
                      "must_not": []
                    }
                  }
                }
              }
            }
            ';

$allStats = runRawMTSMSElasticsearchQuery($qry);

$sort = urlencode('"timestamp":"desc"');
$tempFail = 0;
$sents = 0;
////////////////////////////////////////////////
////////////////////////////////////////////////
////////////////////////////////////////////////
?>

<aside class="right-side">
   <section class="content-header">
      <h1>
         Global Network Summary
      </h1>
   </section>

   <section class="content">
      <div class="row col-lg-12">
         <div class="box box-solid">
            <form action="ereport.php" method="get" id="clientList" class="sidebar-form" style="border:0px;">
               <div class="form-group" style="padding-top:15px; padding-bottom:35px;">
                  <!--div class="callout callout-info">
                      <h4>Instructions</h4>
                      <p>In this report, you can view all the response % per network for the date range specified below.</p>
                  </div-->
                  <div id="reportrange" class="select pull-left" style="cursor: pointer;">
                     <i class="fa fa-calendar fa-lg"></i>
                     <span style="font-size:15px"><?php echo $startDateConv . " - " . $endDateConv; ?></span><b class="caret"></b>
                  </div>
               </div>

            </form>

         </div>
         <a class="btn btn-default" href="networkpercentadmin.php?range=<?php echo $_GET['range']; ?>&elastic_grouping=1d">GROUP DAILY</a>
         <a class="btn btn-default" href="networkpercentadmin.php?range=<?php echo $_GET['range']; ?>&elastic_grouping=1M">GROUP BY MONTH</a>
         <div class="box-body">
            <div class="box-body table-responsive no-padding">
               <table id="example2"  class="table table-hover">
                  <thead>
                     <tr>
                        <th style="width:100px;">Date</th>
                        <th>Network</th>
                        <th>SMSC</th>
                        <th style="background-color:#737AA6">Sent&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</th>
                        <th style="background-color:#737AA6">Units&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</th>
                        <th style="background-color:#DEA52F">Pend&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</th>
                        <th style="background-color:#DEA52F">Pend&nbsp;&nbsp;%&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</th>
                        <th style="background-color:#8BA808">Delv&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</th>
                        <th style="background-color:#8BA808">Delv&nbsp;&nbsp;%&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</th>
                        <th style="background-color:#f56954">Fail&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</th>
                        <th style="background-color:#f56954">Fail&nbsp;&nbsp;%&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</th>
                        <th style="background-color:#B8252B">Rej&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</th>
                        <th style="background-color:#B8252B">Rej&nbsp;&nbsp;%&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</th>
                        <th style="background-color:#979696">Exc&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</th>
                        <th style="background-color:#979696">Exc&nbsp;&nbsp;%&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</th>
                        <th style="background-color:#92e7ed">MO&nbsp;Total&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</th>
                        <th style="background-color:#92e7ed">MO/Delv&nbsp;%&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</th>
                     </tr>
                  </thead>
                  <tbody>
                     <?php
                     $moTota = 0;
                     if (isset($allStats)) {
                        $dlrArr681 = array();
                        $dlrArr680 = array();
                        $dlrArr682 = array();
                        $dlrArr4288 = array();
                        $dlrArrREJ = array();
                        $dlrPos680 = array();
                        $dlrPos681 = array();
                        $dlrPos682 = array();
                        $dlrPos4288 = array();
                        $dlrPosREJ = array();

                        $networkCount = array();
                        $smscCount = array();
                        $sentCount = 0;
                        $unitsCount = 0;
                        $pendingCount = 0;
                        $pendingPCount = 0;
                        $delCount = 0;
                        $delPCount = 0;
                        $failCount = 0;
                        $failPCount = 0;
                        $rejCount = 0;
                        $rejPCount = 0;
                        $exCount = 0;
                        //$exPCount = 0;
                        $moCount = 0;
                        $moPCount = 0;
                        $rowCount = 0;

                        foreach ($allStats as $key => $value) {
                           if ($key == 'aggregations') {
                              for ($i = 0; $i < count($value['5']['buckets']); $i++) {
                                 for ($j = 0; $j < count($value['5']['buckets'][$i]['mccmnc']['buckets']); $j++) {
                                    for ($l = 0; $l < count($value['5']['buckets'][$i]['mccmnc']['buckets'][$j]['smsc']['buckets']); $l++) {
                                       $smsc = $value['5']['buckets'][$i]['mccmnc']['buckets'][$j]['smsc']['buckets'][$l]['key'];
                                       $posSmsc = strrpos($smsc, '_');
                                       $smsc = ucfirst(substr($smsc, 0, $posSmsc));

                                       if (!in_array($value['5']['buckets'][$i]['mccmnc']['buckets'][$j]['smsc']['buckets'][$l]['key'], $smscCount)) {
                                          array_push($smscCount, $value['5']['buckets'][$i]['mccmnc']['buckets'][$j]['smsc']['buckets'][$l]['key']);
                                       }

                                       echo '<tr>';
                                       $rowCount++;
                                       $dater = strstr($value['5']['buckets'][$i]['key_as_string'], 'T', true);
                                       $dater2 = strtotime($dater);
                                       echo '<td style="vertical-align:middle;">' . $dater . '</td>';
                                       $mcc = $value['5']['buckets'][$i]['mccmnc']['buckets'][$j]['key'];
                                       $netw = getNetFromMCC($mcc);
                                       if (!in_array($value['5']['buckets'][$i]['mccmnc']['buckets'][$j]['key'], $networkCount)) {
                                          array_push($networkCount, $value['5']['buckets'][$i]['mccmnc']['buckets'][$j]['key']);
                                       }

                                       $counter = 0;
                                       foreach ($serProviders as $key3 => $value3) {
                                          $value3 = strstr($value3, '.', true);

                                          if (!empty($value3) && strpos($netw, $value3) !== false) {
                                             echo "<td style='vertical-align:middle;'><img src='../img/serviceproviders/" . $value3 . ".png'>&nbsp;&nbsp;" . ucfirst($netw) . ' (' . $value['5']['buckets'][$i]['mccmnc']['buckets'][$j]['key'] . ')</td>';
                                             $counter++;
                                             break;
                                          }
                                       }
                                       if ($counter == 0) {
                                          echo "<td style='vertical-align:middle;'><img src='../img/serviceproviders/Special.png'>&nbsp;&nbsp;" . ucfirst($netw) . ' (' . $value['5']['buckets'][$i]['mccmnc']['buckets'][$j]['key'] . ')</td>';
                                       }

                                       $counter = 0;
                                       foreach ($serProviders as $key3 => $value3) {
                                          if ($value3 == (lcfirst($smsc) . '.png')) {
                                             echo "<td style='vertical-align:middle;'><img src='../img/serviceproviders/" . lcfirst($smsc) . ".png'>&nbsp;&nbsp;" . $smsc . '</td>';
                                             $counter++;
                                          }
                                       }
                                       if ($counter == 0) {
                                          //echo "<td style='vertical-align:middle;'><img src='../img/serviceproviders/Special.png'>111&nbsp;&nbsp;".$smsc.'</td>';
                                          if (lcfirst($smsc) == 'mtn') {
                                             $smsc = 'MTN';
                                          }
                                          //Vodacom provider name is lower case vodacom_smsc hat to update the varible to Uppercase
                                          if (lcfirst($smsc) == 'vodacom') {
                                             $smsc = 'Vodacom SA';
                                          }
                                          //MTC provider name is lower case mtc_smsc hat to update the varible to Uppercase
                                          if (lcfirst($smsc) == 'mtc') {
                                             $smsc = 'MTC';
                                          }
                                          echo "<td style='vertical-align:middle;'><img src='../img/serviceproviders/" . $smsc . ".png'>&nbsp;&nbsp;" . $smsc . '</td>';
                                       }

                                       echo '<td style="vertical-align:middle;background-color:#adb1cb; text-align:right"><a style="color:#333333;cursor:pointer;" onclick="viewMsg(' . urlencode($dater2) . ', `' . $mcc . '`, `' . $value['5']['buckets'][$i]['mccmnc']['buckets'][$j]['smsc']['buckets'][$l]['key'] . '`)" >' . number_format($value['5']['buckets'][$i]['mccmnc']['buckets'][$j]['smsc']['buckets'][$l]['doc_count'], 0, '.', '&nbsp') . '</a></td>';
                                       $sentCount += $value['5']['buckets'][$i]['mccmnc']['buckets'][$j]['smsc']['buckets'][$l]['doc_count'];

                                       echo '<td style="vertical-align:middle;background-color:#adb1cb; text-align:right">' . number_format($value['5']['buckets'][$i]['mccmnc']['buckets'][$j]['smsc']['buckets'][$l]['1']['value'], 0, '.', '&nbsp') . '</td>';
                                       $unitsCount += $value['5']['buckets'][$i]['mccmnc']['buckets'][$j]['smsc']['buckets'][$l]['1']['value'];

                                       $dlrArr680[$value['5']['buckets'][$i]['mccmnc']['buckets'][$j]['smsc']['buckets'][$l]['key']] = 0;
                                       $dlrArr681[$value['5']['buckets'][$i]['mccmnc']['buckets'][$j]['smsc']['buckets'][$l]['key']] = 0;
                                       $dlrArr682[$value['5']['buckets'][$i]['mccmnc']['buckets'][$j]['smsc']['buckets'][$l]['key']] = 0;
                                       $dlrArr4288[$value['5']['buckets'][$i]['mccmnc']['buckets'][$j]['smsc']['buckets'][$l]['key']] = 0;
                                       $dlrArrREJ[$value['5']['buckets'][$i]['mccmnc']['buckets'][$j]['smsc']['buckets'][$l]['key']] = 0;

                                       for ($k = 0; $k < count($value['5']['buckets'][$i]['mccmnc']['buckets'][$j]['smsc']['buckets'][$l]['dlr']['buckets']); $k++) {
                                          $dlrPos = (integer) $value['5']['buckets'][$i]['mccmnc']['buckets'][$j]['smsc']['buckets'][$l]['dlr']['buckets'][$k]['key'];
                                          //if(in_array('680', $value['5']['buckets'][$i]['mccmnc']['buckets'][$j]['smsc']['buckets'][$l]['dlr']['buckets'][$k]))
                                          if (($dlrPos & 32) == 32 && ($dlrPos & 1) == 0 && ($dlrPos & 2) == 0 && ($dlrPos & 16) == 0) {
                                             $countRdnc = count($value['5']['buckets'][$i]['mccmnc']['buckets'][$j]['smsc']['buckets'][$l]['dlr']['buckets'][$k]['rdnc']['buckets']);
                                             for ($jj = 0; $jj < $countRdnc; $jj++) {
                                                //if($value['5']['buckets'][$i]['mccmnc']['buckets'][$j]['smsc']['buckets'][$l]['dlr']['buckets'][$k]['rdnc']['buckets'][$jj]['key'] == 0)
                                                {
                                                   if (!in_array($dlrPos, $dlrPos680)) {
                                                      array_push($dlrPos680, $dlrPos);
                                                   }
                                                   if ($dlrArr680[$value['5']['buckets'][$i]['mccmnc']['buckets'][$j]['smsc']['buckets'][$l]['key']] == 0) {
                                                      //$dlrArr680['5']['buckets']['0']['dlr']['buckets'] = $value['5']['buckets']['0']['dlr']['buckets'][$i]['rdnc']['buckets'][$j]['1']['value'];
                                                      $dlrArr680[$value['5']['buckets'][$i]['mccmnc']['buckets'][$j]['smsc']['buckets'][$l]['key']] = $value['5']['buckets'][$i]['mccmnc']['buckets'][$j]['smsc']['buckets'][$l]['dlr']['buckets'][$k]['rdnc']['buckets'][$jj]['doc_count'];
                                                   } else {
                                                      $dlrArr680[$value['5']['buckets'][$i]['mccmnc']['buckets'][$j]['smsc']['buckets'][$l]['key']] += $value['5']['buckets'][$i]['mccmnc']['buckets'][$j]['smsc']['buckets'][$l]['dlr']['buckets'][$k]['rdnc']['buckets'][$jj]['doc_count'];
                                                   }
                                                }
                                             }
                                          }
                                          if (($dlrPos & 1) == 1) {
                                             if (!in_array($dlrPos, $dlrPos681)) {
                                                array_push($dlrPos681, $dlrPos);
                                             }
                                             if ($dlrArr681[$value['5']['buckets'][$i]['mccmnc']['buckets'][$j]['smsc']['buckets'][$l]['key']] == 0) {
                                                $dlrArr681[$value['5']['buckets'][$i]['mccmnc']['buckets'][$j]['smsc']['buckets'][$l]['key']] = $value['5']['buckets'][$i]['mccmnc']['buckets'][$j]['smsc']['buckets'][$l]['dlr']['buckets'][$k]['doc_count'];
                                             } else {
                                                $dlrArr681[$value['5']['buckets'][$i]['mccmnc']['buckets'][$j]['smsc']['buckets'][$l]['key']] += $value['5']['buckets'][$i]['mccmnc']['buckets'][$j]['smsc']['buckets'][$l]['dlr']['buckets'][$k]['doc_count'];
                                             }
                                          } elseif (($dlrPos & 2) == 2) {
                                             if (!in_array($dlrPos, $dlrPos682)) {
                                                array_push($dlrPos682, $dlrPos);
                                             }
                                             if ($dlrArr682[$value['5']['buckets'][$i]['mccmnc']['buckets'][$j]['smsc']['buckets'][$l]['key']] == 0) {
                                                $dlrArr682[$value['5']['buckets'][$i]['mccmnc']['buckets'][$j]['smsc']['buckets'][$l]['key']] = $value['5']['buckets'][$i]['mccmnc']['buckets'][$j]['smsc']['buckets'][$l]['dlr']['buckets'][$k]['doc_count'];
                                             } else {
                                                $dlrArr682[$value['5']['buckets'][$i]['mccmnc']['buckets'][$j]['smsc']['buckets'][$l]['key']] += $value['5']['buckets'][$i]['mccmnc']['buckets'][$j]['smsc']['buckets'][$l]['dlr']['buckets'][$k]['doc_count'];
                                             }
                                          }

                                          $countRdnc = count($value['5']['buckets'][$i]['mccmnc']['buckets'][$j]['smsc']['buckets'][$l]['dlr']['buckets'][$k]['rdnc']['buckets']);
                                          for ($jj = 0; $jj < $countRdnc; $jj++) {
                                             if ($value['5']['buckets'][$i]['mccmnc']['buckets'][$j]['smsc']['buckets'][$l]['dlr']['buckets'][$k]['rdnc']['buckets'][$jj]['key'] != 0) {
                                                if (!in_array($dlrPos, $dlrPos4288)) {
                                                   array_push($dlrPos4288, $dlrPos);
                                                }
                                                if ($dlrArr4288[$value['5']['buckets'][$i]['mccmnc']['buckets'][$j]['smsc']['buckets'][$l]['key']] == 0) {
                                                   //$dlrArr4288['5']['buckets']['0']['dlr']['buckets'] = $value['5']['buckets']['0']['dlr']['buckets'][$i]['rdnc']['buckets'][$j]['1']['value'];
                                                   $dlrArr4288[$value['5']['buckets'][$i]['mccmnc']['buckets'][$j]['smsc']['buckets'][$l]['key']] = $value['5']['buckets'][$i]['mccmnc']['buckets'][$j]['smsc']['buckets'][$l]['dlr']['buckets'][$k]['rdnc']['buckets'][$jj]['doc_count'];
                                                } else {
                                                   $dlrArr4288[$value['5']['buckets'][$i]['mccmnc']['buckets'][$j]['smsc']['buckets'][$l]['key']] += $value['5']['buckets'][$i]['mccmnc']['buckets'][$j]['smsc']['buckets'][$l]['dlr']['buckets'][$k]['rdnc']['buckets'][$jj]['doc_count'];
                                                }
                                             }
                                          }
                                          if (($dlrPos & 16) == 16) {
                                             if (!in_array($dlrPos, $dlrPosREJ)) {
                                                array_push($dlrPosREJ, $dlrPos);
                                             }
                                             if ($dlrArrREJ[$value['5']['buckets'][$i]['mccmnc']['buckets'][$j]['smsc']['buckets'][$l]['key']] == 0) {
                                                $dlrArrREJ[$value['5']['buckets'][$i]['mccmnc']['buckets'][$j]['smsc']['buckets'][$l]['key']] = $value['5']['buckets'][$i]['mccmnc']['buckets'][$j]['smsc']['buckets'][$l]['dlr']['buckets'][$k]['doc_count'];
                                             } else {
                                                $dlrArrREJ[$value['5']['buckets'][$i]['mccmnc']['buckets'][$j]['smsc']['buckets'][$l]['key']] += $value['5']['buckets'][$i]['mccmnc']['buckets'][$j]['smsc']['buckets'][$l]['dlr']['buckets'][$k]['doc_count'];
                                             }
                                          }
                                       }

                                       $pendUrlAttach = '';
                                       for ($il = 0; $il < count($dlrPos680); $il++) {
                                          $pendUrlAttach .= (string) $dlrPos680[$il] . '.';
                                       }
                                       $pendUrlAttach = substr($pendUrlAttach, 0, -1);
                                       $pendUrlAttach = ("'" . $pendUrlAttach . "'");

                                       $deliUrlAttach = '';
                                       for ($il = 0; $il < count($dlrPos681); $il++) {
                                          $deliUrlAttach .= (string) $dlrPos681[$il] . '.';
                                       }
                                       $deliUrlAttach = substr($deliUrlAttach, 0, -1);
                                       $deliUrlAttach = ("'" . $deliUrlAttach . "'");

                                       $failUrlAttach = '';
                                       for ($il = 0; $il < count($dlrPos682); $il++) {
                                          $failUrlAttach .= (string) $dlrPos682[$il] . '.';
                                       }
                                       $failUrlAttach = substr($failUrlAttach, 0, -1);
                                       $failUrlAttach = ("'" . $failUrlAttach . "'");

                                       $exclUrlAttach = '';
                                       for ($il = 0; $il < count($dlrPos4288); $il++) {
                                          $exclUrlAttach .= (string) $dlrPos4288[$il] . '.';
                                       }
                                       $exclUrlAttach = substr($exclUrlAttach, 0, -1);
                                       $exclUrlAttach = ("'" . $exclUrlAttach . "'");

                                       $rejeUrlAttach = '';
                                       for ($il = 0; $il < count($dlrPosREJ); $il++) {
                                          $rejeUrlAttach .= (string) $dlrPosREJ[$il] . '.';
                                       }
                                       $rejeUrlAttach = substr($rejeUrlAttach, 0, -1);
                                       $rejeUrlAttach = ("'" . $rejeUrlAttach . "'");

                                       $cnt = 0;
                                       foreach ($dlrArr680 as $key2 => $value2) {
                                          if ($key2 == $value['5']['buckets'][$i]['mccmnc']['buckets'][$j]['smsc']['buckets'][$l]['key']) {
                                             $cnt = 1;
                                             echo '<td style="vertical-align:middle;background-color:#e9cb8c; text-align:right"><a style="color:#333333;cursor:pointer;" onclick="viewMsgVar(' . urlencode($dater2) . ', ' . $pendUrlAttach . ',' . $pendingCount . ',`' . $mcc . '`, `' . $value['5']['buckets'][$i]['mccmnc']['buckets'][$j]['smsc']['buckets'][$l]['key'] . '`,`Pending`)">' . number_format($value2, 0, '.', '&nbsp;') . '</a></td>';
                                             echo '<td style="vertical-align:middle;background-color:#e9cb8c; text-align:right">' . number_format(($value2 / $value['5']['buckets'][$i]['mccmnc']['buckets'][$j]['smsc']['buckets'][$l]['doc_count'] * 100), 2) . ' %</td>';
                                             //array_shift($dlrArr680);
                                             $pendingCount += $value2;
                                             $pendingPCount += number_format(($value2 / $value['5']['buckets'][$i]['mccmnc']['buckets'][$j]['smsc']['buckets'][$l]['doc_count'] * 100), 2);
                                          }
                                       }
                                       if ($cnt == 0) {
                                          echo '<td style="background-color:#e9cb8c;">' . '' . '</td>';
                                          echo '<td style="background-color:#e9cb8c;">' . '' . '</td>';
                                       }

                                       $cnt = 0;
                                       foreach ($dlrArr681 as $key2 => $value2) {
                                          if ($key2 == $value['5']['buckets'][$i]['mccmnc']['buckets'][$j]['smsc']['buckets'][$l]['key']) {
                                             $cnt = 1;
                                             echo '<td style="vertical-align:middle;background-color:#c0d26e; text-align:right"><a style="color:#333333;cursor:pointer;" onclick="viewMsgVar(' . urlencode($dater2) . ', ' . $deliUrlAttach . ',' . $delCount . ',`' . $mcc . '`, `' . $value['5']['buckets'][$i]['mccmnc']['buckets'][$j]['smsc']['buckets'][$l]['key'] . '`,`Delivered`)">' . number_format($value2, 0, '.', '&nbsp;') . '</a></td>';
                                             echo '<td style="vertical-align:middle;background-color:#c0d26e; text-align:right">' . number_format(($value2 / $value['5']['buckets'][$i]['mccmnc']['buckets'][$j]['smsc']['buckets'][$l]['doc_count'] * 100), 2) . ' %</td>';
                                             //array_shift($dlrArr681);
                                             $delCount += $value2;
                                             $delPCount += number_format(($value2 / $value['5']['buckets'][$i]['mccmnc']['buckets'][$j]['smsc']['buckets'][$l]['doc_count'] * 100), 2);
                                          }
                                       }
                                       if ($cnt == 0) {
                                          echo '<td style="background-color:#c0d26e;">' . '' . '</td>';
                                          echo '<td style="background-color:#c0d26e;">' . '' . '</td>';
                                       }

                                       $cnt = 0;
                                       foreach ($dlrArr682 as $key2 => $value2) {
                                          if ($key2 == $value['5']['buckets'][$i]['mccmnc']['buckets'][$j]['smsc']['buckets'][$l]['key']) {
                                             $cnt = 1;
                                             echo '<td style="vertical-align:middle;background-color:#f8b6ac; text-align:right"><a style="color:#333333;cursor:pointer;" onclick="viewMsgVar(' . urlencode($dater2) . ', ' . $failUrlAttach . ',' . $failCount . ',`' . $mcc . '`, `' . $value['5']['buckets'][$i]['mccmnc']['buckets'][$j]['smsc']['buckets'][$l]['key'] . '`,`Failed`)">' . number_format($value2, 0, '.', '&nbsp;') . '</a></td>';
                                             echo '<td style="vertical-align:middle;background-color:#f8b6ac; text-align:right">' . number_format(($value2 / $value['5']['buckets'][$i]['mccmnc']['buckets'][$j]['smsc']['buckets'][$l]['doc_count'] * 100), 2) . ' %</td>';
                                             //array_shift($dlrArr682);
                                             $failCount += $value2;
                                             $failPCount += number_format(($value2 / $value['5']['buckets'][$i]['mccmnc']['buckets'][$j]['smsc']['buckets'][$l]['doc_count'] * 100), 2);
                                             $tempFail = $value2;
                                          }
                                       }
                                       if ($cnt == 0) {
                                          echo '<td style="background-color:#f8b6ac;">' . '' . '</td>';
                                          echo '<td style="background-color:#f8b6ac;">' . '' . '</td>';
                                       }

                                       $cnt = 0;
                                       foreach ($dlrArrREJ as $key2 => $value2) {
                                          if ($key2 == $value['5']['buckets'][$i]['mccmnc']['buckets'][$j]['smsc']['buckets'][$l]['key']) {
                                             $cnt = 1;
                                             echo '<td style="vertical-align:middle;background-color:#df8c8f; text-align:right"><a style="color:#333333;cursor:pointer;" onclick="viewMsgVar(' . urlencode($dater2) . ', ' . $rejeUrlAttach . ',' . $rejCount . ',`' . $mcc . '`, `' . $value['5']['buckets'][$i]['mccmnc']['buckets'][$j]['smsc']['buckets'][$l]['key'] . '`,`Rejected`)">' . number_format($value2, 0, '.', '&nbsp;') . '</a></td>';
                                             echo '<td style="vertical-align:middle;background-color:#df8c8f; text-align:right">' . number_format(($value2 / $value['5']['buckets'][$i]['mccmnc']['buckets'][$j]['smsc']['buckets'][$l]['doc_count'] * 100), 2) . ' %</td>';
                                             //array_shift($dlrArr680);
                                             $rejCount += $value2;
                                             $rejPCount += number_format(($value2 / $value['5']['buckets'][$i]['mccmnc']['buckets'][$j]['smsc']['buckets'][$l]['doc_count'] * 100), 2);
                                          }
                                       }
                                       if ($cnt == 0) {
                                          echo '<td style="background-color:#df8c8f;">' . '' . '</td>';
                                          echo '<td style="background-color:#df8c8f;">' . '' . '</td>';
                                       }

                                       $cnt = 0;
                                       foreach ($dlrArr4288 as $key2 => $value2) {
                                          if ($key2 == $value['5']['buckets'][$i]['mccmnc']['buckets'][$j]['smsc']['buckets'][$l]['key']) {
                                             $cnt = 1;
                                             echo '<td style="vertical-align:middle;background-color:#b1b1b1; text-align:right; cursor:pointer;" onclick="viewMsgVarRDNC(' . urlencode($dater2) . ', ' . $exclUrlAttach . ',' . $value2 . ',`' . $mcc . '`, `' . $value['5']['buckets'][$i]['mccmnc']['buckets'][$j]['smsc']['buckets'][$l]['key'] . '`,`Excluded`)">' . number_format($value2, 0, '.', '&nbsp;') . '</td>';
                                             if ($tempFail == 0) {
                                                echo '<td style="vertical-align:middle;background-color:#b1b1b1; text-align:right">0.00 %</td>';
                                             } else {
                                                echo '<td style="vertical-align:middle;background-color:#b1b1b1; text-align:right">' . number_format(($value2 / $tempFail * 100), 2) . ' %</td>';
                                             }
                                             //array_shift($dlrArr682);
                                             $exCount += $value2;
                                             /* if($tempFail == 0)
                                               {
                                               $exPCount += 0;
                                               }
                                               else
                                               {
                                               $exPCount += number_format(($value2/$tempFail*100), 2);
                                               } */
                                          }
                                       }
                                       if ($cnt == 0) {
                                          echo '<td style="background-color:#b1b1b1;">' . '' . '</td>';
                                          echo '<td style="background-color:#b1b1b1;">' . '' . '</td>';
                                       }

                                       foreach ($allStats2 as $key7 => $value7) {
                                          if ($key7 = 'aggregations' && isset($value7['2']['buckets'])) {
                                             if (count($value7['2']['buckets']) > 0) {
                                                for ($y = 0; $y < count($value7['2']['buckets']); $y++) {
                                                   if ($value7['2']['buckets'][$y]['key_as_string'] == $value['5']['buckets'][$i]['key_as_string']) {
                                                      if (count($value7['2']['buckets'][$y]['3']['buckets']) > 0) {
                                                         $cnt = 0;
                                                         for ($x = 0; $x < count($value7['2']['buckets'][$y]['3']['buckets']); $x++) {
                                                            if ($value['5']['buckets'][$i]['mccmnc']['buckets'][$j]['key'] == $value7['2']['buckets'][$y]['3']['buckets'][$x]['key']) {
                                                               if (count($value7['2']['buckets'][$y]['3']['buckets'][$x]['4']['buckets']) > 0) {
                                                                  for ($w = 0; $w < count($value7['2']['buckets'][$y]['3']['buckets'][$x]['4']['buckets']); $w++) {
                                                                     if ($value7['2']['buckets'][$y]['3']['buckets'][$x]['4']['buckets'][$w]['key'] == $value['5']['buckets'][$i]['mccmnc']['buckets'][$j]['smsc']['buckets'][$l]['key']) {
                                                                        //echo '<br>'.$value7['2']['buckets'][$y]['3']['buckets'][$x]['4']['buckets'][$w]['key'].'---'.$value['5']['buckets'][$i]['mccmnc']['buckets'][$j]['smsc']['buckets'][$l]['key'];
                                                                        $cnt = 1;
                                                                        echo '<td style="background-color:#cfebed; text-align:right;"><a style="color:#333333;cursor:pointer;" onclick="gotoMo(`' . urlencode(strstr($value['5']['buckets'][$i]['key_as_string'], 'T', true)) . '`, ' . $mcc . ', `' . $smsc . '`, ' . $value7['2']['buckets'][$y]['3']['buckets'][$x]['4']['buckets'][$w]['doc_count'] . ');">' . number_format($value7['2']['buckets'][$y]['3']['buckets'][$x]['4']['buckets'][$w]['doc_count'], 0, '.', ' ') . '</a></td>';
                                                                        $moTota += $value7['2']['buckets'][$y]['3']['buckets'][$x]['doc_count'];

                                                                        if ($dlrArr681[$value['5']['buckets'][$i]['mccmnc']['buckets'][$j]['smsc']['buckets'][$l]['key']] != 0) {
                                                                           echo '<td style="background-color:#cfebed; text-align:right;">' . round($value7['2']['buckets'][$y]['3']['buckets'][$x]['4']['buckets'][$w]['doc_count'] / $dlrArr681[$value['5']['buckets'][$i]['mccmnc']['buckets'][$j]['smsc']['buckets'][$l]['key']] * 100, 2) . ' %</td>';
                                                                        } else {
                                                                           echo '<td style="background-color:#cfebed; text-align:right;">-</td>';
                                                                        }
                                                                     }
                                                                  }
                                                               }
                                                            }
                                                         }
                                                         if ($cnt == 0) {
                                                            echo '<td style="background-color:#cfebed; text-align:right;">0</td>';
                                                            echo '<td style="background-color:#cfebed; text-align:right;">0</td>';
                                                         }
                                                      }
                                                   }
                                                }
                                             }
                                          }
                                       }
                                       /* $cnt = 0;
                                         foreach ($moData as $key4 => $value4)
                                         {
                                         if($dater == $value4['timestamp'])
                                         {
                                         if(($value4['mccmnc'] == $value['5']['buckets'][$i]['mccmnc']['buckets'][$j]['key']) && ($value4['smsc'] == $value['5']['buckets'][$i]['mccmnc']['buckets'][$j]['smsc']['buckets'][$l]['key']))
                                         {
                                         $cnt = 1;
                                         echo '<td style="vertical-align:middle;background-color:#cfebed; text-align:right"><a style="color:#333333;cursor:pointer;" onclick="gotoMo('.urlencode($dater2).', `'.$mcc.'`, `'.$value['5']['buckets'][$i]['mccmnc']['buckets'][$j]['smsc']['buckets'][$l]['key'].'`);">'.number_format($value4['id'], 0, '.', '&nbsp;').'</a></td>';
                                         $moCount += $value4['id'];
                                         if($dlrArr681[$value4['smsc']] != 0)
                                         {
                                         echo '<td style="vertical-align:middle;background-color:#cfebed; text-align:right">'.round(($value4['id']/$dlrArr681[$value4['smsc']]*100), 2).' %</td>';
                                         $moPCount += round(($value4['id']/$dlrArr681[$value4['smsc']]*100), 2);
                                         }
                                         else
                                         {
                                         echo '<td style="background-color:#cfebed;">'.''.'</td>';
                                         }
                                         }
                                         }
                                         }
                                         if($cnt == 0)
                                         {
                                         echo '<td style="background-color:#cfebed;">'.''.'</td>';
                                         echo '<td style="background-color:#cfebed;">'.''.'</td>';
                                         } */
                                       echo '</tr>';
                                    }
                                 }
                              }
                           }
                        }
                     }
                     ?>
                  </tbody>
                  <tfoot>
                     <tr>
                        <td>Totals:</td>
                        <td><b><?php echo count($networkCount); ?></b></td>
                        <td><b><?php echo count($smscCount); ?></b></td>
                        <td style = "background-color:#e1e4ef; text-align:right;"><b><?php echo number_format($sentCount, 0, '.', '&nbsp;'); ?></b></td>
                        <td style = "background-color:#e1e4ef; text-align:right;"><b><?php echo number_format($unitsCount, 0, '.', '&nbsp;'); ?></b></td>
                        <td style = "background-color:#f2f1d6; text-align:right;"><b><?php echo number_format($pendingCount, 0, '.', '&nbsp;'); ?></b></td>
                        <td style = "background-color:#f2f1d6; text-align:right;"><b><?php echo round(($pendingCount / $sentCount * 100), 2); ?></b>&nbsp;%</td>
                        <td style = "background-color:#e7eec5; text-align:right;"><b><?php echo number_format($delCount, 0, '.', '&nbsp;'); ?></b></td>
                        <td style = "background-color:#e7eec5; text-align:right;"><b><?php echo round(($delCount / $sentCount * 100), 2); ?></b>&nbsp;%</td>
                        <td style = "background-color:#f5dcdc; text-align:right;"><b><?php echo number_format($failCount, 0, '.', '&nbsp;'); ?></b></td>
                        <td style = "background-color:#f5dcdc; text-align:right;"><b><?php echo round(($failCount / $sentCount * 100), 2); ?>&nbsp;%</b></td>
                        <td style = "background-color:#f2cfcf; text-align:right;"><b><?php echo number_format($rejCount, 0, '.', '&nbsp;'); ?></b></td>
                        <?php
                        if ($sentCount != 0) {
                           echo '<td style = "background-color:#f2cfcf; text-align:right;"><b>' . round(($rejCount / $sentCount * 100), 2) . '&nbsp;%</b></td>';
                        } else {
                           echo '<td style = "background-color:#f2cfcf; text-align:right;"><b>-</b>&nbsp;%</td>';
                        }
                        ?>
                        <td style = "background-color:#dddddd; text-align:right;"><b><?php echo number_format($exCount, 0, '.', '&nbsp;'); ?></b></td>
                        <td style = "background-color:#dddddd; text-align:right;"><b><?php echo round(($exCount / $sentCount * 100), 2); ?>&nbsp;%</b></td>
                        <td style = "background-color:#e1eff0; text-align:right;"><b><?php echo number_format($moCount, 0, '.', '&nbsp;'); ?></b></td>
                        <td style = "background-color:#e1eff0; text-align:right;"><b><?php echo round(($moCount / $sentCount * 100), 2); ?>&nbsp;%</b></td>
                     </tr>
                  </tfoot>
               </table>
            </div>
            <div class="box-body">
               <div class="form-group" style="padding-top:30px">
                  <a onclick="saveRep();" class="btn btn-block btn-social btn-dropbox" style="background-color:<?php echo $accRGB; ?>; border: 2px solid; border-radius: 6px !important; float:left; width: 134px; margin-top:-21px;">
                     <i class="fa fa-save"></i>Save Report
                  </a>
               </div>
            </div>
         </div>
      </div>
   </section>
</aside>

<!--  THE JAVASCRIPT IMPORT ALWAYS APPEARS BEFORE LOCAL JAVASCRIPT AT THE BOTTOM OF THE PAGE -->
<?php include("template_import_script.php"); //must import all scripts first ?>
<!-- END JAVASCRIPT IMPORT -->

<script type="text/javascript">
   $(function ()
   {
      $('#example2').dataTable({
         "bPaginate": false,
         "bLengthChange": false,
         "bFilter": true,
         "bSort": true,
         "bInfo": true,
         "bAutoWidth": false,
         "iDisplayLength": 100,
         "aoColumns": [
            null,
            null,
            null,
            {"sType": 'formatted-num', targets: 0},
            {"sType": 'formatted-num', targets: 0},
            {"sType": 'formatted-num', targets: 0},
            {"sType": 'formatted-num', targets: 0},
            {"sType": 'formatted-num', targets: 0},
            {"sType": 'formatted-num', targets: 0},
            {"sType": 'formatted-num', targets: 0},
            {"sType": 'formatted-num', targets: 0},
            {"sType": 'formatted-num', targets: 0},
            {"sType": 'formatted-num', targets: 0},
            {"sType": 'formatted-num', targets: 0},
            {"sType": 'formatted-num', targets: 0},
            {"sType": 'formatted-num', targets: 0},
            {"sType": 'formatted-num', targets: 0}
         ],
      });
   });

   jQuery.extend(jQuery.fn.dataTableExt.oSort,
           {
              "title-numeric-pre": function (a) {
                 var x = a.match(/title="*(-?[0-9\.]+)/)[1];
                 return parseFloat(x);
              },
              "title-numeric-asc": function (a, b) {
                 return ((a < b) ? -1 : ((a > b) ? 1 : 0));
              },
              "title-numeric-desc": function (a, b) {
                 return ((a < b) ? 1 : ((a > b) ? -1 : 0));
              }
           });

   jQuery.extend(jQuery.fn.dataTableExt.oSort,
           {
              "formatted-num-pre": function (a) {
                 a = (a === "-" || a === "") ? 0 : a.replace(/[^\d\-\.]/g, "");
                 return parseFloat(a);
              },
              "formatted-num-asc": function (a, b) {
                 return a - b;
              },
              "formatted-num-desc": function (a, b) {
                 return b - a;
              }
           });


</script>

<script type="text/javascript">
   $('#reportrange').daterangepicker(
           {
              ranges: {
                 'Today': [moment(), moment()],
                 'Yesterday': [moment().subtract('days', 1), moment().subtract('days', 1)],
                 'Last 7 Days': [moment().subtract('days', 6), moment()],
                 'Last 30 Days': [moment().subtract('days', 29), moment()],
                 'This Month': [moment().startOf('month'), moment().endOf('month')],
                 'Last Month': [moment().subtract('month', 1).startOf('month'), moment().subtract('month', 1).endOf('month')]
              },
              startDate: moment(<?php echo $startDate; ?>),
              endDate: moment(<?php echo $endDate; ?>)
           },
           function (start, end) {
              $(".loader").fadeIn("slow");
              $(".loaderIcon").fadeIn("slow");
              $('#reportrange span').html(start + ' - ' + end);

              var repRange = $("#reportrange span").html();

              window.location = "../pages/networkpercentadmin.php?range=" + repRange;
           }
   );

   function viewMsg(dater, mcc, smsc)
   {
      /*for (var i = 0; i < arguments.length; i++) 
       {
       alert(arguments[i]);
       }*/
      /*alert(mcc);
       alert(smsc);*/
      $(".loader").fadeIn("slow");
      $(".loaderIcon").fadeIn("slow");
      //var serviceName = document.getElementById("client").value;

      var repRange1 = $("#reportrange span").html();
      //alert(repRange1);
      //alert(dater);
      //var start = moment(dater).unix();
      var end = dater + (24 * 60 * 60) - 1;//
      //end = (end.unix()-1)*1000;
      var repRange = dater + ' - ' + end;
      //console.log(repRange);
      //window.location = "../pages/apitrafficmsg.php?client=" + serviceName + "&range=" + repRange;
      window.location = "../pages/networkpercentmsgadmin.php?range=" + repRange + "&range1=" + repRange1 + "&type=Sent&sents=" + <?php echo $sentCount; ?> + "&from=1&controller=1&sort=<?php echo $sort; ?>&mcc=`" + mcc + "`&smsc=" + smsc;
   }

   function viewMsgVar(dater, val, valNum, mcc, smsc, t)
   {
      $(".loader").fadeIn("slow");
      $(".loaderIcon").fadeIn("slow");
      var repRange1 = $("#reportrange span").html();
      var end = dater + (24 * 60 * 60) - 1;//
      var repRange = dater + ' - ' + end;
      window.location = "../pages/networkpercentmsgadmin.php?range=" + repRange + "&range1=" + repRange1 + "&type=" + t + "&sents=" + valNum + "&from=1&controller=1&sort=<?php echo $sort; ?>&query=" + val + "&mcc=`" + mcc + "`&smsc=" + smsc;
   }

   function viewMsgVarRDNC(dater, val, valNum, mcc, smsc, t)
   {
      $(".loader").fadeIn("slow");
      $(".loaderIcon").fadeIn("slow");
      var repRange1 = $("#reportrange span").html();
      var end = dater + (24 * 60 * 60) - 1;//
      var repRange = dater + ' - ' + end;
      window.location = "../pages/networkpercentmsgadmin.php?rdnc=1&range=" + repRange + "&range1=" + repRange1 + "&type=" + t + "&sents=" + valNum + "&from=1&controller=1&sort=<?php echo $sort; ?>&query=" + val + "&mcc=`" + mcc + "`&smsc=" + smsc;
   }

   function gotoMo(dater, mcc, smsc, total)
   {
      $(".loader").fadeIn("slow");
      $(".loaderIcon").fadeIn("slow");

      var repRange1 = $("#reportrange span").html();
      //var end = dater + (24*60*60) - 1;//
      //var repRange = dater + ' - ' + end;
      var prevPage = "networkpercentadmin";

      window.location = "../pages/networkpercentmomsgadmin.php?range=" + dater + "&range1=" + repRange1 + "&mcc=" + mcc + "&smsc=" + smsc + "&prevPage=" + prevPage + "&total=" + total;
   }
</script>

<script type="text/javascript">
   function saveRep()
   {
      var csvContent = "data:text/csv;charset=utf-8,";

      var data = [["Date", "Network", "SMSC", "Sent", "Units", "Pending", "Pending %", "Delivered", "Delivered %", "Failed", "Failed %", "Rejected", "Rejected %", "Excluded", "Excluded %", "MO Total", "MO/Delivered %"],
<?php
if (isset($allStats)) {
   foreach ($allStats as $key => $value) {
      if ($key == 'aggregations') {
         for ($i = 0; $i < count($value['5']['buckets']); $i++) {
            for ($j = 0; $j < count($value['5']['buckets'][$i]['mccmnc']['buckets']); $j++) {
               for ($l = 0; $l < count($value['5']['buckets'][$i]['mccmnc']['buckets'][$j]['smsc']['buckets']); $l++) {
                  $smsc = $value['5']['buckets'][$i]['mccmnc']['buckets'][$j]['smsc']['buckets'][$l]['key'];
                  $posSmsc = strrpos($smsc, '_');
                  $smsc = ucfirst(substr($smsc, 0, $posSmsc));

                  echo '[';
                  $dater = strstr($value['5']['buckets'][$i]['key_as_string'], 'T', true);
                  echo '"' . $dater . '",';

                  $netw = getNetFromMCC($value['5']['buckets'][$i]['mccmnc']['buckets'][$j]['key']);
                  $counter = 0;

                  echo '"' . ucfirst($netw) . ' (' . $value['5']['buckets'][$i]['mccmnc']['buckets'][$j]['key'] . ')",';
                  echo '"' . $smsc . '",';
                  echo '"' . $value['5']['buckets'][$i]['mccmnc']['buckets'][$j]['smsc']['buckets'][$l]['doc_count'] . '",';
                  echo '"' . $value['5']['buckets'][$i]['mccmnc']['buckets'][$j]['smsc']['buckets'][$l]['1']['value'] . '",';

                  $dlrArr680[$value['5']['buckets'][$i]['mccmnc']['buckets'][$j]['smsc']['buckets'][$l]['key']] = 0;
                  $dlrArr681[$value['5']['buckets'][$i]['mccmnc']['buckets'][$j]['smsc']['buckets'][$l]['key']] = 0;
                  $dlrArr682[$value['5']['buckets'][$i]['mccmnc']['buckets'][$j]['smsc']['buckets'][$l]['key']] = 0;
                  $dlrArr4288[$value['5']['buckets'][$i]['mccmnc']['buckets'][$j]['smsc']['buckets'][$l]['key']] = 0;
                  $dlrArrREJ[$value['5']['buckets'][$i]['mccmnc']['buckets'][$j]['smsc']['buckets'][$l]['key']] = 0;

                  for ($k = 0; $k < count($value['5']['buckets'][$i]['mccmnc']['buckets'][$j]['smsc']['buckets'][$l]['dlr']['buckets']); $k++) {
                     $dlrPos = (integer) $value['5']['buckets'][$i]['mccmnc']['buckets'][$j]['smsc']['buckets'][$l]['dlr']['buckets'][$k]['key'];
                     //if(in_array('680', $value['5']['buckets'][$i]['mccmnc']['buckets'][$j]['smsc']['buckets'][$l]['dlr']['buckets'][$k]))
                     if (($dlrPos & 32) == 32 && ($dlrPos & 1) == 0 && ($dlrPos & 2) == 0 && ($dlrPos & 16) == 0) {
                        $countRdnc = count($value['5']['buckets'][$i]['mccmnc']['buckets'][$j]['smsc']['buckets'][$l]['dlr']['buckets'][$k]['rdnc']['buckets']);
                        for ($jj = 0; $jj < $countRdnc; $jj++) {
                           //if($value['5']['buckets'][$i]['mccmnc']['buckets'][$j]['smsc']['buckets'][$l]['dlr']['buckets'][$k]['rdnc']['buckets'][$jj]['key'] == 0) 
                           {
                              if ($dlrArr680[$value['5']['buckets'][$i]['mccmnc']['buckets'][$j]['smsc']['buckets'][$l]['key']] == 0) {
                                 //$dlrArr680['5']['buckets']['0']['dlr']['buckets'] = $value['5']['buckets']['0']['dlr']['buckets'][$i]['rdnc']['buckets'][$j]['1']['value'];
                                 $dlrArr680[$value['5']['buckets'][$i]['mccmnc']['buckets'][$j]['smsc']['buckets'][$l]['key']] = $value['5']['buckets'][$i]['mccmnc']['buckets'][$j]['smsc']['buckets'][$l]['dlr']['buckets'][$k]['rdnc']['buckets'][$jj]['doc_count'];
                              } else {
                                 $dlrArr680[$value['5']['buckets'][$i]['mccmnc']['buckets'][$j]['smsc']['buckets'][$l]['key']] += $value['5']['buckets'][$i]['mccmnc']['buckets'][$j]['smsc']['buckets'][$l]['dlr']['buckets'][$k]['rdnc']['buckets'][$jj]['doc_count'];
                              }
                           }
                        }
                     }
                     if (($dlrPos & 1) == 1) {
                        if ($dlrArr681[$value['5']['buckets'][$i]['mccmnc']['buckets'][$j]['smsc']['buckets'][$l]['key']] == 0) {
                           $dlrArr681[$value['5']['buckets'][$i]['mccmnc']['buckets'][$j]['smsc']['buckets'][$l]['key']] = $value['5']['buckets'][$i]['mccmnc']['buckets'][$j]['smsc']['buckets'][$l]['dlr']['buckets'][$k]['doc_count'];
                        } else {
                           $dlrArr681[$value['5']['buckets'][$i]['mccmnc']['buckets'][$j]['smsc']['buckets'][$l]['key']] += $value['5']['buckets'][$i]['mccmnc']['buckets'][$j]['smsc']['buckets'][$l]['dlr']['buckets'][$k]['doc_count'];
                        }
                     }
                     if (($dlrPos & 2) == 2) {
                        if ($dlrArr682[$value['5']['buckets'][$i]['mccmnc']['buckets'][$j]['smsc']['buckets'][$l]['key']] == 0) {
                           $dlrArr682[$value['5']['buckets'][$i]['mccmnc']['buckets'][$j]['smsc']['buckets'][$l]['key']] = $value['5']['buckets'][$i]['mccmnc']['buckets'][$j]['smsc']['buckets'][$l]['dlr']['buckets'][$k]['doc_count'];
                        } else {
                           $dlrArr682[$value['5']['buckets'][$i]['mccmnc']['buckets'][$j]['smsc']['buckets'][$l]['key']] += $value['5']['buckets'][$i]['mccmnc']['buckets'][$j]['smsc']['buckets'][$l]['dlr']['buckets'][$k]['doc_count'];
                        }
                     }

                     $countRdnc = count($value['5']['buckets'][$i]['mccmnc']['buckets'][$j]['smsc']['buckets'][$l]['dlr']['buckets'][$k]['rdnc']['buckets']);
                     for ($jj = 0; $jj < $countRdnc; $jj++) {
                        if ($value['5']['buckets'][$i]['mccmnc']['buckets'][$j]['smsc']['buckets'][$l]['dlr']['buckets'][$k]['rdnc']['buckets'][$jj]['key'] != 0) {
                           if ($dlrArr4288[$value['5']['buckets'][$i]['mccmnc']['buckets'][$j]['smsc']['buckets'][$l]['key']] == 0) {
                              //$dlrArr4288['5']['buckets']['0']['dlr']['buckets'] = $value['5']['buckets']['0']['dlr']['buckets'][$i]['rdnc']['buckets'][$j]['1']['value'];
                              $dlrArr4288[$value['5']['buckets'][$i]['mccmnc']['buckets'][$j]['smsc']['buckets'][$l]['key']] = $value['5']['buckets'][$i]['mccmnc']['buckets'][$j]['smsc']['buckets'][$l]['dlr']['buckets'][$k]['rdnc']['buckets'][$jj]['doc_count'];
                           } else {
                              $dlrArr4288[$value['5']['buckets'][$i]['mccmnc']['buckets'][$j]['smsc']['buckets'][$l]['key']] += $value['5']['buckets'][$i]['mccmnc']['buckets'][$j]['smsc']['buckets'][$l]['dlr']['buckets'][$k]['rdnc']['buckets'][$jj]['doc_count'];
                           }
                        }
                     }
                     if (($dlrPos & 16) == 16) {
                        if ($dlrArrREJ[$value['5']['buckets'][$i]['mccmnc']['buckets'][$j]['smsc']['buckets'][$l]['key']] == 0) {
                           $dlrArrREJ[$value['5']['buckets'][$i]['mccmnc']['buckets'][$j]['smsc']['buckets'][$l]['key']] = $value['5']['buckets'][$i]['mccmnc']['buckets'][$j]['smsc']['buckets'][$l]['dlr']['buckets'][$k]['doc_count'];
                        } else {
                           $dlrArrREJ[$value['5']['buckets'][$i]['mccmnc']['buckets'][$j]['smsc']['buckets'][$l]['key']] += $value['5']['buckets'][$i]['mccmnc']['buckets'][$j]['smsc']['buckets'][$l]['dlr']['buckets'][$k]['doc_count'];
                        }
                     }
                  }

                  $cnt = 0;
                  foreach ($dlrArr680 as $key2 => $value2) {
                     if ($key2 == $value['5']['buckets'][$i]['mccmnc']['buckets'][$j]['smsc']['buckets'][$l]['key']) {
                        $cnt = 1;
                        echo '"' . $value2 . '",';
                        echo '"' . number_format(($value2 / $value['5']['buckets'][$i]['mccmnc']['buckets'][$j]['smsc']['buckets'][$l]['doc_count'] * 100), 2) . '",';
                     }
                  }
                  if ($cnt == 0) {
                     echo '"' . '0' . '",';
                     echo '"' . '0' . '",';
                  }

                  $cnt = 0;
                  foreach ($dlrArr681 as $key2 => $value2) {
                     if ($key2 == $value['5']['buckets'][$i]['mccmnc']['buckets'][$j]['smsc']['buckets'][$l]['key']) {
                        $cnt = 1;
                        echo '"' . $value2 . '",';
                        echo '"' . number_format(($value2 / $value['5']['buckets'][$i]['mccmnc']['buckets'][$j]['smsc']['buckets'][$l]['doc_count'] * 100), 2) . '",';
                     }
                  }
                  if ($cnt == 0) {
                     echo '"' . '0' . '",';
                     echo '"' . '0' . '",';
                  }

                  $cnt = 0;
                  foreach ($dlrArr682 as $key2 => $value2) {
                     if ($key2 == $value['5']['buckets'][$i]['mccmnc']['buckets'][$j]['smsc']['buckets'][$l]['key']) {
                        $cnt = 1;
                        echo '"' . $value2 . '",';
                        echo '"' . number_format(($value2 / $value['5']['buckets'][$i]['mccmnc']['buckets'][$j]['smsc']['buckets'][$l]['doc_count'] * 100), 2) . '",';
                        $tempFail = $value2;
                     }
                  }
                  if ($cnt == 0) {
                     echo '"' . '0' . '",';
                     echo '"' . '0' . '",';
                  }

                  $cnt = 0;
                  foreach ($dlrArrREJ as $key2 => $value2) {
                     if ($key2 == $value['5']['buckets'][$i]['mccmnc']['buckets'][$j]['smsc']['buckets'][$l]['key']) {
                        $cnt = 1;
                        echo '"' . $value2 . '",';
                        echo '"' . number_format(($value2 / $value['5']['buckets'][$i]['mccmnc']['buckets'][$j]['smsc']['buckets'][$l]['doc_count'] * 100), 2) . '",';
                        //array_shift($dlrArr682);       
                     }
                  }
                  if ($cnt == 0) {
                     echo '"' . '0' . '",';
                     echo '"' . '0' . '",';
                  }

                  $cnt = 0;
                  foreach ($dlrArr4288 as $key2 => $value2) {
                     if ($key2 == $value['5']['buckets'][$i]['mccmnc']['buckets'][$j]['smsc']['buckets'][$l]['key']) {
                        $cnt = 1;
                        echo '"' . $value2 . '",';
                        if ($tempFail == 0) {
                           echo '"0",';
                           //echo '<td style="background-color:#b1b1b1; text-align:right">0.00 %</td>';
                        } else {
                           $tn = number_format(($value2 / $tempFail * 100), 2);
                           echo '"\"' . $tn . '\"",';
                           //echo '<td style="background-color:#b1b1b1; text-align:right">'.number_format(($value2/$tempFail*100), 2).' %</td>';
                        }
                     }
                  }
                  if ($cnt == 0) {
                     echo '"' . '0' . '",';
                     echo '"' . '0' . '",';
                  }

                  foreach ($allStats2 as $key7 => $value7) {
                     if ($key7 = 'aggregations' && isset($value7['2']['buckets'])) {
                        if (count($value7['2']['buckets']) > 0) {
                           for ($y = 0; $y < count($value7['2']['buckets']); $y++) {
                              if ($value7['2']['buckets'][$y]['key_as_string'] == $value['5']['buckets'][$i]['key_as_string']) {
                                 if (count($value7['2']['buckets'][$y]['3']['buckets']) > 0) {
                                    $cnt = 0;
                                    for ($x = 0; $x < count($value7['2']['buckets'][$y]['3']['buckets']); $x++) {
                                       if ($value['5']['buckets'][$i]['mccmnc']['buckets'][$j]['key'] == $value7['2']['buckets'][$y]['3']['buckets'][$x]['key']) {
                                          if (count($value7['2']['buckets'][$y]['3']['buckets'][$x]['4']['buckets']) > 0) {
                                             for ($w = 0; $w < count($value7['2']['buckets'][$y]['3']['buckets'][$x]['4']['buckets']); $w++) {
                                                if ($value7['2']['buckets'][$y]['3']['buckets'][$x]['4']['buckets'][$w]['key'] == $value['5']['buckets'][$i]['mccmnc']['buckets'][$j]['smsc']['buckets'][$l]['key']) {
                                                   //echo '<br>'.$value7['2']['buckets'][$y]['3']['buckets'][$x]['4']['buckets'][$w]['key'].'---'.$value['5']['buckets'][$i]['mccmnc']['buckets'][$j]['smsc']['buckets'][$l]['key'];
                                                   $cnt = 1;
                                                   echo '"' . number_format($value7['2']['buckets'][$y]['3']['buckets'][$x]['4']['buckets'][$w]['doc_count'], 0, '.', ' ') . '",';
                                                   $moTota += $value7['2']['buckets'][$y]['3']['buckets'][$x]['doc_count'];

                                                   if ($dlrArr681[$value['5']['buckets'][$i]['mccmnc']['buckets'][$j]['smsc']['buckets'][$l]['key']] != 0) {
                                                      echo '"' . round($value7['2']['buckets'][$y]['3']['buckets'][$x]['doc_count'] / $dlrArr681[$value['5']['buckets'][$i]['mccmnc']['buckets'][$j]['smsc']['buckets'][$l]['key']] * 100, 2) . '"';
                                                   } else {
                                                      echo '"0"';
                                                   }
                                                }
                                             }
                                          }
                                       }
                                    }
                                    if ($cnt == 0) {
                                       echo '"0",';
                                       echo '"0"';
                                    }
                                 }
                              }
                           }
                        }
                     }
                  }
                  echo '],';
               }
            }
         }
      }
   }
}
?>
      ];
      data.forEach(function (infoArray, index)
      {
         dataString = infoArray.join(",");
         csvContent += index < data.length ? dataString + "\n" : dataString;
      });
      var encodedUri = encodeURI(csvContent);
      var link = document.createElement("a");
      link.setAttribute("href", encodedUri);
      link.setAttribute("download", "<?php echo 'NetworkPercent' . '_' . $startDateConv . '-' . $endDateConv; ?>.csv");
      link.click();
   }
</script>

<script type="text/javascript">
   $(window).load(function () {
      $(".loader").fadeOut("slow");
      $(".loaderIcon").fadeOut("slow");
   })
</script>

<!-- Template Footer -->

