<?php
/**
 * Include the header template which sets up the HTML
 *
 * Don't forget to include template_import_script before any Javascripting
 * Don't forget to include template footer.php at the bottom of the page as well
 */
$headerPageTitle = "Create Service"; //set the page title for the template import
TemplateHelper::initialize();

$accountTypes = ['bulk', 'http', 'smpp', 'bulk custom', 'http custom', 'smpp custom'];
$managers = getConnetManagers();
$accounts = getAccounts();
$pgUrl = strrchr(realpath(__FILE__), '/');
$pgUrl = '/pages' . $pgUrl;
$pgId = getRoleIDFromUrl($pgUrl);
$pgId = substr($pgId, 0, 1);

$roles = $_SESSION['roles'];

$manError = '#aaaaaa';
$typError = '#aaaaaa';
$bilError = '#aaaaaa';
$accError = '#aaaaaa';
$acc2Error = '#aaaaaa';
$snError = '#aaaaaa';
$slError = '#aaaaaa';
$unError = '#aaaaaa';
$passError = '#aaaaaa';
$vodaError = '#aaaaaa';
$mtnError = '#aaaaaa';
$cellError = '#aaaaaa';
$telkError = '#aaaaaa';
$thrOldError = '#aaaaaa';
$thrEmailError = '#aaaaaa';
$thrCellError = '#aaaaaa';

//[account] => Please Select
//[account] => New Account
//[account] => Demo Reseller

clog($_GET);

if (isset($_GET['upd'])) {
   $validationCount = 0;
   $showError = 0;

   if ($_GET['manager'] != 'Select Manager') {
      $manager = $_GET['manager'];
      $validationCount++; // 1
   } else {
      $manError = '#ffaaaa';
      $showError = 1;
   }

   if ($_GET['typeCheck'] != 'Please Select') {
      $accType = $_GET['typeCheck'];
      if ($accType == 'smpp' || $accType == 'smpp custom') {
         $moDRL = 'http://server2:3001/core_sms/interfaces/smpp/smpp_mosms.php?debug=hallodaar&system_id=' . trim($_GET['serviceLogin']);
         $dlrURL = 'http://server2:3001/core_sms/interfaces/smpp/smpp_dlr.php?debug=hallodaar&system_id=' . trim($_GET['serviceLogin']);
      }

      if ($accType == 'http' || $accType == 'http custom') {
         $moDRL = trim($_GET['moDRL']);
         $dlrURL = trim($_GET['dlrURL']);
      }

      if ($accType == 'bulk' || $accType == 'bulk custom') {
         $moDRL = '';
         $dlrURL = '';
      }
      //echo '<br>intie';
      //echo "<br>updateing";
      //echo '<br>mo->'.$moDRL.'<-';
      //echo '<br>type->'.$accType.'<-';
      //echo '<br>mo->'.$moDRL.'<-';
      //echo '<br>dlr->'.$dlrURL.'<-';
      $validationCount++; // 2
   } else {
      $typError = '#ffaaaa';
      $showError = 1;
   }

   if ($_GET['billingType'] != 'Please Select') {
      $bilType = $_GET['billingType'];
      $validationCount++; // 3
   } else {
      $bilError = '#ffaaaa';
      $showError = 1;
   }


   if ($_GET['account'] == 'New Account') {

      //unset($_GET['resell'], $_POST['resell'], $_REQUEST['resell']);

      $_GET['newAccountName'] = trim($_GET['newAccountName']);
      //echo "<br>updateing new Account";
      if ($_GET['newAccountName'] != '') {
         if ($_GET['newAccountName'] != 'Account Name already exists.') {
            if ($_GET['newAccountName'] != 'You did not specify an Account Name.') {
               //echo "<br> 1 new acc = ".$_GET['newAccountName'];
               $accountName = checkAccountName($_GET['newAccountName']);
               if ($accountName == 1) {
                  $accountName = $_GET['newAccountName'];
                  $validationCount++; // 4
                  $validationCount++; // 4
               } else {
                  $accError = '#ffaaaa;';
                  $accountName = 'Account Name already exists.';
                  $showError = 1;
               }
            } else {
               $accError = '#ffaaaa;';
               $accountName = 'You did not specify an Account Name.';
               $showError = 1;
            }
         } else {
            $accError = '#ffaaaa;';
            $accountName = 'You did not specify an Account Name.';
            $showError = 1;
         }
      } else {
         $accError = '#ffaaaa;';
         $accountName = 'You did not specify an Account Name.';
         $showError = 1;
      }
   } elseif ($_GET['account'] == 'Please Select') {
      $acc2Error = '#ffaaaa;';
      $showError = 1;
      //$accountName = 'You did not specify an Account Name.';
   } else {
      //get account ID
      $accountName = $_GET['account'];
      $validationCount++; // 4
   }

   if ($_GET['serviceName'] != '') {
      $_GET['serviceName'] = trim($_GET['serviceName']);
      if ($_GET['serviceName'] != 'Service Name already exists.') {
         if ($_GET['serviceName'] != 'You did not specify a Service Name.') {
            $serviceName = checkServiceName($_GET['serviceName']);
            if ($serviceName == 1) {
               $serviceName = $_GET['serviceName'];
               $validationCount++; // 5
            } else {
               $snError = '#ffaaaa';
               $serviceName = 'Service Name already exists.';
               $showError = 1;
            }
         } else {
            $snError = '#ffaaaa';
            $serviceName = 'You did not specify a Service Name.';
            $showError = 1;
         }
      } else {
         $snError = '#ffaaaa';
         $serviceName = 'You did not specify a Service Name.';
         $showError = 1;
      }
   } else {
      $snError = '#ffaaaa';
      $serviceName = 'You did not specify a Service Name.';
      $showError = 1;
   }

   if ($_GET['serviceLogin'] != '') {
      $_GET['serviceLogin'] = trim($_GET['serviceLogin']);
      if ($_GET['serviceLogin'] != 'Service Login already exists.') {
         if ($_GET['serviceLogin'] != 'You did not specify a Service Login.') {
            $serviceLogin = checkServiceLogin($_GET['serviceLogin']);
            if ($serviceLogin == 1) {
               $serviceLogin = $_GET['serviceLogin'];
               $validationCount++; // 6
            } else {
               $slError = '#ffaaaa';
               $serviceLogin = 'Service Login already exists.';
               $showError = 1;
            }
         } else {
            $slError = '#ffaaaa';
            $serviceLogin = 'You did not specify a Service Login.';
            $showError = 1;
         }
      } else {
         $slError = '#ffaaaa';
         $serviceLogin = 'You did not specify a Service Login.';
         $showError = 1;
      }
   } else {
      $slError = '#ffaaaa';
      $serviceLogin = 'You did not specify a Service Login.';
      $showError = 1;
   }

   if ($_GET['servicePass'] != '') {
      $_GET['servicePass'] = trim($_GET['servicePass']);
      if ($_GET['servicePass'] != 'You did not enter a password.') {
         $servicePass = $_GET['servicePass'];
         $validationCount++; // 7
      } else {
         $passError = '#ffaaaa';
         $servicePass = 'You did not enter a password.';
         $showError = 1;
      }
   } else {
      $passError = '#ffaaaa';
      $servicePass = 'You did not enter a password.';
      $showError = 1;
   }

   if ($_GET['vodaCost'] != '') {
      if ($_GET['vodaCost'] != 'You did not enter a value.') {
         $vodaCost = $_GET['vodaCost'];
         $validationCount++; // 8
      } else {
         $vodaError = '#ffaaaa';
         $vodaCost = 'You did not enter a value.';
         $showError = 1;
      }
   } else {
      $vodaError = '#ffaaaa';
      $vodaCost = 'You did not enter a value.';
      $showError = 1;
   }

   if ($_GET['mtnCost'] != '') {
      if ($_GET['mtnCost'] != 'You did not enter a value.') {
         $mtnCost = $_GET['mtnCost'];
         $validationCount++; // 9
      } else {
         $mtnError = '#ffaaaa';
         $mtnCost = 'You did not enter a value.';
         $showError = 1;
      }
   } else {
      $mtnError = '#ffaaaa';
      $mtnCost = 'You did not enter a value.';
      $showError = 1;
   }

   if ($_GET['cellCost'] != '') {
      if ($_GET['cellCost'] != 'You did not enter a value.') {
         $cellCost = $_GET['cellCost'];
         $validationCount++; // 10
      } else {
         $cellError = '#ffaaaa';
         $cellCost = 'You did not enter a value.';
         $showError = 1;
      }
   } else {
      $cellError = '#ffaaaa';
      $cellCost = 'You did not enter a value.';
      $showError = 1;
   }

   if ($_GET['telkCost'] != '') {
      if ($_GET['telkCost'] != 'You did not enter a value.') {
         $telkCost = $_GET['telkCost'];
         $validationCount++; // 11
      } else {
         $telkError = '#ffaaaa';
         $telkCost = 'You did not enter a value.';
         $showError = 1;
      }
   } else {
      $telkError = '#ffaaaa';
      $telkCost = 'You did not enter a value.';
      $showError = 1;
   }

   if (($_GET['threshold'] == "") && ($_GET['threshEmail'] == "") && ($_GET['threshCell'] == "")) {
      $_GET['threshold'] = trim($_GET['threshold']);
      $_GET['threshEmail'] = trim($_GET['threshEmail']);
      $_GET['threshCell'] = trim($_GET['threshCell']);

      
      $threshold = isset($_GET['threshold']) ? $_GET['threshold'] * 10000 : null;
      $threshEmail = $_GET['threshEmail'];
      $threshCell = $_GET['threshCell'];
      //$thrEmailError = '#ffaaaa';
      //$thrCellError = '#ffaaaa';
   } elseif (($_GET['threshold'] == "") && (($_GET['threshEmail'] != "") || ($_GET['threshCell'] != ""))) {
      $_GET['threshold'] = trim($_GET['threshold']);
      $_GET['threshEmail'] = trim($_GET['threshEmail']);
      $_GET['threshCell'] = trim($_GET['threshCell']);

      $thrOldError = '#ffaaaa';
      $threshold = $_GET['threshold'];
      $threshEmail = $_GET['threshEmail'];
      $threshCell = $_GET['threshCell'];
      $validationCount = $validationCount - 2;
      $showError = 1;
   } else {
      if (($_GET['threshold'] != "") && ($_GET['threshEmail'] == "") && ($_GET['threshCell'] == "")) {
         $_GET['threshold'] = trim($_GET['threshold']);
         $_GET['threshEmail'] = trim($_GET['threshEmail']);
         $_GET['threshCell'] = trim($_GET['threshCell']);

         $threshold = $_GET['threshold'];
         $thrEmailError = '#ffaaaa';
         $threshEmail = "";
         $thrCellError = '#ffaaaa';
         $threshCell = "";
         $validationCount = $validationCount - 2;
         $showError = 1;
      } else {
         $_GET['threshold'] = trim($_GET['threshold']);
         $_GET['threshEmail'] = trim($_GET['threshEmail']);
         $_GET['threshCell'] = trim($_GET['threshCell']);

         $threshold = $_GET['threshold'] * 10000;
         $threshEmail = $_GET['threshEmail'];
         $threshCell = $_GET['threshCell'];
      }
   }

   if ($validationCount == 11) {
      $showError = 0;

      if (isset($_GET['is_a_reseller']))
      {
         //addRolesToUser($uId, 'Reseller');
         $success = createNewService($manager, $accType, $bilType, $accountName, $serviceName, $serviceLogin, $servicePass, $vodaCost, $mtnCost, $cellCost, $telkCost, $threshold, $threshEmail, $threshCell, $moDRL, $dlrURL, 'false', 'false');
      }
      else
      {
         $success = createNewService($manager, $accType, $bilType, $accountName, $serviceName, $serviceLogin, $servicePass, $vodaCost, $mtnCost, $cellCost, $telkCost, $threshold, $threshEmail, $threshCell, $moDRL, $dlrURL, 'false', 'true');
         //addRolesToUser($uId, 'Client');
      }
      //$success = createNewService($manager, $accType, $bilType, $accountName, $serviceName, $serviceLogin, $servicePass, $vodaCost, $mtnCost, $cellCost, $telkCost, $threshold, $threshEmail, $threshCell, $moDRL, $dlrURL);
      //error_log("Success var=", $success, "../../../log/connet/plugin/portal.log");

      if ($success == 1) {
         $nSIDArr = getNewAccountData($serviceName);

         foreach ($nSIDArr as $key => $value) {
            $nSID = $value['service_id'];
            $accID = $value['account_id'];
            $uId = $value['user_id'];
         }

         createDefaultSkin($accID);

         if (isset($_GET['is_a_reseller'])) {
            addRolesToUser($uId, 'Reseller');
         } else {
            addRolesToUser($uId, 'Client');
         }

         echo '<meta http-equiv="refresh" content= "0; URL=newusers.php?sId=' . $nSID . '&aId=' . $accID . '&uId=' . $uId . '&sN=' . $serviceName . '&aN=' . $accountName . '" />';
      }
   } elseif ($validationCount == 12) {
      $showError = 0;

      if (isset($_GET['is_a_reseller']))
      {
         //addRolesToUser($uId, 'Reseller');
         $success = createNewService($manager, $accType, $bilType, $accountName, $serviceName, $serviceLogin, $servicePass, $vodaCost, $mtnCost, $cellCost, $telkCost, $threshold, $threshEmail, $threshCell, $moDRL, $dlrURL, 'true', 'false');
      }
      else
      {
         $success = createNewService($manager, $accType, $bilType, $accountName, $serviceName, $serviceLogin, $servicePass, $vodaCost, $mtnCost, $cellCost, $telkCost, $threshold, $threshEmail, $threshCell, $moDRL, $dlrURL, 'true', 'true');
         //addRolesToUser($uId, 'Client');
      }

      //error_log("Success var=", $success, "../../../../log/connet/plugin/portal.log");

      if ($success == 1) {
         $nSIDArr = getNewAccountData($serviceName);

         foreach ($nSIDArr as $key => $value) {
            $nSID = $value['service_id'];
            $accID = $value['account_id'];
            $uId = $value['user_id'];
         }

         createDefaultSkin($accID);

         if (isset($_GET['is_a_reseller'])) {
            addRolesToUser($uId, 'Reseller');
         } else {
            addRolesToUser($uId, 'Client');
         }

         echo '<meta http-equiv="refresh" content= "0; URL=newusers.php?sId=' . $nSID . '&aId=' . $accID . '&uId=' . $uId . '&sN=' . $serviceName . '&aN=' . $accountName . '" />';
      }
   }
}
?>

<!-- Right side column. Contains the navbar and content of the page -->
<aside class="right-side">
   <!--//////////////////////////-->
   <section class="content-header">
      <h1>
         Create Service
      </h1>
   </section>

   <section class="content">
      <div class="row">
         <form action="newservice.php" class="col-md-12" method="get" id="newService" style="border:none;">
            <div class="box box-connet" style="padding-bottom:1px; border-top-color:<?php echo $accRGB; ?>;">
               <div class="box-body" >
                  <div class="callout callout-info" style="margin-bottom:0px;">
                     <h4>Create Service Instructions / Details</h4>
                     <p>This page will allow you to create NEW SERVICES and NEW ACCOUNTS.</p>
                     <p>To create a NEW ACCOUNT select 'New Account' from the "Choose Account" drop down. Then Enter a name in the 'New Account Name' textbox which will appear only once 'New Account' is selected.</p>
                  </div>
                  <?php
                  if (isset($showError)) {
                     if ($showError == 1) {
                        echo '<div class="callout callout-danger" style="margin-bottom:0px;">';
                        echo '<h4>There are errors in the fields below.</h4>';
                        echo '<p>Please rectify the errors below to succesfully create a new service.</p>';
                        echo '</div>';
                     }
                  }
                  ?>
               </div>
            </div>

            <div class="box box-connet" id="mainBox" style="padding-bottom:1px; border-top-color:<?php echo $accRGB; ?>; height:1068px;" >
               <div class="box-header">
                  <h3 class="box-title">New Service Details:</h3>
               </div>
               <div class="box-body" style="padding:0px;">
                  <div class="sidebar-form-group col-md-12" style="padding-top:10px;">
                     <label>Choose Manager</label>
                     <select class="form-control" id="manager" name="manager" style="border:1px solid <?php echo $manError; ?>" form="newService">
                        <?php
                        echo "<option>Select Manager</option>";
                        foreach ($managers as $key => $value) {
                           if ($manager == ucfirst($value['user_username'])) {
                              echo "<option SELECTED>" . ucfirst($value['user_username']) . "</option>";
                           } else {
                              echo "<option>" . ucfirst($value['user_username']) . "</option>";
                           }
                        }
                        ?>
                     </select>
                  </div>

                  <div class="sidebar-form-group col-md-12" style="padding-top:10px;">
                     <label>Billing Type</label>
                     <select class="form-control" id="billingType" name="billingType" form="newService" style="border:1px solid <?php echo $bilError; ?>" >
                        <option>Please Select</option>
                        <option>Postpaid</option>
                        <option>Prepaid</option>
                        <?php
                        if (isset($bilType)) {
                           echo "<option SELECTED>" . $bilType . "</option>";
                        }
                        ?>
                     </select>
                  </div>

                  <div class="sidebar-form-group col-md-12" style="padding-top:10px;">
                     <label>Choose Account</label>
                     <select class="form-control" id="account" name="account" onchange="checkNewAccount(this.value);" form="newService" style="border:1px solid <?php echo $acc2Error; ?>">
                        <?php
                        echo "<option>Please Select</option>";

                        if ($_GET['account'] == 'New Account') {
                           echo "<option SELECTED>New Account</option>";
                        } else {
                           echo "<option>New Account</option>";
                        }

                        foreach ($accounts as $key => $value) {
                           if ($_GET['account'] == $value['account_name']) {
                              echo "<option SELECTED>" . $value['account_name'] . "</option>";
                           } else {
                              echo "<option>" . $value['account_name'] . "</option>";
                           }
                        }
                        ?>
                     </select>
                  </div>

                  <div class="sidebar-form-group col-md-12" id="resellerCheck" name="resellerCheck" style="padding-top:20px;margin-bottom:-10px; display-niel:none;">
                     <div class="callout callout-warning">
                        <h4>Reseller Warning</h4>
                        <p>You will only be able to make this account a reseller if it is <b>not already</b> a reseller.</p>
                        <div class="form-group" style="margin-bottom:-5px;">
                           <label>
                              <input type="checkbox" name="is_a_reseller" id="is_a_reseller" class="minimal-red"/>
                              This is a reseller
                           </label>
                        </div>
                     </div>
                  </div>

                  <div class="sidebar-form-group col-md-12" id="newAccName" name="newAccName" style="padding-top:10px; display:none;">
                     <label>New Account Name</label>
                     <input type="text" class="form-control" name="newAccountName" style="border:1px solid <?php echo $accError; ?>" placeholder="Full Account Name. Eg. Connet Systems (Pty) Ltd." value="<?php
                     if (isset($accountName)) {
                        echo $accountName;
                     }
                     ?>">
                  </div>

                  <div class="sidebar-form-group col-md-12" style="padding-top:10px;">
                     <label>Account Type</label>
                     <select class="form-control" id="typeCheck" name="typeCheck" form="newService" onchange="onAccountSelect(this.value);" style="border:1px solid <?php echo $typError; ?>" >
                        <option>Please Select</option>
                        <?php
                        for ($i = 0; $i < count($accountTypes); $i++) {
                           if (isset($_GET['typeCheck']) && $_GET['typeCheck'] == $accountTypes[$i]) {
                              echo "<option SELECTED>" . $accountTypes[$i] . "</option>";
                           } else {
                              echo "<option>" . $accountTypes[$i] . "</option>";
                           }
                        }
                        ?>
                     </select>
                  </div>

                  <div class="sidebar-form-group col-md-12" style="padding-top:10px;">
                     <label>Service Name</label>
                     <input type="text" class="form-control" name="serviceName" id="serviceName" onclick="appendToTextboxSN(this.value)" onkeyup="appendToTextboxSN(this.value)" style="border:1px solid <?php echo $snError; ?>" placeholder="Service Display Name. eg: Connet/Bulk" value="<?php
                     if (isset($serviceName)) {
                        echo $serviceName;
                     }
                     ?>">
                  </div>

                  <div class="sidebar-form-group col-md-12" style="padding-top:10px;">
                     <label>API Username</label>
                     <input type="text" class="form-control" name="serviceLogin" id="serviceLogin" onclick="appendToTextboxSL(this.value)" onkeyup="appendToTextboxSL(this.value)" maxlength="15" style="border:1px solid <?php echo $slError; ?>" placeholder="Service Login (internal). eg: bulk.connet" value="<?php
                     if (isset($serviceLogin)) {
                        echo $serviceLogin;
                     }
                     ?>">
                  </div>

                  <!--div class="sidebar-form-group col-md-12" style="padding-top:10px;">
                      <label>User Username</label>
                      <input type="text" class="form-control" name="serviceUser" maxlength="15" style="border:1px solid <?php //echo $unError;   ?>" placeholder="Eg. niel.connet (Max 15 characters)" value="<?php /* if(isset($userName)){echo $userName;} */ ?>">
                  </div-->

                  <div class="sidebar-form-group col-md-12" style="padding-top:10px;">
                     <label>API Password</label>
                     <input type="text" class="form-control" name="servicePass" maxlength="8" style="border:1px solid <?php echo $passError; ?>" placeholder="Max 8 characters" value="<?php
                     if (isset($servicePass)) {
                        echo $servicePass;
                     }
                     ?>">
                  </div>

                  <div class="sidebar-form-group col-md-12" style="padding-top:10px;">
                     <img src="../img/serviceproviders/Vodacom SA.png">
                     <label>Vodacom Route Cost</label>
                     <input type="text" class="form-control" name="vodaCost" onkeypress="return isNumberKey(event);" maxlength="4" style="border:1px solid <?php echo $vodaError; ?>" placeholder="cents x 10,000 eg. 1900" value="<?php
                     if (isset($vodaCost)) {
                        echo $vodaCost;
                     }
                     ?>">
                  </div>

                  <div class="sidebar-form-group col-md-12" style="padding-top:10px;">
                     <img src="../img/serviceproviders/MTN.png">
                     <label>MTN Route Cost</label>
                     <input type="text" class="form-control" name="mtnCost" onkeypress="return isNumberKey(event);" maxlength="4" style="border:1px solid <?php echo $mtnError; ?>" placeholder="cents x 10,000 eg. 1900" value="<?php
                     if (isset($mtnCost)) {
                        echo $mtnCost;
                     }
                     ?>">
                  </div>

                  <div class="sidebar-form-group col-md-12" style="padding-top:10px;">
                     <img src="../img/serviceproviders/Cell C.png">
                     <label>Cell C Route Cost</label>
                     <input type="text" class="form-control" name="cellCost" onkeypress="return isNumberKey(event);" maxlength="4" style="border:1px solid <?php echo $cellError; ?>" placeholder="cents x 10,000 eg. 1900" value="<?php
                     if (isset($cellCost)) {
                        echo $cellCost;
                     }
                     ?>">
                  </div>

                  <div class="sidebar-form-group col-md-12" style="padding-top:10px;">
                     <img src="../img/serviceproviders/Telkom Mobile.png">
                     <label>Telkom Route Cost</label>
                     <input type="text" class="form-control" name="telkCost" onkeypress="return isNumberKey(event);" maxlength="4" style="border:1px solid <?php echo $telkError; ?>" placeholder="cents x 10,000 eg. 1900" value="<?php
                     if (isset($telkCost)) {
                        echo $telkCost;
                     }
                     ?>">
                  </div>

                  <div class="sidebar-form-group col-md-12" style="padding-top:10px;">
                     <label>Notification Threshold / Limit</label>
                     <input type="text" class="form-control" name="threshold" onkeypress="return isNumberKey(event);" style="border:1px solid <?php echo $thrOldError; ?>" placeholder="e.g 1000" value="<?php
                     if (isset($threshold)) {
                        echo $threshold;
                     }
                     ?>">
                  </div>

                  <div class="sidebar-form-group col-md-12" style="padding-top:10px;">
                     <label>Notification Email</label>
                     <input type="text" class="form-control" name="threshEmail" style="border:1px solid <?php echo $thrEmailError; ?>" placeholder="e.g john@smit.com, jane@tims.co.za" value="<?php
                     if (isset($threshEmail)) {
                        echo $threshEmail;
                     }
                     ?>">
                  </div>

                  <div class="sidebar-form-group col-md-12" style="padding-top:10px;">
                     <label>Notification SMS Number</label>
                     <input type="text" class="form-control" name="threshCell" style="border:1px solid <?php echo $thrCellError; ?>" placeholder="e.g 0821234567, 27837654321" value="<?php
                     if (isset($threshCell)) {
                        echo $threshCell;
                     }
                     ?>">
                  </div>

                  <div id="httpDlrUrl" class="sidebar-form-group col-md-12" style="padding-top:10px; display:none;">
                     <label>HTTP DLR URL</label>
                     <input type="text" class="form-control" id="dlrURL" name="dlrURL" onfocus="" style="border:1px solid #aaaaaa;" placeholder="(Leave blank if not needed)" value="<?php
                     if (isset($dlrURL)) {
                        echo $dlrURL;
                     }
                     ?>">
                  </div>

                  <div id="httpMoUrl" class="sidebar-form-group col-md-12" style="padding-top:10px; display:none;">
                     <label>HTTP MOSMS URL</label>
                     <input type="text" class="form-control" id="moDRL" name="moDRL" onfocus="" style="border:1px solid #aaaaaa;" placeholder="(Leave blank if not needed)" value="<?php
                     if (isset($moDRL)) {
                        echo $moDRL;
                     }
                     ?>">
                  </div>

               </div>

               <div class="box-footer" style="height:55px;">
                  <input type="hidden" name="upd" id="upd" value="updating">
                  <a class="btn btn-block btn-social btn-dropbox " onclick="newService.submit();" style="background-color: <?php echo $accRGB; ?>; color: #ffffff; border: 2px solid; border-radius: 6px !important; float:left; width: 180px; margin-top:10px;">
                     <i class="fa fa-save"></i>Create New Service
                  </a>
               </div>
            </div>
         </form>
      </div>
   </section>
</aside>

<!--  THE JAVASCRIPT IMPORT ALWAYS APPEARS BEFORE LOCAL JAVASCRIPT AT THE BOTTOM OF THE PAGE -->
<?php include("template_import_script.php"); //must import all scripts first   ?>
<!-- END JAVASCRIPT IMPORT -->

<script type="text/javascript">

   function showUrls(v)
   {
      var dlrBox = document.getElementById("httpDlrUrl");
      var moBox = document.getElementById("httpMoUrl");

      if (v == 'http' || v == 'http custom')
      {
         dlrBox.style.display = 'block';
         moBox.style.display = 'block';
         var boxH = document.getElementById("mainBox").style.height;
         var boxI = parseInt(boxH.substr(0, boxH.length - 2));
         boxI = boxI + 138;
         boxH = boxI.toString() + 'px';
         document.getElementById("mainBox").style.height = boxH;
      } else
      {
         dlrBox.style.display = 'none';
         moBox.style.display = 'none';
         /*var boxH = document.getElementById("mainBox").style.height;
          var boxI = parseInt(boxH.substr(0, boxH.length-2));
          boxI = boxI - 138;
          boxH = boxI.toString()+'px';
          document.getElementById("mainBox").style.height = boxH;*/
      }
   }

   function setDlr(v)
   {
      //var dlrBox = document.getElementById("dlrURL");
      //dlrBox.value = 'http://server2:3001/core_sms/interfaces/smpp/smpp_dlr.php?debug=hallodaar&system_id=' + v;
   }

   function setMos(v)
   {
      //var mosBox = document.getElementById("moDRL");
      // mosBox.value = 'http://server2:3001/core_sms/interfaces/smpp/smpp_mosms.php?debug=hallodaar&system_id=' + v;
   }

   function onAccountSelect(v)
   {
      showUrls(v);
      var tB = document.getElementById("serviceName");
      tB.value = '';
      document.getElementById("serviceLogin").value = '';

      if (v == 'bulk' || v == 'http' || v == 'smpp')
      {
         appendToTextboxSN(v);
         appendToTextboxSL(v);
      }
   }

   function appendToTextboxSN(v)
   {
      var type = document.getElementById("typeCheck").value;
      if (type != 'Please Select')
      {
         if (type == 'bulk' || type == 'http' || type == 'smpp')
         {
            if (v != null)
            {
               if (v != 'Please Select')
               {
                  var n = v.lastIndexOf("/");
                  var u = v.substr(n, v.length);

                  if (u == ('/' + type))
                  {
                     var tB = document.getElementById("serviceName");
                     tB.value = v;

                     moveCaretToPos(tB, n);
                     // Work around Chrome's little problem
                     window.setTimeout(function ()
                     {
                        moveCaretToPos(tB, n);
                     }, 1);
                  } else if (n == -1)
                  {
                     x = v.indexOf(type);
                     if (x > 0)
                     {
                        //alert(type+"-some-"+x);
                        y = v.substr(0, x);

                        var tB = document.getElementById("serviceName");
                        //         document.getElementById("serviceName").value = '/' + type;
                        tB.value = y + '/' + type;

                        moveCaretToPos(tB, x);
                        // Work around Chrome's little problem
                        window.setTimeout(function ()
                        {
                           moveCaretToPos(tB, x);
                        }, 1);
                     } else
                     {
                        var tB = document.getElementById("serviceName");
                        //document.getElementById("serviceName").value = '/' + type;
                        tB.value = '/' + type;

                        moveCaretToStart(tB);
                        //Work around Chrome's little problem
                        window.setTimeout(function ()
                        {
                           moveCaretToStart(tB);
                        }, 1);
                     }
                  } else
                  {
                     document.getElementById("serviceName").value = v + '/' + type;
                  }
               } else
               {
                  var tB = document.getElementById("serviceName");
                  tB.value = '';
               }
               /*var tB = document.getElementById("serviceName");
                tB.value = '/'+type;
                
                moveCaretToStart(tB);
                // Work around Chrome's little problem
                window.setTimeout(function() 
                {
                moveCaretToStart(tB);
                }, 1);*/
            }
         }
      } else
      {
         var tB = document.getElementById("serviceName");
         tB.value = '';
      }
   }

   function appendToTextboxSL(v)
   {
      var type = document.getElementById("typeCheck").value;
      if (type != 'Please Select')
      {
         if (type == 'bulk' || type == 'http' || type == 'smpp')
         {
            if (v != null)
            {
               if (v != 'Please Select')
               {
                  var n = v.indexOf(".");
                  var u = v.substr(0, n);

                  if (u == type)
                  {
                     document.getElementById("serviceLogin").value = v;
                  } else if (n == -1)
                  {
                     document.getElementById("serviceLogin").value = type + '.';
                  } else
                  {
                     document.getElementById("serviceLogin").value = type + '.' + v;
                  }
               } else
               {
                  document.getElementById("serviceLogin").value = '';
               }
            }
         }
      } else
      {
         document.getElementById("serviceLogin").value = '';
      }
   }

   function moveCaretToStart(el)
   {
      if (typeof el.selectionStart == "number")
      {
         el.selectionStart = el.selectionEnd = 0;
      } else if (typeof el.createTextRange != "undefined")
      {
         el.focus();
         var range = el.createTextRange();
         range.collapse(true);
         range.select();
      }
   }

   function moveCaretToPos(el, n)
   {
      if (typeof el.selectionStart == "number")
      {
         el.selectionStart = el.selectionEnd = 0 + n;
      } else if (typeof el.createTextRange != "undefined")
      {
         el.focus();
         var range = el.createTextRange();
         range.collapse(true);
         range.select();
      }
   }

   function isNumberKey(evt)
   {
      var charCode = (evt.which) ? evt.which : evt.keyCode;
      if (charCode != 46 && charCode > 31
              && (charCode < 48 || charCode > 57))
         return false;

      return true;
   }

   function checkNewAccount(v)
   {
      if (v == 'New Account')
      {
         document.getElementById("newAccName").style.display = 'block';
         //NIEL document.getElementById("resellerCheck").style.display = 'block';
         var boxH = document.getElementById("mainBox").style.height;
         var boxI = parseInt(boxH.substr(0, boxH.length - 2));
         boxI = boxI + 208;
         boxH = boxI.toString() + 'px';
         document.getElementById("mainBox").style.height = boxH;
      }
      else
      {
         document.getElementById("newAccName").style.display = 'none';
         $.ajax(
                 {
                    url: "../php/ajaxHandler.php",
                    data: {accountName: v},
                    type: "POST",
                    success: function (result)
                    {
                       //alert('success='+result+'-');
                       if (result == 0)
                       {
                          //NIEL  document.getElementById("resellerCheck").style.display = 'block';
                       } else
                       {
                          //NIEL document.getElementById("resellerCheck").style.display = 'none';
                       }
                    }
                 });
      }
   }

   function checkReseller()
   {

   }

   $(window).load(function ()
   {
      $(".loader").fadeOut("slow");
      $(".loaderIcon").fadeOut("slow");
      var val = document.getElementById("account").value;
      checkNewAccount(val);
   })
</script>

<!-- Template Footer -->