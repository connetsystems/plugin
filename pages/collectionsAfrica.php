<?php
/**
 * Include the header template which sets up the HTML
 *
 * Don't forget to include template_import_script before any Javascripting
 * Don't forget to include template footer.php at the bottom of the page as well
 */
//set the page title for the template import
if (isset($_REQUEST['id'])) {
   $route_collection_id = $_REQUEST['id'];
   if ($route_collection_id == 96) {
      $headerPageTitle = 'Europe';
   }
   if ($route_collection_id == 83) {
      $headerPageTitle = 'Middle East & Africa';
   }
   if ($route_collection_id == 97) {
      $headerPageTitle = 'Asia';
   }
   if ($route_collection_id == 100) {
      $headerPageTitle = 'Americas';
   }
}

TemplateHelper::initialize();


$startDate = date('Y-m-d');
$endDate = date("Y-m-d H:i:s");


if (isset($_REQUEST['id'])) {
   $route_collection_id = $_REQUEST['id'];
   //Europe 96 // Middle East & Africa Collections 83
   $colData = getCollectionsInternational($route_collection_id);
   //echo $id;
}

function get_currency($from_Currency, $to_Currency, $amount) {

   $amount = urlencode($amount);
   $from_Currency = urlencode($from_Currency);
   $to_Currency = urlencode($to_Currency);

   $url = "http://www.google.com/finance/converter?a=$amount&from=$from_Currency&to=$to_Currency";

   $ch = curl_init();
   $timeout = 0;
   curl_setopt($ch, CURLOPT_URL, $url);
   curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);

   curl_setopt($ch, CURLOPT_USERAGENT, "Mozilla/4.0 (compatible; MSIE 8.0; Windows NT 6.1)");
   curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, $timeout);
   $rawdata = curl_exec($ch);
   curl_close($ch);
   $data = explode('bld>', $rawdata);
   $data = explode($to_Currency, $data[1]);

   return round($data[0], 2);
}

// Call the function to get the currency converted
$exrate = get_currency('EUR', 'ZAR', 1)
?>

<aside class="right-side">
   <section class="content-header">
      <h1>
         <?php
         if (isset($_REQUEST['id'])) {
            //Europe 96 // Middle East & Africa Collections 83
            $route_collection_id = $_REQUEST['id'];
            if ($route_collection_id == 96) {
               echo 'Europe - 1 Euro - ZAR : R ' . $exrate;
            }
            if ($route_collection_id == 83) {
               echo 'Middle East & Africa - 1 Euro - ZAR : R ' . $exrate;
            }
            if ($route_collection_id == 97) {
               echo 'Asia - 1 Euro - ZAR : R ' . $exrate;
            }
            if ($route_collection_id == 100) {
               echo 'Americas - 1 Euro - ZAR : R ' . $exrate;
            }
         }
         ?>


      </h1>
   </section>

   <section class="content">
      <div class="box-body col-md-12" style="margin-top:0px;">
         <div class="panel box box-warning" style="padding-bottom:1px; border-top-color:<?php echo $accRGB; ?>;">
            <div class="box-body">
               <div class="box-body table-responsive no-padding">
                  <table id="routesTable" class="table table-hover table-bordered table-striped">
                     <thead>
                        <tr role="row">
                           <th>ID</th>
                           <th>Country</th>
                           <th>Network</th>
                           <th>Default Route</th>
                           <th colspan="2" align="right">Price</th>
                           <th colspan="2" align="right">Nexmo</th>
                           <th colspan="2" align="right">Silver</th>
                           <th colspan="2" align="right">Clx</th>
                           <th colspan="2" align="right">Portal</th>
                           <th colspan="2" align="right">Cellfind</th>
                           <th colspan="2" align="right">Price R</th>
                           <th>Date</th>
                        </tr>
                     </thead>
                     <tbody>
                        <?php
                        $k = 0;

                        $oldName = '';
                        $countColData = 0;
                        foreach ($colData as $key => $value) {


                           if ($oldName != $value['route_collection_name']) {
                              $oldName = $value['route_collection_name'];
                           }


                           $countColData++;
                        }

                        $tempCol = "";



                        $ks = 0;
                        $countrynew = "";
                        foreach ($colData as $key => $value) {



                           $country = $value['prefix_country_name'];
                           /* if($countrynew == "" )
                             {

                             }
                             else */
                           //if($key%4 == 0 && $key != 0)
                           if ($country != $countrynew) {
                              if ($countrynew != '') {
                                 echo '<tr style="height:20px;">';
                                 echo '<td></td>';
                                 echo '<td></td>';
                                 echo '<td></td>';
                                 echo '<td></td>';
                                 echo '<td colspan="2" align="right"><b>Price</b></td>';
                                 echo '<td colspan="2" align="right"><b>Nexmo</b></td>';
                                 echo '<td colspan="2" align="right"><b>Silver</b></td>';
                                 echo '<td colspan="2" align="right"><b>Clx</b></td>';
                                 echo '<td colspan="2" align="right" colspan="2"><b>Portal</b></td>';
                                 echo '<td colspan="2" align="right" ><b>Cellfind</b></td>';
                                 echo '<td colspan="2" align="right" ><b>Price R</b></td>';
                                 echo '<td align="right"><b>Date</b></td>';
                                 echo '</tr>';
                              }
                              $countrynew = $country;

                              //$ks++;
                           }


                           echo '<tr>';

                           //$emptyRow++;
                           echo '<td style="vertical-align:middle;width:50px;">' . $value['cdr_id'] . '</td>';



                           if (substr($value['prefix_country_name'], 0, 3) == "USA") {
                              $flag = "USA";
                           } else {
                              $flag = $value['prefix_country_name'];
                           }
                           $flag = str_replace('/', '&', $flag);

                           //echo '<td>'.$value['prefix_country_name'].'</td>';
                           echo '<td style="vertical-align:middle;width:200px;"><img src="../img/flags/' . $flag . '.png">&nbsp;&nbsp;' . $value['prefix_country_name'] . '</td>';


                           /* $s1 = strrpos($value['prefix_network_name'], '/');
                             $s2 = strrpos($value['prefix_network_name'], '(');
                             $logo = substr($value['prefix_network_name'], $s1, $s2);
                             $logo = trim($logo);

                             if($logo != "Cell C")
                             {
                             if($logo != 'Vodacom SA')
                             {
                             if($logo != 'MTN-SA')
                             {
                             if($logo != 'Telkom Mobile')
                             {
                             if($logo != 'MTN (Nigeria)')
                             {
                             $logo = substr($value['prefix_network_name'], $s1+1, $s2-$s1-2);
                             $logo = trim($logo);
                             }
                             }
                             }
                             }
                             } */

                           $logo = trim(strstr($value['prefix_network_name'], '(', true));

                           //echo '<td style="vertical-align:middle;width:300px;"><img src="../img/serviceproviders/'.$logo.'.png">&nbsp;&nbsp;'.$value['prefix_network_name'].'</td>';
                           echo '<td style="vertical-align:middle;width:200px;"><img src="../img/serviceproviders/' . $logo . '.png">&nbsp;&nbsp;' . $value['prefix_network_name'] . '</td>';


                           //$routeIcon = strstr($value['default_route'], '_', true);
                           $backgroundcolor = "ffffff";
                           if ($value['default_route'] == "silverstreet_smsc") {
                              //echo '<td style="vertical-align:middle;background-color:#fce0ed;width:200px;">'.$value['default_route'].'</td>';
                              $backgroundcolor = "fce0ed";
                           } elseif ($value['default_route'] == "clx_smsc") {
                              //echo '<td style="vertical-align:middle;background-color:#dde5e7;width:200px;">'.$value['default_route'].'</td>';
                              $backgroundcolor = "dde5e7";
                           } elseif ($value['default_route'] == "nexmo_smsc") {
                              //echo '<td style="vertical-align:middle;background-color:#e3f8ff;width:200px;">'.$value['default_route'].'</td>';
                              $backgroundcolor = "e3f8ff";
                           } elseif ($value['default_route'] == "portal_smsc") {
                              //echo '<td style="vertical-align:middle;background-color:#b9ffc6;width:200px;">'.$value['default_route'].'</td>';
                              $backgroundcolor = "b9ffc6";
                           } elseif ($value['default_route'] == "cellfindint_smsc") {
                              //echo '<td style="vertical-align:middle;background-color:#b9ffc6;width:200px;">'.$value['default_route'].'</td>';
                              $backgroundcolor = "fce483";
                           } else {
                              //echo '<td style="vertical-align:middle;background-color:#ffffff;width:200px;">'.$value['default_route'].'</td>';
                              $backgroundcolor = "ffffff";
                           }

                           echo '<td style="vertical-align:middle;background-color:#' . $backgroundcolor . ';width:200px;">' . $value['default_route'] . '</td>';
                           //}

                           $valueTemp = ' ' . $value['cdr_comments'];

                           if ((strpos($valueTemp, 'eur') != false) || $value['cdr_comments'] == 'eur') {
                              echo '<td style="color:#3333dd;vertical-align:middle; text-align:center;width:5px;background-color:#' . $backgroundcolor . ';"><b><i class="fa fa-eur"></i></b></td>';
                           } elseif ((strpos($valueTemp, 'usd') != false) || $value['cdr_comments'] == 'usd') {
                              echo '<td style="color:#17852c;vertical-align:middle; text-align:center;width:5px;background-color:#' . $backgroundcolor . ';"><b><i class="fa fa-usd"></i></b></td>';
                           } elseif ((strpos($valueTemp, 'gbp') != false) || $value['cdr_comments'] == 'gbp') {
                              echo '<td style="color:#927119;vertical-align:middle; text-align:center;width:5px;background-color:#' . $backgroundcolor . ';"><b><i class="fa fa-gbp"></i></b></td>';
                           } else {
                              echo '<td style="color:#993333;vertical-align:middle; text-align:center;width:5px;background-color:#' . $backgroundcolor . ';"><b>R</b></td>';
                           }

                           if ($value['price_change'] == '1') {
                              echo '<td style="vertical-align:middle;text-align:right;width:30px;background-color:#f5a29f;">' . number_format(($value['cdr_cost_per_unit'] / 10000), 4, '.', '') . '</td>';
                           } else {
                              echo '<td style="vertical-align:middle;text-align:right;width:30px;background-color:#' . $backgroundcolor . ';">' . number_format(($value['cdr_cost_per_unit'] / 10000), 4, '.', '') . '</td>';
                           }



                           //Nexmo currency value
                           if ($value['nexmo_cur'] == 'EUR') {
                              echo '<td style="color:#3333dd;vertical-align:middle; text-align:center;width:5px;background-color:#e3f8ff;"><b><i class="fa fa-eur"></i></b></td>';
                           } elseif ($value['nexmo_cur'] == 'usd') {
                              echo '<td style="color:#17852c;vertical-align:middle; text-align:center;width:5px;background-color:#e3f8ff;"><b><i class="fa fa-usd"></i></b></td>';
                           } elseif ($value['nexmo_cur'] == 'gbp') {
                              echo '<td style="color:#927119;vertical-align:middle; text-align:center;width:5px;background-color:#e3f8ff;"><b><i class="fa fa-gbp"></i></b></td>';
                           } elseif ($value['nexmo_cur'] == 'ZAR') {
                              echo '<td style="color:#993333;vertical-align:middle; text-align:center;width:5px;background-color:#e3f8ff;"><b>R</b></td>';
                           } else {
                              echo '<td style="color:#993333;vertical-align:middle; text-align:center;width:5px;background-color:#e3f8ff;"></td>';
                           }
                           //Nexmo cost value
                           if (number_format(($value['nexmo_price'] / 10000), 4, '.', '') == '0.0000') {
                              echo '<td style="vertical-align:middle;text-align:right;width:30px;color:#ff1111;background-color:#e3f8ff;"></td>';
                           } else {
                              echo '<td style="vertical-align:middle;text-align:right;width:30px;background-color:#e3f8ff;">' . number_format(($value['nexmo_price'] / 10000), 4, '.', '') . '</td>';
                           }

                           //Silverstreet currency value
                           if ($value['silverstreet_cur'] == 'EUR') {
                              echo '<td style="color:#3333dd;vertical-align:middle; text-align:center;width:5px;background-color:#fce0ed;"><b><i class="fa fa-eur"></i></b></td>';
                           } elseif ($value['silverstreet_cur'] == 'usd') {
                              echo '<td style="color:#17852c;vertical-align:middle; text-align:center;width:5px;background-color:#fce0ed;"><b><i class="fa fa-usd"></i></b></td>';
                           } elseif ($value['silverstreet_cur'] == 'gbp') {
                              echo '<td style="color:#927119;vertical-align:middle; text-align:center;width:5px;background-color:#fce0ed;"><b><i class="fa fa-gbp"></i></b></td>';
                           } elseif ($value['silverstreet_cur'] == 'ZAR') {
                              echo '<td style="color:#993333;vertical-align:middle; text-align:center;width:5px;background-color:#fce0ed;"><b>R</b></td>';
                           } else {
                              echo '<td style="color:#993333;vertical-align:middle; text-align:center;width:5px;background-color:#fce0ed;"></td>';
                           }
                           //Silverstreet cost value
                           if (number_format(($value['silverstreet_price'] / 10000), 4, '.', '') == '0.0000') {
                              echo '<td style="vertical-align:middle;text-align:right;width:30px;color:#ff1111;background-color:#fce0ed;"></td>';
                           } else {
                              echo '<td style="vertical-align:middle;text-align:right;width:30px;background-color:#fce0ed;">' . number_format(($value['silverstreet_price'] / 10000), 4, '.', '') . '</td>';
                           }

                           //Clx currency value
                           if ($value['clx_cur'] == 'EUR') {
                              echo '<td style="color:#3333dd;vertical-align:middle; text-align:center;width:5px;background-color:#dde5e7;"><b><i class="fa fa-eur"></i></b></td>';
                           } elseif ($value['clx_cur'] == 'usd') {
                              echo '<td style="color:#17852c;vertical-align:middle; text-align:center;width:5px;background-color:#dde5e7;"><b><i class="fa fa-usd"></i></b></td>';
                           } elseif ($value['clx_cur'] == 'gbp') {
                              echo '<td style="color:#927119;vertical-align:middle; text-align:center;width:5px;background-color:#dde5e7;"><b><i class="fa fa-gbp"></i></b></td>';
                           } elseif ($value['clx_cur'] == 'ZAR') {
                              echo '<td style="color:#993333;vertical-align:middle; text-align:center;width:5px;background-color:#dde5e7;"><b>R</b></td>';
                           } else {
                              echo '<td style="color:#993333;vertical-align:middle; text-align:center;width:5px;background-color:#dde5e7;"></td>';
                           }
                           //Clx cost value
                           if (number_format(($value['clx_price'] / 10000), 4, '.', '') == '0.0000') {
                              echo '<td style="vertical-align:middle;text-align:right;width:30px;color:#ff1111;background-color:#dde5e7;"></td>';
                           } else {
                              echo '<td style="vertical-align:middle;text-align:right;width:30px;background-color:#dde5e7;">' . number_format(($value['clx_price'] / 10000), 4, '.', '') . '</td>';
                           }

                           //Portal Currency value
                           if ($value['portal_cur'] == 'EUR') {
                              echo '<td style="color:#3333dd;vertical-align:middle; text-align:center;width:5px;background-color:#b9ffc6;"><b><i class="fa fa-eur"></i></b></td>';
                           } elseif ($value['portal_cur'] == 'usd') {
                              echo '<td style="color:#17852c;vertical-align:middle; text-align:center;width:5px;background-color:#b9ffc6;"><b><i class="fa fa-usd"></i></b></td>';
                           } elseif ($value['portal_cur'] == 'gbp') {
                              echo '<td style="color:#927119;vertical-align:middle; text-align:center;width:5px;background-color:#b9ffc6;"><b><i class="fa fa-gbp"></i></b></td>';
                           } elseif ($value['portal_cur'] == 'ZAR') {
                              echo '<td style="color:#993333;vertical-align:middle; text-align:center;width:5px;background-color:#b9ffc6;"><b>R</b></td>';
                           } else {
                              echo '<td style="color:#993333;vertical-align:middle; text-align:center;width:5px;background-color:#b9ffc6;"></td>';
                           }
                           //Portal cost rate
                           if (number_format(($value['portal_price'] / 10000), 4, '.', '') == '0.0000') {
                              echo '<td style="vertical-align:middle;text-align:right;width:30px;color:#ff1111;background-color:#b9ffc6;"></td>';
                           } else {
                              echo '<td style="vertical-align:middle;text-align:right;width:30px;background-color:#b9ffc6;">' . number_format(($value['portal_price'] / 10000), 4, '.', '') . '</td>';
                           }

                           //Cellfind Currency value
                           if ($value['cellfind_cur'] == 'EUR') {
                              echo '<td style="color:#3333dd;vertical-align:middle; text-align:center;width:5px;background-color:#fce483;"><b><i class="fa fa-eur"></i></b></td>';
                           } elseif ($value['cellfind_cur'] == 'usd') {
                              echo '<td style="color:#17852c;vertical-align:middle; text-align:center;width:5px;background-color:#fce483;"><b><i class="fa fa-usd"></i></b></td>';
                           } elseif ($value['cellfind_cur'] == 'gbp') {
                              echo '<td style="color:#927119;vertical-align:middle; text-align:center;width:5px;background-color:#fce483;"><b><i class="fa fa-gbp"></i></b></td>';
                           } elseif ($value['cellfind_cur'] == 'ZAR') {
                              echo '<td style="color:#993333;vertical-align:middle; text-align:center;width:5px;background-color:#fce483;"><b>R</b></td>';
                           } else {
                              echo '<td style="color:#993333;vertical-align:middle; text-align:center;width:5px;background-color:#fce483;"></td>';
                           }
                           //Cellfind cost rate
                           if (number_format(($value['cellfind_price'] / 10000), 4, '.', '') == '0.0000') {
                              echo '<td style="vertical-align:middle;text-align:right;width:30px;color:#ff1111;background-color:#fce483;"></td>';
                           } else {
                              echo '<td style="vertical-align:middle;text-align:right;width:30px;background-color:#fce483;">' . number_format(($value['cellfind_price'] / 10000), 4, '.', '') . '</td>';
                           }



                           //echo '<td style="vertical-align:middle;text-align:right;width:30px;color:#ff1111;background-color:#b9ffc6;">N/A</td>';


                           echo '<td style="color:#993333;vertical-align:middle; text-align:center;width:5px;"><b>R</b></td>';
                           if ((strpos($valueTemp, 'eur') != false) || $value['cdr_comments'] == 'eur') {
                              echo '<td style="vertical-align:middle;text-align:right;width:30px;">' . number_format(($value['cdr_cost_per_unit'] / 10000) * $exrate, 4, '.', '') . '</td>';
                           } else {
                              echo '<td style="vertical-align:middle;text-align:right;width:30px;">' . number_format(($value['cdr_cost_per_unit'] / 10000), 4, '.', '') . '</td>';
                           }

                           echo '<td style="vertical-align:middle;text-align:right;width:80px;">' . $value['cdr_effective_date'] . '</td>';




                           echo '</tr>';
                        }
                        ?>
                     </tbody>
                  </table>
               </div><!-- /.box-body -->
            </div>
         </div>
      </div>
   </section>
</aside>

<!--  THE JAVASCRIPT IMPORT ALWAYS APPEARS BEFORE LOCAL JAVASCRIPT AT THE BOTTOM OF THE PAGE -->
<?php include("template_import_script.php"); //must import all scripts first ?>
<!-- END JAVASCRIPT IMPORT -->

<script type="text/javascript">
   $(window).load(function () {
      $(".loader").fadeOut("slow");
      $(".loaderIcon").fadeOut("slow");
   })
</script>

<!-- Template Footer -->

