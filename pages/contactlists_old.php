<?php
/**
 * Include the header template which sets up the HTML
 *
 * Don't forget to include template_import_script before any Javascripting
 * Don't forget to include template footer.php at the bottom of the page as well
 */
$headerPageTitle = "Contacts"; //set the page title for the template import
TemplateHelper::initialize();

$singleView = 'block;';

if ($_SESSION['serviceId'] != '' && $_SESSION['resellerId'] != '') {
   if ($_SESSION['serviceId'] != $_SESSION['resellerId']) {
      //if(!isset($_GET['client']))
      {
         $_GET['client'] = $_SESSION['accountName'] . ' - ' . $_SESSION['serviceName'] . " - " . $_SESSION['serviceId'];
         //echo "<br><br><br><br>+---------------------->".$_GET['client'];
         $singleView = 'none;';
         // $noShow = 1;
      }
   } elseif ($_SESSION['serviceId'] == $_SESSION['resellerId']) {
      $singleView = 'block;';
      //$noShow = 1;
   }
   /* elseif($_SESSION['resellerId'] == '')
     {

     $noShow = 1;
     } */
}

////////////////////////////////////////////////
////////////////////////////////////////////////
////////////////////////////////////////////////
/* echo '<pre>';
  print_r($_POST);
  echo '</pre>'; */

if (isset($_POST['separ']) && $_POST['separ'] != '') {
   $_GET['cId'] = $_POST['cId'];
   $cId = $_GET['cId'];
   $_GET['client'] = $_POST['client'];
   $sId = $_POST['sId'];
   $dupped = 0;
   $totSize = 0;
   $errored = 0;
   $inserted = 0;
   $successUpload = 0;
   if ($_POST['separ'] == 'semi') {
      $delimiter = ';';
   } else {
      $delimiter = ',';
   }

   $target_dir = "../tempFiles/";
   $target_file = $target_dir . basename($_FILES["uploadCSV"]["name"]);
   //echo '<br>fileTarget='.$target_file.'<br>';
   $uploadOk = 1;
   $imageFileType = pathinfo($target_file, PATHINFO_EXTENSION);
   // Check if image file is a actual image or fake image
   if (isset($_POST["separ"])) {
      if (move_uploaded_file($_FILES["uploadCSV"]["tmp_name"], $target_file)) {
         // echo "The file ". basename( $_FILES["uploadCSV"]["name"]). " has been uploaded.";
      } else {
         //echo "Sorry, there was an error uploading your file.";
      }
   }
   $handle = fopen($target_file, "r");
   if ($handle) {
      while (($line = fgets($handle)) !== false) {
         //echo '<br>Line='.$line;
         //echo '<br>Delimiter='.$delimiter;
         $contactArr = explode($delimiter, $line);
         //echo '<br>Count Arr='.count($contactArr);
         if (count($contactArr) == 3) {
            $contactArr[0] = trim($contactArr[0]);
            $contactArr[0] = trim($contactArr[0], '"');
            $contactArr[1] = trim($contactArr[1]);
            $contactArr[1] = trim($contactArr[1], '"');
            $contactArr[2] = trim($contactArr[2]);
            $contactArr[2] = trim($contactArr[2], '"');

            $checked = checkContactExists($contactArr[0], $contactArr[1], $contactArr[2], $sId, $cId);
            if ($checked == 0) {
               $linkID = addNewContact($sId, $contactArr[1], $contactArr[2], $contactArr[0]);
               if (isset($linkID) && $linkID != '') {
                  $link = addNewContactLink($cId, $linkID);
                  //echo '<br>inserResult = '.$link;
                  $inserted++;
               } else {
                  $errored++;
               }
            } else {
               //echo '<br>dropped='.$dupped;
               $dupped++;
            }
         }
         $totSize++;
      }
      fclose($handle);
   }

   if ($dupped != 0 || $inserted != 0) {
      $successUpload = 1;
   }
   if ($dupped != 0 && $inserted == 0) {
      $successUpload = 2;
   }
}

////////////////////////////////////////////////
////////////////////////////////////////////////
////////////////////////////////////////////////

$success = 0;
$success2 = 0;
$success3 = 0;
$success4 = 0;
$success5 = 0;
$success6 = 0;
$showContacts = "none;";

if (isset($_GET['saveEditContactList']) && $_GET['saveEditContactList'] == 1) {
   $_GET['name'] = trim($_GET['name']);
   $_GET['desc'] = trim($_GET['desc']);

   $success4 = checkNewContactListClient($_GET['sId'], $_GET['name'], $_GET['desc']);
   //echo '<br><br><br>--+- '.$success4;
   if ($success4 == 0) {
      $success4 = updateContactListClient($_GET['ccId'], $_GET['name'], $_GET['desc']);
   }
}

if (isset($_GET['saveEditContact']) && $_GET['saveEditContact'] == 1) {
   $_GET['name'] = trim($_GET['name']);
   $_GET['last'] = trim($_GET['last']);
   $_GET['cont'] = trim($_GET['cont']);

   //$success5 = checkNewCampaignCamp($_GET['cId'], $_GET['name']);        
   /* if($success5 == 0)
     {
     } */
   $success5 = updateContact($_GET['caId'], $_GET['name'], $_GET['last'], $_GET['cont']);
}

if (isset($_GET['newContactList']) && $_GET['newContactList'] == 1) {
   $_GET['nName'] = trim($_GET['nName']);
   $_GET['nDesc'] = trim($_GET['nDesc']);
   $_GET['nCode'] = trim($_GET['nCode']);

   $success = addNewContactList($_GET['sId'], $_GET['nName'], $_GET['nDesc'], $_GET['stat'], $_GET['nCode']);
   /* $success = checkNewCampaignClient($_GET['sId'], $_GET['nName']);
     if($success == 0)
     {
     } */
   //echo 'SUCCESS=>'.$success.'<=';
}

if (isset($_GET['newContact']) && $_GET['newContact'] == 1) {
   $_GET['nCampName'] = trim($_GET['nCampName']);
   $_GET['nCampLast'] = trim($_GET['nCampLast']);
   $_GET['nCampNumb'] = trim($_GET['nCampNumb']);

   $success2 = checkNewContact($_GET['sId'], $_GET['nCampNumb']);

   if ($success2 == 0) {
      $success2 = addNewContact($_GET['sId'], $_GET['nCampName'], $_GET['nCampLast'], $_GET['nCampNumb']);
      if ($success2 > 0) {
         //$newId
         $success2 = addNewContactLink($_GET['cId'], $success2);
      }
   } else {
      //    echo '<br><br><br><br>---'.$success2;
      $linkCheck = checkContactLinkExist($success2, $_GET['cId']);
      //echo '<br><br><br><br>---'.$linkCheck;
      if ($linkCheck == 0) {
         $success2 = addNewContact($_GET['sId'], $_GET['nCampName'], $_GET['nCampLast'], $_GET['nCampNumb']);
         if ($success2 > 0) {
            $success2 = addNewContactLink($_GET['cId'], $success2);
         }
      } else {
         $success2 = -1;
      }
   }
}

if (isset($_GET['deleteContact']) && $_GET['deleteContact'] == 1) {
   deleteContact($_GET['delId'], $_GET['cId']);
}

if (isset($_GET['deleteContactList']) && $_GET['deleteContactList'] == 1) {
   deleteContactList($_GET['delId']);
}

if (isset($_SESSION['accountId'])) {
   $acId = $_SESSION['accountId'];
} else {
   $acId = null;
}
$clients = getServicesInAccount($acId);
//$clients = getServicesInAccount($acId, $_SESSION['serviceId']);

if (count($clients) == 1) {
   $_GET['client'] = $_SESSION['accountName'] . ' - ' . $_SESSION['serviceName'] . " - " . $_SESSION['serviceId'];
   //$_GET['client'] = $_SESSION['accountName'].' - '.$_SESSION['serviceName']." - ".$_SESSION['serviceId']."&tog1=in";
   $singleView = 'none;';
   $noShow = 1;
}

if (isset($_GET['client'])) {
   $cl = $_GET['client'];
   $clp = strrpos($cl, ' ');
   $cl = trim(substr($cl, $clp, strlen($cl)));

   $CLData = getContactListClient($cl);

   foreach ($CLData as $key => $value) {

      $sN = $value['service_id'];
   }
   $dsp = 'block;';
   $dir = '../img/routes';
   //$serProviders = scandir($dir);
} else {
   $_GET['client'] = "0 - Please select a client...";
   $cl = strstr($_GET['client'], ' ', true);
   $dsp = 'none;';
}

if (isset($_GET['cId'])) {
   $CAData = getContactInfo($_GET['cId']);
   $showContacts = "block;";

   /* foreach ($CAData as $key => $value) 
     {
     echo '<pre>';
     print_r($value);
     echo '</pre>';
     } */
}



////////////////////////////////////////////////
////////////////////////////////////////////////
////////////////////////////////////////////////
?>

<aside class="right-side">
   <section class="content-header">
      <h1>
         Contacts
         <!--small>Control panel</small-->
      </h1>
   </section>

   <section class="content">
      <div class="row col-lg-12">
         <div class="box box-solid" style="height:66px; margin-top:-15px;display:<?php echo $singleView; ?>">
            <form action="updateservice.php" method="get" id="clientList" class="sidebar-form" style="border:0px;">
               <div class="form-group">
                  <label>Service List</label>
                  <select class="form-control" name="client" form="clientList" OnChange="reloadOnSelect(this.value);">
                     <?php
                     if ($cl == '0') {
                        echo '<option SELECTED>Please select a client...</option>';
                     }
                     foreach ($clients as $key => $value) {
                        $sId = $value['service_id'];
                        if ($cl == $sId) {
                           $accountName = $value['account_name'];
                           $serviceName = $value['service_name'];
                           echo "<option name='" . $sId . "' SELECTED>" . $accountName . " - " . $serviceName . " - " . $sId . "</option>";
                        } else {
                           echo "<option name='" . $sId . "'>" . $value['account_name'] . " - " . $value['service_name'] . " - " . $sId . "</option>";
                        }
                     }
                     ?>
                  </select>
               </div>
               <br>
            </form>
         </div>
         <div class="box box-warning" style="border-top-color:<?php echo $accRGB; ?>;display:<?php echo $dsp; ?>">
            <div class="box-header">
               <h4 class="box-title">
                  Groups
               </h4>
            </div>
            <div class="box-body">
               <?php
               if ($success === -1) {
                  echo '<div class="callout callout-danger">';
                  echo '<h4>Group Name Exists!</h4>';
                  echo '<p>The group name that you specified already exists.</p>';
                  echo '</div>';
               }

               if ($success == '1') {
                  echo '<div class="callout callout-success">';
                  echo '<h4>Group Added!</h4>';
                  //echo '<p>Succesfully added a new group.</p>';
                  echo '</div>';
               }

               if ($success4 == '1') {
                  echo '<div class="callout callout-success">';
                  echo '<h4>Group Edited!</h4>';
                  //echo '<p>Succesfully edited a group.</p>';
                  echo '</div>';
               }
               ?>
               <a class="btn btn-block btn-social btn-dropbox" name="'.$value['campaign_id'].'" onclick="newT('newContactList');" style="background-color:<?php echo $accRGB; ?>; color: #ffffff; border: 2px solid; border-radius: 6px !important; font-size:0.8em !important; height:24px; width: 162px; margin-top:0px; padding-top:3px;">
                  <i class="fa fa-plus" style="font-size:1.0em !important; color:#ffffff; margin-top:-6px;"></i>Add New Group
               </a>
               <div class="box-body table-responsive" style="display:none;" id="newContactList">
                  <table class="table table-bordered table-striped dataTable table-hover">
                     <thead>
                        <tr>
                           <!--th>Service</th-->
                           <th>Group Code</th>
                           <th>Group Name</th>
                           <th>Groups Description</th>
                           <th><center>Save</center></th>
                     </tr>
                     </thead>
                     <tbody>
                        <tr>
                            <!--td><?php echo $sN; ?></td-->
                           <td><input type="text" style="height:24px;width:100px;" class="form-control" id="newClientCode" placeholder="Code"></td>
                           <td><input type="text" style="height:24px;width:200px;" class="form-control" id="newClientName" placeholder="Name"></td>
                           <td><input type="text" style="height:24px;width:350px;" class="form-control" id="newClientDesc" placeholder="Description"></td>
                           <td>
                     <center>
                        <a class="btn btn-block btn-social btn-dropbox" name="saveNewClient" onclick="saveNewContactList()" style="background-color:<?php echo $accRGB; ?>; color: #ffffff; border: 2px solid; border-radius: 6px !important; font-size:0.8em !important; height:24px; width: 85px; margin-top:0px; padding-top:3px;">
                           <i class="fa fa-save" style="font-size:1.0em !important; color:#ffffff; margin-top:-6px;"></i>Save
                        </a>
                     </center>
                     </td>
                     </tr>
                     </tbody>
                  </table>
               </div>
               <div class="box-body table-responsive">
                  <table id="example2" class="table table-bordered table-striped dataTable table-hover">
                     <thead>
                        <tr>
                           <th>Group Code</th>
                           <th>Group Name</th>
                           <th>Group Description</th>

                           <th><center>View/Update</center></th>
                     <th><center>Edit</center></th>
                     <th><center>Delete</center></th>
                     </tr>
                     </thead>
                     <tbody>

                        <?php
                        if (isset($CLData) && $CLData != '') {
                           foreach ($CLData as $value) {
                              /* if(isset($_GET['cId']))
                                {
                                if($_GET['cId'] == $value['client_id'])
                                {
                                $colorCC = ' style="background-color:#888888;"';
                                }
                                else
                                {
                                $colorCC = '';
                                }
                                } */
                              //echo '<tr>';
                              echo '<tr id="cctr' . $value['contact_list_id'] . '">';
                              echo '<td>' . $value['contact_list_code'] . '</td>';
                              echo '<td><input type="text" id="clNa' . $value['contact_list_id'] . '" style="border:0px;background-color:transparent;width:200px;" readonly="readonly" value="' . $value['contact_list_name'] . '"></td>';
                              echo '<td><input type="text" id="clDe' . $value['contact_list_id'] . '" style="border:0px;background-color:transparent;width:350px;" readonly="readonly" value="' . $value['contact_list_description'] . '"></td>';
                              echo '<td>
                                                        <center>
                                                            <a class="btn btn-block btn-social btn-dropbox" name="' . $value['contact_list_id'] . '" onclick="showContacts(' . $value['contact_list_id'] . ')" style="background-color:' . $accRGB . '; color: #ffffff; border: 2px solid; border-radius: 6px !important; font-size:0.8em !important; height:24px; width: 121px; margin-top:0px; padding-top:3px;">
                                                                <i class="fa fa-binoculars" style="font-size:1.0em !important; color:#ffffff; margin-top:-6px;"></i>View/Update
                                                            </a>
                                                        </center>
                                                      </td>';
                              echo '<td>
                                                        <center>
                                                            <a class="btn btn-block btn-social btn-dropbox" id="edit' . $value['contact_list_id'] . '" onclick="editContactList(' . $value['contact_list_id'] . ')" style="background-color:' . $accRGB . '; color: #ffffff; border: 2px solid; border-radius: 6px !important; font-size:0.8em !important; height:24px; width: 80px; margin-top:0px; padding-top:3px;">
                                                                <i class="fa fa-pencil" style="font-size:1.0em !important;color:#ffffff; margin-top:-6px;"></i>Edit
                                                            </a>
                                                            <a class="btn btn-block btn-social btn-dropbox" id="save' . $value['contact_list_id'] . '" onclick="saveEditContactList(' . $value['contact_list_id'] . ')" style="display:none; background-color:' . $accRGB . '; color: #ffffff; border: 2px solid; border-radius: 6px !important; font-size:0.8em !important; height:24px; width: 80px; margin-top:0px; padding-top:3px;">
                                                                <i class="fa fa-save" style="font-size:1.0em !important;color:#ffffff; margin-top:-6px;"></i>Save
                                                            </a>
                                                        </center>
                                                      </td>';
                              echo '<td>
                                                        <center>
                                                            <a class="btn btn-block btn-social btn-dropbox" name="' . $value['contact_list_id'] . '" onclick="deleteContactList(' . $value['contact_list_id'] . ', `' . $value['contact_list_name'] . '`, `' . $value['contact_list_description'] . '`)" style="background-color:' . $accRGB . '; color: #ffffff; border: 2px solid; border-radius: 6px !important; font-size:0.8em !important; height:24px; width: 92px; margin-top:0px; padding-top:3px;">
                                                                <i class="fa fa-trash" style="font-size:1.0em !important; color:#ff3333; margin-top:-6px;"></i>Delete
                                                            </a>
                                                        </center>
                                                      </td>';
                              echo '</tr>';
                           }
                        }
                        ?>
                     </tbody>
                  </table>
               </div>
            </div>
         </div>
         <div class="box box-warning" style="border-top-color:<?php echo $accRGB; ?>;display:<?php echo $showContacts; ?>">
            <div class="box-header">
               <h4 class="box-title">
                  Contacts
               </h4>
            </div>
            <div class="box-body">
               <?php
               if ($success2 === -1) {
                  echo '<div class="callout callout-danger">';
                  echo '<h4 style="padding-top:10px;">Contact Exists!</h4>';
                  // echo '<p>The cell number already exists.</p>';
                  echo '</div>';
               }
               if ($success2 == '1') {
                  echo '<div class="callout callout-success">';
                  echo '<h4 style="padding-top:10px;">Contact Added!</h4>';
                  //echo '<p>Succesfully added a new contact.</p>';
                  echo '</div>';
               }
               if ($success5 == '1') {
                  echo '<div class="callout callout-success">';
                  echo '<h4 style="padding-top:10px;">Contact Edited!</h4>';
                  // echo '<p>Succesfully edited a contact.</p>';
                  echo '</div>';
               }
               if (isset($successUpload) && $successUpload == 1) {
                  echo '<div class="callout callout-success">';
                  echo '<h4>List uploaded!</h4>';
                  echo '<p>Added ' . $inserted . ' new contact(s).</p>';
                  if ($dupped > 0) {
                     echo '<p>' . $dupped . ' contact(s) were ingored because they already exist.</p>';
                  }
                  echo '</div>';
               }
               if (isset($successUpload) && $successUpload == 2) {
                  echo '<div class="callout callout-warning">';
                  echo '<h4>List uploaded!</h4>';
                  echo '<p>Added ' . $inserted . ' new contact(s).</p>';
                  if ($dupped > 0) {
                     echo '<p>' . $dupped . ' contact(s) were ingored because they already exist.</p>';
                  }
                  echo '</div>';
               }
               if (isset($errored) && $errored != 0) {
                  echo '<div class="callout callout-danger" style="margin-top:-20px;">';
                  echo '<h4>Upload ERROR!</h4>';
                  echo '<p>' . $errored . ' invalid contact(s) please check your CSV formating. eg. "27821234567,John,Smith"</p>';
                  echo '</div>';
               }
               ?>
               <a class="btn btn-block btn-social btn-dropbox" name="'.$value['contact_id'].'" onclick="newT('newContact');" style="display:inline-block;background-color:<?php echo $accRGB; ?>; color: #ffffff; border: 2px solid; border-radius: 6px !important; font-size:0.8em !important; height:24px; width: 155px; margin-top:0px; padding-top:3px;">
                  <i class="fa fa-plus" style="font-size:1.0em !important; color:#ffffff; margin-top:-6px;"></i>Add Single Contact
               </a>
               <a class="btn btn-block btn-social btn-dropbox" name="'.$value['contact_id'].'" onclick="newT('newUpload');" style="display:inline-block;background-color:<?php echo $accRGB; ?>; color: #ffffff; border: 2px solid; border-radius: 6px !important; font-size:0.8em !important; height:24px; width: 170px; margin-top:0px; padding-top:3px;">
                  <i class="fa fa-plus" style="font-size:1.0em !important; color:#ffffff; margin-top:-6px;"></i>Upload Bulk Contacts
               </a>
               <!------------------------------------------------------------------------------------------>
               <!------------------------------------------------------------------------------------------>
               <form id="uploadContacts" action="contactlists.php" method="post" enctype="multipart/form-data">
                  <div class="box-body table-responsive" style="display:none;" id="newUpload">

                     <div class="callout callout-warning" style="margin-top:10px;">
                        <h4>Note!</h4>
                        <p>Please use this CSV format.</p>
                        <p>Cell Number,Name,Surname</p>
                        <p>27821234567,John,Smith</p>
                     </div>
                     <input type="file" form="uploadContacts" name="uploadCSV" id="uploadCSV">
                     <div class="box-body" style="margin-bottom:0px;">
                        <div class="form-group" style="margin-bottom:0px;">
                           <label>Delimiter (This is the separator that delimits the data in your csv file.)</label>
                           <select class="form-control" name="separ" id="separ" form="uploadContacts">
                              <option value="comma">, (comma delimited)</option>
                              <option value="semi">; (semi-colon delimited)</option>
                           </select>
                        </div>
                     </div>
                     <input type="hidden" name="cId" id="cId" value="<?php
                     if (isset($_GET['cId'])) {
                        echo $_GET['cId'];
                     };
                     ?>">
                     <input type="hidden" name="client" id="client" value="<?php
                     if (isset($_GET['client'])) {
                        echo $_GET['client'];
                     };
                     ?>">
                     <input type="hidden" name="sId" id="sId" value="<?php
                            if (isset($cl)) {
                               echo $cl;
                            };
                     ?>">

                     <a class="btn btn-block btn-social btn-dropbox" onclick="uploadContacts.submit();" style="background-color:<?php echo $accRGB; ?>; color: #ffffff; border: 2px solid; border-radius: 6px !important; font-size:0.8em !important; height:24px; width: 100px; margin-top:0px; padding-top:3px;">
                        <i class="fa fa-save" style="font-size:1.0em !important; color:#ffffff; margin-top:-6px;"></i>Upload
                     </a>
                  </div>
               </form>
               <!------------------------------------------------------------------------------------------>
               <!------------------------------------------------------------------------------------------>
               <div class="box-body table-responsive" style="display:none;" id="newContact">
                  <table class="table table-bordered table-striped dataTable table-hover">
                     <thead>
                        <tr>
                           <th>Number</th>
                           <th>Firstname</th>
                           <th>Surname</th>
                           <th><center>Save</center></th>
                     </tr>
                     </thead>
                     <tbody>
                        <tr>
                           <td><input type="text" style="height:24px;" class="form-control" id="newCampNumb" placeholder="Number"></td>
                           <td><input type="text" style="height:24px;" class="form-control" id="newCampName" placeholder="Firstname"></td>
                           <td><input type="text" style="height:24px;" class="form-control" id="newCampLast" placeholder="Surname"></td>
                           <td>
                     <center>
                        <a class="btn btn-block btn-social btn-dropbox" onclick="saveNewContact(<?php
                            if (isset($_GET['cId'])) {
                               echo $_GET['cId'];
                            }
                     ?>)" style="background-color:<?php echo $accRGB; ?>; color: #ffffff; border: 2px solid; border-radius: 6px !important; font-size:0.8em !important; height:24px; width: 85px; margin-top:0px; padding-top:3px;">
                           <i class="fa fa-save" style="font-size:1.0em !important; color:#ffffff; margin-top:-6px;"></i>Save
                        </a>
                     </center>
                     </td>
                     </tr>
                     </tbody>
                  </table>
               </div>
               <div class="box-body table-responsive" id="camps" style="display:<?php echo $showContacts; ?>">
                  <table id="example2" class="table table-bordered table-striped dataTable table-hover">
                     <thead>
                        <tr>
                           <th>Number</th>
                           <th>Firstname</th>
                           <th>Surname</th>
                           <th><center>Edit</center></th>
                     <th><center>Delete</center></th>
                     </tr>
                     </thead>
                     <tbody>
                        <?php
                        if (isset($CAData) && isset($CAData) != '') {
                           foreach ($CAData as $key => $value) {
                              echo '<tr id="tr' . $value['contact_id'] . '">';
                              echo '<td><input type="text" id="caDe' . $value['contact_id'] . '" style="border:0px;background-color:transparent;" readonly="readonly" value="' . $value['contact_msisdn'] . '"></td>';
                              echo '<td><input type="text" id="caNa' . $value['contact_id'] . '" style="width:350px;border:0px;background-color:transparent;" readonly="readonly" value="' . $value['contact_firstname'] . '"></td>';
                              echo '<td><input type="text" id="caCo' . $value['contact_id'] . '" style="width:350px;border:0px;background-color:transparent;" readonly="readonly" value="' . $value['contact_lastname'] . '"></td>';
                              echo '<td>
                                                            <center>
                                                                <a class="btn btn-block btn-social btn-dropbox" id="editC' . $value['contact_id'] . '" onclick="editContact(' . $value['contact_id'] . ')" style="display:block; background-color:' . $accRGB . '; color: #ffffff; border: 2px solid; border-radius: 6px !important; font-size:0.8em !important; height:24px; width: 85px; margin-top:0px; padding-top:3px;">
                                                                    <i class="fa fa-pencil" style="font-size:1.0em !important;color:#ffffff; margin-top:-6px;"></i>Edit
                                                                </a>
                                                                <a class="btn btn-block btn-social btn-dropbox" id="saveC' . $value['contact_id'] . '" onclick="saveEditContact(' . $value['contact_id'] . ')" style="display:none; background-color:' . $accRGB . '; color: #ffffff; border: 2px solid; border-radius: 6px !important; font-size:0.8em !important; height:24px; width: 85px; margin-top:0px; padding-top:3px;">
                                                                    <i class="fa fa-save" style="font-size:1.0em !important;color:#ffffff; margin-top:-6px;"></i>Save
                                                                </a>
                                                            </center>
                                                          </td>';
                              echo '<td>
                                                            <center>
                                                                <a class="btn btn-block btn-social btn-dropbox" name="' . $value['contact_id'] . '" onclick="deleteContact(' . $value['contact_id'] . ', `' . $value['contact_msisdn'] . '`, `' . $value['contact_firstname'] . '`, `' . $value['contact_lastname'] . '`)" style="background-color:' . $accRGB . '; color: #ffffff; border: 2px solid; border-radius: 6px !important; font-size:0.8em !important; height:24px; width: 92px; margin-top:0px; padding-top:3px;">
                                                                    <i class="fa fa-trash" style="font-size:1.0em !important; color:#ff3333; margin-top:-6px;"></i>Delete
                                                                </a>
                                                            </center>
                                                          </td>';
                              echo '</tr>';
                           }
                        }
                        ?>
                     </tbody>
                  </table>
               </div>
               <div class="box-body" style="padding-top:30px;">
                  <div class="form-group">
                     <a onclick="downloadContactList();" class="btn btn-block btn-social btn-dropbox " style="background-color:<?php echo $accRGB; ?>; border: 2px solid; border-radius: 6px !important; float:left; width: 160px; margin-top:-21px;">
                        <i class="fa fa-save"></i>Save Contact List
                     </a>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </section>
</aside>

<!--  THE JAVASCRIPT IMPORT ALWAYS APPEARS BEFORE LOCAL JAVASCRIPT AT THE BOTTOM OF THE PAGE -->
<?php include("template_import_script.php"); //must import all scripts first   ?>
<!-- END JAVASCRIPT IMPORT -->

<script type="text/javascript">
   function downloadContactList()
   {
      var csvContent = "data:text/csv;charset=utf-8,";

      var data = [["Number", "First Name", "Last Name"],
<?php
if (isset($CAData) && isset($CAData) != '') {
   foreach ($CAData as $key => $value) {
      echo '[';
      echo '"' . $value['contact_msisdn'] . '",';
      echo '"' . $value['contact_firstname'] . '",';
      echo '"' . $value['contact_lastname'] . '"';
      echo '],';
   }
}
?>
      ];
      data.forEach(function (infoArray, index)
      {
         dataString = infoArray.join(",");
         csvContent += index < data.length ? dataString + "\n" : dataString;
      });
      var encodedUri = encodeURI(csvContent);
      var link = document.createElement("a");
      link.setAttribute("href", encodedUri);
      link.setAttribute("download", "<?php echo 'ContactList_'; ?>.csv");

      link.click();
   }
</script>

<script type="text/javascript">
   function reloadOnSelect(name)
   {
      $(".loader").fadeIn("slow");
      $(".loaderIcon").fadeIn("slow");
      window.location = "contactlists.php?client=" + name;
   }

   function newT(type)
   {
      document.getElementById(type).style.display = 'block';
   }

   function saveNewContactList()
   {
      var sId = "<?php
if (isset($cl)) {
   echo $cl;
}
?>";
      var uId = "<?php echo $_SESSION['userId']; ?>";
      var nName = document.getElementById('newClientName').value;
      var nDesc = document.getElementById('newClientDesc').value;
      var nCode = document.getElementById('newClientCode').value;
      var stat = 'ENABLED';
      var cli = "<?php
if (isset($_GET['client'])) {
   echo $_GET['client'];
}
?>";

      window.location = "contactlists.php?newContactList=1&sId=" + sId + "&uId=" + uId + "&nName=" + nName + "&nDesc=" + nDesc + "&nCode=" + nCode + "&stat=" + stat + "&client=" + cli;
   }

   function saveNewContact(client)
   {
      var cId = client;
      var sId = "<?php
if (isset($cl)) {
   echo $cl;
}
?>";
      var nCampName = document.getElementById('newCampName').value;
      var nCampLast = document.getElementById('newCampLast').value;
      var nCampNumb = document.getElementById('newCampNumb').value;
      //var stat = 'ENABLED';
      var cli = "<?php
if (isset($_GET['client'])) {
   echo $_GET['client'];
}
?>";

      window.location = "contactlists.php?newContact=1&cId=" + cId + "&sId=" + sId + "&nCampName=" + nCampName + "&nCampLast=" + nCampLast + "&nCampNumb=" + nCampNumb + "&client=" + cli;
   }

   function uploadNewContact(client)
   {
      var cId = client;
      var sId = "<?php
if (isset($cl)) {
   echo $cl;
}
?>";
      var cli = "<?php
if (isset($_GET['client'])) {
   echo $_GET['client'];
}
?>";

      window.location = "contactlists.php?uploadContact=1&cId=" + cId + "&sId=" + sId + "&client=" + cli;
   }

   function editContact(v)
   {
      var caNa = 'caNa' + v;
      var caDe = 'caDe' + v;
      var caCo = 'caCo' + v;
      var caEd = 'editC' + v;
      var caSa = 'saveC' + v;

      var name = document.getElementById(caNa);
      name.style.border = "thin solid #aaaaaa";
      name.style.background = '#ffffff';
      name.removeAttribute('readonly');

      var desc = document.getElementById(caDe);
      desc.style.border = "thin solid #aaaaaa";
      desc.style.background = '#ffffff';
      desc.removeAttribute('readonly');

      var code = document.getElementById(caCo);
      code.style.border = "thin solid #aaaaaa";
      code.style.background = '#ffffff';
      code.removeAttribute('readonly');

      var editBtn = document.getElementById(caEd);
      editBtn.style.display = 'none';

      var saveBtn = document.getElementById(caSa);
      saveBtn.style.display = 'block';
   }

   function editContactList(v)
   {
      //alert(v);
      var clNa = 'clNa' + v;
      var clDe = 'clDe' + v;
      var clEd = 'edit' + v;
      var clSa = 'save' + v;

      var name = document.getElementById(clNa);
      name.style.border = "thin solid #aaaaaa";
      name.style.background = '#ffffff';
      name.removeAttribute('readonly');

      var desc = document.getElementById(clDe);
      desc.style.border = "thin solid #aaaaaa";
      desc.style.background = '#ffffff';
      desc.removeAttribute('readonly');

      var editBtn = document.getElementById(clEd);
      editBtn.style.display = 'none';

      var saveBtn = document.getElementById(clSa);
      saveBtn.style.display = 'block';
   }

   function saveEditContact(v)
   {
      //alert(v);
      var caNa = 'caNa' + v;
      var caDe = 'caDe' + v;
      var caCo = 'caCo' + v;
      var cli = "<?php
if (isset($_GET['client'])) {
   echo $_GET['client'];
}
?>";
      var cId = "<?php
if (isset($_GET['cId'])) {
   echo $_GET['cId'];
}
?>";
      var name = document.getElementById(caNa).value;
      var cont = document.getElementById(caDe).value;
      var last = document.getElementById(caCo).value;
      window.location = "contactlists.php?saveEditContact=1&caId=" + v + "&name=" + name + "&last=" + last + "&cont=" + cont + "&client=" + cli + "&cId=" + cId;
   }

   function saveEditContactList(v)
   {
      var clNa = 'clNa' + v;
      var clDe = 'clDe' + v;
      var sId = "<?php
if (isset($cl)) {
   echo $cl;
}
?>";
      var cli = "<?php
if (isset($_GET['client'])) {
   echo $_GET['client'];
}
?>";
      var name = document.getElementById(clNa).value;
      var desc = document.getElementById(clDe).value;
      window.location = "contactlists.php?saveEditContactList=1&ccId=" + v + "&name=" + name + "&desc=" + desc + "&client=" + cli + "&sId=" + sId;
   }

   function deleteContactList(clId, cn, cd)
   {
      var r = confirm("Are you sure you want to delete this contact group? \nGroup: " + cn + " - " + cd);
      if (r == true)
      {
         var cli = "<?php
if (isset($_GET['client'])) {
   echo $_GET['client'];
}
?>";
         window.location = "contactlists.php?deleteContactList=1&client=" + cli + "&delId=" + clId;
      }
   }

   function deleteContact(id, cell, n, sn)
   {
      var r = confirm("Are you sure you want to delete this contact? \nContact: " + cell + " - " + n + " - " + sn);
      if (r == true)
      {
         var cli = "<?php
if (isset($_GET['client'])) {
   echo $_GET['client'];
}
?>";
         var cId = "<?php
if (isset($_GET['cId'])) {
   echo $_GET['cId'];
}
?>";
         window.location = "contactlists.php?deleteContact=1&client=" + cli + "&cId=" + cId + "&delId=" + id;
      }
   }

   function showContacts(cId)
   {
      $(".loader").fadeIn("slow");
      $(".loaderIcon").fadeIn("slow");
      window.location = "contactlists.php?client=" + "<?php echo $_GET['client']; ?>" + "&cId=" + cId;
   }
</script>

<script type="text/javascript">
   $(window).load(function () {
      $(".loader").fadeOut("slow");
      $(".loaderIcon").fadeOut("slow");
   })
</script>

<!-- Template Footer -->

