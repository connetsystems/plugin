<?php
/**
 * Include the header template which sets up the HTML
 *
 * Don't forget to include template_import_script before any Javascripting
 * Don't forget to include template footer.php at the bottom of the page as well
 */
//set the page title for the template import

$headerPageTitle = 'Sales Rates';

TemplateHelper::initialize();

$countries = getListOfCountries();
$collections = getListOfCollectionsInt();

function get_currency($from_Currency, $to_Currency, $amount) {

   $amount = urlencode($amount);
   $from_Currency = urlencode($from_Currency);
   $to_Currency = urlencode($to_Currency);

   $url = "http://www.google.com/finance/converter?a=$amount&from=$from_Currency&to=$to_Currency";

   $ch = curl_init();
   $timeout = 0;
   curl_setopt($ch, CURLOPT_URL, $url);
   curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);

   curl_setopt($ch, CURLOPT_USERAGENT, "Mozilla/4.0 (compatible; MSIE 8.0; Windows NT 6.1)");
   curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, $timeout);
   $rawdata = curl_exec($ch);
   curl_close($ch);
   $data = explode('bld>', $rawdata);
   $data = explode($to_Currency, $data[1]);

   return round($data[0], 2);
}

if (isset($_GET['search2']) && $_GET['search2'] == "1") {
   //run query - search by country and collection
   $colData = getCollectionsInternationalByCountryAndCollection($_GET['country'], $_GET['collection']);
}

// Call the function to get the currency converted
$exrate = get_currency('EUR', 'ZAR', 1);
?>

<aside class="right-side">
   <section class="content-header">
      <h1> <?php echo 'Sales Rates - 1 Euro - ZAR : R ' . $exrate; ?>
      </h1>
   </section>

   <section class="content">
      <div class="box-body col-md-12" style="margin-top:0px;">
         <div class="panel box box-warning" style="padding-bottom:1px; border-top-color:<?php echo $accRGB; ?>;">
            <div class="box-body">
               <div class="box-body table-responsive no-padding">
                  <div class="form-group col-md-4">
                     <label>Choose Country</label>
                     <select class="form-control" id="countryCheck" name="country">
                        <?php
                        echo "<option SELECTED>Show All</option>";
                        if (isset($countries)) {
                           foreach ($countries as $key => $value) {
                              $value['prefix_country_name'] = trim($value['prefix_country_name']);
                              if ($value['prefix_country_name'] != '') {
                                 if (isset($_GET['country']) && $_GET['country'] == $value['prefix_country_name']) {
                                    echo '<option value="' . $value['prefix_country_name'] . ' " SELECTED>' . $value['prefix_country_name'] . '</option>';
                                 } else {
                                    echo '<option value="' . $value['prefix_country_name'] . ' ">' . $value['prefix_country_name'] . '</option>';
                                 }
                              }
                           }
                        }
                        ?>
                     </select>
                  </div>

                  <div class="form-group col-md-2">
                     <label>Choose Collection</label>
                     <select id="colCheck" class="form-control" name="colCheck">
                        <?php
                        echo "<option SELECTED>Show All</option>";
                        if (isset($collections)) {
                           foreach ($collections as $key => $value) {
                              if (isset($_GET['collection']) && $_GET['collection'] == $value['route_collection_id']) {
                                 echo "<option SELECTED value='" . $value['route_collection_id'] . "'>" . $value['route_collection_name'] . "</option>";
                              } else {
                                 echo "<option value='" . $value['route_collection_id'] . "' >" . $value['route_collection_name'] . "</option>";
                              }
                           }
                        }
                        ?>
                     </select>
                  </div>

                  <div class="form-group col-md-2">
                     <a class="btn btn-block btn-social btn-dropbox " onclick="filterSelect2();" style="background-color: <?php echo $accRGB; ?>; height:36px; color: #ffffff; border: 2px solid; border-radius: 6px !important; float:left; width: 100px; margin-top:24px;">
                        <i class="fa fa-search"></i>Search
                     </a>
                  </div>

                  <table id="routesTable" class="table table-hover table-bordered table-striped">
                     <thead>
                     <tr role="row">
                        <th>ID</th>
                        <th>Country</th>
                        <th>Network</th>
                        <th colspan="4" align="left" style="background-color:#f3ffd1;">Wholesale</th>
                        <th colspan="4" align="left" style="background-color:#fabe92;">Direct</th>
                     </tr>
                     </thead>
                     <tbody>
                     <?php
                     $k = 0;

                     $oldName = '';
                     $countColData = 0;
                     if (isset($colData) && !empty($colData)) {
                        foreach ($colData as $key => $value) {


                           if ($oldName != $value['route_collection_name']) {
                              $oldName = $value['route_collection_name'];
                           }


                           $countColData++;
                        }

                        $tempCol = "";



                        $ks = 0;
                        $countrynew = "";

                        foreach ($colData as $key => $value) {



                           $country = $value['prefix_country_name'];
                           /* if($countrynew == "" )
                             {

                             }
                             else */
                           //if($key%4 == 0 && $key != 0)
                           if ($country != $countrynew) {
                              if ($countrynew != '') {
                                 echo '<tr style="height:20px;">';
                                 echo '<td></td>';
                                 echo '<td></td>';
                                 echo '<td></td>';
                                 echo '<td colspan="4" align="left" style="background-color:#f3ffd1;"><b>Wholesale</b></td>';
                                 echo '<td colspan="4" align="left" style="background-color:#fabe92;"><b>Direct (Retail)</b></td>';
                                 echo '</tr>';
                              }
                              $countrynew = $country;

                              //$ks++;
                           }


                           echo '<tr>';

                           //$emptyRow++;
                           echo '<td style="vertical-align:middle;width:50px;">' . $value['cdr_id'] . '</td>';



                           if (substr($value['prefix_country_name'], 0, 3) == "USA") {
                              $flag = "USA";
                           } else {
                              $flag = $value['prefix_country_name'];
                           }
                           $flag = str_replace('/', '&', $flag);

                           //echo '<td>'.$value['prefix_country_name'].'</td>';
                           echo '<td style="vertical-align:middle;width:200px;"><img src="../img/flags/' . $flag . '.png">&nbsp;&nbsp;' . $value['prefix_country_name'] . '</td>';




                           $logo = trim(strstr($value['prefix_network_name'], '(', true));


                           echo '<td style="vertical-align:middle;width:200px;"><img src="../img/serviceproviders/' . $logo . '.png">&nbsp;&nbsp;' . $value['prefix_network_name'] . '</td>';

                           $valueTemp = ' ' . $value['cdr_comments'];

                           //Wholesale currency euros
                           echo '<td style="color:#3333dd;vertical-align:middle; text-align:center;width:5px;background-color:#f3ffd1;"><b><i class="fa fa-eur"></i></b></td>';

                           //Wholesale cost value + 15%
                           if ((strpos($valueTemp, 'eur') != false) || $value['cdr_comments'] == 'eur') {
                              echo '<td style="vertical-align:middle;text-align:right;width:30px;;background-color:#f3ffd1;">' . number_format(($value['cdr_cost_per_unit'] / 10000) * 1.20, 4, '.', '') . '</td>';
                           } else {
                              echo '<td style="vertical-align:middle;text-align:right;width:30px;background-color:#f3ffd1;">' . number_format((($value['cdr_cost_per_unit'] / 10000) * 1.20) / $exrate, 4, '.', '') . '</td>';
                           }

                           //Wholesale cost value R + 15%
                           echo '<td style="color:#993333;vertical-align:middle; text-align:center;width:5px;background-color:#f3ffd1;"><b>R</b></td>';
                           if ((strpos($valueTemp, 'eur') != false) || $value['cdr_comments'] == 'eur') {
                              echo '<td style="vertical-align:middle;text-align:right;width:30px;background-color:#f3ffd1;">' . number_format((($value['cdr_cost_per_unit'] / 10000) * 1.20) * $exrate, 4, '.', '') . '</td>';
                           } else {
                              echo '<td style="vertical-align:middle;text-align:right;width:30px;background-color:#f3ffd1;">' . number_format(($value['cdr_cost_per_unit'] / 10000) * 1.20, 4, '.', '') . '</td>';
                           }

                           //Direct currency
                           echo '<td style="color:#3333dd;vertical-align:middle; text-align:center;width:5px;background-color:#fabe92;"><b><i class="fa fa-eur"></i></b></td>';

                           //Direct cost value + 25%
                           if ((strpos($valueTemp, 'eur') != false) || $value['cdr_comments'] == 'eur') {
                              echo '<td style="vertical-align:middle;text-align:right;width:30px;;background-color:#fabe92;">' . number_format(($value['cdr_cost_per_unit'] / 10000) * 1.30, 4, '.', '') . '</td>';
                           } else {
                              echo '<td style="vertical-align:middle;text-align:right;width:30px;background-color:#fabe92;">' . number_format((($value['cdr_cost_per_unit'] / 10000) * 1.30) / $exrate, 4, '.', '') . '</td>';
                           }


                           //Direct cost value R + 25%
                           echo '<td style="color:#993333;vertical-align:middle; text-align:center;width:5px;background-color:#fabe92;"><b>R</b></td>';
                           if ((strpos($valueTemp, 'eur') != false) || $value['cdr_comments'] == 'eur') {
                              echo '<td style="vertical-align:middle;text-align:right;width:30px;background-color:#fabe92;">' . number_format((($value['cdr_cost_per_unit'] / 10000) * 1.30) * $exrate, 4, '.', '') . '</td>';
                           } else {
                              echo '<td style="vertical-align:middle;text-align:right;width:30px;background-color:#fabe92;">' . number_format(($value['cdr_cost_per_unit'] / 10000) * 1.30, 4, '.', '') . '</td>';
                           }


                           echo '</tr>';
                        }
                     }else{
                        echo '<tr style="height:20px;" role="row">';
                        echo '<td colspan="8">No records found.';
                        echo '</td>';
                        echo '</tr>';

                     }
                     ?>
                     </tbody>
                  </table>
               </div><!-- /.box-body -->
            </div>
            <?php if(count($colData) > 0 ) {?>
               <br>
               <div class="box-body">
                  <div class="form-group">
                     <a href="../php/createCSVCollectionRates.php?country=<?php echo $_GET['country']; ?>&collection=<?php echo $_GET['collection']; ?>" class="btn btn-block btn-social btn-dropbox " style="background-color:<?php echo $accRGB; ?>; border: 2px solid; border-radius: 6px !important; float:left; width: 134px; margin-top:-21px;">
                        <i class="fa fa-save"></i>Save Report
                     </a>
                  </div>
               </div>
            <?php } ?>
         </div>
      </div>
   </section>
</aside>

<!--  THE JAVASCRIPT IMPORT ALWAYS APPEARS BEFORE LOCAL JAVASCRIPT AT THE BOTTOM OF THE PAGE -->
<?php include("template_import_script.php"); //must import all scripts first     ?>
<!-- END JAVASCRIPT IMPORT -->
<script type="text/javascript">
   function filterSelect2()
   {
      $(".loader").fadeIn("slow");
      $(".loaderIcon").fadeIn("slow");
      var country = document.getElementById('countryCheck').value;
      country = country.trim();
      var collection = document.getElementById('colCheck').value;
      collection = collection.trim();
      window.location = "collectionsRates.php?search2=1" + "&country=" + country + "&collection=" + collection;

   }
   function saveReport(){
      window.location = "collectionsRates.php?search2=1" + "&country=" + country + "&collection=" + collection;
   }
</script>
<script type="text/javascript">
   $(window).load(function () {
      $(".loader").fadeOut("slow");
      $(".loaderIcon").fadeOut("slow");
   })
</script>
<!-- Template Footer -->

