<?php
/**
 * Include the header template which sets up the HTML
 *
 * Don't forget to include template_import_script before any Javascripting
 * Don't forget to include template footer.php at the bottom of the page as well
 */
$headerPageTitle = "Collections - South Africa"; //set the page title for the template import
TemplateHelper::initialize();

$startDate = date('Y-m-d');
$endDate = date("Y-m-d H:i:s");


//echo ''.$startDate;
//5,14,29,34,37,40,42,54,67,69,73,74,86,91,92,98
$left_column_ids = ['14', '92', '86', '34', '5', '42', '67', '73'];
$right_column_ids = ['37', '91', '69', '40', '54', '74', '98'];
$all_collection_data = getSACollections();
$collection_data_left = array();
$collection_data_right = array();

foreach ($all_collection_data as $key => $collection_data)
{
   if (in_array($collection_data['collection_id'], $right_column_ids))
   {
      array_push($collection_data_right, $collection_data);
   }

   if (in_array($collection_data['collection_id'], $left_column_ids))
   {
      array_push($collection_data_left, $collection_data);
   }
}

$tot = 0;
$k = 0;
$totArr = array();
$oldName = '';
$countColDataL = 0;

foreach ($collection_data_left as $key => $collection_data)
{
   $tot = 0;

   if ($oldName != $collection_data['route_collection_name'])
   {
      $oldName = $collection_data['route_collection_name'];
      $totArr[$oldName] = 0;
      $tot += $collection_data['units'];
   }
   else
   {
      $tot += $collection_data['units'];
   }

   $totArr[$oldName] += $tot;
   $countColDataL++;
}

?>


<aside class="right-side">
   <section class="content-header">
      <h1>
         South African - Collections
      </h1>
   </section>

   <section class="content">
      <div class="box-body col-md-6" style="margin-top:0px;">
         <div class="table-responsive no-padding">
            <table id="routesTable" class="table table-hover table-bordered table-striped">
               <thead>
                  <tr role="row">
                     <th style="text-align:right; width:70px;">Totals</th>
                     <th style="text-align:center; width:50px;">%</th>
                     <th>Network</th>
                     <th style="width:110px;">Live D</th>
                     <th style="width:110px;">Live P</th>
                     <th>Default D</th>
                     <th>Default P</th>
                  </tr>
               </thead>
               <tbody>
                  <?php


                  $tempCol = "";
                  $tots = 0;

                  $ks = 0;
                  foreach ($collection_data_left as $key => $collection_data)
                  {
                     //echo "<pre>";
                     //print_r($collection_data);
                     //echo "</pre>";

                     $ks = $collection_data['route_collection_name'];

                     if ($key % 4 == 0 && $key != 0)
                     {
                        echo '<tr style="height:20px;">';
                        echo '<td style="text-align:right;">' . number_format($tots, 0, '', ' ') . '</td>';
                        echo '<td colspan="6"></td>';
                        echo '</tr>';
                        echo '<tr>';
                        echo '<td colspan=9 style="background-color:#a8c7e1;"><b>' . $collection_data['route_collection_name'] . '</b> ';
                        echo $collection_data['route_collection_description'];
                        echo '</td>';
                        echo '</tr>';
                        $tots = 0;
                        //$ks++;
                     }

                     echo '<tr>';
                     $tots += $collection_data['units'];
                     if ($key % 4 == 0 && $key == 0)
                     {
                        echo '<tr>';
                        echo '<td colspan=9 style="background-color:#a8c7e1;"><b>' . $collection_data['route_collection_name'] . '</b> ';
                        echo $collection_data['route_collection_description'];
                        echo '</td>';
                        echo '</tr>';
                        /* 1echo '<tr>';
                          echo '<td>';
                          echo $collection_data['route_collection_description'];
                          echo '</td>';
                          echo '</tr>'; */
                     }



                     echo '<td style="text-align:right;width:30px;">' . number_format($collection_data['units'], 0, '', ' ') . '</td>';

                     if ($totArr[$ks] != 0) {
                        echo '<td style="text-align:center;width:30px;">' . number_format((($collection_data['units'] / $totArr[$ks]) * 100), 2, '.', ' ') . '</td>';
                     } else {
                        echo '<td style="text-align:center;width:30px;">0</td>';
                     }

                     $s1 = strrpos($collection_data['prefix_network_name'], '/');
                     $s2 = strrpos($collection_data['prefix_network_name'], '(');
                     $logo = substr($collection_data['prefix_network_name'], $s1, $s2);
                     $logo = trim($logo);

                     if ($logo != "Cell C") {
                        if ($logo != 'Vodacom SA') {
                           if ($logo != 'MTN-SA') {
                              if ($logo != 'Telkom Mobile') {
                                 if ($logo != 'MTN (Nigeria)') {
                                    $logo = substr($collection_data['prefix_network_name'], $s1 + 1, $s2 - $s1 - 2);
                                    $logo = trim($logo);
                                 }
                              }
                           }
                        }
                     }
                     echo '<td style="vertical-align:middle;width:20px;"><img src="../img/serviceproviders/' . $logo . '.png"></td>';

                     if ($collection_data['default_route'] != $collection_data['dmatrix_route'])
                     {
                        $btn_class = " btn-warning ";
                     }
                     else
                     {
                        $btn_class = " btn-default ";
                     }

                     $available_routes = getAllNetworkRoutesOnCollection($collection_data['collection_id'], $collection_data['mccmnc_d']);
                     $routeIcon = strstr($collection_data['default_route'], '_', true);

                     /*
                      * SHOW THE LIVE DIRECT ROUTES
                      */
                     echo '<td style="vertical-align:middle;'.$row_css_background.'" id="td_selected_direct_route'.$collection_data['collection_id'].'_'.$collection_data['mccmnc_d'].'">';
                        //build the drop down that controls the LIVE DIRECT route
                        echo '<div class="dropdown">';
                           echo '<input type="hidden" id="selected_route'.$collection_data['collection_id'].'_'.$collection_data['mccmnc_d'].'" value="'.str_replace("_smsc", "", $collection_data['default_route']).'"/>';
                           echo '<button class="btn btn-xs btn-block '.$btn_class.' dropdown-toggle" id="btn_dropdown_route'.$collection_data['collection_id'].'_'.$collection_data['mccmnc_d'].'" current_route_id="'.$collection_data['selected_collection_route_id'].'" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true" data-toggle="tooltip" title="Click to change route.">';
                              echo str_replace("_smsc", "", $collection_data['default_route']).' <span class="caret"></span>';
                           echo '</button>';
                           echo '<ul class="dropdown-menu" aria-labelledby="dropdownMenu1">';
                           foreach($available_routes as $route)
                           {
                              //cleanup the text, the _smsc is unnecessary
                              $option_text = str_replace("_smsc", "", $route['route_collection_route_smsc_name']);
                              echo '<li><a href="#" class="btn_select_route" id="btn_select_route'.$collection_data['collection_id'].'_'.$collection_data['mccmnc_d'].'_'.$route['route_collection_route_id'].'" mccmnc="'.$collection_data['mccmnc_d'].'" collection_id="'.$collection_data['collection_id'].'" route_collection_route_id="'.$route['route_collection_route_id'].'">';
                              if($collection_data['default_route'] == $route['route_collection_route_smsc_name'])
                              {
                                 echo '<strong>'.$option_text.'</strong>';
                              }
                              else
                              {
                                 echo $option_text;
                              }
                              echo '</a></li>';
                           }
                           echo'</ul>';
                        echo'</div>';
                     echo '</td>';


                     /*
                     * SHOW THE LIVE PORTED ROUTES
                     */
                     if ($collection_data['ported_route'] != $collection_data['pmatix_route'])
                     {
                        $btn_class = " btn-warning ";
                     }
                     else
                     {
                        $btn_class = " btn-default ";
                     }

                     $available_routes = getAllNetworkRoutesOnCollection($collection_data['collection_id'], $collection_data['mccmnc_px']);
                     $routeIcon = strstr($collection_data['ported_route'], '_', true);

                     echo '<td style="vertical-align:middle;background-color:#cdcdcd;"  id="td_selected_ported_route'.$collection_data['collection_id'].'_'.$collection_data['mccmnc_d'].'">';
                        //build the drop down that controls the LIVE DIRECT route
                        echo '<div class="dropdown">';
                           echo '<input type="hidden" id="selected_route'.$collection_data['collection_id'].'_'.$collection_data['mccmnc_px'].'" value="'.str_replace("_smsc", "", $collection_data['ported_route']).'"/>';
                           echo '<button class="btn btn-xs btn-block '.$btn_class.' dropdown-toggle" id="btn_dropdown_route'.$collection_data['collection_id'].'_'.$collection_data['mccmnc_px'].'" current_route_id="'.$collection_data['selected_collection_route_id'].'" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true" data-toggle="tooltip" title="Click to change route.">';
                              echo str_replace("_smsc", "", $collection_data['ported_route']).' <span class="caret"></span>';
                           echo '</button>';

                           echo '<ul class="dropdown-menu" aria-labelledby="dropdownMenu1">';
                           foreach($available_routes as $route)
                           {
                              //cleanup the text, the _smsc is unnecessary
                              $option_text = str_replace("_smsc", "", $route['route_collection_route_smsc_name']);
                              echo '<li><a href="#" class="btn_select_route" id="btn_select_route'.$collection_data['collection_id'].'_'.$collection_data['mccmnc_px'].'_'.$route['route_collection_route_id'].'" mccmnc="'.$collection_data['mccmnc_px'].'" collection_id="'.$collection_data['collection_id'].'" route_collection_route_id="'.$route['route_collection_route_id'].'">';
                              if($collection_data['ported_route'] == $route['route_collection_route_smsc_name'])
                              {
                                 echo '<strong>'.$option_text.'</strong>';
                              }
                              else
                              {
                                 echo $option_text;
                              }
                              echo '</a></li>';
                           }
                           echo'</ul>';
                        echo'</div>';
                     echo '</td>';

                     /*
                     * SHOW THE DEFAULT DIRECT ROUTE
                     */
                     if ($collection_data['default_route'] == $collection_data['dmatrix_route'])
                     {
                        $class_default_direct = ' ';
                     }
                     else
                     {
                        $class_default_direct = ' text-warning ';
                     }
                     $default_direct_string = str_replace("_smsc", "", $collection_data['dmatrix_route']);
                     echo '<td class="'.$class_default_direct.'" style="vertical-align:middle;" id="td_default_route'.$collection_data['collection_id'].'_'.$collection_data['mccmnc_d'].'">';
                        echo $default_direct_string;
                        echo '<input type="hidden" id="default_route'.$collection_data['collection_id'].'_'.$collection_data['mccmnc_d'].'" value="'.$default_direct_string.'"/>';
                     echo '</td>';

                     /*
                     * SHOW THE DEFAULT POPRTED ROUTE
                     */
                     if ($collection_data['ported_route'] == $collection_data['pmatix_route'])
                     {
                        $class_default_ported = ' ';
                     }
                     else
                     {
                        $class_default_ported = ' text-warning ';
                     }
                     $default_ported_string = str_replace("_smsc", "", $collection_data['pmatix_route']);
                     echo '<td class="'.$class_default_ported.'" style="vertical-align:middle;" id="td_default_route'.$collection_data['collection_id'].'_'.$collection_data['mccmnc_px'].'">';
                        echo $default_ported_string;
                        echo '<input type="hidden" id="default_route'.$collection_data['collection_id'].'_'.$collection_data['mccmnc_px'].'" value="'.$default_ported_string.'"/>';
                     echo '</td>';

                     echo '</tr>';
                     if ($countColDataL - 1 == $key) {
                        echo '<tr style="height:20px;">';
                        echo '<td style="text-align:right;">' . number_format($tots, 0, '', ' ') . '</td>';
                        echo '<td colspan="6"></td>';
                        echo '</tr>';
                     }
                  }
                  ?>
               </tbody>
            </table>
         </div><!-- /.box-body -->
      </div>
      <div class="box-body col-md-6" style="margin-top:0px;">
         <div class="box-body table-responsive no-padding">
            <table id="routesTable" class="table table-hover table-bordered table-striped">
               <thead>
                  <tr role="row">
                     <th style="text-align:right; width:70px;">Totals</th>
                     <th style="text-align:center; width:50px;">%</th>
                     <th>Network</th>
                     <th style="width:110px;">Live D</th>
                     <th style="width:110px;">Live P</th>
                     <th>Default D</th>
                     <th>Default P</th>
                  </tr>
               </thead>
               <tbody>
                  <?php
                  $tot = 0;
                  $k = 0;
                  $totArr = array();
                  $oldName = '';
                  $countColData = 0;
                  foreach ($collection_data_right as $key => $collection_data) {
                     $tot = 0;

                     if ($oldName != $collection_data['route_collection_name']) {
                        $oldName = $collection_data['route_collection_name'];
                        $totArr[$oldName] = 0;
                        $tot += $collection_data['units'];
                     } else {
                        $tot += $collection_data['units'];
                     }

                     $totArr[$oldName] += $tot;
                     $countColData++;
                  }

                  $tempCol = "";
                  //$tempCou = "";
                  //$emptyRow = 0;
                  $tots = 0;
                  //echo '<br>count='.($countColData);
                  $ks = 0;
                  foreach ($collection_data_right as $key => $collection_data) {
                     $ks = $collection_data['route_collection_name'];
                     //echo '<br>k='.$key;
                     /* if($key == 0)
                       {
                       echo '<tr>';
                       echo '<td>';
                       echo $collection_data['route_collection_description'];
                       echo '</td>';
                       echo '</tr>';
                       } */
                     //echo '<br>key='.$key;

                     if ($key % 4 == 0 && $key != 0) {
                        echo '<tr style="height:20px;">';
                        echo '<td style="text-align:right;">' . number_format($tots, 0, '', ' ') . '</td>';
                        echo '<td colspan="6"></td>';
                        echo '</tr>';
                        echo '<tr>';
                        echo '<td colspan=9 style="background-color:#a8c7e1;"><b>' . $collection_data['route_collection_name'] . '</b> ';
                        echo $collection_data['route_collection_description'];
                        echo '</td>';
                        echo '</tr>';
                        $tots = 0;
                        //$ks++;
                     }

                     echo '<tr>';
                     $tots += $collection_data['units'];
                     if ($key % 4 == 0 && $key == 0) {
                        echo '<tr>';
                        echo '<td colspan=9 style="background-color:#a8c7e1;"><b>' . $collection_data['route_collection_name'] . '</b> ';
                        echo $collection_data['route_collection_description'];
                        echo '</td>';
                        echo '</tr>';
                        /* 1echo '<tr>';
                          echo '<td>';
                          echo $collection_data['route_collection_description'];
                          echo '</td>';
                          echo '</tr>'; */
                     }



                     echo '<td style="text-align:right;width:30px;">' . number_format($collection_data['units'], 0, '', ' ') . '</td>';

                     if ($totArr[$ks] != 0) {
                        echo '<td style="text-align:center;width:30px;">' . number_format((($collection_data['units'] / $totArr[$ks]) * 100), 2, '.', ' ') . '</td>';
                     } else {
                        echo '<td style="text-align:center;width:30px;">0</td>';
                     }

                     $s1 = strrpos($collection_data['prefix_network_name'], '/');
                     $s2 = strrpos($collection_data['prefix_network_name'], '(');
                     $logo = substr($collection_data['prefix_network_name'], $s1, $s2);
                     $logo = trim($logo);

                     if ($logo != "Cell C") {
                        if ($logo != 'Vodacom SA') {
                           if ($logo != 'MTN-SA') {
                              if ($logo != 'Telkom Mobile') {
                                 if ($logo != 'MTN (Nigeria)') {
                                    $logo = substr($collection_data['prefix_network_name'], $s1 + 1, $s2 - $s1 - 2);
                                    $logo = trim($logo);
                                 }
                              }
                           }
                        }
                     }
                     echo '<td style="vertical-align:middle;width:20px;"><img src="../img/serviceproviders/' . $logo . '.png"></td>';

                     if ($collection_data['default_route'] != $collection_data['dmatrix_route'])
                     {
                        $btn_class = " btn-warning ";
                     }
                     else
                     {
                        $btn_class = " btn-default ";
                     }

                     $available_routes = getAllNetworkRoutesOnCollection($collection_data['collection_id'], $collection_data['mccmnc_d']);
                     $routeIcon = strstr($collection_data['default_route'], '_', true);

                     /*
                      * SHOW THE LIVE DIRECT ROUTES
                      */
                     echo '<td style="vertical-align:middle;'.$row_css_background.'" id="td_selected_direct_route'.$collection_data['collection_id'].'_'.$collection_data['mccmnc_d'].'">';
                     //build the drop down that controls the LIVE DIRECT route
                     echo '<div class="dropdown">';
                     echo '<input type="hidden" id="selected_route'.$collection_data['collection_id'].'_'.$collection_data['mccmnc_d'].'" value="'.str_replace("_smsc", "", $collection_data['default_route']).'"/>';
                     echo '<button class="btn btn-xs btn-block '.$btn_class.' dropdown-toggle" id="btn_dropdown_route'.$collection_data['collection_id'].'_'.$collection_data['mccmnc_d'].'" current_route_id="'.$collection_data['selected_collection_route_id'].'" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true" data-toggle="tooltip" title="Click to change route.">';
                     echo str_replace("_smsc", "", $collection_data['default_route']).' <span class="caret"></span>';
                     echo '</button>';
                     echo '<ul class="dropdown-menu" aria-labelledby="dropdownMenu1">';
                     foreach($available_routes as $route)
                     {
                        //cleanup the text, the _smsc is unnecessary
                        $option_text = str_replace("_smsc", "", $route['route_collection_route_smsc_name']);
                        echo '<li><a href="#" class="btn_select_route" id="btn_select_route'.$collection_data['collection_id'].'_'.$collection_data['mccmnc_d'].'_'.$route['route_collection_route_id'].'" mccmnc="'.$collection_data['mccmnc_d'].'" collection_id="'.$collection_data['collection_id'].'" route_collection_route_id="'.$route['route_collection_route_id'].'">';
                        if($collection_data['default_route'] == $route['route_collection_route_smsc_name'])
                        {
                           echo '<strong>'.$option_text.'</strong>';
                        }
                        else
                        {
                           echo $option_text;
                        }
                        echo '</a></li>';
                     }
                     echo'</ul>';
                     echo'</div>';
                     echo '</td>';


                     /*
                     * SHOW THE LIVE PORTED ROUTES
                     */
                     if ($collection_data['ported_route'] != $collection_data['pmatix_route'])
                     {
                        $btn_class = " btn-warning ";
                     }
                     else
                     {
                        $btn_class = " btn-default ";
                     }

                     $available_routes = getAllNetworkRoutesOnCollection($collection_data['collection_id'], $collection_data['mccmnc_px']);
                     $routeIcon = strstr($collection_data['ported_route'], '_', true);

                     echo '<td style="vertical-align:middle;background-color:#cdcdcd;"  id="td_selected_ported_route'.$collection_data['collection_id'].'_'.$collection_data['mccmnc_d'].'">';
                     //build the drop down that controls the LIVE DIRECT route
                     echo '<div class="dropdown">';
                     echo '<input type="hidden" id="selected_route'.$collection_data['collection_id'].'_'.$collection_data['mccmnc_px'].'" value="'.str_replace("_smsc", "", $collection_data['ported_route']).'"/>';
                     echo '<button class="btn btn-xs btn-block '.$btn_class.' dropdown-toggle" id="btn_dropdown_route'.$collection_data['collection_id'].'_'.$collection_data['mccmnc_px'].'" current_route_id="'.$collection_data['selected_collection_route_id'].'" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true" data-toggle="tooltip" title="Click to change route.">';
                     echo str_replace("_smsc", "", $collection_data['ported_route']).' <span class="caret"></span>';
                     echo '</button>';

                     echo '<ul class="dropdown-menu" aria-labelledby="dropdownMenu1">';
                     foreach($available_routes as $route)
                     {
                        //cleanup the text, the _smsc is unnecessary
                        $option_text = str_replace("_smsc", "", $route['route_collection_route_smsc_name']);
                        echo '<li><a href="#" class="btn_select_route" id="btn_select_route'.$collection_data['collection_id'].'_'.$collection_data['mccmnc_px'].'_'.$route['route_collection_route_id'].'" mccmnc="'.$collection_data['mccmnc_px'].'" collection_id="'.$collection_data['collection_id'].'" route_collection_route_id="'.$route['route_collection_route_id'].'">';
                        if($collection_data['ported_route'] == $route['route_collection_route_smsc_name'])
                        {
                           echo '<strong>'.$option_text.'</strong>';
                        }
                        else
                        {
                           echo $option_text;
                        }
                        echo '</a></li>';
                     }
                     echo'</ul>';
                     echo'</div>';
                     echo '</td>';

                     /*
                     * SHOW THE DEFAULT DIRECT ROUTE
                     */
                     if ($collection_data['default_route'] == $collection_data['dmatrix_route'])
                     {
                        $class_default_direct = ' ';
                     }
                     else
                     {
                        $class_default_direct = ' text-warning ';
                     }
                     $default_direct_string = str_replace("_smsc", "", $collection_data['dmatrix_route']);
                     echo '<td class="'.$class_default_direct.'" style="vertical-align:middle;" id="td_default_route'.$collection_data['collection_id'].'_'.$collection_data['mccmnc_d'].'">';
                     echo $default_direct_string;
                     echo '<input type="hidden" id="default_route'.$collection_data['collection_id'].'_'.$collection_data['mccmnc_d'].'" value="'.$default_direct_string.'"/>';
                     echo '</td>';

                     /*
                     * SHOW THE DEFAULT POPRTED ROUTE
                     */
                     if ($collection_data['ported_route'] == $collection_data['pmatix_route'])
                     {
                        $class_default_ported = ' ';
                     }
                     else
                     {
                        $class_default_ported = ' text-warning ';
                     }
                     $default_ported_string = str_replace("_smsc", "", $collection_data['pmatix_route']);
                     echo '<td class="'.$class_default_ported.'" style="vertical-align:middle;" id="td_default_route'.$collection_data['collection_id'].'_'.$collection_data['mccmnc_px'].'">';
                     echo $default_ported_string;
                     echo '<input type="hidden" id="default_route'.$collection_data['collection_id'].'_'.$collection_data['mccmnc_px'].'" value="'.$default_ported_string.'"/>';
                     echo '</td>';


                     echo '</tr>';
                     if ($countColData - 1 == $key) {
                        echo '<tr style="height:20px;">';
                        echo '<td style="text-align:right;">' . number_format($tots, 0, '', ' ') . '</td>';
                        echo '<td colspan="6"></td>';
                        echo '</tr>';
                     }
                  }
                  ?>
               </tbody>
            </table>
         </div><!-- /.box-body -->
      </div>
   </section>
</aside>

<!--  THE JAVASCRIPT IMPORT ALWAYS APPEARS BEFORE LOCAL JAVASCRIPT AT THE BOTTOM OF THE PAGE -->
<?php include("template_import_script.php"); //must import all scripts first ?>
<!-- END JAVASCRIPT IMPORT -->

<script type="text/javascript">


   $(document).ready(function (e)
   {
      $(".loader").fadeOut("slow");
      $(".loaderIcon").fadeOut("slow");

      /*************************************
       * Our button listeners for the page
       *************************************/
      var currentDropBtnName = "";
      $(".btn_select_route").on("click", function (e)
      {
         e.preventDefault();
         var selectedCollectionId = $(this).attr('collection_id');
         var selectedMCCMNC = $(this).attr('mccmnc');
         var selectedRouteId = $(this).attr('route_collection_route_id');
         var selectedRouteName = $(this).html();

         currentDropBtnName = "#btn_dropdown_route"+selectedCollectionId+"_"+selectedMCCMNC;
         var currentRouteId = $(currentDropBtnName).attr('current_route_id');
        // alert(currentRouteId);

         //check that we aren't trying to set the route to what it is currently set to
         if(currentRouteId != selectedRouteId)
         {
            $(currentDropBtnName).attr('disabled', 'true');
            $(currentDropBtnName).html('<span class="glyphicon glyphicon-refresh glyphicon-spin"></span> Saving...');


            //run our file processor before submitting it to niel
            $.post("../php/ajaxCollectionsHandler.php", {
               task: "change_collection_route",
               selectedCollectionId: selectedCollectionId,
               selectedMCCMNC: selectedMCCMNC,
               selectedRouteId: selectedRouteId
            }).done(function (data) {
               var json = $.parseJSON(data);

               if (json.success)
               {
                  //do some cleanup on the html, strip out the strong and add it to the selected element
                  $('#btn_select_route'+selectedCollectionId+'_'+selectedMCCMNC+'_'+currentRouteId).find('strong').contents().unwrap();

                  $(currentDropBtnName).attr('current_route_id', selectedRouteId);
                  $('#btn_select_route'+selectedCollectionId+'_'+selectedMCCMNC+'_'+selectedRouteId).html('<strong>'+selectedRouteName+'</strong>');
                  $('#selected_route'+selectedCollectionId+"_"+selectedMCCMNC).val(selectedRouteName);

                  $(currentDropBtnName).html(selectedRouteName+' <span class="caret"></span>');
                  $(currentDropBtnName).removeAttr('disabled');

                  checkDefaultRouteColourAlert(selectedCollectionId, selectedMCCMNC);
               }
               else
               {
                  alert("We were unable to save your route change. This looks like an internal error, please contact support immediately.");
               }

            }).fail(function (data) {
               alert("There was a server error. We were unable to save your route change. Please check your internet connection and reload this page.");
            });
         }

         /**
         * This function checks a MCCMNC row on a collection to see if it's live and ported route is different
         * from it's default, and if so, shows the cells in orange to provide a visual queue to the user
         */
         function checkDefaultRouteColourAlert(collectionId, selectedMCCMNC)
         {
            var default_route_string = $('#default_route'+collectionId+'_'+selectedMCCMNC).val();
            var selected_route_string = $('#selected_route'+collectionId+'_'+selectedMCCMNC).val();

            if(selected_route_string != default_route_string)
            {
               console.log("Don't match");
               $('#btn_dropdown_route'+selectedCollectionId+'_'+selectedMCCMNC).addClass('btn-warning')
               $('#btn_dropdown_route'+selectedCollectionId+'_'+selectedMCCMNC).removeClass('btn-default');
               $('#td_default_route'+collectionId+'_'+selectedMCCMNC).addClass('text-warning');
            }
            else
            {
               console.log("Matches");
               $('#btn_dropdown_route'+selectedCollectionId+'_'+selectedMCCMNC).removeClass('btn-warning');
               $('#btn_dropdown_route'+selectedCollectionId+'_'+selectedMCCMNC).addClass('btn-default');
               $('#td_default_route'+collectionId+'_'+selectedMCCMNC).removeClass('text-warning');
            }
         }
      });

   });
</script>

<!-- Template Footer -->

