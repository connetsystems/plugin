<?php
if (!session_id()) {
   session_start();
}

if (!ini_get('display_errors')) {
   ini_set('display_errors', 1);
}

//Template Helper Workaround
global $mimic_mode;
global $accRGB;
global $tabLogo;
global $headerLogo;
global $userArr;

//this is here to redirect non admin users away from home.php
if (basename($_SERVER["REQUEST_URI"]) == "home.php") {
   //moved this check to the top to insure that the user is routed out of here before it does any work...
   $userArr = array("3", "18", "1", "521", "267", "40", "225", "973", "92", "555", "105", "745", "563", "15", "695", "479", "233", "485", "1085", "1109", "1494", "1516");
   //var_dump($userArr);
   $cont = 0;
   for ($i = 0; $i < count($userArr); $i++) {
      if ($_SESSION['userId'] == $userArr[$i]) {
         $cont++;
      }
   }

   if ($cont == 0) {
      //echo '<br>uid='.$_SESSION['userId'].'-';
      if (isset($_SESSION['resellerId']) && $_SESSION['resellerId'] != '') {
         if ($_SESSION['serviceId'] == $_SESSION['resellerId']) {
            //echo '<br> testing 2';
            header("Location: /resellerhome.php");
            exit();

            //$cont = 1;
            //break;
         } else {
            header("Location: /clienthome.php");
            exit();
            //$cont = 2;
            //break;
         }
      } else {
         header("Location: /clienthome.php");
         exit();
      }
   }
} 
else if (basename($_SERVER["REQUEST_URI"]) == "clienthome.php") {
   if (isset($_SESSION['resellerId']) && $_SESSION['resellerId'] != '') {
      if ($_SESSION['serviceId'] == $_SESSION['resellerId']) {
         header("Location: /resellerhome.php");
         exit();
      }
   }
}
else if (basename($_SERVER["REQUEST_URI"]) == "resellerhome.php") {
   if (isset($_SESSION['resellerId']) && $_SESSION['resellerId'] != '') {
      if ($_SESSION['serviceId'] != $_SESSION['resellerId']) {
         header("Location: /clienthome.php");
         exit();
      }
   }
}

require_once(SITE_BASE_PATH . "/php/config/dbConn.php");
require_once(SITE_BASE_PATH . "/PluginAPI/PluginAPIAutoload.php");

LoginHelper::assertHttps();

//check if the user is logged in, if not, redirect back to login
if (!isset($_SESSION['userId'])) {
   header('Location: /index.php');
   exit();
}

//check if an admin user is MIMICing a client user

$mimic_mode = false;
if (isset($_SESSION['mimicUserId']) && $_SESSION['mimicUserId'] > 0) {
   $mimic_mode = true;
}

//import helper scripts
require_once(SITE_BASE_PATH . "/php/allInclusive.php");

//setting some display variables according to the users account customization
$accRGB = $_SESSION['accountColour'];
$tabLogo = $_SESSION['tabLogo'];
$headerLogo = $_SESSION['headerLogo'];

//header('Content-Type: text/html; charset=UTF-8');
?>
<!DOCTYPE html>
<html ng-app="app">
   <head>
      <meta charset="UTF-8">

      <title>[template-page-title]</title>

      <!--link rel="icon" type="image/png" href="..< ?php //echo $_SESSION['tabLogo']; ?>"-->

      <meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
      <meta http-equiv="content-type" content="text/html; charset=UTF-8">
      <meta name="google" value="notranslate">

      <style>

         .btn-whitelabel {

            background-color: <?php echo $accRGB; ?> !important; 
            color: #ffffff !important;            
            height:36px !important; 
            border: black 1px solid !important; 
            border-radius: 6px !important; 
         }

         .btn-glyph-whitelabel {

            background-color: <?php echo $accRGB; ?> !important; 
            color: #ffffff !important;            
            height:30px !important; 
            border: black 1px solid !important; 
            border-radius: 6px !important;             
         }

         .box-whitelabel {

            border-top-color: <?php echo $accRGB; ?> !important;
            padding: 15px !important;
         }
      </style>          

      <!-- bootstrap 3.0.2 - getbootstrap.com -->
      <link href="[template-base-url]/css/bootstrap.min.css" rel="stylesheet" type="text/css" />

      <!-- Font Awesome - The iconic font and CSS toolkit - fortawesome.github.io/Font-Awesome -->
      <link href="[template-base-url]/css/font-awesome.min.css" rel="stylesheet" type="text/css" />

      <!-- Ionicons - The premium icon font for Ionic Framework - ionicons.com -->
      <link href="[template-base-url]/css/ionicons.min.css" rel="stylesheet" type="text/css" />

      <!-- AdminLTE Theme style - www.bootstrapstage.com/demo/admin-lte/index.html -->
      <link href="[template-base-url]/css/AdminLTE.css" rel="stylesheet" type="text/css" />

      <!-- Morris.js Chart - good-looking charts shouldn't be difficult - /morrisjs.github.io/morris.js -->
      <link href="[template-base-url]/css/morris/morris.css" rel="stylesheet" type="text/css" />

      <!-- DataTables - Table plug-in for jQuery - datatables.net -->
      <link href="[template-base-url]/css/datatables/dataTables.bootstrap.css" rel="stylesheet" type="text/css" />

      <!-- Date Range Picker - A JavaScript component for choosing date ranges - daterangepicker.com -->
      <link href="[template-base-url]/css/daterangepicker/daterangepicker-bs3.css" rel="stylesheet" type="text/css" />

      <!-- Custom connet css styles -->
      <link href="[template-base-url]/css/style2.css" rel="stylesheet" type="text/css" />

      <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
      <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
      <!-- github.com/aFarkas/html5shiv   &   github.com/scottjehl/Respond -->
      <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
      <![endif]-->

      <link rel="icon" type="image/png" href="<?php echo $tabLogo; ?>">

      <!-- bootstrap-wysihtml5 - Simple, beautiful wysiwyg editors - bootstrap-wysiwyg.github.io/bootstrap3-wysiwyg/ -->
      <link href="[template-base-url]/css/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css" rel="stylesheet" type="text/css" />

      <!-- a fast, small, and feature-rich JavaScript library - jquery.com -->
      <script src="[template-base-url]/js/jquery-2.1.4.min.js"></script>
   </head>
   <body class="skin-blue fixed">
      <header class="header">

         <!--link rel="shortcut icon" href="../img/windowIcons/Small.png"-->
         <a style="background-image: url('<?php echo $headerLogo; ?>'); background-color:<?php echo $accRGB; ?>; background-repeat: no-repeat; background-position: center center;" href="#" class="logo">
            <!-- Add the class icon to your logo image or logo icon to add the margining -->
         </a>

         <nav class="navbar navbar-static-top" role="navigation" style="background-color:<?php echo $accRGB; ?> ">
            <!--a href="#" class="navbar-btn sidebar-toggle" data-toggle="offcanvas" role="button">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>

                style="font-size:20px; text-align:center;"
            </a-->
            <div class="navbar-left" >
               <ul class="nav navbar-nav" data-toggle="offcanvas">

                  <li class="active">
                     <a href="#" class="navbar-link">
                        <i class="fa fa-bars"></i>
                        Menu
                     </a>
                  </li>
                  <?php if ($mimic_mode) { ?>
                     <p class="navbar-text">
                        <strong>[<?php echo $_SESSION['mimicUserName']; ?> - You are in mimic mode]</strong>
                     </p>
                  <?php } ?>
               </ul>
            </div>

            <div class="navbar-right">
               <ul class="nav navbar-nav">
                  <?php
                  if ($mimic_mode) {
                     echo '<li><a href="#" id="btnStopMimic" class="navbar-link"> <i class="fa fa-times-circle"></i> Cancel Mimic Mode of <strong>"' . $_SESSION['userName'] . ' - ' . $_SESSION['serviceName'] . '</strong></a>';
                  } else {
                     echo '<p class="navbar-text text-teal">Welcome ' . $_SESSION['userName'] . '.</p>';
                  }
                  ?>

                  <?php
                  if (isset($_SESSION['settings'])) {
                     echo '<li>';
                     echo '<a href="settings.php" class="navbar-link">';
                     echo '<i class="fa fa-cog"></i>';
                     echo 'Settings';
                     echo '</a>';
                     echo '</li>';
                  }
                  ?>
                  <li>
                     <a href="/index.php?logged=out">
                        <i class="glyphicon glyphicon-user"></i>
                        Logout
                     </a>
                  </li>

               </ul>
            </div>
         </nav>
      </header>

      <!--
      <div class="loader"></div>
      <div class="loaderIcon"></div>
      -->

      <!-- CONTENT AFTER THIS -->
      <div class="wrapper row-offcanvas row-offcanvas-left">

         <?php include("template_menu.php"); ?>

         <!-- Angular -->
         <script src="https://cdnjs.cloudflare.com/ajax/libs/angular.js/1.3.0/angular.min.js"></script>
         <script src="https://cdnjs.cloudflare.com/ajax/libs/angular.js/1.3.0/angular-route.min.js"></script>

         <!-- ng-Flow Standalone -->
         <script src="https://cdnjs.cloudflare.com/ajax/libs/ng-flow/2.6.1/ng-flow-standalone.min.js"></script>

         <!-- Application -->
         <script src="[template-base-url]/ng/js/ng-app.js"></script>         

         [template-page-content]

      </div> <!-- CLOSES THE DIV "wrapper row-offcanvas row-offcanvas-left" from template_header.php -->
   </body>
</html>         