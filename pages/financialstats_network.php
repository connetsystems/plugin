<?php


require_once ("../php/allInclusive.php");

if (isset($_POST['tdate'])) {
    $tdate = $_POST['tdate'];
    $fdate = $_POST['fdate'];
    $mccmnc = $_POST['mccmnc'];
    $account_name = $_POST['account_name'];
    $smsc = $_POST['smsc'];

    if ($smsc != 'Show All')
    {
        $networkData = getSmsc_Popup_Country_Network($fdate, $tdate,$smsc);
    }
    if ($account_name != 'Show All')
    {
        $networkData = getAccount_Service_Network_Profit($fdate, $tdate,$mccmnc,$account_name);
    }
    if ($mccmnc != 'Show All')
    {
        $networkData = getSmsc_Popup_Account_Network($fdate, $tdate,$mccmnc);
    }


}

?>


<?php if($account_name != 'Show All')  {?>
    <div class="row">
        <div class="col-md-12">
            <!-- ACCOUNT TABLE -->
            <div class="box box-solid box-info">
                <div class="box-header">
                    <h3 class="box-title">Network</h3>
                </div><!-- /.box-header -->
                <div class="box-body">
                    <table id="Table_popup" class="table table-hover table-bordered table-striped">
                        <thead>
                        <tr role="row">
                            <th>Account</th>
                            <th>Service</th>
                            <th>Country</th>
                            <th>Network</th>
                            <th>Rate</th>
                            <th align="right">Units</th>
                            <th align="right">Client</th>
                            <th align="right">Connet</th>
                            <th align="right">Profit</th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php

                        $units = 0;
                        $client = 0;
                        $connet = 0;
                        $profit = 0;

                        $units_sub = 0;
                        $client_sub = 0;
                        $connet_sub = 0;
                        $profit_sub = 0;

                        $prefix_country_name = '';
                        $service_name = '';
                        $bgcolor = '';
                        $account_name = '';
                        foreach ($networkData as $key => $value) {


                            if(($service_name != $value['service_name']) && ($service_name== ''))
                            {
                                $units_sub += $value['units'];
                                $client_sub += $value['client_zar'];
                                $connet_sub += $value['connet_zar'];
                                $profit_sub += $value['profit_zar'];

                            }
                            elseif($service_name != $value['service_name'])
                            {
                                echo '<tr>';
                                echo '<td colspan="5" align="right"><b>Sub Total</b></td>';
                                echo '<td align="right" style="vertical-align:middle;width:90px;padding: 1px;font-size: 13px;"><b>' . number_format($units_sub, 0, 0, ' ') . '</b></td>';
                                echo '<td align="right" style="vertical-align:middle;width:90px;padding: 1px;font-size: 13px;"><b>R ' . number_format($client_sub, 2, '.', ' ') . '</b></td>';
                                echo '<td align="right" style="vertical-align:middle;width:90px;padding: 1px;font-size: 13px;"><b>R ' . number_format($connet_sub, 2, '.', ' ') . '</b></td>';
                                echo '<td align="right" style="vertical-align:middle;width:90px;padding: 1px;font-size: 13px;"><b>R ' . number_format($profit_sub, 2, '.', ' ') . '</b></td>';
                                echo '</tr>';

                                $units_sub = 0;
                                $client_sub = 0;
                                $connet_sub = 0;
                                $profit_sub = 0;

                                $units_sub += $value['units'];
                                $client_sub += $value['client_zar'];
                                $connet_sub += $value['connet_zar'];
                                $profit_sub += $value['profit_zar'];
                            }
                            else
                            {
                                $units_sub += $value['units'];
                                $client_sub += $value['client_zar'];
                                $connet_sub += $value['connet_zar'];
                                $profit_sub += $value['profit_zar'];
                            }

                            echo '<tr>';

                            $logo = trim($value['prefix_network_name']);

                            switch ($logo)
                            {
                                case "MTN-SA (65510)":
                                    $logo ="MTN-SA";
                                    break;
                                case "Vodacom SA (65501)":
                                    $logo ="Vodacom SA";
                                    break;
                                case "Telkom Mobile (65502)":
                                    $logo ="Telkom Mobile";
                                    break;
                                case "Cell C (65507)":
                                    $logo ="Cell C";
                                    break;
                                default:
                                    $sp = split('[/(]', $value['prefix_network_name']);
                                    $logo = trim($sp[1]);
                                    break;
                            }


                            if($value['profit_zar'] < 0) {
                                $bgcolor = 'background-color:#fbc1c1;';
                            }
                            else
                            {
                                $bgcolor = '';
                            }

                            if($account_name != $value['account_name'])
                            {
                                echo '<td style="vertical-align:middle;padding: 1px;font-size: 13px;'.$bgcolor.'">' . $value['account_name'] . '</td>';
                            }
                            else
                            {
                                echo '<td style="vertical-align:middle;padding: 1px;font-size: 13px;'.$bgcolor.'"></td>';
                            }
                            $account_name = $value['account_name'];

                            if($service_name != $value['service_name'])
                            {
                                echo '<td style="vertical-align:middle;padding: 1px;font-size: 13px;'.$bgcolor.'">' . $value['service_name'] . '</td>';
                            }
                            else
                            {
                                echo '<td style="vertical-align:middle;padding: 1px;font-size: 13px;'.$bgcolor.'"></td>';
                            }

                            if($prefix_country_name != $value['prefix_country_name'])
                            {
                                echo '<td style="vertical-align:middle;padding: 1px;font-size: 13px;'.$bgcolor.'"><img src="../img/flags/'.$value['prefix_country_name'].'.png"> '.$value['prefix_country_name'].'</td>';
                            }
                            elseif($service_name != $value['service_name'])
                            {
                                echo '<td style="vertical-align:middle;padding: 1px;font-size: 13px;'.$bgcolor.'"><img src="../img/flags/'.$value['prefix_country_name'].'.png"> '.$value['prefix_country_name'].'</td>';
                            }
                            else
                            {
                                echo '<td style="vertical-align:middle;padding: 1px;font-size: 13px;'.$bgcolor.'"></td>';
                            }



                            echo '<td style="vertical-align:middle;padding: 1px;font-size: 13px;'.$bgcolor.'"><img src="../img/serviceproviders/' . $logo . '.png">  ' . $value['prefix_network_name'] . '</td>';

                            echo '<td align="right" style="vertical-align:middle;width:90px;padding: 1px;font-size: 13px;'.$bgcolor.'">R ' . number_format($value['client_cost_price'], 4, '.', ' ') . '</td>';
                            echo '<td align="right" style="vertical-align:middle;width:90px;padding: 1px;font-size: 13px;'.$bgcolor.'">' . number_format($value['units'], 0, 0, ' ') . '</td>';
                            echo '<td align="right" style="vertical-align:middle;width:90px;padding: 1px;font-size: 13px;'.$bgcolor.'">R ' . number_format($value['client_zar'], 2, '.', ' ') . '</td>';
                            echo '<td align="right" style="vertical-align:middle;width:90px;padding: 1px;font-size: 13px;'.$bgcolor.'">R ' . number_format($value['connet_zar'], 2, '.', ' ') . '</td>';
                            echo '<td align="right" style="vertical-align:middle;width:90px;padding: 1px;font-size: 13px;'.$bgcolor.'">R ' . number_format($value['profit_zar'], 2, '.', ' ') . '</td>';

                            echo '</tr>';

                            $units += $value['units'];
                            $client += $value['client_zar'];
                            $connet += $value['connet_zar'];
                            $profit += $value['profit_zar'];

                            $prefix_country_name = $value['prefix_country_name'];
                            $service_name = $value['service_name'];

                        }



                        ?>
                        </tbody>
                        <tfoot>
                        <?php
                        echo '<tr>';
                        echo '<td colspan="5" align="right"><b>Sub Total</b></td>';
                        echo '<td align="right" style="vertical-align:middle;width:90px;padding: 1px;font-size: 13px;"><b>' . number_format($units_sub, 0, 0, ' ') . '</b></td>';
                        echo '<td align="right" style="vertical-align:middle;width:90px;padding: 1px;font-size: 13px;"><b>R ' . number_format($client_sub, 2, '.', ' ') . '</b></td>';
                        echo '<td align="right" style="vertical-align:middle;width:90px;padding: 1px;font-size: 13px;"><b>R ' . number_format($connet_sub, 2, '.', ' ') . '</b></td>';
                        echo '<td align="right" style="vertical-align:middle;width:90px;padding: 1px;font-size: 13px;"><b>R ' . number_format($profit_sub, 2, '.', ' ') . '</b></td>';
                        echo '</tr>';
                        echo '<tr>';
                        echo '<td colspan="5" align="right"><b>Totals</b></td>';
                        echo '<td align="right" style="vertical-align:middle;width:90px;padding: 1px;font-size: 13px;"><b>' . number_format($units, 0, 0, ' ') . '</b></td>';
                        echo '<td align="right" style="vertical-align:middle;width:90px;padding: 1px;font-size: 13px;"><b>R ' . number_format($client, 2, '.', ' ') . '</b></td>';
                        echo '<td align="right" style="vertical-align:middle;width:90px;padding: 1px;font-size: 13px;"><b>R ' . number_format($connet, 2, '.', ' ') . '</b></td>';
                        echo '<td align="right" style="vertical-align:middle;width:90px;padding: 1px;font-size: 13px;"><b>R ' . number_format($profit, 2, '.', ' ') . '</b></td>';
                        echo '</tr>';

                        ?>
                        </tfoot>
                    </table>
                </div><!-- /.box-body -->
            </div><!-- /.box -->
        </div>
    </div>
<?php }?>
<?php if($mccmnc != 'Show All')  {?>
    <div class="row">
        <div class="col-md-12">
            <!-- ACCOUNT TABLE -->
            <div class="box box-solid box-info">
                <div class="box-header">
                    <h3 class="box-title">Network</h3>
                </div><!-- /.box-header -->
                <div class="box-body">
                    <table id="Table_popup_mccmnc" class="table table-hover table-bordered table-striped">
                        <thead>
                        <tr role="row">
                            <th>Account Name</th>
                            <th>Service Name</th>
                            <th>Service Rate</th>
                            <th>Provider</th>
                            <th>Provider Cost</th>
                            <th>Units (Excluded)</th>
                            <th>Invoice Amount</th>
                            <th align="right">Profit</th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php

                        $units = 0;
                        $units_rdnc = 0;
                        $connet = 0;
                        $profit = 0;
                        $profit_rdnc = 0;

                        foreach ($networkData as $key => $value) {
                            echo '<tr>';

                            $logo = trim($value['prefix_network_name']);

                            switch ($logo)
                            {
                                case "MTN-SA (65510)":
                                    $logo ="MTN-SA";
                                    break;
                                case "Vodacom SA (65501)":
                                    $logo ="Vodacom SA";
                                    break;
                                case "Telkom Mobile (65502)":
                                    $logo ="Telkom Mobile";
                                    break;
                                case "Cell C (65507)":
                                    $logo ="Cell C";
                                    break;
                                default:
                                    $sp = split('[/(]', $value['prefix_network_name']);
                                    $logo = trim($sp[1]);
                                    break;
                            }

                            if($value['profit_zar'] < 0) {
                                echo '<td style="vertical-align:middle;padding: 1px;font-size: 13px;background-color:#fbc1c1;">' . $value['account_name'] . '</td>';
                                echo '<td style="vertical-align:middle;padding: 1px;font-size: 13px;background-color:#fbc1c1;">' . $value['service_name'] . '</td>';
                                if($value['service_cur'] == 'EUR')
                                {
                                    echo '<td align="right" style="vertical-align:middle;padding: 1px;font-size: 13px;background-color:#fbc1c1;"><b><i class="fa fa-eur" style="color:#3333dd;"></i></b> ' . number_format($value['client_cost_price'], 4, '.', ' ') . '</td>';
                                }
                                else
                                {
                                    echo '<td align="right" style="vertical-align:middle;padding: 1px;font-size: 13px;background-color:#fbc1c1;"><span style="color:#993333;"><b>R</b></span> ' . number_format($value['client_cost_price'], 4, '.', ' ') . '</td>';
                                }

                                echo '<td style="vertical-align:middle;padding: 1px;font-size: 13px;background-color:#fbc1c1;"><img src="../img/serviceproviders/'.strtok($value['smsc'],'_').'.png"/> ' . $value['smsc'] . '</td>';

                                if($value['connet_cur'] == 'EUR')
                                {
                                    echo '<td align="right" style="vertical-align:middle;padding: 1px;font-size: 13px;background-color:#fbc1c1;"><b><i class="fa fa-eur" style="color:#3333dd;"></i></b> ' . number_format($value['connect_cost_price'], 4, '.', ' ') . '</td>';
                                }
                                else
                                {
                                    echo '<td align="right" style="vertical-align:middle;padding: 1px;font-size: 13px;background-color:#fbc1c1;"><span style="color:#993333;"><b>R</b></span> ' . number_format($value['connect_cost_price'], 4, '.', ' ') . '</td>';
                                }

                                echo '<td align="right" style="vertical-align:middle;padding: 1px;font-size: 13px;background-color:#fbc1c1;">'.number_format($value['units'], 0, 0, ' ').' ('.number_format($value['units_rdnc'], 0, 0, ' ').')</td>';

                                if($value['connet_cur'] == 'EUR')
                                {
                                    echo '<td align="right" style="vertical-align:middle;padding: 1px;font-size: 13px;background-color:#fbc1c1;"><b><i class="fa fa-eur" style="color:#3333dd;"></i></b> '.number_format($value['connet_eur'], 2, '.', ' ').'</td>';
                                }
                                else
                                {
                                    echo '<td align="right" style="vertical-align:middle;padding: 1px;font-size: 13px;background-color:#fbc1c1;"><span style="color:#993333;"><b>R</b></span> '.number_format($value['connet_zar'], 2, '.', ' ').'</td>';
                                }
                                echo '<td align="right" style="vertical-align:middle;padding: 1px;font-size: 13px;background-color:#fbc1c1;"><span style="color:#993333;"><b>R</b></span>  '.number_format($value['profit_zar'], 2, '.', ' ').'(<span style="color:#993333;"><b>R</b></span> '.number_format($value['profit_zar_rdnc'], 2, '.', ' ').')</td>';
                            }
                            else
                            {
                                echo '<td style="vertical-align:middle;padding: 1px;font-size: 13px;">' . $value['account_name'] .'</td>';
                                echo '<td style="vertical-align:middle;padding: 1px;font-size: 13px;">' . $value['service_name'] .'</td>';
                                if($value['service_cur'] == 'EUR')
                                {
                                    echo '<td align="right" style="vertical-align:middle;padding: 1px;font-size: 13px;"><b><i class="fa fa-eur" style="color:#3333dd;"></i></b> ' . number_format($value['client_cost_price'], 4, '.', ' ') . '</td>';
                                }
                                else
                                {
                                    echo '<td align="right" style="vertical-align:middle;padding: 1px;font-size: 13px;"><span style="color:#993333;"><b>R</b></span>  ' . number_format($value['client_cost_price'], 4, '.', ' ') . '</td>';
                                }

                                echo '<td style="vertical-align:middle;padding: 1px;font-size: 13px;"><img src="../img/serviceproviders/'.strtok($value['smsc'],'_').'.png"/>' . $value['smsc'] .'</td>';

                                if($value['connet_cur'] == 'EUR')
                                {
                                    echo '<td align="right" style="vertical-align:middle;padding: 1px;font-size: 13px;"><b><i class="fa fa-eur" style="color:#3333dd;"></i></b> ' . number_format($value['connect_cost_price'], 4, '.', ' ') . '</td>';
                                }
                                else
                                {
                                    echo '<td align="right" style="vertical-align:middle;padding: 1px;font-size: 13px;"><span style="color:#993333;"><b>R</b></span>  ' . number_format($value['connect_cost_price'], 4, '.', ' ') . '</td>';
                                }

                                echo '<td align="right" style="vertical-align:middle;padding: 1px;font-size: 13px;">'.number_format($value['units'], 0, 0, ' ').' ('.number_format($value['units_rdnc'], 0, 0, ' ').')</td>';

                                if($value['connet_cur'] == 'EUR')
                                {
                                    echo '<td align="right" style="vertical-align:middle;padding: 1px;font-size: 13px;"><b><i class="fa fa-eur" style="color:#3333dd;"></i></b> '.number_format($value['connet_eur'], 2, '.', ' ').'</td>';
                                }
                                else
                                {
                                    echo '<td align="right" style="vertical-align:middle;padding: 1px;font-size: 13px;"><span style="color:#993333;"><b>R</b></span>  '.number_format($value['connet_zar'], 2, '.', ' ').'</td>';
                                }
                                echo '<td align="right" style="vertical-align:middle;padding: 1px;font-size: 13px;"><span style="color:#993333;"><b>R</b></span>  '.number_format($value['profit_zar'], 2, '.', ' ').' (<span style="color:#993333;"><b>R</b></span> '.number_format($value['profit_zar_rdnc'], 2, '.', ' ').')</td>';
                            }

                            echo '</tr>';

                            $units += $value['units'];
                            $units_rdnc += $value['units_rdnc'];
                            if($value['connet_cur'] == 'EUR')
                            {
                                $connet += $value['connet_eur'];
                            }
                            else
                            {
                                $connet += $value['connet_zar'];
                            }

                            $profit += $value['profit_zar'];
                            $profit_rdnc += $value['profit_zar_rdnc'];

                        }
                        ?>
                        </tbody>
                        <tfoot>
                        <?php
                            echo '<tr>';
                            echo '<td colspan="5" align="right"><b>Total</b></td>';

                            echo '<td align="right" style="vertical-align:middle;padding: 1px;font-size: 13px;"><b>'.number_format($units, 0, 0, ' ').' ('.number_format($units_rdnc, 0, 0, ' ').')</b></td>';

                            if($value['connet_cur'] == 'EUR')
                            {
                                echo '<td align="right" style="vertical-align:middle;padding: 1px;font-size: 13px;"><b><i class="fa fa-eur" style="color:#3333dd;"></i>' . number_format($connet, 2, '.', ' ') . '</b></td>';
                            }
                            else
                            {
                                echo '<td align="right" style="vertical-align:middle;padding: 1px;font-size: 13px;"><b><span style="color:#993333;">R </span>' . number_format($connet, 2, '.', ' ') . '</b></td>';
                            }
                            echo '<td align="right" style="vertical-align:middle;padding: 1px;font-size: 13px;"><b><span style="color:#993333;">R </span>'.number_format($profit, 2, '.', ' ').' (<span style="color:#993333;">R </span>'.number_format($profit_rdnc, 2, '.', ' ').')</b></td>';
                            echo '</tr>';

                            echo '<tr>';
                            echo '<td colspan="5" align="right"><b>Totals</b></td>';

                            $units_totals = $units + $units_rdnc;
                            echo '<td align="right" style="vertical-align:middle;padding: 1px;font-size: 13px;"><b>'.number_format($units_totals, 0, 0, ' ').'</b></td>';
                            echo '<td align="right" style="vertical-align:middle;padding: 1px;font-size: 13px;"><b></b></td>';
                            $profit_totals = $profit + $profit_rdnc;
                            echo '<td align="right" style="vertical-align:middle;padding: 1px;font-size: 13px;"><b><span style="color:#993333;">R </span>'.number_format($profit_totals, 2, 0, ' ').'</b></td>';
                            echo '</tr>';

                        ?>
                        </tfoot>
                    </table>
                </div><!-- /.box-body -->
            </div><!-- /.box -->
        </div>
    </div>
<?php }?>
<?php if ($smsc != 'Show All') { ?>
<div class="row">
    <div class="col-md-12">
        <!-- ACCOUNT TABLE -->
        <div class="box box-solid box-info">
            <div class="box-header">
                <h3 class="box-title"><b>
                        <?php
                         $logoh = trim($smsc);

                        switch ($logoh)
                        {
                            case "mtn_smsc":
                                $logoh ="MTN-SA";
                                break;
                            case "vodacom_smsc":
                                $logoh ="Vodacom SA";
                                break;
                            case "telkom_smsc":
                                $logoh ="Telkom Mobile";
                                break;
                            case "cellc_smsc":
                                $logoh ="Cell C";
                                break;
                            case "mtn_rev_smsc":
                                $logoh ="MTN-SA";
                                break;
                            case "vodacom_pr_smsc":
                                $logoh ="Vodacom SA";
                                break;
                            case "mtc_smsc":
                                $logoh ="MTC";
                                break;
                            default:
                                $logoh = strtok($logoh, '_');
                                break;
                        }
                        echo '<img src="../img/serviceproviders/'.$logoh.'.png"> ' .$logoh;

                        ?>
                    </b> Network BreakDown</h3>
            </div><!-- /.box-header -->
            <div class="box-body">
                <table id="Table_popup" class="table table-hover table-bordered table-striped">
                    <thead>
                    <tr role="row">
                        <?php
                        if ($smsc != 'Show All') {
                            echo '<th>Counry</th>';
                        }
                        else
                        {
                            echo '<th>Account Name</th>';
                            echo '<th>Service Name</th>';
                        }
                        ?>

                        <th>Network</th>
                        <th>Provider Cost</th>
                        <th>Units (Excluded)</th>
                        <th>Invoice Amount</th>
                        <th align="right">Profit</th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php

                    $units = 0;
                    $units_rdnc = 0;
                    $connet = 0;
                    $profit = 0;
                    $profit_rdnc = 0;

                    foreach ($networkData as $key => $value) {
                        echo '<tr>';

                        $logo = trim($value['prefix_network_name']);

                        switch ($logo)
                        {
                            case "MTN-SA":
                                $logo ="MTN-SA";
                                break;
                            case "Vodacom SA":
                                $logo ="Vodacom SA";
                                break;
                            case "Telkom Mobile":
                                $logo ="Telkom Mobile";
                                break;
                            case "Cell C":
                                $logo ="Cell C";
                                break;
                            default:
                                $sp = split('[/(]', $value['prefix_network_name']);
                                $logo = trim($sp[1]);
                                break;
                        }


                        if($value['profit_zar'] < 0) {
                            if ($smsc != 'Show All') {
                                echo '<td style="vertical-align:middle;padding:1px;font-size: 13px;background-color:#fbc1c1;"><img src="../img/flags/' . $value['prefix_country_name'] . '.png">  ' . $value['prefix_country_name'] . '</td>';
                            }
                            else
                            {
                                echo '<td style="vertical-align:middle;padding: 1px;font-size: 13px;background-color:#fbc1c1;">' . $value['account_name'] . '</td>';
                                echo '<td style="vertical-align:middle;padding: 1px;font-size: 13px;background-color:#fbc1c1;">' . $value['service_name'] . '</td>';
                            }

                            echo '<td style="vertical-align:middle;padding: 1px;font-size: 13px;background-color:#fbc1c1;"><img src="../img/serviceproviders/'.$logo.'.png"> ' . $value['prefix_network_name'] . '</td>';

                            if($value['connet_cur'] == 'EUR')
                            {
                                echo '<td align="right" style="vertical-align:middle;padding: 1px;font-size: 13px;background-color:#fbc1c1;"><b><i class="fa fa-eur" style="color:#3333dd;"></i></b> ' . number_format($value['connect_cost_price'], 4, '.', ' ') . '</td>';
                            }
                            else
                            {
                                echo '<td align="right" style="vertical-align:middle;padding: 1px;font-size: 13px;background-color:#fbc1c1;"><span style="color:#993333;"><b>R</b></span> ' . number_format($value['connect_cost_price'], 4, '.', ' ') . '</td>';
                            }

                            echo '<td align="right" style="vertical-align:middle;padding: 1px;font-size: 13px;background-color:#fbc1c1;">'.number_format($value['units'], 0, 0, ' ').' ('.number_format($value['units_rdnc'], 0, 0, ' ').')</td>';

                            if($value['connet_cur'] == 'EUR')
                            {
                                echo '<td align="right" style="vertical-align:middle;padding: 1px;font-size: 13px;background-color:#fbc1c1;"><b><i class="fa fa-eur" style="color:#3333dd;"></i></b> '.number_format($value['connet_eur'], 2, '.', ' ').'</td>';
                            }
                            else
                            {
                                echo '<td align="right" style="vertical-align:middle;padding: 1px;font-size: 13px;background-color:#fbc1c1;"><span style="color:#993333;"><b>R</b></span> '.number_format($value['connet_zar'], 2, '.', ' ').'</td>';
                            }

                            echo '<td align="right" style="vertical-align:middle;padding: 1px;font-size: 13px;background-color:#fbc1c1;"><span style="color:#993333;"><b>R</b></span>  '.number_format($value['profit_zar'], 2, '.', ' ').'(<span style="color:#993333;"><b>R</b></span> '.number_format($value['profit_zar_rdnc'], 2, '.', ' ').')</td>';
                        }
                        else
                        {
                            if ($smsc != 'Show All') {
                                echo '<td style="vertical-align:middle;padding: 1px;font-size: 13px;"><img src="../img/flags/' . $value['prefix_country_name'] . '.png">  ' . $value['prefix_country_name'] .'</td>';
                            }
                            else
                            {
                                echo '<td style="vertical-align:middle;padding: 1px;font-size: 13px;">' . $value['account_name'] .'</td>';
                                echo '<td style="vertical-align:middle;padding: 1px;font-size: 13px;">' . $value['service_name'] .'</td>';
                            }

                            echo '<td style="vertical-align:middle;padding: 1px;font-size: 13px;"><img src="../img/serviceproviders/'.$logo.'.png"> ' . $value['prefix_network_name'] . '</td>';

                            if($value['connet_cur'] == 'EUR')
                            {
                                echo '<td align="right" style="vertical-align:middle;padding: 1px;font-size: 13px;"><b><i class="fa fa-eur" style="color:#3333dd;"></i></b> ' . number_format($value['connect_cost_price'], 4, '.', ' ') . '</td>';
                            }
                            else
                            {
                                echo '<td align="right" style="vertical-align:middle;padding: 1px;font-size: 13px;"><span style="color:#993333;"><b>R</b></span>  ' . number_format($value['connect_cost_price'], 4, '.', ' ') . '</td>';
                            }

                            echo '<td align="right" style="vertical-align:middle;padding: 1px;font-size: 13px;">'.number_format($value['units'], 0, 0, ' ').' ('.number_format($value['units_rdnc'], 0, 0, ' ').')</td>';

                            if($value['connet_cur'] == 'EUR')
                            {
                                echo '<td align="right" style="vertical-align:middle;padding: 1px;font-size: 13px;"><b><i class="fa fa-eur" style="color:#3333dd;"></i></b> '.number_format($value['connet_eur'], 2, '.', ' ').'</td>';
                            }
                            else
                            {
                                echo '<td align="right" style="vertical-align:middle;padding: 1px;font-size: 13px;"><span style="color:#993333;"><b>R</b></span>  '.number_format($value['connet_zar'], 2, '.', ' ').'</td>';
                            }

                            echo '<td align="right" style="vertical-align:middle;padding: 1px;font-size: 13px;"><span style="color:#993333;"><b>R</b></span>  '.number_format($value['profit_zar'], 2, '.', ' ').' (<span style="color:#993333;"><b>R</b></span> '.number_format($value['profit_zar_rdnc'], 2, '.', ' ').')</td>';
                        }

                        echo '</tr>';

                        $units += $value['units'];
                        $units_rdnc += $value['units_rdnc'];
                        if($value['connet_cur'] == 'EUR')
                        {
                            $connet += $value['connet_eur'];
                        }
                        else
                        {
                            $connet += $value['connet_zar'];
                        }

                        $profit += $value['profit_zar'];
                        $profit_rdnc += $value['profit_zar_rdnc'];

                    }

                    ?>
                    </tbody>
                    <tfoot>
                    <?php
                        echo '<tr>';
                        echo '<td colspan="3" align="right"><b>Total</b></td>';

                        echo '<td align="right" style="vertical-align:middle;padding: 1px;font-size: 13px;"><b>'.number_format($units, 0, 0, ' ').' ('.number_format($units_rdnc, 0, 0, ' ').')</b></td>';

                        if($value['connet_cur'] == 'EUR')
                        {
                            echo '<td align="right" style="vertical-align:middle;padding: 1px;font-size: 13px;"><b><i class="fa fa-eur" style="color:#3333dd;"></i>' . number_format($connet, 2, '.', ' ') . '</b></td>';
                        }
                        else
                        {
                            echo '<td align="right" style="vertical-align:middle;padding: 1px;font-size: 13px;"><b><span style="color:#993333;">R </span>' . number_format($connet, 2, '.', ' ') . '</b></td>';
                        }
                        echo '<td align="right" style="vertical-align:middle;padding: 1px;font-size: 13px;"><b><span style="color:#993333;">R </span>'.number_format($profit, 2, '.', ' ').' (<span style="color:#993333;">R </span>'.number_format($profit_rdnc, 2, '.', ' ').')</b></td>';
                        echo '</tr>';

                        echo '<tr>';
                        echo '<td colspan="3" align="right"><b>Totals</b></td>';

                        $units_totals = $units + $units_rdnc;
                        echo '<td align="right" style="vertical-align:middle;padding: 1px;font-size: 13px;"><b>'.number_format($units_totals, 0, 0, ' ').'</b></td>';
                        echo '<td align="right" style="vertical-align:middle;padding: 1px;font-size: 13px;"><b></b></td>';
                        $profit_totals = $profit + $profit_rdnc;
                        echo '<td align="right" style="vertical-align:middle;padding: 1px;font-size: 13px;"><b><span style="color:#993333;">R </span>'.number_format($profit_totals, 2, 0, ' ').'</b></td>';
                        echo '</tr>';

                    ?>
                    </tfoot>
                </table>
            </div><!-- /.box-body -->
        </div><!-- /.box -->
    </div>
</div>

<?php }?>



<script type="text/javascript">



</script>
