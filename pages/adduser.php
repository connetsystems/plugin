<?php
/**
 * Include the header template which sets up the HTML
 *
 * Don't forget to include template_import_script before any Javascripting
 * Don't forget to include template footer.php at the bottom of the page as well
 */
//-------------------------------------------------------------
// Template
//-------------------------------------------------------------
TemplateHelper::setPageTitle('Add Users');
TemplateHelper::initialize();

$dsp = 'none';
$useError = '#aaaaaa';
$ErrorMsg = '';
$useErrorMsg = 'Enter new user here..';
$pasError = '#aaaaaa';
$pasErrorMsg = 'Enter password here..';
$fulError = '#aaaaaa';
$fulErrorMsg = 'e.g. Jane Smith';


//-------------------------------------------------------------
// Permissions
//-------------------------------------------------------------
if (isset($_GET['client'])) {

   $split = explode(' ', trim($_GET['client']));
   header('location: ' . $_SERVER['DOCUMENT_URI'] . '?sId=' . (integer) end($split));
   die();
}

if (LoginHelper::isSystemAdmin()) {

   $clients = getClients();
} else {
   /**
    * This is flawed, if an account has more than one reseller service, then
    * It will see all of their sub-services, even if they don't have permissions...
    */
   $account_id = LoginHelper::getCurrentAccountId();
   $clients = getServicesInAccountWithOutRid($account_id);
}

if (isset($_GET['sId'])) {

   $service_id = (integer) $_GET['sId'];

   $permission = false;
   foreach ($clients as $service) {

      $_service_id = (integer) $service['service_id'];
      if ($service_id === $_service_id) {
         $permission = true;
         break;
      }
   }

   if (!$permission) {

      header('location: ' . $_SERVER['DOCUMENT_URI']);
      die();
   }
}

//-------------------------------------------------------------
// Users
//-------------------------------------------------------------
if (isset($_GET['sId'])) {

   $sId = $_GET['sId'];
   $dsp = 'block';

   $users = getUsers($sId);
   $accDetails = getAccIdAccNServN($sId);

   foreach ($accDetails as $key => $value) {
      $aId = $value['account_id'];
      $accName = $value['account_name'];
      $serviceName = $value['service_name'];
   }
} 

if (isset($_GET['aId'])) {
   $aId = $_GET['aId'];
}

if (isset($_GET['aN'])) {
   $accName = $_GET['aN'];
}
/////////////////////////////////////////////////////////////////////////////////

if (isset($_GET['successMsg'])) {
   $SuccessMsg = 'You have successfully added a new user. ';
}

if (isset($_GET['upd'])) {
   $sId = $_GET['sId'];
   $validationCount = 0;

   if (isset($_GET['username']) && $_GET['username'] != '') {
      $userCheck = checkUserName($_GET['username']);
      if ($userCheck == 1) {
         $uN = $_GET['username'];
         $validationCount++;
      } else {
         //$err1 = 1;
         $useError = '#ffaaaa';
         $ErrorMsg = 'The username -' . $_GET['username'] . '- already exists. ';
      }
   } else {
      //$err1 = 1;
      $ErrorMsg = 'Please fill in all the required fields.';
      $useError = '#ffaaaa';
   }

   if (isset($_GET['password']) && $_GET['password'] != '') {
      $pW = $_GET['password'];
      $validationCount++;
   } else {
      //$err2 = 1;
      $ErrorMsg = 'Please fill in all the required fields.';
      $pasError = "#ffaaaa";
   }

   if (isset($_GET['fullname']) && $_GET['fullname'] != '') {
      $fN = $_GET['fullname'];
      $validationCount++;
   } else {
      //$err3 = 1;
      $ErrorMsg = 'Please fill in all the required fields.';
      $fulError = "#ffaaaa";
   }

   if ($validationCount == 3) {

      $uId = createNewUser($uN, $pW, $fN, $aId, $sId);
      addUser($uId, $sId);
      addRolesToUser($uId, 'Client');
      //createDefaultSkin($aId);
      $SuccessMsg = 'You have successfully added a new user. ';
   }
   //echo '<meta http-equiv="refresh" content= "0; URL=newusers.php?sId='.$sId.'&successMsg='.$SuccessMsg.' />';
}


if (isset($_GET['sId'])) {
   $sId = $_GET['sId'];
   $users = getUsers($sId);
   $dsp = 'block';
}
?>



<!-- Right side column. Contains the navbar and content of the page -->
<aside class="right-side">
   <section class="content-header">
      <h1>
         Add Users
      </h1>
   </section>
   <section class="content">
      <div class="row">
         <form action="adduser.php" class="col-md-12" method="get" id="newuser" name="newuser" style="border:none;">
            <div class="box box-connet" style="padding-bottom:1px; border-top-color:<?php echo $accRGB; ?>;">
               <div class="box-body">
                  <div class="callout callout-info" style="margin-bottom:0px;">
                     <h4>Add Users Instructions / Details</h4>
                     <p>This page will allow you to create NEW USERS and / or add EXISTING USERS.</p>
                  </div>
                  <div class="box-body">
                     <label>Client List</label>
                     <select class="form-control" name="client" form="clientList" OnChange="reloadOnSelect(this.value);">
<?php
if ($sId == '0') {
   
}
echo '<option SELECTED>Please select a client...</option>';
foreach ($clients as $key => $value) {
   $sIdt = $value['service_id'];
   if ($sId == $sIdt) {
      $accountName = $value['account_name'];
      $serviceName = $value['service_name'];
      echo "<option name='" . $sIdt . "' SELECTED>" . htmlentities($accountName . " - " . $serviceName . " - " . $sIdt) . "</option>";
   } else {
      echo "<option name='" . $sIdt . "'>" . htmlentities($value['account_name'] . " - " . $value['service_name'] . " - " . $sIdt) . "</option>";
   }
}
?>
                     </select>
                  </div>
               </div>
            </div>
            <div class="box box-connet" style="padding-bottom:1px; border-top-color:<?php echo $accRGB; ?>;display:<?php echo $dsp; ?>;">
               <div class="box-header">
                  <h3 class="box-title">User Details</h3>
               </div>
               <div class="box-body">
                  <div class="sidebar-form-group" style="padding-top:10px;">
                     <label>New Username</label>
                     <input type="text" class="form-control" name="username" id="username" maxlength='15' placeholder="<?php echo $useErrorMsg; ?>" value="" style="border:1px solid <?php echo $useError; ?>">
                     <br>
                     <label>Password</label>
                     <input type="text" class="form-control" name="password" id="password" maxlength='8' placeholder="<?php echo $pasErrorMsg; ?>" value="" style="border:1px solid <?php echo $pasError; ?>">
                     <br>
                     <label>Full Name</label>
                     <input type="text" class="form-control" name="fullname" id="fullname" placeholder="<?php echo $fulErrorMsg; ?>" value="" style="border:1px solid <?php echo $fulError; ?>">
                  </div>
                  <br>
                  <div class="sidebar-form-group" style="padding-top:10px;padding-bottom:20px;">
                     <!--input type="hidden" name="upd" id="upd" value="updating"-->
<?php
if (isset($ErrorMsg) && $ErrorMsg != '') {
   echo '<div class="callout callout-danger">';
   echo '<div style="width:80%;">';
   echo $ErrorMsg;
   echo '</div>';
   echo '<div style="float:right; margin-top:-22px">';
   echo '<i class="fa fa-exclamation-triangle" style="color:#dFb5b4; font-size:20pt;"></i>';
   echo '</div>';
   echo '</div>';
} elseif (isset($SuccessMsg)) {
   echo '<div class="callout callout-success">';
   echo '<div style="width:80%;">';
   echo $SuccessMsg;
   echo '</div>';
   echo '<div style="float:right; margin-top:-22px">';
   echo '<i class="fa fa-check" style="color:#5cb157; font-size:20pt;"></i>';
   echo '</div>';
   echo '</div>';
}
?>
                     <a class="btn btn-block btn-social btn-dropbox " onclick="newuser.submit();" style="background-color: <?php echo $accRGB; ?>; color: #ffffff; border: 2px solid; border-radius: 6px !important; float:left; width: 115px; margin-top:0px;">
                        <i class="fa fa-user"></i>Add User
                     </a>
                  </div>
                  <br>

               </div>
            </div>
            <div class="box box-connet" style="padding-bottom:1px; border-top-color:<?php echo $accRGB; ?>;display:<?php echo $dsp; ?>;">

               <div class="sidebar-form-group" style="padding-top:10px;">
                  <div class="box-body table-responsive">
                     <table class="table table-hover">
                        <tr>
                           <th>Username</th>
                           <th>Password</th>
                           <th>Status</th>
                        </tr>
<?php
if (isset($users) && $users != '') {
   foreach ($users as $key => $value) {
      if ($value['user_username'] === $value['service_login']) {
         $rowColor = ' style="background-color:#f4e8c6;"';
      } else {
         $rowColor = '';
      }
      echo '<tr' . $rowColor . '>';
      echo '<td>' . $value['user_username'] . '</td>';
      echo '<td>******</td>';

      if ($value['user_status'] === "ENABLED") {
         echo '<td><i class="fa fa-check" style="color:#00ff00;"></i>&nbsp;&nbsp;ENABLED</td>';
      } else {
         echo '<td><i class="fa fa-close" style="color:#ff0000;"></i>&nbsp;&nbsp;DISABLED</td>';
      }
      echo '</tr>';
   }
}
?>
                        <input type="hidden" name="aN" value="<?php echo $accName; ?>">
                        <input type="hidden" name="sN" value="<?php echo $serviceName; ?>">
                        <input type="hidden" name="aId" value="<?php echo $aId; ?>">
                        <!--input type="hidden" name="uId" value="<?php //echo $uId;     ?>"-->
                        <input type="hidden" name="sId" value="<?php echo $sId; ?>">
                        <input type="hidden" name="upd" value="upd">
                     </table>
                  </div>
               </div>
            </div>
         </form>
      </div>
   </section>
</aside>

<!--  THE JAVASCRIPT IMPORT ALWAYS APPEARS BEFORE LOCAL JAVASCRIPT AT THE BOTTOM OF THE PAGE -->
<?php include("template_import_script.php"); //must import all scripts first    ?>
<!-- END JAVASCRIPT IMPORT -->

<script type="text/javascript">

   $(".loader").fadeOut("slow");
   $(".loaderIcon").fadeOut("slow");

   function reloadOnSelect(name)
   {
      $(".loader").fadeIn("slow");
      $(".loaderIcon").fadeIn("slow");
      window.location = "adduser.php?client=" + name;
   }
</script>

<!-- Template Footer -->