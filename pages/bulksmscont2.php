<?php
/**
 * Include the header template which sets up the HTML
 *
 * Don't forget to include template_import_script before any Javascripting
 * Don't forget to include template footer.php at the bottom of the page as well
 */
$headerPageTitle = "Bulk SMS Continued"; //set the page title for the template import
TemplateHelper::initialize();

//set_time_limit(360);
//    ini_set('memory_limit', '8192M');
set_time_limit(3000);

$singleView = 'block;';

if ($_SESSION['serviceId'] != '' && $_SESSION['resellerId'] != '') {
   if ($_SESSION['serviceId'] != $_SESSION['resellerId']) {
      //if(!isset($_GET['client']))
      {
         $_POST['client'] = $_SESSION['accountName'] . ' - ' . $_SESSION['serviceName'] . " - " . $_SESSION['serviceId'];
         //echo "<br><br><br><br>+---------------------->".$_GET['client'];
         $singleView = 'none;';
         // $noShow = 1;
      }
   } elseif ($_SESSION['serviceId'] == $_SESSION['resellerId']) {
      $singleView = 'block;';
      //$noShow = 1;
   }
   /* elseif($_SESSION['resellerId'] == '')
     {

     $noShow = 1;
     } */
}

if ($_POST['status'] != 'Scheduled') {
   $_POST['sTime'] = '0000-00-00';
   /*
     echo '<br><br><br>';
     echo '<pre>';
     print_r($_POST);
     echo '</pre>';
    */
}

//echo '<br>separ='.
$delimiter = $_POST['separ'];
;


/* echo '<pre>';
  print_r($_FILES);
  echo '</pre>'; */


if (isset($_FILES["uploadCSV"]) && $_FILES["uploadCSV"]["error"] != '4') {
   $maxSize = 1024 * 1024 * 1024 * 1024;

   $target_dir = "../tempFiles/";
   if (!is_dir($target_dir)) {
      mkdir($target_dir, 0777);
   }

   $target_file = $target_dir . basename($_FILES["uploadCSV"]["name"]);

   $imageFileType = pathinfo($target_file, PATHINFO_EXTENSION);

   if (!empty(($_FILES["uploadCSV"]["tmp_name"]))) {
      $uploadOk = 1;
   }

   /* if(file_exists($target_file)) 
     {
     echo "Sorry, file already exists.";
     $uploadOk = 0;
     } */

   if ($_FILES["uploadCSV"]["size"] > $maxSize) {
      //   echo "Sorry, your file is too large.";
      $uploadOk = 0;
   }

   if ($imageFileType != "csv") {
      //   echo "Sorry, only JPG, JPEG, PNG & GIF files are allowed.";
      $uploadOk = 0;
   }

   if (isset($_POST['template']) && $_POST['template'] != 'Enter SMS text') {
      $_POST['template'] = substr($_POST['template'], 10);
   }

   if ($uploadOk == 1) {
//            ini_set('memory_limit', '-1');
      $ul = move_uploaded_file($_FILES["uploadCSV"]["tmp_name"], $target_file);

      $_SESSION['filez'] = $_FILES['uploadCSV'];
   }
   //echo '<br><br><br><br>*-------------------------------------------------><-=--';
} else {
   $target_dir = "../tempFiles/";
   $target_name = date("YmdHis") . '.csv';
   $target_file = $target_dir . $target_name;
   $fh = fopen($target_file, 'w') or die("Can't create file");
   //echo '<br><br><br><br>*------------------------------------------------->'.$fh;
   $ul = move_uploaded_file($_FILES["uploadCSV"]["tmp_name"], $target_file);
   $_SESSION['filez'] = $_FILES['uploadCSV'];
   /* $target_file = $target_dir.basename($_FILES["uploadCSV"]["name"]);
     $_SESSION['filez'] = $_FILES['uploadCSV']; */
}

/* echo "<br><br><br><pre>";
  print_r($_POST);
  echo "</pre>"; */
/* if(isset($_POST['client']))
  {
  $_GET['client'] = $_POST['client'];


  if(isset($_FILES["uploadCSV"]))
  {
  //echo "<br>YES!";
  }
  } */
if (isset($_POST['contactList']) && $_POST['contactList'] != '') {
   //$contactsToSend = getContactInfoNumOnly($_POST['contactList']);
   $conlistID = $_POST['contactList'];
} else {
   $conlistID = 0;
}

if (isset($_POST['client'])) {
   $cl = $_POST['client'];
   $clp = strrpos($cl, ' ');
   $cl = trim(substr($cl, $clp, strlen($cl)));
   $dsp = 'block;';

   if (isset($_POST['camId']) && $_POST['camId'] != "") {
      //echo "<br>this test";
      $dsp2 = 'block;';
      $temps = getTempData($_POST['camId']);
   } else {
      $dsp2 = 'none;';
   }

   //$conts = getContactListClient($cl);
   /* foreach ($camps as $key => $value) 
     {
     echo '<pre>';
     print_r($value);
     echo '</pre>';
     # code...
     } */
   //$dir = '../img/routes';
   //$serProviders = scandir($dir);
} else {
   $_POST['client'] = "0 - Please select a client...";
   $cl = strstr($_POST['client'], ' ', true);
   $dsp = 'none;';
   $dsp2 = 'none;';
}

if (isset($_SESSION['accountId'])) {
   $acId = $_SESSION['accountId'];
} else {
   $acId = null;
}
$clients = getServicesInAccount($acId);

////////////////////////////////////////////////
////////////////////////////////////////////////
////////////////////////////////////////////////



/* $sql = 'INSERT INTO `core_service_payment` (service_id, user_id, payment_value, payment_description,payment_previous_balance) 
  VALUES (:service_id, :user_id, :payment_value, :payment_description,:payment_previous_balance)';

  $sql = mysql_encode_param_array($sql, array(
  'service_id' => $selected_service->service_id,
  'user_id' => $_SESSION['auth']['user']->user_id,
  'payment_value' => $request->payment_value,
  'payment_description' => $request->description,
  'payment_previous_balance' => $selected_service->service_credit_available,
  )); */
?>

<aside class="right-side">
   <section class="content-header">
      <h1>
         Bulk SMS Continued
         <!--small>Control panel</small-->
      </h1>
   </section>

   <section class="content">
      <div class="box box-warning" style="border-top-color:<?php echo $accRGB; ?>;display:block">
         <div class="box-body" style="margin-bottom:0px;padding-bottom:1px;">
            <div class="callout callout-warning" id="summary" style="display:block;">
               <h4 class="box-title">
                  Summary
               </h4>
               <p>Below is a summary of the previous page.</p>
            </div>
            <div class="box-body">
               <p><b>Service:</b> <?php echo $_POST['client']; ?></p>
               <p><b>Campaign:</b> <?php echo $_POST['campaign']; ?></p>
               <p><b>Reference:</b> <?php echo $_POST['refer']; ?></p>
               <p><b>Status:</b> <?php echo $_POST['status']; ?></p>
            </div>
            <form role="form" id="bulkData" form="batch" enctype="multipart/form-data" action="confirmbulkfile.php" method="POST">

               <div class="form-group">
                  <div class="form-group" style="margin-bottom:2px;">
                     <label>Template List</label>
                     <div class="callout callout-danger" id="tempError" style="display:none;">
                        <p>Please select from the template list!</p>
                     </div>
                     <select class="form-control" name="template" id="temper" form="bulkData" OnChange="checkTemplate(this.value)">
                        <?php
                        echo "<option>Enter SMS text</option>";
                        foreach ($temps as $key => $value) {
                           echo "<option>" . $value['template_description'] . ' > ' . $value['template_content'] . "</option>";
                        }
                        ?>
                     </select>
                  </div>

                  <div class="callout callout-danger" id="smsError" style="display:none;">
                     <p>SMS text cannot be empty!</p>
                  </div>

                  <div class="form-group">
                     <label>Enter SMS text here</label>
                     <textarea class="form-control" id="smsText" name="smstext" rows="3" placeholder="SMS text here.."></textarea>
                  </div>
                  <label id="counter"></label>
               </div>

               <div class="form-group" id="manualNumsForm" style='margin-top:0px'>
                  <div class="box box-warning" style="border-top-color:<?php echo $accRGB; ?>;display:block;" >
                     <div class="checkbox" style="padding-bottom:10px;">
                        <label style="padding-left:15px;">
                           <input type="checkbox" id="mN" name="moreN">
                           Add More Numbers
                        </label>
                     </div>
                     <div class="box-body" id="manualNums" style="padding-bottom:0px;display:none;">
                        <div class="form-group">
                           <label>Enter up to 10 recipients manually (optional) e.g. 27821234582,27831234567</label>
                           <textarea class="form-control" onkeypress="return isNumberKey(event)" rows="3" name="numbers" id="numbers" maxchars="120" placeholder=""></textarea>
                           <p class="help-block">Separate multiple MSISDNs with comma-, characters.
                              Include country code. (27821234582,27831234567)</p>
                        </div>
                     </div>
                  </div>
               </div>


               <input type="hidden" name="client" id="client" value="<?php echo $_POST['client']; ?>">
               <input type="hidden" name="serviceId" value="<?php echo $cl; ?>">
               <input type="hidden" name="camCode" value="<?php echo $_POST['camCode']; ?>">
               <input type="hidden" name="camId" value="<?php echo $_POST['camId']; ?>">
               <input type="hidden" name="tFile" value="<?php echo $target_file; ?>">
               <input type="hidden" name="refer" value="<?php echo $_POST['refer']; ?>">
               <input type="hidden" name="status" value="<?php echo $_POST['status']; ?>">
               <input type="hidden" name="sTime" value="<?php echo $_POST['sTime']; ?>">
               <input type="hidden" name="delimiter" value="<?php echo $_POST['separ']; ?>">
               <input type="hidden" name="contactListId" value="<?php echo $conlistID; ?>">
               <input type="hidden" name="smsCount" id="smsCount" value="">

            </form>
         </div>
         <div class="box-body" style="display:<?php echo $dsp2; ?>;margin-bottom:5px;margin-top:-15px;">
            <div>
               <a onclick="backBtn();" class="btn btn-block btn-social btn-dropbox " style="background-color:<?php echo $accRGB; ?>; color: #ffffff; border: 2px solid; border-radius: 6px !important; float:left; width: 92px; margin-top:-13px;">
                  <i class="fa fa-arrow-left"></i>Back
               </a>
            </div>
            <div class="form-group">
               <a onclick="validateBulk();" class="btn btn-block btn-social btn-dropbox " style="background-color:<?php echo $accRGB; ?>; border: 2px solid; border-radius: 6px !important; float:left; width: 170px; margin-top:-13px;">
                  <i class="fa fa-save"></i>Confirm Bulk SMS
               </a>
            </div>
         </div>
      </div>
   </section>
</aside>

<!--  THE JAVASCRIPT IMPORT ALWAYS APPEARS BEFORE LOCAL JAVASCRIPT AT THE BOTTOM OF THE PAGE -->
<?php include("template_import_script.php"); //must import all scripts first  ?>
<!-- END JAVASCRIPT IMPORT -->

<script type="text/javascript">
   $('#scheduledTimer').daterangepicker(
           {
              singleDatePicker: true,
              timePicker: true,
              timePickerIncrement: 5,
              timePicker12Hour: false,
              //format: 'DD-MM-YYYY h:mm A',
              timePickerSeconds: false,
              minDate: Date.now

           },
           function (start, end, label)
           {
              /*alert(label);
               alert(moment(end, 'MMM DD YYYY, h:mmA'));
               var years = moment(start, 'MMM DD YYYY, h:mmA');
               alert(years);*/
              //alert(Date.now);
              $('#scheduledTimer').html(end.format("DD-MM-YYYY HH:mm"));
              //$('#scheduledTimer').html(moment(start));
           }
   );
</script>

<script type="text/javascript">
   function backBtn()
   {
      var client = "<?php echo $_POST['client']; ?>";
      window.location = "../pages/bulksms.php?client=" + client;
   }
</script>

<script type="text/javascript">
   var area = document.getElementById("smsText");
   var message = document.getElementById("counter");
   //var maxLength = 50;
   var checkLength = function ()
   {
      var areaCheck = area.value.indexOf('{');
      if (areaCheck != -1)
      {
         //document.getElementById("contactListForm").style.display = 'none';
         document.getElementById("manualNumsForm").style.display = 'none';
      } else
      {
         //document.getElementById("contactListForm").style.display = 'block';   
         document.getElementById("manualNumsForm").style.display = 'block';
      }

      /*var contCh = <?php echo $z; ?>;
       if(contCh == 0)
       {
       //document.getElementById("contactListForm").style.display = 'none';
       }*/

      if (area.value.length == 0)
      {
         message.innerHTML = "0 characters, 0 SMS";
      } else
      {
         var tot = 0;

         var str = area.value;
         var newLength = area.value.length;
         var count = occurrences(str, '[', 'false');
         if (count != 0)
         {
            newLength = newLength + (2 * count) - count;
         }
         count = occurrences(str, ']', 'false');
         if (count != 0)
         {
            newLength = newLength + (2 * count) - count;
         }
         count = occurrences(str, '^', 'false');
         if (count != 0)
         {
            newLength = newLength + (2 * count) - count;
         }
         count = occurrences(str, '|', 'false');
         if (count != 0)
         {
            newLength = newLength + (2 * count) - count;
         }
         count = occurrences(str, '~', 'false');
         if (count != 0)
         {
            newLength = newLength + (2 * count) - count;
         }
         count = occurrences(str, '\\', 'false');
         if (count != 0)
         {
            newLength = newLength + (2 * count) - count;
         }
         count = occurrences(str, '\u20AC', 'false');
         if (count != 0)
         {
            newLength = newLength + (2 * count) - count;
         }
         count = occurrences(str, '\u000A', 'false');
         if (count != 0)
         {
            newLength = newLength + (2 * count) - count;
         }
         count = occurrences(str, '\u21A1', 'false');
         if (count != 0)
         {
            newLength = newLength + (2 * count) - count;
         }
         count = occurrences(str, '{', 'false');
         if (count != 0)
         {
            newLength = newLength + (2 * count) - count;
         }
         count = occurrences(str, '}', 'false');
         if (count != 0)
         {
            newLength = newLength + (2 * count) - count;
         }

         if (newLength <= 160)
         {
            tot = Math.ceil((newLength) / 160);
         } else
         {
            tot = Math.ceil((newLength) / 153);
         }

         message.innerHTML = newLength + " characters, " + tot + " SMS";
         var elem = document.getElementById("smsCount");
         elem.value = tot;
      }
   }
   setInterval(checkLength, 10);
</script>

<script type="text/javascript">

   function occurrences(string, subString, allowOverlapping)
   {
      string += "";
      subString += "";
      if (subString.length <= 0)
         return string.length + 1;

      var n = 0, pos = 0;
      var step = (allowOverlapping) ? (1) : (subString.length);

      while (true)
      {
         pos = string.indexOf(subString, pos);
         if (pos >= 0) {
            n++;
            pos += step;
         } else
            break;
      }
      return(n);
   }

   function isNumberKey(evt)
   {
      var charCode = (evt.which) ? evt.which : evt.keyCode;

      if (charCode == 44)
         return true;

      /*alert(charCode);*/
      if (charCode == 46)
         return false;

      if (charCode != 46 && charCode > 31
              && (charCode < 48 || charCode > 57))
         return false;

      return true;
   }

   /*function getCamp()
    {
    var clientOp = clientSel.options;
    var name     = clientOp[clientOp.selectedIndex].value;
    
    var options = camp.options;
    var id      = options[options.selectedIndex].id;
    
    window.location = "bulksms.php?client=" + name + "&campId=" + id;
    //var value   = options[options.selectedIndex].value;
    }*/

   $('input').on('ifChecked', function (event)
   {
      var checkedId = $(this, 'input').attr('id');
      //alert(checkedId);
      if (checkedId == 'mN')
      {
         document.getElementById('manualNums').style.display = 'block';
      }

      /*if(checkedId == 'cL')
       {
       document.getElementById('contactList').style.display = 'block';
       }*/
      //alert(event + ' callback');
   });

   $('input').on('ifUnchecked', function (event)
   {
      var checkedId = $(this, 'input').attr('id');
      //alert(checkedId);
      if (checkedId == 'mN')
      {
         document.getElementById('manualNums').style.display = 'none';
      }

      /*if(checkedId == 'cL')
       {
       document.getElementById('contactList').style.display = 'none';
       }*/
      //alert(event + ' callback');
   });

   /*function checkNumbersList()
    {
    alert("Nunm");
    }
    
    function checkContactList()
    {
    alert("Cont");
    }*/

   function checkDelStatus(v)
   {
      if (v == 'Scheduled')
      {
         document.getElementById('schedulerBox').style.display = 'block';
      } else
      {
         document.getElementById('schedulerBox').style.display = 'none';
      }
   }

   function checkTemplate(tempVal)
   {
      var freeTextBox = document.getElementById('smsText');
      if (tempVal == 'Enter SMS text')
      {
         freeTextBox.value = '';
         freeTextBox.disabled = false;
      } else
      {
         freeTextBox.value = '';
         var n = tempVal.indexOf(">");
         tempVal = tempVal.substr(n + 1);
         tempVal = tempVal.trim();
         freeTextBox.value = tempVal;
         freeTextBox.disabled = true;
      }
   }

   function validateBulk()
   {
      var smsErr = document.getElementById('smsError');
      var smstext = document.getElementById('smsText').value;
      var tempErr = document.getElementById('tempError');
      var tempSel = temper.options;
      var tempSelect = tempSel[tempSel.selectedIndex].value;
      var validSubmit = 0;

      if (tempSelect == "Please Select")
      {
         tempErr.style.display = 'block';
      } else
      {
         tempErr.style.display = 'none';
         validSubmit++;
      }

      if (tempSelect != "Please Select" && tempSelect != "Enter SMS text")
      {
         document.getElementById('smsText').value = '';
         //alert('12' + document.getElementById('smsText').value);
      }

      if (smstext.length > 0)
      {
         //alert("hi+" + fileInput);
         smsErr.style.display = 'none';
         validSubmit++
         //bulkData.submit();
      } else
      {
         smsErr.style.display = 'block';
         //error
      }

      //alert("hi+" + validSubmit);
      if (validSubmit >= 2)
      {
         $(".loader").fadeIn("slow");
         $(".loaderIcon").fadeIn("slow");
         bulkData.submit();
      }
      /*else
       {}*/
   }
</script>
<script type="text/javascript">
   function reloadOnSelect(name)
   {
      $(".loader").fadeIn("slow");
      $(".loaderIcon").fadeIn("slow");
      window.location = "bulksms.php?client=" + name;
   }
</script>
<script type="text/javascript">
   $(window).load(function () {
      $(".loader").fadeOut("slow");
      $(".loaderIcon").fadeOut("slow");
   })
</script>

<!-- Template Footer -->

