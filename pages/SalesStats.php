<?php
/**
 * Include the header template which sets up the HTML
 *
 * Don't forget to include template_import_script before any Javascripting
 * Don't forget to include template_footer.php at the bottom of the page as well
 */
//set the page title for the template import

$headerPageTitle = 'Sales Stats';
TemplateHelper::initialize();

if (isset($_GET['startDate']) && isset($_GET['endDate'])) {
   $startDate = $_GET['startDate'];
   $endDate = $_GET['endDate'];
} else {
   $startDate = date('Y-m-d 00:00:00');
   $endDate = date('Y-m-d 23:59:59');
}

$colData = getSalesPersonStats($startDate, $endDate);
//$colMonth = getServiceMonthStats();

$totalSends = 0;

foreach ($colData as $key => $value) {
   $totalSends += $value['units'];
}

$totalSends = number_format($totalSends, 0, '.', ' ');

//Get Rand Euro exchange rate
function get_currency($from_Currency, $to_Currency, $amount) {

   $amount = urlencode($amount);
   $from_Currency = urlencode($from_Currency);
   $to_Currency = urlencode($to_Currency);

   $url = "http://www.google.com/finance/converter?a=$amount&from=$from_Currency&to=$to_Currency";

   $ch = curl_init();
   $timeout = 0;
   curl_setopt($ch, CURLOPT_URL, $url);
   curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);

   curl_setopt($ch, CURLOPT_USERAGENT, "Mozilla/4.0 (compatible; MSIE 8.0; Windows NT 6.1)");
   curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, $timeout);
   $rawdata = curl_exec($ch);
   curl_close($ch);
   $data = explode('bld>', $rawdata);
   $data = explode($to_Currency, $data[1]);

   return round($data[0], 2);
}

// Call the function to get the currency converted
$exrate = get_currency('EUR', 'ZAR', 1);


//*************************************
$year = date('Y');

$monthsStart = '01';
$monthsEnd = ['31', '28', '31', '30', '31', '30', '31', '31', '30', '31', '30', '31'];
$monthsArr = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'];

$sD = "2013-01-01";
$eD = date('Y-01-01', strtotime('+1 year'));

$sqlMonthData = getMonthTotalsSalesPerson($sD, $eD);

$yearArr = array();

foreach ($sqlMonthData as $key => $value) {
   if (!in_array($value['Year'], $yearArr)) {
      array_push($yearArr, $value['Year']);
   }
}
//Send History Tab
$profitData = getSalesPersonProfit();
?>

<aside class="right-side">
   <!-- Content Header (Page header) -->
   <section class="content-header">
      <h1>
         <?php echo 'Corrie Henn Dashboard - 1 Euro - ZAR : R ' . $exrate; ?>
      </h1>

      <div class="row col-lg-3">
         <form action="viewservice.php" method="get" id="clientList" class="sidebar-form" style="border:0px;">
            <select class="form-control" name="client" form="sales">
               <?php
               if ($_SESSION['userId'] == '479') {
                  echo '<option SELECTED>Please select a Sales Person</option>';
                  echo "<option value='3'>Brendan White</option>";
                  echo "<option value='563'>Corrie Henn</option>";
                  echo "<option value='1179'>Customer Service</option>";
                  echo "<option value='15'>Frank de Graaf</option>";
                  echo "<option value='1109'>Ian Flanagan</option>";
                  echo "<option value='555'>Madre Goosen</option>";
                  echo "<option value='745'>Raymond Esson</option>";
                  echo "<option value='92'>Thinus Theart</option>";
               }
               ?>
            </select>
         </form>
      </div>
   </section>
   <!-- Content Boxes -->
   <section class="content col-lg-12">
      <div class="box box-connet" style="padding-top:10px;padding-left:10px;padding-right:10px;padding-bottom:100px;">
         <div class="row col-lg-12">
            <div class="col-lg-2 col-xs-6">
               <!-- small box -->
               <div class="small-box bg-connetBlue">
                  <div class="inner">
                     <h3>
                        <div id="sentDiv" style="color:#ffffff;font-size:20pt;">
                           &nbsp;
                        </div>
                     </h3>

                  </div>

                  <a href="#" class="small-box-footer">
                     SENT
                  </a>
               </div>
            </div><!-- ./col -->
            <div class="col-lg-2 col-xs-6">
               <!-- small box -->
               <div class="small-box bg-connetYellow">
                  <div class="inner">
                     <h3>
                        <div id="pendDiv" style="color:#ffffff;font-size:20pt;">
                           &nbsp;
                        </div>
                     </h3>
                     <!--p style="color:#ffffff">
                        &nbsp;
                     </p-->
                  </div>
                  <!--div class="icon" id="pendDiv2" style="color:#ffffff;font-size:26pt;padding-bottom:15px;display:block;">
                      0%
                  </div-->
                  <a href="#" class="small-box-footer">
                     PENDING <!--div id="pendAve"></div-->
                  </a>
               </div>
            </div><!-- ./col -->
            <div class="col-lg-2 col-xs-6">
               <!-- small box -->
               <div class="small-box bg-connetGreen">
                  <div class="inner">
                     <h3>
                        <div id="deliDiv" style="color:#ffffff;font-size:20pt;">
                           &nbsp;
                        </div>
                     </h3>
                     <!--p style="color:#ffffff">
                        &nbsp;
                     </p-->
                  </div>
                  <!--div class="icon" id="deliDiv2" style="color:#ffffff;font-size:26pt;padding-bottom:15px;display:block;">
                      0%
                  </div-->
                  <a href="#" class="small-box-footer">
                     DELIVERED <!--div id="deliAve"></div-->
                  </a>
               </div>
            </div><!-- ./col -->
            <div class="col-lg-2 col-xs-6">
               <!-- small box -->
               <div class="small-box bg-red">
                  <div class="inner">
                     <h3>
                        <div id="failDiv" style="color:#ffffff;font-size:20pt;">
                           &nbsp;
                        </div>
                     </h3>
                     <!--p style="color:#ffffff">
                         &nbsp;
                     </p-->
                  </div>
                  <!--div class="icon" id="failDiv2" style="color:#ffffff;font-size:26pt;padding-bottom:15px;display:block;">
                      0%
                  </div-->
                  <a href="#" class="small-box-footer">
                     FAILED <!--div id="failAve"></div-->
                  </a>
               </div>
            </div><!-- ./col -->
            <div class="col-lg-2 col-xs-6">
               <!-- small box -->
               <div class="small-box bg-connetRed">
                  <div class="inner">
                     <h3>
                        <div id="rejeDiv" style="color:#ffffff;font-size:20pt;">
                           &nbsp;
                        </div>
                     </h3>
                     <!--p style="color:#ffffff">
                         &nbsp;
                     </p-->
                  </div>
                  <!--div class="icon" id="rejeDiv2" style="color:#ffffff;font-size:26pt;padding-bottom:15px;display:block;">
                      0%
                  </div-->
                  <a href="#" class="small-box-footer">
                     REJECTED <!--div id="rejeAve"></div-->
                  </a>
               </div>
            </div>
            <div class="col-lg-2 col-xs-6">
               <!-- small box -->
               <div class="small-box bg-aqua">
                  <div class="inner">
                     <h3>
                        <div id="moDiv" style="color:#ffffff;font-size:20pt;">
                           &nbsp;
                        </div>
                     </h3>
                     <!--p style="color:#ffffff">
                         &nbsp;
                     </p-->
                  </div>
                  <!--div class="icon" id="rejeDiv2" style="color:#ffffff;font-size:26pt;padding-bottom:15px;display:block;">
                      0%
                  </div-->
                  <a href="#" class="small-box-footer">
                     MOs
                  </a>
               </div>
            </div><!-- ./col -->
         </div><!-- /.row -->
      </div>
   </section>


   <section class="content col-lg-12" >
      <!-- Nav tabs -->
      <ul class="nav nav-tabs" role="tablist">
         <li role="presentation" class="active"><a href="#traffic_totals" aria-controls="traffic_totals" role="tab" data-toggle="tab">Traffic Totals</a></li>
         <li role="presentation"><a href="#history" aria-controls="history" role="tab" data-toggle="tab">Send History</a></li>
         <li role="presentation"><a href="#client_stats" aria-controls="client_stats" role="tab" data-toggle="tab">Client Stats</a></li>
      </ul>
      <!-- Tab panes -->
      <div class="tab-content">
         <div role="tabpanel" class="tab-pane active" id="traffic_totals">
            <section class="content" >
               <div class="box box-connet" style="padding-bottom:1px; border-top-color:<?php echo $accRGB; ?>;">
                  <div id="reportrange"  onmouseover="" style="cursor: pointer;padding-top:5px;padding-left:5px;">
                     <i class="fa fa-calendar fa-lg"></i>
                     <span style="font-size:15px">&nbsp;&nbsp;<?php echo $startDate; ?>&nbsp;&nbsp;-&nbsp;&nbsp;<?php echo $endDate; ?>&nbsp;&nbsp;</span><b class="caret"></b>
                  </div>
                  <br>
                  <br>
                  <div class="callout callout-success" style="margin-bottom:10px;">
                     <i class="fa fa-thumbs-o-up"></i>
                     &nbsp;Total sent for date range: <b><?php echo $totalSends; ?></b>
                  </div>

                  <div class="box-body table-responsive">
                     <table id="example2" class="table table-bordered table-striped dataTable table-hover">
                        <thead>
                           <tr>
                              <!--th>#</th-->
                              <th style="width:300px;">Acc</th>
                              <th style="width:300px;">Service</th>
                              <th style="width:80px;">Units</th>
                              <th style="width:30px;">Status</th>
                              <th style="width:30px;">Type</th>
                              <th style="width:60px;">Balance</th>
                           </tr>
                        </thead>
                        <tbody>
                           <?php
                           if (isset($colData) && !empty($colData)) {
                              $total = 0;
                              foreach ($colData as $key => $value) {

                                 echo '<tr>';
                                 echo '<td style="vertical-align:middle;text-align:left;width:300px;">' . $value['account_name'] . '</td>';
                                 echo '<td style="vertical-align:middle;text-align:left;width:300px;">' . $value['service_name'] . '</td>';
                                 echo '<td style="vertical-align:middle;text-align:right;"><b>' . number_format($value['units'], 0, '', ' ') . '</b></td>';

                                 if ($value['service_status'] == 'ENABLED') {
                                    echo '<td><center><span class="label label-success" title="ENABLED">ENABLED</span></center></td>';
                                 } elseif ($value['service_status'] == 'DISABLED') {
                                    echo '<td><center><span class="label label-danger" title="DISABLED">DISABLED</span></center></td>';
                                 }

                                 if ($value['service_credit_billing_type'] == 'POSTPAID') {
                                    echo '<td><center><span class="label label-primary" title="POSTPAID">POSTPAID</span></center></td>';
                                 } elseif ($value['service_credit_billing_type'] == 'PREPAID') {
                                    echo '<td><center><span class="label label-info" title="PREPAID">PREPAID</span></center></td>';
                                 }


                                 $value['service_credit_available'] = $value['service_credit_available'] / 10000;
                                 //$totalSends += $value['Balance'];
                                 $value['service_credit_available'] = number_format($value['service_credit_available'], 2, '.', ' ');
                                 if (substr($value['service_credit_available'], 0, 1) != '-') {
                                    echo '<td><center><span class="label label-success" title="' . $value['service_credit_available'] . '">' . $value['service_credit_available'] . '</span></center></td>';
                                 } else {
                                    echo '<td><center><span class="label label-danger" title="' . $value['service_credit_available'] . '">' . $value['service_credit_available'] . '</span></center></td>';
                                 }
                                 echo '</tr>';

                                 $total += $value['units'];
                              }
                           }
                           ?>
                        </tbody>
                        <tfoot>
                           <tr>
                              <th>Acc</th>
                              <th>Service</th>
                              <th>Units</th>
                              <th>Status</th>
                              <th>Type</th>
                              <th>Balance</th>
                           </tr>
                        </tfoot>
                     </table>
                  </div><!-- /.box-body -->
               </div><!-- /.box -->
            </section>
         </div>
         <!--//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////// -->
         <!--//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////// -->
         <!--//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////// -->
         <!--//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////// -->
         <!--//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////// -->
         <div role="tabpanel" class="tab-pane" id="history">
            <?php
            echo '<section class="content col-lg-12" style="margin-top:0px;">';
            echo '<div class="box box-connet" style="padding-top:1px; border-top-color:<?php echo $accRGB; ?>;">';
            echo '<div class="box-body">';
            echo '<div class="box-body table-responsive no-padding">';
            echo '<table id="example2"  class="table table-hover">';
            echo '<thead>';
            echo '<tr>';
            echo '<th>';
            echo 'Year';
            echo '</th>';
            for ($i = 0; $i < count($monthsArr); $i++) {
               echo '<th style="text-align:right">' . $monthsArr[$i] . '</th>';
            }
            echo '<th style="text-align:right">Year Total</th>';
            echo '<th style="text-align:right">Month Average</th>';
            echo '</tr>';
            echo '</thead>';
//echo '<td><center>'.$monthsArr[$i].'</center></td>';
            echo '<tbody>';

            for ($i = 0; $i < count($yearArr); $i++) {
               echo '<tr>';
               echo '<td>';
               echo $yearArr[$i];
               echo '</td>';
               $yearTotal = 0;
               for ($ij = 0; $ij < count($monthsArr); $ij++) {
                  $cont = 0;
                  foreach ($sqlMonthData as $key => $value) {
                     if ((substr($value['month'], 0, 3) == $monthsArr[$ij]) && ($value['Year'] == $yearArr[$i])) {
                        $yearTotal += $value['cdr_record_units'];
                        $monthTemp = (($key + 1) - ($i * 12)) + 1;
                        echo '<td style="cursor: pointer;text-align:right" >' . number_format($value['cdr_record_units'], 0, '.', ' ') . '</td>';
                        $cont++;
                     }
                  }
                  if ($cont == 0) {
                     echo '<td style="text-align:right">0</td>';
                     //echo '';
                  }
               }
               echo '<td style="text-align:right">' . number_format($yearTotal, 0, '.', ' ') . '</td>';
               if (date('n') != 12 && $yearArr[$i] == date('Y')) {
                  echo '<td style="text-align:right">' . number_format(($yearTotal / date('n')), 0, '.', ' ') . '</td>';
               } else {
                  echo '<td style="text-align:right">' . number_format(($yearTotal / 12), 0, '.', ' ') . '</td>';
               }
               echo '<tr>';
            }
//echo '<tr>';
//echo '</tr>';

            echo '</tbody>';
            echo '</table>';
            echo '</div>';
            echo '</div>';
            echo '</div>';
            echo '</section>';
            ?>


            <section class="content col-lg-12" >
               <div class="box box-connet" style="padding-top:1px; border-top-color:<?php echo $accRGB; ?>;">
                  <div class="box-body">
                     <div class="box-body table-responsive no-padding">
                        <table id="example2" class="table table-bordered table-striped dataTable table-hover">
                           <thead>
                              <tr>
                                 <!--th>#</th-->
                                 <th style="width:300px;">Acc</th>
                                 <th style="width:300px;">Service</th>
                                 <th style="width:80px;">Units</th>
                                 <th style="width:80px;">Client</th>
                                 <th style="width:80px;">Profit</th>
                                 <th style="width:80px;text-align:right;">Profit %</th>
                              </tr>
                           </thead>
                           <tbody>
                              <?php
                              if (isset($profitData) && !empty($profitData)) {

                                 foreach ($profitData as $key => $value) {

                                    echo '<tr>';
                                    if (number_format((($value['profit_zar'] / $value['client_zar']) * 100), 0, '', ' ') < '16') {
                                       echo '<td style="vertical-align:middle;text-align:left;width:300px;background-color:#eac3c3;">' . $value['account_name'] . '</td>';
                                       echo '<td style="vertical-align:middle;text-align:left;width:300px;background-color:#eac3c3;">' . $value['service_name'] . '</td>';
                                       echo '<td style="vertical-align:middle;text-align:right;background-color:#eac3c3;">' . number_format($value['units'], 0, '', ' ') . '</td>';
                                       echo '<td style="vertical-align:middle;text-align:right;background-color:#eac3c3;">R ' . number_format($value['client_zar'], 2, '.', ' ') . '</td>';
                                       echo '<td style="vertical-align:middle;text-align:right;background-color:#eac3c3;">R ' . number_format($value['profit_zar'], 2, '.', ' ') . '</td>';
                                       echo '<td style="vertical-align:middle;text-align:right;background-color:#eac3c3;"><b>' . number_format((($value['profit_zar'] / $value['client_zar']) * 100), 0, '', ' ') . '%</b></td>';
                                    }
                                    /* else if (number_format((($value['profit_zar']/$value['client_zar'])*100),0,'',' ') > '15' && number_format((($value['profit_zar']/$value['client_zar'])*100),0,'',' ') < '50')
                                      {
                                      echo '<td style="vertical-align:middle;text-align:left;width:300px;background-color:#f2f7c0;">'.$value['account_name'].'</td>';
                                      echo '<td style="vertical-align:middle;text-align:left;width:300px;background-color:#f2f7c0;">'.$value['service_name'].'</td>';
                                      echo '<td style="vertical-align:middle;text-align:right;background-color:#f2f7c0;">'.number_format($value['units'],0,'',' ').'</td>';
                                      echo '<td style="vertical-align:middle;text-align:right;background-color:#f2f7c0;">R '.number_format($value['client_zar'],2,'.',' ').'</td>';
                                      echo '<td style="vertical-align:middle;text-align:right;background-color:#f2f7c0;">R '.number_format($value['profit_zar'],2,'.',' ').'</td>';
                                      echo '<td style="vertical-align:middle;text-align:right;background-color:#f2f7c0;"><b>'.number_format((($value['profit_zar']/$value['client_zar'])*100),0,'',' ').'%</b></td>';
                                      } */ else if (number_format((($value['profit_zar'] / $value['client_zar']) * 100), 0, '', ' ') > '50') {
                                       echo '<td style="vertical-align:middle;text-align:left;width:300px;background-color:#cff4d0;">' . $value['account_name'] . '</td>';
                                       echo '<td style="vertical-align:middle;text-align:left;width:300px;background-color:#cff4d0;">' . $value['service_name'] . '</td>';
                                       echo '<td style="vertical-align:middle;text-align:right;background-color:#cff4d0;">' . number_format($value['units'], 0, '', ' ') . '</td>';
                                       echo '<td style="vertical-align:middle;text-align:right;background-color:#cff4d0;">R ' . number_format($value['client_zar'], 2, '.', ' ') . '</td>';
                                       echo '<td style="vertical-align:middle;text-align:right;background-color:#cff4d0;">R ' . number_format($value['profit_zar'], 2, '.', ' ') . '</td>';
                                       echo '<td style="vertical-align:middle;text-align:right;background-color:#cff4d0;"><b>' . number_format((($value['profit_zar'] / $value['client_zar']) * 100), 0, '', ' ') . '%</b></td>';
                                    } else {
                                       echo '<td style="vertical-align:middle;text-align:left;width:300px;">' . $value['account_name'] . '</td>';
                                       echo '<td style="vertical-align:middle;text-align:left;width:300px;">' . $value['service_name'] . '</td>';
                                       echo '<td style="vertical-align:middle;text-align:right;">' . number_format($value['units'], 0, '', ' ') . '</td>';
                                       echo '<td style="vertical-align:middle;text-align:right;">R ' . number_format($value['client_zar'], 2, '.', ' ') . '</td>';
                                       echo '<td style="vertical-align:middle;text-align:right;">R ' . number_format($value['profit_zar'], 2, '.', ' ') . '</td>';
                                       echo '<td style="vertical-align:middle;text-align:right;"><b>' . number_format((($value['profit_zar'] / $value['client_zar']) * 100), 0, '', ' ') . '%</b></td>';
                                    }
                                    echo '</tr>';
                                 }
                              }
                              ?>
                           </tbody>
                           <tfoot>
                              <tr>

                              </tr>
                           </tfoot>
                        </table>
                     </div>
                  </div>
               </div>
            </section>
         </div>
         <!--//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////// -->
         <!--//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////// -->
         <!--//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////// -->
         <!--//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////// -->
         <!--//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////// -->
         <div role="tabpanel" class="tab-pane" id="client_stats">
            <section class="content col-lg-12" >

            </section>
         </div>


      </div>
   </section>
</aside>

<!--  THE JAVASCRIPT IMPORT ALWAYS APPEARS BEFORE LOCAL JAVASCRIPT AT THE BOTTOM OF THE PAGE -->
<?php include("template_import_script.php"); //must import all scripts first    ?>
<!-- END JAVASCRIPT IMPORT -->

<script type="text/javascript">
   $('#reportrange').daterangepicker(
           {
              ranges: {
                 'Today': [moment(), moment()],
                 'Yesterday': [moment().subtract('days', 1), moment().subtract('days', 1)],
                 'Last 7 Days': [moment().subtract('days', 6), moment()],
                 'Last 30 Days': [moment().subtract('days', 29), moment()],
                 'This Month': [moment().startOf('month'), moment().endOf('month')],
                 'Last Month': [moment().subtract('month', 1).startOf('month'), moment().subtract('month', 1).endOf('month')]
              },
              startDate: moment(<?php echo '"' . $startDate . '"'; ?>),
              endDate: moment(<?php echo '"' . $endDate . '"'; ?>)
           },
           function (start, end) {
              //alert("hi");
              $('#reportrange span').html(start.format('YYYY-MM-DD 00:00:00') + ' - ' + end.format('YYYY-MM-DD 23:59:59'));
              var st = moment(this.startDate).format("YYYY-MM-DD 00:00:00");
              var et = moment(this.endDate).format("YYYY-MM-DD 23:59:59");
              //window.location.href = 'clientrep.php?startDate=' + st + "&endDate=" + et;
              window.location = "salesstats.php?startDate=" + st + "&endDate=" + et;
           }
   );
</script>
<!-- AdminLTE for demo purposes -->


<script src="../js/plugins/datatables/jquery.dataTables.js" type="text/javascript"></script>

<script src="../js/plugins/datatables/dataTables.bootstrap.js" type="text/javascript"></script>

<script type="text/javascript">

   jQuery.extend(jQuery.fn.dataTableExt.oSort, {
      "title-numeric-pre": function (a) {
         var x = a.match(/title="*(-?[0-9\.]+)/)[1];
         return parseFloat(x);
      },
      "title-numeric-asc": function (a, b) {
         return ((a < b) ? -1 : ((a > b) ? 1 : 0));
      },
      "title-numeric-desc": function (a, b) {
         return ((a < b) ? 1 : ((a > b) ? -1 : 0));
      }
   });

   jQuery.extend(jQuery.fn.dataTableExt.oSort, {
      "signed-num-pre": function (a) {
         return (a == "-" || a === "") ? 0 : a.replace('+', '') * 1;
      },
      "signed-num-asc": function (a, b) {
         return ((a < b) ? -1 : ((a > b) ? 1 : 0));
      },
      "signed-num-desc": function (a, b) {
         return ((a < b) ? 1 : ((a > b) ? -1 : 0));
      }
   });

   jQuery.extend(jQuery.fn.dataTableExt.oSort,
           {
              "formatted-num-pre": function (a) {
                 a = (a === "-" || a === "") ? 0 : a.replace(/[^\d\-\.]/g, "");
                 return parseFloat(a);
              },
              "formatted-num-asc": function (a, b) {
                 return a - b;
              },
              "formatted-num-desc": function (a, b) {
                 return b - a;
              }
           });

   jQuery.extend(jQuery.fn.dataTableExt.oSort, {
      "currency-pre": function (a) {
         a = (a === "-") ? 0 : a.replace(/[^\d\-\.]/g, "");
         return parseFloat(a);
      },
      "currency-asc": function (a, b) {
         return a - b;
      },
      "currency-desc": function (a, b) {
         return b - a;
      }
   });

   $(function ()
   {
      $('#example2').dataTable({
         "bPaginate": true,
         "bLengthChange": false,
         "bFilter": true,
         "bInfo": true,
         "bsort": true,
         "bSortable": true,
         "bAutoWidth": false,
         "iDisplayLength": 100,
         "aoColumns": [
            null,
            null,
            {"sType": 'formatted-num', targets: 0},
            null,
            null,
            {"sType": 'formatted-num', targets: 0}
         ],
      });
   });

</script>


<script type="text/javascript">
   $(document).ready(function (e) {

      $(window).load(function () {
         $(".loader").fadeOut("slow");
         $(".loaderIcon").fadeOut("slow");

         //show loading spiners in the main stats blocks
         $("#moDiv").html('<span class="glyphicon glyphicon-refresh glyphicon-spin"></span>');
         $("#sentDiv").html('<span class="glyphicon glyphicon-refresh glyphicon-spin"></span>');
         $("#pendDiv").html('<span class="glyphicon glyphicon-refresh glyphicon-spin"></span>');
         $("#deliDiv").html('<span class="glyphicon glyphicon-refresh glyphicon-spin"></span>');
         $("#rejeDiv").html('<span class="glyphicon glyphicon-refresh glyphicon-spin"></span>');
         $("#failDiv").html('<span class="glyphicon glyphicon-refresh glyphicon-spin"></span>');

         //setInterval(refreshData(counter), 1000);
         //refreshMOData(counter);
         //refreshDataTables(counter);
         //refreshData(counter);
         //getClientBreakdown();

         var d = new Date();
         var month = d.getMonth();
         var year = d.getFullYear();
         var initDateStart = String(year) + '-' + String(month + 1) + '-' + '01'
         var initDateEnd = String(year) + '-' + String(month + 2) + '-' + '01'
         /*alert(initDateStart);
          alert(initDateEnd);*/
         var monthsArr = ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'];
         //Set the current month we are in now variable where page loads
         var monthnow = monthsArr[parseInt(month)];

         getCountryData(initDateStart, initDateEnd, monthnow);


      });
   });
</script>
<!-- Template Footer -->

