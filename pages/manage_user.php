<?php
   /**
    * Include the header template which sets up the HTML
    *
    * Don't forget to include template_import_script before any Javascripting
    * Don't forget to include template footer.php at the bottom of the page as well
    */
   //-------------------------------------------------------------
   // Template
   //-------------------------------------------------------------
   TemplateHelper::setPageTitle('Manage User');
   TemplateHelper::initialize();

   //-------------------------------------------------------------
   // MANAGE SERVICE
   //-------------------------------------------------------------
   //get the service

   if(isset($_GET['user_id']))
   {
      $user_id = $_GET['user_id'];
   }
   else
   {
      header('location: manage_user_select.php' );
      die();
   }

   //get the user object
   $user_obj = new User($user_id);

   //if a service ID is set, then the back button to the particular service the user was working on will show
   if(isset($_GET['service_id']))
   {
      $service_id = $_GET['service_id'];
      $show_service_back_button = true;
      $back_service = new Service($service_id);
   }
   else
   {
      $show_service_back_button = false;
   }

   //if a service ID is set, then the back button to the particular service the user was working on will show
   if(isset($_GET['account_id']))
   {
      $account_id = $_GET['account_id'];
      $show_account_back_button = true;
      $back_account = new Account($account_id);
   }
   else
   {
      $show_account_back_button = false;
   }

   //determine the styling class for the enabled/disabled display
   if($user_obj->user_status == "ENABLED")
   {
      $user_status_class = "btn-success";
   }
   else
   {
      $user_status_class = "btn-danger";
   }

?>

<aside class="right-side">
   <section class="content-header">
      <h1>
         Manage User
      </h1>
   </section>

   <section class="content">

      <?php
         //include the top nav
         $nav_section = "users";
         include('modules/module_admin_nav.php');
      ?>

      <!-- MAIN USER CONTENT -->
      <div class="row">

         <!-- USER MAIN DETAILS ACCOUNT ECT -->
         <div class="col-lg-2">
            <?php if($show_service_back_button) { ?>
               <!-- THE BACK TO SERVICE BUTTON -->
               <a class="btn btn-default btn-block btn-sm" href="manage_service.php?service_id=<?php echo $service_id; ?>#tab_users" style="margin-top:10px; overflow:hidden;">
                  <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span> Back to Service "<?php echo htmlentities($back_service->service_name); ?>"
               </a>
            <?php } ?>

            <?php if($show_account_back_button) { ?>
               <!-- THE BACK TO ACCOUNT BUTTON -->
               <a class="btn btn-default btn-block btn-sm" href="manage_account.php?account_id=<?php echo $account_id; ?>#tab_users" style="margin-top:10px; overflow:hidden;">
                  <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span> Back to Service "<?php echo htmlentities($back_account->account_name); ?>"
               </a>
            <?php } ?>
         </div>

         <div class="col-lg-7">
            <h1 style="margin:0px;">
               <small>Username: </small>
               <strong class="connet-field-display"><?php echo $user_obj->user_username; ?></strong>
               <!-- PREVENT RENAMING IF API USER -->
               <?php if($user_obj->isAPIUser()) { ?>
                  <small class="text-warning">[API USER]</small>
               <?php } else { ?>
                  <a href="#" class="btn-edit-user-property btn btn-xs btn-primary"
                     connet-id="<?php echo $user_obj->user_id; ?>"
                     connet-value="<?php echo htmlspecialchars($user_obj->user_username); ?>"
                     connet-ajax-task="rename_username"
                     connet-field="user_username"
                     connet-title="Username">Edit</a>
               <?php } ?>
            </h1>
         </div>

         <div class="col-lg-1">
            <!-- BUTONN FOR DELETING A USER -->
            <!-- FEATURE REMOVED FOR NOW
            <button type="button" class="btn btn-danger" id="btnDeleteUser" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
               <i class="fa fa-close"></i> Delete User
            </button> -->
            <!-- FOR MIMICING THE USER -->
            <button class="btn btn-primary pull-right btnMimic">
               <i class="fa fa-user-secret"></i> Mimic This User
            </button>
         </div>

         <div class="col-lg-2">
            <div class="input-group-btn" style="text-align:center;">
               <button type="button" class="btn btn-block <?php echo $user_status_class; ?> dropdown-toggle" id="btnStatus" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                  <span id="btnStatusString">USER <?php echo $user_obj->user_status; ?></span> <span class="caret"></span>
               </button>
               <ul class="dropdown-menu dropdown-menu-right">
                  <li><a class="btn-select-status" connet-user-status="ENABLED" href="#">Enable User</a></li>
                  <li><a class="btn-select-status" connet-user-status="DISABLED" href="#">Disable User</a></li>
               </ul>
            </div>
         </div>
      </div>

      <hr/>
      <div class="row">
         <div class="col-lg-2">
            <div class="box box-solid">
               <div class="box-body" style="text-align:right;">
                  <h1 style="margin:5px;">
                     <small>User ID:</small>
                     <?php echo $user_obj->user_id; ?>
                  </h1>
                  <hr/>
                  <h4>
                     <small>Main Account</small><br/>
                     <strong>
                        <a href="manage_account.php?account_id=<?php echo $user_obj->default_account['account_id']; ?>&user_id=<?php echo $user_id; ?>">
                           <?php echo htmlentities($user_obj->default_account['account_name']); ?>
                        </a>
                     </strong>
                  </h4>
                  <h4>
                     <small>Default Service</small><br/>
                     <strong>
                        <a href="manage_service.php?service_id=<?php echo $user_obj->default_service['service_id']; ?>&user_id=<?php echo $user_id; ?>">
                           <?php echo htmlentities($user_obj->default_service['service_name']); ?>
                        </a>
                     </strong>
                  </h4>
                  <hr/>
                  <h4>
                     <small>Is Admin: </small>
                     <?php echo ($user_obj->user_system_admin == 1 ? '<span class="label label-success">YES</span>' : '<span class="label label-danger">NO</span>'); ?>
                  </h4>
                  <h4 style="margin-top:15px;">
                     <small>Is API User: </small>
                     <?php echo ($user_obj->isAPIUser() ? '<span class="label label-success">YES</span>' : '<span class="label label-danger">NO</span>'); ?>
                  </h4>
               </div>
            </div>
         </div>

         <!-- THE TABS THAT HOLD THE CONFIG SECTIONS -->
         <div class="col-lg-10">
            <div class="nav-tabs-custom">
               <!-- Nav tabs -->
               <ul class="nav nav-tabs" id="user_tabs">
                  <li role="presentation"><a href="#tab_overview" aria-controls="tab_overview" role="tab" data-toggle="tab"><i class="fa fa-eye"></i> User Overview</a></li>
                  <li role="presentation"><a href="#tab_general" aria-controls="tab_general" role="tab" data-toggle="tab"><i class="fa fa-user"></i> User Details</a></li>
                  <li role="presentation"><a href="#tab_security" aria-controls="tab_security" role="tab" data-toggle="tab"><i class="fa fa-lock"></i> Password And Security</a></li>
                  <li role="presentation"><a href="#tab_services" aria-controls="tab_services" role="tab" data-toggle="tab"><i class="fa fa-flag"></i> Service Permissions</a></li>
               </ul>

               <!-- Tab panes -->
               <div class="tab-content">
                  <!-- THE OVERVIEW TAB -->
                  <div role="tabpanel" class="tab-pane active" id="tab_overview">

                  </div>

                  <!-- THE GENERAL SETTINGS TAB -->
                  <div role="tabpanel" class="tab-pane active" id="tab_general">

                  </div>


                  <!-- THE SECURITY AND PASSWORDS TAB -->
                  <div role="tabpanel" class="tab-pane" id="tab_security">

                  </div>


                  <!-- THE SERVICES TAB -->
                  <div role="tabpanel" class="tab-pane" id="tab_services">

                  </div>

               </div>
            </div>
         </div>
   </section>

   <!-- MODALS HERE -->
   <div id="modalEditField" class="modal fade" tabindex="-1" role="dialog" data-backdrop="static">
      <div class="modal-dialog">
         <div class="modal-content">
            <div class="modal-header" >
               <button type="button" class="close" data-dismiss="modal" aria-label="Close" id="btnModalCloseTop"><span aria-hidden="true">&times;</span></button>
               <h4 class="modal-title" id="modalTitle">TITLE</h4>
            </div>
            <div class="modal-body">
               <div class="alert alert-danger" style="display:none;" id="modAlert">
                  <h4><i class="icon fa fa-ban"></i> Error!</h4>
                  <p id="modAlertText">Unfortunately there was an error, please try again.</p>
               </div>
               <div class="form-group">
                  <label for="modalInputValue" id="modalInputLabel">Label</label>
                  <input type="text" class="form-control" id="modalInputValue" placeholder="" value="" />
               </div>
               <small>Please edit the value and click save to commit, or close to cancel.</small>
            </div>
            <div class="modal-footer">
               <button type="button" class="btn btn-default" data-dismiss="modal" id="btnModalCloseBottom">Close</button>
               <button type="button" class="btn btn-primary" id="btnModalSave" style="background-color:<?php echo $_SESSION['accountColour']; ?>;border: 2px solid; border-radius: 6px !important;">Save</button>
               <input type="hidden" class="form-control" id="modalInputField" value=""/>
               <input type="hidden" class="form-control" id="modalInputId" value=""/>
               <input type="hidden" class="form-control" id="modalInputTask" value=""/>
            </div>
         </div><!-- /.modal-content -->
      </div><!-- /.modal-dialog -->
   </div><!-- /.modal -->
</aside>


<!--  THE JAVASCRIPT IMPORT ALWAYS APPEARS BEFORE LOCAL JAVASCRIPT AT THE BOTTOM OF THE PAGE -->
<?php include("template_import_script.php"); //must import all scripts first   ?>
<!-- END JAVASCRIPT IMPORT -->

<script type="text/javascript">
   var userId = <?php echo (isset($user_id) ? $user_id : '-1') ?>;

   //set the tab and start the tab load process according to the hash string passed to it
   function showTab(hashTab)
   {
      console.log(hashTab);
      $('#user_tabs a[href="' + hashTab + '"]').tab('show');
      $(window).scrollTop(0);
   }


   $(document).ready(function (e)
   {
      /**************************************
       * TAB LOGIC
       **************************************/
      $('#service_tabs a').click(function(e) {
         e.preventDefault();
         $(this).tab('show');
         $(window).scrollTop(0);
      });

      //store the currently selected tab in the hash value
      $("ul.nav-tabs > li > a").on("shown.bs.tab", function(e) {
         var id = $(e.target).attr("href").substr(1);
         window.location.hash = id;

         loadTabFragment(id);
      });

      //on load of the page: switch to the currently selected tab
      var hash = window.location.hash;
      if(hash == "") {
         hash = "#tab_overview";
      }

      showTab(hash);

      /**
       * This button loads the generic field editor in the modal
       */
      $('.btn-edit-user-property').on("click", function (e)
      {
         e.preventDefault();

         var fieldName = $(this).attr('connet-field');
         var fieldTitle = $(this).attr('connet-title');
         var fieldId = $(this).attr('connet-id');
         var fieldTask = $(this).attr('connet-ajax-task');
         var fieldValue = $(this).attr('connet-value');

         $('#modalTitle').html("Edit Field: " + fieldTitle);
         $('#modalInputLabel').html(fieldTitle);
         $('#modalInputValue').val(fieldValue);
         $('#modalInputField').val(fieldName);
         $('#modalInputId').val(fieldId);
         $('#modalInputTask').val(fieldTask);

         $('#modalEditField').modal('show');
      });

      /**
       * This button catches the ENABLED/DISABLED status of a service.
       */
      $('.btn-select-status').on("click", function (e)
      {
         e.preventDefault();

         var prevHTML = $('#btnStatusString').html();
         var status = $(this).attr('connet-user-status');
         var confirmText = "";
         var newClass = "";
         var oldClass = "";
         if(status == "ENABLED")
         {
            confirmText = "Are you sure you want to ENABLE this user?";
            newClass = "btn-success";
            oldClass = "btn-danger";
         }
         else
         {
            confirmText = "Are you sure you want to DISABLE this user?";
            newClass = "btn-danger";
            oldClass = "btn-danger";
         }

         if(confirm(confirmText))
         {
            $('#btnStatusString').html('<span class="glyphicon glyphicon-refresh glyphicon-spin"></span> Saving...');

            $.post("../php/ajaxManageUserHandler.php", {task: "set_user_status", userId: userId, status: status}).done(function (data) {
               var json = $.parseJSON(data);
               if (json.success)
               {
                  $('#btnStatusString').html("USER " + status);
                  $('#btnStatus').removeClass(oldClass);
                  $('#btnStatus').addClass(newClass);
               }
               else
               {
                  $('#btnStatusString').html(prevHTML);
                  alert("A server error occurred and we could not change the status of this service, please contact technical support.");
               }
            }).fail(function (data) {
               $('#btnStatusString').html(prevHTML);
               showModalError("A server error occurred and we could not change the status of this service, please contact technical support.");
            });
         }
      });

      /**
       * ALLOWS THE ADMIN TO DELETE THE USER OBJECT, IF IT IS SAFE TO DO SO
       */
      $('#btnDeleteUser').on("click", function (e)
      {
         e.preventDefault();

         if(confirm("Are you sure you want to permanently delete this user?"))
         {
            var thisButton = $(this);
            var thisButtonHTML = $(this).html();
            $(this).html('<span class="glyphicon glyphicon-refresh glyphicon-spin"></span> Deleting...');
            $.post("../php/ajaxManageUserHandler.php", {task: 'delete_user', userId: userId}).done(function (data) {
               var json = $.parseJSON(data);
               if (json.success)
               {
                  alert("This user has been successfully deleted and will never be seen again :'(");
                  $(thisButton).html("Reloading...");
                  window.location = "manage_user_select.php";
               }
               else
               {
                  $(thisButton).html(thisButtonHTML);
                  if(json.reason == "must_preserve")
                  {
                     alert("This user cannot be deleted in order to preserve links to previous actions such as bulk sends and credit transactions.");
                  }
                  else
                  {
                     alert("A server error occurred and we could not delete the user, please contact technical support.");
                  }
               }
            }).fail(function (data) {
               $(thisButton).html(thisButtonHTML);
               alert("A server error occurred  and we could not delete the user, please contact technical support.");
            });
         }
      });

      $('#btnModalSave').on("click", function (e)
      {
         e.preventDefault();

         lockModalBusy("Saving...");

         var fieldName = $('#modalInputField').val();
         var fieldValue = $('#modalInputValue').val();
         var fieldTask = $('#modalInputTask').val();
         var fieldId = $('#modalInputId').val();

         $.post("../php/ajaxManageUserHandler.php", {task: fieldTask, serviceId: fragServiceId, fieldId: fieldId, fieldName:fieldName, fieldValue:fieldValue}).done(function (data) {
            var json = $.parseJSON(data);
            if (json.success)
            {
               unlockModalBusy();

               //ocate the html elements that hold this data and update them for display
               $("a[connet-id='"+fieldId+"'][connet-field='"+fieldName+"']" ).attr('connet-value', fieldValue);
               $("a[connet-id='"+fieldId+"'][connet-field='"+fieldName+"']" ).parent().find('.connet-field-display').html(fieldValue);
               $('#modalEditField').modal('hide');
            }
            else
            {
               unlockModalBusy();
               showModalError("A server error occurred and we could not save the data, please contact technical support.");
            }
         }).fail(function (data) {
            unlockModalBusy();
            showModalError("A server error occurred and we could not save the data, please contact technical support.");
         });
      });

      /**
       * ALLOWS AN ADMIN TO MIMIC A CLIENT USER TO SEE WHAT THEY SEE
       */
         //assign a click listener to the forgot password link
      $(".btnMimic").on("click", function (e)
      {
         e.preventDefault();

         $(".loader").fadeIn("slow");
         $(".loaderIcon").fadeIn("slow");
         $(".btn_mimic").attr('disabled', 'true');

         $.post("../php/ajaxToggleMimic.php", {mimic_task: 'start', user_id_to_mimic: userId}).done(function (raw_data) {
            var data = $.parseJSON(raw_data);
            if (data.success)
            {
               alert('You are now entering mimic mode, where you can view and engage with Plugin while pretending to be this user.');
               window.location = "../clienthome.php";
            }
            else
            {
               alert('There was a problem with enabling mimic mode on this user, please contact the development department.');
               window.location.reload();
            }

            $(".loader").fadeOut("slow");
            $(".loaderIcon").fadeOut("slow");
            $(".btn_mimic").removeAttr('disabled');
         }).fail(function (data) {
            alert('There was an error: ' + data + '');
            $(".btn_mimic").removeAttr('disabled');
            $(".loader").fadeOut("slow");
            $(".loaderIcon").fadeOut("slow");
         });
      });
   });

   //this locks the modal so the user can't do anything, useful when runing ajax calls off of it
   var prevButtonContent = "";
   function lockModalBusy(busyMessage)
   {
      prevButtonContent = $('#btnModalSave').html();
      $('#btnModalCloseTop').prop("disabled", true);
      $('#btnModalCloseBottom').prop("disabled", true);
      $('#btnModalSave').prop("disabled", false);
      $('#btnModalSave').html('<span class="glyphicon glyphicon-refresh glyphicon-spin"></span> ' + busyMessage);
   }

   //unlocks the modal so the user can close it
   function unlockModalBusy()
   {
      $('#btnModalCloseTop').prop("disabled", false);
      $('#btnModalCloseBottom').prop("disabled", false);
      $('#btnModalSave').prop("disabled", false);
      $('#btnModalSave').html(prevButtonContent);
      prevButtonContent = "";
   }

   //shows the error in the MODAL with a custom error message
   function showModalError(message)
   {
      $("#modAlertText").html(message);
      $("#modAlert").show();
   }

   //hides the modal error and resets the message
   function hideModalError()
   {
      $("#modAlertText").html();
      $("#modAlert").hide();
   }

   function loadTabFragment(tabName)
   {
      showTabLoading(tabName);

      var getVariableString = window.location.search.substring(1);

      var scriptPath = "";
      switch (tabName) {
         case "tab_overview":
            scriptPath = "admin_users/frag_user_overview.php?"+getVariableString;
            break;
         case "tab_general":
            scriptPath = "admin_users/frag_user_general.php?"+getVariableString;
            break;
         case "tab_security":
            scriptPath = "admin_users/frag_user_security.php?"+getVariableString;
            break;
         case "tab_services":
            scriptPath = "admin_users/frag_user_services.php?"+getVariableString;
            break;
      }

      //load up the page tab we have selected
      $.post(scriptPath, {user_id:userId}).done(function (data) {
         $('#'+tabName).html(data);
      }).fail(function (data) {
         showTabLoadingError(divName, "A server error occurred and we could not load this list, please contact technical support.");
      });
   }

   function showTabLoading(elementId)
   {
      $('#'+elementId).html('<div style="width:100%; text-align:center; padding:20px; font-size:20px;"><span class="glyphicon glyphicon-refresh glyphicon-spin"></span><br/>Loading... Please wait.</div>');
   }

   function showTabLoadingError(elementId, message)
   {
      $('#'+elementId).html('<h5 class="text-warning">' + message + '</h5>');
   }

   function showLoading(elementId)
   {
      $('#'+elementId).html('<div style="width:100%; text-align:center; padding:20px;"><span class="glyphicon glyphicon-refresh glyphicon-spin"></span> Loading...</div>');
   }

   function showLoadingError(elementId, message)
   {
      $('#'+elementId).html('<h5 class="text-warning">' + message + '</h5>');
   }
</script>

<!-- Template Footer -->

