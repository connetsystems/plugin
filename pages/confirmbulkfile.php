<?php
//prevent user from accessing this page manually without going through the previous pages
if(!isset($_POST['serviceId']))
{
   header('Location: ../clienthome.php' );
   exit();
}

/**
 * Include the header template which sets up the HTML
 *
 * Don't forget to include template_import_script before any Javascripting
 * Don't forget to include template footer.php at the bottom of the page as well
 */
TemplateHelper::setPageTitle('Bulk SMS Confirm');
TemplateHelper::initialize();

define('MYSQL_MNP_DATABASE_STRING', '10.3.0.42;core;youyaiN3kooduwi;MNP;persistent');

set_time_limit(720);
ini_set('memory_limit', '512M');

setlocale(LC_ALL, 'en_US.utf8');

$smsCounting = $_POST['smsCount'];
$postNums = 0;
$queued = 0;
$dsp2 = 'block';
$singleView = 'block';
$delimer = '';

//extract the SMS text post variable for use
if (isset($_POST['smsText']) && $_POST['smsText'] != '') {
   //trim out all the crap from the right and left side of the text
   $sms_text = trim($_POST['smsText']);
} else {
   $sms_text = "";
}


if (isset($_POST['dedupe']) && $_POST['dedupe'] != '') {
   $dedupe = 1;
} else {
   $dedupe = 0;
}

if (isset($_POST['numbers']) && $_POST['numbers'] != '') {
   $postNums = 1;
}

$serviceId = $_POST['serviceId'];
$typeOfAccount = getServiceType($serviceId);
$credAvailForSend = getServiceCredit($serviceId);



//echo "<br/><br/> Service ID: ".$serviceId." - Credit available: ".$credAvailForSend;
//check if the user uploaded a file, and do the correct configuration
if (isset($_FILES['uploadCSV']['error']) && $_FILES['uploadCSV']['error'] == 0 && $_FILES['uploadCSV']['size'] > 0) {
   //we are uploading a CSV file here
   $maxSize = 1024 * 1024 * 1024 * 1024;

   $target_dir = "../tempFiles/";
   if (!is_dir($target_dir)) {
      mkdir($target_dir, 0777);
   }

   $target_file = $target_dir . basename($_FILES["uploadCSV"]["name"]);
   $imageFileType = pathinfo($target_file, PATHINFO_EXTENSION);

   if (!empty(($_FILES["uploadCSV"]["tmp_name"]))) {
      $uploadOk = 1;
   }

   if ($_FILES["uploadCSV"]["size"] > $maxSize) {
      $uploadOk = 0;
   }

   if ($imageFileType != "csv") {
      $uploadOk = 0;
   }

   if ($uploadOk == 1) {
      $ul = move_uploaded_file($_FILES["uploadCSV"]["tmp_name"], $target_file);
   }

   $csv_source_file = $target_file;
   $trueFileName = $_FILES["uploadCSV"]["name"];
} else {
   $csv_source_file = "";
   $trueFileName = "";
}

//check if the user sent through a manual comma seperated numbers string, and set appropriately
if (isset($_POST['numbers']) && $_POST['numbers'] != '') {
   //lastly, if an MSISDN string is sent, send the string for processing
   $task = 'msisdn_string';
   $numbers_string = $_POST['numbers'];
   $trueFileName = "";
} else {
   $numbers_string = "";
}

//check if the user selected a contact list to send to, and set the ID
if (isset($_POST['contactList']) && $_POST['contactList'] != '0') {
   //if contact list is sent, send the task as contact_list and send the ID of the particular Contact List selected
   $task = 'contact_list';
   $contact_list_id = $_POST['contactList'];
   $trueFileName = "";
} else {
   $contact_list_id = "";
}
?>
<aside class="right-side">
   <section class="content-header">
      <h1>
         Review Bulk Send
         <!--small>Control panel</small-->
      </h1>
   </section>

   <section class="content">
      <div class="row col-lg-12">

         <!-- PROCESSING HOLDER, SHOWS FIRST WHILE FILE IS ANALYZED FOR STATS -->
         <div id="processing_holder" class="center-block" style="width:400px; text-align: center;">
            <br/>
            <span style="font-size: 72pt;" class="glyphicon glyphicon-refresh glyphicon-spin"></span>
            <h3>Analyzing your bulk send, please wait... <br/><small>This may take very long with large batches.</small></h3>
            <p><strong id="txt_sends_processed">0</strong> sends have been processed.</p>
         </div>

         <!-- ERROR HOLDER -->
         <div id="error_holder" class="center-block" style="width:400px; text-align: center; display:none;">
            <br/>
            <span style="font-size: 72pt;" class="fa fa-exclamation-triangle text-danger"></span>
            <h3 class="text-danger">Unfortunately an error has occurred... <br/><small>Please retry your submission, or contact technical support.</small></h3>
         </div>


         <!-- RESULT HOLDER, SHOWS AFTER PROCESSING HOLDER, WHEN STATS HAVE BEEN LOADED -->
         <div id="result_holder" style="border-top-color:<?php echo $accRGB; ?>;margin-top:0px;display:none;">
            <div class="box-body">
               <h4 style="margin-top:0px;">Please review the page below, and click "Build Send" if you are happy with the configuration.</h4>
               <!-- ERROR FOR INSUFFICIENT FUNDS -->
               <div class="callout callout-danger" id="errNoFunds" style="margin-bottom:0px; display:none;">
                  <h4>Insufficient Funds</h4>
                  <p>You do not have enough funds to complete this bulk send. Please contact the accounts department.</p>
                  <div style="float:right; margin-top:-44px; margin-right:10px;">
                     <i class="fa fa-exclamation-triangle" style="color:#c99b9d; font-size:20pt;"></i>
                  </div>
               </div>
               <!-- ERROR FOR IGNORED ROWS -->
               <div class="callout callout-danger" id="errIgnoredRows" style="margin-bottom:0px; display:none;">
                  <h4>Some Numbers or Rows Ignored</h4>
                  <p><strong><span id="txtInvalidCount">-</span> numbers</strong> have been ignored due to invalid characters in the MSISDN field, either in your CSV file (Column A) or your manually entered numbers.</p>
                  <div style="float:right; margin-top:-44px; margin-right:10px;">
                     <i class="fa fa-exclamation-triangle" style="color:#c99b9d; font-size:20pt;"></i>
                  </div>
               </div>
               <!-- ERROR FOR FILE WITH ALL NUMBERS INVALID -->
               <div class="callout callout-danger" id="errNoValidNumbers" style="margin-bottom:0px; display:none;">
                  <h4>All Numbers Unknown or Invalid</h4>
                  <p>The numbers you submitted are either blank, invalid or incorrectly formatted. Please correct your numbers before you continue.</p>
                  <div style="float:right; margin-top:-44px; margin-right:10px;">
                     <i class="fa fa-exclamation-triangle" style="color:#c99b9d; font-size:20pt;"></i>
                  </div>
               </div>
               <!-- ERROR FOR FILE WITH DUPLICATE NUMBERS -->
               <div class="callout callout-warning" id="errDuplicates" style="margin-bottom:0px; display:none;">
                  <h4>Your data contains duplicate numbers!</h4>
                  <p>Duplicate numbers and messages have been detected in your send. You can choose to have these numbers excluded automatically by checking the "Remove duplicates before sending" checkbox before starting your build.</p>
               </div>
               <br/>

               <!-- SEND PREVIEW -->
               <div class="box box-primary">
                  <div class="box-header with-border">
                     <h3 class="box-title">Send Preview <small>(N.B. Only the first 10 numbers are shown below)</small></h3>
                  </div>
                  <div class="box-body">

                     <table id="sampleTable" class="table table-bordered table-striped dataTable table-hover">
                        <thead>
                           <tr>
                              <th style="width:100px">Number <br/></th>
                              <th>SMS Text</th>
                              <th style="width:80px">Sends</th>
                           </tr>
                        </thead>
                        <tbody id="tblSendExamplesHolder">

                        </tbody>
                     </table>
                  </div>
               </div>

               <!-- BATCH STATS -->
               <div class="row">
                  <div class="col-lg-7">
                     <div class="box box-success">
                        <div class="box-header with-border">
                           <h3 class="box-title">Send Properties</h3>
                        </div>
                        <div class="box-body" style="height:350px; overflow-y:auto;">

                           <h3 style="margin-top:0px;"><small>Reference:</small><br/><?php echo htmlentities($_POST['refer']); ?></h3>
                           <h5><small>Service:</small><br/><?php echo htmlentities($_POST['client']); ?></h5>
                           <?php
                           if (isset($_FILES['uploadCSV']['error']) && $_FILES['uploadCSV']['error'] == 0 && $_FILES['uploadCSV']['size'] > 0) {
                              ?>
                              <h5><small>File Name:</small><br/><?php echo htmlentities($trueFileName); ?></h5>
                              <?php
                           }
                           ?>
                           <h5><small>Campaign:</small><br/><?php echo htmlentities($_POST['campN']); ?></h5>
                           <h5><small>Status:</small><br/><?php echo htmlentities($_POST['status']); ?></h5>
                           <?php
                           if (isset($_POST['sTime']) && $_POST['status'] == 'Scheduled') {
                              echo "<h5><small>Schedule Time: </small><br/>" . htmlentities($_POST['sTime']) . "</h5>";
                           }
                           ?>
                           <h5><small>Template:</small><br/><?php echo htmlentities($_POST['temp']); ?></h5>
                           <h5><small>SMS Text:</small><br/><?php echo htmlentities($sms_text); ?></h5>
                           <!--
                           <h3 style="margin-top:0px;"><small>Send</small><br/>Statistics</h3>
                           <h5><small>total numbers</small><br/><span id="txtTotalNumbers">0</span></h5>
                           <h5><small>valid numbers</small><br/><span id="txtValidNumbers" class="text-success">0</span></h5>
                           <h5><small>unknown numbers</small><br/><span id="txtUnknownNumbers" class="text-warning">0</span></h5>
                           <h5><small>invalid numbers</small><br/><span id="txtInvalidNumbers" class="text-danger">0</span></h5>
                           <h5><small>duplicate numbers</small><br/><span id="txtDuplicateNumbers">0</span></h5>
                           -->

                        </div>
                        <div class="box-footer"  style="height:190px;">
                           <div class="row">
                              <div class="col-lg-4">
                                 <h1 style="margin-top:0px;">
                                    <small>Valid numbers: </small><br/><span id="txtValidNumbers" class="text-success">0</span>
                                 </h1>
                                 <h4 style="margin-top:0px;"><small>With a total of <strong id="txtDuplicateNumbers">0</strong> duplicates.</small></h4>
                              </div>
                              <div class="col-lg-4"  style="text-align: center;">
                                 <p style="margin-top:30px;">
                                    <strong id="txtTotalNumbers" >0</strong> total rows<br/>
                                    <strong id="txtInvalidNumbers" class="text-danger">0</strong> invalid or unreadable rows<br/>
                                    <strong id="txtUnknownNumbers" class="text-warning">0</strong> unidentifiable numbers<br/>
                                 </p>
                              </div>
                              <div class="col-lg-4"  style="text-align: right;">
                                 <h1 style="margin-top:0px;">
                                    <small>Total cost to send:</small><br/><span id="txtTotalCost">0.0</span>
                                 </h1>
                                 <h4 style="margin-top:0px;"><small>Sending a total of <strong id="txtTotalUnits">0</strong> units.</small></h4>
                              </div>
                           </div>
                           <div class="row">
                              <div class="col-lg-12" style="text-align: right;">
                                 <h5 class="text-warning">
                                    N.B. Estimate Only
                                    <br/><small>Final cost will appear on the Network Traffic Report.</small>
                                 </h5>
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>
                  <div class="col-lg-5">
                     <div class="box box-primary">
                        <div class="box-header with-border">
                           <h3 class="box-title">Network Breakdown</h3>
                        </div>
                        <div class="box-body" id="networkSendBreakdownHolder"  style="height:350px; overflow-y:auto;">
                        </div>
                        <div class="box-footer" style="height:190px;">
                           <h4 class="text-warning">
                              Unknown Networks
                              <br/>
                              <small>If you see 'Unknown Network' in your summary, it means that you have either not added the country code to the cell numbers, or you don't have
                                 the network for the cell number on your account. The 'Unknown Network' numbers will be rejected.
                                 <br/><br/>
                                 Please contact support if you need more countries added to your account.</small>
                           </h4>
                        </div>
                     </div>
                  </div>
               </div>

               <div class="box box-primary" id="file_generation_holder">
                  <div class="box-header with-border">
                     <h3 class="box-title">Send Generation <small id="processing_progress_status">(N.B. Only the first 10 numbers are shown below)</small></h3>
                  </div>
                  <div class="box-body">
                     <!-- PROGRESS BAR FOR THE FILE PROCESSING -->
                     <div class="progress active">
                        <div id="processing_progress_bar" class="progress-bar progress-bar-striped" role="progressbar" aria-valuenow="1" aria-valuemin="0" aria-valuemax="100" style="width: 45%">
                           <span id="processing_progress_percent">45%</span>
                        </div>
                     </div>
                     <div class="row">
                        <div class="col-lg-4">
                           <a onclick="backBtn();" class="btn btn-social btn-dropbox " style="background-color:<?php echo $accRGB; ?>; color: #ffffff; border: 2px solid; border-radius: 6px !important; width: 92px;">
                              <i class="fa fa-arrow-left"></i>Back
                           </a>
                        </div>

                        <div class="col-lg-6" style="text-align:right;">
                           <div id="check_remove_duplicates_holder" class="checkbox" style="padding-bottom:0px; display:none;">
                              <label style="padding-left:15px;">
                                 <input type="checkbox" id="check_remove_duplicates" name="check_remove_duplicates">
                                 Remove duplicates before sending<br/><small>This will reduce the cost of your send.</small>
                              </label>
                           </div>
                        </div>
                        <div class="col-lg-2" style="text-align:right;">
                           <a onclick="buildSend();" class="btn btn-social btn-dropbox" id="btnBuildSend" style="background-color:<?php echo $accRGB; ?>; border: 2px solid; border-radius: 6px !important; display:none;">
                              <i class="fa fa-save"></i>Build Bulk Send
                           </a>
                           <a onclick="validateBulk();" class="btn btn-social btn-dropbox" id="btnSendBulk" style="background-color:<?php echo $accRGB; ?>; border: 2px solid; border-radius: 6px !important; display:none;">
                              <i class="fa fa-save"></i>Send Bulk SMS
                           </a>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </section>
</aside>

<!--  THE JAVASCRIPT IMPORT ALWAYS APPEARS BEFORE LOCAL JAVASCRIPT AT THE BOTTOM OF THE PAGE -->
<?php include("template_import_script.php"); //must import all scripts first   ?>
<!-- END JAVASCRIPT IMPORT -->



<?php
$javascript_request = array(
    'sId' => $serviceId,
    'smsTxt' => $sms_text,
    'camCode' => $_POST['camCode'],
    'camId' => $_POST['camId'],
    'refer' => $_POST['refer'],
    'status' => $_POST['status'],
    'sTime' => isset($_POST['sTime']) ? $_POST['sTime'] : '',
    'dedupe' => $dedupe,
    'smsCount' => $_POST['smsCount'],
    'client' => $_POST['client'],
    'trueFileName' => $trueFileName,
    'csv_source_file' => $csv_source_file,
    'numbers_string' => $numbers_string,
    'contact_list_id' => $contact_list_id,
);
?>

<script type="text/javascript">

   var request = <?php echo json_encode($javascript_request); ?>;

   var invalidMNCs = 0;
   var validNumbers = 0;

   var fName = "";
   var preBuildFileName = "";
   var sId = request.sId;
   var smsTxt = request.smsTxt;
   var colCount = 0;
   var camCode = request.camCode;
   var camId = request.camId;

   var refer = request.refer;
   var status = request.status;
   var sTime = request.sTime;

   var dedupe = request.dedupe;
   var smsCount = request.smsCount;
   var client = request.client;
   var trueFileName = request.trueFileName;
   var csv_source_file = request.csv_source_file;
   var numbers_string = request.numbers_string;
   var contact_list_id = request.contact_list_id;
   var analysisJobName = "";
   var timer2 = null;

   $(document).ready(function (e)
   {

      //schedule the checker
      timer2 = setInterval(function ()
      {
         checkAnalysis();
      }, 3000);

      //start the file generation checker
      checkAnalysis();
   });

   function checkAnalysis()
   {
      var params = {
         sms_text: smsTxt,
         service_id: sId,
         csv_source_file: csv_source_file,
         numbers_string: numbers_string,
         contact_list_id: contact_list_id,
         job_name : analysisJobName
      };

      console.log(params);

      //run our file processor before submitting it to niel
      $.post("../php/ajaxAnalyzeBulkSendNewDaemon.php", params).done(function (data) {
         console.log(data);
         try
         {
            var json = $.parseJSON(data);
            if (json.success)
            {
               if(json.action == "queued")
               {
                  analysisJobName = json.job_name;
               }
               else if(json.action == "status" && json.completed == 0)
               {
                  $("#txt_sends_processed").html(json.sends_processed);
               }
               else if(json.action == "status" && json.completed == 1)
               {
                  window.clearInterval(timer2);
                  var result_data = $.parseJSON(json.job_data);
                  fName = result_data.temp_file + '.downloaded';
                  preBuildFileName = result_data.temp_file;
                  colCount = result_data.file_col_count;
                  showResultsOfAnalysis(result_data);
               }
            }
            else
            {
               showAnalysisError();
               window.clearInterval(timer2);
            }
         }
         catch(e)
         {
            showAnalysisError();
            window.clearInterval(timer2);
         }
      }).fail(function (data) {
         showAnalysisError();
         window.clearInterval(timer2);
      });
   }


   function showResultsOfAnalysis(jsonData)
   {
      var sendValid = true;

      //show the number stats
      $('#txtTotalNumbers').html(jsonData.total);
      $('#txtValidNumbers').html(jsonData.valid_count);
      $('#txtUnknownNumbers').html(jsonData.unknown_count);
      $('#txtInvalidNumbers').html(jsonData.invalid_count);
      $('#txtDuplicateNumbers').html(jsonData.duplicate_count);

      if ((jsonData.invalid_count + jsonData.unknown_count) == jsonData.total)
      {
         //no valid numbers, so can't send
         $('#errNoValidNumbers').show();
         sendValid = false;
      }

      if (jsonData.invalid_count > 0)
      {
         //some rows are invalid and therefore ignored
         $('#errIgnoredRows').show();
         $('#txtInvalidCount').html(jsonData.invalid_count);
      }

      var totalUnits = 0;
      var currency = "ZAR"; //assume ZAR

      //show the send preview of the first 10 sends
      if (jsonData.sms_previews.length == 0)
      {
         var appendNoNetworkHTML = '<h4 colspan="3" align="center">No networks to show.</h4>';

         $('#networkSendBreakdownHolder').append(appendNoNetworkHTML);
      }

      //show the network costs breakdown
      for (var i = 0; i < jsonData.costing.network_totals.length; i++)
      {
         var network = jsonData.costing.network_totals[i];

         //check if it is an unknown network
         if (network.network == "Unknown Network")
         {
            //show a different view for unknown
            var appendNetworkHTML = '<div class="box box-solid"><div class="box-header with-border bg-yellow">'
                    + '<h3 class="box-title" style="color:white;">&nbsp;' + network.network + ' <small style="color:white;">(Currency: ' + network.currency + ')</small></h3>'
                    + '<h3 class="box-title pull-right"><small style="color:white;">' + network.country + '</small></h3>'
                    + '</div><div class="box-body clearfix">'
                    + '<div class="col-lg-4"><strong>' + network.numbers + '</strong> numbers</div>'
                    + '<div class="col-lg-4" style="text-align:center;"><strong>' + network.send_count + '</strong> units</div>'
                    + '<div class="col-lg-4" style="text-align:right;"><strong>' + (network.total_cost / 10000).toFixed(2) + '</strong> cost</div>'
                    + '</div></div>';
         } else
         {
            //show the main display for a network
            var appendNetworkHTML = '<div class="box box-solid"><div class="box-header with-border bg-gray">'
                    + '<h3 class="box-title text-primary"><img src="../img/serviceproviders/' + network.network + '.png"> &nbsp;' + network.network + ' <small>(Currency: ' + network.currency + ')</small></h3>'
                    + '<h3 class="box-title pull-right"><small>' + network.country + '</small> <img src="../img/flags/' + network.country + '.png"/></h3>'
                    + '</div><div class="box-body clearfix">'
                    + '<div class="col-lg-4"><strong>' + network.numbers + '</strong> numbers</div>'
                    + '<div class="col-lg-4" style="text-align:center;"><strong>' + network.send_count + '</strong> units</div>'
                    + '<div class="col-lg-4" style="text-align:right;"><strong>' + (network.total_cost / 10000).toFixed(2) + '</strong> cost</div>'
                    + '</div></div>';
         }


         $('#networkSendBreakdownHolder').append(appendNetworkHTML);

         totalUnits += network.send_count;
         if (network.currency != "" && network.currency != "?")
         {
            currency = network.currency;
         }
      }

      //show the send preview of the first 10 sends
      if (jsonData.sms_previews.length == 0)
      {
         var appendNoPreviewHTML = '<tr>'
                 + '<td colspan="3" align="center">No message previews to show.</td>'
                 + '</tr>';

         $('#tblSendExamplesHolder').append(appendNoPreviewHTML);
      }

      for (var i = 0; i < jsonData.sms_previews.length; i++)
      {
         var preview = jsonData.sms_previews[i];
         var appendPreviewHTML = '<tr>'
                 + '<td>' + preview.msisdn + '</td>'
                 + '<td  style="white-space:pre-wrap;">' + preview.sms_text + '</td>'
                 + '<td>' + preview.units + ' <small class="text-primary pull-right">(' + preview.sms_length + ' chars)</small></td>'
                 + '</tr>';

         $('#tblSendExamplesHolder').append(appendPreviewHTML);
      }

      //set the units and total cost
      $('#txtTotalCost').html('<small><small>[' + currency + ']</small></small> ' + (jsonData.costing.total_cost / 10000).toFixed(2));
      $('#txtTotalUnits').html(totalUnits);

      if (!sendValid)
      {
         $('#btnBuildSend').hide();
         $('#btnSendBulk').hide();
         $('#processing_progress_status').html('Cannot build this bulk send.');
         $('#processing_progress_bar').addClass('progress-bar-danger');
         $('#file_generation_holder').removeClass('box-primary');
         $('#file_generation_holder').addClass('box-danger');
         $('#processing_progress_percent').html('Send invalid.');
         $('#processing_progress_bar').css('width', '100%');
      } else if (jsonData.costing.can_afford == false)
      {
         //cant afford to send, show error and prevent submission
         $('#errNoFunds').show();
         $('#btnBuildSend').hide();
         $('#btnSendBulk').hide();
         $('#processing_progress_status').html('You cannot afford this bulk send.');
         $('#processing_progress_bar').addClass('progress-bar-danger');
         $('#file_generation_holder').removeClass('box-primary');
         $('#file_generation_holder').addClass('box-danger');
         $('#processing_progress_percent').html('Insufficient funds.');
         $('#processing_progress_bar').css('width', '100%');
      } else
      {
         if (jsonData.duplicate_count > 0)
         {
            $('#errDuplicates').show();
            $('#check_remove_duplicates_holder').show();
         }

         $('#btnBuildSend').show();
         $('#btnSendBulk').hide();

         $('#processing_progress_status').html('Ready to build...');
         $('#processing_progress_percent').html('0%');
         $('#processing_progress_bar').css('width', '0%');
      }

      $('#processing_holder').fadeOut(300, function () {

         $('#result_holder').fadeIn(300);
      });
   }

   function showAnalysisError()
   {
      $('#processing_holder').fadeOut(300, function () {

         $('#error_holder').fadeIn(300);
      });
   }

   var sendBuilding = false;
   function buildSend()
   {
      //prevent the user from triggering this repeatedly
      if (!sendBuilding)
      {
         $('#btnBuildSend').html('<span class="glyphicon glyphicon-refresh glyphicon-spin"></span> Building, please wait...');

         sendBuilding = true;
         //reset the progress meter
         $('#processing_progress_bar').css('width', '0%');

         var remove_duplicates = 0;
         if ($('#check_remove_duplicates').is(":checked"))
         {
            remove_duplicates = 1;
         }

         //schedule the checker
         var timer = setInterval(function ()
         {
            checkUpload(preBuildFileName, timer, remove_duplicates);
         }, 3000);

         //start the file generation checker
         checkUpload(preBuildFileName, timer, remove_duplicates);
      }

   }


   function checkUpload(fN, timer, remove_duplicates)
   {
      $.ajax({url: '../php/bulkHandler.php',
         data: {fName: fN, dedup: remove_duplicates},
         type: 'post',
         success: function (output)
         {

            console.log(output);

            obj = JSON.parse(output);
            var pro = parseInt(obj.data.processed);
            var tot = parseInt(obj.data.records);
            $('#processing_progress_status').html('<span class="glyphicon glyphicon-refresh glyphicon-spin"></span> Generating final send file...');
            var percent = Math.round((pro / tot) * 100);
            //var sId = <?php echo $serviceId; ?>;

            if (obj.status == 'COMPLETED')
            {
               window.clearInterval(timer);
               clearInterval(timer);

               //check if there are any valid numbers, and show the send button if there are
               if (tot > obj.data.unknown) {
                  $('#btnBuildSend').fadeOut(200, function () {
                     $('#btnSendBulk').fadeIn(200);
                  });

                  $('#processing_progress_status').html('Ready to send.');
                  $('#processing_progress_bar').addClass('progress-bar-success');
                  $('#file_generation_holder').removeClass('box-primary');
                  $('#file_generation_holder').addClass('box-success');
                  $('#check_remove_duplicates_holder').hide();

                  $('#processing_progress_bar').css('width', '100%');
                  $('#processing_progress_percent').html('100% - Send is ready.');
               }
               else
               {
                  $('#btnBuildSend').fadeOut(200, function () {});

                  //cant afford to send, show error and prevent submission
                  $('#btnBuildSend').hide();
                  $('#btnSendBulk').hide();
                  $('#processing_progress_status').html('None of the numbers are valid.');
                  $('#processing_progress_bar').addClass('progress-bar-danger');
                  $('#file_generation_holder').removeClass('box-primary');
                  $('#file_generation_holder').addClass('box-danger');
                  $('#processing_progress_percent').html('Invalid send.');
                  $('#processing_progress_bar').css('width', '100%');
                  $('#processing_progress_percent').html('Send is invalid.');
                  $('#check_remove_duplicates_holder').hide();
               }

               console.log("STATUS: " + obj.status);
               console.log("RECORDS: " + obj.data.records);
               console.log("PROCESSED: " + obj.data.processed);
               console.log("PORTED: " + obj.data.ported);
               console.log("DUPLICATES: " + obj.data.duplicate);
               console.log("EMPTY: " + obj.data.empty);
               console.log("INVALID: " + obj.data.invalid);
               console.log('UNKNOWN: ' + obj.data.unknown);

               validNumbers = tot;

            } else
            {
               $('#processing_progress_bar').css('width', percent + '%');
               $('#processing_progress_percent').html(percent + '%');
            }
         }
      });

   }

   function backBtn()
   {
      // var client = "<?php echo $_POST['client']; ?>";
      window.location = "../pages/bulksms.php?client=" + client;
   }

   function validateBulk()
   {

      var form = document.createElement("form");
      form.setAttribute("method", "post");
      form.setAttribute("action", "successbulkfile.php");
      var hiddenField = document.createElement("input");
      hiddenField.setAttribute("type", "hidden");
      hiddenField.setAttribute("name", "fName");
      hiddenField.setAttribute("value", fName);
      var hiddenFieldCol = document.createElement("input");
      hiddenFieldCol.setAttribute("type", "hidden");
      hiddenFieldCol.setAttribute("name", "colCount");
      hiddenFieldCol.setAttribute("value", colCount);
      var hiddenField2 = document.createElement("input");
      hiddenField2.setAttribute("type", "hidden");
      hiddenField2.setAttribute("name", "sId");
      hiddenField2.setAttribute("value", sId);
      var hiddenField3 = document.createElement("input");
      hiddenField3.setAttribute("type", "hidden");
      hiddenField3.setAttribute("name", "smsText");
      hiddenField3.setAttribute("value", smsTxt);
      var hiddenField4 = document.createElement("input");
      hiddenField4.setAttribute("type", "hidden");
      hiddenField4.setAttribute("name", "camCode");
      hiddenField4.setAttribute("value", camCode);
      var hiddenField5 = document.createElement("input");
      hiddenField5.setAttribute("type", "hidden");
      hiddenField5.setAttribute("name", "camId");
      hiddenField5.setAttribute("value", camId);
      var hiddenField6 = document.createElement("input");
      hiddenField6.setAttribute("type", "hidden");
      hiddenField6.setAttribute("name", "refer");
      hiddenField6.setAttribute("value", refer);
      var hiddenField7 = document.createElement("input");
      hiddenField7.setAttribute("type", "hidden");
      hiddenField7.setAttribute("name", "status");
      hiddenField7.setAttribute("value", status);
      var hiddenField8 = document.createElement("input");
      hiddenField8.setAttribute("type", "hidden");
      hiddenField8.setAttribute("name", "sTime");
      hiddenField8.setAttribute("value", sTime);
      var hiddenField9 = document.createElement("input");
      hiddenField9.setAttribute("type", "hidden");
      hiddenField9.setAttribute("name", "tfn");
      hiddenField9.setAttribute("value", trueFileName);

      var smsRandCheck = document.createElement('input');
      smsRandCheck.setAttribute('name', 'smsRandCheck');
      smsRandCheck.setAttribute('type', 'hidden');
      smsRandCheck.setAttribute('value', "<?php echo urlencode($_SESSION['randomSendCheck']); ?>");

      form.appendChild(hiddenField);
      form.appendChild(hiddenFieldCol);
      form.appendChild(hiddenField2);
      form.appendChild(hiddenField3);
      form.appendChild(hiddenField4);
      form.appendChild(hiddenField5);
      form.appendChild(hiddenField6);
      form.appendChild(hiddenField7);
      form.appendChild(hiddenField8);
      form.appendChild(hiddenField9);
      form.appendChild(smsRandCheck);
      document.body.appendChild(form);
      form.submit();
   }
</script>

<!-- Template Footer -->

