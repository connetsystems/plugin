<?php
$cred = getCreditOnly($_SESSION['serviceId']);
$_SESSION['serviceCredit'] = $cred;
$s = $_SESSION['serviceCredit'];
$sc = ($s / 10000);
if ($sc > 1000) {
   $scS = 'success';
} elseif ($sc < 1000 && $sc > 0) {
   $scS = 'warning';
   # code...
} elseif ($sc <= 0) {
   $scS = 'danger';
   # code...
}
$sc = number_format($sc, 2, '.', ' ');

$roles = $_SESSION['roles'];
//NIEL $pgUrl = strrchr(realpath($_SERVER['SCRIPT_NAME']), '/');
//NIEL $pgUrl = '/pages' . $pgUrl;
$pgUrl = $_SERVER['SCRIPT_NAME'];

//ob_end_clean();
//var_dump($pgUrl);
//die();

$pgId = getRoleIDFromUrl($pgUrl);
$pgId = substr($pgId, 0, 1);

////////////////////////////////////////////////
?>

<!-- Right side column. Contains the navbar and content of the page -->
<aside class="left-side sidebar-offcanvas">
   <!-- sidebar: style can be found in sidebar.less -->
   <section class="sidebar">
      <ul class="sidebar-menu">
         <?php
         echo '<div class="callout callout-warning" style="margin-bottom:0px; border-color:#eaeaea; background-color:#eaeaea">';
         echo '<h4>' . $_SESSION['accountName'] . '</h4>';
         echo '<p>Service: ' . $_SESSION['serviceName'] . '</p>';

         echo '<span class="label label-' . $scS . '" >R&nbsp;&nbsp;' . $sc . '</span>';

         echo '<p style="margin-top:8px;">' . $_SESSION['serviceBillingType'] . '</p>';
         echo '</div>';
         ?>
         <li>
            <a href="/home.php">
               <i class="glyphicon glyphicon-home"></i><span>Dashboard</span>
            </a>
         </li>
         <!--i class="fa fa-home"></i><span>Dashboard</span-->
         <?php
         $openDivCount = 0;
         $active = '';

         for ($i = 0; $i < count($roles); $i++) {
            $roleIC = $roles[$i]['role_icon'];
            $roleNA = $roles[$i]['role_name'];
            $roleUR = $roles[$i]['role_url'];
            $roleID = $roles[$i]['role_id'];
            $roleID_sub = substr($roleID, 0, 1);
            //    echo "<br>2-".$pgId."-=-".$roleID_sub;

            if ($roleID % 100 == 0) {
               if ($openDivCount == 0) {
                  $openDivCount = 1;
                  //$rId = $roleID%10;
                  //echo "<br>2-".$roleID."-";
                  if ($pgId == $roleID_sub) {
                     $active = 'active';
                  } else {
                     $active = '';
                  }
                  //open div here
                  echo '<li class="treeview ' . $active . ' ">';
                  echo '<a href="#">';
                  echo '<i class="' . $roleIC . '"></i>';
                  echo '<span>' . $roleNA . '</span>';
                  echo '<i class="fa pull-right fa-angle-left"></i>';
                  echo '</a>';
                  echo '<ul class="treeview-menu" style="display:none;">';
               } else {
                  echo '</ul>';
                  echo '</li>';

                  if ($pgId == $roleID_sub) {
                     $active = 'active';
                  } else {
                     $active = '';
                  }

                  echo '<li class="treeview ' . $active . ' ">';
                  echo '<a href="#">';
                  echo '<i class="' . $roleIC . '"></i>';
                  echo '<span>' . $roleNA . '</span>';
                  echo '<i class="fa pull-right fa-angle-left"></i>';
                  echo '</a>';
                  echo '<ul class="treeview-menu" style="display:none;">';
                  //close div here
                  //then open new div after here
               }
            } else {
               //DOUG: THis is a temporary hack to prevent PREPAID resellers from seeing the update credits menu item
               $no_show_of_update_credits = false;
               if (isset($_SESSION['resellerId']) && $_SESSION['serviceId'] == $_SESSION['resellerId'] && $_SESSION['serviceBillingType'] == "PREPAID" && $roleUR == '/pages/updatecredits.php') {
                  $no_show_of_update_credits = true;
               }

               if (!$no_show_of_update_credits) {
                  //not update credits or reseller so lets move on
                  if ($pgUrl == $roleUR) {
                     $active2 = ' class="active" ';
                  } else {
                     $active2 = '';
                  }

                  echo '<li' . $active2 . '>';
                  echo '<a href="' . $roleUR . '">';
                  echo '<i class="' . $roleIC . '"></i> <span>' . $roleNA . '</span>';
                  echo '</a>';
                  echo '</li>';
               }
            }
         }
         echo '</ul>';
         echo '</li>';
         //close last div last
         ?>
      </ul>
   </section>
</aside>