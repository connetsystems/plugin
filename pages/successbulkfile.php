<?php
//prevents caching of form data
header("Cache-Control: no-store, no-cache, must-revalidate, max-age=0");
header("Cache-Control: post-check=0, pre-check=0", false);
header("Pragma: no-cache");

/**
 * Include the header template which sets up the HTML
 *
 * Don't forget to include template_import_script before any Javascripting
 * Don't forget to include template footer.php at the bottom of the page as well
 */
$headerPageTitle = "Bulk Send Status"; //set the page title for the template import
TemplateHelper::initialize();

//prevents resubmission of batches
/*
  if($_POST['fName'] == $_SESSION['prevent_resubmit_fName'] && $_POST['tfn'] == $_SESSION['prevent_resubmit_tfn'])
  {
  unset($_SESSION['prevent_resubmit_fName']);
  unset($_SESSION['prevent_resubmit_tfn']);
  header('Location: batchlist.php?client='.$_POST['sId'] );
  exit();
  } */



include("../php/uploadbulkNew.php");

/*
  require_once('ConnetAutoloader.php');
  preset::base();
  clog::setLogFilename('/var/log/connet/plugin-website.log');
  clog::setLevel(clog::TRACE);
 */


set_time_limit(360);

if (isset($_POST['fName']) && $_POST['fName'] != '') {
   if (isset($_POST['smsRandCheck']) && $_POST['smsRandCheck'] == $_SESSION['randomSendCheck']) {
      $_SESSION['randomSendCheck'] = rand(0, 1000);
      //this far prevents us from the resubmission problem
      $_SESSION['prevent_resubmit_fName'] = $_POST['fName'];
      $_SESSION['prevent_resubmit_tfn'] = $_POST['tfn'];

      $sms_text = $_POST['smsText'];
      $col_count = $_POST['colCount'];

      $tfn = 'csv_filename=' . $_POST['tfn'];
      $local_filename = '../tempFiles/' . $_POST['fName'];
      $remote_filename = '/var/spool/connet/plugin/' . basename($_POST['fName']);

      //$checkSafe = checkIfRefresh($_SESSION['userId'], $_POST['sId'], $remote_filename);
      $checkSafe = 0;

      if ($checkSafe == 0) {
         $placeholder_numbers = helperSMSGeneral::getSMSPlaceholderNumbersAsArray($sms_text);
         $replace_placeholder_array = array();

         //we need to check for placeholders and increment them by 4
         for ($i = 0; $i < count($placeholder_numbers); $i ++) {
            //check if the slot {1} or {2} ect exists and then add 4 to it in order to compensate for niel 4 extra fields
            $placeholder_string = '{' . $placeholder_numbers[$i] . '}';

            if (strpos($sms_text, $placeholder_string) !== false) {
               $replace_placeholder_string = '{' . ($placeholder_numbers[$i] + 4) . '}';
               $replace_placeholder_array[$placeholder_string] = $replace_placeholder_string;
            }
         }

         //now replace all the strings
         $sms_text = strtr($sms_text, $replace_placeholder_array);

         $moveResult = nfs_upload_file($local_filename, $remote_filename);

         if (filesize($remote_filename) == filesize($local_filename)) {
            if ($moveResult == 'success') {
               $date = date("Y/m/d H:i:s");

               $result = insertIntoBatchBatch($_SESSION['userId'], $_POST['sId'], $sms_text, $_POST['camCode'], $remote_filename, $_POST['status'], $_POST['sTime'], $date, $tfn);
               //echo '<br>res='.$result.'=';
               $id = getNewBatchId($remote_filename);
               //echo '<br>remfn='.$remote_filename.'=';

               insertIntoCampaignBatch($id, $_POST['refer'], $_POST['camId']);
            } else {
               $result = 3; //Error moving file here.
            }
         } else {
            $result = 4;
         }
      } else {
         $result = 2;
      }
   } else {
      $result = 5;
   }
}


////////////////////////////////////////////////
////////////////////////////////////////////////
////////////////////////////////////////////////
?>

<aside class="right-side">
   <section class="content-header">
      <h1>
         Bulk Send Status
      </h1>
   </section>

   <section class="content">
      <div class="row col-lg-12">
         <div class="box box-warning" style="border-top-color:<?php echo $accRGB; ?>;margin-top:0px;">
            <div class="box-body" style="height:116px;">
               <?php
               if ($result == 1) {
                  echo '<div class="callout callout-success" style="margin-bottom:0px;">';
                  //echo '<h4>Success Bulk SMS</h4>';
                  if ($_POST['status'] == 'Paused') {
                     echo '<p>Your batch has been successfully submitted but it is currently paused.</p>';
                  } elseif ($_POST['status'] == 'Scheduled') {
                     echo '<p>Your batch has been successfully scheduled for ' . $_POST['sTime'] . '.</p>';
                  } else {
                     echo '<p>Your batch has been successfully submitted for immediate release.</p>';
                  }
                  echo '</div>';
               } elseif ($result == 2) {
                  echo '<div class="callout callout-warning" style="margin-bottom:0px;">';
                  echo '<h4>No need to resubmit!</h4>';
                  echo '<p>Your batch has ALREADY been succesfully submitted</p>';
                  echo '</div>';
               } elseif ($result == 3) {
                  echo '<div class="callout callout-danger" style="margin-bottom:0px;">';
                  echo '<h4>Error Bulk SMS</h4>';
                  echo '<p>Error moving file! Please try again. If the error persists; contact support.</p>';
                  echo '</div>';
               } elseif ($result == 4) {
                  echo '<div class="callout callout-danger" style="margin-bottom:0px;">';
                  echo '<h4>Error Bulk SMS</h4>';
                  echo '<p>Mismatched file sizes! Please try again. If the error persists; contact support.</p>';
                  echo '</div>';
               } elseif ($result == 5) {
                  echo '<div class="callout callout-warning" style="margin-bottom:0px;">';
                  echo '<h4>Notice</h4>';
                  echo '<p>Your batch has already been sent.</p>';
                  echo '</div>';
               } else {
                  echo '<div class="callout callout-danger" style="margin-bottom:0px;">';
                  echo '<h4>Error Bulk SMS</h4>';
                  echo '<p>Something went wrong with your batch! Please try again. If the error persists; contact support.</p>';
                  echo '</div>';
               }
               ?>
               <div class="form-group">
                  <a onclick="gotoViewBatch();" class="btn btn-block btn-social btn-dropbox " style="background-color:<?php echo $accRGB; ?>; border: 2px solid; border-radius: 6px !important; float:left; width: 150px; margin-top:5px;">
                     <i class="fa fa-save"></i>View Batch
                  </a>
                  <a onclick="sendAnotherBatch();" class="btn btn-block btn-social btn-dropbox " style="background-color:<?php echo $accRGB; ?>; border: 2px solid; border-radius: 6px !important; float:left; width: 180px; margin-top:5px;">
                     <i class="fa fa-save"></i>Send Another Batch
                  </a>
               </div>
            </div>
         </div>
      </div>
   </section>
</aside>

<!--  THE JAVASCRIPT IMPORT ALWAYS APPEARS BEFORE LOCAL JAVASCRIPT AT THE BOTTOM OF THE PAGE -->
<?php include("template_import_script.php"); //must import all scripts first ?>
<!-- END JAVASCRIPT IMPORT -->

<script type="text/javascript">
   function sendAnotherBatch()
   {
      window.location = "../pages/bulksms.php"/* + "&range=" + repRange + "&range1=" + repRange1 + "&sents=" + <?php echo $sentCount; ?> + "&from=1&controller=1&sort=<?php echo $sort; ?>&mcc=" + mcc + "&smsc=" + smsc;*/
   }

   function gotoViewBatch()
   {
      var serviceName = <?php echo $_POST['sId']; ?>;
      window.location = "../pages/batchlist.php?client=" + serviceName;/* + "&range=" + repRange + "&range1=" + repRange1 + "&sents=" + <?php echo $sentCount; ?> + "&from=1&controller=1&sort=<?php echo $sort; ?>&mcc=" + mcc + "&smsc=" + smsc;*/
   }
</script>

<script type="text/javascript">
   $(window).load(function () {
      $(".loader").fadeOut("slow");
      $(".loaderIcon").fadeOut("slow");
   });
</script>

<!-- Template Footer -->

