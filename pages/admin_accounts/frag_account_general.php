<?php

   /**
    * This script is called by the manage users page, and runs certain ajax based tasks
    * @author - Doug Jenkinson
    */
   require_once("../../php/allInclusive.php");
   require_once("../../PluginAPI/PluginAPIAutoload.php");

   session_start();

   if(isset($_POST['account_id']))
   {
      $account_id = $_POST['account_id'];
   }
   else if(isset($_GET['account_id']))
   {
      $account_id = $_GET['account_id'];
   }
   else
   {
      echo '<p class="text-error">An error occurred, please refresh your browser.</p>';
      die();
   }

   //get the account object
   $account_obj = new Account($account_id);

   //get the user object
   $user_obj = new User($user_id);

?>

   <div class="row">
      <div class="col-lg-8">
         <p class="lead">You can manage the general configuration of this account using this tab.</p>
      </div>
      <div class="col-lg-4">

      </div>
   </div>
   <div class="row">

      <!-- ACCOUNT STYLING SECTION -->
      <div class="col-lg-9">
         <div class="box box-primary">
            <div class="box-header with-border">
               <h3 class="box-title"><i class="fa fa-paint-brush"></i> Styling Configuration</h3>
            </div>
            <div class="box-body">
               <h3>Main Styling</h3>
               <div class="row">
                  <div class="col-lg-6">
                     <label for="headerLogo">Header Logo:</label>
                     <img src="<?php echo $account_obj->header_logo; ?>" class="img-responsive thumbnail" id="image_header_logo" alt="Account Logo" style="max-height:80px; margin-bottom:5px;"/>
                     <button class="btn btn-default btn-xs btn-upload-image" connet-field-name="header_logo" style="margin:2px 0 10px 0;"><i class="fa fa-cloud-upload" ></i> Replace Image</button>
                  </div>
                  <div class="col-lg-6">
                     <label for="headerLogo">Login Logo:</label>
                     <img src="<?php echo $account_obj->login_logo; ?>" class="img-responsive thumbnail" id="image_login_logo" alt="Login Logo" style="max-height:80px; margin-bottom:5px;"/>
                     <button class="btn btn-default btn-xs btn-upload-image" connet-field-name="login_logo" style="margin:2px 0 10px 0;"><i class="fa fa-cloud-upload"></i> Replace Image</button>
                  </div>
               </div>
               <div class="row">
                  <div class="col-lg-6">
                     <label for="headerLogo">Tab Logo:</label>
                     <img src="<?php echo $account_obj->tab_logo; ?>" class="img-responsive thumbnail" id="image_tab_logo" alt="Tab Logo" style="max-height:80px; margin-bottom:5px;"/>
                     <button class="btn btn-default btn-xs btn-upload-image" connet-field-name="tab_logo" style="margin:2px 0 10px 0;"><i class="fa fa-cloud-upload"></i> Replace Image</button>
                  </div>
                  <div class="col-lg-6">
                     <label for="primary_colour">Primary Colour <small id="colourSavingStatus"></small></label>
                     <div id="accountColour" class="input-group colorpicker-component">
                        <input type="text" class="form-control"  value="<?php echo $account_obj->account_colour; ?>"/>
                        <span class="input-group-addon"><i></i></span>
                     </div>
                  </div>
               </div>
               <hr/>
               <h3>Email Styling</h3>
               <div class="row">
                  <div class="col-lg-6">
                     <label for="headerLogo">Header Logo:</label>
                     <img src="<?php echo $account_obj->email_header; ?>" class="img-responsive thumbnail" id="image_email_header" alt="Email Header" style="max-height:80px; margin-bottom:5px;"/>
                     <button class="btn btn-default btn-xs btn-upload-image" connet-field-name="email_header" style="margin:2px 0 10px 0;"><i class="fa fa-cloud-upload" ></i> Replace Image</button>
                  </div>
                  <div class="col-lg-6">
                     <label for="headerLogo">Login Logo:</label>
                     <img src="<?php echo $account_obj->email_footer; ?>" class="img-responsive thumbnail" id="image_email_footer" alt="Email Footer" style="max-height:80px; margin-bottom:5px;"/>
                     <button class="btn btn-default btn-xs btn-upload-image" connet-field-name="email_footer" style="margin:2px 0 10px 0;"><i class="fa fa-cloud-upload"></i> Replace Image</button>
                  </div>
               </div>
               <div class="row">
                  <div class="col-lg-12">
                     <h4>
                        <small>Email From Address:</small><br/>
                        <span class="connet-field-display"><?php echo (isset($account_obj->email_from) && $account_obj->email_from != "" ? $account_obj->email_from : "Not set."); ?></span>
                        <a href="#" class="btn-frag-edit-account-property connet-small-link"
                           connet-id="<?php echo $account_id; ?>"
                           connet-value="<?php echo htmlspecialchars($account_obj->email_from); ?>"
                           connet-ajax-task="save_account_field_value"
                           connet-field="email_from"
                           connet-title="Email From Address (example@example.co.za)">[edit]</a>
                     </h4>
                  </div>
               </div>
            </div>
         </div>
      </div>

      <!-- THE USER BATCH ACTIVITY -->
      <div class="col-lg-3">
         <div class="box box-primary">
            <div class="box-header with-border">
               <h3 class="box-title"><i class="fa fa-link"></i> URL Configuration</h3>
            </div>
            <div class="box-body">
               <div class="row">
                  <div class="col-lg-12">
                     <h4>
                        <small>Account URL Prefix:</small><br/>
                        <span class="connet-field-display"><?php echo (isset($account_obj->url_prefix) && $account_obj->url_prefix != "" ? $account_obj->url_prefix : "Not set."); ?></span>
                        <a href="#" class="btn-frag-edit-account-property connet-small-link"
                           connet-id="<?php echo $account_id; ?>"
                           connet-value="<?php echo htmlspecialchars($account_obj->url_prefix); ?>"
                           connet-ajax-task="save_account_field_value"
                           connet-field="url_prefix"
                           connet-title="Account URL Prefix">[edit]</a>
                        <br/><br/>
                        <small>
                           The URL will appear like so: <br/>
                           <strong>http:// <span class="connet-field-display"><?php echo (isset($account_obj->url_prefix) && $account_obj->url_prefix != "" ? $account_obj->url_prefix : "Not set."); ?></span>.connet-systems.com/</strong>
                        </small>
                     </h4>

                  </div>
                  <div class="col-lg-12">
                     <h4>
                        <small>Account URL Suffix:</small><br/>
                        <span class="connet-field-display"><?php echo (isset($account_obj->url_suffix) && $account_obj->url_suffix != "" ? $account_obj->url_suffix : "Not set."); ?></span>
                        <a href="#" class="btn-frag-edit-account-property connet-small-link"
                           connet-id="<?php echo $account_id; ?>"
                           connet-value="<?php echo htmlspecialchars($account_obj->url_suffix); ?>"
                           connet-ajax-task="save_account_field_value"
                           connet-field="url_suffix"
                           connet-title="Account URL Suffix">[edit]</a>
                        <br/><br/>
                        <small>
                           The URL will appear like so: <br/>
                           <strong>http://connet-systems.com/<span class="connet-field-display"><?php echo (isset($account_obj->url_suffix) && $account_obj->url_suffix != "" ? $account_obj->url_suffix : "Not set."); ?></span></strong>
                        </small>
                     </h4>
                  </div>
               </div>
            </div>
         </div>
         </div>
      </div>
   </div>


<!-- MODALS HERE -->
<!-- THE EDIT CONTACT MODAL -->
<div id="modalImageUpload" class="modal fade" tabindex="-1" role="dialog" data-backdrop="static">
   <div class="modal-dialog">
      <div class="modal-content">
         <div class="modal-header" >
            <button type="button" class="close" data-dismiss="modal" aria-label="Close" id="btnUploadModalCloseTop"><span aria-hidden="true">&times;</span></button>
            <h4 class="modal-title" id="modalImageUploadTitle">Logo Upload</h4>
         </div>
         <div class="modal-body">
            <div class="alert alert-danger" style="display:none;" id="modalImageUploadAlert">
               <h4><i class="icon fa fa-ban"></i> Error!</h4>
               <p id="modalImageUploadAlertText">Unfortunately there was an error, please try again.</p>
            </div>
            <!-- for contact file uploading -->
            <div id="modalImageUploadBody">
               <form id="uploadImageForm" action="../php/ajaxManageAccountHandler.php" method="post">
                  <div  class="row">
                     <div class="col-sm-12">
                        <label for="import_file">Select your image:</label>
                        <input name="imageFile" id="imageFile" type="file" autocomplete="off"/>
                        <input type="hidden" name="task" value="ajax_image_upload"/>
                        <input type="hidden" id="uploadFieldName" name="fieldName" value=""/>
                        <input type="hidden" name="accountId" value="<?php echo $account_id; ?>"/>
                     </div>
                  </div>
               </form>
               <div class="row" id="importImageProgressHolder" style="display:none;">
                  <div class="col-sm-12">
                     <h3>Uploading image... Please wait...
                        <br/><small>This process can take long with large files.</small></h3>
                     <div class="progress">
                        <div class="progress-bar progress-bar-striped" id="imageUploadProgressBar" role="progressbar" aria-valuenow="2" aria-valuemin="0" aria-valuemax="100" style="min-width:2em; width:2%;">
                           2%
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
         <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal" id="btnUploadModalCloseBottom">Close</button>
            <button type="button" class="btn btn-primary" id="btnUploadModalSave">Start Upload</button>
         </div>
      </div><!-- /.modal-content -->
   </div><!-- /.modal-dialog -->
</div><!-- /.modal -->


<!-- MODALS HERE -->
<!-- THE EDIT CONTACT MODAL -->
<div id="modalFragEditField" class="modal fade" tabindex="-1" role="dialog" data-backdrop="static">
   <div class="modal-dialog">
      <div class="modal-content">
         <div class="modal-header" >
            <button type="button" class="close" data-dismiss="modal" aria-label="Close" id="btnModalCloseTop"><span aria-hidden="true">&times;</span></button>
            <h4 class="modal-title" id="modalFragTitle">Add A New Contact Group</h4>
         </div>
         <div class="modal-body">
            <div class="alert alert-danger" style="display:none;" id="modAlert">
               <h4><i class="icon fa fa-ban"></i> Error!</h4>
               <p id="modAlertText">Unfortunately there was an error, please try again.</p>
            </div>
            <div class="form-group">
               <label for="modalFragInputValue" id="modalFragInputLabel">Label</label>
               <input type="text" class="form-control" id="modalFragInputValue" placeholder="" value="" />
            </div>
            <small>Please edit the value and click save to commit, or close to cancel.</small>
         </div>
         <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal" id="btnModalCloseBottom">Close</button>
            <button type="button" class="btn btn-primary" id="btnFragModalSave" style="background-color:<?php echo $_SESSION['accountColour']; ?>;border: 2px solid; border-radius: 6px !important;">Save</button>
            <input type="hidden" class="form-control" id="modalFragInputField" value=""/>
            <input type="hidden" class="form-control" id="modalFragInputId" value=""/>
            <input type="hidden" class="form-control" id="modalFragInputTask" value=""/>
         </div>
      </div><!-- /.modal-content -->
   </div><!-- /.modal-dialog -->
</div><!-- /.modal -->


   <script type="text/javascript">
      var fragAccountId = <?php echo (isset($account_id) ? $account_id : '-1') ?>;

      $(document).ready(function (e)
      {
         /**
          * This button loads the generic field editor by launching a modal a modal
          */
         $('.btn-frag-edit-account-property').on("click", function (e)
         {
            e.preventDefault();

            var fieldName = $(this).attr('connet-field');
            var fieldTitle = $(this).attr('connet-title');
            var fieldId = $(this).attr('connet-id');
            var fieldTask = $(this).attr('connet-ajax-task');
            var fieldValue = $(this).attr('connet-value');

            $('#modalFragTitle').html("Edit Field: " + fieldTitle);
            $('#modalFragInputLabel').html(fieldTitle);
            $('#modalFragInputValue').val(fieldValue);
            $('#modalFragInputField').val(fieldName);
            $('#modalFragInputId').val(fieldId);
            $('#modalFragInputTask').val(fieldTask);

            $('#modalFragEditField').modal('show');
         });

         $('#btnFragModalSave').on("click", function (e)
         {
            e.preventDefault();

            lockFragModalBusy("Saving...");

            var fieldName = $('#modalFragInputField').val();
            var fieldValue = $('#modalFragInputValue').val();
            var fieldTask = $('#modalFragInputTask').val();
            var fieldId = $('#modalFragInputId').val();

            $.post("../php/ajaxManageAccountHandler.php", {task: fieldTask, accountId: fragAccountId, fieldId: fieldId, fieldName:fieldName, fieldValue:fieldValue}).done(function (data) {
               var json = $.parseJSON(data);
               if (json.success)
               {
                  unlockFragModalBusy();

                  //ocate the html elements that hold this data and update them for display
                  $("a[connet-id='"+fieldId+"'][connet-field='"+fieldName+"']" ).attr('connet-value', fieldValue);
                  $("a[connet-id='"+fieldId+"'][connet-field='"+fieldName+"']" ).parent().find('.connet-field-display').html(fieldValue);
                  $('#modalFragEditField').modal('hide');
               }
               else
               {
                  unlockFragModalBusy();
                  showModalError("A server error occurred and we could not save the data, please contact technical support.");
               }
            }).fail(function (data) {
               unlockFragModalBusy();
               showModalError("A server error occurred and we could not save the data, please contact technical support.");
            });
         });


         /**
          * init the color picker for our form
          */
         $('#accountColour').colorpicker().on('hidePicker', function(e) {
            console.log("Change colour? " + e.color.toHex());

            if(confirm("Do you want to save this colour ("+e.color.toHex()+")?"))
            {

               $('#colourSavingStatus').html('<span class="glyphicon glyphicon-refresh glyphicon-spin"></span> Saving colour...');
               var fieldName = "account_colour";
               var fieldValue = e.color.toHex();

               $.post("../php/ajaxManageAccountHandler.php", {task: 'save_account_field_value', accountId: fragAccountId, fieldName:fieldName, fieldValue:fieldValue}).done(function (data) {
                  var json = $.parseJSON(data);
                  if (json.success)
                  {
                     $('#colourSavingStatus').html('Saved.');
                  }
                  else
                  {
                     $('#colourSavingStatus').html('Error saving.');
                     alert("A server error occurred and we could not save the data, please contact technical support.");
                  }
               }).fail(function (data) {
                  $('#colourSavingStatus').html('Error saving.');
                  alert("A server error occurred and we could not save the data, please contact technical support.");
               });
            }
         });


         //show the upload form for the appropriate image type
         $(".btn-upload-image").on("click", function (e) {
            e.preventDefault();
            $('#import_file').val("");

            var fieldName = $(this).attr('connet-field-name');
            $('#uploadFieldName').val(fieldName);

            $('#modalImageUpload').modal('show');
         });


         /*****************************************************
          * SCRIPT FOR HANDLING THE IMAGE UPLOADING
          *****************************************************/
         $("#btnUploadModalSave").on("click", function (e)
         {
            e.preventDefault();
            hideUploadModalError();
            lockUploadModalBusy('Uploading...');


            //Some validation
            var startUpload = true;
            if ($('#imageFile').val() == "")
            {
               startUpload = false;
               showUploadModalError("Please select a file to upload.");
               unlockUploadModalBusy();
            }
            else
            {
               //check file properties
               var file = $('#imageFile')[0].files[0];

               console.log("File type: "+file.name);

               //check file type
               if (!file.name.match(/\.(jpg|jpeg|png|gif)$/)) //for 2mb - 2097152
               {
                  startUpload = false;
                  showUploadModalError("Your file type is invalid. Please select a valid image.");
                  unlockUploadModalBusy();
               }
            }

            //if all is valid then we proceed
            if (startUpload)
            {
               //fade out the form and fade in the progress bar, then start the actual upload
               $('#uploadImageForm').fadeOut(500, function ()
               {
                  $('#importImageProgressHolder').fadeIn(500, function ()
                  {
                     $('#importImageProgressHolder').show();
                     startAjaxUpload();
                  });
               });
            }
         });
      });

      //This function runs the ajax upload submitter from the jquery.form library
      var uploadError = "";
      function startAjaxUpload() {
         $('#imageUploadProgressBar').addClass('active');

         //$('#loader-icon').show();
         $('#uploadImageForm').ajaxSubmit({
            beforeSubmit: function () {
               updateProgress(2);
            },
            uploadProgress: function (event, position, total, percentComplete) {
               updateProgress(percentComplete);
            },
            success: function (responseText, statusText, xhr, form) {
               if (responseText != "")
               {
                  var jsonObj = $.parseJSON(responseText)

                  //upload was successful? redirect to the data assignment page so the user can configure the data
                  if (jsonObj.success == true)
                  {
                     endProgress();

                     location.reload();
                  }
                  else
                  {
                     //shit, there was a bloody error, act accordingly
                     uploadError = jsonObj.reason;
                     $('#modalImageUploadAlert').addClass('hidden');
                     $('#importImageProgressHolder').fadeOut(300, function () {
                        $('#uploadImageForm').fadeIn(300, function () {
                           showUploadModalError("There was an error uploading this file. Reason: " + uploadError);
                           unlockUploadModalBusy();
                        });
                     });
                  }
               }
               else
               {
                  //dismal error, act accordingly as well
                  $('#modalImageUploadAlert').addClass('hidden');
                  $('#importImageProgressHolder').fadeOut(300, function () {
                     $('#uploadImageForm').fadeIn(300, function () {
                        showUploadModalError("There was an error uploading this file. Please try again.");
                        unlockUploadModalBusy();
                     });
                  });
               }
               //$('#loader-icon').hide();
            },
            resetForm: false
         });
         return false;
      }


      //this locks the modal so the user can't do anything, useful when runing ajax calls off of it
      var prevButtonContent = "";
      function lockFragModalBusy(busyMessage)
      {
         prevButtonContent = $('#btnFragModalSave').html();
         $('#btnModalCloseTop').prop("disabled", true);
         $('#btnModalCloseBottom').prop("disabled", true);
         $('#btnFragModalSave').prop("disabled", false);
         $('#btnFragModalSave').html('<span class="glyphicon glyphicon-refresh glyphicon-spin"></span> ' + busyMessage);
      }

      //unlocks the modal so the user can close it
      function unlockFragModalBusy()
      {
         $('#btnModalCloseTop').prop("disabled", false);
         $('#btnModalCloseBottom').prop("disabled", false);
         $('#btnFragModalSave').prop("disabled", false);
         $('#btnFragModalSave').html(prevButtonContent);
         prevButtonContent = "";
      }

      //shows the error in the MODAL with a custom error message
      function showFragModalError(message)
      {
         $("#modAlertText").html(message);
         $("#modAlert").show();
      }

      //hides the modal error and resets the message
      function hideFragModalError()
      {
         $("#modAlertText").html();
         $("#modAlert").hide();
      }


      //this locks the modal so the user can't do anything, useful when runing ajax calls off of it
      function lockUploadModalBusy(busyMessage)
      {
         prevButtonContent = $('#btnUploadModalSave').html();
         $('#btnUploadModalCloseTop').prop("disabled", true);
         $('#btnUploadModalCloseBottom').prop("disabled", true);
         $('#btnUploadModalSave').prop("disabled", false);
         $('#btnUploadModalSave').html('<span class="glyphicon glyphicon-refresh glyphicon-spin"></span> ' + busyMessage);
      }

      //unlocks the modal so the user can close it
      function unlockUploadModalBusy()
      {
         $('#btnUploadModalCloseTop').prop("disabled", false);
         $('#btnUploadModalCloseBottom').prop("disabled", false);
         $('#btnUploadModalSave').prop("disabled", false);
         $('#btnUploadModalSave').html(prevButtonContent);
         prevButtonContent = "";
      }

      //shows the error in the MODAL with a custom error message
      function showUploadModalError(message)
      {
         $("#modalImageUploadAlertText").html(message);
         $("#modalImageUploadAlert").show();
      }

      //hides the modal error and resets the message
      function hideUploadModalError()
      {
         $("#modalImageUploadAlertText").html();
         $("#modalImageUploadAlert").hide();
      }

      //updates the progress bar to show that the upload is complete
      function endProgress()
      {
         $('#imageUploadProgressBar').html('File upload complete, moving on...');
      }

      //updates the progress bar with the correct numbers and width
      function updateProgress(percentage)
      {
         if (percentage > 100)
            percentage = 100;
         $('#imageUploadProgressBar').css('width', percentage + '%');
         $('#imageUploadProgressBar').html(percentage + '%');
      }

   </script>


