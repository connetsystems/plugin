<?php

   /**
    * This script is called by the manage services page, and runs certain ajax based tasks
    * @author - Doug Jenkinson
    */
   require_once("../../php/allInclusive.php");
   require_once("../../PluginAPI/PluginAPIAutoload.php");

   session_start();

   if(isset($_POST['account_id']))
   {
      $account_id = $_POST['account_id'];
   }
   else if(isset($_GET['account_id']))
   {
      $account_id = $_GET['account_id'];
   }
   else
   {
      echo '<p class="text-error">An error occurred, please refresh your browser.</p>';
      die();
   }

   //get the user object
   $account_obj = new Account($account_id);
   $account_user_objects = $account_obj->getAllAccountUsersAsObjects();

?>
<div class="row">
   <div class="col-lg-10">
      <p class="lead">You can view and manage all users who are attached to this account as their primary account.</p>
   </div>
   <div class="col-lg-2">
      <!-- <button id="btnAddUserToService" class="btn btn-primary btn-sm btn-block"><i class="fa fa-user-plus"></i> Add User To Service</button> -->
   </div>
</div>
<hr/>

<?php if(count($account_user_objects) == 0) { ?>
    <h4>No users have been added to this account.</h4>
<?php } ?>

<?php foreach($account_user_objects as $count => $user) { ?>
   <div class="box box-solid">
      <div class="box-body">
         <div class="row">
            <div class="col-lg-1">
               <small>ID: </small>
               <h3 style="margin-top:0;"><strong><?php echo $user->user_id; ?></strong></h3>
            </div>
            <div class="col-lg-3">
               <small>Username: </small>
               <h3 style="margin-top:0; margin-bottom:0;">
                  <strong><?php echo htmlentities($user->user_username); ?></strong>
               </h3>
               <?php echo ($user->user_system_admin == 1 ? '<small class="text-info">[ADMINISTRATOR]</small>' : ''); ?>
            </div>
            <div class="col-lg-2">
               <small>Default Service: </small><br/>
               <?php if(!isset($user->default_service['user_default_service_id']) || $user->default_service['user_default_service_id'] == "") {?>
                  <i>No default service.</i>
               <?php } else { ?>
                  <a class="btn btn-xs btn-primary" href="manage_service.php?service_id=<?php echo $user->default_service['user_default_service_id']; ?>"><?php echo htmlentities($user->default_service['service_name']); ?></a>
               <?php } ?>
            </div>
            <div class="col-lg-3">
               <small>All Services (<?php echo count($user->user_services); ?> total):</small><br/>
               <div id="divAllServiceHolder">
                  <?php foreach($user->user_services as $service_counter => $user_service) {?>
                     <a class="btn btn-xs btn-default <?php echo ($service_counter > 5 ? 'btn-show-hide' : '') ?>" href="manage_service.php?service_id=<?php echo $user_service['service_id']; ?>" style="<?php echo ($service_counter > 5 ? 'display:none' : '') ?>"><?php echo htmlentities($user_service['service_name']); ?></a>
                  <?php } ?>
                  <?php if($service_counter > 5) {?>
                     <br/><a href="#" class="btn-show-hide-all-user-services" connet-user-hidden-service-count="<?php echo ($service_counter - 5); ?>"><small>[show <?php echo ($service_counter - 5); ?> hidden services]</small></a>
                  <?php } ?>
               </div>
            </div>
            <div class="col-lg-1">
               <small>Status: </small><br/>
               <?php if($user->user_status == "ENABLED") { ?>
                  <p class="text-success"><strong><?php echo htmlentities($user->user_status); ?></strong></p>
               <?php } else { ?>
                  <p class="text-danger"><strong><?php echo htmlentities($user->user_status); ?></strong></p>
               <?php } ?>
            </div>
            <div class="col-lg-2">
               <a href="manage_user.php?user_id=<?php echo $user->user_id; ?>&account_id=<?php echo $account_id; ?>" class="btn btn-primary btn-block"><i class="fa fa-edit"></i> Edit User</a>
               <!-- <button class="btn btn-xs btn-danger btn-block btn-remove-user-from-service" connet-user-id="<?php echo $user->user_id; ?>"><i class="fa fa-minus-circle"></i> Remove From Account</button> -->
            </div>
         </div>
      </div>
   </div>
<?php } ?>


<!-- MODALS HERE -->
<!-- THE SELECT USER MODAL, LOADED VIA AJAX-->
<div id="modalFragSelectUser" class="modal fade" tabindex="-1" role="dialog" data-backdrop="static">
   <div class="modal-dialog">
      <div class="modal-content">
         <div class="modal-header" >
            <button type="button" class="close" data-dismiss="modal" aria-label="Close" id="btnFragModalCloseTop"><span aria-hidden="true">&times;</span></button>
            <h4 class="modal-title" id="modalFragSelectUserTitle">Add User To Service</h4>
         </div>
         <div class="modal-body">
            <div class="alert alert-danger" style="display:none;" id="modalFragSelectUserAlert">
               <h4><i class="icon fa fa-ban"></i> Error!</h4>
               <p id="modalFragSelectUserAlertText">Unfortunately there was an error, please try again.</p>
            </div>
            <h5>Please select a user to add to this service.</h5>
            <div id="modalFragSelectUserPageHolder" style="height:400px !important;">
               <!-- THE AJAX LOADED USER SELECTOR IS PLACE IN HERE -->
            </div>
         </div>
         <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal" id="modalFragSelectUserCloseBottom">Close</button>
         </div>
      </div><!-- /.modal-content -->
   </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<script type="text/javascript">
   var fragServiceId = <?php echo (isset($service_id) ? $service_id : '-1') ?>;

   $(document).ready(function (e)
   {
      /**
       * FOR SHOWING ALL ADMIN USERS TOO
       */
      $('#checkShowConnetUsers').change(function() {
         console.log("Showing....");
         if($(this).is(":checked"))
         {
            console.log("checked....");
            window.location.href = window.location.href.replace("#tab_users", "&show_admin=true#tab_users");
         }
         else
         {
            console.log("un checked....");
            window.location.href = window.location.href.replace("&show_admin=true", "");
         }


         //location.reload();
      });

      /**
       * This button shows the add new service permission modal
       */
      $('#btnAddUserToService').on("click", function (e)
      {
         e.preventDefault();

         hideFragModalError();

         $('#modalFragSelectUserPageHolder').html('<span class="glyphicon glyphicon-refresh glyphicon-spin"></span> Loading user list...');

         $.post("<?php echo WebAppHelper::getBaseServerURL(); ?>/pages/modules/module_user_select.php", {parent_callback_function: 'addUserToService'}).done(function (data) {
            unlockFragModalBusy();
            $('#modalFragSelectUserPageHolder').html(data);
         }).fail(function (data) {
            unlockFragModalBusy();
            showModalError("A server error occurred and we could not load the user list, please contact technical support.");
         });

         $('#modalFragSelectUser').modal('show');

      });

      /**
       * This button allows the user to remove a user from a service.
       */
      $('.btn-remove-user-from-service').on("click", function (e)
      {
         e.preventDefault();

         if(confirm("Are you sure you want to revoke this user's access to this service?"))
         {
            var userId = $(this).attr('connet-user-id');
            $(this).html('<span class="glyphicon glyphicon-refresh glyphicon-spin"></span> Removing user...');
            $.post("<?php echo WebAppHelper::getBaseServerURL(); ?>/php/ajaxManageServiceHandler.php",
                {
                   task: 'remove_user_from_service',
                   serviceId: fragServiceId,
                   userId:userId,

                }).done(function (data) {
               var json = $.parseJSON(data);
               if (json.success)
               {
                  location.reload();
               }
               else
               {
                  if(json.reason == 'no_permissions_exist')
                  {
                     alert("This user does not have access to this service.");
                  }
                  else
                  {
                     alert("A server error occurred and we could not save the data, please refresh your page or contact technical support.");
                  }

               }
            }).fail(function (data) {
               alert("A server error occurred and we could not save the data, please refresh your page or contact technical support.");
            });
         }
      });

      /**
       * THis button allows the user to show or hide extra services, which saves on screen real estate with user that have many service permissions
       */
      $('.btn-show-hide-all-user-services').on("click", function (e) {
         e.preventDefault();
         console.log('showing...');
         if($(this).parent().find('.btn-show-hide').is(":visible") )
         {
            //hide the elements
            $(this).parent().find('.btn-show-hide').hide();
            $(this).html('<small>[show ' + $(this).attr('connet-user-hidden-service-count') +' hidden services]</small>');
         }
         else
         {
            //show the elements
            $(this).parent().find('.btn-show-hide').show();
            $(this).html('<small>[show less services]</small>');
         }

      });
   });

   function addUserToService(userId)
   {
      if(confirm("Are you sure you want to add this user to this service?")) {
         lockFragModalBusy("Adding user...");
         $.post("../php/ajaxManageServiceHandler.php",
             {
            task: 'add_user_to_service',
            serviceId: fragServiceId,
            userId:userId,

         }).done(function (data) {
            var json = $.parseJSON(data);
            if (json.success)
            {
               lockFragModalBusy("Refreshing...");
               location.reload();
            }
            else
            {

               unlockFragModalBusy();
               if(json.reason == 'user_exists')
               {
                  showFragModalError("This user already exists on this service.");
               }
               else
               {
                  showFragModalError("A server error occurred and we could not save the data, please refresh your page or contact technical support.");
               }

            }
         }).fail(function (data) {
            unlockFragModalBusy();
            showFragModalError("A server error occurred and we could not save the data, please refresh your page or contact technical support.");
         });
      }
   }

   /*****************************************
    * HELPERS
    *****************************************/

   //this locks the modal so the user can't do anything, useful when runing ajax calls off of it
   var prevButtonContent = "";
   function lockFragModalBusy(busyMessage)
   {
      prevButtonContent = $('#btnFragModalSave').html();
      $('#btnFragModalCloseTop').prop("disabled", true);
      $('#btnFragModalCloseBottom').prop("disabled", true);
      $('#btnFragModalSave').prop("disabled", false);
      $('#btnFragModalSave').html('<span class="glyphicon glyphicon-refresh glyphicon-spin"></span> ' + busyMessage);
   }

   //unlocks the modal so the user can close it
   function unlockFragModalBusy()
   {
      $('#btnFragModalCloseTop').prop("disabled", false);
      $('#btnFragModalCloseBottom').prop("disabled", false);
      $('#btnFragModalSave').prop("disabled", false);
      $('#btnFragModalSave').html(prevButtonContent);
      prevButtonContent = "";
   }

   //shows the error in the MODAL with a custom error message
   function showFragModalError(message)
   {
      $("#modalFragSelectUserAlertText").html(message);
      $("#modalFragSelectUserAlert").show();
   }

   //hides the modal error and resets the message
   function hideFragModalError()
   {
      $("#modalFragSelectUserAlertText").html();
      $("#modalFragSelectUserAlert").hide();
   }

</script>

<!-- Template Footer -->

