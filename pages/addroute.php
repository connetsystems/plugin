<?php
/**
 * Include the header template which sets up the HTML
 *
 * Don't forget to include template_import_script before any Javascripting
 * Don't forget to include template footer.php at the bottom of the page as well
 */
//-------------------------------------------------------------
// Template
//-------------------------------------------------------------
TemplateHelper::setPageTitle('Add Route');
TemplateHelper::initialize();

$dsp = 'none';

//-------------------------------------------------------------
// Permissions
//-------------------------------------------------------------
if (isset($_GET['client'])) {

   $split = explode(' ', trim($_GET['client']));
   header('location: ' . $_SERVER['DOCUMENT_URI'] . '?sId=' . (integer) end($split));
   die();
}

if (LoginHelper::isSystemAdmin()) {
   
   $clients = getClients();
}
else {
   /**
    * This is flawed, if an account has more than one reseller service, then
    * It will see all of their sub-services, even if they don't have permissions...
    */   
   $account_id = LoginHelper::getCurrentAccountId();
   $clients = getServicesInAccountWithOutRid($account_id);
}
 
if (isset($_GET['sId'])) {

   $service_id = (integer) $_GET['sId'];
   $permission = false;
   foreach ($clients as $service) {

      $_service_id = (integer) $service['service_id'];
      if ($service_id === $_service_id) {
         $permission = true;
         break;
      }
   }

   if (!$permission) {

      header('location: ' . $_SERVER['DOCUMENT_URI']);
      die();
   }
}
 
//-------------------------------------------------------------
// Add / Remove Routes
//-------------------------------------------------------------
if (isset($_GET['sId'])) {

   $cl = $_GET['sId'];
   $dsp = 'block';
} else {
   $dsp = 'none';
}

if (isset($cl)) {
   $service_id = $cl;

   if (isset($_GET['removeId'])) {

      $remId = $_GET['removeId'];
      removeRouteFromSubAccount($remId);
      header('location: ' . $_SERVER['DOCUMENT_URI'] . '?sId=' . (integer) $service_id);
      die();
   }

   if (isset($_GET['nVal'])) {
      $nVal = $_GET['nVal'];
      $manId = $_GET['manId'];
      $rDesc = urldecode($_GET['rDesc']);
      $provider_id = urldecode($_GET['provId']);
      $msgType = urldecode($_GET['msgType']);
      $rdn = urldecode($_GET['rdn']);
      $route_code = urldecode($_GET['rc']);
      $sts = urldecode($_GET['sts']);
      $regex = urldecode($_GET['regex']);
      $prior = urldecode($_GET['prior']);
      $route_mccmnc = urldecode($_GET['mcc']);
      $rcid = urldecode($_GET['rcid']);

      //make sure they don't readd the route because they refreshed the page. Major hole in the get variable approach
      if (!checkRouteExistsInService($provider_id, $service_id, $route_code, $route_mccmnc)) {

         createNewClientRoutes($nVal, $service_id, $manId, $rDesc, $provider_id, $msgType, $rdn, $route_code, $sts, $regex, $prior, $route_mccmnc, $rcid);
         header('location: ' . $_SERVER['DOCUMENT_URI'] . '?sId=' . (integer) $service_id);
         die();
      }
   }

   $resellerRoutes = getResellerDefaultRouteData($_SESSION['serviceId'], $service_id);
   $subRoutes = getAddedDefaultRouteData($service_id);
}
?>
<!-- Right side column. Contains the navbar and content of the page -->
<aside class="right-side">
   <!--//////////////////////////-->
   <section class="content-header">
      <h1>
         Add Routes To Client
      </h1>
   </section>

   <section class="content">
      <div class="row">
         <form action="newresellerservice.php" class="col-md-12" method="get" id="newService" style="border:none;">
            <!--div class="col-md-12"-->
            <div class="box box-connet" style="padding-bottom:1px; border-top-color:<?php echo $accRGB; ?>;">
               <div class="box-body" id="notify" style="padding-bottom:0px;">
                  <div class="callout callout-info" style="margin-bottom:0px;">
                     <h4>Add Routes To Client Instructions / Details</h4>
                     <p>Here you should add the routes you wish to assign to the service you just created.</p>
                  </div>
                  <div id="notifyE" class="callout callout-danger" style="display:none;" >
                     <h4>Error:</h4>
                     <p>The selling price has to be greater than or the same as the cost price.</p>
                  </div>
               </div>
               <div class="box-body">
                  <label>Client List</label>
                  <select class="form-control" name="client" form="clientList" OnChange="reloadOnSelect(this.value);">
                     <?php
                     if ($cl == '0') {
                        echo '<option SELECTED>Please select a client...</option>';
                     }
                     foreach ($clients as $key => $value) {
                        $service_idt = $value['service_id'];
                        if ($cl == $service_idt) {
                           $accountName = $value['account_name'];
                           $serviceName = $value['service_name'];
                           echo "<option name='" . $sIdt . "' SELECTED>" . htmlentities($accountName . " - " . $serviceName . " - " . $service_idt) . "</option>";
                        } else {
                           echo "<option name='" . $sIdt . "'>" . htmlentities($value['account_name'] . " - " . $value['service_name'] . " - " . $service_idt) . "</option>";
                        }
                     }
                     ?>
                  </select>
               </div>
            </div>

            <div class="box box-connet" style="padding-bottom:1px; border-top-color:<?php echo $accRGB; ?>; height:auto; display:<?php echo $dsp; ?>;" >
               <div class="box-header">
                  <h3 class="box-title">Available routes:</h3>
               </div>
               <div class="box-body">
                  <table id="example2" class="table table-bordered table-striped dataTable table-hover">
                     <thead>
                        <tr>
                           <th>Country</th>
                           <th>Network</th>
                           <th><center>STATUS</center></th>
                     <th><center>COST RATE</center></th>    
                     <th><center>SELL RATE</center></th>    
                     <th><center>Assign Route</center></th>
                     </tr>
                     </thead>

                     <tbody>
                        <!--form action="newresellerroutes.php" method="get" id="resellerRouteRates" name="resellerRouteRates" class="sidebar-form" style="border:0px;"-->
                        <?php
                        $numId = 0;
                        if (!isset($resellerRoutes)) {
                           echo '<tr><td colspan="6" align="center">No other routes available to assign.</td>';
                        } else {

                           foreach ($resellerRoutes as $key => $value) {
                              /*
                                $value['COUNTRY'];
                                $value['NETWORK'];
                                $value['STATUS'];
                                $value['RATE'];
                                $value['COLLECTION'];
                                $value['route_collection_id'];
                                $value['MCCMNC'];
                                $value['route_id'];
                                $value['DEFAULT'];
                                $value['PORTED'];
                                $value['provider_id'];
                                $value['service_id'];
                                $value['route_msg_type'];
                                $value['route_description'];
                                $value['route_display_name'];
                                $value['route_code'];
                                $value['route_match_regex'];
                                $value['route_match_priority'];
                                $value['route_billing'];
                               */
                              $rDescPos = strrpos($value['route_description'], ' ');
                              $rDesc = substr($value['route_description'], 0, $rDescPos - 1);
                              $rDesc = urlencode($rDesc);
                              $value['route_display_name'] = urlencode($value['route_display_name']);
                              $value['route_code'] = urlencode($value['route_code']);
                              $value['route_billing'] = urlencode($value['route_billing']);

                              //echo '<br>rdesc-'.$value['route_display_name'].'-';

                              $logo = strstr($value['NETWORK'], ' (', true);
                              $logoP = strpos($logo, ' / ');
                              //echo '<br>logoP='.$logoP;
                              if ($logoP != '') {
                                 $logo = substr($logo, $logoP + 3, strlen($logo));
                              }

                              $urlReg = urlencode($value['route_match_regex']);

                              echo '<tr name="routeId" value="' . $value['route_id'] . '">';
                              echo '<td style="vertical-align:middle;"><img src="../img/flags/' . $value['COUNTRY'] . '.png">&nbsp;&nbsp;' . htmlentities($value['COUNTRY']) . '</td>';

                              $img = 'http://portal.connet-systems.com/img/serviceproviders/' . $logo . '.png';
                              $header_response = get_headers($img, 1);
                              if (strpos($header_response[0], "404") !== false) {
                                 //echo "<td><img src='../img/serviceproviders/Special.png'>&nbsp;&nbsp;".$value['NETWORK']."</td>";
                                 echo '<td style="vertical-align:middle;"><img src="../img/serviceproviders/Special.png">&nbsp;&nbsp;' . htmlentities($value['NETWORK']) . '</td>';
                                 // FILE DOES NOT EXIST
                              } else {
                                 //echo "<td><img src='../img/serviceproviders/".$logo.".png'>&nbsp;&nbsp;".$value['NETWORK']."</td>";
                                 echo '<td style="vertical-align:middle;"><img src="../img/serviceproviders/' . $logo . '.png">&nbsp;&nbsp;' . htmlentities($value['NETWORK']) . '</td>';
                                 // FILE EXISTS!!
                              }
                              //echo '<td style="vertical-align:middle;"><img src="../img/serviceproviders/'.$logo.'.png">&nbsp;&nbsp;'.$value['NETWORK'].'</td>';
                              if ($value['STATUS'] == 'ENABLED') {
                                 echo '<td style="vertical-align:middle;"><center><i class="fa fa-check" style="color:#00ff00;"></i>&nbsp;&nbsp;' . htmlentities($value['STATUS']) . '</center></td>';
                              } else {
                                 echo '<td style="vertical-align:middle;"><center><i class="fa fa-times" style="color:#ff0000;"></i>&nbsp;&nbsp;' . htmlentities($value['STATUS']) . '</center></td>';
                              }
                              echo '<td style="vertical-align:middle;"><center><div style="display: inline-table;"></div>' . htmlentities($value['RATE']) . '</div></center></td>';

                              echo '<td style="vertical-align:middle;">
                                         <center>
                                             <div class="fakey" style="padding-left:10px;width:100px;">
                                                 <div style="display: inline-table;">0.</div>
                                                     <input type="text" onkeypress="return isNumberKey(' . $value['RATE'] . ', this.value, event)" id="routeRate' . $value['route_id'] . '" name="routeRate' . $value['route_id'] . '" placeholder="" style="background:#ffffff;width:40px;border-color:#929292;border:0px;" maxlength="4" value="' . substr($value['RATE'], 2) . '">
                                             </div>
                                         </center>
                                 </td>';

                              echo '<td style="vertical-align:middle;">
                                     <center>
                                             <a class="btn btn-block btn-social btn-dropbox" name="' . $value['route_id'] . '" onclick="submitRates(this.name, ' . $numId . ', ' . $value['provider_id'] . ', \'' . $value['route_msg_type'] . '\', \'' . $rDesc . '\', \'' . htmlspecialchars($value['route_display_name']) . '\', \'' . htmlspecialchars($value['route_code']) . '\', \'' . $value['STATUS'] . '\', \'' . htmlspecialchars($urlReg) . '\', \'' . htmlspecialchars($value['route_match_priority']) . '\', \'' . $value['route_billing'] . '\', \'' . $value['MCCMNC'] . '\', ' . $value['route_collection_id'] . ')" style="background-color:' . $accRGB . '; color: #ffffff; border: 2px solid; border-radius: 6px !important; width: 140px; margin-top:0px;">
                                                 <i class="fa fa-plus"></i>Assign Route
                                             </a>
                                     </center>
                                 </td>';
                              echo '</tr>';

                              $numId++;
                           }
                        }
//echo '</form>';
                        ?>
                        <!--/form-->
                     </tbody>
                  </table>
               </div>
            </div>

            <div class="box box-connet" style="padding-bottom:1px; border-top-color:<?php echo $accRGB; ?>; height:auto; display:<?php echo $dsp; ?>;" >
               <div class="box-header">
                  <h3 class="box-title">Assigned routes:</h3>
               </div>

               <div class="box-body" style="padding:0px;">
                  <div class="box-body table-responsive">
                     <table id="example2" class="table table-bordered table-striped dataTable table-hover">
                        <thead>
                           <tr>
                              <!--th>#</th-->
                              <th>Country</th>
                              <th>Network</th>
                              <th><center>SMSC</center></th>
                        <th><center>SELL RATE</center></th>    
                        <th><center>Remove Route</center></th>    
                        </tr>
                        </thead>

                        <tbody>
                           <?php
                           if (isset($subRoutes)) {
                              $numId = 0;

                              foreach ($subRoutes as $key => $value) {
                                 if (strpos($value['NETWORK'], 'Special')) {
                                    $logo = trim('Special');
                                 } else {
                                    $logo = strstr($value['NETWORK'], ' (', true);
                                    $logoP = strpos($logo, ' / ');
                                    //echo '<br>logoP='.$logoP.'<-';
                                    if ($logoP != '') {
                                       $logo = substr($logo, $logoP + 3, strlen($logo));
                                    }
                                    $logo = trim($logo);
                                 }

                                 $flag = str_replace(' / ', '', $value['COUNTRY']);
                                 echo '<tr name="routeId" value="' . $value['route_id'] . '">';
                                 echo '<td style="vertical-align:middle;"><img src="../img/flags/' . $flag . '.png">&nbsp;&nbsp;' . htmlentities($value['COUNTRY']) . '</td>';
                                 //echo '<br>->'.$logo.'.png<';
                                 $img = 'http://portal.connet-systems.com/img/serviceproviders/' . $logo . '.png';
                                 $header_response = get_headers($img, 1);
                                 if (strpos($header_response[0], "404") !== false) {
                                    //echo "<td><img src='../img/serviceproviders/Special.png'>&nbsp;&nbsp;".$value['NETWORK']."</td>";
                                    echo '<td style="vertical-align:middle;"><img src="../img/serviceproviders/Special.png">&nbsp;&nbsp;' . htmlentities($value['NETWORK']) . '</td>';
                                    // FILE DOES NOT EXIST
                                 } else {
                                    //echo "<td><img src='../img/serviceproviders/".$logo.".png'>&nbsp;&nbsp;".$value['NETWORK']."</td>";
                                    echo '<td style="vertical-align:middle;"><img src="../img/serviceproviders/' . $logo . '.png">&nbsp;&nbsp;' . htmlentities($value['NETWORK']) . '</td>';
                                    // FILE EXISTS!!
                                 }
                                 if ($value['STATUS'] == 'ENABLED') {
                                    echo '<td style="vertical-align:middle;"><center><i class="fa fa-check" style="color:#00ff00;"></i>&nbsp;&nbsp;' . htmlentities($value['STATUS']) . '</center></td>';
                                 } else {
                                    echo '<td style="vertical-align:middle;"><center><i class="fa fa-times" style="color:#ff0000;"></i>&nbsp;&nbsp;' . htmlentities($value['STATUS']) . '</center></td>';
                                 }
                                 echo '<td style="vertical-align:middle;"><center><div style="display: inline-table;"></div>' . htmlentities($value['RATE']) . '</div></center></td>';

                                 echo '<td style="vertical-align:middle;">
                                                                        <center>
                                                                                <a class="btn btn-block btn-social btn-dropbox" name="' . $value['route_id'] . '" onclick="removeRate(' . $value['route_id'] . ')" style="background-color:' . $accRGB . '; color: #ffffff; border: 2px solid; border-radius: 6px !important; width: 140px; margin-top:0px;">
                                                                                    <i class="fa fa-minus"></i>Remove Route
                                                                                </a>
                                                                        </center>
                                                                    </td>';
                                 echo '</tr>';

                                 $numId++;
                              }
                           }
                           ?>
                        </tbody>
                     </table>

<!--a class="btn btn-block btn-social btn-dropbox" name="" onclick="gotoAddUsers(<?php echo $service_id; ?>)" style="background-color:<?php echo $accRGB; ?>; color: #ffffff; border: 2px solid; border-radius: 6px !important; width: 140px; margin-top:0px;">
<i class="fa fa-users"></i>Add Users
</a-->    
                  </div>
               </div>
         </form>
      </div>
   </section>
</aside>


<!--  THE JAVASCRIPT IMPORT ALWAYS APPEARS BEFORE LOCAL JAVASCRIPT AT THE BOTTOM OF THE PAGE -->
<?php include("template_import_script.php"); //must import all scripts first      ?>
<!-- END JAVASCRIPT IMPORT -->

<script type="text/javascript">
   $(window).load(function ()
   {
      $(".loader").fadeOut("slow");
      $(".loaderIcon").fadeOut("slow");
   })
</script>

<script type="text/javascript">
   
   /*function gotoAddUsers(sId)
    {
    $(".loader").fadeIn("slow");
    $(".loaderIcon").fadeIn("slow");
    window.location = "newuser.php?sId=" + sId;                
    };*/
   
   function addRouteAndReload(routeId, newValue, numId, provider_id, route_msg_type, rDesc, route_display_name, route_code, STATUS, route_match_regex, route_match_priority, route_billing, MCCMNC, route_collection_id)
   {
      $(".loader").fadeIn("slow");
      $(".loaderIcon").fadeIn("slow");
      window.location = "addroute.php?sId=" + <?php echo urlencode($service_id); ?> + "&rId=" + routeId + "&nVal=" + newValue + "&numId=" + numId + "&manId=" + <?php echo urlencode($_SESSION['serviceId']); ?> + "&rDesc=" + rDesc + "&provId=" + provider_id + "&msgType=" + route_msg_type + "&rdn=" + route_display_name + "&rc=" + route_code + "&sts=" + STATUS + "&regex=" + route_match_regex + "&prior=" + route_match_priority + "&mcc=" + MCCMNC + "&rcid=" + route_collection_id;
   }
   ;
   
   function submitRates(v, numId, provider_id, route_msg_type, rDesc, route_display_name, route_code, STATUS, route_match_regex, route_match_priority, route_billing, MCCMNC, route_collection_id)
   {
      routeToAdd = 'routeRate' + v;
      var r2Add = document.getElementById(routeToAdd);
      r2Add = parseInt(r2Add.value);
      var oldRoute = urldecode(route_billing);
      var n = oldRoute.indexOf(",");
      oldRoute = oldRoute.substr(0, n);
      oldRoute = parseInt(oldRoute);
      
      if (oldRoute <= r2Add)
      {
         addRouteAndReload(v, r2Add, numId, provider_id, route_msg_type, rDesc, route_display_name, route_code, STATUS, route_match_regex, route_match_priority, route_billing, MCCMNC, route_collection_id);
      } else
      {
         //alert("Error");
         document.getElementById("notify").style.height = '200px';
         document.getElementById("notifyE").style.display = 'block';
         /*var notify = document.getElementById("notify");
          notify.height = 200px;
          var notifyE = document.getElementById("notifyE");
          notifyE.style.display = 'block';*/
      }
   }
   ;
   
   function removeRate(rId)
   {
      //alert(' rId=' + rId);
      window.location = "addroute.php?sId=" + <?php echo urlencode($service_id); ?> + "&removeId=" + rId;
   }
   ;
   
   function urldecode(url)
   {
      return decodeURIComponent(url.replace(/\+/g, ' '));
   }
   
   function reloadOnSelect(name)
   {
      $(".loader").fadeIn("slow");
      $(".loaderIcon").fadeIn("slow");
      encodeURI(name);
      window.location = "addroute.php?client=" + encodeURIComponent(name);
   }
   
</script>

<script language=Javascript>
   function isNumberKey(oV, v, evt)
   {
      //alert('oVs=' + oVs +' v=' + v + '- evt=' + e + '-');
      var charCode = (evt.which) ? evt.which : evt.keyCode;
      
      if (charCode != 46 && charCode > 31
              && (charCode < 48 || charCode > 57))
         return false;
      
      return true;
      /*}
       else
       {
       return false;
       }*/
   }
</script>

<!-- Template Footer -->