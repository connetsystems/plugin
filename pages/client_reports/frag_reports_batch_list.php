<?php

   /**
    * This script is called by the manage services page, and runs certain ajax based tasks
    * @author - Doug Jenkinson
    */
   require_once("../../php/allInclusive.php");
   require_once("../../PluginAPI/PluginAPIAutoload.php");

   session_start();

   if(isset($_POST['service_id']))
   {
      $service_id = $_POST['service_id'];
   }
   else if(isset($_GET['service_id']))
   {
      $service_id = $_GET['service_id'];
   }
   else
   {
      echo '<p class="text-error">An error occurred, please refresh your browser.</p>';
      die();
   }

   //get the service object
   $service_obj = new Service($service_id);

   if(isset($_GET['start_date']) && isset($_GET['end_date']))
   {
      $start_date = new DateTime($_GET['start_date']);
      $end_date = new DateTime($_GET['end_date']);
   }
   else
   {
      $start_date = new DateTime('first day of this week');
      $end_date = new DateTime('today');
   }


   $start_date_string = $start_date->format('Y-m-d');
   $end_date_string = $end_date->format('Y-m-d');

   $service_clients = getCampaignClient($service_id);
   $service_campaigns = getCampaignData($service_id);

   $batch_list = Batch::getServiceBatchListWithStats($service_obj->service_id, $start_date_string, $end_date_string);

   //echo json_encode($batch_list);
   $csv_batch_list_link = "../php/csv_reports/createCSVBatchList.php?service_id=".$service_id."&start_date=".$start_date_string."&end_date=".$end_date_string;
?>
   <div class="row">
      <div class="col-lg-6">
         <p class="lead" style="margin-bottom:5px;">Select a batch to view it's details.</p>
         <p>Showing batches for the date range <strong><?php echo htmlentities($start_date_string); ?></strong> to <strong><?php echo htmlentities($end_date_string); ?></strong>.</p>
         <hr/>
      </div>
      <div class="col-lg-3">
         <div class="form-group">
            <label for="selectFilterClients">Filter by client:</label>
            <select id="selectFilterClients" class="form-control input-sm">
               <option value="-1"> - All Clients - </option>
               <?php foreach($service_clients as $client) { ?>
                  <option value="<?php echo $client['client_id']; ?>"><?php echo $client['client_name']; ?></option>
               <?php } ?>
            </select>
         </div>
      </div>
      <div class="col-lg-3">
         <div class="form-group">
            <label for="selectFilterCampaigns">Filter by campaign:</label>
            <select id="selectFilterCampaigns" class="form-control input-sm" disabled="true">
               <option value="-1"> - All Campaigns - </option>
               <?php foreach($service_campaigns as $campaign) { ?>
                  <option value="<?php echo $campaign['campaign_id']; ?>" connet-client-id="<?php echo $campaign['client_id']; ?>"><?php echo $campaign['campaign_name']; ?></option>
               <?php } ?>
            </select>
         </div>
      </div>
   </div>
   <div class="box box-primary">
      <div class="box-header with-border">
         <h3 class="box-title pull-left">
            <i class="fa fa-pie-chart"></i> Batch List <small>Click a batch to view details.</small>
         </h3>
         <div class="box-tools pull-right">
            <a id="btnDownloadBatchlistReport" href="<?php echo $csv_batch_list_link; ?>" class="btn btn-xs btn-primary pull-right" style="color:white; margin-top:5px; background-color:<?php echo $_SESSION['accountColour']; ?>;">
               <i class="fa fa-save"></i> Download CSV Report
            </a>
         </div>
      </div>
      <div class="box-body">
         <div id="holderNoResults" style="text-align:center; <?php echo (count($batch_list) > 0) ? 'display:none;' : ''; ?>">
            <br/><br/>
            <p class="text-warning"><strong>No batches to display.</strong></p>
         </div>

         <div class="list-group" id="holderBatchList">
            <?php foreach($batch_list as $batch) { ?>
               <a href="client_batch_report.php?batch_id=<?php echo $batch['batch_id']; ?>" class="list-group-item" connet-client-id="<?php echo $batch['client_id']; ?>" connet-campaign-id="<?php echo $batch['campaign_id']; ?>"
                  data-toggle="tooltip" data-placement="bottom" title="Click on this batch to view details.">
                  <div class="row">
                     <div class="col-lg-5">
                        <div style="overflow:hidden !important; white-space: nowrap;">
                           <h4 class="list-group-item-heading">
                              Ref: <strong><?php echo htmlentities($batch['batch_reference']); ?></strong>
                              <small>
                                 <span  class="<?php echo $batch['batch_submit_status_text_class']; ?>">
                                    <i class="<?php echo $batch['batch_submit_status_icon']; ?>"></i>
                                    <?php echo htmlentities($batch['batch_submit_status']); ?>
                                 </span>
                              </small>
                              <br/>
                              <small>
                                 Client: <strong class="text-black"><?php echo htmlentities($batch['client_name']); ?></strong> -
                                 Campaign: <strong class="text-black"><?php echo htmlentities($batch['campaign_name']); ?></strong>
                              </small>
                           </h4>
                        </div>
                     </div>
                     <div class="col-lg-6" style="text-align:right;">
                        <h4 class="list-group-item-heading">
                           <small>"<?php echo htmlentities(substr($batch['batch_csv_content'], 0, 90)); ?>..."</small>
                           <br/>
                           <small style="margin-top:10px !important;">
                              <span class="text-info"><i class="fa fa-send"></i> <?php echo (isset($batch['send_stats']['sent']) ? $batch['send_stats']['sent'] : 0); ?> <small>submitted</small></span> |
                              <span class="text-success"><i class="fa fa-check-circle"></i> <?php echo (isset($batch['send_stats']['delivered']) ? $batch['send_stats']['delivered'] : 0); ?> <small>delivered</small></span>
                              <span class="text-warning"><i class="fa fa-question-circle"></i> <?php echo (isset($batch['send_stats']['pending']) ? $batch['send_stats']['pending'] : 0); ?> <small>pending</small></span> |
                              <span class="text-danger"><i class="fa fa-times-circle"></i> <?php echo (isset($batch['send_stats']['failed']) ? $batch['send_stats']['failed'] : 0); ?> <small>failed</small></span> |
                              <span><i class="fa fa-minus-circle"></i> <?php echo (isset($batch['send_stats']['rejected']) ? $batch['send_stats']['rejected'] : 0); ?> <small>rejected</small></span>
                           </small>
                        </h4>
                     </div>
                     <div class="col-lg-1" style="text-align:right;">
                        <h4 class="list-group-item-heading">
                           <strong><?php echo date('H:i', strtotime($batch['batch_schedule'])); ?></strong>
                           <br/>
                           <small><?php echo date('d M Y', strtotime($batch['batch_schedule'])); ?></small>
                        </h4>
                     </div>
                  </div>
               </a>
            <?php } ?>

         </div>
      </div>
   </div>

   <script type="text/javascript">
      var fragServiceId = <?php echo (isset($service_id) ? $service_id : '-1') ?>;
      var startDate = "<?php echo (isset($start_date_string) ? $start_date_string : date('Y-m-d')) ?>";
      var endDate = "<?php echo (isset($end_date_string) ? $end_date_string : date('Y-m-d')) ?>";

      $(document).ready(function (e)
      {
         //refresh tooltips
         $('[data-toggle="tooltip"]').tooltip();

         //listener for the client filter changes
         $('#selectFilterClients').on("change", function (e)
         {
            e.preventDefault();

            setSelectedClient($(this).val());
         });

         //listener for the campaign filter changes
         $('#selectFilterCampaigns').on("change", function (e)
         {
            e.preventDefault();

            setSelectedCampaign($(this).val());
         });
      });

      /*************************************************
       * HELPERS FOR FILTERS
       *************************************************/
      function setSelectedClient(clientId)
      {
         console.log("Selected client: "+ clientId);

         //if the client is -1, then disabled campaigns as all clients includes all campaigns
         if(clientId == -1)
         {
            $('#selectFilterCampaigns').children().show();
            $('#selectFilterCampaigns').val(-1); //set it to all campaigns
            $('#selectFilterCampaigns').attr('disabled', 'true');
         }
         else
         {
            $('#selectFilterCampaigns').children().hide();
            $('#selectFilterCampaigns').find("[value='-1']").show();
            $('#selectFilterCampaigns').find("[connet-client-id='"+clientId+"']").show();
            $('#selectFilterCampaigns').removeAttr('disabled');


         }

         //run the filter to remove items that don't belong
         filterBatches();
      }

      function setSelectedCampaign(campaignId)
      {
         console.log("Selected campaign: "+ campaignId);

         //run the filter to remove items that don't belong
         filterBatches();
      }

      function filterBatches()
      {

         var selectedClientId = $('#selectFilterClients').val();
         var selectedCampaignId = $('#selectFilterCampaigns').val();

         if($('#selectFilterCampaigns').val() != -1)
         {
            $('#holderBatchList').children().hide();
            $('#holderBatchList').find("[connet-campaign-id='"+selectedCampaignId+"']").show();
         }
         else if($('#selectFilterClients').val() != -1)
         {
            $('#holderBatchList').children().hide();
            $('#holderBatchList').find("[connet-client-id='"+selectedClientId+"']").show();
         }
         else
         {
            $('#holderBatchList').children().show();
         }

         //show the no results holder if nothing is showing, otherwise hide that chaka
         if($('#holderBatchList').children(':visible').length == 0) {
            $('#holderNoResults').show();
         }
         else
         {
            $('#holderNoResults').hide();
         }
      }

   </script>


