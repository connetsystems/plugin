<?php

   /**
    * This script is called by the batchlist page, and runs certain ajax based tasks
    * @author - Doug Jenkinson
    */
   require_once("../../php/allInclusive.php");
   require_once("../../PluginAPI/PluginAPIAutoload.php");

   session_start();

   //get the batch ID
   if(!isset($_POST['batch_id']))
   {
      echo "No batch ID was selected.";
      die();
   }
   else
   {
      $batch_id = $_POST['batch_id'];

      //get the batch info and check if we can acutally view this batch using permissions
      $batch_obj = Batch::getBatchData($batch_id);
      $service_id = $batch_obj['service_id'];

      if (!PermissionsHelper::hasServicePermission($service_id))
      {
         echo "You do not have permission to view this data.";
         die();
      }

      //get the service object
      $service_obj = new Service($service_id);

      //get the send type to display
      if (isset($_GET['send_type']))
      {
         $send_type = $_GET['send_type'];
      }
      else
      {
         //default to all sends
         $send_type = "all";
      }

      //get the page to display
      if (isset($_POST['page']))
      {
         $page = $_POST['page'];
      }
      else
      {
         //default to all sends
         $page = 1;
      }


      $total_sends_per_page = 25;
      $sends_start = $total_sends_per_page * ($page - 1);

      //getting the records
      $send_results = Batch::getMTSMSArray($batch_id, $sends_start, $total_sends_per_page, $send_type);
      $total_sends = $send_results['total_search_hits'];
      $send_array = $send_results['objects'];

      //pagination calculations
      $total_pages = ceil($total_sends / $total_sends_per_page);
      $pages_to_display = array();
      for ($p = 1; $p <= $total_pages; ++$p)
      {
         //check if we have too many pages to display them all in the paginator
         if ($p >= ($page - 3) && $p <= ($page + 3))
         {
            array_push($pages_to_display, $p);
         }
      }

      $csv_batch_sends_link = "../php/csv_reports/createCSVBatchSends.php?service_id=".$service_id."&batch_id=".$batch_id."&send_type=".$send_type;
   }
?>

   <div class="row">
      <div class="col-lg-9">
         <?php if($send_type == "all") { ?>
            <p class="lead">Showing all sends for this batch.</p>
         <?php } else { ?>
            <p class="lead">Showing sends with the delivery status of <strong><?php echo htmlentities($send_type); ?></strong>.</p>
         <?php } ?>
      </div>
      <div class="col-lg-3">
         <a href="<?php echo $csv_batch_sends_link; ?>" id="btnDownloadBatchSendsReport" class="btn btn-primary btn-sm pull-right" style="background-color:<?php echo $_SESSION['accountColour']; ?> !important;">
            <i class="fa fa-download"></i> Download As CSV Report
         </a>
      </div>

   </div>
   <div class="row">
      <div class="col-lg-12">
         <table class="table table-striped">
            <tr>
               <th>Message ID</th>
               <th>Date</th>
               <th>Number</th>
               <th style="width:250px;">Content</th>
               <th>Network</th>
               <th>Status</th>
            </tr>
            <?php if(count($send_array) == 0) { ?>
               <tr><td align="center" colspan="6" class="text-warning">There are no sends that match this delivery status.</td></tr>
            <?php } else if(count($send_array) == 0) { ?>
               <tr><td align="center" colspan="6" class="text-warning">No results to show.</td></tr>
            <?php } ?>

            <?php foreach($send_array as $send_obj) { ?>
               <tr>
                  <!-- THE ID OF THE MESSAGE -->
                  <td><?php echo htmlentities($send_obj->mtsms_id); ?></td>

                  <!-- THE DATE THAT THE MESSAEG WAS SENT -->
                  <td><?php echo date('H:i:s - d M Y', strtotime($send_obj->timestamp)); ?></td>

                  <!-- THE DESTINATION ADDRESS OF THE MESSAGE -->
                  <td><?php echo htmlentities($send_obj->dest); ?></td>

                  <!-- THE CONTENT OF THE MESSAGE -->
                  <td>
                     <?php
                        //some message display processing
                        $message_to_display = helperSMSGeneral::restorePlaceholderValuesForDisplay(utf8_decode($send_obj->content));
                        if(strlen($message_to_display) > 35)
                        {
                           $cropped_message = substr($message_to_display, 0, 35).'...';
                        }
                        else
                        {
                           $cropped_message = $message_to_display;
                        }
                     ?>
                     <span class="text-info" style="cursor: pointer;" data-toggle="tooltip" data-placement="top" title="<?php echo  htmlentities($message_to_display); ?>">
                        <?php echo  htmlentities($cropped_message); ?>
                     </span>
                  </td>

                  <!-- THE NETWORK IT WAS SENT OVER -->
                  <?php $network_name = getNetFromMCC($send_obj->mccmnc); ?>
                  <td><img src='../img/serviceproviders/<?php echo htmlentities($network_name); ?>.png'> <?php echo htmlentities($network_name); ?></td>

                  <!-- THE DELIVERY STATUS OF THE MESSAGE -->
                  <td class="<?php echo MTSMS::getDLRDisplayClass($send_obj->dlr); ?>">
                     <i class="<?php echo MTSMS::getDLRDisplayIcon($send_obj->dlr); ?>"></i>
                     <?php echo htmlentities(DeliveryReportHelper::dlrMaskToString($send_obj->dlr, DeliveryReportHelper::DLR_STRING_PRETTY)); ?>
                  </td>
               </tr>
            <?php } ?>
         </table>
      </div>
   </div>
   <div class="row">
      <div class="col-lg-6">
         <p>
            Showing <strong><?php echo $sends_start; ?></strong>
            to <strong><?php echo ($page*$total_sends_per_page > $total_sends ? $total_sends : $page*$total_sends_per_page); ?></strong>
            of a total <strong><?php echo $total_sends; ?></strong> records.
         </p>
      </div>
      <div class="col-lg-6">
         <nav>
            <ul class="pagination pull-right no-margin">
               <?php if ($page != 1) { ?>
                  <li>
                     <a href="#" class="btnPaginate" connet-page="1" aria-label="First">
                        <span aria-hidden="true"><i class="fa fa-angle-double-left"></i></span>
                     </a>
                  </li>
                  <li>
                     <a href="#" class="btnPaginate" connet-page="<?php echo $page - 1; ?>" aria-label="Previous">
                        <span aria-hidden="true"><i class="fa fa-angle-left"></i></span>
                     </a>
                  </li>
               <?php } ?>

               <?php foreach($pages_to_display as $page_number) { ?>
                  <li class="btnPaginate <?php echo ($page_number == $page ? "active" : ""); ?>" connet-page="<?php echo $page_number; ?>"><a href="#"><?php echo $page_number; ?></a></li>
               <?php } ?>

               <?php if ($page < $total_pages) { ?>
                  <li>
                     <a href="#" class="btnPaginate" connet-page="<?php echo $page + 1; ?>" aria-label="Next">
                        <span aria-hidden="true"><i class="fa fa-angle-right"></i></span>
                     </a>
                  </li>
                  <li>
                     <a href="#" class="btnPaginate" connet-page="<?php echo $total_pages; ?>" aria-label="Last">
                        <span aria-hidden="true"><i class="fa fa-angle-double-right"></i></span>
                     </a>
                  </li>
               <?php } ?>
            </ul>
         </nav>
      </div>
   </div>

   <script type="text/javascript">
      var fragServiceId = <?php echo (isset($service_id) ? $service_id : '-1') ?>;
      var fragBatchId = <?php echo (isset($batch_id) ? $batch_id : '-1') ?>;

      $(document).ready(function (e)
      {
         //enable tooltips
         $('[data-toggle="tooltip"]').tooltip();

         /**
          * This button type controls pagination
          */
         $('.btnPaginate').on("click", function (e)
         {
            e.preventDefault();
            var pageNumber = $(this).attr('connet-page');
            console.log("Showing page: ".pageNumber);

            //called on parent script
            paginateOnTab(pageNumber);
         });
      });

   </script>


