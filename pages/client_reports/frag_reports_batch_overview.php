<?php

   /**
    * This script is called by the manage services page, and runs certain ajax based tasks
    * @author - Doug Jenkinson
    */
   require_once("../../php/allInclusive.php");
   require_once("../../PluginAPI/PluginAPIAutoload.php");

   session_start();

   //get the batch ID
   if(!isset($_POST['batch_id']))
   {
      echo "No batch ID was selected.";
      die();
   }
   else
   {
      $batch_id = $_POST['batch_id'];

      //get the batch info and check if we can acutally view this batch using permissions
      $batch_obj = Batch::getBatchData($batch_id);
      $service_id = $batch_obj['service_id'];

      if (!PermissionsHelper::hasServicePermission($service_id))
      {
         echo "You do not have permission to view this data.";
         die();
      }

      //get the service object
      $service_obj = new Service($service_id);

      //get some totals for the page
      $sent_total = $batch_obj['send_stats']['sent'];
      $sent_units =  $batch_obj['send_stats']['units'];
      $delivered_total = $batch_obj['send_stats']['delivered'];
      $delivered_units =  $batch_obj['send_stats']['delivered_units'];
      $pending_total = $batch_obj['send_stats']['pending'];
      $pending_units =  $batch_obj['send_stats']['pending_units'];
      $failed_total = $batch_obj['send_stats']['failed'];
      $failed_units =  $batch_obj['send_stats']['failed_units'];
      $rejected_total = $batch_obj['send_stats']['rejected'] + $batch_obj['send_stats']['blacklisted'];
      $rejected_units =  $batch_obj['send_stats']['rejected_units'] + $batch_obj['send_stats']['blacklisted_units'];
      $replies_total = 0; //MOSMS::getMOCountInBatchByBatchId($batch_id);

   }
?>

   <div class="row">
      <div class="col-lg-12">
         <p class="lead">You can view the general details of this batch on this tab.</p>
      </div>
   </div>
   <!-- THE GENERAL SENDING STATS -->
   <div class="row">
      <div class="col-lg-3">
         <h2 style="margin-top:0px;"  data-toggle="tooltip" data-placement="top" title="Units: <?php echo $sent_units; ?>"><i class="fa fa-send"></i> <?php echo $sent_total; ?> <small>total sent</small></h2>
      </div>
      <div class="col-lg-9" style="text-align:right;">
         <h4 style="margin-top:12px;">
            <span class="text-success" data-toggle="tooltip" data-placement="top" title="Units: <?php echo $delivered_units; ?>"><i class="fa fa-check-circle"></i> <?php echo $delivered_total; ?> <small>delivered</small></span> |
            <span class="text-warning" data-toggle="tooltip" data-placement="top" title="Units: <?php echo $pending_units; ?>"><i class="fa fa-question-circle"></i> <?php echo $pending_total; ?> <small>pending</small></span> |
            <span class="text-danger" data-toggle="tooltip" data-placement="top" title="Units: <?php echo $failed_units; ?>"><i class="fa fa-times-circle"></i> <?php echo $failed_total; ?> <small>failed</small></span> |
            <span class="text-danger" data-toggle="tooltip" data-placement="top" title="Units: <?php echo $rejected_units; ?>"><i class="fa fa-minus-circle"></i> <?php echo $rejected_total; ?> <small>rejected</small></span> |
            <span class="text-info"><i class="fa fa-reply"></i> <span id="frag_reply_count" class="reply-count-display"><?php echo $replies_total; ?></span> <small>replies</small></span>
         </h4>
      </div>
   </div>

   <hr/>
   <div class="row">
      <div class="col-lg-8">
         <div class="box box-primary">
            <div class="box-header with-border">
               <h3 class="box-title"><i class="fa fa-area-chart"></i> Delivery And Replies Over Time</h3>
            </div>
            <div class="box-body">
               <div id="batchDlrAndMOHolder"></div>
            </div>
         </div>
         <div class="box box-primary">
            <div class="box-header with-border">
               <h3 class="box-title"><i class="fa fa-area-chart"></i> Send Rate Over Time</h3>
            </div>
            <div class="box-body">
               <div id="batchSendRateHolder"></div>
            </div>
         </div>

      </div>
      <div class="col-lg-4">
         <div class="box box-primary">
            <div class="box-header with-border">
               <h3 class="box-title"><i class="fa fa-area-chart"></i> Sends By Network</h3>
            </div>
            <div class="box-body">
               <div id="batchNetworkSendsHolder"></div>
               <div>
                  <div class="table-responsive">
                     <table class="table no-margin"  id="batchNetworkSendsTextHolder">

                     </table>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>

   <script type="text/javascript">
      var fragServiceId = <?php echo (isset($service_id) ? $service_id : '-1') ?>;
      var fragBatchId = <?php echo (isset($batch_id) ? $batch_id : '-1') ?>;
      var startDate = "<?php echo (isset($start_date_string) ? $start_date_string : date('Y-m-d')) ?>";
      var endDate = "<?php echo (isset($end_date_string) ? $end_date_string : date('Y-m-d')) ?>";

      $(document).ready(function (e)
      {
         //refresh tooltips
         $('[data-toggle="tooltip"]').tooltip();

         //load the chart data via ajax
         loadSendRateChartData();

         loadNetworkChartData();

         if(replyCount != null && replyCount > 0)
         {
            $('#frag_reply_count').html(replyCount);
         }
      });

      /************************************
       * FOR THE SEND RATE CHART
       ***********************************/
      function loadNetworkChartData()
      {
         showLoading("batchNetworkSendsHolder");

         //load up the page tab we have selected
         $.post("../php/ajaxReportingBatchHandler.php", {task: 'get_batch_network_send_chart_data', serviceId: fragServiceId, batchId: fragBatchId}).done(function (data) {
            var json = $.parseJSON(data);
            if (json.success)
            {
               setupNetworkSendChart(json);
            }
            else
            {
               showLoadingError("batchNetworkSendsHolder", "A server error occurred, please contact technical support.");
            }
         }).fail(function () {
            showLoadingError("batchNetworkSendsHolder", "A server error occurred, please contact technical support.");
         });
      }

      var batchNetworkSendsChartOptions  = {

         ///Boolean - Whether grid lines are shown across the chart
         segmentShowStroke : true,
         segmentStrokeColor : "#fff"
      };


      function setupNetworkSendChart(ajaxData)
      {
         var data = [];
         $('#batchNetworkSendsTextHolder').empty();

         //setup the chart data for the daily range
         for(var i = 0; i < ajaxData.networks.length; ++ i)
         {
            //add the data to the graph array
            var network = ajaxData.networks[i];
            var colour = getNetworkColour(network.mccmnc);
            data[i] = {'value':network.send_total, 'label':network.network_name, 'color': "" + colour};

            //add the table rows for the networks
            addNetworkTableRow(network);
         }

         var chartWidth = $("#batchNetworkSendsHolder").width() - 5;
         $("#batchNetworkSendsHolder").html('<canvas id="batchNetworkSendsChart" width="'+chartWidth+'" height="300"></canvas>')

         // Get context with jQuery - using jQuery's .get() method.
         var ctx2 = $("#batchNetworkSendsChart").get(0).getContext("2d");
         var batchNetworkSendsChart = new Chart(ctx2).Doughnut(data, batchNetworkSendsChartOptions);
      }

      function addNetworkTableRow(network)
      {
         $('#batchNetworkSendsTextHolder').append('<tr><td><h5 style="margin-top:5px;">'
            + '<img src="../' + network.network_icon + '"> <strong>' + network.network_name + '</strong><br/><strong>' + network.send_total + '</strong> sends'
            + '</h5></td><td align="right"><h5 style="margin-top:5px;">'
            + '<strong>' + network.country_name + '</strong> <img src="../' + network.country_icon + '"><br/>(' + network.currency + ') <strong>' + network.cost + '</strong>'
            + '</h5></td></tr>'
         );
      }

      /************************************
       * FOR THE SEND RATE CHART
       ***********************************/
      function loadSendRateChartData()
      {
         showLoading("batchSendRateHolder");
         showLoading("batchDlrAndMOHolder");


         //load up the page tab we have selected
         $.post("../php/ajaxReportingBatchHandler.php", {task: 'get_batch_overview_chart_data', serviceId: fragServiceId, batchId: fragBatchId}).done(function (data) {
            var json = $.parseJSON(data);
            if (json.success)
            {
               setupSendRateChart(json.charts.send_rate_chart);
               setupDlrAndMOChart(json.charts.dlr_and_mo_chart);
            }
            else
            {
               showLoadingError("sendRateChartHolder", "A server error occurred, please contact technical support.");
            }
         }).fail(function () {
            showLoadingError("sendRateChartHolder", "A server error occurred, please contact technical support.");
         });
      }

      var sendRateChartOptions  = {

         ///Boolean - Whether grid lines are shown across the chart
         scaleShowGridLines : true,
         bezierCurve : true
      };


      function setupSendRateChart(ajaxData)
      {
         //setup the chart data for the daily range
         var data = {
            labels: ajaxData.labels,
            datasets: [
               {
                  label: "Send Rate",
                  fillColor: "rgba(220,220,220,0.2)",
                  strokeColor: "<?php echo $_SESSION['accountColour'] ?>",
                  pointColor: "<?php echo $_SESSION['accountColour'] ?>",
                  pointStrokeColor: "#fff",
                  pointHighlightFill: "#fff",
                  pointHighlightStroke: "rgba(220,220,220,1)",
                  data: ajaxData.values
               }
            ]
         };

         var chartWidth = $("#batchSendRateHolder").width() - 5;
         $("#batchSendRateHolder").html('<canvas id="batchSendRateChart" width="'+chartWidth+'" height="300"></canvas>')

         // Get context with jQuery - using jQuery's .get() method.
         var ctx = $("#batchSendRateChart").get(0).getContext("2d");
         var batchSendRateChart = new Chart(ctx).Line(data, sendRateChartOptions);
      }

      var dlrAndMOChartOptions  = {

         ///Boolean - Whether grid lines are shown across the chart
         scaleShowGridLines : true,
         bezierCurve : true,
         pointDot : false,
      };

      function setupDlrAndMOChart(ajaxData)
      {
         //setup the chart data for the daily range
         var data = {
            labels: ajaxData.labels,
            datasets: [
               {
                  label: "Successful Delivery",
                  fillColor: "rgba(220,220,220,0.2)",
                  strokeColor: "#2b542c",
                  pointColor: "#2b542c",
                  pointStrokeColor: "#fff",
                  pointHighlightFill: "#fff",
                  pointHighlightStroke: "rgba(220,220,220,1)",
                  data: ajaxData.delivered
               },
               {
                  label: "Replies Received",
                  fillColor: "rgba(220,220,220,0.2)",
                  strokeColor: "<?php echo $_SESSION['accountColour'] ?>",
                  pointColor: "<?php echo $_SESSION['accountColour'] ?>",
                  pointStrokeColor: "#fff",
                  pointHighlightFill: "#fff",
                  pointHighlightStroke: "rgba(220,220,220,1)",
                  data: ajaxData.replies
               }

            ]
         };



         var chartWidth = $("#batchDlrAndMOHolder").width() - 5;
         $("#batchDlrAndMOHolder").html('<canvas id="batchDlrAndMOChart" width="'+chartWidth+'" height="300"></canvas>')

         // Get context with jQuery - using jQuery's .get() method.
         var ctx2 = $("#batchDlrAndMOChart").get(0).getContext("2d");
         var batchDlrAndMOChart = new Chart(ctx2).Line(data, dlrAndMOChartOptions);
      }

      /*******************************************
       * HELPERS
       *******************************************/

      function getNetworkColour(mccmnc)
      {
         var color = "#333333";

         switch(mccmnc)
         {
            case '65501' :
            case 65501 :
               color = "#ef0920";
               break;
            case '65502' :
            case 65502 :
               color = "#0083c3";
               break;
            case '65507' :
            case 65507 :
               color = "#000000";
               break;
            case '65510' :
            case 65510 :
               color = "#ffbd00";
               break;
         }

         return color;
      }
   </script>


