<?php

   /**
    * This script is called by the manage services page, and runs certain ajax based tasks
    * @author - Doug Jenkinson
    */
   require_once("../../php/allInclusive.php");
   require_once("../../PluginAPI/PluginAPIAutoload.php");

   session_start();

   if(isset($_POST['service_id']))
   {
      $service_id = $_POST['service_id'];
   }
   else if(isset($_GET['service_id']))
   {
      $service_id = $_GET['service_id'];
   }
   else
   {
      echo '<p class="text-error">An error occurred, please refresh your browser.</p>';
      die();
   }

   //get the service object
   $service_obj = new Service($service_id);

   if(isset($_GET['start_date']) && isset($_GET['end_date']))
   {
      $start_date = new DateTime($_GET['start_date']);
      $end_date = new DateTime($_GET['end_date']);
   }
   else
   {
      $start_date = new DateTime('first day of this week');
      $end_date = new DateTime('today');
   }


   $start_date_string = $start_date->format('Y-m-d');
   $end_date_string = $end_date->format('Y-m-d');

   //get all the data for the graph by day
   $network_stats = Service::getFullNetworkSendStats($service_obj->service_id, $start_date_string, $end_date_string);
   //echo json_encode($network_stats);

   $csv_network_traffic_link = "../php/csv_reports/createCSVNetworkTraffic.php?service_id=".$service_id."&start_date=".$start_date_string."&end_date=".$end_date_string;
?>

<div class="row" xmlns:background-color="http://www.w3.org/1999/xhtml">
      <div class="col-lg-12">
         <p class="lead">This tab features a full breakdown of your sending by network and country.</p>
         <p>Showing report data for the date range <strong><?php echo htmlentities($start_date_string); ?></strong> to <strong><?php echo htmlentities($end_date_string); ?></strong>.</p>
      </div>
   </div>
   <div class="row">
      <div class="col-lg-4">
         <div class="box box-primary">
            <div class="box-header with-border">
               <h3 class="box-title"><i class="fa fa-pie-chart"></i> Network Send Ratio</h3>
            </div>
            <div class="box-body">
               <div id="chartNetworkSendsHolder">
                  <canvas id="chartNetworkSends" width="400" height="400"></canvas>
               </div>
            </div>
         </div>
      </div>
      <div class="col-lg-8">
         <div class="box box-primary">
            <div class="box-header with-border">
               <h3 class="box-title pull-left">
                  <i class="fa fa-server"></i> Network Traffic
               </h3>
               <div class="box-tools pull-right">
                  <a id="btnDownloadNetworkTrafficReport" href="<?php echo $csv_network_traffic_link; ?>" class="btn btn-xs btn-primary pull-right" style="color:white; margin-top:5px; background-color:<?php echo $_SESSION['accountColour']; ?>;">
                     <i class="fa fa-save"></i> Download CSV Report
                  </a>
               </div>
            </div>
            <div class="box-body">
               <div class="list-group">
                  <!-- NETWORK DISPLAY SLOT -->
                  <?php foreach($network_stats['networks'] as $network) { ?>
                     <div class="list-group-item">
                        <div class="row">
                           <div class="col-lg-3">
                              <h4 style="margin-top:5px;">
                                 <img src="../img/serviceproviders/<?php echo trim(substr($network['network_name'], strpos($network['network_name'], "/") + 1));?>.png">
                                 <?php echo htmlentities($network['network_name']); ?>
                              </h4>
                              <h5 style="margin-top:0px;">
                                 <img src="../img/flags/<?php echo (str_replace(' / ', '', $network['network_country']) == "" ? 'Special' : $network['network_country']); ?>.png">
                                 <?php echo htmlentities($network['network_country']); ?>
                              </h5>
                           </div>
                           <div class="col-lg-9" style="text-align:right;">
                              <h4 style="margin-top:5px;">
                                 <i class="fa fa-send"></i> <strong><?php echo $network['network_total_sends']; ?></strong> <small>total sends</small> |
                                 <i class="fa fa-send-o"></i> <?php echo $network['network_total_units']; ?> <small>units</small> |
                                 <span class="text-info"><i class="fa fa-money"></i> <?php echo number_format(($network['network_total_cost'] / 10000), 2, '.', ' ') ; ?>
                                 <small>total cost (Rate: <?php echo implode(', ', $network['network_costs_per_unit']); ?>)</small></span>
                              </h4>
                              <h5 style="margin-top:5px;">
                                 <span class="text-success"><i class="fa fa-check-circle"></i> <?php echo (isset($network['delivered']) ? $network['delivered'] : 0); ?> <small>delivered</small></span> |
                                 <span class="text-warning"><i class="fa fa-question-circle"></i> <?php echo (isset($network['pending']) ? $network['pending'] : 0); ?> <small>pending</small></span> |
                                 <span class="text-danger"><i class="fa fa-times-circle"></i> <?php echo (isset($network['failed']) ? $network['failed'] : 0); ?> <small>failed</small></span> |
                                 <span><i class="fa fa-minus-circle"></i> <?php echo (isset($network['rejected']) ? $network['rejected'] : 0); ?> <small>rejected</small></span>
                              </h5>
                           </div>
                        </div>
                     </div>
                  <?php } ?>
               </div>
            </div>
         </div>
      </div>
   </div>


   <script type="text/javascript">
      var fragServiceId = <?php echo (isset($service_id) ? $service_id : '-1') ?>;
      var startDate = "<?php echo (isset($start_date_string) ? $start_date_string : date('Y-m-d')) ?>";
      var endDate = "<?php echo (isset($end_date_string) ? $end_date_string : date('Y-m-d')) ?>";

      $(document).ready(function (e)
      {
         //load the chart data via ajax
         loadSendChartData();
      });

      function loadSendChartData()
      {
         showLoading("sendChartHolder");

         //load up the page tab we have selected
         $.post("../php/ajaxReportingHandler.php", {task: 'get_network_traffic_stats', serviceId: fragServiceId, startDate: startDate, endDate:endDate}).done(function (data) {
            var json = $.parseJSON(data);
            if (json.success)
            {
               setupAndShowNetworkSendsChart(json.network_stats);
            }
            else
            {
               showLoadingError("sendChartHolder", "A server error occurred, please contact technical support.");
            }
         }).fail(function () {
            showLoadingError("sendChartHolder", "A server error occurred, please contact technical support.");
         });
      }

      var chartNetworkSendsOptions  = {

         ///Boolean - Whether grid lines are shown across the chart
         scaleShowGridLines : true,
         barValueSpacing : 25,
         barShowStroke : false
      };


      function setupAndShowNetworkSendsChart(ajaxData)
      {
         var chartNetworkSendsData = [];
         $('#chartNetworkSendsHolder').empty();

         //setup the chart data for the daily range
         for(var i = 0; i < ajaxData.networks.length; ++ i)
         {
            //add the data to the graph array
            var network = ajaxData.networks[i];
            var colour = getNetworkColour(network.mccmnc);
            chartNetworkSendsData[i] = {'value':network.network_total_sends, 'label':network.network_name + '(approx ' + network.network_total_send_percent +' %)', 'color': "" + colour};

         }

         var chartWidth = $("#chartNetworkSendsHolder").width() - 5;
         $("#chartNetworkSendsHolder").html('<canvas id="chartNetworkSends" width="'+chartWidth+'" height="360"></canvas>')

         // Get context with jQuery - using jQuery's .get() method.
         var ctx = $("#chartNetworkSends").get(0).getContext("2d");
         var chartNetworkSends = new Chart(ctx).Doughnut(chartNetworkSendsData, chartNetworkSendsOptions);
      }

      /*******************************************
       * HELPERS
       *******************************************/

      function getNetworkColour(mccmnc)
      {
         var color = "#333333";

         switch(mccmnc)
         {
            case '65501' :
            case 65501 :
               color = "#ef0920";
               break;
            case '65502' :
            case 65502 :
               color = "#0083c3";
               break;
            case '65507' :
            case 65507 :
               color = "#000000";
               break;
            case '65510' :
            case 65510 :
               color = "#ffbd00";
               break;
         }

         return color;
      }
   </script>


