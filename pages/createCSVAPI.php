<?php

if (isset($_GET['cl']) && $_GET['cl'] != '') {
   $cl = 'service_id:' . $_GET['cl'];
   $range = $_GET['range'];
   $dateArr = explode(' - ', $_GET['range']);
   $startDate = $dateArr[0];
   $endDate = $dateArr[1];
   $reportName = $cl;
   /* if(isset($_GET['bId']) && $_GET['bId'] != '')
     {
     $bId = $_GET['bId'];
     } */
} else {
   $cl = 'service_id:*';
   $range = $_GET['range'];
   $dateArr = explode(' - ', $_GET['range']);
   $startDate = $dateArr[0];
   $endDate = $dateArr[1];
   if (!isset($_GET['net']) && $_GET['net'] != '1') {
      $startDate = strtotime($dateArr[0]);
      $endDate = strtotime($dateArr[1]);
   }
   if ($startDate == $endDate) {
      $endDate = $endDate + (24 * 60 * 60);
   }
   $startDate = $startDate * 1000;
   $endDate = $endDate * 1000;
   $reportName = 'All';
   /* if(isset($_GET['bId']) && $_GET['bId'] != '')
     {
     $bId = $_GET['bId'];
     } */
}

//ini_set('memory_limit', '8192M');
set_time_limit(3000);

header('Content-Type: text/csv; charset=utf-8');
header('Content-Disposition: attachment; filename=Report_' . $reportName . '.csv');
header("Cache-Control: no-cache, no-store, must-revalidate"); // HTTP 1.1
header("Pragma: no-cache");
header("Expires: 0");

if (isset($_GET['query']) && $_GET['query'] != '') {
   //$dlr
   //echo "<br><br><br>---BUERY-".$_GET['query'];
   //echo "<br>AND dlr:161 OR dlr:681";
   //echo "<br>-->".$qryStr;

   $qryArr = explode('.', $_GET['query']);
   $qryStr = 'AND (';
   for ($i = 0; $i < count($qryArr); $i++) {
      $qryStr .= ' dlr:' . $qryArr[$i] . ' OR';
   }
   $qryStr = substr($qryStr, 0, -3) . ')';
} else {
   $_GET['query'] = '';
   $qryStr = '';
}

if (isset($_GET['searchTerm']) && $_GET['searchTerm'] != '') {
   $searchT = $_GET['searchTerm'];

   $searchTerm = ',{
                         "multi_match" : {
                            "query":    "' . $searchT . '",
                            "fields": ["smsc", "src", "dest", "content", "mccmnc"]
                          }   
                        }';
} else {
   $searchT = '';
   $searchTerm = '';
}

$qry = '{
    		 "size":1000000,
              "fields" : ["mtsms_id","timestamp","src","dest","content","dlr","mccmnc","smsc","rdnc"],
              "query": {
                "filtered": {
                  "query": {
                    "bool" : {
                        "must" : [
                            {
                             "query_string": {
                             "query": "' . $cl . ' ' . $qryStr . '",
                             "analyze_wildcard": true
                                }
                            }
                            ' . $searchTerm . '
                        ]
                    }
                  },
                  "filter": {
                        "bool": {
                          "must": [
                            {
                              "range": {
                                "timestamp": {
                                  "gte": ' . $startDate . ',
                                  "lte": ' . $endDate . '
                                }
                              }
                            }
                          ],
                          "must_not": []
                        }
                      }
                }
              }  
            }';

$allStats = runRawMTSMSElasticsearchQuery($qry);

$nl = "\n";

//echo $qry.$nl;

if (isset($_GET['admin']) && $_GET['admin'] == 1) {
   echo "SMS_id, Date, From, To, Content, Network, SMSC, Status, RDNC" . $nl;
} else {
   echo "SMS_id, Date, From, To, Content, Network, Status" . $nl;
}
//echo $qry.$nl;
foreach ($allStats as $key => $value) {
   if ($key == 'hits') {
      $hits = count($value['hits']);
      for ($i = 0; $i < $hits; $i++) {
         $netw = ($value['hits'][$i]['fields']['mccmnc']['0']);
         $smsc = $value['hits'][$i]['fields']['smsc']['0'];
         $posSmsc = strrpos($smsc, '_');
         $smsc = ucfirst(substr($smsc, 0, $posSmsc));
         $time = str_replace('T', ' ', $value['hits'][$i]['fields']['timestamp']['0']);
         $time = strstr($time, '+', true);
         $timeSec = strrpos($time, ':');
         $time = substr($time, 0, $timeSec);

         $dlrPos = $value['hits'][$i]['fields']['dlr']['0'];
         if (($dlrPos & 1) == 1) {
            $dlrPos = 'Delivered';
         } elseif (($dlrPos & 16) == 16) {
            $dlrPos = 'Rejected';
         } elseif (($dlrPos & 32) == 32 && ($dlrPos & 1) == 0 && ($dlrPos & 2) == 0) {
            $dlrPos = 'Pending';
         } elseif (($dlrPos & 2) == 2) {
            $dlrPos = 'Failed';
         }

         $rdnc = $value['hits'][$i]['fields']['rdnc']['0'];

         if (isset($_GET['admin']) && $_GET['admin'] == 1) {
            echo '' . $value['hits'][$i]['fields']['mtsms_id']['0'] . ',' . $time . ',' . $value['hits'][$i]['fields']['src']['0'] . ',' . $value['hits'][$i]['fields']['dest']['0'] . ',"' . preg_replace('/[^(\x20-\x7F)]*/', '', $value['hits'][$i]['fields']['content']['0']) . '",' . $value['hits'][$i]['fields']['mccmnc']['0'] . ',' . $smsc . ',' . $dlrPos . ',' . $rdnc . $nl;
         } else {
            echo '' . $value['hits'][$i]['fields']['mtsms_id']['0'] . ',' . $time . ',' . $value['hits'][$i]['fields']['src']['0'] . ',' . $value['hits'][$i]['fields']['dest']['0'] . ',"' . preg_replace('/[^(\x20-\x7F)]*/', '', $value['hits'][$i]['fields']['content']['0']) . '",' . $value['hits'][$i]['fields']['mccmnc']['0'] . ',' . $dlrPos . $nl;
         }
      }
   }
}
?>
