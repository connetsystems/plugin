<?php
/**
 * Include the header template which sets up the HTML
 *
 * Don't forget to include template_import_script before any Javascripting
 * Don't forget to include template footer.php at the bottom of the page as well
 */
$headerPageTitle = "View Service"; //set the page title for the template import
TemplateHelper::initialize();

//echo "<br>Session->".$_SESSION['serviceId']."<- = ->".$_SESSION['resellerId'];
$singleView = 'block;';

$noShow = 0;

if ($_SESSION['serviceId'] != '' && $_SESSION['resellerId'] != '') {
   if ($_SESSION['serviceId'] != $_SESSION['resellerId']) {
      if (!isset($_GET['client'])) {
         $_GET['client'] = $_SESSION['accountName'] . ' - ' . $_SESSION['serviceName'] . " - " . $_SESSION['serviceId'] . "&tog1=in";
         $singleView = 'none;';
         $noShow = 1;
      }
   } elseif ($_SESSION['serviceId'] == $_SESSION['resellerId']) {
      $noShow = 1;
   }
   /* elseif($_SESSION['resellerId'] == '')
     {

     $noShow = 1;
     } */
}

/* if($_SESSION['serviceId'] != '' && $_SESSION['resellerId'] != '')
  {
  if($_SESSION['serviceId'] != $_SESSION['resellerId'])
  {
  if(!isset($_GET['client']))
  {
  $_GET['client'] = ($_SESSION['accountName'].' - '.$_SESSION['serviceName']." - ".$_SESSION['serviceId']);
  $singleView = 'none;';
  }
  }
  } */

$userCount = 0;
$routeCount = 0;
$scrPos = 0;

$accountName = "";
$serviceName = "";

$tog1 = "in";
$tog2 = "in";
$tog3 = "in";
$tog4 = "in";
$tog5 = "in";
$tog6 = "in";

if (isset($_GET['client'])) {
   //echo '<br><br><br>client='.$_GET['client'];
   $cl = ($_GET['client']);
   //echo '<br><br><br>'.$cl;
   $clp = strrpos($cl, ' ');
   //echo '<br><br><br>clp ='.$clp;
   $cl = trim(substr($cl, $clp, strlen($cl)));
   //echo '<br><br><br>cl='.$cl;
   $dsp = 'inline';
   $dir = '../img/routes';
   $serProviders = scandir($dir);
} else {
   $_GET['client'] = "0 - Please select a client...";
   $cl = strstr($_GET['client'], ' ', true);
   $dsp = 'none';
}

if (isset($_SESSION['accountId'])) {
   $acId = $_SESSION['accountId'];
} else {
   $acId = null;
}
$clients = getServicesInAccount($acId);


if ($cl != 0) {
   //echo "<br>1-----".$cl;
   if (strpos($cl, '&')) {
      $cl = strstr($cl, '&', true);
   }
   //echo "<br>2-----".$cl;
   $client = getClient($cl);
   $defaultRouteData = getDefaultRouteData($cl);
   $payments = getPayments($cl);
   $users = getUsers($cl);

   $funds = getFunds($cl);

   /*
     $alerts = getAlerts($cl);
     $manager = getManager($cl);
     $urls = getURLs($cl);
    */

   $serviceData = getServiceData($cl);

   /* foreach ($serviceData as $key => $value) 
     {
     echo '<pre>';
     print_r($value);
     echo '</pre>';
     } */

   $urls = array();

   //$managers = getManagers();
   //$portedRouteData = getPortedRouteData($cl);
}
?>
<aside class="right-side">
   <section class="content-header">
      <h1>
         View Service
         <!--small>Control panel</small-->
      </h1>
   </section>
   <section class="content">
      <div class="row col-lg-12">
         <div class="box box-solid" style="height:66px; margin-top:-15px;display:<?php echo $singleView; ?>">
            <form action="viewserviceAdmin.php" method="get" id="clientList" class="sidebar-form" style="border:0px;">
               <div class="form-group">
                  <label>Service List</label>
                  <select class="form-control" name="client" form="clientList" OnChange="reloadOnSelect(this.value);">
                     <?php
                     if ($cl == '0') {
                        echo '<option SELECTED>Please select a service to view...</option>';
                     }
                     foreach ($clients as $key => $value) {
                        $sId = $value['service_id'];
                        if ($cl == $sId) {
                           $accountName = $value['account_name'];
                           $serviceName = $value['service_name'];
                           echo "<option name='" . $sId . "' SELECTED>" . $accountName . " - " . $serviceName . " - " . $sId . "</option>";
                        } else {
                           echo "<option name='" . $sId . "'>" . $value['account_name'] . " - " . $value['service_name'] . " - " . $sId . "</option>";
                        }
                     }
                     ?>
                  </select>
               </div>
               <br>
            </form>
         </div>
         <!--button type="submit" class="btn btn-primary">Select</button-->
         <!-- /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////// -->
         <!--form action="admin.php" method="get" id="accountStatus" class="sidebar-form"-->
         <div class="box box-solid" style="display:<?php echo $dsp; ?>;">
            <div class="box-header">
               <h3 class="box-title"><?php echo $serviceName; //$_GET['client'];  ?></h3>
            </div><!-- /.box-header -->
            <div class="box-body col-md-6">
               <div class="panel box box-info" style="padding-bottom:1px; border-top-color:<?php echo $accRGB; ?>;">
                  <div class="box-header">
                     <h4 class="box-title">

                        Service Information

                     </h4>
                  </div>
                  <div id="collapse1" class="panel-collapse collapse <?php echo $tog1; ?>">
                     <div class="box-body">
                        <div class="callout callout-info" style="margin-bottom:10px;">
                           <!--h4>Service Details</h4-->
                           <p>In this section you can view all service related information.</p>
                        </div>
                        <div class="box-body">
                           <?php
                           foreach ($serviceData as $key => $value) {
                              $a[0] = number_format($value['service_notification_threshold'] / 10000, 2, '.', ' ');
                              $a[1] = $value['service_notification_email'];
                              $a[2] = $value['service_notification_sms'];

                              $urls['dlrLegacyUrl'] = $value['DLR_ADDRESS_LEGACY'];
                              $urls['dlrLegacyStatus'] = $value['DLR_STATUS_LEGACY'];
                              $urls['moLegacyUrl'] = $value['MO_ADDRESS_LEGACY'];
                              $urls['moLegacyStatus'] = $value['MO_STATUS_LEGACY'];
                              $urls['smppStatus'] = $value['SMPP'];
                              /* if($key == 0)
                                { */
                              echo "<p style='background-color:;'>";
                              echo "<b>Account Name:</b> " . $value['account_name'];
                              echo "</p>";

                              echo "<p>";
                              echo "<b>Service Login:</b> " . $value['service_login'];
                              echo "</p>";

                              echo "<p>";
                              echo "<b>Service Name:</b> " . $value['service_name'];
                              echo "</p>";


                              echo "<p>";
                              echo "<b>Service Manager:</b> " . $value['user_fullname'];
                              echo "</p>";

                              echo "<p>";
                              if ($value['service_status'] == 'ENABLED') {
                                 echo "<b>Status:</b> <span class='label label-success'>" . $value['service_status'];
                              } else {
                                 echo "<b>Status:</b> <span class='label label-danger'>" . $value['service_status'];
                              }
                              echo "</p>";

                              echo "<p>";
                              echo "<b>Payment Type:</b> " . $value['service_credit_billing_type'];
                              echo "</p>";


                              if (isset($value['service_credit_available'])) {
                                 $fn = $value['service_credit_available'] / 10000; //money_format('%=*^-14#8.2i', $funds['1']);
                                 $fn = number_format($fn, 2, '.', ' ');
                                 if ($value['service_credit_available'] < 10000000 && $fn > 0) {
                                    echo '<div class="callout callout-warning" style="margin-bottom:0px;">';
                                    echo '<div style="width:80%;">';
                                    echo '<p><b>Alert!</b> This balance is running low!</p>';
                                    echo "<p><b>Current Balance:&nbsp;&nbsp;</b><span class='label label-warning'>" . $fn . "</p>";
                                    echo '</div>';
                                    echo '<div style="float:right; margin-top:-36px;margin-right: 10px;">';
                                    echo '<i class="fa fa-exclamation-triangle" style="color:#d1ba8a; font-size:20pt;"></i>';
                                    echo '</div>';
                                    echo '</div>';
                                 } elseif (substr($value['service_credit_available'], 0, 1) != '-' && $fn != 0) {
                                    echo '<div class="callout callout-success" style="margin-bottom:0px;">';
                                    echo '<div style="width:80%;">';
                                    echo "<b>Current Balance:&nbsp;&nbsp;</b><span class='label label-success'>" . $fn;
                                    echo '</div>';
                                    echo '<div style="float:right; margin-top:-24px;margin-right: 10px;">';
                                    echo '<i class="fa fa-thumbs-up" style="color:#5cb157; font-size:20pt;"></i>';
                                    echo '</div>';
                                    echo '</div>';
                                 } elseif ($fn == 0) {
                                    echo '<div class="callout callout-danger" style="margin-bottom:0px;">';
                                    echo '<div style="width:80%;">';
                                    echo '<p><b>Alert!</b> This balance is zero!!</p>';
                                    echo "<p><b>Current Balance:&nbsp;&nbsp;</b><span class='label label-danger'>" . $fn . "</p>";
                                    echo '</div>';
                                    echo '<div style="float:right; margin-top:-36px;margin-right: 10px;">';
                                    echo '<i class="fa fa-exclamation-triangle" style="color:#c99b9d; font-size:20pt;"></i>';
                                    echo '</div>';
                                    echo '</div>';
                                 } else {
                                    echo '<div class="callout callout-danger" style="margin-bottom:0px;">';
                                    echo '<div style="width:80%;">';
                                    echo '<p><b>Alert!</b> This balance is negative!!!</p>';
                                    echo "<p><b>Current Balance:&nbsp;&nbsp;</b><span class='label label-danger'>" . $fn . "</p>";
                                    echo '</div>';
                                    echo '<div style="float:right; margin-top:-36px;margin-right: 10px;">';
                                    echo '<i class="fa fa-exclamation-triangle" style="color:#c99b9d; font-size:20pt;"></i>';
                                    echo '</div>';
                                    echo '</div>';
                                 }
                              } else {
                                 echo '<div class="callout callout-danger" style="margin-bottom:0px;">';
                                 echo '<div style="width:80%;">';
                                 echo '<p><b>Alert!</b> This balance is zero!!</p>';
                                 if (isset($fn)) {
                                    echo "<p><b>Current Balance:&nbsp;&nbsp;</b><span class='label label-success'>" . $fn . "</p>";
                                 } else {
                                    echo "<p><b>Current Balance:&nbsp;&nbsp;</b><span class='label label-success'>No Data Found...</p>";
                                 }
                                 echo '</div>';
                                 echo '<div style="float:right; margin-top:-36px;margin-right: 10px;">';
                                 echo '<i class="fa fa-exclamation-triangle" style="color:#c99b9d; font-size:20pt;"></i>';
                                 echo '</div>';
                                 echo '</div>';
                              }
                           }
                           ?>
                        </div><!-- /.box-body -->
                     </div><!-- /.box-body -->
                  </div>
               </div>
               <div class="panel box box-connet" style="padding-bottom:1px; border-top-color:<?php echo $accRGB; ?>;">
                  <div class="box-header">
                     <h4 class="box-title">
                        Users
                     </h4>
                  </div>
                  <div id="collapse3" class="panel-collapse collapse <?php echo $tog3; ?>">
                     <div class="callout callout-info" style='margin-left:10px; margin-right:10px; margin-top:10px; height:auto; margin-bottom:0px;'>
                        <div style="width:80%;">
                           <!--h4>Users Details</h4-->
                           <p>In this section, you can view all user logins that have access to your service.</p>
                        </div>
                     </div>
                     <div class="callout callout-warning" style='margin-left:10px; margin-right:10px; margin-top:0px; height:auto; margin-bottom:0px;'>
                        <div style="width:80%;">
                           <p>
                              The user which is highlighted in orange, is the main API user. This user will be used when integrating to us via the API.
                           </p>
                        </div>
                        <div style="float:right; margin-top:-32px;margin-right:20px;"><i class="fa fa-warning" style="color:#d1ba8a; font-size:20pt;"></i>
                        </div>
                     </div>
                     <div class="box-body">
                        <div class="box-body table-responsive no-padding">
                           <table class="table table-hover">
                              <tr>
                                 <th>Username</th>
                                 <th>Password</th>
                                 <th>Status</th>
                              </tr>
                              <?php
                              if (isset($users)) {
                                 foreach ($users as $key => $value) {
                                    if ($value['user_username'] === $value['service_login']) {
                                       $rowColor = ' style="background-color:#f4e8c6;"';
                                    } else {
                                       $rowColor = '';
                                    }

                                    echo '<tr' . $rowColor . '>';
                                    echo '<td>' . $value['user_username'] . '</td>';
                                    echo '<td>' . $value['user_password'] . '</td>';

                                    if ($value['user_status'] === "ENABLED") {
                                       echo '<td><i class="fa fa-check" style="color:#00ff00;"></i>&nbsp;&nbsp;ENABLED</td>';
                                    } else {
                                       echo '<td><i class="fa fa-close" style="color:#ff0000;"></i>&nbsp;&nbsp;DISABLED</td>';
                                    }
                                    echo '</td>';
                                 }
                              }
                              ?>
                           </table>
                        </div>
                     </div>
                  </div>
               </div>
            </div>

            <div class="box-body col-md-6">
               <div class="panel box box-danger" style="padding-bottom:1px; border-top-color:<?php echo $accRGB; ?>;">
                  <div class="box-header">
                     <h4 class="box-title">

                        Low Balance Alerts

                     </h4>
                  </div>
                  <div id="collapse5" class="box-body panel-collapse collapse <?php echo $tog5; ?>">
                     <div class="callout callout-info" style="margin-bottom:0px;">
                        <!--h4>Low Balance Alerts Notifications</h4-->
                        <p>
                           In this section, you can view at which threshold limit you will receive notifications (SMS/EMAIL/BOTH).
                        </p>
                     </div>
                     <div class="box-body">
                        <div class="box-body" style="padding-bottom:0px;">
                           <p><b>Notification Threshold / Limit:</b> <span class='label label-success'><?php
                                 if (isset($a[0])) {
                                    echo $a[0];
                                 } else {
                                    echo "Not set";
                                 }
                                 ?></span></p>
                           <p><b>Notification Email:</b> <?php
                              if (isset($a[1])) {
                                 echo $a[1];
                              } else {
                                 echo "Not set";
                              }
                              ?></p>
                           <p style="margin-bottom:0px;"><b>Notification SMS Number:</b> <?php
                              if (isset($a[2])) {
                                 echo $a[2];
                              } else {
                                 echo "Not set";
                              }
                              ?></p>
                        </div>
                     </div>
                  </div>
               </div>
               <div class="panel box box-connet" style="padding-bottom:1px; border-top-color:<?php echo $accRGB; ?>;">
                  <div class="box-header">
                     <h4 class="box-title">
                        DLR Responses and MOSMS URL's
                     </h4>
                  </div>
                  <div id="collapse6" class="box-body panel-collapse collapse <?php echo $tog6; ?>">
                     <div class="callout callout-info">
                        <!--h4>DLR and MOSMS Details</h4-->
                        <p>In this section, you can view what the delivery responses and reply SMS URL is set to. Delivery responses and reply SMSs are received via the API on the selected URL's that are specified below. (If you would like to update the URL, please contact support).</p>

                     </div>
                     <div class="box-body" style="padding-bottom:0px;">
                        <p><b>DLR URL (Legacy / HTTP): </b><?php
                           if (isset($urls['dlrLegacyUrl'])) {
                              echo $urls['dlrLegacyUrl'];
                           } else {
                              echo 'Not set';
                           }
                           ?></p>

                        <p><b>DLR STATUS (Legacy / HTTP): </b><?php
                           if (isset($urls['dlrLegacyStatus'])) {
                              if ($urls['dlrLegacyStatus'] == 'ENABLED') {
                                 echo "<span class='label label-success'>" . $urls['dlrLegacyStatus'];
                              } else {
                                 echo "<span class='label label-danger'>" . $urls['dlrLegacyStatus'];
                              }
                              //echo $urls['dlrLegacyStatus'];
                           }
                           ?></p>

                        <p><b>MOSMS URL (Legacy / HTTP): </b><?php
                           if (isset($urls['moLegacyUrl'])) {
                              echo $urls['moLegacyUrl'];
                           } else {
                              echo 'Not set';
                           }
                           ?></p>

                        <p><b>MOSMS STATUS (Legacy / HTTP): </b><?php
                           if (isset($urls['moLegacyStatus'])) {
                              if ($urls['moLegacyStatus'] == 'ENABLED') {
                                 echo "<span class='label label-success'>" . $urls['moLegacyStatus'];
                              } else {
                                 echo "<span class='label label-danger'>" . $urls['moLegacyStatus'];
                              }
                           }
                           ?></p>

                        <p><b>SMPP STATUS: </b><?php
                           if (isset($urls['smppStatus'])) {
                              if ($urls['smppStatus'] == 'ENABLED') {
                                 echo "<span class='label label-success'>" . $urls['smppStatus'];
                              } else {
                                 echo "<span class='label label-danger'>" . $urls['smppStatus'];
                              }
                           }
                           ?></p>
                     </div>
                  </div>
               </div>
            </div>

            <div class="box-body col-md-12" style="margin-top:-20px;">
               <div class="panel box box-warning" style="padding-bottom:1px; border-top-color:<?php echo $accRGB; ?>;">
                  <div class="box-header">
                     <h4 class="box-title">

                        Routes

                     </h4>
                  </div>
                  <div id="collapse2" class="box-body panel-collapse collapse <?php echo $tog2; ?>">
                     <div class="callout callout-info" style='height:auto;margin-bottom:-0px;'>
                        <div style="width:100%;height:auto;" >
                           <!--h4>Routes Details</h4-->
                           <p>In this section you can view the SMS routes assigned to the service. Routes are displayed per Country, Network, Status and Rate.
                           </p>
                           <!--a class="btn btn-block btn-social btn-dropbox " style="background-color: #367fa9; color: #ffffff; border: 2px solid; border-radius: 6px !important; width: 200px;" onclick="toggleContent('addRoute');">
                               <i class="fa fa-link"></i>Add New Routes
                           </a-->
                        </div>
                     </div>
                     <div class="box-body">
                        <div class="box-body table-responsive no-padding">
                           <table id="routesTable" class="table table-hover table-bordered table-striped">
                              <thead>
                                 <tr role="row">
                                    <th class="sorting_asc" role="columnheader" tabindex="0" aria-controls="routesTable" rowspan="1" colspan="1" aria-sort="ascending" aria-label="Country: activate to sort column descending">Country</th>
                                    <th>Network</th>
                                    <th>Status</th>
                                    <th style="text-align:center;">Rate</th>
                                    <?php
// Jacques
                                    if ($noShow == 0) {
                                       echo'<th style="text-align:center;">RDNC</th>';
                                    }
                                    ?>

                                    <th style="text-align:center;">Cur</th>

                                 <?php
// Jacques
                                 if ($noShow == 0) {
                                    echo '<th>Collection</th>';
                                    echo '<th>Default</th>';
                                    echo '<th>Ported</th>';
                                 }
                                 ?>
                                 </tr>
                              </thead>
                              <tbody>
                                 <?php
                                 if (isset($defaultRouteData)) {
                                    foreach ($defaultRouteData as $key => $value) {
                                       if (substr($value['COUNTRY'], 0, 3) == "USA") {
                                          $flag = "USA";
                                       } else {
                                          $flag = $value['COUNTRY'];
                                       }
                                       $flag = str_replace('/', '&', $flag);

                                       $s1 = strrpos($value['NETWORK'], '/');
                                       $s2 = strrpos($value['NETWORK'], '(');

                                       $logo = substr($value['NETWORK'], $s1, $s2);
                                       $logo = trim($logo);

                                       if ($logo != "Cell C") {  // | $logo != 'Vodacom SA' | $logo != 'MTN-SA'
                                          if ($logo != 'Vodacom SA') {
                                             if ($logo != 'MTN-SA') {
                                                if ($logo != 'Telkom Mobile') {
                                                   if ($logo != 'MTN (Nigeria)') {
                                                      $logo = substr($value['NETWORK'], $s1 + 1, $s2 - $s1 - 2);
                                                      $logo = trim($logo);
                                                   }
                                                }
                                             }
                                          }
                                       }

                                       echo '<tr >';
                                       echo '<td style="vertical-align:middle;"><img src="../img/flags/' . $flag . '.png">&nbsp;&nbsp;' . $value['COUNTRY'] . '</td>';
                                       echo '<td style="vertical-align:middle;"><img src="../img/serviceproviders/' . $logo . '.png">&nbsp;&nbsp;' . $value['NETWORK'] . '</td>';
                                       if ($value['STATUS'] == 'ENABLED') {
                                          echo '<td style="vertical-align:middle;"><i class="fa fa-check" style="color:#00ff00;"></i>&nbsp;&nbsp;' . $value['STATUS'] . '</td>';
                                       } else {
                                          echo '<td style="vertical-align:middle;"><i class="fa fa-times" style="color:#ff0000;"></i>&nbsp;&nbsp;' . $value['STATUS'] . '</td>';
                                       }

                                       if (strpos($value['RATE'], ',')) {
                                          $rateV = strstr($value['RATE'], ',', true);
                                       } else {
                                          $rateV = $value['RATE'];
                                       }

                                       $rdncVp = strrpos($value['RATE'], ',');
                                       $rdncV = substr($value['RATE'], $rdncVp + 1);
                                       if ($rdncVp == '') {
                                          $rateV = $value['RATE'];
                                          $rdncV = '0';
                                       }
                                       if (strlen($rdncV) < 4) {
                                          $rdncVn = '';
                                          for ($c = 0; $c < (4 - strlen($rdncV)); $c++) {
                                             $rdncVn .= '0';
                                          }
                                          $rdncV = $rdncVn . $rdncV;
                                       }
                                       if (strlen($rateV) < 4) {
                                          $rateVn = '';
                                          for ($c = 0; $c < (4 - strlen($rateV)); $c++) {
                                             $rateVn .= '0';
                                          }
                                          $rateV = $rateVn . $rateV;
                                          //echo '<br>RAte='.$rateV;
                                       }
                                       //echo '<td style="vertical-align:middle;"><center><div class="fakey" style="padding-left:5px;width:60px;"><div style="display: inline-table;">0.</div><input type="text" onkeypress="return isNumberKey(event)" name="routeRate'.($key+1).'" placeholder="" style="background:#ffffff;width:40px;border-color:#929292;border:0px;" maxlength="4" value="'.$rateV.'"></div></center></td>';
                                       //echo '<td style="vertical-align:middle;"><center><div class="fakey" style="padding-left:5px;width:60px;"><div style="display: inline-table;">0.</div><input type="text" onkeypress="return isNumberKey(event)" name="routeRDNC'.($key+1).'" placeholder="" style="background:#ffffff;width:40px;border-color:#929292;border:0px;" maxlength="4" value="'.$rdncV.'"></div></center></td>';

                                       /* $rdncVp = strrpos($value['RATE'], ',');
                                         $rdncV = substr($value['RATE'], $rdncVp); */
                                       echo '<td style="vertical-align:middle;"><center><div class="fakey1" style="border:0px;padding-left:10px;width:60px;"><div style="display: inline-table;">0.</div>' . (string) $rateV . '</div></center></td>';

                                       // Jacques
                                       if ($noShow == 0) {
                                          echo '<td style="vertical-align:middle;"><center><div class="fakey1" style="border:0px;padding-left:10px;width:60px;"><div style="display: inline-table;">0.</div>' . $rdncV . '</div></center></td>';
                                       }

                                       echo '<td style="text-align:center;">' . $value['route_currency'] . '</td>';

                                       // Jacques
                                       if ($noShow == 0) {
                                          echo '<td style="vertical-align:middle;">' . $value['COLLECTION'] . '</td>';

                                          $mccNetSubCont = false;
                                          foreach ($serProviders as $key13 => $value13) {
                                             $imageT = strstr($value13, '.', true);

                                             if ($imageT == $value['DEFAULT']) {
                                                $mccNetSubCont = $imageT;
                                                break;
                                             }
                                          }

                                          //MTN provider name is lower case mtn_smsc hat to update the varible to Uppercase
                                          if ($value['DEFAULT'] == 'mtn') {
                                             $value['DEFAULT'] = 'MTN';
                                          }
                                          //Vodacom provider name is lower case vodacom_smsc hat to update the varible to Uppercase
                                          if ($value['DEFAULT'] == 'vodacom') {
                                             $value['DEFAULT'] = 'Vodacom SA';
                                          }
                                          //Mtc provider name is lower case mtc_smsc hat to update the varible to Uppercase
                                          if ($value['DEFAULT'] == 'mtc') {
                                             $value['DEFAULT'] = 'MTC';
                                          }

                                          if ($mccNetSubCont != false) {
                                             //echo '<td style="vertical-align:middle"><img src="../img/routes/'.$value['DEFAULT'].'.png">&nbsp;&nbsp;'.$value['DEFAULT'].'</td>';
                                             echo '<td style="vertical-align:middle"><img src="../img/serviceproviders/' . $value['DEFAULT'] . '.png">&nbsp;&nbsp;' . $value['DEFAULT'] . '</td>';
                                          } else {

                                             //echo '<td style="vertical-align:middle;">-'.$value['DEFAULT'].'-</td>';
                                             echo '<td style="vertical-align:middle"><img src="../img/serviceproviders/' . $value['DEFAULT'] . '.png">&nbsp;&nbsp;' . $value['DEFAULT'] . '</td>';
                                          }

                                          $mccNetSubCont2 = false;
                                          foreach ($serProviders as $key14 => $value14) {
                                             $imageT2 = strstr($value14, '.', true);

                                             if ($imageT2 == $value['PORTED']) {
                                                $mccNetSubCont2 = $imageT2;
                                                break;
                                             }
                                          }

                                          //MTN provider name is lower case mtn_smsc hat to update the varible to Uppercase
                                          if ($value['PORTED'] == 'mtn') {
                                             $value['PORTED'] = 'MTN';
                                          }
                                          //Vodacom provider name is lower case vodacom_smsc hat to update the varible to Uppercase
                                          if ($value['PORTED'] == 'vodacom') {
                                             $value['PORTED'] = 'Vodacom SA';
                                          }
                                          //Mtc provider name is lower case mtc_smsc hat to update the varible to Uppercase
                                          if ($value['PORTED'] == 'mtc') {
                                             $value['PORTED'] = 'MTC';
                                          }
                                          if ($mccNetSubCont2 != false) {
                                             //echo '<td style="vertical-align:middle"><img src="../img/routes/'.$value['PORTED'].'.png">&nbsp;&nbsp;'.$value['PORTED'].'</td>';
                                             echo '<td style="vertical-align:middle"><img src="../img/serviceproviders/' . $value['PORTED'] . '.png">&nbsp;&nbsp;' . $value['PORTED'] . '</td>';
                                          } else {
                                             echo '<td style="vertical-align:middle"><img src="../img/serviceproviders/' . $value['PORTED'] . '.png">&nbsp;&nbsp;' . $value['PORTED'] . '</td>';
                                          }
                                       }

                                       echo '</tr>';
                                       $routeCount++;
                                    }
                                 }
                                 ?>
                              </tbody>
                           </table>
                        </div><!-- /.box-body -->
                     </div>
                  </div>
               </div>
               <div class="panel box box-success" style="padding-bottom:1px; border-top-color:<?php echo $accRGB; ?>;">
                  <div class="box-header">
                     <h4 class="box-title">

                        Payments

                     </h4>
                  </div>
                  <div id="collapse4" class="panel-collapse collapse <?php echo $tog4; ?>">
                     <div class="box-body">
                        <div class="callout callout-info" style="margin-bottom:10px;">
                           <!--h4>Payment Details</h4-->
                           <p>In this section, you can view when credits were purchased and the prior balance before the new purchase was made.</p>
                        </div>
                        <div class="box-body">
                           <div class="box-body table-responsive no-padding">
                              <table class="table table-hover">
                                 <tr>
                                    <th>#</th>
                                    <th>Date</th>
                                    <th>Description</th>
                                    <th>Value</th>
                                    <th>Previous Balance</th>
                                 <?php
                                 if ($noShow == 0) {
                                    echo '<th>User</th>';
                                 }
                                 ?>
                                 </tr>
                                 <?php
                                 if (isset($payments)) {
                                    foreach ($payments as $key => $value) {
                                       echo '<tr>';
                                       echo '<td>' . ($key + 1) . '</td>';
                                       echo '<td>' . $value['payment_timestamp'] . '</td>';
                                       echo '<td>' . $value['payment_description'] . '</td>';
                                       $value['Value'] = str_replace(",", "", $value['Value']);
                                       $value['Value'] = str_replace(".", "", $value['Value']);
                                       $value['Value'] = $value['Value'] / 10000; //money_format('%=*^-14#8.2i', $funds['1']);
                                       $fn1 = number_format($value['Value'], 2, '.', ' ');

                                       if (substr($value['Value'], 0, 1) != '-') {
                                          echo '<td><span class="label label-success">' . $fn1 . '</span></td>';
                                       } else {
                                          echo '<td><span class="label label-danger">' . $fn1 . '</span></td>';
                                       }

                                       $value['Previous Balance'] = str_replace(",", "", $value['Previous Balance']);
                                       $value['Previous Balance'] = str_replace(".", "", $value['Previous Balance']);
                                       $value['Previous Balance'] = $value['Previous Balance'] / 10000; //money_format('%=*^-14#8.2i', $funds['1']);
                                       $fn2 = number_format($value['Previous Balance'], 2, '.', ' ');

                                       //$fn2 = $value['Previous Balance']/10000;//money_format('%=*^-14#8.2i', $funds['1']);
                                       if (substr($value['Previous Balance'], 0, 1) != '-') {
                                          echo '<td><span class="label label-success">' . $fn2 . '</span></td>';
                                       } else {
                                          echo '<td><span class="label label-danger">' . $fn2 . '</span></td>';
                                       }
                                       if ($noShow == 0) {
                                          echo '<td>' . $value['user_username'] . '</td>';
                                       }
                                       echo '</tr>';
                                    }
                                 }
                                 ?>
                              </table>
                           </div><!-- /.box-body -->
                        </div><!-- /.box-body -->
                     </div><!-- /.box-body -->
                  </div>
               </div>
            </div>
         </div>
      </div>
   </section>
</aside>

<!--  THE JAVASCRIPT IMPORT ALWAYS APPEARS BEFORE LOCAL JAVASCRIPT AT THE BOTTOM OF THE PAGE -->
<?php include("template_import_script.php"); //must import all scripts first   ?>
<!-- END JAVASCRIPT IMPORT -->

<script type="text/javascript">
   function callPHP(varToCheck, controller, sId)
   {
      console.log("1 vartoCheck:->" + varToCheck + "<-  serviceName:->" + '<?php echo $serviceName; ?>' + '<-');

      if (controller == "clientInfo")
      {
         if (varToCheck != '<?php echo $serviceName; ?>')
         {
            console.log("looking for " + varToCheck);
            $.ajax(
                    {
                       url: "../php/ajaxHandler.php",
                       data: {serviceName: varToCheck, serviceId: sId},
                       type: "POST",
                       success: function (result)
                       {
                          console.log("result ->" + result + "<- looked for ->" + varToCheck + "<- ");

                          if (result == varToCheck)
                          {
                             console.log(result + "=result_ERROR=" + varToCheck);
                             var contentId = document.getElementById("clientInfoErr");
                             contentId.style.display = "block";

                             var clientInfoBox = document.getElementById("clientInfoBoxFooter");
                             clientInfoBox.style.height = "124px";

                             var clientSuccessBox = document.getElementById("clientSuccess");
                             if (clientSuccessBox != null)
                             {
                                clientSuccessBox.style.display = "none";
                             }

                             var clientWarningBox = document.getElementById("clientWarning");
                             if (clientWarningBox != null)
                             {
                                clientWarningBox.style.display = "none";
                             }
                             //contentId.style.display == "block" ? contentId.style.display = "none" : 
                          } else
                          {
                             console.log("resultSUBMIT");
                             var contentId = document.getElementById("clientInfoErr");
                             contentId.style.display = "none";
                             var clientInfoBox = document.getElementById("clientInfoBoxFooter");
                             clientInfoBox.style.height = "55px";
                             console.log("Couldnt find this: " + result);
                             console.log("Save this name: " + varToCheck);
                             //console.log("sernam this name: " + '<?php echo $serviceName; ?>');
                             document.getElementById('updateAccount').submit();
                          }
                       }
                    });
         } else
         {
            document.getElementById('updateAccount').submit();
         }
      } else
      {
         // document.getElementById('updateAccount').submit();
      }

      if (controller == "collectionDefault")
      {
         $.ajax(
                 {
                    url: "../php/ajaxHandler.php",
                    data: {collectionName: varToCheck},
                    type: "POST",
                    success: function (result)
                    {
                       var res = result.split(";");
                       //console.log("- " + res.length);
                       var select = document.getElementById("newDefault");
                       select.options.length = 0;
                       for (var i = 0; i < (res.length - 1); i++)
                       {
                          var opt = res[i];
                          var el = document.createElement("option");
                          el.textContent = opt;
                          el.value = opt;
                          select.appendChild(el);
                       }
                    }
                 });
      }

      if (controller == "collectionPorted")
      {
         $.ajax(
                 {
                    url: "../php/ajaxHandler.php",
                    data: {portedName: varToCheck},
                    type: "POST",
                    success: function (result)
                    {
                       var res = result.split(";");
                       console.log("- " + res.length);
                       var select = document.getElementById("newPorted");
                       select.options.length = 0;
                       for (var i = 0; i < (res.length - 1); i++)
                       {
                          var opt = res[i];
                          var el = document.createElement("option");
                          el.textContent = opt;
                          el.value = opt;
                          select.appendChild(el);
                       }
                    }
                 });
      }

      if (controller == "country")
      {
         $.ajax(
                 {
                    url: "../php/ajaxHandler.php",
                    data: {countryName: varToCheck},
                    type: "POST",
                    success: function (result)
                    {
                       var res = result.split(";");
                       console.log("- " + res.length);
                       var select = document.getElementById("newNet");
                       select.options.length = 0;
                       for (var i = 0; i < (res.length - 1); i++)
                       {
                          var opt = res[i];
                          var el = document.createElement("option");
                          el.textContent = opt;
                          el.value = opt;
                          select.appendChild(el);
                       }
                    }
                 });
      }
   }
</script>

<script type="text/javascript">
   /*$(function() {
    $('#routesTable').dataTable({
    "bPaginate": true,
    "bLengthChange": false,
    "bFilter": false,
    "bSort": true,
    "bInfo": true,
    "bAutoWidth": false
    });
    });*/
</script>
<script type="text/javascript">
   $(window).load(function () {
      $(".loader").fadeOut("slow");
      $(".loaderIcon").fadeOut("slow");
   })
</script>
<script type="text/javascript">
   function reloadOnSelect(name)
   {
      encodeURI(name);
      $(".loader").fadeIn("slow");
      $(".loaderIcon").fadeIn("slow");
      window.location = "viewserviceAdmin.php?client=" + encodeURIComponent(name) + "&tog1=in";
   }
</script>

<script type='text/javascript'>
   /*$(window).load(function() 
    {
    $(document).scrollTop(<?php echo $scrPos; ?>);
    });*/
</script>

<script type="text/javascript">
   function check(what)
   {
      if (what == 'clientInfo')
      {

      }
   }

   function toggleContent(what)
   {
      if (what == 'addRoute')
      {
         var contentId = document.getElementById("routeContent");
         contentId.style.display == "block" ? contentId.style.display = "none" :
                 contentId.style.display = "block";
      }
      if (what == 'addUser')
      {
         var contentId = document.getElementById("userContent");
         contentId.style.display == "block" ? contentId.style.display = "none" :
                 contentId.style.display = "block";
      }
   }
</script>

<script language=Javascript>
   function isNumberKey(evt)
   {
      var charCode = (evt.which) ? evt.which : evt.keyCode;
      if (charCode != 46 && charCode > 31
              && (charCode < 48 || charCode > 57))
         return false;

      return true;
   }
</script>
<!-- Sparkline -->


<!-- Template Footer -->