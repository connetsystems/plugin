<?php
   /*************************
    * SSM = Service Select Module
    *
    * This module can be used in a client or admin context, the object detects permissions within the session and limits results appropriately.
    */
   if (!class_exists('PermissionsHelper', false)) {
      die('Denied');
   }

   //first check if we can inherit the service object set in the parent page that loads this module
   if(!isset($service_obj->service_id) || $service_obj->service_id < 1)
   {
      if (isset($_GET['service_id']))
      {
         $service_select_module_service_id = $_GET['service_id'];

      }
      else
      {
         $service_select_module_service_id = LoginHelper::getCurrentServiceId();
      }

      //get the service object
      $service_obj = new Service($ssm_service_id);
   }

   if(!isset($service_obj->service_id) || $service_obj->service_id < 1)
   {
      $ssm_service_name = "Please select a service...";
   }
   else
   {
      $ssm_service_name = "Account: <strong>".htmlentities($service_obj->account_name). "</strong> - Service: <strong>" .htmlentities($service_obj->service_name). "</strong> (<i>".htmlentities($service_obj->service_id)."</i>)";
   }

   $permissions = PermissionsHelper::getAllPermissions();
?>
<?php if(count($permissions) == 1) { ?>
   <label>Viewing Service:</label>
   <div style="margin-bottom:10px; text-align: left; padding:5px; font-size:15px">
      <p class="no-margin pull-left"><i class="fa fa-flag"></i> <span id="ssmSelectedServiceName"><?php echo $ssm_service_name; ?></span></p>
   </div>
<?php } else { ?>
   <label>Select Service:</label>
   <button id="btnSSMServiceSelect" class="btn btn-default btn-sm btn-block" style="margin-bottom:10px; text-align: left; padding:5px; font-size:15px">
      <p class="no-margin pull-left"><i class="fa fa-flag"></i> <span id="ssmSelectedServiceName"><?php echo $ssm_service_name; ?></span></p>
      <p class="no-margin pull-right"><b class="caret"></b>&nbsp;</p>
   </button>
<?php } ?>


<!-- MODALS HERE -->
<!-- THE SELECT ACOUNT MODAL, LOADED VIA AJAX-->
<div id="modalSSMSelectService" class="modal fade" tabindex="-1" role="dialog" data-backdrop="static">
   <div class="modal-dialog">
      <div class="modal-content">
         <div class="modal-header" >
            <button type="button" class="close" data-dismiss="modal" aria-label="Close" id="btnFragModalCloseTop"><span aria-hidden="true">&times;</span></button>
            <h4 class="modal-title" id="modalSSMTitle">Select A Service</h4>
         </div>
         <div class="modal-body">
            <div class="alert alert-danger" style="display:none;" id="modalSSMAlert">
               <h4><i class="icon fa fa-ban"></i> Error!</h4>
               <p id="modalSSMAlertText">Unfortunately there was an error, please try again.</p>
            </div>
            <h5>Please select a service. You can use the input below to filter the service list.</h5>
            <div id="modalSSMPageHolder" style="height:400px !important;">
               <!-- THE AJAX LOADED SERVICE SELECTOR IS PLACE IN HERE -->
            </div>
         </div>
         <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal" id="modalSSMCloseBottom">Close</button>
         </div>
      </div><!-- /.modal-content -->
   </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<script>

   $(function () {

      /**
       * This button shows the account selection modal
       */
      $('#btnSSMServiceSelect').on("click", function (e)
      {
         e.preventDefault();

         hideModalServiceSelectError();

         $('#modalSSMPageHolder').html('<span class="glyphicon glyphicon-refresh glyphicon-spin"></span> Loading service list...');

         $.post("<?php echo WebAppHelper::getBaseServerURL(); ?>/pages/modules/module_service_select.php", {parent_callback_function: 'setSelectedService'}).done(function (data) {
            $('#modalSSMPageHolder').html(data);
         }).fail(function (data) {
            showModalServiceSelectError("A server error occurred and we could not load the service list, please contact technical support.");
         });

         $('#modalSSMSelectService').modal('show');
      });
   });

   function setSelectedService(serviceId, serviceName)
   {
      console.log(serviceId + " NAME: " + serviceName);

      var newURL = setServiceIdInURL(serviceId);
      console.log(newURL);
      window.location = newURL;
   }

   //shows the error in the MODAL with a custom error message
   function showModalServiceSelectError(message)
   {
      $("#modalSSMAlertText").html(message);
      $("#modalSSMAlert").show();
   }

   //hides the modal error and resets the message
   function hideModalServiceSelectError()
   {
      $("#modalSSMAlertText").html('');
      $("#modalSSMAlert").hide();
   }

   /**
    * Grabs the URL, checks for service_id, and sets or replaces it depending on it's existence
    * @param serviceId
    * @returns {string}
    */
   function setServiceIdInURL(serviceId)
   {
      var url = document.URL
      var newAdditionalURL = "";

      //split out the hashes
      var tempArrayHash = url.split("#");
      var noHashURL = tempArrayHash[0];
      var hash = '#'+tempArrayHash[1];

      //now split out the get variables
      var noHashURLArray = noHashURL.split("?");
      var baseURL = noHashURLArray[0];
      var getVars = noHashURLArray[1];
      var temp = "";

      if(getVars)
      {
         var getVarsArray = getVars.split("&");

         for(var i in getVarsArray)
         {
            if(getVarsArray[i].indexOf("service_id") == -1)
            {
               newAdditionalURL += temp+getVarsArray[i];
               temp = "&";
            }
         }
      }

      var serviceIdGetVar = temp+"service_id="+serviceId;
      var fixedURL = baseURL+"?"+newAdditionalURL+serviceIdGetVar+hash;

      return fixedURL;
   }

</script>