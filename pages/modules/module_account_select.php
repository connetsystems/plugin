<?php
   require_once("../../php/allInclusive.php");
   require_once("../../PluginAPI/PluginAPIAutoload.php");

   //-------------------------------------------------------------
   // SELECT ACCOUNTS MODULE
   //-------------------------------------------------------------

   //if this variable is set, then selecting a particular user will attempt to trigger the
   // function name defined here wit hte user_id sent to that function within the window
   if(isset($_POST['parent_callback_function']))
   {
      $parent_callback_function = $_POST['parent_callback_function'];
   }
   else
   {
      $parent_callback_function = "";
   }
?>


<!-- THE MAIN PARENT -->
<div id="moduleHolder">

   <!-- SEARCH BAR AND ADD BUTTON -->
   <div class="row">
      <div class="col-lg-12">
         <!-- SEARCH BAR -->
         <div class="input-group">
            <input type="text" class="form-control" id="inputSearchAccount" placeholder="Type to search accounts..." aria-label="">
            <div class="input-group-btn">
               <button type="button" class="btn btn-default dropdown-toggle" id="btnSearchType" search_type="all" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                  <span id="btnSearchTypeString">Search All</span> <span class="caret"></span>
               </button>
               <ul class="dropdown-menu dropdown-menu-right">
                  <li><a class="search-type" search_type="all" search_type_string="Search All" href="#">Search All</a></li>
                  <li><a class="search-type" search_type="account_id" search_type_string="Account ID" href="#">Account ID</a></li>
                  <li><a class="search-type" search_type="account_name" search_type_string="Account Name" href="#">Account Name</a></li>
               </ul>
            </div>
         </div>
      </div>
   </div>

   <hr/>

   <div class="row">
      <div class="col-lg-3">
         <p style="margin-left:10px;"><strong>Account ID</strong></p>
      </div>
      <div class="col-lg-9">
         <p style="margin-left:10px;"><strong>Account Name</strong></p>
      </div>
   </div>

   <!-- THE LIST OF ACCOUNTS ON THIS PAGE -->
   <div id="listHolderParent" style="overflow-y:auto;">
      <div class="list-group" id="listAccountsHolder">
         <a href="#" class="list-group-item" style="text-align:center;" id="listItemLoading">
            <p class="list-group-item-text"><span class="glyphicon glyphicon-refresh glyphicon-spin"></span> Loading accounts...</p>
         </a>
      </div>
   </div>

</div>


<script type="text/javascript">

   var page = 1;
   var limit = 25;
   var searchType = "all";
   var searchTerm = "";
   var loadingListItem = null;
   var isLoading = false;
   var noMoreAccounts = false;

   var parentCallbackFunctionString = "<?php echo $parent_callback_function; ?>";

   $(document).ready(function (e)
   {
      //perform initial setup for this module sitting inside it's parent
      var moduleParentHeight = $("#moduleHolder").parent().height();
      $("#moduleHolder").height(moduleParentHeight);
      $("#listHolderParent").height(moduleParentHeight - 100);
      $("#listHolderParent").scroll(0);

      loadingListItem = $('#listItemLoading');

      //load the initial accounts
      loadAccountListViaAjax();

      //hide the loading popup if it is showing
      $(".loader").fadeOut("fast");
      $(".loaderIcon").fadeOut("fast");

      /***************************************
       * Listener for changing the search type
       ***************************************/
      $(".search-type").on("click", function (e)
      {
         e.preventDefault();
         setSearchType($(this).attr('search_type'), $(this).attr('search_type_string'));
      });

      /***************************************
       * Listener for changing the search type
       ***************************************/
      $("#inputSearchAccount").keyup(function (e)
      {
         e.preventDefault();
         searchTerm = $(this).val();
         showListLoading();
         keyDelay(function() { runSearchForAccount(); }, 400);
      });

      /***************************************
       * Listener for changing the search type
       ***************************************/
      $("#inputSearchAccount").mouseup(function (e)
      {
         e.preventDefault();
         searchTerm = $(this).val();
         showListLoading();
         keyDelay(function() { runSearchForAccount(); }, 400);
      });

      /*************************************
       * For loading more accounts
       * This listener checks scroll position and starts loading more items
       *************************************/
      $('#listHolderParent').scroll(function()
      {
         if(!noMoreAccounts && !isLoading && ($(this).scrollTop()) >= $(this).height() * page )
         {
            isLoading = true;
            page++;
            console.log("Loading page : " + page);

            loadAccountListViaAjax();
         }
      });

   });

   //this function grabs the search variables and refreshes the page to run a search
   function runSearchForAccount()
   {
      noMoreAccounts = false;

      //remove the loading element and clear all the accounts, then show loading
      loadingListItem.detach();
      $('#listAccountsHolder').empty();
      loadingListItem.appendTo('#listAccountsHolder');

      //reset the page number as we are now doing a search
      page = 1;

      loadAccountListViaAjax();
   }

   function setSearchType(type, typeString)
   {
      var doSearch = false;
      if(type != searchType && searchTerm != "")
      {
         doSearch = true;
      }
      searchType = type;
      $("#btnSearchTypeString").html(typeString);

      if(doSearch)
      {
         runSearchForAccount();
      }
   }




   /*************************************
    * This function uses the current properties of the page,
    * such as "page" number, search terms, and loads the account list dyamically.
    *
    *************************************/
   function loadAccountListViaAjax()
   {
      //run our ajaxController to save the details
      $.post("<?php echo WebAppHelper::getBaseServerURL(); ?>/php/ajaxAdminGetAccountList.php", {page: page, limit: limit, search_type:searchType, search_term:searchTerm}).done(function (data) {
         try
         {
            var json = $.parseJSON(data);

            if (json.success)
            {
               if(json.is_search == true && json.page == 0 && json.accounts.length == 0)
               {
                  noMoreAccounts = true;
                  showNoResults();
               }
               else if(json.accounts.length == 0)
               {
                  noMoreAccounts = true;
                  showNoMoreAccounts();
               }
               else if(json.accounts.length < limit)
               {
                  addAccountsToList(json.accounts);
                  showNoMoreAccounts();
               }
               else
               {

                  addAccountsToList(json.accounts);
               }
            }
            else
            {
               showLoadingFailure();
            }

         }
         catch (error)
         {
            console.log("Could not load list: " + error.message);
            showLoadingFailure();
         }

         isLoading = false;

      }).fail(function (data) {
         showLoadingFailure();
      });
   }

   /*************************************
    * This function takes the dynamically
    * loaded json noMoreAccountss and populates the list
    *
    *************************************/
   function addAccountsToList(accounts)
   {
      //remove the loading element and add the accounts
      loadingListItem.detach();

      clearListMessages();

      for(var i = 0; i < accounts.length; ++ i)
      {
         var account = accounts[i];

         var accountName = addslashes(account.account_name);

         //could do with an actual tempalting language here, but for the sake of time, we have not
         $('#listAccountsHolder').append('<a href="#" onclick="triggerSelectAccount(' + account.account_id + ', \'' + accountName + '\')" class="list-group-item"'
             + ' data-toggle="tooltip" data-placement="bottom" title="Click to select this account.">'
             + '<div class="row">'
             + '<div class="col-lg-3">'
             + '<h3 class="list-group-item-heading"><strong>' + account.account_id + '</strong></h3>'
             + '</div>'
             + '<div class="col-lg-9">'
             + '<h5 class="list-group-item-heading">Account: <strong>' + account.account_name + '</strong></h5>'
             + '</div>'
             + '</div>'
             + '</a>');

      }

      loadingListItem.appendTo('#listAccountsHolder');

      //refresh tooltips
      $('[data-toggle="tooltip"]').tooltip();
   }

   function clearListMessages()
   {
      $('#listItemNoResults').remove();
      $('#listItemEnd').remove();
      $('#listItemError').remove();
   }

   function showNoResults()
   {
      clearListMessages();

      //remove the loading element and show no results
      loadingListItem.detach();
      $('#listAccountsHolder').append('<div id="listItemNoResults" class="list-group-item text-warning" style="text-align:center;"> No results match your query.</div>');
   }

   function showNoMoreAccounts()
   {
      clearListMessages();

      //remove the loading element and show no results
      loadingListItem.detach();
      $('#listAccountsHolder').append('<div id="listItemEnd" class="list-group-item" style="text-align:center;"> End of accounts list.</div>');
   }

   function showLoadingFailure()
   {
      clearListMessages();

      //remove the loading element and show no results
      loadingListItem.detach();
      $('#listAccountsHolder').append('<div id="listItemError" class="list-group-item text-danger" style="text-align:center;"> Could not load anymore results, please check your internet connection.</div>');
   }

   function triggerSelectAccount(accountId, accountName)
   {
      if(parentCallbackFunctionString != "") {
         window[parentCallbackFunctionString](accountId, accountName);
      }
   }

   //SOURCE: http://stackoverflow.com/questions/24816/escaping-html-strings-with-jquery

   function addslashes( string ) {
      return (string + '').replace(/[\\"']/g, '\\$&').replace(/\u0000/g, '\\0');
   }

   function showListLoading()
   {
      //remove the loading element and clear all the services, then show loading
      loadingListItem.detach();
      $('#listAccountsHolder').empty();
      loadingListItem.appendTo('#listAccountsHolder');
   }

   var keyDelay = (function(){
      var timer = 0;
      return function(callback, ms){
         clearTimeout (timer);
         timer = setTimeout(callback, ms);
      };
   })();
</script>


