<?php
   require_once("../../php/allInclusive.php");
   require_once("../../PluginAPI/PluginAPIAutoload.php");

   //-------------------------------------------------------------
   // SELECT USERS
   //-------------------------------------------------------------
   //get all the user for this page

   //if this variable is set, then selecting a particulr user will attempt to trigger the
   // function name defined here wit hte user_id sent to that function within the window
   if(isset($_POST['parent_callback_function']))
   {
      $parent_callback_function = $_POST['parent_callback_function'];
   }
   else
   {
      $parent_callback_function = "";
   }
?>

<!-- THE MAIN PARENT -->
<div id="moduleHolder">
   <!-- SEARCH BAR AND ADD BUTTON -->
   <div class="row">
      <div class="col-lg-12">
         <!-- SEARCH BAR -->
         <div class="input-group">
            <input type="text" class="form-control" id="inputSearchUser" placeholder="Type to filter users..." aria-label="">
            <div class="input-group-btn">
               <button type="button" class="btn btn-default dropdown-toggle" id="btnSearchType" search_type="all" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                  <span id="btnSearchTypeString">Search All</span> <span class="caret"></span>
               </button>
               <ul class="dropdown-menu dropdown-menu-right">
                  <li><a class="search-type" search_type="all" search_type_string="Search All" href="#">Search All</a></li>
                  <li><a class="search-type" search_type="user_id" search_type_string="User ID" href="#">User ID</a></li>
                  <li><a class="search-type" search_type="user_username" search_type_string="Username" href="#">Username</a></li>
               </ul>
            </div>
         </div>
      </div>
   </div>
   <hr/>
   <div class="row">
      <div class="col-lg-2">
         <p style="margin-left:10px;"><strong>User ID</strong></p>
      </div>
      <div class="col-lg-6">
         <p style="margin-left:10px;"><strong>Username</strong></p>
      </div>
      <div class="col-lg-4" style="text-align:right;">
         <p style="margin-right:10px;"><strong>User Details</strong></p>
      </div>
   </div>


   <!-- THE LIST OF USERS ON THIS PAGE -->
   <div id="listHolderParent" style="overflow-y:auto;">
      <div class="list-group" id="listUsersHolder">
         <a href="#" class="list-group-item" style="text-align:center;" id="listItemLoading">
            <p class="list-group-item-text"><span class="glyphicon glyphicon-refresh glyphicon-spin"></span> Loading users...</p>
         </a>
      </div>
   </div>

</div>

<script type="text/javascript">

   var page = 1;
   var limit = 25;
   var searchType = "all";
   var searchTerm = "";
   var loadingListItem = null;
   var isLoading = false;
   var noMoreUsers = false;

   var parentCallbackFunctionString = "<?php echo $parent_callback_function; ?>";

   $(document).ready(function (e)
   {
      //perform initial setup for this module sitting inside it's parent
      var moduleParentHeight = $("#moduleHolder").parent().height();
      $("#moduleHolder").height(moduleParentHeight);
      $("#listHolderParent").height(moduleParentHeight - 100);
      $("#listHolderParent").scroll(0);

      loadingListItem = $('#listItemLoading');

      //load the initial users
      loadUserListViaAjax();

      //hide the loading popup if it is showing
      $(".loader").fadeOut("fast");
      $(".loaderIcon").fadeOut("fast");

      /***************************************
       * Listener for changing the search type
       ***************************************/
      $(".search-type").on("click", function (e)
      {
         e.preventDefault();
         setSearchType($(this).attr('search_type'), $(this).attr('search_type_string'));
      });

      /***************************************
       * Listener for changing the search type
       ***************************************/
      $("#inputSearchUser").keyup(function (e)
      {
         e.preventDefault();
         searchTerm = $(this).val();
         showListLoading();
         keyDelay(function() { runSearchForUser(); }, 400);
      });

      /***************************************
       * Listener for changing the search type
       ***************************************/
      $("#inputSearchUser").mouseup(function (e)
      {
         e.preventDefault();
         searchTerm = $(this).val();
         showListLoading();
         keyDelay(function() { runSearchForUser(); }, 400);
      });

      /*************************************
       * For loading more services
       * This listener checks scroll position and starts loading more items
       *************************************/
      $('#listHolderParent').scroll(function()
      {
         if(!noMoreUsers && !isLoading && ($(this).scrollTop()) >= $(this).height() * page )
         {
            isLoading = true;
            page++;
            console.log("Loading page : " + page);

            loadUserListViaAjax();
         }
      });

   });


   //this function grabs the search variables and refreshes the page to run a search
   function runSearchForUser()
   {
      noMoreUsers = false;

      //remove the loading element and clear all the users, then show loading
      loadingListItem.detach();
      $('#listUsersHolder').empty();
      loadingListItem.appendTo('#listUsersHolder');

      //reset the page number as we are now doing a search
      page = 1;

      loadUserListViaAjax();
   }

   function setSearchType(type, typeString)
   {
      var doSearch = false;
      if(type != searchType && searchTerm != "")
      {
         doSearch = true;
      }
      searchType = type;
      $("#btnSearchTypeString").html(typeString);

      if(doSearch)
      {
         runSearchForUser();
      }
   }




   /*************************************
    * This function uses the current properties of the page,
    * such as "page" number, search terms, and loads the User list dyamically.
    *
    *************************************/
   function loadUserListViaAjax()
   {
      //run our ajaxController to save the details
      $.post("<?php echo WebAppHelper::getBaseServerURL(); ?>/php/ajaxAdminGetUserList.php", {page: page, limit: limit, search_type:searchType, search_term:searchTerm}).done(function (data) {
         try
         {
            var json = $.parseJSON(data);

            if (json.success)
            {
               if(json.is_search == true && json.page == 0 && json.users.length == 0)
               {
                  noMoreUsers = true;
                  showNoResults();
               }
               else if(json.users.length == 0)
               {
                  noMoreUsers = true;
                  showNoMoreUsers();
               }
               else if(json.users.length < limit)
               {
                  addUsersToList(json.users);
                  showNoMoreUsers();
               }
               else
               {

                  addUsersToList(json.users);
               }
            }
            else
            {
               showLoadingFailure();
            }

         }
         catch (err)
         {
            showLoadingFailure();
            console.log("Could not load list: " + err.message);

         }

         isLoading = false;

      }).fail(function (data) {
         showLoadingFailure();
      });
   }

   /*************************************
    * This function takes the dynamically
    * loaded json users and populates the list
    *
    *************************************/
   function addUsersToList(users)
   {
      //remove the loading element and add the users
      loadingListItem.detach();

      clearListMessages();

      for(var i = 0; i < users.length; ++ i)
      {
         var user = users[i];

         var headerLogo = '/img/skinlogos/1/1_header.png';
         if(user.header_logo != "" && user.header_logo != null)
         {
            headerLogo = user.header_logo;
            console.log("Header logo: " + user.header_logo);
         }

         //could do with an actual tempalting language here, but for the sake of time, we have not
         $('#listUsersHolder').append('<a href="#" onclick="triggerSelectUser(' + user.user_id + ')" class="list-group-item"'
            + ' data-toggle="tooltip" data-placement="bottom" title="Click to manage this user.">'
            + '<div class="row">'
            + '<div class="col-lg-2">'
            + '<h3 class="list-group-item-heading"><strong>' + user.user_id + '</strong></h3>'
            + '</div>'
            + '<div class="col-lg-6">'
            + '<h5 class="list-group-item-heading"><strong>' + user.user_username + '</strong></h5>'
            + '</div>'
            + '<div class="col-lg-4">'
             + '<small class="list-group-item-text">Details: ' + user.firstname + '</small>'
            + '</div>'
            + '</div>'
            + '</a>');

      }

      loadingListItem.appendTo('#listUsersHolder');

      //refresh tooltips
      $('[data-toggle="tooltip"]').tooltip();
   }

   function clearListMessages()
   {
      $('#listItemNoResults').remove();
      $('#listItemEnd').remove();
      $('#listItemError').remove();
   }

   function showNoResults()
   {
      clearListMessages();

      //remove the loading element and show no results
      loadingListItem.detach();
      $('#listUsersHolder').append('<div id="listItemNoResults" class="list-group-item text-warning" style="text-align:center;"> No results match your query.</div>');
   }

   function showNoMoreUsers()
   {
      clearListMessages();

      //remove the loading element and show no results
      loadingListItem.detach();
      $('#listUsersHolder').append('<div id="listItemEnd" class="list-group-item" style="text-align:center;"> End of user list.</div>');
   }

   function showLoadingFailure()
   {
      clearListMessages();

      //remove the loading element and show no results
      loadingListItem.detach();
      $('#listUsersHolder').append('<div id="listItemError" class="list-group-item text-danger" style="text-align:center;"> Could not load anymore results, please check your internet connection.</div>');
   }

   function triggerSelectUser(userId)
   {
      if(parentCallbackFunctionString != "") {
         window[parentCallbackFunctionString](userId);
      }
   }

   function showListLoading()
   {
      //remove the loading element and clear all the services, then show loading
      loadingListItem.detach();
      $('#listUsersHolder').empty();
      loadingListItem.appendTo('#listUsersHolder');
   }

   /**
    * This delay is used to prevent the ajax call been made immediately while the user types, preventing too many requests
    */
   var keyDelay = (function(){
      var timer = 0;
      return function(callback, ms){
         clearTimeout (timer);
         timer = setTimeout(callback, ms);
      };
   })();

</script>

