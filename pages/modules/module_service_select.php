<?php
   require_once("../../php/allInclusive.php");
   require_once("../../PluginAPI/PluginAPIAutoload.php");

   //-------------------------------------------------------------
   // SELECT SERVICES MODULE
   //-------------------------------------------------------------

   //if this variable is set, then selecting a particular user will attempt to trigger the
   // function name defined here wit hte user_id sent to that function within the window
   if(isset($_POST['parent_callback_function']))
   {
      $parent_callback_function = $_POST['parent_callback_function'];
   }
   else
   {
      $parent_callback_function = "";
   }
?>


<!-- THE MAIN PARENT -->
<div id="moduleHolder">

   <!-- SEARCH BAR AND ADD BUTTON -->
   <div class="row">
      <div class="col-lg-12">
         <!-- SEARCH BAR -->
         <div class="input-group">
            <input type="text" class="form-control" id="inputSearchService" placeholder="Type to search accounts/services..." aria-label="">
            <div class="input-group-btn">
               <button type="button" class="btn btn-default dropdown-toggle" id="btnSearchType" search_type="all" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                  <span id="btnSearchTypeString">Search All</span> <span class="caret"></span>
               </button>
               <ul class="dropdown-menu dropdown-menu-right">
                  <li><a class="search-type" search_type="all" search_type_string="Search All" href="#">Search All</a></li>
                  <li><a class="search-type" search_type="service_id" search_type_string="Service ID" href="#">Service ID</a></li>
                  <li><a class="search-type" search_type="service_name" search_type_string="Service Name" href="#">Service Name</a></li>
                  <li><a class="search-type" search_type="account_name" search_type_string="Account Name" href="#">Account Name</a></li>
                  <li><a class="search-type" search_type="account_id" search_type_string="Account ID" href="#">Account ID</a></li>
               </ul>
            </div>
         </div>
      </div>
   </div>

   <hr/>

   <div class="row">
      <div class="col-lg-2">
         <p style="margin-left:10px;"><strong>ID</strong></p>
      </div>
      <div class="col-lg-10">
         <p style="margin-left:10px;"><strong>Service</strong></p>
      </div>
   </div>

   <!-- THE LIST OF SERVICES ON THIS PAGE -->
   <div id="listHolderParent" style="overflow-y:auto;">
      <div class="list-group" id="listServicesHolder">
         <a href="#" class="list-group-item" style="text-align:center;" id="listItemLoading">
            <p class="list-group-item-text"><span class="glyphicon glyphicon-refresh glyphicon-spin"></span> Loading services...</p>
         </a>
      </div>
   </div>

</div>


<script type="text/javascript">

   var page = 1;
   var limit = 25;
   var searchType = "all";
   var searchTerm = "";
   var loadingListItem = null;
   var isLoading = false;
   var noMoreServices = false;

   var parentCallbackFunctionString = "<?php echo $parent_callback_function; ?>";

   $(document).ready(function (e)
   {
      //perform initial setup for this module sitting inside it's parent
      var moduleParentHeight = $("#moduleHolder").parent().height();
      $("#moduleHolder").height(moduleParentHeight);
      $("#listHolderParent").height(moduleParentHeight - 100);
      $("#listHolderParent").scroll(0);

      loadingListItem = $('#listItemLoading');

      //load the initial services
      loadServiceListViaAjax();

      //hide the loading popup if it is showing
      $(".loader").fadeOut("fast");
      $(".loaderIcon").fadeOut("fast");

      /***************************************
       * Listener for changing the search type
       ***************************************/
      $(".search-type").on("click", function (e)
      {
         e.preventDefault();
         setSearchType($(this).attr('search_type'), $(this).attr('search_type_string'));
      });

      /***************************************
       * Listener for changing the search type
       ***************************************/
      $("#inputSearchService").keyup(function (e)
      {
         e.preventDefault();
         searchTerm = $(this).val();
         showListLoading();
         keyDelay(function() { runSearchForService(); }, 400);
      });

      /***************************************
       * Listener for changing the search type
       ***************************************/
      $("#inputSearchService").mouseup(function (e)
      {
         e.preventDefault();
         searchTerm = $(this).val();
         showListLoading();
         keyDelay(function() { runSearchForService(); }, 400);
      });

      /*************************************
       * For loading more services
       * This listener checks scroll position and starts loading more items
       *************************************/
      $('#listHolderParent').scroll(function()
      {
         if(!noMoreServices && !isLoading && ($(this).scrollTop()) >= $(this).height() * page )
         {
            isLoading = true;
            page++;
            console.log("Loading page : " + page);

            loadServiceListViaAjax();
         }
      });

   });

   //this function grabs the search variables and refreshes the page to run a search
   function runSearchForService()
   {
      noMoreServices = false;

      //remove the loading element and clear all the services, then show loading
      loadingListItem.detach();
      $('#listServicesHolder').empty();
      loadingListItem.appendTo('#listServicesHolder');

      //reset the page number as we are now doing a search
      page = 1;

      loadServiceListViaAjax();
   }

   function setSearchType(type, typeString)
   {
      var doSearch = false;
      if(type != searchType && searchTerm != "")
      {
         doSearch = true;
      }
      searchType = type;
      $("#btnSearchTypeString").html(typeString);

      if(doSearch)
      {
         runSearchForService();
      }
   }




   /*************************************
    * This function uses the current properties of the page,
    * such as "page" number, search terms, and loads the service list dyamically.
    *
    *************************************/
   function loadServiceListViaAjax()
   {
      //run our ajaxController to save the details
      $.post("<?php echo WebAppHelper::getBaseServerURL(); ?>/php/ajaxGetServiceList.php", {page: page, limit: limit, search_type:searchType, search_term:searchTerm}).done(function (data) {
         try
         {
            var json = $.parseJSON(data);

            if (json.success)
            {
               if(json.is_search == true && json.page == 0 && json.services.length == 0)
               {
                  noMoreServices = true;
                  showNoResults();
               }
               else if(json.services.length == 0)
               {
                  noMoreServices = true;
                  showNoMoreServices();
               }
               else if(json.services.length < limit)
               {
                  addServicesToList(json.services);
                  showNoMoreServices();
               }
               else
               {

                  addServicesToList(json.services);
               }
            }
            else
            {
               showLoadingFailure();
            }

         }
         catch (error)
         {
            console.log("Could not load list: " + error.message);
            showLoadingFailure();
         }

         isLoading = false;

      }).fail(function (data) {
         showLoadingFailure();
      });
   }

   /*************************************
    * This function takes the dynamically
    * loaded json services and populates the list
    *
    *************************************/
   function addServicesToList(services)
   {
      //remove the loading element and add the services
      loadingListItem.detach();

      clearListMessages();

      for(var i = 0; i < services.length; ++ i)
      {
         var service = services[i];

         var headerLogo = '/img/skinlogos/1/1_header.png';
         if(service.header_logo != "" && service.header_logo != null)
         {
            headerLogo = service.header_logo;
         }

         var serviceName = addslashes(service.service_name);

         //could do with an actual tempalting language here, but for the sake of time, we have not
         $('#listServicesHolder').append('<a href="#" onclick="triggerSelectService(' + service.service_id + ', \'' + serviceName + '\')" class="list-group-item"'
             + ' data-toggle="tooltip" data-placement="bottom" title="Click to manage this service.">'
             + '<div class="row">'
             + '<div class="col-lg-2">'
             + '<h3 class="list-group-item-heading"><strong>' + service.service_id + '</strong></h3>'
             + '</div>'
             + '<div class="col-lg-10">'
             + '<h5 class="list-group-item-heading">Service: <strong>' + service.service_name + '</strong></h5>'
             + '<p class="list-group-item-text">Account: ' + service.account_name + '</p>'
             + '</div>'
             + '</div>'
             + '</a>');

      }

      loadingListItem.appendTo('#listServicesHolder');

      //refresh tooltips
      $('[data-toggle="tooltip"]').tooltip();
   }

   function clearListMessages()
   {
      $('#listItemNoResults').remove();
      $('#listItemEnd').remove();
      $('#listItemError').remove();
   }

   function showNoResults()
   {
      clearListMessages();

      //remove the loading element and show no results
      loadingListItem.detach();
      $('#listServicesHolder').append('<div id="listItemNoResults" class="list-group-item text-warning" style="text-align:center;"> No results match your query.</div>');
   }

   function showNoMoreServices()
   {
      clearListMessages();

      //remove the loading element and show no results
      loadingListItem.detach();
      $('#listServicesHolder').append('<div id="listItemEnd" class="list-group-item" style="text-align:center;"> End of service list.</div>');
   }

   function showLoadingFailure()
   {
      clearListMessages();

      //remove the loading element and show no results
      loadingListItem.detach();
      $('#listServicesHolder').append('<div id="listItemError" class="list-group-item text-danger" style="text-align:center;"> Could not load anymore results, please check your internet connection.</div>');
   }

   function triggerSelectService(serviceId, serviceName)
   {
      if(parentCallbackFunctionString != "") {
         window[parentCallbackFunctionString](serviceId, serviceName);
      }
   }

   //SOURCE: http://stackoverflow.com/questions/24816/escaping-html-strings-with-jquery

   function addslashes( string ) {
      return (string + '').replace(/[\\"']/g, '\\$&').replace(/\u0000/g, '\\0');
   }

   /**
    * This delay is used to prevent the ajax call been made immediately while the user types, preventing too many requests
    */
   var keyDelay = (function(){
      var timer = 0;
      return function(callback, ms){
         clearTimeout (timer);
         timer = setTimeout(callback, ms);
      };
   })();

   function showListLoading()
   {
      //remove the loading element and clear all the services, then show loading
      loadingListItem.detach();
      $('#listServicesHolder').empty();
      loadingListItem.appendTo('#listServicesHolder');
   }

</script>

