<!-- MAIN NAVE SECTION OF THE SERVICE MANAGEMENT AREA -->

<div class="row">
   <div class="col-lg-12">
      <ul class="nav nav-tabs" style="margin-bottom:20px;">
         <li role="presentation" <?php echo ($nav_section == 'accounts' ? "class=\"active\"" : ""); ?>>
            <a href="<?php echo WebAppHelper::getBaseServerURL(); ?>/pages/manage_account_select.php">Manage Accounts</a>
         </li>
         <li role="presentation" <?php echo ($nav_section == 'services' ? "class=\"active\"" : ""); ?>>
            <a href="<?php echo WebAppHelper::getBaseServerURL(); ?>/pages/manage_service_select.php">Manage Services</a>
         </li>
         <li role="presentation" <?php echo ($nav_section == 'users' ? "class=\"active\"" : ""); ?>>
            <a href="<?php echo WebAppHelper::getBaseServerURL(); ?>/pages/manage_user_select.php">Manage Users</a>
         </li>
      </ul>
   </div>
</div>