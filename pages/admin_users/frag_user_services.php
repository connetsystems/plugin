<?php

   /**
    * This script is called by the manage users page, and runs certain ajax based tasks
    * @author - Doug Jenkinson
    */
   require_once("../../php/allInclusive.php");
   require_once("../../PluginAPI/PluginAPIAutoload.php");

   session_start();

   if(isset($_POST['user_id']))
   {
      $user_id = $_POST['user_id'];
   }
   else if(isset($_GET['user_id']))
   {
      $user_id = $_GET['user_id'];
   }
   else
   {
      echo '<p class="text-error">An error occurred, please refresh your browser.</p>';
      die();
   }

   //get the user object
   $user_obj = new User($user_id);
?>

   <div class="row">
      <div class="col-lg-10">
         <p class="lead">You can manage the service access of this user.</p>
         <p><strong>Total services: <span id="textTotalServiceCount"><?php echo count($user_obj->user_services); ?></span></strong></p>
      </div>
      <div class="col-lg-2">
         <button id="btnAddServicePermission" class="btn btn-primary btn-sm btn-block"><i class="fa fa-plus"></i> Add Service Permission</button>
      </div>
   </div>
   <?php foreach($user_obj->user_services as $user_service) {?>
      <div class="box box-solid">
         <div class="box-body">
            <div class="row">
               <div class="col-lg-6">
                  <small>Service Name: </small>
                  <h4 style="margin-top:0; margin-bottom:0;"><strong><?php echo htmlentities($user_service['service_name']); ?> (<?php echo $user_service['service_id']; ?>)</strong></h4>
                  <small>Account: <?php echo htmlentities($user_service['account_name']); ?></small>
               </div>
               <div class="col-lg-2">
                  <small>Access Rights: </small>
                  <h5 style="margin-top:0;">
                     <strong>All <small>(<?php echo htmlentities($user_service['permission_type']); ?>)</small></strong>
                     <a href="#" data-toggle="tooltip" data-placement="bottom" title="Plugin currently treats any access right as the ability to access a service. This will change in the future.">[?]</a>
                  </h5>
               </div>
               <div class="col-lg-2">
                  <small>Permission Created: </small>
                  <h5 style="margin-top:0;">
                     <strong><?php echo date('d-M-Y - H:i:s', strtotime($user_service['permission_created'])); ?></strong>
                  </h5>
               </div>
               <div class="col-lg-2">
                  <a href="manage_service.php?service_id=<?php echo $user_service['service_id']; ?>" class="btn btn-xs btn-primary btn-block btn-manage-service"><i class="fa fa-edit"></i> Manage Service</a>
                  <button class="btn btn-xs btn-danger btn-block btn-remove-user-from-service" connet-user-service-id="<?php echo $user_service['service_id']; ?>"><i class="fa fa-minus-circle"></i> Remove Service Permission</button>
               </div>
            </div>
         </div>
      </div>
   <?php } ?>

   </div>


   <!-- MODALS HERE -->
   <!-- THE SELECT USER MODAL, LOADED VIA AJAX-->
   <div id="modalFragSelectService" class="modal fade" tabindex="-1" role="dialog" data-backdrop="static">
      <div class="modal-dialog">
         <div class="modal-content">
            <div class="modal-header" >
               <button type="button" class="close" data-dismiss="modal" aria-label="Close" id="btnFragModalCloseTop"><span aria-hidden="true">&times;</span></button>
               <h4 class="modal-title" id="modalFragSelectServiceTitle">Add Service Permission</h4>
            </div>
            <div class="modal-body">
               <div class="alert alert-danger" style="display:none;" id="modalFragSelectServiceAlert">
                  <h4><i class="icon fa fa-ban"></i> Error!</h4>
                  <p id="modalFragSelectServiceAlertText">Unfortunately there was an error, please try again.</p>
               </div>
               <h5>Please select the service you wish to assign to this user.</h5>
               <div id="modalFragSelectServicePageHolder" style="height:400px !important;">
                  <!-- THE AJAX LOADED SERVICE SELECTOR IS PLACE IN HERE -->
               </div>
            </div>
            <div class="modal-footer">
               <button type="button" class="btn btn-default" data-dismiss="modal" id="modalFragSelectServiceCloseBottom">Close</button>
            </div>
         </div><!-- /.modal-content -->
      </div><!-- /.modal-dialog -->
   </div><!-- /.modal -->

<script type="text/javascript">
   var fragUserId = <?php echo (isset($user_id) ? $user_id : '-1') ?>;

   $(document).ready(function (e)
   {
      //enable the tooltips, as bootstrap standard defines them as opt in
      $('[data-toggle="tooltip"]').tooltip();

      /**
       * This button shows the add new service permission modal
       */
      $('#btnAddServicePermission').on("click", function (e)
      {
         e.preventDefault();

         hideFragModalError();

         $('#modalFragSelectServicePageHolder').html('<span class="glyphicon glyphicon-refresh glyphicon-spin"></span> Loading service list...');

         $.post("<?php echo WebAppHelper::getBaseServerURL(); ?>/pages/modules/module_service_select.php", {parent_callback_function: 'addServicePermission'}).done(function (data) {
            unlockFragModalBusy();
            $('#modalFragSelectServicePageHolder').html(data);
         }).fail(function (data) {
            unlockFragModalBusy();
            showModalError("A server error occurred and we could not load the service list, please contact technical support.");
         });

         $('#modalFragSelectService').modal('show');
      });


      /**
       * This button allows the user to remove a service permission from a particular service user.
       */
      $('.btn-remove-user-from-service').on("click", function (e)
      {
         e.preventDefault();

         if(confirm("Are you sure you want to revoke this user's access rights to this service?"))
         {
            var serviceId = $(this).attr('connet-user-service-id');

            var thisButton = $(this);
            var thisButtonHTML = $(this).html();
            $(this).attr("disabled", true);
            $(this).html('<span class="glyphicon glyphicon-refresh glyphicon-spin"></span> Saving password...');

            $.post("../php/ajaxManageUserHandler.php", {
               task: 'remove_service_permission',
               userId: fragUserId,
               serviceId: serviceId,
            }).done(function (data) {
               var json = $.parseJSON(data);
               if (json.success) {
                  $(thisButton).html("Reloading...");
                  location.reload();
               }
               else {
                  $(thisButton).removeAttr("disabled");
                  $(thisButton).html(thisButtonHTML);
                  alert("A server error occurred and we could not save the data, please refresh your page or contact technical support.");
               }
            }).fail(function (data) {
               $(thisButton).removeAttr("disabled");
               $(thisButton).html(thisButtonHTML);
               alert("A server error occurred and we could not save the data, please refresh your page or contact technical support.");
            });
         }
      });

   });


   function addServicePermission(serviceId, serviceName)
   {
      hideFragModalError();

      if(confirm("Are you sure you want to grant this user access rights to this service?"))
      {
         lockFragModalBusy("Adding permission...");
         $.post("<?php echo WebAppHelper::getBaseServerURL(); ?>/php/ajaxManageUserHandler.php",
             {
                task: 'add_service_permission',
                userId: fragUserId,
                serviceId:serviceId

             }).done(function (data) {
            var json = $.parseJSON(data);
            if (json.success)
            {
               lockFragModalBusy("Refreshing...");
               location.reload();
            }
            else
            {

               unlockFragModalBusy();
               if(json.reason == 'permission_exists')
               {
                  showFragModalError("This user already has permission to access this service.");
               }
               else
               {
                  showFragModalError("A server error occurred and we could not save the data, please refresh your page or contact technical support.");
               }

            }
         }).fail(function (data) {
            unlockFragModalBusy();
            showFragModalError("A server error occurred and we could not save the data, please refresh your page or contact technical support.");
         });
      }
   }


   /*****************************************
    * HELPERS
    *****************************************/

   //this locks the modal so the user can't do anything, useful when runing ajax calls off of it
   var prevButtonContent = "";
   function lockFragModalBusy(busyMessage)
   {
      prevButtonContent = $('#btnFragModalSave').html();
      $('#btnFragModalCloseTop').prop("disabled", true);
      $('#btnFragModalCloseBottom').prop("disabled", true);
      $('#btnFragModalSave').prop("disabled", false);
      $('#btnFragModalSave').html('<span class="glyphicon glyphicon-refresh glyphicon-spin"></span> ' + busyMessage);
   }

   //unlocks the modal so the user can close it
   function unlockFragModalBusy()
   {
      $('#btnFragModalCloseTop').prop("disabled", false);
      $('#btnFragModalCloseBottom').prop("disabled", false);
      $('#btnFragModalSave').prop("disabled", false);
      $('#btnFragModalSave').html(prevButtonContent);
      prevButtonContent = "";
   }

   //shows the error in the MODAL with a custom error message
   function showFragModalError(message)
   {
      $("#modalFragSelectServiceAlertText").html(message);
      $("#modalFragSelectServiceAlert").show();
   }

   //hides the modal error and resets the message
   function hideFragModalError()
   {
      $("#modalFragSelectServiceAlertText").html();
      $("#modalFragSelectServiceAlert").hide();
   }

</script>


