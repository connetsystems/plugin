<?php
   /**
    * Include the header template which sets up the HTML
    *
    * Don't forget to include template_import_script before any Javascripting
    * Don't forget to include template footer.php at the bottom of the page as well
    */
   //-------------------------------------------------------------
   // Template
   //-------------------------------------------------------------
   TemplateHelper::setPageTitle('Create A New User');
   TemplateHelper::initialize();

   //-------------------------------------------------------------
   // CREATE USER
   //-------------------------------------------------------------

   $service_managers = User::getAllAdministrators();

   if(isset($_GET['service_id']))
   {
      $preselected_service = new Service($_GET['service_id']);
   }
?>

<aside class="right-side">
   <section class="content-header">
      <h1>
         Create A New User
      </h1>
   </section>

   <section class="content">

      <!-- BACK BUTTON AND SERVICE TITLE -->
      <div class="row">
         <div class="col-lg-2">
            <a class="btn btn-default btn-block btn-sm" href="<?php echo WebAppHelper::getBaseServerURL(); ?>/pages/manage_user_select.php" style="margin:0px;">
               <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span> Back to User Selection
            </a>
         </div>
         <div class="col-lg-10">

         </div>
      </div>
      <hr/>

      <!-- ACCOUNT CREATION/SELECTION -->
      <div class="row">
         <div class="col-lg-8 col-lg-offset-2">
            <!-- THE TITLE BOX FOR ACCOUTN CREATION -->
            <div class="box box-primary">
               <div class="box-header with-border">
                  <h3 class="box-title"><i class="fa fa-flag"></i> Select A Service</h3>
               </div>
               <div class="box-body">
                  <p class="lead">Please select a parent service to add this user to. A user must be associated with at least one service.</p>
                  <!-- FOR ACCOUNT SELECTION -->
                  <div id="serviceSelectContainer" style="text-align: center;">
                     <label for="serviceSelectName">Selected Service</label>
                     <h2 id="serviceSelectName"  class="<?php echo (isset($preselected_service) ? "text-success" : ""); ?>" style="margin-top:2px;">
                        <?php echo (isset($preselected_service) ? $preselected_service->service_name.' <i class="fa fa-check-circle"></i>' : "No service selected."); ?>
                     </h2>
                     <small class="text-danger" id="errorServiceInvalid" style="display:none;">The service you selected is invalid, please select another one.</small>
                     <br/>
                     <button type="button" id="btnShowServiceSelectPopup" class="btn btn-xs btn-default">Select A Service</button>
                  </div>
                  <br/><br/>
               </div>
            </div>
         </div>
      </div>


      <!-- USER CONFIGURATION -->
      <div class="row">
         <div class="col-lg-8 col-lg-offset-2">

            <!-- THE TITLE BOX FOR USER CREATION -->
            <div class="box box-primary">
               <div class="box-header with-border">
                  <h3 class="box-title"><i class="fa fa-flag"></i> New User Details</h3>
               </div>
               <div class="box-body">
                  <p class="lead">Please fill out all the relevant information about the new user in the form below.</p>
                  <div class="row">
                     <!-- USERNAME AND USER TYPE -->
                     <div class="col-lg-6">
                        <div class="form-group">
                           <label for="userName">Username</label>
                           <input type="email" class="form-control" id="userName" name="userName" placeholder="New user's username...">
                           <small class="text-danger" id="userNameError" style="display:none;">The service name cannot be blank.</small>
                        </div>
                     </div>
                     <div class="col-lg-6">
                        <div class="form-group">
                           <label for="userType">User type</label>
                           <select class="form-control" id="userType" name="userType">
                              <option value="0" selected>STANDARD [For clients]</option>
                              <option value="1">CONNET ADMINISTRATOR [Internal only]</option>
                           </select>
                        </div>
                     </div>
                  </div>
                  <div class="row">
                     <!-- FIRST NAME AND LAST NAME -->
                     <div class="col-lg-6">
                        <div class="form-group">
                           <label for="firstName">First Name</label>
                           <input type="email" class="form-control" id="firstName" name="firstName" placeholder="First name...">
                           <small class="text-danger" id="firstNameError" style="display:none;"></small>
                        </div>
                     </div>
                     <div class="col-lg-6">
                        <div class="form-group">
                           <label for="lastName">Last Name</label>
                           <input type="email" class="form-control" id="lastName" name="lastName" placeholder="Last name...">
                           <small class="text-danger" id="lastNameError" style="display:none;"></small>
                        </div>
                     </div>
                  </div>
                  <hr/>
                  <div class="row">
                     <!-- SETTING A PASSWORD -->
                     <div class="col-lg-6">
                        <div class="form-group">
                           <label for="setPassword">Password:</label>
                           <input type="password" class="form-control" maxlength="8" name="setPassword" id="setPassword" placeholder="Enter a password..." />
                           <small class="text-danger" id="setPasswordError" style="display:none;"></small>
                        </div>
                     </div>

                     <div class="col-lg-6">
                        <div class="form-group">
                           <label for="setPasswordConfirm">Confirm Password:</label>
                           <input type="password" class="form-control" maxlength="8" name="setPasswordConfirm" id="setPasswordConfirm" placeholder="Confirm the password..." />
                        </div>
                     </div>
                  </div>
                  <hr/>
                  <div class="row">
                     <div class="col-lg-12" style="text-align:center;">
                        <button type="button" id="btnCreateNewUser" class="btn btn-bg btn-primary">
                           <i class="fa fa-user-plus" style="font-size:40pt; margin-top:10px;"></i>
                           <br/>
                           <span style="font-size:20pt;">Create User</span>
                        </button>
                        <hr/>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>


      <!-- MODALS HERE -->
      <!-- THE SELECT ACOUNT MODAL, LOADED VIA AJAX-->
      <div id="modalFragSelectService" class="modal fade" tabindex="-1" role="dialog" data-backdrop="static">
         <div class="modal-dialog">
            <div class="modal-content">
               <div class="modal-header" >
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close" id="btnFragModalCloseTop"><span aria-hidden="true">&times;</span></button>
                  <h4 class="modal-title" id="modalFragSelectServiceTitle">Add Service Permission</h4>
               </div>
               <div class="modal-body">
                  <div class="alert alert-danger" style="display:none;" id="modalFragSelectServiceAlert">
                     <h4><i class="icon fa fa-ban"></i> Error!</h4>
                     <p id="modalFragSelectServiceAlertText">Unfortunately there was an error, please try again.</p>
                  </div>
                  <h5>Please select the service you wish to assign to this user.</h5>
                  <div id="modalFragSelectServicePageHolder" style="height:400px !important;">
                     <!-- THE AJAX LOADED SERVICE SELECTOR IS PLACE IN HERE -->
                  </div>
               </div>
               <div class="modal-footer">
                  <button type="button" class="btn btn-default" data-dismiss="modal" id="modalFragSelectServiceCloseBottom">Close</button>
               </div>
            </div><!-- /.modal-content -->
         </div><!-- /.modal-dialog -->
      </div><!-- /.modal -->




   </section>
</aside>


<!--  THE JAVASCRIPT IMPORT ALWAYS APPEARS BEFORE LOCAL JAVASCRIPT AT THE BOTTOM OF THE PAGE -->
<?php include("../template_import_script.php"); //must import all scripts first   ?>
<!-- END JAVASCRIPT IMPORT -->

<script type="text/javascript">

   var selectedServiceId = <?php echo (isset($preselected_service) ? $preselected_service->service_id : -1); ?>;
   var selectedServiceName = "<?php echo (isset($preselected_service) ? $preselected_service->service_name : ""); ?>";

   $(document).ready(function (e)
   {
      //enable the tooltips, as bootstrap standard defines them as opt in
      $('[data-toggle="tooltip"]').tooltip();

      /***********************************************
       * SERVICE LOGIC
       **********************************************/
      /**
       * This button shows the account selection modal
       */
      $('#btnShowServiceSelectPopup').on("click", function (e)
      {
         e.preventDefault();

         hideFragModalError();

         $('#modalFragSelectServicePageHolder').html('<span class="glyphicon glyphicon-refresh glyphicon-spin"></span> Loading service list...');

         $.post("../modules/module_service_select.php", {parent_callback_function: 'setSelectedService'}).done(function (data) {
            $('#modalFragSelectServicePageHolder').html(data);
         }).fail(function (data) {
            showModalError("A server error occurred and we could not load the service list, please contact technical support.");
         });

         $('#modalFragSelectService').modal('show');
      });

      /***********************************************
       * SERVICE CREATION LOGIC
       **********************************************/
      //this button collects the form data, validates, submits, and proceeds accordingly
      $('#btnCreateNewUser').on("click", function (e)
      {
         e.preventDefault();

         console.log("Attempting to create user.");

         var formValid = validateUserCreateForm();
         if(formValid)
         {
            var userName = $("#userName").val();
            var userIsAdmin = $("#userType").val();
            var firstName = $("#firstName").val();
            var lastName = $("#lastName").val();
            var setPassword = $("#setPassword").val();
            var setPasswordConfirm = $("#setPasswordConfirm").val();

            //send the service information to the server and catch errors appropriately
            $.post("<?php echo WebAppHelper::getBaseServerURL(); ?>/php/ajaxManageUserHandler.php", {
               task: 'create_new_user',
               serviceId: selectedServiceId,
               userName: userName,
               userIsAdmin: userIsAdmin,
               firstName: firstName,
               lastName: lastName,
               setPassword: setPassword
            }).done(function (data) {
               var json = $.parseJSON(data);
               if (json.success)
               {
                  window.location = "<?php echo WebAppHelper::getBaseServerURL(); ?>/pages/manage_user.php?user_id="+json.user_id;
               }
               else
               {
                  if(json.reason == "validation_failed")
                  {
                     alert("There were errors in your form");
                     processServerFormErrors(json.form_errors);
                  }
                  else
                  {
                     alert("A server error occurred and we could not create the service, please contact technical support.");
                  }
               }
            }).fail(function (data) {
               alert("A server error occurred and we could not create the service, please contact technical support.");
            });

         }
      });

      //reset the errors on typing for each of hte boxes
      $("#userName").change(function() {
         $("#userName").parent().removeClass('text-error');
         $("#userNameError").hide();
      });

      $("#firstName").change(function() {
         $("#firstName").parent().removeClass('text-error');
         $("#firstNameError").hide();
      });

      $("#lastName").change(function() {
         $("#lastName").parent().removeClass('text-error');
         $("#lastNameError").hide();
      });

      $("#setPassword").change(function() {
         $("#setPassword").parent().removeClass('text-error');
         $("#setPasswordError").hide();
      });

   });

   /**
    * Takes the servers form error response and displays it to the form
    * @param formErrors (json object)
    * */
   function processServerFormErrors(formErrors)
   {
      if(formErrors.hasOwnProperty('userName'))
      {
         $("#userName").parent().addClass('has-error');
         $("#userNameError").html(formErrors.userName);
         $("#userNameError").show();
      }
   }

   function validateUserCreateForm()
   {
      console.log("Validating form.");
      var isFormValid = true;
      resetFormErrors();

      //validate the new or selected account
      if(!isInteger(selectedServiceId) || selectedServiceId < 1)
      {
         isFormValid = false;
         $("#serviceSelectName").addClass('text-danger');
         $("#errorServiceInvalid").show();
      }

      var userName = $("#userName").val();
      var userType = $("#userType").val();
      var firstName = $("#firstName").val();
      var lastName = $("#lastName").val();
      var setPassword = $("#setPassword").val();
      var setPasswordConfirm = $("#setPasswordConfirm").val();


      if(userName == "")
      {
         isFormValid = false;
         $("#userName").parent().addClass('text-error');
         $("#userNameError").html("The username cannot be blank.");
         $("#userNameError").show();
      }

      if(firstName == "")
      {
         isFormValid = false;
         $("#firstName").parent().addClass('text-error');
         $("#firstNameError").html("The first name cannot be blank.");
         $("#firstNameError").show();
      }

      if(lastName == "")
      {
         isFormValid = false;
         $("#lastName").parent().addClass('text-error');
         $("#lastNameError").html("The first name cannot be blank.");
         $("#lastNameError").show();
      }

      if(setPassword == "")
      {
         isFormValid = false;
         $("#setPassword").parent().addClass('text-error');
         $("#setPasswordError").html("The password cannot be blank.");
         $("#setPasswordError").show();
      }
      else if(setPassword.length < 6)
      {
         isFormValid = false;
         $("#setPassword").parent().addClass('text-error');
         $("#setPasswordError").html("Your password is too short, it must be at least 6 characters.");
         $("#setPasswordError").show();
      }
      else if(setPassword !==  setPasswordConfirm)
      {
         isFormValid = false;
         $("#setPassword").parent().addClass('text-error');
         $("#setPasswordError").html("The passwords you enetered do not match.");
         $("#setPasswordError").show();
      }

      return isFormValid;
   }

   function resetFormErrors()
   {
      //for the accounts section
      $("#serviceSelectName").removeClass('text-error');
      $("#serviceName").parent().removeClass('has-error');
      $("#errorServiceName").hide();
      $("#errorServiceInvalid").hide();

      //for the service section
      $("#userName").parent().removeClass('text-error');
      $("#userNameError").hide();
      $("#firstName").parent().removeClass('text-error');
      $("#firstNameError").hide();
      $("#lastName").parent().removeClass('text-error');
      $("#lastNameError").hide();
      $("#setPassword").parent().removeClass('text-error');
      $("#setPasswordError").hide();
   }


   function setSelectedService(serviceId, serviceName)
   {
      console.log(serviceId + " NAME: " + serviceName);

      $('#serviceSelectName').html(serviceName + '  <i class="fa fa-check-circle"></i>');
      selectedServiceId = serviceId;
      selectedServiceName = serviceName;

      $('#serviceSelectName').addClass("text-success");
      $("#errorServiceInvalid").hide();

      $('#modalFragSelectService').modal('hide');
   }


   /*****************************************
    * HELPERS
    *****************************************/
   function isInteger(value)
   {
      return value % 1 === 0;
   }

   //shows the error in the MODAL with a custom error message
   function showFragModalError(message)
   {
      $("#modalFragSelectAccountAlertText").html(message);
      $("#modalFragSelectAccountAlert").show();
   }

   //hides the modal error and resets the message
   function hideFragModalError()
   {
      $("#modalFragSelectAccountAlertText").html();
      $("#modalFragSelectAccountAlert").hide();
   }
</script>

<!-- Template Footer -->

