<?php

   /**
    * This script is called by the manage users page
    * @author - Doug Jenkinson
    */
   require_once("../../php/allInclusive.php");
   require_once("../../PluginAPI/PluginAPIAutoload.php");

   session_start();

   if(isset($_POST['user_id']))
   {
      $user_id = $_POST['user_id'];
   }
   else if(isset($_GET['user_id']))
   {
      $user_id = $_GET['user_id'];
   }
   else
   {
      echo '<p class="text-error">An error occurred, please refresh your browser.</p>';
      die();
   }

   //get the user object
   $user_obj = new User($user_id);

   $roles = $user_obj->helper_users->getAllRoles();

   $client_reseller_roles = array();
   $admin_roles = array();

   foreach($roles as $role)
   {
      $role_type_lowercase = strtolower($role['role_type']);

      //check if its a page link or section
      if($role['role_id'] % 100 == 0)
      {
         $role['is_section'] = true;
      }
      else
      {
         $role['is_section'] = false;
      }

      if (strpos($role_type_lowercase, 'client') !== false || strpos($role_type_lowercase, 'reseller') !== false)
      {
         array_push($client_reseller_roles, getRoleChecked($user_obj, $role));
      }
      else if(strpos($role_type_lowercase, 'admin') !== false && !(strpos($role_type_lowercase, 'client') !== false || strpos($role_type_lowercase, 'reseller') !== false))
      {
         array_push($admin_roles, getRoleChecked($user_obj, $role));
      }
   }

   function getRoleChecked($user_obj, $role)
   {
      if($user_obj->checkUserHasRole($role['role_id']))
      {
         $role['display_class'] = "text-success";
         $role['checked'] = true;
      }
      else
      {
         $role['display_class'] = "";
         $role['checked'] = false;
      }
      return $role;
   }
?>

   <div class="row">
      <div class="col-lg-8">
         <p class="lead">You can manage this users password from this page.</p>
      </div>
      <div class="col-lg-4">

      </div>
   </div>
   <div class="row">

      <!-- MENU ITEM CONTROL SECTION -->
      <div class="col-lg-8">
         <div class="box box-primary">
            <div class="box-header with-border">
               <h3 class="box-title"><i class="fa fa-navicon"></i> Menu Access (Roles)</h3>
            </div>
            <div class="box-body">
               <p>Select which menu items this user should be able to see and access, these are commonly referred to as user "roles". Be sure not to provide admin access to client and reseller users.</p>
               <div class="row">
                  <div class="col-lg-4">
                     <p><strong>Assign Role Templates: </strong></p>
                  </div>
                  <div class="col-lg-8">
                     <div class="btn-group btn-group-xs pull-right" role="group" aria-label="Role Templates">
                        <button class="btn btn-secondary btn-assign-role-template" connet-role-template="client">
                           <i class="fa fa-user"></i> All Client Roles
                        </button>
                        <button class="btn btn-secondary btn-assign-role-template" connet-role-template="reseller">
                           <i class="fa fa-users"></i> All Reseller Roles
                        </button>
                        <button class="btn btn-secondary btn-assign-role-template" connet-role-template="admin">
                           <i class="fa fa-user-secret"></i> All Admin Roles
                        </button>
                     </div>
                  </div>
               </div>
               <div class="row">
                  <div class="col-lg-6">
                     <h4>Client/Reseller Roles</h4>
                     <?php foreach($client_reseller_roles as $role) { ?>
                        <?php if($role['is_section'] ) { ?>
                           <hr/>
                           <div class="row">
                              <div class="col-lg-9">
                                 <label class="<?php echo $role['display_class']; ?>" style="font-size:14pt;" data-toggle="tooltip" data-placement="right" title="Menu Section">
                                    <input type="checkbox" class="menu-access-checkbox" connet-role-id="<?php echo $role['role_id'] ?>" <?php echo ($role['checked'] === true ? 'checked' : '') ?>>
                                    <strong>  <?php echo $role['role_name']; ?> <small><i class="fa fa-plus-square-o"></i></small> </strong>
                                    <!-- <small>(<?php echo $role['role_url']; ?>)</small> -->
                                 </label><br/>
                              </div>
                              <div class="col-lg-3">
                                 <small class="pull-right" id="ajaxStatus<?php echo $role['role_id'] ?>"></small>
                              </div>
                           </div>
                        <?php } else { ?>
                           <div class="row">
                              <div class="col-lg-9">
                                 <label class="<?php echo $role['display_class']; ?>"  data-toggle="tooltip" data-placement="right" title="Role Type: <?php echo $role['role_type']; ?>">
                                    <input type="checkbox" class="menu-access-checkbox" connet-role-id="<?php echo $role['role_id'] ?>" <?php echo ($role['checked'] === true ? 'checked' : '') ?>>
                                    - <i class="fa fa-link"></i> <?php echo $role['role_name']; ?>
                                    <!-- <small>(<?php echo $role['role_url']; ?>)</small> -->
                                 </label><br/>
                              </div>
                              <div class="col-lg-3">
                                 <small class="pull-right" id="ajaxStatus<?php echo $role['role_id'] ?>"></small>
                              </div>
                           </div>
                        <?php } ?>
                     <?php } ?>
                  </div>
                  <div class="col-lg-6">
                     <h4>Admin Roles</h4>
                     <hr/>
                     <?php foreach($admin_roles as $role) { ?>
                        <div class="row">
                           <div class="col-lg-9">
                              <label class="<?php echo $role['display_class']; ?>"  data-toggle="tooltip" data-placement="right" title="Role Type: <?php echo $role['role_type']; ?>">
                                 <input type="checkbox" class="menu-access-checkbox" connet-role-id="<?php echo $role['role_id'] ?>" <?php echo ($role['checked']  === true ? 'checked' : '') ?>>
                                 <?php echo $role['role_name']; ?>
                                 <!-- <small>(<?php echo $role['role_url']; ?>)</small> -->
                              </label><br/>
                           </div>

                           <div class="col-lg-3">
                              <small class="pull-right" id="ajaxStatus<?php echo $role['role_id'] ?>"></small>
                           </div>
                        </div>
                     <?php } ?>
                  </div>
               </div>
            </div>
         </div>
      </div>


      <!-- PASSWORD CHANGE SECTION -->
      <div class="col-lg-4">
         <div class="box box-primary">
            <div class="box-header with-border">
               <h3 class="box-title"><i class="fa fa-lock"></i> Change Password</h3>
            </div>
            <div class="box-body">
               <p>
                  Type and retype a new password in the fields below. Be sure to share the new password with the user.<br/>
                  <small class="text-warning">N.B. Passwords are limited to 8 characters only.</small>
               </p>
               <div class="form-group">
                  <label for="new_password">New Password:</label>
                  <input type="password" class="form-control" maxlength="8" id="inputNewPassword" placeholder="******* (enter new password to change)" value="" />
               </div>
               <div class="form-group">
                  <label for="new_password_confirm">Confirm New Password:</label>
                  <input type="password" class="form-control" maxlength="8" id="inputNewPasswordConfirm" placeholder="******* (enter new password to change)" value="" />
               </div>

               <br/>
               <div class="callout callout-success" id="alertPasswordSuccess" style="display:none;">
                  <p>The password was successfully changed.</p>
               </div>
               <button class="btn btn-primary" id="btnSaveNewPassword">
                  <i class="fa fa-save"></i> Change Password
               </button>
            </div>
         </div>
      </div>
   </div>

   <script type="text/javascript">
      var fragUserId = <?php echo (isset($user_id) ? $user_id : '-1') ?>;

      $(document).ready(function (e)
      {
         //opt in on the tooltips
         $('[data-toggle="tooltip"]').tooltip();

         /**
          * Assigns a role template to this user according to the definition within the button
          */
         $('.btn-assign-role-template').on("click", function (e)
         {
            e.preventDefault();
            var roleTemplate = $(this).attr('connet-role-template');

            if (confirm("Are you sure you want to assign all " + roleTemplate + " roles to this user?"))
            {
               //show loading
               var thisButton = $(this);
               var thisButtonHTML = $(this).html();
               $(this).attr("disabled", true);
               $(this).html('<span class="glyphicon glyphicon-refresh glyphicon-spin"></span> Saving password...');

               //run the ajax script
               $.post("../php/ajaxManageUserHandler.php", {
                  task: 'assign_role_template',
                  userId: fragUserId,
                  roleTemplate:roleTemplate,
               }).done(function (data) {
                  var json = $.parseJSON(data);
                  if (json.success)
                  {
                     $(thisButton).html("Reloading...");
                     location.reload();
                  }
                  else
                  {
                     $(thisButton).removeAttr("disabled");
                     $(thisButton).html(thisButtonHTML);
                     alert("A server error occurred and we could not save the data, please refresh your page or contact technical support.");
                  }
               }).fail(function (data) {
                  $(thisButton).removeAttr("disabled");
                  $(thisButton).html(thisButtonHTML);
                  alert("A server error occurred and we could not save the data, please refresh your page or contact technical support.");
               });
            }
         });

         /**
          * Checks and saves the new passwords
          */
         $('#btnSaveNewPassword').on("click", function (e)
         {
            e.preventDefault();

            if(confirm("Are you sure you want to update this users password?"))
            {
               $('#inputNewPassword').parent().removeClass('has-error');
               $('#inputNewPasswordConfirm').parent().removeClass('has-error');
               $('#alertPasswordSuccess').hide();

               var thisButton = $(this);
               var thisButtonHTML = $(this).html();
               $(this).attr("disabled", true);
               $(this).html('<span class="glyphicon glyphicon-refresh glyphicon-spin"></span> Saving password...');

               var newPassword = $('#inputNewPassword').val();
               var newPasswordConfirm = $('#inputNewPasswordConfirm').val();

               if(newPassword == "")
               {
                  alert("A user password cannot be blank.");
                  $('#inputNewPassword').parent().addClass('has-error');

                  $(thisButton).removeAttr("disabled");
                  $(thisButton).html(thisButtonHTML);
               }
               else if(newPassword !== newPasswordConfirm)
               {
                  alert("The passwords you entered do not match.");
                  $('#inputNewPassword').parent().addClass('has-error');
                  $('#inputNewPasswordConfirm').parent().addClass('has-error');

                  $(thisButton).removeAttr("disabled");
                  $(thisButton).html(thisButtonHTML);
               }
               else if(newPassword.length < 6)
               {
                  alert("The password you entered is too short. It must be at least 6 characters.");
                  $('#inputNewPassword').parent().addClass('has-error');
                  $('#inputNewPasswordConfirm').parent().addClass('has-error');

                  $(thisButton).removeAttr("disabled");
                  $(thisButton).html(thisButtonHTML);
               }
               else
               {
                  $.post("../php/ajaxManageUserHandler.php", {
                     task: 'save_new_password',
                     userId: fragUserId,
                     newPassword:newPassword,
                  }).done(function (data) {
                     var json = $.parseJSON(data);
                     if (json.success)
                     {
                        $('#inputNewPassword').val('');
                        $('#inputNewPasswordConfirm').val('');
                        $('#alertPasswordSuccess').show();

                        $(thisButton).removeAttr("disabled");
                        $(thisButton).html(thisButtonHTML);
                     }
                     else
                     {
                        $(thisButton).removeAttr("disabled");
                        $(thisButton).html(thisButtonHTML);
                        alert("A server error occurred and we could not save the data, please refresh your page or contact technical support.");
                     }
                  }).fail(function (data) {
                     $(thisButton).removeAttr("disabled");
                     $(thisButton).html(thisButtonHTML);
                     alert("A server error occurred and we could not save the data, please refresh your page or contact technical support.");
                  });
               }
            }
         });



         /**
          * Detects checkbox check/uncheck for menu roles
          */
         $('.menu-access-checkbox').change(function() {

            var thisCheckbox = $(this);
            var thisChecked = this.checked;
            var roleId = $(this).attr('connet-role-id');
            $(thisCheckbox).attr("disabled", true);

            //show that htere is an action taking pace
            $('#ajaxStatus'+roleId).removeClass('text-success');
            $('#ajaxStatus'+roleId).removeClass('text-danger');
            $('#ajaxStatus'+roleId).html('<span class="glyphicon glyphicon-refresh glyphicon-spin"></span> Saving...');

            $.post("../php/ajaxManageUserHandler.php", {
               task: 'toggle_role',
               userId: fragUserId,
               roleId: roleId,
               checked:this.checked
            }).done(function (data) {
               var json = $.parseJSON(data);
               if (json.success)
               {
                  $('#ajaxStatus'+roleId).html('Saved.');
                  $('#ajaxStatus'+roleId).addClass('text-success');
                  if(!thisChecked)
                  {
                     $(thisCheckbox).parent().removeClass('text-success');
                  }
                  else
                  {
                     $(thisCheckbox).parent().addClass('text-success');
                  }
                  $(thisCheckbox).removeAttr("disabled");
               }
               else
               {
                  $('#ajaxStatus'+roleId).html('Error. Not saved.');
                  $('#ajaxStatus'+roleId).addClass('text-danger');
                  $(thisCheckbox).removeAttr("disabled");
                  alert("We were unable to assign this role.");
               }
            }).fail(function (data) {
               $('#ajaxStatus'+roleId).html('Error. Not saved.');
               $('#ajaxStatus'+roleId).addClass('text-danger');
               $(thisCheckbox).removeAttr("disabled");
               alert("We were unable to assign this role.");
            });

         });


      });


   </script>


