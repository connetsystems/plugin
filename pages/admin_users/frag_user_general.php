<?php

   /**
    * This script is called by the manage users page, and runs certain ajax based tasks
    * @author - Doug Jenkinson
    */
   require_once("../../php/allInclusive.php");
   require_once("../../PluginAPI/PluginAPIAutoload.php");

   session_start();

   if(isset($_POST['user_id']))
   {
      $user_id = $_POST['user_id'];
   }
   else if(isset($_GET['user_id']))
   {
      $user_id = $_GET['user_id'];
   }
   else
   {
      echo '<p class="text-error">An error occurred, please refresh your browser.</p>';
      die();
   }

   //get the user object
   $user_obj = new User($user_id);
?>

   <div class="row">
      <div class="col-lg-8">
         <p class="lead">You can edit this users general account details from this tab.</p>
      </div>
      <div class="col-lg-4">

      </div>
   </div>
   <div class="row">
      <div class="col-lg-12">
         <p>You can update some of the specific account details of this user using this form.</p>
         <div class="row">
            <div class="col-lg-4">
               <div class="form-group">
                  <label for="new_password">First Name:</label>
                  <input type="text" class="form-control" maxlength="40" id="inputFirstName" placeholder="No first name entered..." value="<?php echo $user_obj->first_name; ?>" />
               </div>
            </div>
            <div class="col-lg-4">
               <div class="form-group">
                  <label for="new_password_confirm">Last Name:</label>
                  <input type="text" class="form-control" maxlength="40" id="inputLastName" placeholder="No last name entered..." value="<?php echo $user_obj->last_name; ?>" />
               </div>
            </div>
            <div class="col-lg-4">
               <div class="form-group">
                  <label for="new_password_confirm">Company Name:</label>
                  <input type="text" class="form-control" maxlength="40" id="inputCompanyName" placeholder="No company name entered..." value="<?php echo $user_obj->company_name; ?>" />
               </div>
            </div>
         </div>
         <div class="row">
            <div class="col-lg-6">
               <div class="form-group">
                  <label for="new_password">Email Address:</label>
                  <input type="text" class="form-control" maxlength="40" id="inputEmailAddress" placeholder="No email address entered..." value="<?php echo $user_obj->email_address; ?>" />
               </div>
            </div>
            <div class="col-lg-6">
               <div class="form-group">
                  <label for="new_password_confirm">Cell:</label>
                  <input type="text" class="form-control" maxlength="12" id="inputCellNumber" placeholder="No cellphone number entered..." value="<?php echo $user_obj->cell_number; ?>" />
               </div>
            </div>
         </div>

         <button class="btn btn-primary" id="btnSaveAccountDetails">
            <i class="fa fa-save"></i> Update Account Details
         </button>

      </div>
   </div>

   <script type="text/javascript">
      var fragUserId = <?php echo (isset($user_id) ? $user_id : '-1') ?>;

      $(document).ready(function (e)
      {
         /**
          * Saves the form if available!
          */
         $('#btnSaveAccountDetails').on("click", function (e)
         {
            e.preventDefault();

            var thisButton = $(this);
            $(this).attr("disabled", true);
            $(this).html('<span class="glyphicon glyphicon-refresh glyphicon-spin"></span> Saving...');

            var firstName = $('#inputFirstName').val();
            var lastName = $('#inputLastName').val();
            var companyName = $('#inputCompanyName').val();
            var emailAddress = $('#inputEmailAddress').val();
            var cellNumber = $('#inputCellNumber').val();

            $.post("../php/ajaxManageUserHandler.php", {
               task: 'save_account_details',
               userId: fragUserId,
               firstName:firstName,
               lastName:lastName,
               companyName:companyName,
               emailAddress:emailAddress,
               cellNumber:cellNumber
            }).done(function (data) {
               var json = $.parseJSON(data);
               if (json.success)
               {
                  location.reload();
               }
               else
               {
                  $(thisButton).removeAttr("disabled");
                  alert("A server error occurred and we could not save the data, please refresh your page or contact technical support.");
               }
            }).fail(function (data) {
               $(thisButton).removeAttr("disabled");
               alert("A server error occurred and we could not save the data, please refresh your page or contact technical support.");
            });

         });

      });


   </script>


