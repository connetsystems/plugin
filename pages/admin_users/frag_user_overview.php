<?php

   /**
    * This script is called by the manage users page, and runs certain ajax based tasks
    * @author - Doug Jenkinson
    */
   require_once("../../php/allInclusive.php");
   require_once("../../PluginAPI/PluginAPIAutoload.php");

   session_start();

   if(isset($_POST['user_id']))
   {
      $user_id = $_POST['user_id'];
   }
   else if(isset($_GET['user_id']))
   {
      $user_id = $_GET['user_id'];
   }
   else
   {
      echo '<p class="text-error">An error occurred, please refresh your browser.</p>';
      die();
   }

   //get the user object
   $user_obj = new User($user_id);

   $first_name = (isset($user_obj->first_name) && $user_obj->first_name != "" ? htmlentities($user_obj->first_name) : "n/a");
   $last_name = (isset($user_obj->last_name) && $user_obj->last_name != "" ? htmlentities($user_obj->last_name) : "n/a");
   $company_name = (isset($user_obj->company_name) && $user_obj->company_name != "" ? htmlentities($user_obj->company_name) : "n/a");
   $email_address = (isset($user_obj->first_name) && $user_obj->email_address != "" ? htmlentities($user_obj->email_address) : "n/a");
   $cell_phone = (isset($user_obj->cell_number) && $user_obj->cell_number != "" ? htmlentities($user_obj->cell_number) : "n/a");

   //generate the service links
   $service_links = array();
   foreach($user_obj->user_services as $service) {
      array_push($service_links, '<a href="manage_service.php?service_id='.$service['service_id'].'&user_id='.$user_id.'">'.htmlentities($service['service_name']).'</a>');
   }

   $batch_activity = $user_obj->getBatchActivity();
   $payment_activity = $user_obj->getPaymentActivity()
?>

   <div class="row">
      <div class="col-lg-8">
         <p class="lead">Below is the general overview of this user.</p>
      </div>
      <div class="col-lg-4">

      </div>
   </div>
   <div class="row">

      <!-- PASSWORD CHANGE SECTION -->
      <div class="col-lg-6">
         <div class="box box-primary">
            <div class="box-header with-border">
               <h3 class="box-title"><i class="fa fa-tag"></i> General Info</h3>
            </div>
            <div class="box-body">

               <!-- basic contact information -->
               <h4>Contact Information</h4>
               <div class="row">
                  <div class="col-lg-3">
                     <p>First Name:</p>
                  </div>
                  <div class="col-lg-8">
                     <strong><?php echo $first_name; ?></strong>
                  </div>
               </div>
               <div class="row">
                  <div class="col-lg-3">
                     <p>Last Name:</p>
                  </div>
                  <div class="col-lg-8">
                     <strong><?php echo $last_name; ?></strong>
                  </div>
               </div>
               <div class="row">
                  <div class="col-lg-3">
                     <p>Company Name:</p>
                  </div>
                  <div class="col-lg-8">
                     <strong><?php echo $company_name; ?></strong>
                  </div>
               </div>
               <div class="row">
                  <div class="col-lg-3">
                     <p>Email Address:</p>
                  </div>
                  <div class="col-lg-8">
                     <strong><?php echo $email_address; ?></strong>
                  </div>
               </div>
               <div class="row">
                  <div class="col-lg-3">
                     <p>Cellphone:</p>
                  </div>
                  <div class="col-lg-8">
                     <strong><?php echo $cell_phone; ?></strong>
                  </div>
               </div>

               <!-- basic contact information -->
               <h4>Account/Service Information</h4>
               <div class="row">
                  <div class="col-lg-3">
                     <p>Owner Account:</p>
                  </div>
                  <div class="col-lg-8">
                     <strong>
                        <a href="manage_account.php?account_id=<?php echo $user_obj->default_account['account_id']; ?>&user_id=<?php echo $user_id; ?>">
                           <?php echo htmlentities($user_obj->default_account['account_name']); ?>
                        </a>
                     </strong>
                  </div>
               </div>
               <div class="row">
                  <div class="col-lg-3">
                     <p>Default Service:</p>
                  </div>
                  <div class="col-lg-8">
                     <strong><?php echo  htmlentities($user_obj->default_service['service_name']); ?></strong>
                  </div>
               </div>
               <div class="row">
                  <div class="col-lg-3">
                     <p>Other Services:</p>
                  </div>
                  <div class="col-lg-8">
                     <?php echo implode(', ', $service_links); ?>
                  </div>
               </div>
            </div>
         </div>
      </div>

      <!-- THE USER BATCH ACTIVITY -->
      <div class="col-lg-3">
         <div class="box box-primary">
            <div class="box-header with-border">
               <h3 class="box-title"><i class="fa fa-truck"></i> Latest Batch Activity</h3>
            </div>
            <div class="box-body">
               <?php if(count($batch_activity) == 0 ) { ?>
                  <p>No batch activity exists for this user.</p>
               <?php } else { ?>
                   <table class="table table-striped">
                      <thead>
                        <tr>
                           <th>Service Name</th>
                           <th style="text-align:right;">Total Batches</th>
                        </tr>
                      </thead>
                      <tbody>
                        <?php foreach($batch_activity as $batch_total) { ?>
                           <tr>
                              <td><?php echo  htmlentities($batch_total['service_name']); ?></td>
                              <td align="right"><?php echo  htmlentities($batch_total['total']); ?></td>
                           </tr>
                         <?php } ?>
                      </tbody>
                   </table>
               <?php } ?>
            </div>
         </div>
      </div>

      <!-- THE USER PAYMENT ACTIVITY -->
      <div class="col-lg-3">
         <div class="box box-primary">
            <div class="box-header with-border">
               <h3 class="box-title"><i class="fa fa-money"></i> Payment Activity</h3>
            </div>
            <div class="box-body">
               <?php if(count($batch_activity) == 0 ) { ?>
                  <p>No payment activity exists for this user.</p>
               <?php } else { ?>
                  <table class="table table-striped">
                     <thead>
                     <tr>
                        <th>Service Name</th>
                        <th style="text-align:right;">Total Payments</th>
                     </tr>
                     </thead>
                     <tbody>
                     <?php foreach( $payment_activity as $payment_total) { ?>
                        <tr>
                           <td><?php echo htmlentities($payment_total['service_name']); ?></td>
                           <td align="right"><?php echo  htmlentities($payment_total['total']); ?></td>
                        </tr>
                     <?php } ?>
                     </tbody>
                  </table>
               <?php } ?>

            </div>
         </div>
      </div>
   </div>

   <script type="text/javascript">
      var fragUserId = <?php echo (isset($user_id) ? $user_id : '-1') ?>;

      $(document).ready(function (e)
      {

      });


   </script>


