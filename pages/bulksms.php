<?php
/**
 * Include the header tempalte which sets up the HTML
 *
 * Don't forget to include template_import_script before any Javascripting
 * Don't forget to include template footer.php at the bottom of the page as well
 */
set_time_limit(360);

//-------------------------------------------------------------
// Template
//-------------------------------------------------------------
TemplateHelper::setPageTitle('Bulk SMS');
TemplateHelper::initialize();

$singleView = 'block;';
$freeAcc = "";
$freeTxt = "";

//-------------------------------------------------------------
// Permissions
//-------------------------------------------------------------
$clients = PermissionsHelper::getAllServicesWithPermissions('submit.mtsms');

if (isset($_GET['client'])) {

   $split = explode(' ', trim($_GET['client']));
   if (!isset($clients[(integer) end($split)])) {
      
      header('location: ' . $_SERVER['DOCUMENT_URI']);
      die();      
   }
}

if (count($clients) == 1) {

   $_GET['client'] = $_SESSION['accountName'] . ' - ' . $_SESSION['serviceName'] . " - " . $_SESSION['serviceId'];
   $_GET['service_id'] = LoginHelper::getCurrentServiceId();
   
   $singleView = 'none;';
} 

//-------------------------------------------------------------
// Free Credits
//-------------------------------------------------------------
switch (LoginHelper::getCurrentAccountId()) {

   case 650:
      $freeAcc = "readonly";
      $freeTxt = "Did you know that 90% of the population in RSA own a cell phone?  Use bulk SMS from Connet Systems for your mobile marketing solutions. www.connet-systems.com";
      break;
}

if (isset($_GET['client'])) {

   $split = explode(' ', trim($_GET['client']));
   $cl = (integer) end($split);

   $dsp = 'block;';
   $camps = getCampaignData($cl);
   $check = '';
   $checked = 'checked';

   if (isset($_GET['campId']) && $_GET['campId'] != "") {
      $dsp2 = 'block;';
      $temps = getTempData($_GET['campId']);
   } else {
      if ($singleView != 'block;') {
         $check = 'disabled';
         $checked = '';
      }
      $dsp2 = 'none;';
   }

   $checkPerm = checkUserCanSend($cl, $_SESSION['userId']);
} else {
   $check = 'disabled';
   $checked = '';
   $_GET['client'] = "0 - Please select a client...";
   $cl = strstr($_GET['client'], ' ', true);
   $dsp = 'none;';
   $dsp2 = 'none;';

   $checkPerm = 'null';
}

if ($checkPerm != 'null' && $checkPerm != 2) {
   $showPerm = 'block;';
   $showPermOp = 'none;';
} else {
   $showPerm = 'none;';
   $showPermOp = 'block;';
}


////////////////////////////////////////////////
$noFunds = "";
////////////////////////////////////////////////
?>

<aside class="right-side">
   <section class="content-header">
      <h1>
         Bulk SMS
      </h1>
   </section>

   <section class="content">
      <div class="row col-lg-12">
         <?php if (isset($_GET['error'])) { ?>
            <div class="callout callout-danger" style="padding-top:10px;margin-bottom:30px;">
               <h3 class="text-danger" style="margin-top:2px;">There was an error with the format of your file.</h3>
               <p>Please check the formatting of your file and try again.</p>
            </div>
         <?php } ?>
         <div class="callout callout-danger" id="numssError" style="padding-top:10px;margin-bottom:30px;display:<?php echo $showPerm; ?>">
            <h4>No Permissions</h4>
            <p>
               You do not have permissions to send on this account. Please contact support.
            </p>
            <div style="float:right; margin-top:-44px; margin-right:10px;">
               <i class="fa fa-exclamation-triangle" style="color:#c99b9d; font-size:20pt;"></i>
            </div>
         </div>
         <div class="callout callout-warning" id="numssError" style="padding-top:10px;margin-bottom:30px;display:<?php echo $showPermOp; ?>">
            <h4>Consumer Protection Act (CPA)</h4>
            <p>
               According to the Consumer Protection Act, direct marketing messages may only be delivered to subscribers between 08:00 and 20:00 during weekdays, and 09:00 and 13:00 on Saturdays.
               <br/>No direct marketing messages may be delivered on Sundays or public holidays. Direct marketing messages must include a valid opt-out message
            </p>
            <div style="float:right; margin-top:-36px; margin-right:10px;">
               <i class="fa fa-exclamation-triangle" style="color:#c99b9d; font-size:20pt;"></i>
            </div>

            <div class="checkbox" style="padding-bottom:0px;">
               <label style="padding-left:15px;">
                  <input type="checkbox" id="cpa" name="cpaInfo" <?php echo $checked; ?>>
                  I have read and understood the CPA direct marketing regulations above and I also confirm that I will abide by the WASPA regulations available <strong><a target="_blank" href="http://waspa.org.za/coc/">here</a></strong>.
               </label>
            </div>
         </div>
         <div class="box box-solid" style="height:66px; margin-top:-15px;display:<?php echo $singleView; ?>">
            <form action="" method="get" id="clientList" class="sidebar-form" style="border:0px;">
               <div class="form-group">
                  <label>Service List</label>
                  <select class="form-control" <?php echo $check; ?> name="client" id="clientSel" form="clientList" OnChange="reloadOnSelect(this.value);">
                     <?php
                     if ($cl == '0') {
                        echo '<option SELECTED>Please select a client...</option>';
                     }
                     foreach ($clients as $key => $value) {
                        $sId = $value['service_id'];
                        if ($cl == $sId) {
                           $accountName = $value['account_name'];
                           $serviceName = $value['service_name'];
                           echo "<option name='" . $sId . "' SELECTED>" . $accountName . " - " . $serviceName . " - " . $sId . "</option>";
                        } else {
                           echo "<option name='" . $sId . "'>" . $value['account_name'] . " - " . $value['service_name'] . " - " . $sId . "</option>";
                        }
                     }
                     ?>
                  </select>
               </div>
            </form>
         </div>
         <div class="box box-warning" style="border-top-color:<?php echo $accRGB; ?>;display:<?php echo $dsp; ?>">
            <div class="box-body" style="margin-bottom:0px;padding-bottom:1px;">
               <form role="form" id="bulkData" form="batch" enctype="multipart/form-data" action="bulksmscont.php" method="POST">
                  <?php
                  if (isset($cl) && $cl != 0) {
                     $sR = getServiceCredit($cl);
                     $sT = getServiceType($cl);

                     if ($sR / 10000 <= 0 && $sT == 'PREPAID') {
                        $noFunds = "disabled";
                        echo '<div class="callout callout-danger" id="numssError" style="margin-bottom:0px;">';
                        echo '<h4>Insufficient Credits!</h4>';
                        echo '<p>You do not have enough funds to send on this service. Please contact support.</p>';
                        echo '<div style="float:right; margin-top:-42px; margin-right:10px;">';
                        echo '<i class="fa fa-exclamation-triangle" style="color:#c99b9d; font-size:20pt;"></i>';
                        echo '</div>';
                        echo '</div>';
                     }
                  }
                  ?>
                  <div class="callout callout-info" style="margin-bottom:10px;">
                     <h4>Bulk SMS</h4>
                     <p>You can upload a CSV file -AND/OR- enter a list of numbers(comma separated) manually.</p>
                  </div>
                  <div class="box box-warning" style="border-top-color:<?php echo $accRGB; ?>;display:<?php echo $dsp; ?>;margin-bottom:10px;">
                     <div class="box-body" style="margin-bottom:0px;">
                        <div class="form-group" style="margin-bottom:0px;">
                           <label>Campaign List</label>
                           <select class="form-control" <?php echo $check; ?> name="campaign" id="camp" form="bulkData" <?php echo $noFunds; ?> OnChange="getCamp()">
                              <?php
                              echo "<option>Please Select</option>";
                              foreach ($camps as $key => $value) {
                                 if (isset($_GET['campId']) && $_GET['campId'] == $value['campaign_id']) {
                                    $camCode = $value['campaign_code'];
                                    $camId = $value['campaign_id'];
                                    echo "<option SELECTED id='" . $value['campaign_id'] . "'>Campaign Group: '" . $value['client_name'] . "'  -  Campaign: '" . $value['campaign_name'] . "'  -  Campaign Code: '" . $value['campaign_code'] . "' </option>";
                                 } else {
                                    echo "<option id='" . $value['campaign_id'] . "'>Campaign Group: '" . $value['client_name'] . "'  -  Campaign: '" . $value['campaign_name'] . "'  -  Campaign Code: '" . $value['campaign_code'] . "' </option>";
                                 }
                              }
                              ?>
                           </select>
                        </div>
                     </div>
                  </div>
                  <div class="box box-warning" style="border-top-color:<?php echo $accRGB; ?>;display:<?php echo $dsp2; ?>">

                     <div class="box-body">
                        <p><b>Reference</b></p>
                        <div class="callout callout-danger" id="referError" style="display:none;">
                           <p>Reference cannot be empty!</p>
                        </div>
                        <input type="text" class="form-control" name="refer" id="refer" placeholder="Provide a reference for this batch" value="" style="border:1px solid #aaaaaa;" >
                     </div>

                     <div class="box-body">
                        <label>Delivery Status</label>
                        <select class="form-control" name="status" id="temper" form="bulkData" OnChange="checkDelStatus(this.value)">
                           <option selected>Paused</option>
                           <option>Scheduled</option>
                           <option>Send Immediately</option>
                        </select>
                     </div>

                     <div class="box-body" id="schedulerBox" style="padding-bottom:35px;display:none;">
                        <label>Delivery Time</label>
                        <br>
                        <div id="scheduledTimer" class="select pull-left" style="cursor: pointer;padding-left:10px;">
                           <i class="fa fa-calendar fa-lg"></i>
                           <span style="font-size:15px" form="bulkData" id="sTime"><?php echo date("Y-m-d H:i"); ?></span><b class="caret"></b>
                        </div>
                        <input type="hidden" id="inputScheduleTime" name="inputScheduleTime" value="<?php echo date("Y-m-d H:i:s"); ?>" />
                     </div>

                     <div class="box-body" style="padding-bottom:1px;padding-top:5px;">
                        <div class="form-group">
                           <div class="form-group" style="margin-bottom:2px;">
                              <label>Template List</label>
                              <div class="callout callout-danger" id="tempError" style="display:none;">
                                 <p>Please select from the template list!</p>
                              </div>
                              <?php
                              if (count($temps) == 0) {
                                 $dropStat = 'disabled';
                                 $noTemps = ' - no templates available.';
                              } else {
                                 $dropStat = '';
                                 $noTemps = '';
                              }
                              ?>
                              <select class="form-control" <?php echo $dropStat; ?> name="template" id="temperl" form="bulkData" OnChange="checkTemplate(this.value)">
                                 <?php
                                 echo "<option value=''>Enter SMS text" . $noTemps . "</option>";
                                 foreach ($temps as $key => $value) {
                                    if ($value['template_status'] != "DISABLED") {
                                       echo "<option value='" . $value['template_id'] . "'> NAME: " . htmlentities($value['template_description']) . " | SMS TEXT: " . htmlentities($value['template_content']) . "</option>";
                                    }
                                 }
                                 ?>
                              </select>
                              <?php
                              //need to outpiut the actual text of the template
                              foreach ($temps as $key => $value)
                              {
                                 if ($value['template_status'] != "DISABLED")
                                 {
                                    echo '<input type="hidden" id="template_content' . $value['template_id'] . '" value="' . htmlentities($value['template_content']) . '" />';
                                 }
                              }
                              ?>
                           </div>

                           <div class="form-group" style="margin-top:15px;">
                              <label>Enter SMS text here</label>
                              <p>
                                 If you are going to upload a csv (comma delimited) file with multiple columns, you can reference a column by entering a placeholder containing the
                                 column number. Example: Hello {2}, your account balance is R{3}.
                              </p>
                              <div class="callout callout-danger" id="smsError" style="display:none;">
                                 <p>SMS text cannot be empty!</p>
                              </div>
                              <!--textarea class="form-control" id="smsText" name="smstext" rows="3" form="bulkData" placeholder="SMS text here.."></textarea-->
                              <textarea class="form-control" id="smsText" name="smsText" rows="3" placeholder="SMS text here.." <?php echo $freeAcc; ?> ></textarea>
                           </div>
                           <label id="counter"></label>
                        </div>
                     </div>
                  </div>
                  <input type="hidden" name="client" id="client" value="<?php echo $_GET['client']; ?>">
                  <input type="hidden" name="serviceId" value="<?php echo $cl; ?>">
                  <input type="hidden" name="camCode" value="<?php echo $camCode; ?>">
                  <input type="hidden" name="camId" value="<?php echo $camId; ?>">
                  <input type="hidden" name="smsCount" id="smsCount" value="">
               </form>
            </div>
            <div class="box-body" style="display:<?php echo $dsp2; ?>;padding-bottom:10px;margin-top:-5px;">
               <div class="form-group" style="margin-bottom:20px;">
                  <?php
                  if ($showPerm != 'block;') {
                     ?>
                     <a onclick="cont()" class="btn btn-block btn-social btn-dropbox" id="contBtn" style="background-color:<?php echo $accRGB; ?>; border: 2px solid; border-radius: 6px !important; float:left; width: 120px; margin-bottom:10px;margin-top:-15px;">
                        <i class="fa fa-arrow-right"></i>Continue
                     </a>
                     <?php
                  }
                  ?>
               </div>
            </div>
         </div>
      </div>
   </section>
</aside>

<!--  THE JAVASCRIPT IMPORT ALWAYS APPEARS BEFORE LOCAL JAVASCRIPT AT THE BOTTOM OF THE PAGE -->
<?php include("template_import_script.php"); //must import all scripts first   ?>
<!-- END JAVASCRIPT IMPORT -->

<script type="text/javascript">
   function getFormatDate(d)
   {
      return d.getFullYear() + '-' + twoDigits(d.getMonth() + 1) + '-' + twoDigits(d.getDate()) + ' ' + twoDigits(d.getHours()) +':'+ twoDigits(d.getMinutes());
   }

   function getFormatDateWithSeconds(d)
   {
      return d.getFullYear() + '-' + twoDigits(d.getMonth() + 1) + '-' + twoDigits(d.getDate()) + ' ' + twoDigits(d.getHours()) +':'+ twoDigits(d.getMinutes()) +':'+ twoDigits(d.getSeconds());
   }

   var minDate = getFormatDate(new Date()),
           mdTemp = new Date(),
           maxDate = getFormatDate(new Date(mdTemp.setDate(mdTemp.getDate() + 7)));
   //alert("min=" + minDate);

   $('#scheduledTimer').daterangepicker(
     {
        minDate: minDate,
        maxDate: maxDate,
        singleDatePicker: true,
        timePicker: true,
        timePickerIncrement: 5,
        timePicker24Hour: true,
        format: 'YYYY-MM-DD hh:mm A',
        timePickerSeconds: false
     },
     function (start, end, label)
     {
        if(end.format("YYYY-MM-DD HH:mm") < getFormatDate(new Date()))
        {
           $('#sTime').html(getFormatDate(new Date()));
           $('#inputScheduleTime').val(getFormatDateWithSeconds(new Date()));
           alert("You cannot select a schedule time before the current time. The schedule time has been reset to the current time.");
        }
        else
        {
           $('#sTime').html(end.format("YYYY-MM-DD HH:mm"));
           $('#inputScheduleTime').val(end.format("YYYY-MM-DD HH:mm:ss"));
        }
     });

   function twoDigits(d)
   {
      if(0 <= d && d < 10) return "0" + d.toString();
      if(-10 < d && d < 0) return "-0" + (-1*d).toString();
      return d.toString();
   }
</script>

<script type="text/javascript">
   $('input').on('ifChecked', function (event)
   {
      var checkedId = $(this, 'input').attr('id');
      if (checkedId == 'cpa')
      {
         var ammount = <?php
if (isset($sR)) {
   echo $sR / 10000;
} else {
   echo 0;
}
?>;
         var accType = "<?php
if (isset($sT)) {
   echo $sT;
} else {
   echo 'POSTPAID';
}
?>";

         if (ammount > 0 && accType == 'PREPAID')
         {
            document.getElementById('clientSel').disabled = false;
            document.getElementById('camp').disabled = false;
            //document.getElementById('contBtn').disabled = false;
            document.getElementById('contBtn').style.pointerEvents = "auto";
            document.getElementById('contBtn').style.cursor = "pointer";
         } else if (accType == 'POSTPAID')
         {
            document.getElementById('clientSel').disabled = false;
            document.getElementById('camp').disabled = false;
            //document.getElementById('contBtn').disabled = false;
            document.getElementById('contBtn').style.pointerEvents = "auto";
            document.getElementById('contBtn').style.cursor = "pointer";
         }

      }
   });

   $('input').on('ifUnchecked', function (event)
   {
      var checkedId = $(this, 'input').attr('id');
      if (checkedId == 'cpa')
      {
         document.getElementById('clientSel').disabled = true;
         document.getElementById('camp').disabled = true;
         //document.getElementById('contBtn').disabled = true;
         document.getElementById('contBtn').style.pointerEvents = "none";
         document.getElementById('contBtn').style.cursor = "default";
         //alert('The Term of the CPA must be accepted');
         //location.reload(true);
         location.href = 'bulksms.php';

      }
   });

</script>

<script type="text/javascript">
   $(function () {
      //execute validation on keyup
      $("#smsText").keyup(function (e)
      {
         validateSmsText();
      });

      //execute validation on keyup
      $("#smsText").change(function (e)
      {
         validateSmsText();
      });

      /**
       * REGEX SOURCE FOR 7 BIT GSM ENCODING - http://www.wenda.io/questions/602168/how-to-detect-non-gsm-7-bit-alphabet-characters-in-input-field.html
       */
      function cleanUp7BitGSMEncoding(string) {
         var pattern = new RegExp("^[A-Za-z0-9 \\r\\n@£$¥èéùìòÇØøÅå\u0394_\u03A6\u0393\u039B\u03A9\u03A0\u03A8\u03A3\u0398\u039EÆæßÉ!\"#$%&'()*+,\\-./:;<=>?¡ÄÖÑÜ§¿äöñüà^{}\\\\\\[~\\]|\u20AC]*$");

         return string.replace(pattern, '');
      }
   });

   var area = document.getElementById("smsText");
   var message = document.getElementById("counter");

   function validateSmsText()
   {
      if (area.value.length == 0)
      {
         message.innerHTML = "0 characters, 0 SMS";
      } else
      {
         //validation
         var thisText = $("#smsText").val();

         //replacing curly quotes with straight quotes
         var fixText = thisText
                 .replace(/[\u2018\u2019]/g, "'")
                 .replace(/[\u201C\u201D]/g, '"')
                 .replace("{ }", "( )")
                 .replace("{}", "()")
                 .replace(/\u2013|\u2014/g, "-");  //long dash
         //.replace(/\u20ac/g, 'E'); //euro sign

         //fixText = cleanUp7BitGSMEncoding(fixText);

         //fixText = decodeURI(encodeURIComponent(fixText));
         $("#smsText").val(fixText);

         var tot = 0;

         var str = area.value;
         var newLength = area.value.length;
         var count = occurrences(str, '[', 'false');
         if (count != 0)
         {
            newLength = newLength + (2 * count) - count;
         }
         count = occurrences(str, ']', 'false');
         if (count != 0)
         {
            newLength = newLength + (2 * count) - count;
         }
         count = occurrences(str, '^', 'false');
         if (count != 0)
         {
            newLength = newLength + (2 * count) - count;
         }
         count = occurrences(str, '|', 'false');
         if (count != 0)
         {
            newLength = newLength + (2 * count) - count;
         }
         count = occurrences(str, '~', 'false');
         if (count != 0)
         {
            newLength = newLength + (2 * count) - count;
         }
         count = occurrences(str, '\\', 'false');
         if (count != 0)
         {
            newLength = newLength + (2 * count) - count;
         }
         count = occurrences(str, '\u20AC', 'false');
         if (count != 0)
         {
            newLength = newLength + (2 * count) - count;
         }

         count = occurrences(str, '\u21A1', 'false');
         if (count != 0)
         {
            newLength = newLength + (2 * count) - count;
         }
         count = occurrences(str, '{', 'false');
         if (count != 0)
         {
            newLength = newLength + (2 * count) - count;
         }
         count = occurrences(str, '}', 'false');
         if (count != 0)
         {
            newLength = newLength + (2 * count) - count;
         }

         if (newLength <= 160)
         {
            tot = Math.ceil((newLength) / 160);
         } else
         {
            tot = Math.ceil((newLength) / 153);
         }

         message.innerHTML = newLength + " characters, " + tot + " SMS";
         var elem = document.getElementById("smsCount");
         elem.value = tot;
      }
   }

   function reloadOnSelect(name)
   {
      $(".loader").fadeIn("slow");
      $(".loaderIcon").fadeIn("slow");
      window.location = "bulksms.php?client=" + name;
   }

   function getCamp()
   {
      var clientOp = clientSel.options;
      var name = clientOp[clientOp.selectedIndex].value;
      var options = camp.options;
      var id = options[options.selectedIndex].id;

      window.location = "bulksms.php?client=" + name + "&campId=" + id;
   }

   function checkDelStatus(v)
   {
      if (v == 'Scheduled')
      {
         document.getElementById('schedulerBox').style.display = 'block';
      } else
      {
         document.getElementById('schedulerBox').style.display = 'none';
      }
   }

   function occurrences(string, subString, allowOverlapping)
   {
      string += "";
      subString += "";
      if (subString.length <= 0)
         return string.length + 1;

      var n = 0, pos = 0;
      var step = (allowOverlapping) ? (1) : (subString.length);

      while (true)
      {
         pos = string.indexOf(subString, pos);
         if (pos >= 0) {
            n++;
            pos += step;
         } else
            break;
      }
      return(n);
   }


   function checkTemplate(templateId)
   {
      var freeTextBox = document.getElementById('smsText');
      if (templateId == '')
      {
         freeTextBox.value = '';
         freeTextBox.disabled = false;
      }
      else if ('<?php echo $freeTxt; ?>' != '')
      {
         freeTextBox.value = 'Did you know that 90% of the population in RSA own a cell phone?  Use bulk SMS from Connet Systems for your mobile marketing solutions. www.connet-systems.com';
      }
      else
      {
         freeTextBox.value = '';
         var text = $("#template_content" + templateId).val(); //converts special chars to normal text
         freeTextBox.value = text;
         freeTextBox.disabled = true;
         validateSmsText();
      }
   }

   function cont()
   {
      var smsInput = document.getElementById('smsText').value;
      var smsInputB = document.getElementById('smsText');
      var smsErr = document.getElementById('smsError');
      var referInput = document.getElementById('refer').value;
      var referErr = document.getElementById('referError');
      var validater = 0;

      if (smsInput == '')
      {
         smsErr.style.display = 'block';
      } else
      {
         smsErr.style.display = 'none';
         validater++;
      }

      if (referInput == '')
      {
         referErr.style.display = 'block';
      } else
      {
         referErr.style.display = 'none';
         validater++;
      }

      //smsInputB.value = smsInput.replace(/\r?\n|\r/g, '\n');
      //////////////////////////////////////////////////////


      if (validater == 2)
      {
         $('#bulkData').submit();
      }
   }

   var tempBox = document.getElementById('temperl').value;
   checkTemplate(tempBox);

   $(window).load(function () {
      $(".loader").fadeOut("slow");
      $(".loaderIcon").fadeOut("slow");
   });
</script>


<!-- Template Footer -->
