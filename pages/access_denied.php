<?php
/**
 * Include the header tempalte which sets up the HTML
 *
 * Don't forget to include template_import_script before any Javascripting
 * Don't forget to include template footer.php at the bottom of the page as well
 */
$headerPageTitle = "Batchlist SMS"; //set the page title for the template import
TemplateHelper::initialize();
?>

<aside class="right-side">
   <section class="content-header">
      <h1 class="text-error">
         Access Denied
         <!--small>Control panel</small-->
      </h1>
   </section>

   <section class="content" style="padding-bottom:0px;">
      <p class="text-danger">You do not have the correct permissions to view this content.<p>
   </section>
</aside>

<!--  THE JAVASCRIPT IMPORT ALWAYS APPEARS BEFORE LOCAL JAVASCRIPT AT THE BOTTOM OF THE PAGE -->
<?php include("template_import_script.php"); //must import all scripts first  ?>
<!-- END JAVASCRIPT IMPORT -->

<script type="text/javascript">
   $(window).load(function () {
      $(".loader").fadeOut("slow");
      $(".loaderIcon").fadeOut("slow");
   })
</script>

<!-- Template Footer -->

