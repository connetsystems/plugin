<?php
/**
 * Niel's Hacks 101
 * 
 * I've hacked the elastic response document to contain dummy rows / buckets
 * for MO, otherwise MOs are not visible if no MTs was sent for a day
 * 
 * The JS Save Report does not work, hacked in code to rework elastic response
 * and output the CSV server-side. Uses STDOUT, which is not ideal.
 * 
 * CSV contants a seperator row, not 100% sure about compatibility
 * 
 * Ok, appears JS Save Report only works in chrome
 * 
 */
//-------------------------------------------------------------
// Template
//-------------------------------------------------------------
TemplateHelper::setPageTitle("SMS Traffic Breakdown");
TemplateHelper::initialize();

$singleView = 'block;';
$sort = urlencode('"timestamp":"desc"');
$sents = 0;

//-------------------------------------------------------------
// Permissions
//-------------------------------------------------------------
$clients = PermissionsHelper::getAllServicesWithPermissions();

if (isset($_GET['client'])) {

   $split = explode(' ', trim($_GET['client']));
   if (!isset($clients[(integer) end($split)])) {

      header('location: ' . $_SERVER['DOCUMENT_URI']);
      die();
   }
}

if (count($clients) == 1) {

   $_GET['client'] = $_SESSION['accountName'] . ' - ' . $_SESSION['serviceName'] . " - " . $_SESSION['serviceId'];
   $singleView = 'none;';
   
} elseif (!isset($_GET['client'])) {

   $_GET['client'] = $_SESSION['accountName'] . ' - ' . $_SESSION['serviceName'] . " - " . $_SESSION['serviceId'];
}

//-------------------------------------------------------------
// Datepicker
//-------------------------------------------------------------
if (isset($_GET['range'])) {
   $dateArr = explode(' - ', $_GET['range']);
   $startDate = $dateArr[0];
   $endDate = $dateArr[1];
} else {
   $startDate = (strtotime('today midnight')) * 1000;
   $endDate = (time() * 1000);
}

$startDateConv = date('Y-m-d', ($startDate / 1000));
$endDateConv = date('Y-m-d', ($endDate / 1000));

//-------------------------------------------------------------
// Client
//-------------------------------------------------------------
if (isset($_GET['client']) && $_GET['client'] != 'Show All') {

   $split = explode(' ', trim($_GET['client']));
   $cl = (integer) end($split);

   $dsp = 'inline';
   $dir = '../img/routes';
} else {
   $_GET['client'] = "0 - Please select a client...";
   $cl = 0;
   $dsp = 'none';
}

//-------------------------------------------------------------
// Elastic Queries / Save Report
//-------------------------------------------------------------
// <editor-fold defaultstate="collapsed">
if ($cl != 0) {

   $qryMo = '
{
                      "size": 0,
                      "aggs": {
                        "5": {
                          "date_histogram": {
                            "field": "timestamp",
                            "interval": "1d",
                            "pre_zone": "+02:00",
                            "pre_zone_adjust_large_interval": false,
                            "min_doc_count": 1,
                            "extended_bounds": {
                                "min": ' . $startDate . ',
                                "max": ' . $endDate . '
                            }
                          },
                          "aggs": {
                            "3": {
                              "terms": {
                                "field": "_type",
                                "size": 0,
                                "order": {
                                  "_count": "desc"
                                }
                              }
                            }
                          }
                        }
                      },
                      "query": {
                        "filtered": {
                          "query": {
                            "query_string": {
                              "query": "service_id:' . $cl . '",
                              "analyze_wildcard": true
                            }
                          },
                          "filter": {
                            "bool": {
                              "must": [
                                {
                                  "range": {
                                    "timestamp": {
                                      "gte": ' . $startDate . ',
                                      "lte": ' . $endDate . '
                                    }
                                  }
                                }
                              ],
                              "must_not": []
                            }
                          }
                        }
                      }
                    }';


   $allStats2 = runRawMOSMSElasticsearchQuery($qryMo);
   // <editor-fold defaultstate="collapsed" desc="$qery">
   $qry = '{
                  "size": 0,
                  "aggs": {
                    "5": {
                      "date_histogram": {
                        "field": "timestamp",
                        "interval": "1d",
                        "pre_zone": "+02:00",
                        "pre_zone_adjust_large_interval": false,
                        "min_doc_count": 1,
                        "extended_bounds": {
                          "min": ' . $startDate . ',
                          "max": ' . $endDate . '
                        }
                      },
                      "aggs": {
                        "1": {
                          "sum": {
                            "script": "doc[\'billing_units\'].value",
                            "lang": "expression"
                          }
                        },
                        "dlr": {
                          "terms": {
                            "field": "dlr",
                            "size": 0,
                            "order": {
                              "dlr_units": "desc"
                            }
                          },
                          "aggs": {
                            "dlr_units": {
                              "sum": {
                                "script": "doc[\'billing_units\'].value",
                                "lang": "expression"
                              }
                            },
                            "rdnc": {
                              "terms": {
                                "field": "rdnc",
                                "size": 0,
                                "order": {
                                  "1": "desc"
                                }
                              },
                              "aggs": {
                                "1": {
                                  "sum": {
                                    "script": "doc[\'billing_units\'].value",
                                    "lang": "expression"
                                  }
                                }
                              }
                            }
                          }
                        }
                      }
                    }
                },
                      "query": {
                        "filtered": {
                          "query": {
                            "query_string": {
                              "query": "service_id:' . $cl . '",
                              "analyze_wildcard": true
                            }
                          },
                          "filter": {
                            "bool": {
                              "must": [
                                {
                                  "range": {
                                    "timestamp": {
                                      "gte": ' . $startDate . ',
                                      "lte": ' . $endDate . '
                                    }
                                  }
                                }
                              ],
                              "must_not": []
                            }
                          }
                        }
                      }
                    }
                ';

   // </editor-fold>
   $allStats = runRawMTSMSElasticsearchQuery($qry);

   //-------------------------------------------------------------
   // HACK: Build Mesage Info for CSV download
   //-------------------------------------------------------------   
   // <editor-fold defaultstate="collapsed">
   $sms_totals = array();
   if (isset($allStats2['aggregations'])) {
      foreach ($allStats2['aggregations'] as $date_buckets) {

         foreach ($date_buckets['buckets'] as $date_bucket) {

            $date = date('Y-m-d', strtotime($date_bucket['key_as_string']));

            if (!isset($sms_totals[$date])) {

               $sms_totals[$date]['date'] = $date;
               $sms_totals[$date]['count'] = 0;
               $sms_totals[$date]['units'] = 0;
            }

            if (!isset($sms_totals[$date]['mosms'])) {

               $sms_totals[$date]['mosms'] = (integer) $date_bucket['doc_count'];
            } else {
               $sms_totals[$date]['mosms'] += (integer) $date_bucket['doc_count'];
            }
         }
      }
   }

   if (isset($allStats['aggregations'])) {

      foreach ($allStats['aggregations'] as $date_buckets) {

         foreach ($date_buckets['buckets'] as $date_bucket) {

            $date = date('Y-m-d', strtotime($date_bucket['key_as_string']));

            if (isset($date_bucket['dlr'])) {

               foreach ($date_bucket['dlr']['buckets'] as $dlr_bucket) {

                  $dlr_mask = (integer) $dlr_bucket['key'];
                  $count = (integer) $dlr_bucket['doc_count'];
                  $units = (integer) $dlr_bucket['dlr_units']['value'];

                  $status_masks = array(
                      1 => 'delivered',
                      // 4096 => 'rdnc', 8192 => 'rdnc',
                      2 => 'failed',
                      16 => 'rejected', 64 => 'rejected', 1024 => 'rejected',
                      512 => 'pending', 1024 => 'pending',
                      0 => 'pending'
                  );

                  foreach ($status_masks as $mask => $status) {

                     if (($dlr_mask & $mask) == $mask) {


                        break;
                     }
                  }

                  if (!isset($sms_totals[$date])) {

                     $sms_totals[$date]['date'] = $date;
                     $sms_totals[$date]['count'] = $count;
                     $sms_totals[$date]['units'] = $units;
                  } else {
                     $sms_totals[$date]['count'] += $count;
                     $sms_totals[$date]['units'] += $units;
                  }

                  if (!isset($sms_totals[$date][$status])) {

                     $sms_totals[$date][$status] = $units;
                  } else {
                     $sms_totals[$date][$status] += $units;
                  }

                  foreach ($dlr_bucket['rdnc']['buckets'] as $rdnc_bucket) {

                     //What must be done here?
                  }
               }
            }
         }
      }
   }// </editor-fold>

   /**
    * -------------------------------------------------------------
    * Save Report
    * -------------------------------------------------------------
    * 
    * Currently writes to stdout, which makes error handling,
    * and content-length impossible
    *     
    */
   // <editor-fold defaultstate="collapsed">
   if (isset($_REQUEST['save'])) {

      TemplateHelper::disgard();

      if (false) {

         header('Content-Type: text/plain; charset=UTF-8');
         clog::setVerbose();
         clog::setLogLevel(0);
      } else {

         header('Content-type: application/vnd.ms-excel');
         header('Content-Disposition: attachment; filename=' . "APITrafficSummary_{$startDate}-{$endDate}.csv");
         header('Expires: 0');
         header('Cache-Control: must-revalidate');
         header('Pragma: public');
         //header('Content-Length: ' . filesize($filename));
      }

      ob_clean();
      ob_implicit_flush(true);
      $stdout = fopen('php://output', 'w'); //Do this properly
      fputcsv($stdout, array('sep=,'), "\t");
      foreach (array_values($sms_totals) as $index => $row) {

         $csv = array(
             "Date" => $row['date'],
             "Description" => 'All Traffic',
             "Sent" => isset($row['count']) ? $row['count'] : 0,
             "Units" => isset($row['units']) ? $row['units'] : 0,
             "Pending" => isset($row['pending']) ? $row['pending'] : 0,
             "Delivered" => isset($row['delivered']) ? $row['delivered'] : 0,
             "Failed" => isset($row['failed']) ? $row['failed'] : 0,
             "Rejected" => isset($row['rejected']) ? $row['rejected'] : 0,
             "Replies" => isset($row['mosms']) ? $row['mosms'] : 0,
         );

         if ($index == 0) {

            fputcsv($stdout, array_keys($csv), ",");
         }
         fputcsv($stdout, $csv, ",");
      }
      fclose($stdout);
      die();
   }
   // </editor-fold>
//   
//   TemplateHelper::disgard();
//   header('Content-Type: text/plain; charset=UTF-8');
//   clog::setVerbose();
//   clog::setLogLevel(0);
//   clog($sms_totals);
//   die();   
   //-------------------------------------------------------------
   // HACK: Inject dummy records into elastic query for MOs
   //-------------------------------------------------------------
   // <editor-fold defaultstate="collapsed">
   $buckets = array();
   if (isset($allStats['aggregations'][5]['buckets'])) {

      foreach ($allStats['aggregations'][5]['buckets'] as $bucket) {

         $key_as_string = $bucket['key_as_string'];
         $buckets[$key_as_string] = $bucket;
      }
   }

   if (isset($allStats2['aggregations'][5]['buckets'])) {
      foreach ($allStats2['aggregations'][5]['buckets'] as $bucket) {

         $key_as_string = $bucket['key_as_string'];
         $key = $bucket['key'];

         if (!isset($buckets[$key_as_string])) {

            $buckets[$key_as_string] = array(
                'key_as_string' => $key_as_string,
                'key' => $key,
                'doc_count' => 0,
                1 => array(
                    'value' => 0
                ),
                'dlr' => array(
                    'buckets' => array(
                    )
                )
            );
         }
      }
   }

   if (!empty($buckets)) {

      ksort($buckets);
      $allStats['aggregations'][5]['buckets'] = array_values($buckets);
   }
   // </editor-fold>
}

// </editor-fold>
?>
<aside class="right-side">
   <section class="content-header">
      <h1>
         SMS Traffic Breakdown
      </h1>
   </section>

   <section class="content">
      <div class="box box-connet" style="padding-top:1px; border-top-color:<?php echo $accRGB; ?>;">
         <form action="updateservice.php" method="get" id="clientList" class="sidebar-form" style="border:0px;">
            <?php
            if ($singleView != 'block;') {
               ?>
               <div class="callout callout-info" style="margin-right:0px;margin-bottom:0px;">
                  <!--h4>Instructions</h4-->
                  <p>To view SMS Traffic, select a date range from your Calendar control.</p>
               </div>
               <?php
            } else {
               ?>
               <div class="callout callout-info" style="margin-right:0px;margin-bottom:0px;">
                  <!--h4>Instructions</h4-->
                  <p>To view SMS Traffic, select a date range from your Calendar control and then select a service form the Service List.</p>
               </div>
               <?php
            }
            ?>
            <br>
            <div id="reportrange" class="select pull-left" style="cursor:pointer;">
               <label>Select Date Range</label><br>
               <i class="fa fa-calendar fa-lg"></i>
               <span style="font-size:15px"><?php echo $startDateConv . " - " . $endDateConv; ?></span><b class="caret"></b>
            </div>
            <br>
            <br>
            <br>

<!--            <div class="form-group" style="padding-bottom:12px;display:<?php echo $singleView; ?>">
               <label>Template Selector</label>            
            <?php //require(SITE_BASE_PATH . '/pages/template_service_selector.php');     ?>
            </div>-->
            <div class="form-group" style="padding-bottom:12px;display:<?php echo $singleView; ?>">
               <label>Client List</label>
               <select class="form-control" name="client" id="client" form="clientList" OnChange="reloadOnSelect(this.value);">
                  <?php
                  if ($cl == '0') {
                     echo '<option SELECTED>Please select a client...</option>';
                  }
                  foreach ($clients as $key => $value) {
                     $sId = $value['service_id'];
                     if ($cl == $sId) {
                        $accountName = $value['account_name'];
                        $serviceName = $value['service_name'];
                        echo "<option name='" . $sId . "' SELECTED>" . $accountName . " - " . $serviceName . " - " . $sId . "</option>";
                     } else {
                        echo "<option name='" . $sId . "'>" . $value['account_name'] . " - " . $value['service_name'] . " - " . $sId . "</option>";
                     }
                  }
                  ?>
               </select>
            </div>
         </form>
      </div>
   </section>

   <section class="content">
      <div class="box box-connet" style="padding-top:1px; border-top-color:<?php echo $accRGB; ?>;">
         <div class="box-body">
            <div class="box-body table-responsive no-padding">
               <table id="example2"  class="table table-hover">
                  <thead>
                     <tr>
                        <th style="width:100px;">Date</th>
                        <th>Description</th>
                        <th style="background-color:#737AA6"><center>Sent</center></th>
                  <th style="background-color:#737AA6"><center>Units</center></th>
                  <th style="background-color:#DEA52F"><center>Pending</center></th>
                  <th style="background-color:#8BA808"><center>Delivered</center></th>
                  <th style="background-color:#f56954"><center>Failed</center></th>
                  <th style="background-color:#B8252B"><center>Rejected</center></th>
                  <!--th style="background-color:#ecebc0"><center>PENDING&nbsp;%</center></th>
                  <th style="background-color:#c0eccd"><center>DELIVERED&nbsp;%</center></th>
                  <th style="background-color:#ecc0c0"><center>FAILED&nbsp;%</center></th>
                  <th><center>View Msgs</center></th>
                  <th style="background-color:#cfebed"><center>MO/Delivered&nbsp;%</center></th>
                  <th style="background-color:#b1b1b1"><center>EXCLUDED&nbsp;%</center></th-->
                  <th style="background-color:#92e7ed"><center>MO&nbsp;Total</center></th>
                  </tr>
                  </thead>
                  <tbody>
                     <?php
                     if (isset($allStats)) {
                        $dlrArr681 = array();
                        $dlrArr680 = array();
                        $dlrArr682 = array();
                        $dlrArrREJ = array();

                        $dlrPos680 = array();
                        $dlrPos681 = array();
                        $dlrPos682 = array();
                        $dlrPosREJ = array();

                        foreach ($allStats as $key => $value) {
                           /* echo '<pre>';
                             print_r($value);
                             echo '</pre>'; */
                           if ($key = 'aggregations' && isset($value['5']['buckets'])) {
                              if (count($value['5']['buckets']) > 0) {
                                 for ($z = 0; $z < count($value['5']['buckets']); $z++) {
                                    /* $moCount = 0;
                                      foreach ($moData as $keyZ => $valueZ)
                                      {
                                      if($valueZ['timestamp'] == strstr($value['5']['buckets'][$z]['key_as_string'], 'T', true))
                                      {
                                      $moCount += $valueZ['id'];
                                      }
                                      } */
                                    # code...
                                    echo '<tr>';
                                    echo '<td>' . strstr($value['5']['buckets'][$z]['key_as_string'], 'T', true) . '</td>';
                                    echo '<td>All Traffic</td>';
                                    $sents = $value['5']['buckets'][$z]['1']['value'];
                                    $dc = $value['5']['buckets'][$z]['doc_count'];


                                    if ($dc != 0) {
                                       echo '<td style="cursor:pointer;background-color:#adb1cb; text-align:right;"; text-align:right;><a style="color:#000000;" onclick="viewMsg(`' . strstr($value['5']['buckets'][$z]['key_as_string'], 'T', true) . '`)">' . number_format($dc, 0, '.', ' ') . '</a></td>';
                                    } else {
                                       echo '<td style="background-color:#adb1cb; text-align:right;"; text-align:right;>' . number_format($dc, 0, '.', ' ') . '</td>';
                                    }
                                    echo '<td style="background-color:#adb1cb; text-align:right;"; text-align:right;>' . number_format($sents, 0, '.', ' ') . '</td>';

                                    $dlrArr680['5']['buckets'][$z]['dlr']['buckets'] = 0;
                                    $dlrArr681['5']['buckets'][$z]['dlr']['buckets'] = 0;
                                    $dlrArr682['5']['buckets'][$z]['dlr']['buckets'] = 0;
                                    $dlrArrREJ['5']['buckets'][$z]['dlr']['buckets'] = 0;
                                    //$dlrArr4288['5']['buckets'][$z]['dlr']['buckets'] = 0;

                                    for ($i = 0; $i < count($value['5']['buckets'][$z]['dlr']['buckets']); $i++) {
                                       $dlrPos = (integer) $value['5']['buckets'][$z]['dlr']['buckets'][$i]['key'];

                                       if (($dlrPos & 32) == 32 && ($dlrPos & 1) == 0 && ($dlrPos & 2) == 0 && ($dlrPos & 16) == 0) {
                                          $countRdnc = count($value['5']['buckets'][$z]['dlr']['buckets'][$i]['rdnc']['buckets']);
                                          for ($j = 0; $j < $countRdnc; $j++) {
                                             //if($value['5']['buckets'][$z]['dlr']['buckets'][$i]['rdnc']['buckets'][$j]['key'] == 0)
                                             {
                                                if (!in_array($dlrPos, $dlrPos680)) {
                                                   array_push($dlrPos680, $dlrPos);
                                                }
                                                if ($dlrArr680['5']['buckets'][$z]['dlr']['buckets'] == 0) {
                                                   $dlrArr680['5']['buckets'][$z]['dlr']['buckets'] = $value['5']['buckets'][$z]['dlr']['buckets'][$i]['rdnc']['buckets'][$j]['1']['value'];
                                                   //$dlrArr680['5']['buckets'][$z]['dlr']['buckets'] = $value['5']['buckets'][$z]['dlr']['buckets'][$i]['dlr_units']['value'];
                                                } else {
                                                   $dlrArr680['5']['buckets'][$z]['dlr']['buckets'] += $value['5']['buckets'][$z]['dlr']['buckets'][$i]['rdnc']['buckets'][$j]['1']['value'];
                                                   //$dlrArr680['5']['buckets'][$z]['dlr']['buckets'] += $value['5']['buckets'][$z]['dlr']['buckets'][$i]['dlr_units']['value'];
                                                }
                                             }
                                          }
                                       }
                                       if (($dlrPos & 1) == 1) {
                                          if (!in_array($dlrPos, $dlrPos681)) {
                                             array_push($dlrPos681, $dlrPos);
                                          }
                                          if ($dlrArr681['5']['buckets'][$z]['dlr']['buckets'] == 0) {
                                             $dlrArr681['5']['buckets'][$z]['dlr']['buckets'] = $value['5']['buckets'][$z]['dlr']['buckets'][$i]['dlr_units']['value'];
                                          } else {
                                             $dlrArr681['5']['buckets'][$z]['dlr']['buckets'] += $value['5']['buckets'][$z]['dlr']['buckets'][$i]['dlr_units']['value'];
                                          }
                                       } elseif (($dlrPos & 2) == 2) {
                                          if (!in_array($dlrPos, $dlrPos682)) {
                                             array_push($dlrPos682, $dlrPos);
                                          }
                                          if ($dlrArr682['5']['buckets'][$z]['dlr']['buckets'] == 0) {
                                             $dlrArr682['5']['buckets'][$z]['dlr']['buckets'] = $value['5']['buckets'][$z]['dlr']['buckets'][$i]['dlr_units']['value'];
                                          } else {
                                             $dlrArr682['5']['buckets'][$z]['dlr']['buckets'] += $value['5']['buckets'][$z]['dlr']['buckets'][$i]['dlr_units']['value'];
                                          }
                                       }
                                       if (($dlrPos & 16) == 16) {
                                          if (!in_array($dlrPos, $dlrPosREJ)) {
                                             array_push($dlrPosREJ, $dlrPos);
                                          }
                                          if ($dlrArrREJ['5']['buckets'][$z]['dlr']['buckets'] == 0) {
                                             $dlrArrREJ['5']['buckets'][$z]['dlr']['buckets'] = $value['5']['buckets'][$z]['dlr']['buckets'][$i]['dlr_units']['value'];
                                          } else {
                                             $dlrArrREJ['5']['buckets'][$z]['dlr']['buckets'] += $value['5']['buckets'][$z]['dlr']['buckets'][$i]['dlr_units']['value'];
                                          }
                                       }
                                       /* $countRdnc = count($value['5']['buckets'][$z]['dlr']['buckets'][$i]['rdnc']['buckets']);
                                         for ($j=0; $j < $countRdnc; $j++)
                                         {
                                         if($value['5']['buckets'][$z]['dlr']['buckets'][$i]['rdnc']['buckets'][$j]['key'] != 0)
                                         {
                                         if($dlrArr4288['5']['buckets'][$z]['dlr']['buckets'] == 0)
                                         {
                                         $dlrArr4288['5']['buckets'][$z]['dlr']['buckets'] = $value['5']['buckets'][$z]['dlr']['buckets'][$i]['rdnc']['buckets'][$j]['1']['value'];
                                         }
                                         else
                                         {
                                         $dlrArr4288['5']['buckets'][$z]['dlr']['buckets'] += $value['5']['buckets'][$z]['dlr']['buckets'][$i]['rdnc']['buckets'][$j]['1']['value'];
                                         }
                                         }
                                         } */
                                    }

                                    $pendUrlAttach = '';
                                    for ($il = 0; $il < count($dlrPos680); $il++) {
                                       $pendUrlAttach .= (string) $dlrPos680[$il] . '.';
                                    }
                                    $pendUrlAttach = substr($pendUrlAttach, 0, -1);
                                    $pendUrlAttach = ("'" . $pendUrlAttach . "'");

                                    $deliUrlAttach = '';
                                    for ($il = 0; $il < count($dlrPos681); $il++) {
                                       $deliUrlAttach .= (string) $dlrPos681[$il] . '.';
                                    }
                                    $deliUrlAttach = substr($deliUrlAttach, 0, -1);
                                    $deliUrlAttach = ("'" . $deliUrlAttach . "'");

                                    $failUrlAttach = '';
                                    for ($il = 0; $il < count($dlrPos682); $il++) {
                                       $failUrlAttach .= (string) $dlrPos682[$il] . '.';
                                    }
                                    $failUrlAttach = substr($failUrlAttach, 0, -1);
                                    $failUrlAttach = ("'" . $failUrlAttach . "'");

                                    $rejeUrlAttach = '';
                                    for ($il = 0; $il < count($dlrPosREJ); $il++) {
                                       $rejeUrlAttach .= (string) $dlrPosREJ[$il] . '.';
                                    }
                                    $rejeUrlAttach = substr($rejeUrlAttach, 0, -1);
                                    $rejeUrlAttach = ("'" . $rejeUrlAttach . "'");


                                    /* echo '<td style="background-color:#c0eccd; text-align:right;">'.$dlrArr681['5']['buckets'][$z]['dlr']['buckets'].'</td>';
                                      echo '<td style="background-color:#ecc0c0; text-align:right;">'.$dlrArr682['5']['buckets'][$z]['dlr']['buckets'].'</td>';
                                      echo '<td style="background-color:#dbb2b2; text-align:right;">'.$dlrArrREJ['5']['buckets'][$z]['dlr']['buckets'].'</td>'; */
                                    //echo '<td style="background-color:#ecebc0; text-align:right;">'.$dlrArr680['5']['buckets'][$z]['dlr']['buckets'].'</td>';

                                    if ($dlrArr680['5']['buckets'][$z]['dlr']['buckets'] != 0) {
                                       echo '<td style="background-color:#e9cb8c; text-align:right;"><a style="color:#000000;cursor:pointer;" onclick="viewMsgVar(' . $pendUrlAttach . ',`' . strstr($value['5']['buckets'][$z]['key_as_string'], 'T', true) . '`,`Pending`)">' . number_format($dlrArr680['5']['buckets'][$z]['dlr']['buckets'], 0, '.', ' ') . '</a></td>';
                                    } else {
                                       echo '<td style="background-color:#e9cb8c; text-align:right;"><a style="color:#000000;">0</a></td>';
                                    }

                                    if ($dlrArr681['5']['buckets'][$z]['dlr']['buckets'] != 0) {
                                       echo '<td style="background-color:#c0d26e; text-align:right;"><a style="color:#000000;cursor:pointer;" onclick="viewMsgVar(' . $deliUrlAttach . ',`' . strstr($value['5']['buckets'][$z]['key_as_string'], 'T', true) . '`,`Delivered`)">' . number_format($dlrArr681['5']['buckets'][$z]['dlr']['buckets'], 0, '.', ' ') . '</a></td>';
                                    } else {
                                       echo '<td style="background-color:#c0d26e; text-align:right;"><a style="color:#000000;">0</a></td>';
                                    }

                                    if ($dlrArr682['5']['buckets'][$z]['dlr']['buckets'] != 0) {
                                       echo '<td style="background-color:#f8b6ac; text-align:right;"><a style="color:#000000;cursor:pointer;" onclick="viewMsgVar(' . $failUrlAttach . ',`' . strstr($value['5']['buckets'][$z]['key_as_string'], 'T', true) . '`,`Failed`)">' . number_format($dlrArr682['5']['buckets'][$z]['dlr']['buckets'], 0, '.', ' ') . '</a></td>';
                                    } else {
                                       echo '<td style="background-color:#f8b6ac; text-align:right;"><a style="color:#000000;">0</a></td>';
                                    }

                                    if ($dlrArrREJ['5']['buckets'][$z]['dlr']['buckets'] != 0) {
                                       echo '<td style="background-color:#df8c8f; text-align:right;"><a style="color:#000000;cursor:pointer;" onclick="viewMsgVar(' . $rejeUrlAttach . ',`' . strstr($value['5']['buckets'][$z]['key_as_string'], 'T', true) . '`,`Rejected`)">' . number_format($dlrArrREJ['5']['buckets'][$z]['dlr']['buckets'], 0, '.', ' ') . '</a></td>';
                                    } else {
                                       echo '<td style="background-color:#df8c8f; text-align:right;"><a style="color:#000000;">0</a></td>';
                                    }
                                    //echo '<td style="background-color:#ecebc0; text-align:right;">'.round($dlrArr680['5']['buckets'][$z]['dlr']['buckets']/$sents*100, 2).' %</td>';
                                    //echo '<td style="background-color:#c0eccd; text-align:right;">'.round($dlrArr681['5']['buckets'][$z]['dlr']['buckets']/$sents*100, 2).' %</td>';
                                    //echo '<td style="background-color:#ecc0c0; text-align:right;">'.round($dlrArr682['5']['buckets'][$z]['dlr']['buckets']/$sents*100, 2).' %</td>';
                                    //echo '<td style="background-color:#b1b1b1; text-align:right;">'.$dlrArr4288['5']['buckets'][$z]['dlr']['buckets'].'</td>';
                                    //echo '<td style="background-color:#b1b1b1; text-align:right;">'.round($dlrArr4288['5']['buckets'][$z]['dlr']['buckets']/$sents*100, 2).' %</td>';
                                    //echo '<td style="background-color:#b1b1b1; text-align:right;">'.$value['5']['buckets'][$z]['dlr']['buckets'][$i]['rdnc']['buckets'][$z]['doc_count'].'</td>';
                                    $cnn = 0;
                                    if (is_array($allStats2)) {
                                       foreach ($allStats2 as $key7 => $value7) {
                                          if ($key7 = 'aggregations' && isset($value7['5']['buckets'])) {
                                             if (count($value7['5']['buckets']) > 0) {
                                                for ($y = 0; $y < count($value7['5']['buckets']); $y++) {
                                                   if ($value7['5']['buckets'][$y]['key_as_string'] == $value['5']['buckets'][$z]['key_as_string']) {
                                                      $cnn++;
                                                      //echo '<td>'.$value7['5']['buckets'][$y]['3']['buckets']['0']['doc_count'].'</td>';
                                                      echo '<td style="background-color:#cfebed; text-align:right;"><a style="color:#333333;cursor:pointer;" onclick="gotoMo(`' . urlencode(strstr($value['5']['buckets'][$z]['key_as_string'], 'T', true)) . '`, ' . $cl . ', ' . $value7['5']['buckets'][$y]['3']['buckets']['0']['doc_count'] . ');">' . number_format($value7['5']['buckets'][$y]['3']['buckets']['0']['doc_count'], 0, '.', ' ') . '</a></td>';
                                                      /* if($dlrArr681['5']['buckets'][$z]['dlr']['buckets'] != 0)
                                                        {
                                                        echo '<td style="background-color:#cfebed; text-align:right;">'.round($value7['5']['buckets'][$y]['3']['buckets']['0']['doc_count']/$dlrArr681['5']['buckets'][$z]['dlr']['buckets']*100, 2).' %</td>';
                                                        }
                                                        else
                                                        {
                                                        echo '<td style="background-color:#cfebed; text-align:right;">-</td>';
                                                        } */
                                                   }
                                                }
                                             }
                                          }
                                       }
                                    }

                                    if ($cnn == 0) {
                                       echo '<td style="background-color:#cfebed; text-align:right;"><a style="color:#333333;">0</a></td>';
                                    }

                                    /* echo '<td>
                                      <center>
                                      <a class="btn btn-block btn-social btn-dropbox" name="nid" onclick="viewMsg()" style="background-color:'.$accRGB.'; color: #ffffff; border: 2px solid; border-radius: 6px !important; font-size:0.8em !important; height:24px; width: 120px; margin-top:0px; padding-top:2px;">
                                      <i class="fa fa-eye" style="font-size:1.0em !important; color:#ffffff; margin-top:-6px;"></i>View Msgs
                                      </a>
                                      </center>
                                      </td>'; */
                                    echo '<tr>';
                                 }
                              }
                           }
                        }
                     }
                     ?>
                  </tbody>
               </table>
            </div>
         </div>
         <!--         <div class="box-body" style="margin-top:10px;">
                     <div class="form-group">
                        <a onclick="saveRep();" class="btn btn-block btn-social btn-dropbox " style="background-color:<?php echo $accRGB; ?>; border: 2px solid; border-radius: 6px !important; float:left; width: 134px; margin-top:-21px;">
                           <i class="fa fa-save"></i>Save Report
                        </a>
                     </div>
                  </div>-->
         <div class="box-body" style="margin-top:10px;">
            <div class="form-group">
               <button 
                  onclick='window.location = <?php echo json_encode('?' . http_build_query(array('client' => $cl, 'range' => isset($_GET['range']) ? $_GET['range'] : NULL, 'save' => uniqid()))); ?>'
                  class="btn btn-block btn-social btn-dropbox" 
                  style="background-color:<?php echo $accRGB; ?>; border: 2px solid; border-radius: 6px !important; float:left; width: 134px; margin-top:-21px;">
                  <i class="fa fa-save"></i>Save Report
               </button>
            </div>                     
         </div>   
      </div>
   </section>
</aside>

<!--  THE JAVASCRIPT IMPORT ALWAYS APPEARS BEFORE LOCAL JAVASCRIPT AT THE BOTTOM OF THE PAGE -->
<?php include("template_import_script.php"); //must import all scripts first               ?>
<!-- END JAVASCRIPT IMPORT -->

<script type="text/javascript">
                     $('#reportrange').daterangepicker(
                             {
                                //singleDatePicker:true,
                                ranges: {
                                   'Today': [moment(), moment()],
                                   'Yesterday': [moment().subtract('days', 1), moment().subtract('days', 1)],
                                   'Last 7 Days': [moment().subtract('days', 6), moment()],
                                   'Last 30 Days': [moment().subtract('days', 29), moment()],
                                   'This Month': [moment().startOf('month'), moment().endOf('month')],
                                   'Last Month': [moment().subtract('month', 1).startOf('month'), moment().subtract('month', 1).endOf('month')]
                                },
                                startDate: moment(<?php echo $startDate; ?>),
                                endDate: moment(<?php echo $endDate; ?>)
                             },
                             function (start, end) {

                                $('#reportrange span').html(start + ' - ' + end);
                                $(".loader").fadeIn("slow");
                                $(".loaderIcon").fadeIn("slow");

                                var serviceName = document.getElementById("client").value;
                                var repRange1 = $("#reportrange span").html();

                                //alert(repRange1);
                                /*var start = moment(repRange1).unix()*1000;
                                 var end = moment(repRange1).add(1, 'day');//
                                 end = (end.unix()-1)*1000;*/
                                var repRange = start + ' - ' + end;
                                encodeURI(serviceName);
                                window.location = "../pages/apitraffic.php?client=" + encodeURIComponent(serviceName) + "&range=" + repRange;
                             }
                     );
</script>

<script type="text/javascript">
   function saveRep()
   {
      var csvContent = "data:text/csv;charset=utf-8,";

      var data = [["Date", "Description", "Sent", "Units", "Pending", "Delivered", "Failed", "Rejected", "Replies"],
<?php
if (isset($allStats)) {
   $dlrArr681 = array();
   $dlrArr680 = array();
   $dlrArr682 = array();
   $dlrArrREJ = array();

   $dlrPos680 = array();
   $dlrPos681 = array();
   $dlrPos682 = array();
   $dlrPosREJ = array();

   foreach ($allStats as $key => $value) {
      if ($key = 'aggregations' && isset($value['5']['buckets'])) {
         if (count($value['5']['buckets']) > 0) {
            for ($z = 0; $z < count($value['5']['buckets']); $z++) {
               echo '[';
               echo '"' . strstr($value['5']['buckets'][$z]['key_as_string'], 'T', true) . '",';
               echo '"All Traffic",';
               $sents = $value['5']['buckets'][$z]['1']['value'];
               $dc = $value['5']['buckets'][$z]['doc_count'];
               //echo '"'.strstr($value['5']['buckets'][$z]['key_as_string'], 'T', true).'",';//
               echo '"' . number_format($dc, 0, '.', ' ') . '",';
               echo '"' . number_format($sents, 0, '.', ' ') . '",';

               $dlrArr680['5']['buckets'][$z]['dlr']['buckets'] = 0;
               $dlrArr681['5']['buckets'][$z]['dlr']['buckets'] = 0;
               $dlrArr682['5']['buckets'][$z]['dlr']['buckets'] = 0;
               $dlrArrREJ['5']['buckets'][$z]['dlr']['buckets'] = 0;
               //$dlrArr4288['5']['buckets'][$z]['dlr']['buckets'] = 0;

               for ($i = 0; $i < count($value['5']['buckets'][$z]['dlr']['buckets']); $i++) {
                  $dlrPos = (integer) $value['5']['buckets'][$z]['dlr']['buckets'][$i]['key'];

                  if (($dlrPos & 32) == 32 && ($dlrPos & 1) == 0 && ($dlrPos & 2) == 0 && ($dlrPos & 16) == 0) {
                     $countRdnc = count($value['5']['buckets'][$z]['dlr']['buckets'][$i]['rdnc']['buckets']);
                     for ($j = 0; $j < $countRdnc; $j++) {
                        //if($value['5']['buckets'][$z]['dlr']['buckets'][$i]['rdnc']['buckets'][$j]['key'] == 0)
                        {
                           if (!in_array($dlrPos, $dlrPos680)) {
                              array_push($dlrPos680, $dlrPos);
                           }
                           if ($dlrArr680['5']['buckets'][$z]['dlr']['buckets'] == 0) {
                              $dlrArr680['5']['buckets'][$z]['dlr']['buckets'] = $value['5']['buckets'][$z]['dlr']['buckets'][$i]['rdnc']['buckets'][$j]['1']['value'];
                              //$dlrArr680['5']['buckets'][$z]['dlr']['buckets'] = $value['5']['buckets'][$z]['dlr']['buckets'][$i]['dlr_units']['value'];
                           } else {
                              $dlrArr680['5']['buckets'][$z]['dlr']['buckets'] += $value['5']['buckets'][$z]['dlr']['buckets'][$i]['rdnc']['buckets'][$j]['1']['value'];
                              //$dlrArr680['5']['buckets'][$z]['dlr']['buckets'] += $value['5']['buckets'][$z]['dlr']['buckets'][$i]['dlr_units']['value'];
                           }
                        }
                     }
                  }
                  if (($dlrPos & 1) == 1) {
                     if (!in_array($dlrPos, $dlrPos681)) {
                        array_push($dlrPos681, $dlrPos);
                     }
                     if ($dlrArr681['5']['buckets'][$z]['dlr']['buckets'] == 0) {
                        $dlrArr681['5']['buckets'][$z]['dlr']['buckets'] = $value['5']['buckets'][$z]['dlr']['buckets'][$i]['dlr_units']['value'];
                     } else {
                        $dlrArr681['5']['buckets'][$z]['dlr']['buckets'] += $value['5']['buckets'][$z]['dlr']['buckets'][$i]['dlr_units']['value'];
                     }
                  } elseif (($dlrPos & 2) == 2) {
                     if (!in_array($dlrPos, $dlrPos682)) {
                        array_push($dlrPos682, $dlrPos);
                     }
                     if ($dlrArr682['5']['buckets'][$z]['dlr']['buckets'] == 0) {
                        $dlrArr682['5']['buckets'][$z]['dlr']['buckets'] = $value['5']['buckets'][$z]['dlr']['buckets'][$i]['dlr_units']['value'];
                     } else {
                        $dlrArr682['5']['buckets'][$z]['dlr']['buckets'] += $value['5']['buckets'][$z]['dlr']['buckets'][$i]['dlr_units']['value'];
                     }
                  }
                  if (($dlrPos & 16) == 16) {
                     if (!in_array($dlrPos, $dlrPosREJ)) {
                        array_push($dlrPosREJ, $dlrPos);
                     }
                     if ($dlrArrREJ['5']['buckets'][$z]['dlr']['buckets'] == 0) {
                        $dlrArrREJ['5']['buckets'][$z]['dlr']['buckets'] = $value['5']['buckets'][$z]['dlr']['buckets'][$i]['dlr_units']['value'];
                     } else {
                        $dlrArrREJ['5']['buckets'][$z]['dlr']['buckets'] += $value['5']['buckets'][$z]['dlr']['buckets'][$i]['dlr_units']['value'];
                     }
                  }
               }

               $pendUrlAttach = '';
               for ($il = 0; $il < count($dlrPos680); $il++) {
                  $pendUrlAttach .= (string) $dlrPos680[$il] . '.';
               }
               $pendUrlAttach = substr($pendUrlAttach, 0, -1);
               $pendUrlAttach = ("'" . $pendUrlAttach . "'");

               $deliUrlAttach = '';
               for ($il = 0; $il < count($dlrPos681); $il++) {
                  $deliUrlAttach .= (string) $dlrPos681[$il] . '.';
               }
               $deliUrlAttach = substr($deliUrlAttach, 0, -1);
               $deliUrlAttach = ("'" . $deliUrlAttach . "'");

               $failUrlAttach = '';
               for ($il = 0; $il < count($dlrPos682); $il++) {
                  $failUrlAttach .= (string) $dlrPos682[$il] . '.';
               }
               $failUrlAttach = substr($failUrlAttach, 0, -1);
               $failUrlAttach = ("'" . $failUrlAttach . "'");

               $rejeUrlAttach = '';
               for ($il = 0; $il < count($dlrPosREJ); $il++) {
                  $rejeUrlAttach .= (string) $dlrPosREJ[$il] . '.';
               }
               $rejeUrlAttach = substr($rejeUrlAttach, 0, -1);
               $rejeUrlAttach = ("'" . $rejeUrlAttach . "'");

               if ($dlrArr680['5']['buckets'][$z]['dlr']['buckets'] != 0) {
                  echo '"' . number_format($dlrArr680['5']['buckets'][$z]['dlr']['buckets'], 0, '.', ' ') . '",';
               } else {
                  echo '"0",';
               }

               if ($dlrArr681['5']['buckets'][$z]['dlr']['buckets'] != 0) {
                  echo '"' . number_format($dlrArr681['5']['buckets'][$z]['dlr']['buckets'], 0, '.', ' ') . '",';
               } else {
                  echo '"0",';
               }

               if ($dlrArr682['5']['buckets'][$z]['dlr']['buckets'] != 0) {
                  echo '"' . number_format($dlrArr682['5']['buckets'][$z]['dlr']['buckets'], 0, '.', ' ') . '",';
               } else {
                  echo '"0",';
               }

               if ($dlrArrREJ['5']['buckets'][$z]['dlr']['buckets'] != 0) {
                  echo '"' . number_format($dlrArrREJ['5']['buckets'][$z]['dlr']['buckets'], 0, '.', ' ') . '",';
               } else {
                  echo '"0",';
               }

               $cnn = 0;
               foreach ($allStats2 as $key7 => $value7) {
                  if ($key7 = 'aggregations' && isset($value7['5']['buckets'])) {
                     if (count($value7['5']['buckets']) > 0) {
                        for ($y = 0; $y < count($value7['5']['buckets']); $y++) {
                           if ($value7['5']['buckets'][$y]['key_as_string'] == $value['5']['buckets'][$z]['key_as_string']) {
                              $cnn++;
                              echo '"' . number_format($value7['5']['buckets'][$y]['3']['buckets']['0']['doc_count'], 0, '.', ' ') . '"';
                           }
                        }
                     }
                  }
               }
               if ($cnn == 0) {
                  echo '"0"';
               }
               echo '],';
            }
         }
      }
   }
}
?>
      ];
      data.forEach(function (infoArray, index)
      {
         dataString = infoArray.join(",");
         csvContent += index < data.length ? dataString + "\n" : dataString;
      });
      var encodedUri = encodeURI(csvContent);
      var link = document.createElement("a");
      link.setAttribute("href", encodedUri);
      link.setAttribute("download", "<?php echo 'APITrafficSummary_' . $startDate . '-' . $endDate; ?>.csv");

      link.click();
   }

   function reloadOnSelect(name)
   {
      $(".loader").fadeIn("slow");
      $(".loaderIcon").fadeIn("slow");
      var serviceName = name;

      //var repRange1 = $("#reportrange span").html();
      var repRange = $("#reportrange span").html();

      var start = moment(repRange.substr(0, 10)).unix() * 1000;
      //alert('start:' + start);
      //var end = moment(repRange.substr(13, 23)).unix()*1000;
      var end = moment(repRange.substr(13, 23)).add(1, 'day');//
      end = (end.unix() - 1) * 1000;
      repRange = start + ' - ' + end;
      //alert(repRange1);

      /*var start = moment(repRange1).unix()*1000;
       var end = moment(repRange1).add(1, 'day');//
       end = (end.unix()-1)*1000;*/
      encodeURI(serviceName);
      window.location = "../pages/apitraffic.php?client=" + encodeURIComponent(serviceName) + "&range=" + repRange;
   }

   function viewMsg(dater)
   {
      //alert(dater);
      $(".loader").fadeIn("slow");
      $(".loaderIcon").fadeIn("slow");
      var serviceName = document.getElementById("client").value;

      var repRange1 = $("#reportrange span").html();
      var repRange = dater;
      var start = moment(repRange).unix() * 1000;
      var end = moment(repRange).add(1, 'day');//
      end = (end.unix() - 1) * 1000;
      repRange = start + ' - ' + end;
      //window.location = "../pages/apitrafficmsg.php?client=" + serviceName + "&range=" + repRange;
      encodeURI(serviceName);
      window.location = "../pages/apitrafficmsg.php?client=" + encodeURIComponent(serviceName) + "&range=" + repRange + "&range1=" + repRange1 + "&type=Sent&sents=" + <?php echo $sents; ?> + "&from=1&controller=1&sort=<?php echo $sort; ?>";
   }

   function viewMsgVar(val, dater, t)
   {
      $(".loader").fadeIn("slow");
      $(".loaderIcon").fadeIn("slow");
      var serviceName = document.getElementById("client").value;

      //alert(val);
      var repRange1 = $("#reportrange span").html();
      var repRange = dater;
      //var t = ty;
      var start = moment(repRange).unix() * 1000;
      var end = moment(repRange).add(1, 'day');//
      end = (end.unix() - 1) * 1000;
      repRange = start + ' - ' + end;
      encodeURI(serviceName);
      window.location = "../pages/apitrafficmsg.php?client=" + encodeURIComponent(serviceName) + "&range=" + repRange + "&range1=" + repRange1 + "&type=" + t + "&sents=" + <?php echo $sents; ?> + "&from=1&controller=1&sort=<?php echo $sort; ?>&query=" + val;
   }

   function gotoMo(dater, sId, total)
   {
      $(".loader").fadeIn("slow");
      $(".loaderIcon").fadeIn("slow");

      var repRange1 = $("#reportrange span").html();
      //var end = dater + (24*60*60) - 1;//
      //alert(dater + ' - ' + end);
      //var repRange = dater;
      var prevPage = "apitraffic";

      window.location = "../pages/apimomsg.php?range=" + dater + "&range1=" + repRange1 + "&prevPage=" + prevPage + "&sId=" + sId + "&total=" + total;
   }
</script>

<script type="text/javascript">
   $(window).load(function () {
      $(".loader").fadeOut("slow");
      $(".loaderIcon").fadeOut("slow");
   })
</script>

<!-- Template Footer -->

