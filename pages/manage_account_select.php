<?php
   /**
    * Include the header template which sets up the HTML
    *
    * Don't forget to include template_import_script before any Javascripting
    * Don't forget to include template footer.php at the bottom of the page as well
    */
   //-------------------------------------------------------------
   // Template
   //-------------------------------------------------------------
   TemplateHelper::setPageTitle('Manage Accounts');
   TemplateHelper::initialize();

   //-------------------------------------------------------------
   // MANAGE ACCOUNTS
   //-------------------------------------------------------------
   //get all the account for this page

?>

<aside class="right-side">
   <section class="content-header">
      <h1>
         Select An Account
      </h1>
   </section>

   <section class="content">
      <?php
         //include the top nav
         $nav_section = "accounts";
         include('modules/module_admin_nav.php');
      ?>

      <!-- SEARCH BAR AND ADD BUTTON -->
      <div class="row">
         <div class="col-lg-9">
            <!-- SEARCH BAR -->
            <div class="input-group">
               <input type="text" class="form-control" id="inputSearchAccount" placeholder="Type to filter accounts..." aria-label="">
               <div class="input-group-btn">
                  <button type="button" class="btn btn-default dropdown-toggle" id="btnSearchType" search_type="all" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                     <span id="btnSearchTypeString">Search All</span> <span class="caret"></span>
                  </button>
                  <ul class="dropdown-menu dropdown-menu-right">
                     <li><a class="search-type" search_type="all" search_type_string="Search All" href="#">Search All</a></li>
                     <li><a class="search-type" search_type="account_id" search_type_string="Account ID" href="#">Account ID</a></li>
                     <li><a class="search-type" search_type="account_name" search_type_string="Account Name" href="#">Account Name</a></li>
                  </ul>
               </div>
            </div>
         </div>
         <div class="col-lg-3">
            <button class="btn btn-primary btn-block" disabled="true"><span class="fa fa-plus" aria-hidden="true"></span> Create A New Account</button>
         </div>
      </div>
      <hr/>

      <p>Please select a account to manage.</p>
      <div class="row">
         <div class="col-lg-1">
            <p style="margin-left:10px;"><strong>Account ID</strong></p>
         </div>
         <div class="col-lg-8">
            <p style="margin-left:10px;"><strong>Account Name</strong></p>
         </div>
         <div class="col-lg-2" style="text-align:right;">
            <p style="margin-right:10px;"><strong>Default Logo</strong></p>
         </div>
      </div>
      <div class="row">
         <div class="col-lg-12">
            <!-- THE LIST OF ACCOUNTS ON THIS PAGE -->
            <div class="list-group" id="listAccountsHolder">
               <a href="#" class="list-group-item" style="text-align:center;" id="listItemLoading">
                  <p class="list-group-item-text"><span class="glyphicon glyphicon-refresh glyphicon-spin"></span> Loading accounts...</p>
               </a>
            </div>
         </div>
      </div>
   </section>
</aside>


<!--  THE JAVASCRIPT IMPORT ALWAYS APPEARS BEFORE LOCAL JAVASCRIPT AT THE BOTTOM OF THE PAGE -->
<?php include("template_import_script.php"); //must import all scripts first   ?>
<!-- END JAVASCRIPT IMPORT -->

<script type="text/javascript">

   var page = 1;
   var limit = 25;
   var searchType = "all";
   var searchTerm = "";
   var loadingListItem = null;
   var isLoading = false;
   var noMoreAccounts = false;

   $(document).ready(function (e)
   {
      loadingListItem = $('#listItemLoading');

      //load the initial accounts
      loadAccountListViaAjax();

      //hide the loading popup if it is showing
      $(".loader").fadeOut("fast");
      $(".loaderIcon").fadeOut("fast");

      /***************************************
       * Listener for changing the search type
       ***************************************/
      $(".search-type").on("click", function (e)
      {
         e.preventDefault();
         setSearchType($(this).attr('search_type'), $(this).attr('search_type_string'));
      });

      /***************************************
       * Listener for changing the search type
       ***************************************/
      $("#inputSearchAccount").keyup(function (e)
      {
         e.preventDefault();
         searchTerm = $(this).val();
         showListLoading();
         keyDelay(function() { runSearchForAccount(); }, 400);
      });

      /***************************************
       * Listener for changing the search type
       ***************************************/
      $("#inputSearchAccount").mouseup(function (e)
      {
         e.preventDefault();
         searchTerm = $(this).val();
         showListLoading();
         keyDelay(function() { runSearchForAccount(); }, 400);
      });

      /*************************************
       * For loading more accounts
       * This listener checks scroll position and starts loading more items
       *
       *************************************/
      $(window).scroll(function()
      {
         if(!noMoreAccounts && !isLoading && ($(window).scrollTop() + 80) >= $(document).height() - $(window).height())
         {
            isLoading = true;
            page++;
            console.log("Loading page : " + page);

            loadAccountListViaAjax();
         }
      });

   });

   function reloadOnSelect(serviceId)
   {
      $(".loader").fadeIn("slow");
      $(".loaderIcon").fadeIn("slow");
      window.location = "contactlists.php?service_id=" + serviceId;
   }

   //this function grabs the search variables and refreshes the page to run a search
   function runSearchForAccount()
   {
      noMoreAccounts = false;

      //remove the loading element and clear all the accounts, then show loading
      loadingListItem.detach();
      $('#listAccountsHolder').empty();
      loadingListItem.appendTo('#listAccountsHolder');

      //reset the page number as we are now doing a search
      page = 1;

      loadAccountListViaAjax();
   }

   function setSearchType(type, typeString)
   {
      var doSearch = false;
      if(type != searchType && searchTerm != "")
      {
         doSearch = true;
      }
      searchType = type;
      $("#btnSearchTypeString").html(typeString);

      if(doSearch)
      {
         runSearchForAccount();
      }
   }




   /*************************************
    * This function uses the current properties of the page,
    * such as "page" number, search terms, and loads the Account list dyamically.
    *
    *************************************/
   function loadAccountListViaAjax()
   {
      //run our ajaxController to save the details
      $.post("../php/ajaxAdminGetAccountList.php", {page: page, limit: limit, search_type:searchType, search_term:searchTerm}).done(function (data) {
         try
         {
            var json = $.parseJSON(data);

            if (json.success)
            {
               if(json.is_search == true && json.page == 0 && json.accounts.length == 0)
               {
                  noMoreAccounts = true;
                  showNoResults();
               }
               else if(json.accounts.length == 0)
               {
                  noMoreAccounts = true;
                  showNoMoreAccounts();
               }
               else if(json.accounts.length < limit)
               {
                  addAccountsToList(json.accounts);
                  showNoMoreAccounts();
               }
               else
               {

                  addAccountsToList(json.accounts);
               }
            }
            else
            {
               showLoadingFailure();
            }

         }
         catch (err)
         {
            showLoadingFailure();
            console.log("Could not load list: " + err.message);

         }

         isLoading = false;

      }).fail(function (data) {
         showLoadingFailure();
      });
   }

   /*************************************
    * This function takes the dynamically
    * loaded json accounts and populates the list
    *
    *************************************/
   function addAccountsToList(accounts)
   {
      //remove the loading element and add the accounts
      loadingListItem.detach();

      clearListMessages();

      for(var i = 0; i < accounts.length; ++ i)
      {
         var account = accounts[i];

         var headerLogo = '/img/skinlogos/1/1_header.png';
         if(account.header_logo != "" && account.header_logo != null)
         {
            headerLogo = account.header_logo;
            console.log("Header logo: " + account.header_logo);
         }

         //could do with an actual tempalting language here, but for the sake of time, we have not
         $('#listAccountsHolder').append('<a href="manage_account.php?account_id=' + account.account_id + '" class="list-group-item"'
            + ' data-toggle="tooltip" data-placement="bottom" title="Click to manage this account.">'
            + '<div class="row">'
            + '<div class="col-lg-1">'
            + '<h2 class="list-group-item-heading"><strong>' + account.account_id + '</strong></h2>'
            + '</div>'
            + '<div class="col-lg-9">'
            + '<h4 class="list-group-item-heading">Account: <strong>' + account.account_name + '</strong></h4>'
            + '</div>'
            + '<div class="col-lg-2">'
             + '<img src="' + headerLogo + '" class="img-responsive" alt="Account Logo"/>'
            + '</div>'
            + '</div>'
            + '</a>');

      }

      loadingListItem.appendTo('#listAccountsHolder');

      //refresh tooltips
      $('[data-toggle="tooltip"]').tooltip();
   }

   function clearListMessages()
   {
      $('#listItemNoResults').remove();
      $('#listItemEnd').remove();
      $('#listItemError').remove();
   }

   function showNoResults()
   {
      clearListMessages();

      //remove the loading element and show no results
      loadingListItem.detach();
      $('#listAccountsHolder').append('<div id="listItemNoResults" class="list-group-item text-warning" style="text-align:center;"> No results match your query.</div>');
   }

   function showNoMoreAccounts()
   {
      clearListMessages();

      //remove the loading element and show no results
      loadingListItem.detach();
      $('#listAccountsHolder').append('<div id="listItemEnd" class="list-group-item" style="text-align:center;"> End of account list.</div>');
   }

   function showLoadingFailure()
   {
      clearListMessages();

      //remove the loading element and show no results
      loadingListItem.detach();
      $('#listAccountsHolder').append('<div id="listItemError" class="list-group-item text-danger" style="text-align:center;"> Could not load anymore results, please check your internet connection.</div>');
   }

   var keyDelay = (function(){
      var timer = 0;
      return function(callback, ms){
         clearTimeout (timer);
         timer = setTimeout(callback, ms);
      };
   })();

   function showListLoading()
   {
      //remove the loading element and clear all the services, then show loading
      loadingListItem.detach();
      $('#listAccountsHolder').empty();
      loadingListItem.appendTo('#listAccountsHolder');
   }

</script>

<!-- Template Footer -->

