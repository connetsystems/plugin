<?php
/**
 * Include the header template which sets up the HTML
 *
 * Don't forget to include template_import_script before any Javascripting
 * Don't forget to include template footer.php at the bottom of the page as well
 */
$headerPageTitle = "Assign Roles"; //set the page title for the template import
TemplateHelper::initialize();


$userId = 0;
$showRoles = 'none;';
$users = getAllUsers();

if (isset($_GET['addId'])) {
   addRoleToUser($_GET['uId'], $_GET['addId']);
}

if (isset($_GET['removeId'])) {
   removeRoleFromUser($_GET['uId'], $_GET['removeId']);
}

if (isset($_GET['user'])) {
   $userId = substr($_GET['user'], (strpos($_GET['user'], ' - ') + 3), strlen($_GET['user']));

   //echo '<br>uId ='.$userId.'=';

   $assignedRoles = getRoles($userId);
   $unassignedRoles = getUnassignedRoles($userId);
   /* foreach ($unassignedRoles as $key => $value) 
     {
     echo '<pre>';
     print_r($value);
     echo '</pre>';
     } */

   $showRoles = 'block;';
}

if (isset($_GET['uId'])) {
   $userId = $_GET['uId'];
   $assignedRoles = getRoles($userId);
   $unassignedRoles = getUnassignedRoles($userId);

   $showRoles = 'block;';
}
?>

<aside class="right-side">
   <section class="content-header">
      <h1>
         Assign Roles
         <!--small>Control panel</small-->
      </h1>
   </section>

   <section class="content">
      <div class="row col-lg-12">
         <div class="box box-solid" style="height:66px; margin-top:-15px;display:<?php echo $singleView; ?>">
            <form action="updateservice.php" method="get" id="clientList" class="sidebar-form" style="border:0px;">
               <div class="form-group">
                  <label>Client List</label>
                  <select class="form-control" name="client" form="clientList" OnChange="reloadOnSelect(this.value);">
                     <?php
                     if ($userId == '0') {
                        echo '<option SELECTED>Please select a client...</option>';
                     }
                     foreach ($users as $key => $value) {
                        $uId = $value['user_id'];
                        $userName = $value['user_username'];
                        if ($userId == $uId) {
                           //$serviceName = $value['service_name'];
                           echo "<option name='" . $uId . "' SELECTED>" . $userName . " - " . $uId . "</option>";
                        } else {
                           echo "<option name='" . $uId . "'>" . $userName . " - " . $uId . "</option>";
                           //echo "<option name='".$sId."'>".$value['account_name']." - ".$value['service_name']." - ".$sId."</option>";
                        }
                     }
                     ?>
                  </select>
               </div>
               <br>
            </form>
         </div>

         <div class="box box-connet" style="padding-bottom:1px; border-top-color:<?php echo $accRGB; ?>; height:auto; display:<?php echo $showRoles; ?>" >
            <div class="box-header">
               <h3 class="box-title">Available Roles:</h3>
            </div>
            <div class="box-body">
               <table id="example2" class="table table-bordered table-striped dataTable table-hover">
                  <thead>
                     <tr>

                        <th>Role Icon</th>
                        <th>Role Name</th>
                        <th>Role URL</th>
                        <th>Type</th>
                        <th>Assign</th>

                     </tr>
                  </thead>

                  <tbody>
                     <!--form action="newresellerroutes.php" method="get" id="resellerRouteRates" name="resellerRouteRates" class="sidebar-form" style="border:0px;"-->
                     <?php
                     $numId = 0;
//$resellerRouteRates = "'resellerRouteRates'";
//echo '<form action="newresellerroutes.php" method="get" id="resellerRouteRates" class="sidebar-form" style="border:0px;">';
                     if (isset($unassignedRoles)) {
                        foreach ($unassignedRoles as $key => $value) {
                           $v = $value['role_id'] % 100;

                           if ($v == 0) {
                              $color = '#dedede;';
                           } else {
                              $color = '#f9f9f9;';
                           }

                           echo '<tr name="rollId" value="' . $value['role_id'] . '">';
                           //echo '<td style="background-color:'.$color.';">'.$value['role_id'].' ='.substr($value['role_id'], -1).'=</td>';
                           echo '<td style="background-color:' . $color . ';"><i class="' . $value['role_icon'] . '"></i>&nbsp;&nbsp;' . $value['role_icon'] . '</td>';
                           echo '<td style="background-color:' . $color . ';">' . $value['role_name'] . '</td>';
                           echo '<td style="background-color:' . $color . ';">' . $value['role_url'] . '</td>';
                           echo '<td style="background-color:' . $color . ';">' . $value['role_type'] . '</td>';
                           if ($v != 0) {
                              echo '<td style="background-color:' . $color . ';">
                                                                        <center>
                                                                                <a class="btn btn-block btn-social btn-dropbox" name="' . $value['role_id'] . '" onclick="addRole(' . $value['role_id'] . ')" style="background-color:' . $accRGB . '; color: #ffffff; border: 2px solid; border-radius: 6px !important; font-size:0.8em !important; height:24px; width: 120px; margin-top:0px; padding-top:2px;">
                                                                                    <i class="fa fa-plus" style="font-size:0.8em !important; margin-top:-6px;"></i>Assign Role
                                                                                </a>
                                                                        </center>
                                                                  </td>';
                           } else {
                              echo '<td style="background-color:' . $color . ';"></td>';
                           }
                           echo '</tr>';
                        }
                     }
//echo '</form>';
                     ?>
                     <!--/form-->
                  </tbody>
               </table>
            </div>
         </div>

         <div class="box box-connet" style="padding-bottom:1px; border-top-color:<?php echo $accRGB; ?>; height:auto; display:<?php echo $showRoles; ?>" >
            <div class="box-header">
               <h3 class="box-title">Assigned Roles:</h3>
            </div>

            <div class="box-body" style="padding:0px;">
               <div class="box-body table-responsive">
                  <table id="example2" class="table table-bordered table-striped dataTable table-hover">
                     <thead>
                        <tr>

                           <th>Role Icon</th>
                           <th>Role Name</th>
                           <th>Role URL</th>
                           <th>Remove</th>

                        </tr>
                     </thead>

                     <tbody>
                        <?php
                        if (isset($assignedRoles)) {
                           //uasort($assignedRoles);
                           $numId = 0;

                           foreach ($assignedRoles as $key => $value) {
                              $v = $value['role_id'] % 100;

                              if ($v == 0) {
                                 $color = '#dedede;';
                              } else {
                                 $color = '#f9f9f9;';
                              }

                              echo '<tr name="rollId" value="' . $value['role_id'] . '">';
                              //echo '<td style="background-color:'.$color.';">'.$value['role_id'].' ='.substr($value['role_id'], -1).'=</td>';
                              echo '<td style="background-color:' . $color . ';"><i class="' . $value['role_icon'] . '"></i>&nbsp;&nbsp;' . $value['role_icon'] . '</td>';
                              echo '<td style="background-color:' . $color . ';">' . $value['role_name'] . '</td>';
                              echo '<td style="background-color:' . $color . ';">' . $value['role_url'] . '</td>';
                              if ($v != 0) {
                                 echo '<td style="background-color:' . $color . ';">
                                                                    <center>
                                                                            <a class="btn btn-block btn-social btn-dropbox" name="' . $value['role_id'] . '" onclick="removeRole(' . $value['role_id'] . ')" style="background-color:' . $accRGB . '; color: #ffffff; border: 2px solid; border-radius: 6px !important; font-size:0.8em !important; height:24px; width: 120px; margin-top:0px; padding-top:2px;">
                                                                                <i class="fa fa-minus" style="font-size:0.8em !important; margin-top:-6px;"></i>Remove Role
                                                                            </a>
                                                                    </center>
                                                              </td>';
                              } else {
                                 echo '<td style="background-color:' . $color . ';"></td>';
                              }
                              echo '</tr>';
                              /* $logo = strstr($value['NETWORK'], ' (', true);

                                echo '<td style="vertical-align:middle;"><img src="../img/flags/'.$value['COUNTRY'].'.png">&nbsp;&nbsp;'.$value['COUNTRY'].'</td>';
                                echo '<td style="vertical-align:middle;"><img src="../img/serviceproviders/'.$logo.'.png">&nbsp;&nbsp;'.$value['NETWORK'].'</td>';
                                if($value['STATUS'] == 'ENABLED')
                                {
                                echo '<td style="vertical-align:middle;"><center><i class="fa fa-check" style="color:#00ff00;"></i>&nbsp;&nbsp;'.$value['STATUS'].'</center></td>';
                                }
                                else
                                {
                                echo '<td style="vertical-align:middle;"><center><i class="fa fa-times" style="color:#ff0000;"></i>&nbsp;&nbsp;'.$value['STATUS'].'</center></td>';
                                }
                                echo '<td style="vertical-align:middle;"><center><div style="display: inline-table;"></div>'.$value['RATE'].'</div></center></td>';

                                echo '<td style="vertical-align:middle;">
                                <center>
                                <a class="btn btn-block btn-social btn-dropbox" name="'.$value['route_id'].'" onclick="removeRate('.$value['route_id'].')" style="background-color:'.$accRGB.'; color: #ffffff; border: 2px solid; border-radius: 6px !important; width: 140px; margin-top:0px;">
                                <i class="fa fa-minus"></i>Remove Route
                                </a>
                                </center>
                                </td>';

                                $numId++; */
                           }
                        }
                        ?>
                     </tbody>
                  </table>
                  <br>
                  <?php
                  if ($numId != 0) {
                     echo '<a class="btn btn-block btn-social btn-dropbox" name="" onclick="gotoAddUsers(' . $sId . ')" style="background-color:' . $accRGB . '; color: #ffffff; border: 2px solid; border-radius: 6px !important; width: 140px; margin-top:0px;">';
                     echo '<i class="fa fa-users"></i>Add Users';
                     echo '</a>';
                  }
                  ?>
               </div>
            </div>
         </div>
   </section>

</aside>

<!--  THE JAVASCRIPT IMPORT ALWAYS APPEARS BEFORE LOCAL JAVASCRIPT AT THE BOTTOM OF THE PAGE -->
<?php include("template_import_script.php"); //must import all scripts first ?>
<!-- END JAVASCRIPT IMPORT -->

<script type="text/javascript">

   function addRole(rId)
   {
      //alert(' rId=' + rId);
      window.location = "assignroles.php?uId=" + <?php echo $userId; ?> + "&addId=" + rId;
   }
   ;

   function removeRole(rId)
   {
      //alert(' rId=' + rId);
      window.location = "assignroles.php?uId=" + <?php echo $userId; ?> + "&removeId=" + rId;
   }
   ;

   function reloadOnSelect(name)
   {
      $(".loader").fadeIn("slow");
      $(".loaderIcon").fadeIn("slow");
      window.location = "assignroles.php?user=" + name;
   }
</script>

<script type="text/javascript">
   $(window).load(function () {
      $(".loader").fadeOut("slow");
      $(".loaderIcon").fadeOut("slow");
   })
</script>

<!-- Template Footer -->
