<?php
/**
 * Include the header template which sets up the HTML
 *
 * Don't forget to include template_import_script before any Javascripting
 * Don't forget to include template footer.php at the bottom of the page as well
 */
//-------------------------------------------------------------
// Template
//-------------------------------------------------------------
TemplateHelper::setPageTitle('Network Traffic');
TemplateHelper::initialize();

$singleView = 'block;';


//-------------------------------------------------------------
// Permissions
//-------------------------------------------------------------
$clients = PermissionsHelper::getAllServicesWithPermissions();

if (isset($_GET['service_id'])) {


   $_service_id = (integer) $_GET['service_id'];

   if ($_service_id != -1) {
      if (!isset($clients[$_service_id])) {

         header('location: ' . $_SERVER['DOCUMENT_URI']);
         die();
      }
   }
} elseif (isset($_GET['client'])) {

   $split = explode(' ', trim($_GET['client']));
   header('location: ' . $_SERVER['DOCUMENT_URI'] . '?service_id=' . (integer) end($split));
   die();
}

if (count($clients) == 1) {

   $_GET['service_id'] = LoginHelper::getCurrentServiceId();
   $singleView = 'none;';

} elseif (!isset($_GET['service_id'])) {

   $_GET['service_id'] = -1;
}

//-------------------------------------------------------------
// Datepicker
//-------------------------------------------------------------
if (isset($_GET['range'])) {
   $dateArr = explode(' - ', $_GET['range']);
   $startDate = $dateArr[0];
   $endDate = $dateArr[1];
   $endDateSearch = date('Y-m-d', strtotime($dateArr[1] . ' +1 day')); //elasticsearch includes the "from" date in the aggregation, but not the "to"
} else {
   $startDate = date('Y-m-d');
   $endDate = date('Y-m-d');
   $endDateSearch = date('Y-m-d', strtotime(date('Y-m-d') . ' +1 day')); //elasticsearch includes the "from" date in the aggregation, but not the "to"
}

if (isset($_GET['service_id']) && $_GET['service_id'] != -1) {
   $fileName = $_GET['service_id'];
   $service_id = $_GET['service_id'];
} else {
   $fileName = 'Show All';
   $service_id = -1;
}

//assemble the unix dates for querying
$startDateUnix = strtotime($startDate) * 1000;
$endDateUnix = strtotime($endDate) * 1000;
$endDateUnix += (23 * 59 * 59 * 1000);

//our holder for all the services
$all_service_data = array();
if (LoginHelper::isSystemAdmin())
{
   if($service_id != -1)
   {
      //user has selected one service to view
      $service_stats = Service::getFullNetworkSendStats($service_id, $startDate, $endDateSearch);
      array_push($all_service_data, $service_stats);
   }
}
elseif (LoginHelper::isResellerService() && $service_id == -1) {
   //show all services
   foreach ($clients as $service)
   {

      $service_stats = Service::getFullNetworkSendStats($service['service_id'], $startDate, $endDateSearch);
      //echo json_encode($service_stats).'<br/><br/>';
      array_push($all_service_data, $service_stats);
   }
} elseif (!LoginHelper::isResellerService()) {

   //user has selected one service to view
   $service_stats = Service::getFullNetworkSendStats($_SESSION['serviceId'], $startDate, $endDateSearch);
   array_push($all_service_data, $service_stats);
}
else
{
   //user has selected one service to view
   $service_stats = Service::getFullNetworkSendStats($service_id, $startDate, $endDateSearch);
   array_push($all_service_data, $service_stats);
}
?>
<aside class="right-side">
   <section class="content-header">
      <h1>
         Network Traffic
         <!--small>Control panel</small-->
      </h1>
   </section>

   <section class="content" style="padding-bottom:0px;margin-bottom:2px;margin-top:1px;">
      <div class="form-group" style="margin-bottom:2px;;margin-top:1px;">
         <div class="box box-solid" style="margin-bottom:0px;display:block; padding-top:1px;padding-bottom:1px;">
            <form action="updateservice.php" method="get" id="clientList" class="sidebar-form" style="border:0px;margin-bottom:0px;">
               <div class="callout callout-info" style="margin-bottom:0px;">
                  <h4>Network Traffic Report Instructions / Details</h4>
                  <p>This page will show you ALL NETWORK traffic for the date range that you specify in the calendar below.</p>
               </div>
               <div class="callout callout-warning">
                  <h4>Note</h4>
                  <p>The total cost are approximations only. The actual cost may vary slightly.</p>
                  <div style="float:right; margin-top:-42px;margin-right: 10px;">
                     <i class="fa fa-exclamation-triangle" style="color:#d1ba8a; font-size:20pt;"></i>
                  </div>
               </div>
               <div id="reportrange" class="select pull-left" onmouseover="" style="cursor: pointer;">
                  <i class="fa fa-calendar fa-lg"></i>
                  <span style="font-size:15px"><?php echo $startDate . " - " . $endDate; ?></span><b class="caret"></b>
               </div>
               <br>
               <div class="form-group" style="display:<?php echo $singleView; ?>;margin-bottom:0px;">
                  <br>
                  <label>Client List</label>
                  <select class="form-control" name="client" id="client" form="clientList" OnChange="reloadOnSelect(this.value);">
                     <?php
                     echo '<option value="-1" SELECTED>Show All</option>';
                     foreach ($clients as $key => $service) {
                        if ($service_id == $service['service_id']) {
                           echo "<option value='" . $service['service_id'] . "' SELECTED>" . $service['account_name'] . " - " . $service['service_name'] . " - " . $service['service_id'] . "</option>";
                        } else {
                           echo "<option value='" . $service['service_id'] . "'>" . $service['account_name'] . " - " . $service['service_name'] . " - " . $service['service_id'] . "</option>";
                        }
                     }
                     ?>
                  </select>
               </div>
               <br>
            </form>
         </div>
      </div>
   </section>

   <section class="content">
      <div class="box box-connet" style="padding-bottom:1px; border-top-color:<?php echo $accRGB; ?>;">
         <div class="box-header">
            <h3 class="box-title">Network Traffic Total</h3>
         </div><!-- /.box-header -->
         <div class="box-body table-responsive">
            <table id="example2" class="table table-bordered table-striped dataTable table-hover">
               <thead>
                  <tr>
                     <th>Service</th>
                     <th>Network</th>
                     <th>Country</th>
                     <th style="text-align:right; width:100px;">Sends&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</th>
                     <th style="text-align:right; width:100px;">Units&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</th>
                     <th style="text-align:right; width:100px;">Cost per Unit&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</th>
                     <th style="text-align:right; width:100px;">Total Cost&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</th>
                  </tr>
               </thead>
               <tbody>
                  <?php
                  $total_sends = 0;
                  $total_units = 0;
                  $total_cost = 0;

                  foreach ($all_service_data as $service_data) {
                     foreach ($service_data['networks'] as $network_data) {
                        //get some info
                        $service_name = getServiceName($service_data['service_id']);
                        $network_name = getNetFromMCC($network_data['mccmnc']);
                        $country_name = getCountryFromMCC($network_data['mccmnc']);
                        $currency = "?";

                        $flag = str_replace(' / ', '', $country_name);
                        if ($flag == '') {
                           $flag = 'Special';
                        }

                        echo '<tr>';
                        //show service name
                        echo '<td style="vertical-align: middle;">' . $service_name . ' (' . $service_data['service_id'] . ')</td>';

                        //show network name
                        if (strpos($network_name, 'Special')) {
                           echo '<td style="vertical-align: middle;"><img src="../img/serviceproviders/Special.png">&nbsp;&nbsp;' . $network_name . ' (' . $network_data['mccmnc'] . ')' . '</td>';
                        } else {
                           echo '<td><img src="../img/serviceproviders/' . trim(substr($network_name, strpos($network_name, "/") + 1)) . '.png">&nbsp;&nbsp;' . $network_name . ' (' . $network_data['mccmnc'] . ')' . '</td>';
                        }

                        echo '<td style="vertical-align: middle;"><img src="../img/flags/' . $flag . '.png">&nbsp;&nbsp;' . $country_name . '</td>';

                        $send_array_holder = array();
                        $unit_array_holder = array();
                        $rate_array_holder = array();
                        $cost_array_holder = array();
                        $total_sends_this_network = 0;
                        $total_units_this_network = 0;
                        $total_cost_this_network = 0;

                        //now cycle through any potential rates, as a certain date range may feature a rate change, in which case there will be multiple records
                        foreach ($network_data['rates'] as $rate_data) {
                           array_push($send_array_holder, $rate_data['total_sends']);
                           array_push($unit_array_holder, $rate_data['total_units']);
                           array_push($rate_array_holder, number_format(($rate_data['rate'] / 10000), 2, '.', ' ') . '');
                           array_push($cost_array_holder, number_format(($rate_data['total_cost'] / 10000), 2, '.', ' ') . '');
                           $unit_str_holder = $rate_data['total_units'] . '<br/>';
                           $cost_str_holder = number_format(($rate_data['total_cost'] / 10000), 2, '.', ' ') . '<br/>';

                           $total_sends_this_network += $rate_data['total_sends'];
                           $total_units_this_network += $rate_data['total_units'];
                           $total_cost_this_network += $rate_data['total_cost'];

                           //some totals tracking
                           $total_sends += $rate_data['total_sends'];
                           $total_units += $rate_data['total_units'];
                           $total_cost += $rate_data['total_cost'];
                        }

                        //for total send display
                        if (count($send_array_holder) > 1) {
                           echo '<td align="right">';
                           echo implode('<br/>', $send_array_holder) . '<br/>';
                           echo '<span style="text-decoration: overline;">(<strong>Total: </strong>' . $total_sends_this_network . ')</span>';
                           echo '</td>';
                        } else {
                           echo '<td align="right">' . $total_sends_this_network . '</td>';
                        }

                        //for total send display
                        if (count($unit_array_holder) > 1) {
                           echo '<td align="right">';
                           echo implode('<br/>', $unit_array_holder) . '<br/>';
                           echo '<span style="text-decoration: overline;">(<strong>Total: </strong>' . $total_units_this_network . ')</span>';
                           echo '</td>';
                        } else {
                           echo '<td align="right">' . $total_units_this_network . '</td>';
                        }

                        //SHOW THE RATES
                        echo '<td align="right">' . implode('<br/>', $rate_array_holder) . '</td>';

                        //for total cost display
                        if (count($cost_array_holder) > 1) {
                           echo '<td align="right">';
                           echo implode('<br/>', $cost_array_holder) . '<br/>';
                           echo '<span style="text-decoration: overline;">(<strong>Total: </strong>' . number_format(($total_cost_this_network / 10000), 2, '.', ' ') . ')</span>';
                           echo '</td>';
                        } else {
                           echo '<td align="right">' . number_format(($total_cost_this_network / 10000), 2, '.', ' ') . '</td>';
                        }
                        echo '</tr>';
                     }
                  }
                  ?>
               </tbody>
               <tfoot>
                  <tr>
                     <th></th>
                     <th></th>
                     <th></th>
                     <th style="text-align:right;" id="totalSendsCell"><?php echo $total_sends; ?></th>
                     <th style="text-align:right;" id="totalUnitsCell"><?php echo $total_units; ?></th>
                     <th style="text-align:right;"> - </th>
                     <th style="text-align:right;" id="totalCostCell"><?php echo number_format($total_cost / 10000, 2, '.', ' '); ?></th>
                  </tr>
               </tfoot>
            </table>
         </div><!-- /.box-body -->
         <div class="box-body">
            <div class="form-group">
               <a href="../php/createCSVNetworkTraffic.php?service_id=<?php echo $service_id; ?>&start_date=<?php echo $startDate; ?>&end_date=<?php echo $endDateSearch; ?>" class="btn btn-block btn-social btn-dropbox " style="background-color:<?php echo $accRGB; ?>; border: 2px solid; border-radius: 6px !important; float:left; width: 134px; margin-top:-21px;">
                  <i class="fa fa-save"></i>Save Report
               </a>
            </div>
         </div>
      </div>
   </section>
</aside>

<!--  THE JAVASCRIPT IMPORT ALWAYS APPEARS BEFORE LOCAL JAVASCRIPT AT THE BOTTOM OF THE PAGE -->
<?php include("template_import_script.php"); //must import all scripts first  ?>
<!-- END JAVASCRIPT IMPORT -->
<script type="text/javascript">
   $(function ()
   {
      $('#example2').dataTable({
         "bPaginate": true,
         "bLengthChange": false,
         "bFilter": true,
         "bSort": true,
         "bInfo": true,
         "bAutoWidth": false,
         "iDisplayLength": 100,
         "aoColumns": [
            null,
            null,
            null,
            {"sType": 'formatted-num', targets: 0},
            {"sType": 'formatted-num', targets: 0},
            {"sType": 'formatted-num', targets: 0},
            null
         ],
         "order": [[3, "desc"]],
      });
   });

   jQuery.extend(jQuery.fn.dataTableExt.oSort,
           {
              "title-numeric-pre": function (a) {
                 var x = a.match(/title="*(-?[0-9\.]+)/)[1];
                 return parseFloat(x);
              },
              "title-numeric-asc": function (a, b) {
                 return ((a < b) ? -1 : ((a > b) ? 1 : 0));
              },
              "title-numeric-desc": function (a, b) {
                 return ((a < b) ? 1 : ((a > b) ? -1 : 0));
              }
           });

   jQuery.extend(jQuery.fn.dataTableExt.oSort,
           {
              "formatted-num-pre": function (a) {
                 a = (a === "-" || a === "") ? 0 : a.replace(/[^\d\-\.]/g, "");
                 return parseFloat(a);
              },
              "formatted-num-asc": function (a, b) {
                 return a - b;
              },
              "formatted-num-desc": function (a, b) {
                 return b - a;
              }
           });

</script>
<script type="text/javascript" src="../js/plugins/daterangepicker/moment.js" ></script>

<script type="text/javascript" src="../js/plugins/daterangepicker/daterangepickerNetTraffic.js"></script>

<script type="text/javascript">
   $('#reportrange').daterangepicker(
           {
              ranges: {
                 'Today': [moment(), moment()],
                 'Yesterday': [moment().subtract('days', 1), moment().subtract('days', 1)],
                 'Last 7 Days': [moment().subtract('days', 6), moment()],
                 'Last 30 Days': [moment().subtract('days', 29), moment()],
                 'This Month': [moment().startOf('month'), moment().endOf('month')],
                 'Last Month': [moment().subtract('month', 1).startOf('month'), moment().subtract('month', 1).endOf('month')]
              },
              startDate: moment(<?php echo '"' . $startDate . '"'; ?>),
              endDate: moment(<?php echo '"' . $endDate . '"'; ?>)
           },
           function (start, end) {
              $(".loader").fadeIn("slow");
              $(".loaderIcon").fadeIn("slow");
              $('#reportrange span').html(start.format('YYYY-MM-DD') + ' - ' + end.format('YYYY-MM-DD'));
              var serviceName = document.getElementById("client").value;

              var repRange = $("#reportrange span").html();
              encodeURI(serviceName);
              window.location = "../pages/networktraffic.php?service_id=<?php echo $service_id; ?>&range=" + repRange;
           }
   );
</script>

<script type="text/javascript">
   function reloadOnSelect(serviceId)
   {
      var repRange = $("#reportrange span").daterangepicker('getDate').html();
      window.location = "../pages/networktraffic.php?service_id=" + serviceId + "&range=" + repRange;
   }
</script>

<script type="text/javascript">
   $(window).load(function () {
      $(".loader").fadeOut("slow");
      $(".loaderIcon").fadeOut("slow");
   })
</script>

<!-- Template Footer -->

