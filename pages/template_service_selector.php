<?php
if (!class_exists('PermissionsHelper', false)) {
   die('Denied');
}

$service_id = LoginHelper::getCurrentServiceId();
$permissions = PermissionsHelper::getAllPermissions();


if (isset($_GET['client'])) {

   $split = explode(' ', trim($_GET['client']));
   $selected_service_id = (integer) end($split);
} else {

   $selected_service_id = $service_id;
}

clog($service_id, $selected_service_id);
//OnChdange="window.location = '?client=' + encodeURIComponent(this.value)"
?>
<select class="form-control" id="service-selector">
   <?php foreach ($permissions as $service_id => $service) { ?>
      <option name="<?php echo htmlspecialchars($service_id); ?>" 
              <?php echo ($service_id == $selected_service_id ? 'selected=""' : ''); ?> >
                 <?php echo htmlentities(implode(' - ', array($service['account_name'], $service['service_name'], $service_id))); ?>
      </option>
   <?php } ?>
</select>
<script>

   $(function () {

      $('#service-selector').change(function () {

         if (typeof reloadOnSelect === 'function') {

            reloadOnSelect($(this).val());
         }
      });
   });

</script>