<?php
/**
 * Include the header template which sets up the HTML
 *
 * Don't forget to include template_import_script before any Javascripting
 * Don't forget to include template footer.php at the bottom of the page as well
 */
$headerPageTitle = "Update Credits"; //set the page title for the template import
TemplateHelper::initialize();

/*
  if(isset($_SESSION['serviceId']) && $_SESSION['serviceId'] != '')
  {
  $clients = getServicesInAccountWithOutRid($_SESSION['accountId']);
  }
  else
  {
  $clients = getClients();
  }
 */
/* $err1 = 0;
  $err2 = 0; */
$succ = 0;
$height = 215;

if (isset($_POST['clientName'])) {
   //echo '<br><br>C-->'.$_POST['prevAmount'];
   //echo '<br><br>A-->'.$_POST['ammount'];
   /* echo '<br><br>D-->'.$_POST['desc']; */

   $cl = strrpos($_POST['clientName'], ' ');
   $cl = substr($_POST['clientName'], $cl + 1, strlen($_POST['clientName']));

   $prevFund = getFundsOnly($cl);
   $resellerFunds = getFundsOnly($_SESSION['resellerId']);

   if (isset($_POST['ammount']) && ($_POST['ammount'] != 0) && (isset($_POST['desc']) && $_POST['desc'] != '')) {
      $amount = ($_POST['ammount'] * 10000);
      //echo '<br>AMM==='.$amount;
      if (isset($_POST['confir']) && $_POST['confir'] == 1) {
         //echo "PAYMENT SUCCESS";
         createPayment($cl, $_SESSION['userId'], $amount, $_POST['desc'], $prevFund);
         $amount = ($amount * -1);
         createPayment($_SESSION['resellerId'], $_SESSION['userId'], $amount, $_POST['desc'], $prevFund);
         $height = 72;
         $succ = 1;
      }
   }
   /* else
     {-
     $err2 = 1;
     } */

   $prevFund = getFundsOnly($cl);
   $prevFund = $prevFund / 10000;
   $prevFund = round($prevFund, 2);
} else {
   //$err1 = 1;
}

/* $sql = 'INSERT INTO `core_service_payment` (service_id, user_id, payment_value, payment_description,payment_previous_balance) 
  VALUES (:service_id, :user_id, :payment_value, :payment_description,:payment_previous_balance)';

  $sql = mysql_encode_param_array($sql, array(
  'service_id' => $selected_service->service_id,
  'user_id' => $_SESSION['auth']['user']->user_id,
  'payment_value' => $request->payment_value,
  'payment_description' => $request->description,
  'payment_previous_balance' => $selected_service->service_credit_available,
  )); */
?>

<aside class="right-side">
   <section class="content-header">
      <h1>
         Confirm Credit Payment
         <!--small>Control panel</small-->
      </h1>
   </section>

   <section class="content">
      <div class="row col-lg-12">
         <div class="box box-connet" style="height:<?php echo $height; ?>px; margin-top:-15px;display:<?php echo $singleView; ?>; border-top-color:<?php echo $accRGB; ?>;">
            <form action="confirmcredit.php" method="post" id="conff" class="sidebar-form" style="border:0px;">
               <div class="form-group">
                  <?php
                  if ($succ == 0) {
                     echo 'You are about to make a payment with the following details:';
                     echo '<div class="form-group" style="padding-left:24px;">';
                     if (isset($_POST['clientName'])) {
                        echo '<br><b>Client:</b> ' . $_POST['clientName'];
                     }
                     if (isset($_POST['prevAmount'])) {
                        echo '<br><b>Previous Amount:</b> R ' . $_POST['prevAmount'];
                     }
                     if (isset($_POST['ammount'])) {
                        echo '<br><b>Amount To Pay:</b> R ' . $_POST['ammount'];
                     }
                     if (isset($_POST['ammount']) && isset($_POST['prevAmount'])) {
                        echo '<br><b>Amount After Payment:</b> R ' . round(($_POST['ammount'] + $_POST['prevAmount']), 2);
                     }
                     if (isset($_POST['desc']) && $_POST['desc'] != '') {
                        echo '<br><b>Description:</b> ' . $_POST['desc'];
                     }
                     echo '<input type="hidden" name="confir" id="confir" value="1">';
                     echo '<input type="hidden" name="clientName" id="clientName" value="' . $_POST['clientName'] . '">';
                     echo '<input type="hidden" name="ammount" id="ammount" value="' . $_POST['ammount'] . '">';
                     echo '<input type="hidden" name="desc" id="desc" value="' . $_POST['desc'] . '">';
                     echo '</div>';
                     echo '</div>';
                     echo '<br>';
                     echo '<div class="form-group" style="padding-top:10px;">';
                     echo '<a class="btn btn-block btn-social btn-dropbox" onclick="goBack()" style="background-color:' . $accRGB . '; color: #ffffff; border: 2px solid; border-radius: 6px !important; float:left; width: 100px; height:36px; margin-top:-25px;padding-left:10px;">';
                     echo '<i class="fa fa-arrow-left"></i>Back';
                     echo '</a>';
                     $conff = "document.forms['conff'].submit()";
                     echo '<a class="btn btn-block btn-social btn-dropbox " onclick=' . $conff . ' style="background-color:' . $accRGB . '; color: #ffffff; border: 2px solid; border-radius: 6px !important; float:left; width: 165px; height:36px; margin-top:-25px;padding-left:10px;">';
                     echo '<i class="fa fa-credit-card"></i>Confirm Payment';
                     echo '</a>';
                     echo '</div>';
                  }

                  if ($succ == 1) {
                     echo '<div class="callout callout-success">';
                     echo "You have successfully made a payment.";
                     echo '<div style="float:right; margin-top:-3px">';
                     echo '<i class="fa fa-check" style="color:#5cb157; font-size:20pt;"></i>';
                     echo '</div>';
                     echo '</div>';
                  }
                  ?>
            </form>
         </div>
      </div>
   </section>
</aside>

<!--  THE JAVASCRIPT IMPORT ALWAYS APPEARS BEFORE LOCAL JAVASCRIPT AT THE BOTTOM OF THE PAGE -->
<?php include("template_import_script.php"); //must import all scripts first ?>
<!-- END JAVASCRIPT IMPORT -->

<script type="text/javascript">
   function goBack()
   {
      /*serviceName = "<?php echo urlencode($_GET['client']); ?>";
       repRange = "<?php echo urlencode($_GET['range']); ?>";*/
      var client = "<?php echo $_POST['clientName']; ?>"
      var ammount = "<?php echo $_POST['ammount']; ?>"
      var desc = "<?php echo $_POST['desc']; ?>"
      window.location = "../pages/updatecredits.php?client=" + client + "&ammount=" + ammount + "&desc=" + desc;
   }
</script>

<script type="text/javascript">
   $(window).load(function () {
      $(".loader").fadeOut("slow");
      $(".loaderIcon").fadeOut("slow");
   })
</script>

<!-- Template Footer -->

