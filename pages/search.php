<?php
/**
 * Include the header template which sets up the HTML
 *
 * Don't forget to include template_import_script before any Javascripting
 * Don't forget to include template footer.php at the bottom of the page as well
 */
$headerPageTitle = "Search SMS"; //set the page title for the template import
TemplateHelper::initialize();

set_time_limit(3000);
//ini_set('memory_limit', '256M');

if (isset($_SESSION['resellerId']) && $_SESSION['resellerId'] != '') {
   if ($_SESSION['serviceId'] == $_SESSION['resellerId']) {
      //this is a reseller;
      $serviceVar = ' AND (' . getAllServicesInReseller($_SESSION['serviceId']) . ')';
      //echo "<br><br><br>==========================================================".$serviceVar;
   } else {
      //this is a client
      $serviceVar = ' AND service_id:' . $_SESSION['serviceId'];
      //echo "<br><br><br>==========================================================".$serviceVar;
   }
} else {
   $serviceVar = '';
   $serviceVar = ' AND service_id:' . $_SESSION['serviceId'];
   //this is a connet user
}



/* if(isset($_GET['searchTerm']) && $_GET['searchTerm'] != '')
  {
  $searchT = $_GET['searchTerm'];

  $searchTerm = ',{
  "multi_match" : {
  "query":    "'.$searchT.'",
  "fields": ["service_login", "mtsms_id", "smsc", "src", "dest", "content", "mccmnc"]
  }
  }';
  }
  else
  {
  $searchT = '';
  $searchTerm = '';
  } */

/* if(isset($_GET['range']))
  {
  }
  else
  {

  //$startDate = strtotime('today', time())*1000;
  } */

if (isset($_GET['date']) && $_GET['date'] != '') {
   $dateArr = explode(' - ', $_GET['date']);
   $startDate = $dateArr[0];
   $startDateConv = $dateArr[0];
   $endDate = $dateArr[1];
   $endDateConv = $dateArr[1];
   $date = $_GET['date'];
} else {
   $startDate = strtotime("midnight", time()) * 1000;
   $endDate = (time() * 1000);
   $startDateConv = date('Y-m-d', ($startDate / 1000));
   $endDateConv = date('Y-m-d', ($endDate / 1000));
   $date = $startDateConv . ' - ' . $endDateConv;
}

if (isset($_GET['dest']) && $_GET['dest'] != '') {
   $dest2 = $_GET['dest'];
   $dests = explode(',', $dest2);
   $dest = ' AND (';
   for ($i = 0; $i < count($dests); $i++) {
      $dest .= 'dest:' . $dests[$i] . ' OR ';
   }
   $dest = substr($dest, 0, -4);
   $dest .= ')';
   // (dest:27837788774* OR dest:27833928610*)
} else {
   $dest = "";
   $dest2 = "";
}

if (isset($_GET['cont']) && $_GET['cont'] != '') {
   $cont = ' AND content:' . $_GET['cont'];
   $cont2 = $_GET['cont'];
} else {
   $cont = "";
   $cont2 = "";
}

/* if(isset($_GET['serv']) && $_GET['serv'] != '')
  {
  $serv = ' AND service_login:'.$_GET['serv'];
  $serv2 = $_GET['serv'];
  }
  else
  {
  $serv = "";
  $serv2 = "";
  } */

/* if(isset($_GET['smsc']) && $_GET['smsc'] != '')
  {
  $smsc = ' AND smsc:'.$_GET['smsc'];
  $smsc2 = $_GET['smsc'];
  }
  else
  {
  $smsc = "";
  $smsc2 = "";
  } */

$query = $dest . $cont . $serviceVar;
if (substr($query, 0, 5) == ' AND ') {
   $query = substr($query, 5);
}

if ($query == '') {
   $query = '*';
}

/* if(isset($_GET['batc']) && $_GET['batc'] != '')
  {
  $batc = $_GET['batc'];
  }
  else
  {
  $batc = "";
  }

  if(isset($_GET['stat']) && $_GET['stat'] != '')
  {
  $stat = $_GET['stat'];
  }
  else
  {
  $stat = "";
  } */

if (isset($_GET['from'])) {
   $from = $_GET['from'];
   //$from = $from - 1;
   $starting = ($from - 1) * 100;
} else {
   $from = 1;
   $starting = 0;
}

$ordering = 'ASC';
$units = 0;

if (isset($_GET['sort'])) {
   $sort = urldecode($_GET['sort']);
   $ordering = strstr($sort, ':');
   $ordering = trim($ordering, ':');
   $ordering = trim($ordering, '"');
   $sortUrl = urlencode($sort);
} else {
   $sort = '"timestamp":"desc"';
   $ordering = strstr($sort, ':');
   $ordering = trim($ordering, ':');
   $ordering = trim($ordering, '"');
   $sortUrl = urlencode($sort);
}

$visible = 'none';

if (isset($_GET['searching']) && $_GET['searching'] == 1) {
   $visible = 'block';
   /* $client = $_GET['client'];
     $cl = $_GET['client'];
     $clp = strrpos($cl, ' ');
     $cl = trim(substr($cl, $clp, strlen($cl))); */
   $dsp = 'inline';
   $dir = '../img/serviceproviders';
   $serProviders = scandir($dir);
   /* $mcc = $_GET['mcc'];
     $smsc = $_GET['smsc']; */
   //echo '<br><br><br><br>br='..'=';

   $qry = '{
                  "size":100,
                  "from":' . $starting . ',
                  "sort": {
                    ' . $sort . '
                  },
                  "fields" : ["service_id","service_login","mtsms_id","timestamp","src","dest","content","dlr","mccmnc"],
                  "query": {
                    "filtered": {
                      "query": {
                        "bool" : {
                            "must" : [
                                {
                                 "query_string": {
                                 "query": "' . $query . '",
                                 "analyze_wildcard": true
                                    }
                                },
                                {
                                  "range": {
                                    "timestamp": {
                                      "gte": "' . $startDateConv . '",
                                      "lte": "' . $endDateConv . '"
                                    }
                                  }
                                }
                            ]
                        }
                      }
                    }
                  }  
                }';

   $allStats = runRawMTSMSElasticsearchQuery($qry);

   foreach ($allStats as $key => $value) {
      if ($key == 'hits') {
         $units = $value['total'];
      }
   }
}
?>

<aside class="right-side">
   <section class="content-header">
      <h1>
         Search SMS
      </h1>
   </section>

   <section class="content">
      <div class="box box-connet" style="padding-top:10px;padding-left:10px;padding-right:10px;padding-bottom:120px; border-top-color:<?php echo $accRGB; ?>;">
         <div class="callout callout-info" style="margin-bottom:10px;">
            <h4>Instruction</h4>
            <p>Use the boxes below to filter your search.</p>
         </div>

         <div class="col-xs-4">
            <b>Date:</b>
            <br>
            <div id="reportrange" class="select pull-left" style="cursor:pointer;padding-top:10px;padding-left:0px;">
               <i class="fa fa-calendar fa-lg"></i>
               <span style="font-size:15px"><?php echo $startDateConv . " - " . $endDateConv; ?></span><b class="caret"></b>
            </div>
            <!--p>
                <input type="text" id="seDate" class="form-control" style="border:1px solid #929292;margin-top:5px;height:35px;" name="servName" placeholder="" value="<?php //echo $date;     ?>">
            </p-->
         </div>


         <div class="col-xs-4">
            <b>Destination:</b>
            <p>
               <input type="text" id="seDestination" class="form-control" style="border:1px solid #929292;margin-top:5px;height:35px;" name="servName" placeholder="" value="<?php echo $dest2; ?>">
            </p>
         </div>

         <div class="col-xs-4">
            <b>Content:</b>
            <p>
               <input type="text" id="seContent" class="form-control" style="border:1px solid #929292;margin-top:5px;height:35px;" name="servName" placeholder="" value="<?php echo $cont2; ?>">
            </p>
         </div>

         <!--div class="col-xs-4">
             <b>Service:</b>
             <p>
                 <input type="text" id="seService" class="form-control" style="border:1px solid #929292;margin-top:5px;height:35px;" name="servName" placeholder="" value="<?php echo $serv2; ?>">
             </p>
         </div-->

         <!--div class="col-xs-4">
             <b>SMSC:</b>
             <p>
                 <input type="text" id="seSMSC" class="form-control" style="border:1px solid #929292;margin-top:5px;height:35px;" name="servName" placeholder="" value="<?php echo $smsc2; ?>">
             </p>
         </div-->

         <!--div class="col-xs-4">
             <b>Batch Id:</b>
             <p>
                 <input type="text" id="seBatchid" class="form-control" style="border:1px solid #929292;margin-top:5px;height:35px;" name="servName" placeholder="" value="<?php echo $batc; ?>">
             </p>
         </div>

         <div class="col-xs-4">
             <b>Status:</b>
             <p>
                 <input type="text" id="seStatus" class="form-control" style="border:1px solid #929292;margin-top:5px;height:35px;" name="servName" placeholder="" value="<?php echo $stat; ?>">
             </p>
         </div-->
         <div class="col-xs-12">
            <a class="btn btn-block btn-social btn-dropbox" name="nid" onclick="searchElastics()" style="background-color:<?php echo $accRGB; ?>; border: 2px solid; border-radius: 6px !important; float:left; width: 100px;">
               <i class="fa fa-search"></i>Search
            </a>
         </div>
      </div>
   </section>

   <section class="content" style="display:<?php echo $visible; ?>">
      <div class="box box-connet" style="padding-top:1px; border-top-color:<?php echo $accRGB; ?>;">
         <div class="box-body">
            <div class="box-body table-responsive no-padding">
               <table id="example2"  class="table table-striped table-hover">
                  <thead>
                     <tr>
                        <?php
                        if (isset($_GET['controller'])) {
                           if ($ordering == 'desc') {
                              $ordering = 'asc';
                           } else {
                              $ordering = 'desc';
                           }
                        }
                        echo '<th><a href="../pages/search.php?sort=' . '%22mtsms_id%22%3A%22' . $ordering . '%22' . '&searching=1&date=' . $date . '&controller=1&dest=' . $dest2 . '&cont=' . $cont2 . '&serv=' . $serviceVar . '&from=' . $from . '">MTSMS_id</a></th>';
                        echo '<th><a href="../pages/search.php?sort=' . '%22timestamp%22%3A%22' . $ordering . '%22' . '&searching=1&date=' . $date . '&controller=1&dest=' . $dest2 . '&cont=' . $cont2 . '&serv=' . $serviceVar . '&from=' . $from . '">Date</a></th>';
                        echo '<th><a href="../pages/search.php?sort=' . '%22mccmnc%22%3A%22' . $ordering . '%22' . '&searching=1&date=' . $date . '&controller=1&dest=' . $dest2 . '&cont=' . $cont2 . '&serv=' . $serviceVar . '&from=' . $from . '">Network</a></th>';
//echo '<th><a href="../pages/search.php?sort='.'%22smsc%22%3A%22'.$ordering.'%22'.'&searching=1&date='.$date.'&controller=1&dest='.$dest2.'&cont='.$cont2.'&serv='.$serviceVar.'&from='.$from.'">SMSC</a></th>';
                        echo '<th><a href="../pages/search.php?sort=' . '%22service_login%22%3A%22' . $ordering . '%22' . '&searching=1&date=' . $date . '&controller=1&dest=' . $dest2 . '&cont=' . $cont2 . '&serv=' . $serviceVar . '&from=' . $from . '">Service</a></th>';
//echo '<th><a href="../pages/search.php?sort='.'%22src%22%3A%22'.$ordering.'%22'.'&searching=1&date='.$date.'&controller=1&dest='.$dest2.'&cont='.$cont2.'&serv='.$serviceVar.'&from='.$from.'">Source</a></th>';
                        echo '<th><a href="../pages/search.php?sort=' . '%22dest%22%3A%22' . $ordering . '%22' . '&searching=1&date=' . $date . '&controller=1&dest=' . $dest2 . '&cont=' . $cont2 . '&serv=' . $serviceVar . '&from=' . $from . '">Destination</a></th>';
                        echo '<th><a href="../pages/search.php?sort=' . '%22content%22%3A%22' . $ordering . '%22' . '&searching=1&date=' . $date . '&controller=1&dest=' . $dest2 . '&cont=' . $cont2 . '&serv=' . $serviceVar . '&from=' . $from . '">Content</a></th>';
//echo '<th><a href="../pages/search.php?sort='.'%22rdnc%22%3A%22'.$ordering.'%22'.'&searching=1&date='.$date.'&controller=1&dest='.$dest2.'&cont='.$cont2.'&serv='.$serviceVar.'&from='.$from.'">RDNC</a></th>';
                        echo '<th><a href="../pages/search.php?sort=' . '%22dlr%22%3A%22' . $ordering . '%22' . '&searching=1&date=' . $date . '&controller=1&dest=' . $dest2 . '&cont=' . $cont2 . '&serv=' . $serviceVar . '&from=' . $from . '">DLR</a></th>';
                        ?>
                     </tr>
                  </thead>
                  <tbody>
                     <?php
                     if (isset($allStats) && $allStats != '') {
                        foreach ($allStats as $key => $value) {
                           if ($key == 'hits') {
                              $hits = count($value['hits']);
                              for ($i = 0; $i < $hits; $i++) {
                                 $mtid = $value['hits'][$i]['fields']['mtsms_id']['0'];
                                 //$time = strstr($value['hits'][$i]['fields']['timestamp']['0'], 'T', true);
                                 $time = str_replace('T', ' ', $value['hits'][$i]['fields']['timestamp']['0']);
                                 $time = strstr($time, '+', true);
                                 $serv = $value['hits'][$i]['fields']['service_login']['0'];
                                 $dest = $value['hits'][$i]['fields']['dest']['0'];
                                 //$src = $value['hits'][$i]['fields']['src']['0'];
                                 $cont = $value['hits'][$i]['fields']['content']['0'];
                                 //$rdnc = $value['hits'][$i]['fields']['rdnc']['0'];
                                 $dlrPos = $value['hits'][$i]['fields']['dlr']['0'];
                                 $mcc = getNetFromMCC($value['hits'][$i]['fields']['mccmnc']['0']);
                                 if (strrpos($mcc, '/')) {
                                    $mccE = strstr($mcc, '/');
                                    $mccE = substr($mccE, 2);
                                 } else {
                                    $mccE = $mcc;
                                 }
                                 /* $smsc = $value['hits'][$i]['fields']['smsc']['0'];
                                   if(strrpos($smsc, '_'))
                                   {
                                   $smscE = strstr($smsc, '_', true);
                                   }
                                   else
                                   {
                                   $smscE = $smsc;
                                   } */

                                 echo '<tr>';
                                 echo '<td style="vertical-align:middle;">' . $mtid . '</td>';
                                 echo '<td style="vertical-align:middle;width:100px;">' . $time . '</td>';
                                 echo '<td style="vertical-align:middle;width:160px;"><img src="../img/serviceproviders/' . $mccE . '.png">&nbsp;&nbsp;' . $mcc . ' (' . $value['hits'][$i]['fields']['mccmnc']['0'] . ')</td>';
                                 //echo '<td style="vertical-align:middle;width:150px;"><img src="../img/serviceproviders/'.$smscE.'.png">&nbsp;&nbsp;'.$smsc.'</td>';
                                 echo '<td style="vertical-align:middle;">' . $serv . '</td>';
                                 //echo '<td style="vertical-align:middle;">'.$src.'</td>';
                                 echo '<td style="vertical-align:middle;">' . $dest . '</td>';
                                 echo '<td style="vertical-align:middle;width:380px;">' . $cont . '</td>';
                                 //echo '<td style="vertical-align:middle;">'.$rdnc.'</td>';


                                 switch ($status = DeliveryReportHelper::dlrMaskToString($dlrPos, DeliveryReportHelper::DLR_STRING_PRETTY)) {

                                    case 'Delivered': $color = '#009900';
                                       break;
                                    case 'Failed':$color = '#cb0234';
                                       break;
                                    case 'Rejected':$color = '#cb0234';
                                       break;
                                    case 'Excluded':$color = '#223322';
                                       break;
                                    default:

                                       $color = '#eec00d';
                                 }

                                 echo '<td style="vertical-align:middle;color:' . $color . ';">' . htmlentities($status) . '</td>';
                                 echo '</tr>';
                              }
                           }
                        }
                     }
                     ?>
                  </tbody>
               </table>
               <div class="row">
                  <div class="col-xs-6">
                     <div class="dataTables_info" id="example2_info">Showing <?php echo ($starting); ?> to <?php
                        if (($from * 100) < $units) {
                           echo ($from * 100);
                        } else {
                           echo $units;
                        }
                        ?> of <?php echo $units; ?> entries
                     </div>
                  </div>
                  <div class="col-xs-6">
                     <div class="dataTables_paginate paging_bootstrap">
                        <ul class="pagination">
                           <?php
                           if ($from > 1) {
                              echo '<li class="prev">';
                              echo '<a href="../pages/search.php?sort=' . $sortUrl . '&searching=1&date=' . $date . '&dest=' . $dest2 . '&cont=' . $cont2 . '&serv=' . $serviceVar . '&from=1">First</a>';
                              echo '</li>';
                              if ($from > 2) {
                                 echo '<li>';
                                 echo '<a href="../pages/search.php?sort=' . $sortUrl . '&searching=1&date=' . $date . '&dest=' . $dest2 . '&cont=' . $cont2 . '&serv=' . $serviceVar . '&from=' . ($from - 2) . '">' . ($from - 2) . '</a>';
                                 echo '</li>';
                              }
                              echo '<li>';
                              echo '<a href="../pages/search.php?sort=' . $sortUrl . '&searching=1&date=' . $date . '&dest=' . $dest2 . '&cont=' . $cont2 . '&serv=' . $serviceVar . '&from=' . ($from - 1) . '">' . ($from - 1) . '</a>';
                              echo '</li>';
                           }
                           echo '<li class="active">';
                           echo '<a href="../pages/search.php?sort=' . $sortUrl . '&searching=1&date=' . $date . '&dest=' . $dest2 . '&cont=' . $cont2 . '&serv=' . $serviceVar . '&from=' . $from . '">' . $from . '</a>';
                           echo '</li>';
                           if (($from * 100) < $units) {
                              echo '<li>';
                              echo '<a href="../pages/search.php?sort=' . $sortUrl . '&searching=1&date=' . $date . '&dest=' . $dest2 . '&cont=' . $cont2 . '&serv=' . $serviceVar . '&from=' . ($from + 1) . '">' . ($from + 1) . '</a>';
                              echo '</li>';
                              if ((($from + 1) * 100) < $units) {
                                 echo '<li>';
                                 echo '<a href="../pages/search.php?sort=' . $sortUrl . '&searching=1&date=' . $date . '&dest=' . $dest2 . '&cont=' . $cont2 . '&serv=' . $serviceVar . '&from=' . ($from + 2) . '">' . ($from + 2) . '</a>';
                                 echo '</li>';
                              }
                              echo '<li class="next">';
                              echo '<a href="../pages/search.php?sort=' . $sortUrl . '&searching=1&date=' . $date . '&dest=' . $dest2 . '&cont=' . $cont2 . '&serv=' . $serviceVar . '&from=' . (floor($units / 100) + 1) . '>">Last &#8594;</a>';
                              echo '</li>';
                           }
                           ?>
                        </ul>
                     </div>
                  </div>
               </div>
               <div class="box-body">
                  <div class="form-group" style="padding-top:30px">
<?php
echo '<a href="../php/createCSVsearch.php?admin=0sort=' . $sortUrl . '&date=' . $date . '&dest=' . $dest2 . '&cont=' . $cont2 . '&serv=' . $serviceVar . '&from=' . $from . '&total=' . $units . '" class="btn btn-block btn-social btn-dropbox" style="background-color:' . $accRGB . '; border: 2px solid; border-radius: 6px !important; float:left; width: 134px; margin-top:-21px;">';
?>
                     <i class="fa fa-save"></i>Save Report
                     </a>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </section>
</aside>

<!--  THE JAVASCRIPT IMPORT ALWAYS APPEARS BEFORE LOCAL JAVASCRIPT AT THE BOTTOM OF THE PAGE -->
<?php include("template_import_script.php"); //must import all scripts first   ?>
<!-- END JAVASCRIPT IMPORT -->

<script type="text/javascript">
   $(function ()
   {
      $('#example2').dataTable({
         "bPaginate": false,
         "bLengthChange": false,
         "bFilter": false,
         "bSort": false,
         "bInfo": false,
         "bAutoWidth": false,
         "iDisplayLength": 10,
      });
   });

   /*jQuery.extend( jQuery.fn.dataTableExt.oSort, 
    {
    "title-numeric-pre": function ( a ) {
    var x = a.match(/title="*(-?[0-9\.]+)/)[1];
    return parseFloat( x );
    },
    
    "title-numeric-asc": function ( a, b ) {
    return ((a < b) ? -1 : ((a > b) ? 1 : 0));
    },
    
    "title-numeric-desc": function ( a, b ) {
    return ((a < b) ? 1 : ((a > b) ? -1 : 0));
    }
    } );
    
    jQuery.extend( jQuery.fn.dataTableExt.oSort, 
    {
    "formatted-num-pre": function ( a ) {
    a = (a === "-" || a === "") ? 0 : a.replace( /[^\d\-\.]/g, "" );
    return parseFloat( a );
    },
    
    "formatted-num-asc": function ( a, b ) {
    return a - b;
    },
    
    "formatted-num-desc": function ( a, b ) {
    return b - a;
    }
    } );*/
</script>

<script type="text/javascript">
   /*function searchText()
    {
    
    var repRange = "<?php echo urlencode($_GET['range']); ?>";
    var repRange1 = "<?php echo urlencode($_GET['range1']); ?>";
    var searchT = document.getElementById('search_filter').value;
    //alert(searchT);
    //window.location = "../pages/apitrafficadmin.php?client=" + serviceName + "&range=" + repRange;
    var t = "<?php echo $_GET['type']; ?>";
    window.location = "../pages/networkpercentmsgadmin.php?range=" + repRange + "&type=" + t + "&range1=" + repRange1 + "&searchTerm=" + searchT + "&sents=<?php echo $units; ?>" + "&from=1&controller=1&sort=<?php echo $sortUrl; ?>" + "&query=<?php echo $_GET['query']; ?>" + "&mcc=<?php echo $_GET['mcc']; ?>" + "&smsc=<?php echo $_GET['smsc']; ?>" + "&rdnc=<?php echo $_GET['rdnc']; ?>";
    }*/
   var vStart = '<?php echo $startDateConv; ?>';
   var vEnd = '<?php echo $endDateConv; ?>';

   function searchElastics(vS, vE)
   {
      if (vS != null && vE != null)
      {
         vStart = vS;
         vEnd = vE;
         //alert('Set bitch');
      } else
      {
         var date = vStart + ' - ' + vEnd;
         var dest = document.getElementById('seDestination').value;
         var cont = document.getElementById('seContent').value;
         //var serv = document.getElementById('seService').value;
         //var smsc = document.getElementById('seSMSC').value;
         //var rr = document.getElementById('reportrange').value;
         /*var batc = document.getElementById('seBatchid').value;
          var stat = document.getElementById('seStatus').value;*/

         //window.location = "../pages/search.php?searching=1&date=" + date + "&dest=" + dest + "&cont=" + cont + "&serv=" + serv + "&smsc=" + smsc + "&batc=" + batc + "&stat=" + stat;
         //alert(date);
         //alert(date);
         window.location = "../pages/search.php?searching=1&date=" + date + "&dest=" + dest + "&cont=" + cont;
      }
   }
</script>

<script type="text/javascript">
   $('#reportrange').daterangepicker(
           {
              ranges: {
                 'Today': [moment(), moment()],
                 'Yesterday': [moment().subtract('days', 1), moment().subtract('days', 1)],
                 'Last 7 Days': [moment().subtract('days', 6), moment()],
                 'Last 30 Days': [moment().subtract('days', 29), moment()],
                 'This Month': [moment().startOf('month'), moment().endOf('month')],
                 'Last Month': [moment().subtract('month', 1).startOf('month'), moment().subtract('month', 1).endOf('month')]
              },
              startDate: moment('<?php echo isset($startDateConv) ? $startDateConv : ''; ?>'),
              endDate: moment('<?php echo isset($endDateConv) ? $endDateConv : ''; ?>')
           },
           function (start, end) {
              //$(".loader").fadeIn("slow");
              //$(".loaderIcon").fadeIn("slow");
              var month = new Array();
              month[0] = "01";
              month[1] = "02";
              month[2] = "03";
              month[3] = "04";
              month[4] = "05";
              month[5] = "06";
              month[6] = "07";
              month[7] = "08";
              month[8] = "09";
              month[9] = "10";
              month[10] = "11";
              month[11] = "12";

              start = new Date(start);
              var yearS = start.getFullYear();
              var monthS = month[start.getMonth()];
              var dateS = ("0" + start.getDate()).slice(-2);
              start = yearS + '-' + monthS + '-' + dateS;

              end = new Date(end);
              var yearE = end.getFullYear();
              var monthE = month[end.getMonth()];
              var dateE = ("0" + end.getDate()).slice(-2);
              end = yearE + '-' + monthE + '-' + dateE;

              searchElastics(start, end);

              $('#reportrange span').html(start + ' - ' + end);

              var repRange = $("#reportrange span").html();

              //window.location = "../pages/networkpercentadmin.php?range=" + repRange;
           }
   );
</script>

<script type="text/javascript">
   $(window).load(function () {
      $(".loader").fadeOut("slow");
      $(".loaderIcon").fadeOut("slow");
   })
</script>

<!-- Template Footer -->

