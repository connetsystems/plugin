<?php
/**
 * Include the header template which sets up the HTML
 *
 * Don't forget to include template_import_script before any Javascripting
 * Don't forget to include template footer.php at the bottom of the page as well
 */
//-------------------------------------------------------------
// Template
//-------------------------------------------------------------
TemplateHelper::setPageTitle('Quick SMS');
TemplateHelper::initialize();

$singleView = 'block;';
$freeAcc = "";
$freeTxt = "";

//-------------------------------------------------------------
// Permissions
//-------------------------------------------------------------
$clients = PermissionsHelper::getAllServicesWithPermissions('submit.mtsms');

if (isset($_GET['service_id'])) {
   
   if (!isset($clients[(integer) $_GET['service_id']])) {      
      header('location: ' . $_SERVER['DOCUMENT_URI']);
      die();      
   }
}

if (count($clients) == 1) {

   $_GET['service_id'] = LoginHelper::getCurrentServiceId();
   $singleView = 'none;';
 
} elseif (!isset($_GET['service_id'])) {

   $_GET['service_id'] = LoginHelper::getCurrentServiceId();
}


//-------------------------------------------------------------
// Free Credits
//-------------------------------------------------------------
switch (LoginHelper::getCurrentAccountId()) {

   case 650:
      $freeAcc = "readonly=\"readonly\"";
      $freeTxt = "Did you know that 90% of the population in RSA own a cell phone?  Use bulk SMS from Connet Systems for your mobile marketing solutions. www.connet-systems.com";
      break;
}

//-------------------------------------------------------------
// Client
//-------------------------------------------------------------
if (isset($_GET['service_id'])) {
   $cl = $_GET['service_id'];

   $dsp = 'block;';

   $checkPerm = checkUserCanSend($cl, $_SESSION['userId']);
} else {
   $_GET['service_id'] = 0;
   $cl = $_GET['service_id'];
   $dsp = 'none;';
   $checkPerm = 'null';
}

if ($checkPerm != 'null' && $checkPerm < 1) {
   $showPerm = 'block;';
   $showPermOp = 'none;';
} else {
   $showPerm = 'none;';
   $showPermOp = 'block;';
}
////////////////////////////////////////////////
////////////////////////////////////////////////
////////////////////////////////////////////////
?>

<aside class="right-side">
   <section class="content-header">
      <h1>
         Quick SMS
         <!--small>Control panel</small-->
      </h1>
   </section>

   <section class="content">
      <div class="row col-lg-12">
         <div class="callout callout-danger" id="numssError" style="padding-top:10px;margin-bottom:30px;display:<?php echo $showPerm; ?>">
            <h4>No Permissions</h4>
            <p>
               You do not have permissions to send on this account. Please contact support.
            </p>
            <div style="float:right; margin-top:-44px; margin-right:10px;">
               <i class="fa fa-exclamation-triangle" style="color:#c99b9d; font-size:20pt;"></i>
            </div>
         </div>
         <div class="box box-solid" style="height:66px; margin-top:-15px;display:<?php echo $singleView; ?>">
            <form action="" method="get" id="clientList" class="sidebar-form" style="border:0px;">
               <div class="form-group">
                  <label>Service List</label>
                  <select class="form-control" name="client" form="clientList" OnChange="reloadOnSelect(this.value);">
                     <?php
                     if ($cl == '0') {
                        echo '<option SELECTED>Please select a client...</option>';
                     }
                     foreach ($clients as $key => $value) {
                        $service_id = $value['service_id'];
                        if ($cl == $service_id) {
                           $accountName = $value['account_name'];
                           $serviceName = $value['service_name'];
                           echo "<option value=\"" . $service_id . "\" SELECTED>" . $accountName . " - " . $serviceName . " - " . $service_id . "</option>";
                        } else {
                           echo "<option value=\"" . $service_id . "\">" . $value['account_name'] . " - " . $value['service_name'] . " - " . $service_id . "</option>";
                        }
                     }
                     ?>
                  </select>
               </div>
               <br>
            </form>
         </div>

         <div class="box box-warning" style=" border-top-color:<?php echo $accRGB; ?>;display:<?php echo $dsp; ?>">
            <div class="box-body">
               <div class="callout callout-info" style="margin-bottom:0px;">
                  <!--h4>Quick SMS</h4-->
                  <p>Under recipients, please enter the international cell number format (ie. 27821234567). Multiple numbers should be separated by a comma sign (,) with no spaces.</p>
               </div>
               <?php
               if (isset($cl) && $cl != 0) {
                  //echo '<br><br><br><br>Ser='.$cl;
                  $sR = getServiceCredit($cl);
                  $sT = getServiceType($cl);

                  if ($sR / 10000 <= 0 && $sT == 'PREPAID') {
                     echo '<div class="callout callout-danger" id="numssError" style="margin-bottom:0px;">';
                     echo '<h4>Insufficient Credits!</h4>';
                     echo '<p>You do not have enough funds to send. Please contact support.</p>';
                     echo '<div style="float:right; margin-top:-42px; margin-right:10px;">';
                     echo '<i class="fa fa-exclamation-triangle" style="color:#c99b9d; font-size:20pt;"></i>';
                     echo '</div>';
                     echo '</div>';
                  }
               }
               if (isset($_GET['error'])) {
                  switch ($_GET['error']) {
                     case 1:
                        echo '<div class="callout callout-danger" id="numssError" style="margin-bottom:0px;">';
                        echo '<p style="color:#B94A48;margin-bottom:0px;">No network associated with recipient number(s). Please contact support to update your networks setup.</p>';
                        echo '<div style="float:right; margin-top:-20px; margin-right:0px;">';
                        echo '<i class="fa fa-exclamation-triangle" style="color:#c99b9d; font-size:16pt;"></i>';
                        echo '</div>';
                        echo '</div>';
                        break;
                     case 2:
                        echo '<div class="callout callout-danger" id="numssError" style="margin-bottom:0px;">';
                        echo '<p style="color:#B94A48;margin-bottom:0px;">No network associated with recipient number(s). Please contact support to update your networks setup.</p>';
                        echo '<div style="float:right; margin-top:-20px; margin-right:0px;">';
                        echo '<i class="fa fa-exclamation-triangle" style="color:#c99b9d; font-size:16pt;"></i>';
                        echo '</div>';
                        echo '</div>';
                        break;
                     case 3:
                        echo '<div class="callout callout-danger" id="numssError" style="margin-bottom:0px;">';
                        echo '<p style="color:#B94A48;margin-bottom:0px;">Numbers must be in international format and must not begin with "0".</p>';
                        echo '<div style="float:right; margin-top:-20px; margin-right:0px;">';
                        echo '<i class="fa fa-exclamation-triangle" style="color:#c99b9d; font-size:16pt;"></i>';
                        echo '</div>';
                        echo '</div>';
                        break;
                  }
               }
               ?>
            </div>
            <div class="box-body">
               <form role="form" id="smsData" action="confirmquicksms.php" method="post">
                  <div class="form-group">
                     <label>Recipients:</label>
                     <textarea class="form-control" rows="3" id="smsNumbers" name="numbers" placeholder="Enter ..."></textarea>
                  </div>
                  <div class="callout callout-danger" id="numError" style="display:none;">
                     <p style="color:#B94A48;">Please enter recipient number.</p>
                     <div style="float:right; margin-top:-20px; margin-right:0px;">
                        <i class="fa fa-exclamation-triangle" style="color:#c99b9d; font-size:16pt;"></i>
                     </div>
                  </div>
                  <div class="form-group" style="margin-bottom:5px;">
                     <label>SMS text:</label>
                     <textarea class="form-control" id="smsText" name="smstext" rows="3" placeholder="Enter ..." <?php echo $freeAcc; ?> ></textarea>
                  </div>
                  <label style="display:block;padding-bottom:10px;" id="counter">0 characters, 0 SMS</label>
                  <div class="callout callout-danger" id="smsError" style="display:none;">
                     <p style="color:#B94A48;">Please enter SMS text.</p>
                     <div style="float:right; margin-top:-20px; margin-right:0px;">
                        <i class="fa fa-exclamation-triangle" style="color:#c99b9d; font-size:16pt;"></i>
                     </div>
                  </div>
                  <br>
                  <input type="hidden" name="service_id" value="<?php echo $cl; ?>">
                  <input type="hidden" name="client" value="<?php echo $_GET['client']; ?>">
                  <?php
                  if (isset($cl) && $cl != 0) {
                     //echo '<br><br><br><br>Ser='.$cl;
                     $sR = getServiceCredit($cl);
                     $sT = getServiceType($cl);

                     if ($sR / 10000 <= 0 && $sT == 'PREPAID') {
                        ?>
                        <div class="form-group">
                           <a class="btn btn-block btn-social btn-dropbox " onclick="javascript:void(0);" disabled style="background-color: <?php echo $accRGB; ?>; color: #ffffff; border: 2px solid; border-radius: 6px !important; float:left; width: 160px; margin-top:-23px;">
                              <i class="fa fa-save"></i>Send Quick SMS
                           </a>
                        </div>
                        <?php
                     } else {
                        ?>
                        <div class="form-group">
                           <?php
                           if ($showPerm != 'block;') {
                              ?>
                              <a class="btn btn-block btn-social btn-dropbox " onclick="validateQuickSMS()" style="background-color: <?php echo $accRGB; ?>; color: #ffffff; border: 2px solid; border-radius: 6px !important; float:left; width: 160px; margin-top:-23px;">
                                 <i class="fa fa-save"></i>Send Quick SMS
                              </a>
                              <?php
                           }
                           ?>
                        </div>
                        <?php
                     }
                  } else {
                     ?>
                     <div class="form-group">
                        <?php
                        if ($showPerm != 'block;') {
                           ?>
                           <a class="btn btn-block btn-social btn-dropbox " onclick="validateQuickSMS()" style="background-color: <?php echo $accRGB; ?>; color: #ffffff; border: 2px solid; border-radius: 6px !important; float:left; width: 160px; margin-top:-23px;">
                              <i class="fa fa-save"></i>Send Quick SMS
                           </a>
                           <?php
                        }
                        ?>
                     </div>
                     <?php
                  }
                  ?>


               </form>
            </div>
         </div>
      </div>
   </section>
</aside>

<!--  THE JAVASCRIPT IMPORT ALWAYS APPEARS BEFORE LOCAL JAVASCRIPT AT THE BOTTOM OF THE PAGE -->
<?php include("template_import_script.php"); //must import all scripts first    ?>
<!-- END JAVASCRIPT IMPORT -->

<script type="text/javascript">
   $(function () {
      var area = document.getElementById("smsText");
      var message = document.getElementById("counter");

      countAndValidateSMSText();

      $("#smsNumbers").change(function (e) {
         cleanUpNumbersField();
      });

      $("#smsNumbers").keyup(function (e) {
         console.log("Checking validation...");
         var numbersString = $("#smsNumbers").val();
         numbersString = numbersString.replace(/[^0-9,]/g, '');
         $("#smsNumbers").val(numbersString);
      });

      function cleanUpNumbersField()
      {
         console.log("Checking validation...");
         var numbersString = $("#smsNumbers").val();
         numbersString = numbersString.replace(/[^0-9,]/g, '');
         var numbersArr = numbersString.split(',');
         console.log("size: " + numbersArr.length);
         for (var i = 0; i < numbersArr.length; i++)
         {
            console.log("NUMBER: " + numbersArr[i]);
            if (numbersArr[i] == "")
            {
               console.log("Remove it... ");
               numbersArr.splice(i, 1);
            }
         }

         $("#smsNumbers").val(numbersArr.join(','));
      }

      //var maxLength = 50;
      $("#smsText").keyup(function (e) {
         countAndValidateSMSText();
      });

      function countAndValidateSMSText()
      {
         //validation
         var thisText = $("#smsText").val();

         //replacing curly quotes with straight quotes
         var fixText = thisText
                 .replace(/[\u2018\u2019]/g, "'")
                 .replace(/[\u201C\u201D]/g, '"')
                 .replace("{ }", "( )")
                 .replace("{}", "()")
                 .replace(/\r\n/g, "\n")
                 .replace(/\u2013|\u2014/g, "-"); //long dash

         //fixText = decodeURI(encodeURIComponent(fixText));
         $("#smsText").val(fixText);

         if (area.value.length == 0) {
            message.innerHTML = "0 characters, 0 SMS";
         } else {
            var tot = 0;

            var str = area.value;
            var newLength = str.length;
            console.log("Curr length: " + newLength);
            var count = occurrences(str, '[', 'false');
            if (count != 0)
            {
               newLength = newLength + (2 * count) - count;
            }
            count = occurrences(str, ']', 'false');
            if (count != 0)
            {
               newLength = newLength + (2 * count) - count;
            }
            count = occurrences(str, '^', 'false');
            if (count != 0)
            {
               newLength = newLength + (2 * count) - count;
            }
            count = occurrences(str, '|', 'false');
            if (count != 0)
            {
               newLength = newLength + (2 * count) - count;
            }
            count = occurrences(str, '~', 'false');
            if (count != 0)
            {
               newLength = newLength + (2 * count) - count;
            }
            count = occurrences(str, '\\', 'false');
            if (count != 0)
            {
               newLength = newLength + (2 * count) - count;
            }
            count = occurrences(str, '\u20AC', 'false');
            if (count != 0)
            {
               newLength = newLength + (2 * count) - count;
            }

            count = occurrences(str, '\u21A1', 'false');
            if (count != 0)
            {
               newLength = newLength + (2 * count) - count;
            }
            count = occurrences(str, '{', 'false');
            if (count != 0)
            {
               newLength = newLength + (2 * count) - count;
            }
            count = occurrences(str, '}', 'false');
            if (count != 0)
            {
               newLength = newLength + (2 * count) - count;
            }

            if (newLength <= 160)
            {
               tot = Math.ceil((newLength) / 160);
            } else
            {
               tot = Math.ceil((newLength) / 153);
            }

            message.innerHTML = newLength + " characters, " + tot + " SMS";
         }
      }
   });
</script>
<script type="text/javascript">

   function occurrences(string, subString, allowOverlapping)
   {
      string += "";
      subString += "";
      if (subString.length <= 0)
         return string.length + 1;

      var n = 0, pos = 0;
      var step = (allowOverlapping) ? (1) : (subString.length);

      while (true)
      {
         pos = string.indexOf(subString, pos);
         if (pos >= 0) {
            n++;
            pos += step;
         } else
            break;
      }
      return(n);
   }

   function isNumberKey(evt)
   {
      var charCode = (evt.which) ? evt.which : evt.keyCode;

      if (charCode == 44)
         return true;

      /*alert(charCode);*/
      if (charCode == 46)
         return false;

      if (charCode != 46 && charCode > 31
              && (charCode < 48 || charCode > 57))
         return false;

      return true;
   }
   function reloadOnSelect(service_id)
   {
      $(".loader").fadeIn("slow");
      $(".loaderIcon").fadeIn("slow");
      window.location = "quicksms.php?service_id=" + service_id;
   }
   function validateQuickSMS()
   {
      valid = 0;
      var smsN = document.getElementById('smsNumbers').value;
      if (smsN.length == 0)
      {
         document.getElementById('numError').style.display = 'block';
      } else
      {
         document.getElementById('numError').style.display = 'none';
         valid++;
      }

      var smsT = document.getElementById('smsText').value;
      if (smsT.length == 0)
      {
         document.getElementById('smsError').style.display = 'block';
      } else
      {
         document.getElementById('smsError').style.display = 'none';
         valid++;
      }

      if (valid == 2)
      {
         document.getElementById('smsData').submit();
      }
   }
</script>

<script type="text/javascript">
   $(window).load(function () {
      $(".loader").fadeOut("slow");
      $(".loaderIcon").fadeOut("slow");
   })
</script>

<!-- Template Footer -->

