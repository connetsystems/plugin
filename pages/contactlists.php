<?php
/**
 * Include the header template which sets up the HTML
 *
 * Don't forget to include template_import_script before any Javascripting
 * Don't forget to include template footer.php at the bottom of the page as well
 */
//-------------------------------------------------------------
// Template
//-------------------------------------------------------------
TemplateHelper::setPageTitle('Contacts');
TemplateHelper::initialize();

$show_service_selector = 'block;';

//-------------------------------------------------------------
// Permissions
//-------------------------------------------------------------
$clients = PermissionsHelper::getAllServicesWithPermissions();

if (isset($_GET['service_id'])) {
   
   if (!isset($clients[(integer) $_GET['service_id']])) {
      header('location: ' . $_SERVER['DOCUMENT_URI']);
      die();
   }
}

if (count($clients) == 1) {

   $_GET['service_id'] = LoginHelper::getCurrentServiceId();
   $show_service_selector = 'none;';
 
} elseif (!isset($_GET['service_id'])) {

   $_GET['service_id'] = LoginHelper::getCurrentServiceId();
}


//-------------------------------------------------------------
// Contacts Lists
//-------------------------------------------------------------
//initalize the contact helper
$contact_helper = new helperContactLists();
$contact_lists = array();

if (isset($_GET['service_id']) && $_GET['service_id'] != "")
{
   $service_id = $_GET['service_id'];

   //get the contactlist data
   $contact_lists = $contact_helper->getContactListClient($service_id);
} else {
   //if this guy isn't a reseller
   if (!isset($_SESSION['resellerId']) || $_SESSION['serviceId'] != $_SESSION['resellerId']) {
      $service_id = $_SESSION['serviceId'];

      //get the contactlist data
      $contact_lists = $contact_helper->getContactListClient($service_id);
   } else {
      $service_id = -1;
   }
}

//get the list data for a particular ID isf it is set
if (isset($_GET['list_id'])) {
   $list_id = $_GET['list_id'];

   if (!$contact_helper->checkContactListExists($list_id)) {
      $list_id = -1;
   }
} else {
   $list_id = -1;
}

//check the pagination
if (!isset($_GET['page'])) {
   $page = 1;
} else {
   $page = $_GET['page'];
}

//check for a search query
if (isset($_GET['search_type']) && isset($_GET['search_string'])) {
   $search_type = $_GET['search_type'];
   $search_string = $_GET['search_string'];
   $is_search = true;
} else {
   $is_search = false;
   $search_type = "contact_msisdn";
   $search_string = "";
}

//get the list data for a particular ID isf it is set
if (isset($_GET['contacts_deleted'])) {
   $bulk_contacts_deleted = $_GET['contacts_deleted'];
}

switch ($search_type) {
   case 'contact_msisdn' :
      $search_type_display = 'MSISDN';
      break;
   case 'contact_firstname' :
      $search_type_display = 'First Name';
      break;
   case 'contact_lastname' :
      $search_type_display = 'Last Name';
      break;
}



//get the total contacts for this service
$records_per_page = 25;
$total_contacts = $contact_helper->countAllContactsInService($service_id);
$total_orphans = $contact_helper->countAllOrphanContactsInService($service_id);

if ($list_id != -1) {
   $total_contacts_in_this_list = $contact_helper->countContactsInList($list_id);
} else {
   $total_contacts_in_this_list = $total_contacts;
}


//figure out what get variables we need to add to these URLs
if ($is_search) {
   $base_page_url = 'contactlists.php?service_id=' . $service_id . '&list_id=' . $list_id . '&search_type=' . urlencode($search_type) . '&search_string=' . urlencode($search_string);
} else {
   $base_page_url = 'contactlists.php?service_id=' . $service_id . '&list_id=' . $list_id;
}

//Get the content for the table according to the paramaeters caught at the top
if (isset($list_id) && $list_id != -1) {
   if ($is_search) {
      $contact_list_data = $contact_helper->searchContactsInContactList($list_id, $search_type, $search_string, $page, $records_per_page);
   } else {
      $contact_list_data = $contact_helper->getContactsInContactList($list_id, $page, $records_per_page);
   }
} else {
   if ($is_search) {
      $contact_list_data = $contact_helper->searchAllContactsInService($service_id, $search_type, $search_string, $page, $records_per_page);
   } else {
      $contact_list_data = $contact_helper->getAllContactsInService($service_id, $page, $records_per_page);
   }
}

$dsp = 'block;';
?>

<aside class="right-side">
   <section class="content-header">
      <h1>
         Contacts
      </h1>
   </section>

   <section class="content">
      <div class="row col-lg-12">
         <div class="box box-solid" style="height:66px; margin-top:-15px;display:<?php echo $show_service_selector; ?>">
            <form action="contactlists.php" method="get" id="clientList" class="sidebar-form" style="border:0px;">
               <div class="form-group">
                  <label>Service List</label>
                  <select class="form-control" name="client" form="clientList" OnChange="reloadOnSelect(this.value);">
                     <?php
                     if ($service_id == -1) {
                        echo '<option value="-1" SELECTED>Please select a client...</option>';
                     }
                     foreach ($clients as $client) {
                        if ($service_id == $client['service_id']) {
                           echo "<option value='" . $client['service_id'] . "' SELECTED>" . $client['account_name'] . " - " . $client['service_name'] . " - " . $client['service_id'] . "</option>";
                        } else {
                           echo "<option value='" . $client['service_id'] . "'>" . $client['account_name'] . " - " . $client['service_name'] . " - " . $client['service_id'] . "</option>";
                        }
                     }
                     ?>
                  </select>
               </div>
               <br>
            </form>
         </div>
         <?php if ($service_id != -1) { ?>

            <?php
               //show in the case of import success
               if (isset($_GET['import_success']) && $_GET['import_success'] == true) {
                  echo '<div class="callout callout-success">';
                  echo '<h4>Contact File Import Successful</h4>';
                  echo '<p>' . ($_GET['total'] - $_GET['invalid']) . ' contacts were successfully imported from your file. (' . $_GET['duplicates'] . ' duplicates ignored,  ' . $_GET['invalid'] . ' invalid numbers)</p>';
                  echo '</div>';
               }

               if (isset($_GET['error']) && $_GET['error'] == -1) {
                  echo '<div class="callout callout-danger">';
                  echo '<h4>Group Name Exists!</h4>';
                  echo '<p>The group name that you specified already exists.</p>';
                  echo '</div>';
               }

               if(isset($bulk_contacts_deleted))
               {
                  echo '<div class="callout callout-warning">';
                  echo '<h4>Bulk Delete Successful!</h4>';
                  echo '<p>A total of <strong>'.$bulk_contacts_deleted.'</strong> numbers were matched from your file and deleted from your contact list.</strong></p>';
                  echo '</div>';
               }
            ?>
            <div class="nav-tabs-custom">
               <!-- Nav tabs -->
               <ul class="nav nav-tabs">

                  <?php
                     $tab_url = 'contactlists.php?service_id=' . $service_id;
                     $active_list_object = null;

                     //check if the list should show all
                     $all_tab_active = "";
                     if (!isset($list_id) || $list_id == -1)
                     {
                        $all_tab_active = 'class="active"';
                     }

                     echo '<li role="presentation" ' . $all_tab_active . '><a href="' . $tab_url . '"><strong>All Contacts</strong> <span class="badge">' . $total_contacts . '</span></a></li>';

                     //cycle through the lists for the tabs
                     foreach ($contact_lists as $list)
                     {
                        //check if this contactl ist is active
                        $make_active = "";
                        if (isset($list_id) && $list_id == $list['contact_list_id'])
                        {
                           $make_active = 'class="active"';
                           $active_list_object = $list;
                        }

                        $contact_count_in_list = $contact_helper->countContactsInList($list['contact_list_id']);
                        echo '<li role="presentation" ' . $make_active . '><a href="' . $tab_url . '&list_id=' . $list['contact_list_id'] . '">' . $list['contact_list_name'] . ' <span class="badge">' . $contact_count_in_list . '</span></a></li>';
                     }

                     //add a final tab so we can add a new group
                     echo '<li role="presentation"><a href="#" id="btnCreateList"><i class="fa fa-plus"></i> Add Group</a></li>';
                     echo '<input type="hidden" id="total_contact_groups" value="'.count($contact_lists).'"/>'
                  ?>

               </ul>

               <!-- Tab panes -->
               <div class="tab-content">
                  <div role="tabpanel" class="tab-pane active">
                     <!-- CONTACTS IN THE LIST GO HERE -->
                     <?php
                        //get the contact list details and show options
                        $show_group_settings = true;
                        if (!isset($active_list_object)) {
                           $active_list_object = array();
                           $active_list_object['contact_list_code'] = "n/a";
                           $active_list_object['contact_list_name'] = "All Contacts";
                           $active_list_object['contact_list_description'] = "Listed below are all your contacts.";
                           $active_list_object['contact_list_status'] = "ENABLED";
                           $active_list_object['client_id'] = -1;
                           $active_list_object['campaign_id'] = -1;
                           $show_group_settings = false;
                        }
                     ?>
                     <div class="row">
                        <div class="col-lg-8">
                           <h3 style="margin-top:0px;">
                  <?php
                  if ($show_group_settings) {
                     echo 'Group: "' . $active_list_object['contact_list_name'] . '"';

                     //show the disabled/enabled tag
                     if ($active_list_object['contact_list_status'] == "DISABLED") {
                        echo ' <span class="label label-danger" style="font-size:8pt;">' . $active_list_object['contact_list_status'] . '</span> ';
                     } else {
                        echo ' <span class="label label-success" style="font-size:8pt;">' . $active_list_object['contact_list_status'] . '</span> ';
                     }

                     //show the client/campaign tag
                     if (!isset($active_list_object['client_id']) && !isset($active_list_object['campaign_id'])) {
                        $client_campaign_tag = 'All Clients | All Campaigns';
                     } else {
                        $campaign_details = getClientAndCampaignName($active_list_object['campaign_id']);
                        $client_campaign_tag = 'Client: ' . $campaign_details['client_name'] . ' | Campaign: ' . $campaign_details['campaign_name'];
                     }
                     echo ' <span class="label label-primary" style="font-size:8pt;">' . $client_campaign_tag . '</span> ';

                     //show the contact list CODE tag
                     echo ' <span class="label label-info" style="font-size:8pt;">Code: ' . $active_list_object['contact_list_code'] . '</span> ';
                  } else {
                     echo 'All Contacts';
                  }
                  ?>
                           </h3>
                        </div>
                        <div class="col-lg-4" style="text-align:right;">
                           <!-- The ad contact button -->
                           <button class="btn btn-primary btn-sm" type="button" id="btnAddContact" style="background-color:<?php echo $accRGB; ?>;border: 2px solid; border-radius: 6px !important;">
                              <i class="fa fa-plus"></i> Add A Contact
                           </button>

                           <!-- The upload button -->
                           <button class="btn btn-primary btn-sm" type="button" id="btnUploadContacts" style="background-color:<?php echo $accRGB; ?>;border: 2px solid; border-radius: 6px !important;">
                              <i class="fa fa-upload"></i> Upload Contacts
                           </button>

                           <!-- The upload button -->
                           <button class="btn btn-danger btn-sm" type="button" id="btnUploadContactsDelete" style="border: 2px solid; border-radius: 6px !important;">
                              <i class="fa fa-remove"></i> Bulk Delete Upload
                           </button>
                        </div>
                     </div>
                     <div class="row">
                        <!-- The settings button -->
                              <?php if ($show_group_settings) { ?>
                           <div class="col-lg-1">
                              <div class="btn-group" role="group" style="text-align:left;">
                                 <button class="btn btn-primary btn-sm dropdown-toggle" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" style="background-color:<?php echo $accRGB; ?>;border: 2px solid; border-radius: 6px !important;">
                                    <i class="fa fa-cog"></i> Options <span class="caret"></span>
                                 </button>
                                 <ul class="dropdown-menu">
                                    <li><a href="#" class="text-primary" id="btnEditList" list_id="<?php echo $active_list_object['contact_list_id'] ?>">Edit Group</a></li>
                                    <li><a href="#" class="test-danger" id="btnDeleteList" list_id="<?php echo $active_list_object['contact_list_id'] ?>">Delete Group</a></li>
                                 </ul>
                                 <?php
                                    //just want to store the information for our form when editing so we don't have to run a query
                                    echo '<input type="hidden" id="hidListName' . $active_list_object['contact_list_id'] . '" value="' . htmlspecialchars($active_list_object['contact_list_name']) . '" />';
                                    echo '<input type="hidden" id="hidListDescription' . $active_list_object['contact_list_id'] . '" value="' . htmlspecialchars($active_list_object['contact_list_description']) . '" />';
                                    echo '<input type="hidden" id="hidListCode' . $active_list_object['contact_list_id'] . '" value="' . htmlspecialchars($active_list_object['contact_list_code']) . '" />';
                                    echo '<input type="hidden" id="hidListStatus' . $active_list_object['contact_list_id'] . '" value="' . htmlspecialchars($active_list_object['contact_list_status']) . '" />';
                                    echo '<input type="hidden" id="hidListClientId' . $active_list_object['contact_list_id'] . '" value="' . htmlspecialchars($active_list_object['client_id']) . '" />';
                                    echo '<input type="hidden" id="hidListCampaignId' . $active_list_object['contact_list_id'] . '" value="' . htmlspecialchars($active_list_object['campaign_id']) . '" />';
                                 ?>
                              </div>
                           </div>
                           <div class="col-lg-7">
                              <h5><strong>Description:</strong> <?php echo $active_list_object['contact_list_description']; ?></h5>
                           </div>
                              <?php } else { ?>
                           <div class="col-lg-8">
                              <p>
                                 Total <strong><?php echo $total_contacts; ?></strong> contacts. |
                                 Total <strong><?php echo count($contact_lists); ?></strong> contact groups. |
                                 Total <strong><?php echo $total_orphans; ?></strong> orphan contacts. <small>(contacts that are not assigned to a group)</small>
                              </p>
                           </div>
                              <?php } ?>
                        <div class="col-lg-4">
                           <!-- FOR SEARCHING AND FILTERING -->
                           <div class="input-group input-group-sm">
                              <div class="input-group-btn">
                                 <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" id="btnSearchType" search_type="<?php echo $search_type; ?>"><?php echo $search_type_display; ?> <span class="caret"></span></button>
                                 <ul class="dropdown-menu">
                                    <li><a href="#" class="btnSetSearchType" search_type="contact_msisdn">MSISDN</a></li>
                                    <li><a href="#" class="btnSetSearchType" search_type="contact_firstname">First Name</a></li>
                                    <li><a href="#" class="btnSetSearchType" search_type="contact_lastname">Last Name</a></li>
                                 </ul>
                              </div>
                              <input type="text" class="form-control" id="inputSearchField" value="<?php echo $search_string; ?>">
                              <span class="input-group-btn">
                                 <button class="btn btn-default" type="button" id="btnSearchContacts">Search</button>
                              </span>
                           </div>
                        </div>
                     </div>



                     <!-- CONTACTS IN THE LIST GO HERE -->
                     <table class="table table-bordered table-striped table-hover dataTable">
                        <thead>
                           <tr>
                              <th style="width:100px;">Number</th>
                              <th style="width:150px;">Name</th>
                              <th style="width:150px;">Surname</th>
                              <th>Assigned Groups</th>
                              <th style="text-align:right; width:90px;">Actions</th>
                           </tr>
                        </thead>
                        <tbody>
                              <?php
                              if (count($contact_list_data) == 0) {
                                 if ($is_search) {
                                    echo '<tr><td colspan="5" align="center">';
                                       echo '<p style="margin:40px 0 40px 0;">No contacts match your search query.</p>';
                                    echo '</td></tr>';
                                 }
                                 else if(count($contact_lists) == 0)
                                 {
                                    echo '<tr><td colspan="5" align="center">';
                                       echo '<br/>Please create a contact group before adding any contacts.<br/>';
                                       echo '<button class="btn btn-primary" id="btnCreateGroupBig" style="background-color:' . $accRGB . ';border: 2px solid; border-radius: 6px !important; margin:20px 0 40px 0;"><i class="fa fa-plus"></i><br/>Create Contact Group</button>';
                                    echo '</td></tr>';
                                 }
                                 else
                                 {
                                    echo '<tr><td colspan="5" align="center">';
                                       echo '<br/>No contacts exist in this group.<br/>';
                                       echo '<button class="btn btn-primary" id="btnAddSome" style="background-color:' . $accRGB . ';border: 2px solid; border-radius: 6px !important; margin:20px 0 40px 0;"><i class="fa fa-plus"></i><br/>Add Contacts</button>';
                                    echo '</td></tr>';
                                 }
                              }

                              foreach ($contact_list_data as $contact) {
                                 echo '<tr>';
                                 echo '<td><strong>' . $contact['contact_msisdn'] . '</strong></td>';
                                 echo '<td>' . $contact['contact_firstname'] . '</td>';
                                 echo '<td>' . $contact['contact_lastname'] . '</td>';
                                 echo '<td>';

                                 //cycle through the contact lists to see which are active and which are not
                                 foreach ($contact_lists as $list) {
                                    if ($contact_helper->checkContactIsInList($contact['contact_id'], $list['contact_list_id'])) {
                                       echo '<button class="btn btn-success btn-xs btnToggleContactList" contact_id="' . $contact['contact_id'] . '" contact_list_id="' . $list['contact_list_id'] . '" data-toggle="tooltip" data-placement="top" title="Click to toggle this contact in this list." type="button">' . $list['contact_list_name'] . '</button> ';
                                    } else {
                                       echo '<button class="btn btn-secondary btn-xs btnToggleContactList" contact_id="' . $contact['contact_id'] . '" contact_list_id="' . $list['contact_list_id'] . '" data-toggle="tooltip" data-placement="top" title="Click to toggle this contact in this list." type="button">' . $list['contact_list_name'] . '</button> ';
                                    }
                                 }

                                 echo '</td>';
                                 echo '<td align="right">';
                                 echo '<button class="btn btn-primary btn-xs btnEditContact" contact_id="' . $contact['contact_id'] . '" type="button" style="background:' . $accRGB . ';">Edit</button> ';
                                 echo '<button class="btn btn-danger btn-xs btnDeleteContact" contact_id="' . $contact['contact_id'] . '" type="button">Delete</button> ';

                                 //just want to store the information for our form
                                 echo '<input type="hidden" id="hidContactMsisdn' . $contact['contact_id'] . '" value="' . htmlspecialchars($contact['contact_msisdn']) . '" />';
                                 echo '<input type="hidden" id="hidContactFirstname' . $contact['contact_id'] . '" value="' . htmlspecialchars($contact['contact_firstname']) . '" />';
                                 echo '<input type="hidden" id="hidContactLastname' . $contact['contact_id'] . '" value="' . htmlspecialchars($contact['contact_lastname']) . '" />';
                                 echo '</td>';
                                 echo '</tr>';
                              }
                              ?>

                        </tbody>
                     </table>
                     <div class="row">
                        <div class="col-lg-12" style="text-align:right;">
   <?php
   //show details about the contact count on this page
   if ($is_search) {
      if ($list_id == -1) {
         $total_contacts_in_search = $contact_helper->countAllContactsSearch($service_id, $search_type, $search_string);
      } else {
         $total_contacts_in_search = $contact_helper->countContactsInListSearch($list_id, $search_type, $search_string);
      }

      $showing_from = $records_per_page * ($page - 1);
      $showing_to = $showing_from + $records_per_page;
      if ($showing_to > $total_contacts_in_search) {
         $showing_to = $total_contacts_in_search;
      }

      if ($showing_to > 0) {
         echo '<p><small>Showing <strong>' . ($showing_from + 1) . '</strong> to <strong>' . $showing_to . '</strong> of a total <strong>' . $total_contacts_in_search . '</strong> contacts that match your search.</small></p>';
      }

      $total_contacts_in_pages = $total_contacts_in_search;
   } else {
      $showing_from = $records_per_page * ($page - 1);
      $showing_to = $showing_from + $records_per_page;
      if ($showing_to > $total_contacts_in_this_list) {
         $showing_to = $total_contacts_in_this_list;
      }

      if ($showing_to > 0) {
         echo '<p><small>Showing <strong>' . ($showing_from + 1) . '</strong> to <strong>' . $showing_to . '</strong> of a total <strong>' . $total_contacts_in_this_list . '</strong> contacts.</small></p>';
      }

      $total_contacts_in_pages = $total_contacts_in_this_list;
   }
   ?>
                           <!-- PAGINATION GOES HERE -->
                           <nav>
                              <ul class="pagination">
                           <?php
                           if ($total_contacts_in_pages != 0) {

                              /**
                               * PAGINATION CODE IS HERE
                               */
                              $total_pages = ceil($total_contacts_in_pages / $records_per_page);

                              if ($page != 1) {
                                 echo '<li>';
                                 echo '<a href="' . $base_page_url . '&page=1" aria-label="First">';
                                 echo '<span aria-hidden="true" class="glyphicon glyphicon-fast-backward"></span>';
                                 echo '</a>';
                                 echo '</li>';

                                 echo '<li>';
                                 echo '<a href="' . $base_page_url . '&page=' . ($page - 1) . '" aria-label="Previous">';
                                 echo '<span aria-hidden="true" class="glyphicon glyphicon-chevron-left"></span>';
                                 echo '</a>';
                                 echo '</li>';
                              }

                              for ($p = 1; $p <= $total_pages; ++$p) {
                                 //check if we have too many pages to display them all in the paginator
                                 if ($p >= ($page - 5) && $p <= ($page + 5)) {
                                    $active_page_class = '';
                                    $active_link_style = '';
                                    if ($p == $page) {
                                       $active_page_class = 'class="active"';
                                       $active_link_style = ' style="background:' . $accRGB . ';"';
                                    }

                                    echo '<li ' . $active_page_class . '><a href="' . $base_page_url . '&page=' . $p . '" ' . $active_link_style . '>' . $p . '</a></li>';
                                 }
                              }

                              if ($page != $total_pages) {
                                 echo '<li>';
                                 echo '<a href="' . $base_page_url . '&page=' . ($page + 1) . '" aria-label="Next">';
                                 echo '<span aria-hidden="true" class="glyphicon glyphicon-chevron-right"></span>';
                                 echo '</a>';
                                 echo '</li>';

                                 echo '<li>';
                                 echo '<a href="' . $base_page_url . '&page=' . $total_pages . '" aria-label="Last">';
                                 echo '<span aria-hidden="true" class="glyphicon glyphicon-fast-forward"></span>';
                                 echo '</a>';
                                 echo '</li>';
                              }
                           }
                           ?>
                              </ul>
                           </nav>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </section>
   </aside>

   <!-- MODALS HERE -->
   <!-- THE EDIT CONTACT MODAL -->
   <div id="modalContactLists" class="modal fade" tabindex="-1" role="dialog" data-backdrop="static">
      <div class="modal-dialog">
         <div class="modal-content">
            <div class="modal-header" >
               <button type="button" class="close" data-dismiss="modal" aria-label="Close" id="btnModalCloseTop"><span aria-hidden="true">&times;</span></button>
               <h4 class="modal-title" id="modTitleAddList">Add A New Contact Group</h4>
               <h4 class="modal-title" id="modTitleEditList">Edit Contact Group</h4>
               <h4 class="modal-title" id="modTitleAddContact">Add A Contact</h4>
               <h4 class="modal-title" id="modTitleEditContact">Edit Contact</h4>
               <h4 class="modal-title" id="modTitleUploadContacts">Upload CSV Contacts</h4>
               <h4 class="modal-title" id="modTitleUploadContactsDelete">Bulk Delete</h4>
               <h4 class="modal-title" id="modTitleDeleteList">Delete List?</h4>
            </div>
            <div class="modal-body">
               <div class="alert alert-danger" style="display:none;" id="modAlert">
                  <h4><i class="icon fa fa-ban"></i> Error!</h4>
                  <p id="modAlertText">Unfortunately there was an error, please try again.</p>
               </div>
               <!-- for list deletion confirmation -->
               <div id="modBodyDeleteList">
                  <h4>Are you sure you want to permanently delete this group?</h4>
                  <div class="checkbox">
                     <label>
                        <input type="checkbox" id="checkDeleteContactsInGroup"> <small>Delete all unique contacts in this group. (Ignores contacts that are members of other groups)</small>
                     </label>
                  </div>
               </div>
               <!-- for contact list creation and editing -->
               <div id="modBodyAddEditList">
                  <div class="form-group">
                     <label for="inputListName">Contact Group Name</label>
                     <input type="text" class="form-control" id="inputListName" placeholder="Contact group name...">
                  </div>
                  <div class="form-group">
                     <label for="inputListDescription">Contact Group Description</label>
                     <input type="text" class="form-control" id="inputListDescription" placeholder="Contact group description...">
                  </div>
                  <div class="form-group">
                     <label for="inputListCode">Contact Group Code</label>
                     <input type="text" class="form-control" id="inputListCode" placeholder="Contact group code...">
                  </div>
                  <div class="form-group">
                     <label for="inputClientCampaign">Client/Campaign</label>
                     <select class="form-control" id="inputListClientCampaign">
                        <option value="-1">Client: (All Clients) - Campaign: (All Campaigns)</option>;
                                 <?php
                                 $campaigns = getCampaignData($service_id);
                                 foreach ($campaigns as $campaign) {
                                    echo '<option value="' . $campaign['client_id'] . ',' . $campaign['campaign_id'] . '">Client: ' . $campaign['client_name'] . ' - Campaign: ' . $campaign['campaign_name'] . '</option>';
                                 }
                                 ?>
                     </select>
                  </div>
                  <div class="form-group">
                     <label for="inputGroupStatus">Status</label>
                     <select class="form-control" id="inputListStatus">
                        <option value="ENABLED">ENABLED</option>
                        <option value="DISABLED">DISABLED</option>
                     </select>
                  </div>
               </div>
               <!-- for contact number creation and editing -->
               <div id="modBodyAddEditContact">
                  <p>Please insure that you add a valid cell phone number which includes the correct country code (E.G. 27821234567).</p>
                  <div class="form-group">
                     <label for="inputContactFirstName">First Name</label>
                     <input type="text" class="form-control" id="inputContactFirstName" placeholder="The contact's first name...">
                  </div>
                  <div class="form-group">
                     <label for="inputContactLastName">Last Name</label>
                     <input type="text" class="form-control" id="inputContactLastName" placeholder="The contact's surname...">
                  </div>
                  <div class="form-group">
                     <label for="inputMSISDN">Cellphone Number</label>
                     <input type="text" class="form-control" id="inputContactMSISDN" placeholder="The contact's cellphone number...">
                  </div>
               </div>
               <!-- for contact file uploading -->
               <div id="modBodyUploadContacts">
                  <form id="import_form" action="../php/ajaxContactLists.php" method="post">
                     <div  class="row">
                        <div class="col-sm-12">
                           <p id="txtUploadContacts">
                              Please insure your file contains valid phone numbers with the country code (E.G. 27821234567), as well as a first name column and a last name column.<br/>
                              <br/>
                              <strong>Your file must be 5MB or less.</strong>
                           </p>
                           <p id="txtUploadContactsDelete">
                              <strong>Bulk delete allows you to remove a large amount of contacts by uploading a file.</strong>
                              <br/><br/>
                              Please upload a csv file with a single column of numbers, formatted with the country code (E.G. 27821234567).
                              <br/>
                              These numbers will be compared with numbers in your contact list,
                              and if any numbers match, the corresponding number in your contact list will be removed automatically.
                           </p>
                           <label for="import_file">Select your import file:</label>
                           <input name="import_file" id="import_file" type="file" autocomplete="off"/>
                           <input type="hidden" name="task" id="upload_task" value="ajax_upload"/>
                           <input type="hidden" name="service_id" value="<?php echo $service_id; ?>"/>
                        </div>
                     </div>
                  </form>
                  <div class="row" id="form_progress_holder" style="display:none;">
                     <div class="col-sm-12">
                        <h3>Uploading file... Please wait...
                           <br/><small>This process can take long with large files.</small></h3>
                        <div class="progress">
                           <div class="progress-bar progress-bar-striped" id="progress_bar" role="progressbar" aria-valuenow="2" aria-valuemin="0" aria-valuemax="100" style="min-width:2em; width:2%;">
                              2%
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
            <div class="modal-footer">
               <button type="button" class="btn btn-default" data-dismiss="modal" id="btnModalCloseBottom">Close</button>
               <button type="button" class="btn btn-primary" id="btnModalSave" style="background-color:<?php echo $accRGB; ?>;border: 2px solid; border-radius: 6px !important;">Save</button>
            </div>
         </div><!-- /.modal-content -->
      </div><!-- /.modal-dialog -->
   </div><!-- /.modal -->

<?php } ?>

<!--  THE JAVASCRIPT IMPORT ALWAYS APPEARS BEFORE LOCAL JAVASCRIPT AT THE BOTTOM OF THE PAGE -->
<?php include("template_import_script.php"); //must import all scripts first   ?>
<!-- END JAVASCRIPT IMPORT -->

<script type="text/javascript">

   var serviceId = <?php echo $service_id; ?>;
   var selectedListId = <?php echo $list_id; ?>;
   function reloadOnSelect(serviceId)
   {
      $(".loader").fadeIn("slow");
      $(".loaderIcon").fadeIn("slow");
      window.location = "contactlists.php?service_id=" + serviceId;
   }

   //shows the title of the modal
   function showModalTitle(titleId)
   {
      //hide all the other titles
      $("#modTitleAddList").hide();
      $("#modTitleEditList").hide();
      $("#modTitleAddContact").hide();
      $("#modTitleEditContact").hide();
      $("#modTitleUploadContacts").hide();
      $("#modTitleDeleteList").hide();
      $('#modTitleUploadContactsDelete').hide();

      $(titleId).show();
   }

   //shows the correct body of the modal
   function showModalBody(bodyId)
   {
      //reset the modal
      $("#btnModalSave").off("click");
      hideModalError();

      $("#modBodyDeleteList").hide();
      $("#modBodyAddEditList").hide();
      $("#modBodyAddEditContact").hide();
      $("#modBodyUploadContacts").hide();

      $(bodyId).show();
   }

   //shows the error in the MODAL with a custom error message
   function showModalError(message)
   {
      $("#modAlertText").html(message);
      $("#modAlert").show();
   }

   //hides the modal error and resets the message
   function hideModalError()
   {
      $("#modAlertText").html();
      $("#modAlert").hide();
   }

   //this locks the modal so the user can't do anything, useful when runing ajax calls off of it
   var prevButtonContent = "";
   function lockModalBusy(busyMessage)
   {
      prevButtonContent = $('#btnModalSave').html();
      $('#btnModalCloseTop').prop("disabled", true);
      $('#btnModalCloseBottom').prop("disabled", true);
      $('#btnModalSave').prop("disabled", false);
      $('#btnModalSave').html('<span class="glyphicon glyphicon-refresh glyphicon-spin"></span> ' + busyMessage);
   }

   //unlocks the modal so the user can close it
   function unlockModalBusy()
   {
      $('#btnModalCloseTop').prop("disabled", false);
      $('#btnModalCloseBottom').prop("disabled", false);
      $('#btnModalSave').prop("disabled", false);
      $('#btnModalSave').html(prevButtonContent);
      prevButtonContent = "";
   }

   //shows the create modal for group creation
   function startEditListModal(listId)
   {
      showModalTitle("#modTitleEditList");
      showModalBody("#modBodyAddEditList");

      //get the details from the list
      $("#inputListName").val($("#hidListName" + listId).val());
      $("#inputListDescription ").val($("#hidListDescription" + listId).val());
      $("#inputListCode").val($("#hidListCode" + listId).val());
      $("#inputListStatus").val($("#hidListStatus" + listId).val());

      var getClientId = $("#hidListClientId" + listId).val();
      var getCampaignId = $("#hidListCampaignId" + listId).val();
      if (getClientId == '' && getCampaignId == '')
      {
         var campaignClient = -1;
      } else
      {
         var campaignClient = getClientId + ',' + getCampaignId;
      }

      $("#inputListClientCampaign").val(campaignClient);

      $("#btnModalSave").html("Save Group");
      $("#btnModalSave").on("click", function (e)
      {
         e.preventDefault();
         lockModalBusy('Saving...');

         //get the data to save
         var listName = $("#inputListName").val();
         var listDescription = $("#inputListDescription ").val();
         var listCode = $("#inputListCode").val();
         var listStatus = $("#inputListStatus").val();

         //extract the client and campaign information
         var listClientCampaign = $("#inputListClientCampaign").val();
         var clientId = -1;
         var campaignId = -1;
         if (listClientCampaign != "-1")
         {
            var idArray = listClientCampaign.split(",");
            clientId = idArray[0];
            campaignId = idArray[1];
         }

         if (listName == "" || listDescription == "" || listCode == "" || listStatus == "")
         {
            showModalError("Please fill out all fields.");
            unlockModalBusy();
         } else
         {
            //run our file processor before submitting it to niel
            $.post("../php/ajaxContactLists.php", {
               task: "save_list",
               serviceId: serviceId,
               listId: listId,
               listName: listName,
               listDescription: listDescription,
               listCode: listCode,
               listStatus: listStatus,
               clientId: clientId,
               campaignId: campaignId
            }).done(function (data) {
               var json = $.parseJSON(data);

               if (json.success) {
                  location.reload();
               } else {
                  unlockModalBusy();
                  showModalError("We were unable to save this Group.");
               }

            }).fail(function (data) {
               unlockModalBusy();
               showModalError("There was a server error, please try again.");
            });
         }
      });

      $('#modalContactLists').modal('show');
   }

   //shows the create modal for group creation
   function startCreateListModal()
   {
      showModalTitle("#modTitleAddList");
      showModalBody("#modBodyAddEditList");

      //reset the form
      $("#inputListName").val("");
      $("#inputListDescription ").val("");
      $("#inputListCode").val("");
      $("#inputListStatus").val("ENABLED");
      $("#inputListClientCampaign").val("-1");

      $("#btnModalSave").html("Create Group");
      $("#btnModalSave").on("click", function (e)
      {
         e.preventDefault();
         lockModalBusy('Saving...');

         //get the data to save
         var listName = $("#inputListName").val();
         var listDescription = $("#inputListDescription ").val();
         var listCode = $("#inputListCode").val();
         var listStatus = $("#inputListStatus").val();

         //extract the client and campaign information
         var listClientCampaign = $("#inputListClientCampaign").val();
         var clientId = -1;
         var campaignId = -1;
         if (listClientCampaign != "-1")
         {
            var idArray = listClientCampaign.split(",");
            clientId = idArray[0];
            campaignId = idArray[1];
         }

         if (listName == "" || listDescription == "" || listCode == "" || listStatus == "")
         {
            showModalError("Please fill out all fields.");
            unlockModalBusy();
         } else
         {
            //run our ajaxController to save the details
            $.post("../php/ajaxContactLists.php", {
               task: "save_list",
               serviceId: serviceId,
               listId: -1,
               listName: listName,
               listDescription: listDescription,
               listCode: listCode,
               listStatus: listStatus,
               clientId: clientId,
               campaignId: campaignId
            }).done(function (data) {
               var json = $.parseJSON(data);

               if (json.success) {
                  window.location = "<?php echo $base_page_url; ?>&add_contact_success=true&list_id="+json.contact_list_id;
               } else {
                  unlockModalBusy();
                  showModalError("We were unable to save this Group.");
               }

            }).fail(function (data) {
               unlockModalBusy();
               showModalError("There was a server error, please try again.");
            });
         }
      });

      $('#modalContactLists').modal('show');
   }

   //shows the a popup that allows the user to confirm deletion of a list
   function confirmListDelete(listId)
   {
      showModalTitle("#modTitleDeleteList");
      showModalBody("#modBodyDeleteList");

      $("#btnModalSave").html("Delete List");
      $("#btnModalSave").on("click", function (e)
      {
         e.preventDefault();
         lockModalBusy('Deleting...');

         if ($("#checkDeleteContactsInGroup").is(':checked'))
         {
            var deleteContactsInList = 1;
         } else
         {
            var deleteContactsInList = 0;
         }

         //run our ajaxController to save the details
         $.post("../php/ajaxContactLists.php", {task: "delete_list", listId: listId, deleteContactsInList: deleteContactsInList}).done(function (data) {
            var json = $.parseJSON(data);
            if (json.success)
            {
               window.location = "<?php echo $base_page_url; ?>";
            } else
            {
               unlockModalBusy();
               showModalError("A server error occurred and we could not delete your list, please contact technical support.");
            }
         }).fail(function (data) {
            unlockModalBusy();
            showModalError("A server error occurred and we could not delete your list, please contact technical support.");
         });
      });

      $('#modalContactLists').modal('show');
   }

   //shows the contact modal so the user can create a new contact
   function startAddContactModal()
   {
      showModalTitle("#modTitleAddContact");
      showModalBody("#modBodyAddEditContact");

      //get the details from the list
      $("#inputContactFirstName").val("");
      $("#inputContactLastName ").val("");
      $("#inputContactMSISDN").val("");

      $("#btnModalSave").html("Create Contact");
      $("#btnModalSave").on("click", function (e)
      {
         e.preventDefault();
         lockModalBusy('Saving...');

         //get the data to save
         var firstName = $("#inputContactFirstName").val();
         var lastName = $("#inputContactLastName ").val();
         var msisdn = $("#inputContactMSISDN").val();

         if (msisdn == "")
         {
            showModalError("Please fill out the MSISDN field.");
            unlockModalBusy();
         } else
         {
            //run our ajaxController to save the details
            $.post("../php/ajaxContactLists.php", {
               task: "save_contact",
               serviceId: serviceId,
               contactId: -1,
               listId: selectedListId,
               firstName: firstName,
               lastName: lastName,
               msisdn: msisdn
            }).done(function (data) {
               var json = $.parseJSON(data);

               if (json.success) {
                  location.reload();
               } else {
                  unlockModalBusy();
                  showModalError("We were unable to save this Contact.");
               }

            }).fail(function (data) {
               unlockModalBusy();
               showModalError("There was a server error, please try again.");
            });
         }
      });

      $('#modalContactLists').modal('show');
   }

   //shows the contact modal, extracts contact details and populates the form so the user can modify a contact
   function startEditContactModal(contactId)
   {
      showModalTitle("#modTitleEditContact");
      showModalBody("#modBodyAddEditContact");

      //get the details from the list
      $("#inputContactFirstName").val($("#hidContactFirstname" + contactId).val());
      $("#inputContactLastName ").val($("#hidContactLastname" + contactId).val());
      $("#inputContactMSISDN").val($("#hidContactMsisdn" + contactId).val());


      $("#btnModalSave").html("Save Contact");
      $("#btnModalSave").on("click", function (e)
      {
         e.preventDefault();
         lockModalBusy('Saving...');

         //get the data to save
         var firstName = $("#inputContactFirstName").val();
         var lastName = $("#inputContactLastName ").val();
         var msisdn = $("#inputContactMSISDN").val();

         if (msisdn == "")
         {
            showModalError("Please fill out the MSISDN field.");
            unlockModalBusy();
         } else
         {
            //run our ajaxController to save the details
            $.post("../php/ajaxContactLists.php", {
               task: "save_contact", serviceId: serviceId, contactId: contactId, firstName: firstName, lastName: lastName, msisdn: msisdn
            }).done(function (data) {
               var json = $.parseJSON(data);

               if (json.success)
               {
                  location.reload();
               } else
               {
                  unlockModalBusy();
                  showModalError("We were unable to save this contact.");
               }

            }).fail(function (data) {
               unlockModalBusy();
               showModalError("There was a server error, please try again.");
            });
         }
      });

      $('#modalContactLists').modal('show');
   }

   //shows the a popup that allows the user to confirm deletion of a particular contact
   function confirmContactDelete(contactId)
   {
      if (confirm("Are you sure you want to permanently delete this contact? It will be removed from any active contact lists."))
      {
         $(".loader").fadeIn("fast");
         $(".loaderIcon").fadeIn("fast");

         //run our ajaxController to save the details
         $.post("../php/ajaxContactLists.php", {task: "delete_contact", contactId: contactId}).done(function (data) {
            var json = $.parseJSON(data);

            if (json.success)
            {
               window.location = "<?php echo $base_page_url; ?>";
            } else
            {
               alert("A server error occurred and we could not delete your contact, please contact technical support.");
            }
         }).fail(function (data) {
            alert("A server error occurred and we could not delete your contact, please contact technical support.");
         });
      }
   }

   //this function is triggered when the user clicks on the contact group button on a contact row, which will either add it to that group or remove it from that group
   function toggleContactList(jButton, contactId, contactListId)
   {
      var buttonTextHolder = jButton.html();
      jButton.html('<span class="glyphicon glyphicon-refresh glyphicon-spin"></span> Saving...');

      //run our ajaxController to save the details
      $.post("../php/ajaxContactLists.php", {task: "toggle_contact_list", contactId: contactId, contactListId: contactListId}).done(function (data) {
         var json = $.parseJSON(data);

         if (json.success)
         {
            if (json.contact_list_toggle == "added")
            {
               jButton.html(buttonTextHolder);
               jButton.addClass("btn-success");
               jButton.removeClass("btn-secondary");
            } else if (json.contact_list_toggle == "removed")
            {
               jButton.html(buttonTextHolder);
               jButton.addClass("btn-secondary");
               jButton.removeClass("btn-success");
            }
         } else
         {
            alert("A server error occurred and we could not toggle this contact list reference, please contact technical support.");
         }
      }).fail(function (data) {
         alert("A server error occurred and we could not toggle this contact list reference, please contact technical support.");
      });
   }

   //this function grabs the search variables and refreshes the page to run a search
   function runSearchForContact()
   {
      var searchType = $('#btnSearchType').attr('search_type');
      var searchString = $('#inputSearchField').val();

      window.location = "contactlists.php?service_id=" + serviceId + "&list_id=" + selectedListId + "&search_type=" + encodeURIComponent(searchType) + "&search_string=" + encodeURIComponent(searchString);
   }


   $(document).ready(function (e)
   {
      //hide the loading popup if it is showing
      $(".loader").fadeOut("fast");
      $(".loaderIcon").fadeOut("fast");

      /*************************************
       * Our button listeners for the page
       *************************************/
      $("#btnCreateList").on("click", function (e)
      {
         e.preventDefault();
         startCreateListModal();
      });

      $("#btnDeleteList").on("click", function (e)
      {
         e.preventDefault();

         var listId = $(this).attr('list_id');
         confirmListDelete(listId);
      });

      $("#btnEditList").on("click", function (e)
      {
         e.preventDefault();

         var listId = $(this).attr('list_id');
         startEditListModal(listId);
      });

      $("#btnAddContact").on("click", function (e)
      {
         e.preventDefault();

         var totalContactGroups = $('#total_contact_groups').val();

         if(totalContactGroups > 0)
         {
            startAddContactModal();
         }
         else
         {
            alert("Please create a contact group before importing data.");
         }
      });

      $("#btnCreateGroupBig").on("click", function (e)
      {
         e.preventDefault();
         startCreateListModal();
      });

      $("#btnAddSome").on("click", function (e)
      {
         e.preventDefault();
         startAddContactModal();
      });


      $(".btnEditContact").on("click", function (e)
      {
         e.preventDefault();

         var contactId = $(this).attr('contact_id');
         startEditContactModal(contactId);
      });

      $(".btnDeleteContact").on("click", function (e)
      {
         e.preventDefault();

         var contactId = $(this).attr('contact_id');
         confirmContactDelete(contactId);
      });

      $(".btnToggleContactList").on("click", function (e)
      {
         e.preventDefault();

         var contactId = $(this).attr('contact_id');
         var contactListId = $(this).attr('contact_list_id');
         toggleContactList($(this), contactId, contactListId);
      });

      /**********************************
       * STARTING THE UPLOAD MODAL
       **********************************/
      $("#btnUploadContacts").on("click", function (e)
      {
         e.preventDefault();

         var totalContactGroups = $('#total_contact_groups').val();

         if(totalContactGroups > 0)
         {
            startUploadCSVModal('ajax_upload');
         }
         else
         {
            alert("Please create a contact group before importing data.");
         }
      });

      /**********************************
       * STARTING THE DELETE UPLOAD MODAL
       **********************************/
      $("#btnUploadContactsDelete").on("click", function (e)
      {
         e.preventDefault();

         startUploadCSVModal('bulk_delete')
      });

      /**********************************
       * BUTTONS FOR HANDLING SEARCHING
       **********************************/
      $(".btnSetSearchType").on("click", function (e)
      {
         e.preventDefault();

         var searchType = $(this).attr('search_type');

         $("#btnSearchType").attr('search_type', searchType);
         $("#btnSearchType").html($(this).html());
      });


      $("#btnSearchContacts").on("click", function (e)
      {
         e.preventDefault();

         runSearchForContact();
      });

      /*****************************************************************
       * JUST MAKES SURE WE PROPERLY LIMIT MSISDN TO NUMERIC CHARACTERS
       *****************************************************************/
      $("#inputContactMSISDN").keydown(function (e) {
         // Allow: backspace, delete, tab, escape, enter and .
         if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 110, 190]) !== -1 ||
                 // Allow: Ctrl+A
                         (e.keyCode == 65 && e.ctrlKey === true) ||
                         // Allow: Ctrl+C
                                 (e.keyCode == 67 && e.ctrlKey === true) ||
                                 // Allow: Ctrl+X
                                         (e.keyCode == 88 && e.ctrlKey === true) ||
                                         // Allow: home, end, left, right
                                                 (e.keyCode >= 35 && e.keyCode <= 39)) {
                                    // let it happen, don't do anything
                                    return;
                                 }

                                 // Ensure that it is a number and stop the keypress
                                 if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
                                    e.preventDefault();
                                 }
                              });

                   });

           /*****************************************************
            * SCRIPT FOR HANDLING THE UPLOADING OF A CONTACT CSV
            *****************************************************/
           //shows the create modal for group creation
           function startUploadCSVModal(uploadTask)
           {
              if(uploadTask == "bulk_delete")
              {
                 showModalTitle("#modTitleUploadContactsDelete");
                 showModalBody("#modBodyUploadContacts");
                 $('#txtUploadContactsDelete').show();
                 $('#txtUploadContacts').hide();
                 $('#upload_task').val('ajax_upload_bulk_delete');
              }
              else
              {
                 showModalTitle("#modTitleUploadContacts");
                 showModalBody("#modBodyUploadContacts");
                 $('#txtUploadContactsDelete').hide();
                 $('#txtUploadContacts').show();
                 $('#upload_task').val('ajax_upload');
              }

              $('#import_file').val("");

              $("#btnModalSave").html("Start Upload");
              $("#btnModalSave").on("click", function (e)
              {
                 e.preventDefault();
                 hideModalError();
                 lockModalBusy('Uploading...');

                 var startUpload = true;
                 if ($('#import_file').val() == "")
                 {
                    startUpload = false;
                    showModalError("Please select a file to upload.");
                    unlockModalBusy();
                 }
                 else
                 {
                     //check file properties
                     var file = $('#import_file')[0].files[0];

                     console.log("File type: " + file.type);
                     //limit to 5 megs
                     if (file.size > 5242880) //for 2mb - 2097152
                     {
                        startUpload = false;
                        showModalError("The file you selected is too big.");
                        unlockModalBusy();
                     }
                     else if(file.type == '')
                     {
                        var ext = file.name.split('.').pop();

                        if(ext != 'csv')
                        {
                           startUpload = false;
                           showModalError("The file you selected is the wrong format.");
                           unlockModalBusy();
                        }
                     }
                     else if (file.type != 'text/plain' && file.type != 'application/csv' && file.type != 'text/csv' &&
                         file.type != 'application/vnd.ms-excel' && file.type != 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet')
                     {
                        startUpload = false;
                        showModalError("The file you selected is the wrong format.");
                        unlockModalBusy();
                     }
               }

                 if (startUpload)
                 {
                    //fade out the form and fade in the progress bar, then start the actual upload
                    $('#import_form').fadeOut(500, function ()
                    {
                       $('#form_progress_holder').fadeIn(500, function ()
                       {
                          $('#form_progress_holder').show();
                          startAjaxContactUpload();
                       });
                    });
                 }
              });

              $('#modalContactLists').modal('show');
           }

           //This function runs the ajax upload submitter from the jquery.form library
           var uploadError = "";
           function startAjaxContactUpload() {
              $('#progress_bar').addClass('active');

              //$('#loader-icon').show();
              $('#import_form').ajaxSubmit({
                 beforeSubmit: function () {
                    updateProgress(2);
                 },
                 uploadProgress: function (event, position, total, percentComplete) {
                    updateProgress(percentComplete);
                 },
                 success: function (responseText, statusText, xhr, form) {
                    if (responseText != "")
                    {
                       var jsonObj = $.parseJSON(responseText)

                       //upload was successful? redirect to the data assignment page so the user can configure the data
                       if (jsonObj.success == true)
                       {
                          endProgress();
                          if(jsonObj.task == 'ajax_upload_bulk_delete')
                          {
                             window.location = "<?php echo $base_page_url; ?>&contacts_deleted=" + jsonObj.contact_delete_count;
                          }
                          else
                          {
                             window.location = "contactlists_import_data.php?file_path=" + encodeURIComponent(jsonObj.full_path) + "&service_id=" + serviceId + "&list_id=" + selectedListId;
                          }

                       } else
                       {
                          //shit, there was a bloody error, act accordingly
                          uploadError = jsonObj.reason;
                          $('#form_alert').addClass('hidden');
                          $('#form_progress_holder').fadeOut(300, function () {
                             $('#import_form').fadeIn(300, function () {
                                showModalError("There was an error uploading this file. Reason: " + uploadError);
                                unlockModalBusy();
                             });
                          });
                       }
                    } else
                    {
                       //dismal error, act accordingly as well
                       $('#form_alert').addClass('hidden');
                       $('#form_progress_holder').fadeOut(300, function () {
                          $('#import_form').fadeIn(300, function () {
                             showModalError("There was an error uploading this file. Please try again.");
                             unlockModalBusy();
                          });
                       });
                    }
                    //$('#loader-icon').hide();
                 },
                 resetForm: false
              });
              return false;
           }


           //updates the progress bar to show that the upload is complete
           function endProgress()
           {
              $('#progress_bar').html('File upload complete, moving on...');
           }

           //updates the progress bar with the correct numbers and width
           function updateProgress(percentage)
           {
              if (percentage > 100)
                 percentage = 100;
              $('#progress_bar').css('width', percentage + '%');
              $('#progress_bar').html(percentage + '%');
           }

</script>

<!-- Template Footer -->

