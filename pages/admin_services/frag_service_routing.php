<?php

   /**
    * This script is called by the manage services page, and runs certain ajax based tasks
    * @author - Doug Jenkinson
    */
   require_once("../../php/allInclusive.php");
   require_once("../../PluginAPI/PluginAPIAutoload.php");

   session_start();

   $service_helper = new sqlHelperServices();

   if(isset($_POST['service_id']))
   {
      $service_id = $_POST['service_id'];
   }
   else if(isset($_GET['service_id']))
   {
      $service_id = $_GET['service_id'];
   }
   else
   {
      echo '<p class="text-error">An error occurred, please refresh your browser.</p>';
      die();
   }

   //get the service object
   $service_obj = new Service($service_id);


?>
<!-- PAGE LEAD DESCRIPTION -->

<div class="row">
   <div class="col-lg-10">
      <p class="lead">You can view and configure this service's routing and collection information in this tab.</p>
   </div>
   <div class="col-lg-2">
      <button id="btnAddNewRoute" class="btn btn-primary btn-sm btn-block"><i class="fa fa-plus"></i> Add New Route</button>
   </div>
</div>
<hr/>

<!-- LOCAL ROUTES ARE DISPLAYED SEPARATELY -->
<h3>Local Routing</h3>
<hr/>
<div class="row">
   <?php if(count($service_obj->local_routes) == 0) { ?>
      <div class="col-lg-12" style="text-align:center;">
         <p class="text-warning">No local routes exist on this account.</p>
      </div>
   <?php } ?>
   <?php foreach($service_obj->local_routes as $route) { ?>
      <div class="col-lg-6">
         <div class="box box-solid connet-box-to-button btn-edit-route" connet-route-id="<?php echo $route['route_id']; ?>" data-toggle="tooltip" data-placement="top" title="Click to edit this route.">
            <div class="box-header with-border bg-gray">
               <h3 class="box-title text-primary"><img src="../<?php echo $route['logo_path']; ?>"> &nbsp;<?php echo htmlentities($route['NETWORK']); ?> <small>(Currency: <strong><?php echo htmlentities($route['route_currency']); ?></strong>)</small></h3>
               <h3 class="box-title pull-right"><small><?php echo htmlentities($route['COUNTRY']); ?></small> <img src="../<?php echo $route['flag_path']; ?>"/></h3>
            </div>
            <div class="box-body">
               <div class="row">
                  <div class="col-lg-4">
                     <small>Status:</small><br/>
                     <strong class=" <?php echo ($route['STATUS'] == "ENABLED" ? 'text-success' : 'text-danger'); ?>"><?php echo htmlentities($route['STATUS']); ?></strong>
                  </div>
                  <div class="col-lg-4">
                     <small>Rate:</small><br/>
                     <strong><?php echo htmlentities($route['real_rate']); ?></strong>
                  </div>
                  <div class="col-lg-4">
                     <small>RDNC:</small><br/>
                     <strong><?php echo htmlentities($route['rdnc']); ?></strong>
                  </div>
                  <div class="col-lg-4">
                     <small>Collection:</small><br/>
                     <strong><?php echo htmlentities($route['COLLECTION']); ?></strong>
                  </div>
                  <div class="col-lg-4">
                     <small>Default Route:</small><br/>
                     <strong><img src="../img/routes/<?php echo htmlentities(($route['DEFAULT'] ? $route['DEFAULT'] : 'n/a')); ?>.png"> <?php echo htmlentities(($route['DEFAULT'] ? $route['DEFAULT'] : 'n/a')); ?></strong>
                  </div>
                  <div class="col-lg-4">
                     <small>Ported Route:</small><br/>
                     <strong><img src="../img/routes/<?php echo htmlentities(($route['PORTED'] ? $route['PORTED'] : 'n/a')); ?>.png"> <?php echo htmlentities(($route['PORTED'] ? $route['PORTED'] : 'n/a')); ?></strong>
                  </div>
               </div>
               <?php
                  $meta = $route['route_meta'];
                  $meta_array = array();
                  parse_str($meta, $meta_array);
               ?>
               <?php if(count($meta_array) > 0) { ?>
                  <div class="row">
                     <div class="col-lg-12">
                        <small>Meta:</small><br/>
                        <?php foreach($meta_array as $meta_name => $meta_value) { ?>
                           <div class="label label-default"><?php echo htmlentities($meta_name); ?>: <?php echo htmlentities($meta_value); ?></div>
                        <?php } ?>
                     </div>
                  </div>
               <?php } ?>

            </div>
         </div>
         <input type="hidden" id="routeEditCountryName<?php echo $route['route_id']; ?>" value="<?php echo htmlentities($route['COUNTRY']); ?>" />
         <input type="hidden" id="routeEditNetworkMCCMNC<?php echo $route['route_id']; ?>" value="<?php echo htmlentities($route['MCCMNC']); ?>" />
         <input type="hidden" id="routeEditNetwork<?php echo $route['route_id']; ?>" value="<?php echo htmlentities($route['NETWORK']); ?>" />
         <input type="hidden" id="routeEditCollectionId<?php echo $route['route_id']; ?>" value="<?php echo htmlentities($route['route_collection_id']); ?>" />
         <input type="hidden" id="routeEditRate<?php echo $route['route_id']; ?>" value="<?php echo htmlentities($route['real_rate']); ?>" />
         <input type="hidden" id="routeEditRDNC<?php echo $route['route_id']; ?>" value="<?php echo htmlentities($route['rdnc']); ?>" />
         <input type="hidden" id="routeEditStatus<?php echo $route['route_id']; ?>" value="<?php echo htmlentities($route['STATUS']); ?>" />
      </div>
   <?php } ?>
</div>
<hr/>

<!-- INTERNATIONAL ROUTES ARE DISPLAYED SEPARATELY -->
<h3>International Routing</h3>
<hr/>
<div class="row">
   <?php if(count($service_obj->international_routes) == 0) { ?>
      <div class="col-lg-12" style="text-align:center;">
          <p class="text-warning">No international routes exist on this account.</p>
      </div>
   <?php } ?>
   <?php foreach($service_obj->international_routes as $route) {?>
      <div class="col-lg-6">
         <div class="box box-solid connet-box-to-button btn-edit-route" connet-route-id="<?php echo $route['route_id']; ?>" data-toggle="tooltip" data-placement="top" title="Click to edit this route.">
            <div class="box-header with-border bg-gray">
               <h3 class="box-title text-primary"><img src="../<?php echo $route['logo_path']; ?>"> &nbsp;<?php echo htmlentities($route['NETWORK']); ?> <small>(Currency: <strong><?php echo htmlentities($route['route_currency']); ?></strong>)</small></h3>
               <h3 class="box-title pull-right"><small><?php echo htmlentities($route['COUNTRY']); ?></small> <img src="../<?php echo $route['flag_path']; ?>"/></h3>
            </div>
            <div class="box-body">
               <div class="row">
                  <div class="col-lg-4">
                     <small>Status:</small><br/>
                     <strong class=" <?php echo ($route['STATUS'] == "ENABLED" ? 'text-success' : 'text-danger'); ?>"><?php echo htmlentities($route['STATUS']); ?></strong>
                  </div>
                  <div class="col-lg-4">
                     <small>Rate:</small><br/>
                     <strong><?php echo htmlentities($route['real_rate']); ?></strong>
                  </div>
                  <div class="col-lg-4">
                     <small>RDNC:</small><br/>
                     <strong><?php echo htmlentities($route['rdnc']); ?></strong>
                  </div>
                  <div class="col-lg-4">
                     <small>Collection:</small><br/>
                     <strong><?php echo htmlentities($route['COLLECTION']); ?></strong>
                  </div>
                  <div class="col-lg-4">
                     <small>Default Route:</small><br/>
                     <strong><img src="../img/routes/<?php echo htmlentities(($route['DEFAULT'] ? $route['DEFAULT'] : 'n/a')); ?>.png"> <?php echo htmlentities(($route['DEFAULT'] ? $route['DEFAULT'] : 'n/a')); ?></strong>
                  </div>
                  <div class="col-lg-4">
                     <small>Ported Route:</small><br/>
                     <strong><img src="../img/routes/<?php echo htmlentities(($route['DEFAULT'] ? $route['DEFAULT'] : 'n/a')); ?>.png"> <?php echo htmlentities(($route['PORTED'] ? $route['PORTED'] : 'n/a')); ?></strong>
                  </div>
               </div>
               <?php
                  //for the route meta
                  $meta = $route['route_meta'];
                  $meta_array = array();
                  parse_str($meta, $meta_array);
               ?>
               <?php if(count($meta_array) > 0) { ?>
                  <div class="row">
                     <div class="col-lg-12">
                        <small>Meta:</small><br/>
                        <?php foreach($meta_array as $meta_name => $meta_value) { ?>
                           <div class="label label-default"><?php echo htmlentities($meta_name); ?>: <?php echo htmlentities($meta_value); ?></div>
                        <?php } ?>
                     </div>
                  </div>
               <?php } ?>
            </div>

            <input type="hidden" id="routeEditCountryName<?php echo $route['route_id']; ?>" value="<?php echo htmlentities($route['COUNTRY']); ?>" />
            <input type="hidden" id="routeEditNetworkMCCMNC<?php echo $route['route_id']; ?>" value="<?php echo htmlentities($route['MCCMNC']); ?>" />
            <input type="hidden" id="routeEditNetwork<?php echo $route['route_id']; ?>" value="<?php echo htmlentities($route['NETWORK']); ?>" />
            <input type="hidden" id="routeEditCollectionId<?php echo $route['route_id']; ?>" value="<?php echo htmlentities($route['route_collection_id']); ?>" />
            <input type="hidden" id="routeEditRate<?php echo $route['route_id']; ?>" value="<?php echo htmlentities($route['real_rate']); ?>" />
            <input type="hidden" id="routeEditRDNC<?php echo $route['route_id']; ?>" value="<?php echo htmlentities($route['rdnc']); ?>" />
            <input type="hidden" id="routeEditStatus<?php echo $route['route_id']; ?>" value="<?php echo htmlentities($route['STATUS']); ?>" />

         </div>
      </div>
   <?php } ?>
</div>

<!-- MODALS HERE -->
<!-- THE EDIT CONTACT MODAL -->
<div id="modalFragRoute" class="modal fade" tabindex="-1" role="dialog" data-backdrop="static">
   <div class="modal-dialog">
      <div class="modal-content">
         <div class="modal-header" >
            <button type="button" class="close" data-dismiss="modal" aria-label="Close" id="btnFragModalCloseTop"><span aria-hidden="true">&times;</span></button>
            <h4 class="modal-title" id="modalFragTitle">Add/Edit Route</h4>
         </div>
         <div class="modal-body">
            <div class="alert alert-danger" style="display:none;" id="modalFragAlert">
               <h4><i class="icon fa fa-ban"></i> Error!</h4>
               <p id="modalFragAlertText">Unfortunately there was an error, please try again.</p>
            </div>
            <h5>You can use this form to configure this route.</h5>
            <div class="form-group">
               <label for="modalFragSelectRouteCountry">Country <span id="modalFragSelectRouteCountryLoading"></span></label>
               <select class="form-control" id="modalFragSelectRouteCountry">
               </select>
            </div>
            <div class="form-group">
               <label for="modalFragSelectRouteNetwork">Network <span id="modalFragSelectRouteNetworkLoading"></span></label>
               <select class="form-control" id="modalFragSelectRouteNetwork">
               </select>
            </div>
            <div class="form-group">
               <label for="modalFragSelectRouteCollection">Collection <span id="modalFragSelectRouteCollectionLoading"></span></label>
               <select class="form-control" id="modalFragSelectRouteCollection">
               </select>
            </div>
            <div class="row">
               <div class="col-lg-6">
                  <div class="form-group">
                     <label for="modalFragSelectRouteStatus">Status</label>
                     <select class="form-control" id="modalFragSelectRouteStatus">
                        <option value="ENABLED">Enabled</option>
                        <option value="DISABLED">Disabled</option>
                     </select>
                  </div>
               </div>
               <div class="col-lg-6">
                  <div class="form-group">
                     <label for="modalFragSelectRouteCurrency">Status</label>
                     <select class="form-control" id="modalFragSelectRouteCurrency">
                        <option value="ZAR">South African Rand (ZAR)</option>
                        <option value="EUR">European Euro (EUR)</option>
                     </select>
                  </div>
               </div>
            </div>
            <div class="row">
               <div class="col-lg-6">
                  <div class="form-group">
                     <label for="modalFragInputRouteRate">Rate</label>
                     <input type="text" class="form-control" id="modalFragInputRouteRate" placeholder="" value="" />
                  </div>
               </div>
               <div class="col-lg-6">
                  <div class="form-group">
                     <label for="modalFragInputRouteRDNC">RDNC</label>
                     <input type="text" class="form-control" id="modalFragInputRouteRDNC" placeholder="" value="" />
                  </div>
               </div>
            </div>
         </div>
         <div class="modal-footer">
            <div class="row">
               <div class="col-lg-3" style="text-align: left;">
                  <button type="button" class="btn btn-danger" id="btnFragModalDeleteRoute">Delete This Route</button>
               </div>
               <div class="col-lg-4">
                  <p id="modalFragStatusText"></p>
               </div>
               <div class="col-lg-5">
                  <button type="button" class="btn btn-default" data-dismiss="modal" id="btnFragModalCloseBottom">Close</button>
                  <button type="button" class="btn btn-primary" id="btnFragModalSave">Save Route</button>
               </div>
            </div>
            <input type="hidden" class="form-control" id="modalFragInputField" value=""/>
         </div>
      </div><!-- /.modal-content -->
   </div><!-- /.modal-dialog -->
</div><!-- /.modal -->


<script type="text/javascript">
   var fragServiceId = <?php echo (isset($service_id) ? $service_id : '-1') ?>;

   var routeEditId = -1;
   var routeEditCountryName = "";
   var routeEditNetworkMCCMNC = "";
   var routeEditNetwork = "";
   var routeEditCollectionId = -1;
   var routeEditRate = -1;
   var routeEditRDNC = -1;
   var routeEditStatus = "";
   var routeEditCurrency = ""

   $(document).ready(function (e) {
      //enable tooltips
      $('[data-toggle="tooltip"]').tooltip();

      /**
       * This button shows the add new route modal
       */
      $('#btnAddNewRoute').on("click", function (e)
      {
         e.preventDefault();

         routeEditId = -1;
         routeEditCountryName = "";
         routeEditNetworkMCCMNC = "";
         routeEditNetwork = "";
         routeEditCollectionId = -1;
         routeEditRate = 2100;
         routeEditRDNC = 2100;
         routeEditStatus = "ENABLED";
         routeEditCurrency = "ZAR";

         setFormProgress(0);

         //hide the delete button, we can't delete a non created route
         $('#btnFragModalDeleteRoute').hide();

         $('#modalFragRoute').modal('show');
         setupFragModalForm();

         loadFragModalRouteCountries();
      });

      /**
       * This button shows the add new route modal
       */
      $('.btn-edit-route').on("click", function (e)
      {
         e.preventDefault();

         routeEditId = $(this).attr("connet-route-id");
         routeEditCountryName = $('#routeEditCountryName'+routeEditId).val();
         routeEditNetworkMCCMNC = $('#routeEditNetworkMCCMNC'+routeEditId).val();
         routeEditNetwork = $('#routeEditNetwork'+routeEditId).val();
         routeEditCollectionId = $('#routeEditCollectionId'+routeEditId).val();
         routeEditRate = $('#routeEditRate'+routeEditId).val();
         routeEditRDNC = $('#routeEditRDNC'+routeEditId).val();
         routeEditStatus = $('#routeEditStatus'+routeEditId).val();

         setFormProgress(3);

         //show the delete button
         $('#btnFragModalDeleteRoute').show();

         $('#modalFragRoute').modal('show');
         setupFragModalForm();
         loadFragModalRouteCountries();
      });

      /**
       * Saves the form if available!
       */
      $('#btnFragModalSave').on("click", function (e)
      {
         e.preventDefault();

         lockFragModalBusy("Saving...");

         var routeCountry = $('#modalFragSelectRouteCountry').val();
         var networkMCCMNC = $('#modalFragSelectRouteNetwork').val();
         var collectionId = $('#modalFragSelectRouteCollection').val();
         var status = $('#modalFragSelectRouteStatus').val();
         var currency = $('#modalFragSelectRouteCurrency').val();
         var rate = $('#modalFragInputRouteRate').val();
         var rdnc = $('#modalFragInputRouteRDNC').val();

         $.post("../php/ajaxManageServiceHandler.php", {
            task: 'save_service_route',
            serviceId: fragServiceId,
            routeId:routeEditId,
            routeCountry:routeCountry,
            networkMCCMNC:networkMCCMNC,
            collectionId:collectionId,
            status:status,
            currency:currency,
            rate:rate,
            rdnc:rdnc
         }).done(function (data) {
            var json = $.parseJSON(data);
            if (json.success)
            {
               lockFragModalBusy("Refreshing...");
               location.reload();
            }
            else
            {
               unlockFragModalBusy();
               showFragModalError("A server error occurred and we could not save the data, please refresh your page or contact technical support.");
            }
         }).fail(function (data) {
            unlockFragModalBusy();
            showFragModalError("A server error occurred and we could not save the data, please refresh your page or contact technical support.");
         });

      });

      /**
       * Delete the route if the user opts to do so
       */
      $('#btnFragModalDeleteRoute').on("click", function (e) {
         e.preventDefault();

         if(confirm("Are you sure you want to delete this route?"))
         {
            lockFragModalBusy("Removing route...");

            $.post("../php/ajaxManageServiceHandler.php", {
               task: 'delete_service_route',
               serviceId: fragServiceId,
               routeId:routeEditId
            }).done(function (data) {
               var json = $.parseJSON(data);
               if (json.success)
               {
                  lockFragModalBusy("Refreshing...");
                  location.reload();
               }
               else
               {
                  unlockFragModalBusy();
                  showFragModalError("A server error occurred and we could not delete this route, please refresh your page or contact technical support.");
               }
            }).fail(function (data) {
               unlockFragModalBusy();
               showFragModalError("A server error occurred and we could not delete this route, please refresh your page or contact technical support.");
            });
         }

      });

      //specifically loads the networks for a selected country
      $('#modalFragSelectRouteCountry').on("change", function (e)
      {
         e.preventDefault();

         loadFragModalRouteNetworks();

         setFormProgress(1);
      });

      //specifically loads the collections for a selected network
      $('#modalFragSelectRouteNetwork').on("change", function (e)
      {
         e.preventDefault();

         loadFragModalRouteCollections();

         setFormProgress(2);
      });

      //handle form functions after selection is complete and valid
      $('#modalFragSelectRouteCollection').on("change", function (e)
      {
         e.preventDefault();

         setFormProgress(3);
      });
   });

   //sets the defaults for hte route form if the user is editing a route
   function setupFragModalForm()
   {
      //$('#modalFragSelectRouteCountry').val(routeEditCountryName);
      //$('#modalFragSelectRouteNetwork').val(routeEditNetworkMCCMNC);
      //$('#modalFragSelectRouteCollection').val(routeEditCollectionId);
      $('#modalFragSelectRouteStatus').val(routeEditStatus);
      $('#modalFragInputRouteRate').val(routeEditRate);
      $('#modalFragInputRouteRDNC').val(routeEditRDNC);
   }

   /**
    * Loads all the relevent countries
    */
   function loadFragModalRouteCountries()
   {
      $('#modalFragSelectRouteNetwork').empty();
      $('#modalFragSelectRouteCollection').empty();

      showLoading('modalFragSelectRouteCountryLoading');
      $('#modalFragSelectRouteCountry').hide();

      $.post("../php/ajaxManageServiceHandler.php", {task: 'get_route_countries', serviceId: fragServiceId}).done(function (data) {
         var json = $.parseJSON(data);
         if (json.success)
         {
            $('#modalFragSelectRouteCountry').empty();
            for(var i = 0; i < json.countries.length; ++ i)
            {
               var country = json.countries[i];
               $('#modalFragSelectRouteCountry').append('<option value="' + country.prefix_country_name + '">' + country.prefix_country_name + '</option>');
            }

            if(routeEditCountryName != "")
            {
               $('#modalFragSelectRouteCountry').val(routeEditCountryName);
               loadFragModalRouteNetworks();
            }
            else {
               $('#modalFragSelectRouteCountry').val('');
            }

            $('#modalFragSelectRouteCountryLoading').empty();
            $('#modalFragSelectRouteCountry').show();
            $('#modalFragSelectRouteCountry').removeAttr('disabled');
         }
         else
         {
            showModalError("A server error occurred and we could not load the data, please contact technical support.");
         }
      }).fail(function (data) {
         showModalError("A server error occurred and we could not load the data, please contact technical support.");
      });
   }

   /**
    * Loads all the relevent countries
    */
   function loadFragModalRouteNetworks()
   {
      $('#modalFragSelectRouteCollection').empty();

      showLoading('modalFragSelectRouteNetworkLoading');
      $('#modalFragSelectRouteNetwork').hide();

      var countryName = $('#modalFragSelectRouteCountry').val();
      $.post("../php/ajaxManageServiceHandler.php", {task: 'get_route_networks', serviceId: fragServiceId, countryName:countryName}).done(function (data) {
         var json = $.parseJSON(data);
         if (json.success)
         {
            $('#modalFragSelectRouteNetwork').empty();

            var canLoadCollections = false;
            if(routeEditId != -1 && routeEditCountryName == countryName)
            {
               canLoadCollections = true;
               $('#modalFragSelectRouteNetwork').append('<option value="' + routeEditNetworkMCCMNC + '" selected>' + routeEditNetwork + '</option>');
               loadFragModalRouteCollections();
            }


            if(json.networks.length == 0 && routeEditId == -1)
            {
               $('#modalFragSelectRouteNetworkLoading').html('<span class="text-warning">No networks exist for this country.</span>');
            }
            else
            {
               canLoadCollections = true;
               for (var i = 0; i < json.networks.length; ++i) {
                  var network = json.networks[i];
                  $('#modalFragSelectRouteNetwork').append('<option value="' + network.prefix_mccmnc + '">' + network.prefix_network_name + ' (' + network.prefix_mccmnc + ')</option>');
               }
            }

            if(canLoadCollections)
            {
               if (routeEditNetworkMCCMNC != "") {
                  console.log('Setting network MCCMNC : ' + routeEditNetworkMCCMNC);
                  $('#modalFragSelectRouteNetwork').val(routeEditNetworkMCCMNC);
                  loadFragModalRouteCollections();
               }
               else {
                  $('#modalFragSelectRouteNetwork').val('');
               }

               $('#modalFragSelectRouteNetworkLoading').empty();
               $('#modalFragSelectRouteNetwork').show();
               $('#modalFragSelectRouteNetwork').removeAttr('disabled');
            }
         }
         else
         {
            showModalError("A server error occurred and we could not load the data, please contact technical support.");
         }
      }).fail(function (data) {
         showModalError("A server error occurred and we could not load the data, please contact technical support.");
      });
   }

   /**
    * Loads all the relevent countries
    */
   function loadFragModalRouteCollections()
   {
      var countryName = $('#modalFragSelectRouteCountry').val();
      var networkMCCMNC = $('#modalFragSelectRouteNetwork').val();

      showLoading('modalFragSelectRouteCollectionLoading');
      $('#modalFragSelectRouteCollection').hide();

      $.post("../php/ajaxManageServiceHandler.php", {task: 'get_route_collections', serviceId: fragServiceId, networkMCCMNC:networkMCCMNC, countryName:countryName}).done(function (data) {
         var json = $.parseJSON(data);
         if (json.success)
         {
            $('#modalFragSelectRouteCollection').empty();

            if(json.collections.length == 0)
            {
               $('#modalFragSelectRouteCollectionLoading').html('<span class="text-warning">No collections exist for this network.</span>');
            }
            else
            {
               for(var i = 0; i < json.collections.length; ++ i)
               {
                  var collection = json.collections[i];
                  $('#modalFragSelectRouteCollection').append('<option value="' + collection.route_collection_id + '">' + collection.route_collection_name + ' (' + collection.route_collection_id + ')</option>');
               }

               if(routeEditCollectionId != -1)
               {
                  console.log('Setting Collection : ' + routeEditCollectionId);
                  $('#modalFragSelectRouteCollection').val(routeEditCollectionId);
               }
               else {
                  $('#modalFragSelectRouteCollection').val('');
               }

               $('#modalFragSelectRouteCollectionLoading').empty();
               $('#modalFragSelectRouteCollection').show();
               $('#modalFragSelectRouteCollection').removeAttr('disabled');
            }
         }
         else
         {
            showModalError("A server error occurred and we could not load the data, please contact technical support.");
         }
      }).fail(function (data) {
         showModalError("A server error occurred and we could not load the data, please contact technical support.");
      });
   }


   /*****************************************
    * HELPERS
    *****************************************/

   //this locks the modal so the user can't do anything, useful when runing ajax calls off of it
   var prevButtonContent = "";
   function lockFragModalBusy(busyMessage)
   {
      prevButtonContent = $('#btnFragModalSave').html();
      $('#btnFragModalCloseTop').prop("disabled", true);
      $('#btnFragModalCloseBottom').prop("disabled", true);
      $('#btnFragModalSave').prop("disabled", true);
      $('#btnFragModalDeleteRoute').prop("disabled", true);
      $('#modalFragStatusText').html('<span class="glyphicon glyphicon-refresh glyphicon-spin"></span> ' + busyMessage);
   }

   //unlocks the modal so the user can close it
   function unlockFragModalBusy()
   {
      $('#btnFragModalCloseTop').prop("disabled", false);
      $('#btnFragModalCloseBottom').prop("disabled", false);
      $('#btnFragModalSave').prop("disabled", false);
      $('#btnFragModalDeleteRoute').prop("disabled", false);
      $('#modalFragStatusText').html('<span class="glyphicon glyphicon-refresh glyphicon-spin"></span> ' + busyMessage);
   }

   //shows the error in the MODAL with a custom error message
   function showFragModalError(message)
   {
      $("#modalFragAlertText").html(message);
      $("#modalFragAlert").show();
   }

   //hides the modal error and resets the message
   function hideFragModalError()
   {
      $("#modalFragAlertText").html();
      $("#modalFragAlert").hide();
   }


   //this function appropraitely enables and disables form elements as the user progresses
   function setFormProgress(progress) {
      if(progress < 1) {
         $('#modalFragSelectRouteCountry').attr('disabled', true);
      }
      else {
         $('#modalFragSelectRouteCountry').removeAttr('disabled');
      }

      if(progress < 2) {
         $('#modalFragSelectRouteNetwork').attr('disabled', true);
      }
      else {
         $('#modalFragSelectRouteNetwork').removeAttr('disabled');
      }

      if(progress < 3) {
         $('#modalFragSelectRouteCollection').attr('disabled', true);
      }
      else {
         $('#modalFragSelectRouteCollection').removeAttr('disabled');
      }

      if(progress != 3) {
         $('#modalFragInputRouteRate').attr('disabled', true);
         $('#modalFragInputRouteRDNC').attr('disabled', true);
         $('#modalFragSelectRouteStatus').attr('disabled', true);
         $('#modalFragSelectRouteCurrency').attr('disabled', true);
         $('#btnFragModalSave').attr('disabled', true);
      }
      else {
         $('#modalFragInputRouteRate').removeAttr('disabled');
         $('#modalFragInputRouteRDNC').removeAttr('disabled');
         $('#modalFragSelectRouteStatus').removeAttr('disabled');
         $('#modalFragSelectRouteCurrency').removeAttr('disabled');
         $('#btnFragModalSave').removeAttr('disabled');
      }
   }

</script>

<!-- Template Footer -->

