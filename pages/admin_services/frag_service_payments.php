<?php

   /**
    * This script is called by the manage services page, and runs certain ajax based tasks
    * @author - Doug Jenkinson
    */
   require_once("../../php/allInclusive.php");
   require_once("../../PluginAPI/PluginAPIAutoload.php");

   session_start();

$service_helper = new sqlHelperServices();

   if(isset($_POST['service_id']))
   {
      $service_id = $_POST['service_id'];
   }
   else if(isset($_GET['service_id']))
   {
      $service_id = $_GET['service_id'];
   }
   else
   {
      echo '<p class="text-error">An error occurred, please refresh your browser.</p>';
      die();
   }

   //get the service object
   $service_obj = new Service($service_id);

   //determine the current balance and styling class for this service
   if($service_obj->service_credit_available / 10000 <= 0)
   {
      $service_credit_class = "text-warning";
   }
   elseif($service_obj->service_credit_available / 10000 < 50)
   {
      $service_credit_class = "text-danger";
   }
   else
   {
      $service_credit_class = "text-success";
   }
   $service_credit_display = number_format($service_obj->service_credit_available / 10000, 2, '.', ' ');

?>

   <p class="lead">You can view and make payments using this tab.</p>
   <div class="row">
      <!-- ADD PAYMENTS FORM -->
      <div class="col-lg-3">
         <div class="box box-primary">
            <div class="box-header with-border">
               <h3 class="box-title"><i class="fa fa-area-chart"></i> Update Credits</h3>
            </div>
            <div class="box-body">
               <div class="form-group">
                  <div class="box box-solid">
                     <label>Current Balance</label>
                     <p class="<?php echo $service_credit_class; ?>"><?php echo $service_credit_display; ?></p>
                     <div class="form-group">
                        <label>Payment Amount (Currency Amount)</label>
                        <input type="text" id="inputCreditAmount" class="form-control" placeholder="" />
                     </div>
                     <div class="form-group">
                        <label>Description</label>
                        <input type="text" id="inputCreditDescription" class="form-control" placeholder="" />
                     </div>
                     <div class="callout callout-success" id="alertCreditSuccess" style="display:none;">
                        <p>Your credit was successfully added.</p>
                     </div>
                     <button id="btnUpdateCredit" class="btn btn-primary btn-sm btn-block"><i class="fa fa-plus"></i> Update Credit</button>
                  </div>
               </div>
            </div>
         </div>
      </div>

      <!-- PAYMENT HISTORY -->
      <div class="col-lg-9">
         <div class="box box-primary">
            <div class="box-header with-border">
               <h3 class="box-title"><i class="fa fa-area-chart"></i> Payment History</h3>
            </div>
            <div class="box-body">
               <table class="table table-bordered">
                  <thead>
                  <th>Transaction Date</th>
                  <th>Transaction Amount</th>
                  <th>Previous Balance</th>
                  <th>New Balance</th>
                  <th width="200">Payment Description</th>
                  <th>User</th>
                  </thead>
                  <tbody id="payment_history_holder">
                     <!-- PAYMENT RECORDS ARE LOADED VIA AJAX INTO HERE -->
                  </tbody>
               </table>
               <div class="row">
                  <div class="col-lg-9">
                     <p><small>Showing page <span id="pageCurrentPage">0</span> of <span id="pageTotalPages">0</span> pages.</small></p>
                  </div>
                  <div class="col-lg-3" style="text-align:right;">
                     <nav>
                        <ul class="pagination pagination-sm">
                           <li>
                              <button aria-label="Previous" id="pagingPrevPayments">
                                 <span aria-hidden="true">&laquo; Prev</span>
                              </button>
                           </li>
                           <li>
                              <button aria-label="Next" id="pagingNextPayments">
                                 <span aria-hidden="true">Next &raquo;</span>
                              </button>
                           </li>
                        </ul>
                     </nav>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>


<script type="text/javascript">
   var fragServiceId = <?php echo (isset($service_id) ? $service_id : '-1') ?>;
   var paymentPagingLimit = 10;
   var paymentPagingOffset = 0;

   $(document).ready(function (e)
   {
      //prevent non numeric characters
      $('#inputCreditAmount').keyup(function () {
         this.value = this.value.replace(/[^0-9\.]/g,'');
      });

      $('#btnUpdateCredit').on("click", function (e)
      {
         e.preventDefault();

         $('#inputCreditAmount').parent().removeClass('has-error');
         $('#inputCreditDescription').parent().removeClass('has-error');
         $('#alertCreditSuccess').hide();

         var creditAmount = $('#inputCreditAmount').val();
         var creditDescription = $('#inputCreditDescription').val();
         var prevUpdateCredHTML = $(this).html();
         var thisObject = $(this);

         if(creditAmount == "") {
            alert("Please enter a payment amount.");
            $('#inputCreditAmount').parent().addClass('has-error');
         }
         else if(creditDescription == "") {
            alert("Please enter a payment description.");
            $('#inputCreditDescription').parent().addClass('has-error');
         }
         else
         {
            $(this).html('<span class="glyphicon glyphicon-refresh glyphicon-spin"></span> Updating credit...');
            $(this).attr('disabled', 'true');
            $.post("../php/ajaxManageServiceHandler.php", {
               task: 'update_credit',
               serviceId: fragServiceId,
               creditAmount:creditAmount,
               creditDescription:creditDescription,
            }).done(function (data) {
               var json = $.parseJSON(data);
               if (json.success)
               {
                  $(thisObject).html(prevUpdateCredHTML);
                  $(thisObject).removeAttr('disabled');

                  $('#inputCreditDescription').val('');
                  $('#inputCreditAmount').val('');
                  $('#alertCreditSuccess').show();

               }
               else
               {
                  alert("There was a server error and we could not update credit.");
                  $(thisObject).html(prevUpdateCredHTML);
                  $(thisObject).removeAttr('disabled');
               }
               loadPaymentRecords(fragServiceId, paymentPagingOffset, paymentPagingLimit); //reload history
            }).fail(function (data) {
               alert("There was a server error and we could not update credit.");
               $(thisObject).html(prevUpdateCredHTML);
               $(thisObject).removeAttr('disabled');
               loadPaymentRecords(fragServiceId, paymentPagingOffset, paymentPagingLimit); //reload history
            });
         }
      });


      //------------------------------
      // Paging for the payments tab
      //------------------------------
      loadPaymentRecords(fragServiceId, paymentPagingOffset, paymentPagingLimit);

      $('#pagingPrevPayments').on("click", function (e)
      {
         e.preventDefault();
         if((paymentPagingOffset - paymentPagingLimit) >= 0)
         {
            paymentPagingOffset -= paymentPagingLimit;
            loadPaymentRecords(fragServiceId, paymentPagingOffset, paymentPagingLimit);
         }
      });

      $('#pagingNextPayments').on("click", function (e)
      {
         e.preventDefault();

         paymentPagingOffset += paymentPagingLimit;
         loadPaymentRecords(fragServiceId, paymentPagingOffset, paymentPagingLimit);

      });

   });

   function loadPaymentRecords(serviceId, offset, limit)
   {
      showLoading('payment_history_holder');
      //run our ajaxController to save the details

      $.post("../php/ajaxManageServiceHandler.php", {task: "get_payment_history", serviceId: serviceId, limit:limit, offset:offset}).done(function (data) {
         var json = $.parseJSON(data);
         if (json.success)
         {
            $('#payment_history_holder').html("");

            if(json.payments.length == 0)
            {
               $('#payment_history_holder').append("<p>No records to show.</p>");
            }


            for(var i = 0; i < json.payments.length; i++) {
               var payment = json.payments[i];

               var transClass = "text-success";
               var prevClass = "text-success";
               var currClass = "text-success";

               if(payment.payment_value_display <= 0) { transClass = "text-danger"; }
               if(payment.payment_previous_balance_display <= 0) { prevClass = "text-danger"; }
               if(payment.new_balance_display <= 0) { currClass = "text-danger"; }

               $('#payment_history_holder').append(
                   $('<tr>')
                       .append($('<td>').append('<span class="text-info">' + payment.payment_timestamp + '</span>'))
                       .append($('<td>').append('<span class="' + transClass + '">' + payment.payment_value_display + '</span>'))
                       .append($('<td>').append('<span class="' + prevClass + '">' + payment.payment_previous_balance_display + '</span>' ))
                       .append($('<td>').append('<span class="' + currClass + '">' + payment.new_balance_display + '</span>'))
                       .append($('<td>').append(payment.payment_description))
                       .append($('<td>').append(payment.user_username))
               );
            }

            if(json.payments.length < paymentPagingLimit)
            {
               $('#pagingNextPayments').attr('disabled', true);
            }
            else
            {
               $('#pagingNextPayments').removeAttr('disabled');
            }

            if(paymentPagingOffset == 0)
            {
               $('#pagingPrevPayments').attr('disabled', true);
            }
            else
            {
               $('#pagingPrevPayments').removeAttr('disabled');
            }

            //update page display
            var totalPages = Math.ceil(json.total_payment_records / json.limit);
            var currPage = Math.ceil(json.offset/json.limit) + 1;
            $('#pageTotalPages').html(totalPages);
            $('#pageCurrentPage').html(currPage);
            //pageTotalPages
            //pageCurrentPage
         }
         else
         {
            showLoadingError('payment_history_holder', "A server error occurred and we could not load this list, please contact technical support.");
         }
      }).fail(function (data) {
         unlockModalBusy();
         showLoadingError('payment_history_holder', "A server error occurred and we could not load this list, please contact technical support.");
      });

   }

</script>

<!-- Template Footer -->

