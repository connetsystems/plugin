<?php

   /**
    * This script is called by the manage services page, and runs certain ajax based tasks
    * @author - Doug Jenkinson
    */
   require_once("../../php/allInclusive.php");
   require_once("../../PluginAPI/PluginAPIAutoload.php");

   session_start();

$service_helper = new sqlHelperServices();

   if(isset($_POST['service_id']))
   {
      $service_id = $_POST['service_id'];
   }
   else if(isset($_GET['service_id']))
   {
      $service_id = $_GET['service_id'];
   }
   else
   {
      echo '<p class="text-error">An error occurred, please refresh your browser.</p>';
      die();
   }

   //get the service object
   $service_obj = new Service($service_id);

   if(isset($_GET['start_date']) && isset($_GET['end_date']))
   {
      $start_date = new DateTime($_GET['start_date']);
      $end_date = new DateTime($_GET['end_date']);
   }
   else
   {
      $start_date = new DateTime('first day of this week');
      $end_date = new DateTime('today');
   }


   $start_date_string = $start_date->format('Y-m-d');
   $end_date_string = $end_date->format('Y-m-d');


   $service_send_stats = Service::getFullSendStats($service_id, $start_date_string, $end_date_string);
   $service_batch_stats = Service::getBatchStats($service_id, $start_date_string, $end_date_string);

   //generate the URL for going to the batchlist page
   $batch_list_url = "batchlist.php?service_id=".$service_id."&start_date=".($start_date->getTimestamp()*1000)."&end_date=".($end_date->getTimestamp()*1000)."";
?>

   <div class="row">
      <div class="col-lg-8">
         <p class="lead">Below is the general overview of this service.</p>
      </div>
      <div class="col-lg-4">
         <div class="form-group pull-right">
            <div id="overview_date_picker" class="select" style="cursor: pointer;">
               <i class="fa fa-calendar fa-lg"></i>
               <span style="font-size:15px"><?php echo htmlentities($start_date_string).' - '.htmlentities($end_date_string); ?></span><b class="caret"></b>
            </div>
         </div>
      </div>
   </div>
   <div class="row">
      <div class="col-lg-9">
         <div class="row">
            <div class="col-lg-3">
               <h2 style="margin-top:0px;"><i class="fa fa-send"></i> <?php echo $service_send_stats['sent']; ?></h2>
            </div>
            <div class="col-lg-9" style="text-align:right;">
               <h4 style="margin-top:12px;">
                  <span class="text-success"><i class="fa fa-check-circle"></i> <?php echo $service_send_stats['delivered']; ?> <small>delivered</small></span> |
                  <span class="text-warning"><i class="fa fa-question-circle"></i> <?php echo $service_send_stats['pending']; ?> <small>pending</small></span> |
                  <span class="text-danger"><i class="fa fa-times-circle"></i> <?php echo $service_send_stats['failed']; ?> <small>failed</small></span> |
                  <span><i class="fa fa-minus-circle"></i> <?php echo $service_send_stats['rejected']; ?> <small>rejected</small></span>
               </h4>
            </div>
         </div>

         <div class="row">
            <div class="col-lg-12">
               <div class="box box-primary">
                  <div class="box-header with-border">
                     <h3 class="box-title"><i class="fa fa-area-chart"></i> Daily Sends</h3>
                  </div>
                  <div class="box-body" id="sendChartHolder">
                     <canvas id="serviceSendChart" width="400" height="400"></canvas>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <div class="col-lg-3">
         <h2 style="margin-top:0px;"><?php echo $service_batch_stats['total']; ?> <small>total batches</small></h2>
         <div class="box box-primary">
            <div class="box-header with-border">
               <h3 class="box-title"><i class="fa fa-bullhorn"></i> Batches</h3>
               <a href="<?php echo $batch_list_url; ?>" class="btn btn-xs btn-primary pull-right text-white" style="margin:10px 5px; color:white;">View All</a>
            </div>
            <div class="box-body">
               <?php if($service_batch_stats['total'] == 0) {?>
                  <p>No batches sent on this service within the specified date range.</p>
               <?php } else { ?>
                  <small>Showing last 5 batches within your date selection.</small>
                  <?php foreach(array_slice($service_batch_stats['batch_list'], 0, 5) as $batch) { ?>
                     <div class="box box-solid">
                        <div class="box-body">
                           <p style="margin:0;">
                              <strong><?php echo $batch['batch_reference']; ?></strong><br/>
                              <small><?php echo $batch['batch_created']; ?></small>
                           </p>
                           <small style="margin:0;">
                              <strong><i class="fa fa-send"></i> <?php echo $batch['send_stats']['TOTAL']; ?></strong> |
                              <span class="text-success"><i class="fa fa-check-circle"></i> <?php echo $batch['send_stats']['delivered']; ?></span> |
                              <span class="text-warning"><i class="fa fa-question-circle"></i> <?php echo $batch['send_stats']['pending']; ?></span> |
                              <span class="text-danger"><i class="fa fa-times-circle"></i> <?php echo $batch['send_stats']['failed']; ?></span>
                           </small>
                        </div>
                     </div>
                  <?php } ?>
               <?php } ?>
            </div>
         </div>
      </div>
   </div>

   <script type="text/javascript">
      var fragServiceId = <?php echo (isset($service_id) ? $service_id : '-1') ?>;
      var startDate = "<?php echo (isset($start_date_string) ? $start_date_string : date('Y-m-d')) ?>";
      var endDate = "<?php echo (isset($end_date_string) ? $end_date_string : date('Y-m-d')) ?>";

      $(document).ready(function (e)
      {
         //load the chart data via ajax
         loadSendChartData();

         //setup the date range picker
         $('#overview_date_picker').daterangepicker(
             {
                ranges: {
                   'Today': [moment(), moment()],
                   'Yesterday': [moment().subtract('days', 1), moment().subtract('days', 1)],
                   'Last 7 Days': [moment().subtract('days', 6), moment()],
                   'Last 30 Days': [moment().subtract('days', 29), moment()],
                   'This Month': [moment().startOf('month'), moment().endOf('month')],
                   'Last Month': [moment().subtract('month', 1).startOf('month'), moment().subtract('month', 1).endOf('month')]
                },
                startDate: moment('<?php echo isset($start_date_string) ? $start_date_string : ''; ?>'),
                endDate: moment('<?php echo isset($end_date_string) ? $end_date_string : ''; ?>')
             },
             function (start, end) {
                $('#overview_date_picker span').html(start.format('YYYY-MM-DD') + ' - ' + end.format('YYYY-MM-DD'));

                $(".loader").fadeIn("slow");
                $(".loaderIcon").fadeIn("slow");

                var startDateCal = start.format('YYYY-MM-DD');
                //startDateCal = moment(startDateCal).unix() * 1000;

                var endDateCal = end.format('YYYY-MM-DD');
                //endDateCal = moment(endDateCal).unix() * 1000;

                window.location = "../pages/manage_service.php?service_id=" + encodeURIComponent(serviceId) + "&start_date=" + startDateCal + "&end_date=" + endDateCal + "#tab_overview";
             }
         );
      });

      function loadSendChartData()
      {
         showLoading("sendChartHolder");

         //load up the page tab we have selected
         $.post("../php/ajaxManageServiceHandler.php", {task: 'get_daily_send_chart_data_by_date', serviceId: fragServiceId, startDate: startDate, endDate:endDate}).done(function (data) {
            var json = $.parseJSON(data);
            if (json.success)
            {
               setupAndShowChart(json);
            }
            else
            {
               showLoadingError("sendChartHolder", "A server error occurred, please contact technical support.");
            }
         }).fail(function () {
            showLoadingError("sendChartHolder", "A server error occurred, please contact technical support.");
         });
      }

      var chartOptions  = {

         ///Boolean - Whether grid lines are shown across the chart
         scaleShowGridLines : true,
         bezierCurve : false
      };


      function setupAndShowChart(ajaxData)
      {
         //setup the chart data for the daily range
         var data = {
            labels: ajaxData.labels,
            datasets: [
               {
                  label: "Daily Send Stats",
                  fillColor: "rgba(220,220,220,0.2)",
                  strokeColor: "<?php echo $_SESSION['accountColour'] ?>",
                  pointColor: "<?php echo $_SESSION['accountColour'] ?>",
                  pointStrokeColor: "#fff",
                  pointHighlightFill: "#fff",
                  pointHighlightStroke: "rgba(220,220,220,1)",
                  data: ajaxData.daily_sent
               }
            ]
         };

         var chartWidth = $("#sendChartHolder").width() - 5;
         $("#sendChartHolder").html('<canvas id="serviceSendChart" width="'+chartWidth+'" height="300"></canvas>')

         // Get context with jQuery - using jQuery's .get() method.
         var ctx = $("#serviceSendChart").get(0).getContext("2d");
         var serviceSendChart = new Chart(ctx).Line(data, chartOptions);
      }
   </script>


