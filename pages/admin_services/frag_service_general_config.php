<?php

   /**
    * This script is called by the manage services page, and runs certain ajax based tasks
    * @author - Doug Jenkinson
    */
   require_once("../../php/allInclusive.php");
   require_once("../../PluginAPI/PluginAPIAutoload.php");

   session_start();

   $service_helper = new sqlHelperServices();

   if(isset($_POST['service_id']))
   {
      $service_id = $_POST['service_id'];
   }
   else if(isset($_GET['service_id']))
   {
      $service_id = $_GET['service_id'];
   }
   else
   {
      echo '<p class="text-error">An error occurred, please refresh your browser.</p>';
      die();
   }

   //get the service object
   $service_obj = new Service($service_id);

   //determine the current balance and styling class for this service
   if($service_obj->service_credit_available / 10000 <= 0)
   {
      $service_credit_class = "text-warning";
   }
   elseif($service_obj->service_credit_available / 10000 < 50)
   {
      $service_credit_class = "text-danger";
   }
   else
   {
      $service_credit_class = "text-success";
   }
   $service_credit_display = number_format($service_obj->service_credit_available / 10000, 2, '.', ' ');

   //--------------------------------------
   // FOR LOW BALANCE NOTIFICATIONS
   //--------------------------------------
   if($service_obj->service_notification_enabled)
   {
      $low_balance_notifications_status = "ENABLED";
      $low_balance_notifications_class = "btn-success";
   }
   else
   {
      $low_balance_notifications_status = "DISABLED";
      $low_balance_notifications_class = "btn-danger";
   }

   //--------------------------------------
   // FOR DLR AND MO URLS
   //--------------------------------------

   if ($service_obj->http_dlr_enabled === true)
   {
      $http_dlr_class = "btn-success";
   }
   else
   {
      $http_dlr_class = "btn-danger";
   }

   if ($service_obj->http_mosms_enabled === true)
   {
      $http_mosms_class = "btn-success";
   }
   else
   {
      $http_mosms_class = "btn-danger";
   }

   if ($service_obj->smpp_dlr_enabled === true)
   {
      $smpp_dlr_class = "btn-success";
   }
   else
   {
      $smpp_dlr_class = "btn-danger";
   }

   if ($service_obj->smpp_mosms_enabled === true)
   {
      $smpp_mosms_class = "btn-success";
   }
   else
   {
      $smpp_mosms_class = "btn-danger";
   }


?>

      <p class="lead">You can view and edit all the general configuration of this route from this tab.</p>
      <div class="row">
         <div class="col-lg-5">
            <div class="box box-primary">
               <div class="box-header with-border">
                  <h3 class="box-title"><i class="fa fa-user"></i> API CREDENTIALS</h3>
               </div>
               <div class="box-body">
                  <small>
                     Every service has a default API user that the client can use to authenticate via SMPP and HTTP. The details of this user are listed below.
                  </small>
                  <div class="row">
                     <div class="col-lg-6">
                        <h4>
                           <small>Username:</small><br/>
                           <span><?php echo $service_obj->service_login; ?></span>
                        </h4>
                     </div>
                     <div class="col-lg-6">
                        <h4>
                           <small>Password:</small><br/>
                           <?php echo $service_obj->service_password; ?>
                        </h4>
                     </div>
                  </div>
                  <small class="text-warning">N.B. Please note these details should not be used to access to Plugin, only API.</small>
               </div>
            </div>

            <div class="box box-primary">
               <div class="box-header with-border">
                  <h3 class="box-title"><i class="fa fa-battery-1"></i> Low Balance Notifications</h3>
               </div>
               <div class="box-body">
                  <small>
                     Service can to have low credit balance notification emails sent to specific addresses.
                     If their credit drops below the specified threshold, an automated email will be sent to their account to notify that their credits are indeed low.
                  </small>
                  <hr/>
                  <div class="row">
                     <div class="col-lg-4">
                        <div class="input-group-btn" style="top:5px">
                           <button type="button" class="btn <?php echo $low_balance_notifications_class; ?>  btn-block btn-sm dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                              <span class="btn-toggle-text"><?php echo $low_balance_notifications_status; ?></span> <span class="caret"></span>
                           </button>
                           <ul class="dropdown-menu dropdown-menu-right">
                              <li><a class="btn-frag-toggle-status" connet-status="ENABLED" connet-ajax-task='toggle_low_balance_notifications_status' href="#">Enable Low Balance Notifications</a></li>
                              <li><a class="btn-frag-toggle-status" connet-status="DISABLED" connet-ajax-task='toggle_low_balance_notifications_status' href="#">Disable Low Balance Notifications</a></li>
                           </ul>
                        </div>
                     </div>
                     <div class="col-lg-8">
                        <h4 style="margin-top:0px">
                           <small>Notification Balance Threshold:</small><br/>
                           <span class="connet-field-display"><?php echo number_format($service_obj->service_notification_threshold / 10000, 2, '.', ' '); ?></span>
                           <a href="#" class="btn-frag-edit-service-property connet-small-link"
                              connet-id="<?php echo $service_obj->service_id; ?>"
                              connet-value="<?php echo htmlspecialchars(number_format($service_obj->service_notification_threshold / 10000, 2, '.', ' ')); ?>"
                              connet-ajax-task="save_service_field_value"
                              connet-field="service_notification_threshold"
                              connet-title="Low Balance Notification Threshold">[edit]</a>
                        </h4>
                     </div>
                     <div class="col-lg-12">
                        <h4>
                           <small>Notification Email:</small><br/>
                           <span class="connet-field-display"><?php echo $service_obj->service_notification_email; ?></span>
                           <a href="#" class="btn-frag-edit-service-property connet-small-link"
                              connet-id="<?php echo $service_obj->service_id; ?>"
                              connet-value="<?php echo htmlspecialchars($service_obj->service_notification_email); ?>"
                              connet-ajax-task="save_service_field_value"
                              connet-field="service_notification_email"
                              connet-title="Low Balance Notification Email">[edit]</a>
                        </h4>
                     </div>
                  </div>
               </div>
            </div>
         </div>
         <div class="col-lg-7">
            <div class="box box-primary">
               <div class="box-header with-border">
                  <h3 class="box-title"><i class="fa fa-cloud"></i> HTTP Endpoint Config</h3>
               </div>
               <div class="box-body">
                  <small>
                     HTTP services can opt to have delivery reports (DLRs) and reply messages (MOs) sent to specific endpoints of their choosing for consumption.
                     These endpoint URLs are configured here.
                  </small>
                  <h5><strong>HTTP DLR Enpoint</strong></h5>
                  <div class="row">
                     <div class="col-lg-2">
                        <div class="input-group-btn">
                           <button type="button" class="btn <?php echo $http_dlr_class; ?> btn-sm dropdown-toggle" id="btnHttpDLR" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                              <span class="btn-toggle-text"><?php echo $service_obj->http_dlr_status; ?></span> <span class="caret"></span>
                           </button>
                           <ul class="dropdown-menu dropdown-menu-right">
                              <li><a class="btn-frag-toggle-status" connet-status="ENABLED" connet-ajax-task='toggle_http_dlr_status' href="#">Enable</a></li>
                              <li><a class="btn-frag-toggle-status" connet-status="DISABLED" connet-ajax-task='toggle_http_dlr_status' href="#">Disable</a></li>
                           </ul>
                        </div>
                     </div>
                     <div class="col-lg-10">
                        <h5 style="margin-top:0px;">
                           <small>Custom Address:</small><br/>
                           <span class="connet-field-display"><?php echo $service_obj->http_dlr_address; ?></span>
                           <a href="#" class="btn-frag-edit-service-property connet-small-link"
                              connet-id="<?php echo $service_obj->http_dlr_id; ?>"
                              connet-value="<?php echo htmlspecialchars($service_obj->http_dlr_address); ?>"
                              connet-ajax-task="save_http_dlr_endpoint_value"
                              connet-field="endpoint_dlr_address"
                              connet-title="Delivery Report HTTP Endpoint URL">[edit]</a>
                        </h5>
                     </div>
                  </div>
                  <h5><strong>HTTP MOSMS Enpoint</strong></h5>
                  <div class="row">
                     <div class="col-lg-2">
                        <div class="input-group-btn">
                           <button type="button" class="btn <?php echo $http_mosms_class; ?> btn-sm dropdown-toggle" id="btnHttpDLR" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                              <span class="btn-toggle-text"><?php echo $service_obj->http_mosms_status; ?></span> <span class="caret"></span>
                           </button>
                           <ul class="dropdown-menu dropdown-menu-right">
                              <li><a class="btn-frag-toggle-status" connet-status="ENABLED" connet-ajax-task='toggle_http_mosms_status' href="#">Enable</a></li>
                              <li><a class="btn-frag-toggle-status" connet-status="DISABLED" connet-ajax-task='toggle_http_mosms_status' href="#">Disable</a></li>
                           </ul>
                        </div>
                     </div>
                     <div class="col-lg-10">
                        <h5 style="margin-top:0px;">
                           <small>Custom Address:</small><br/>
                           <span class="connet-field-display"><?php echo $service_obj->http_mosms_address; ?></span>
                           <a href="#" class="btn-frag-edit-service-property connet-small-link"
                              connet-id="<?php echo $service_obj->http_mosms_id; ?>"
                              connet-value="<?php echo htmlspecialchars($service_obj->http_mosms_address); ?>"
                              connet-ajax-task="save_http_mosms_endpoint_value"
                              connet-field="endpoint_mosms_address"
                              connet-title="MOSMS HTTP Endpoint URL">[edit]</a>
                        </h5>
                     </div>
                  </div>
               </div>
            </div>

            <!-- SMPP CONFIG -->
            <div class="box box-primary">
               <div class="box-header with-border">
                  <h3 class="box-title"><i class="fa fa-paper-plane-o"></i> SMPP Config</h3>
               </div>
               <div class="box-body">
                  <small>
                     SMPP DLR and MOSMS routing for this service can be enabled or disabled here.
                  </small>
                  <h5><strong>SMPP DLR</strong></h5>
                  <div class="row">
                     <div class="col-lg-2">
                        <div class="input-group-btn">
                           <button type="button" class="btn <?php echo $smpp_dlr_class; ?> btn-sm dropdown-toggle" id="btnHttpDLR" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                              <span class="btn-toggle-text"><?php echo $service_obj->smpp_dlr_status; ?></span> <span class="caret"></span>
                           </button>
                           <ul class="dropdown-menu dropdown-menu-right">
                              <li><a class="btn-frag-toggle-status" connet-status="ENABLED" connet-ajax-task='toggle_smpp_dlr_status' href="#">Enable</a></li>
                              <li><a class="btn-frag-toggle-status" connet-status="DISABLED" connet-ajax-task='toggle_smpp_dlr_status' href="#">Disable</a></li>
                           </ul>
                        </div>
                     </div>
                     <div class="col-lg-10">
                        <h5 style="margin-top:0px;">
                           <small>Auto Generated Address:</small><br/>
                           <span><?php echo $service_obj->smpp_dlr_address; ?></span>
                        </h5>
                     </div>
                  </div>
                  <h5><strong>SMPP MOSMS</strong></h5>
                  <div class="row">
                     <div class="col-lg-2">
                        <div class="input-group-btn">
                           <button type="button" class="btn <?php echo $smpp_mosms_class; ?> btn-sm dropdown-toggle" id="btnHttpDLR" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                              <span class="btn-toggle-text"><?php echo $service_obj->smpp_mosms_status; ?></span> <span class="caret"></span>
                           </button>
                           <ul class="dropdown-menu dropdown-menu-right">
                              <li><a class="btn-frag-toggle-status" connet-status="ENABLED" connet-ajax-task='toggle_smpp_mosms_status' href="#">Enable</a></li>
                              <li><a class="btn-frag-toggle-status" connet-status="DISABLED" connet-ajax-task='toggle_smpp_mosms_status' href="#">Disable</a></li>
                           </ul>
                        </div>
                     </div>
                     <div class="col-lg-10">
                        <h5 style="margin-top:0px;">
                           <small>Auto Generated Address:</small><br/>
                           <span><?php echo $service_obj->smpp_mosms_address; ?></span>
                        </h5>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>


      <!-- MODALS HERE -->
      <!-- THE EDIT CONTACT MODAL -->
      <div id="modalFragEditField" class="modal fade" tabindex="-1" role="dialog" data-backdrop="static">
         <div class="modal-dialog">
            <div class="modal-content">
               <div class="modal-header" >
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close" id="btnModalCloseTop"><span aria-hidden="true">&times;</span></button>
                  <h4 class="modal-title" id="modalFragTitle">Add A New Contact Group</h4>
               </div>
               <div class="modal-body">
                  <div class="alert alert-danger" style="display:none;" id="modAlert">
                     <h4><i class="icon fa fa-ban"></i> Error!</h4>
                     <p id="modAlertText">Unfortunately there was an error, please try again.</p>
                  </div>
                  <div class="form-group">
                     <label for="modalFragInputValue" id="modalFragInputLabel">Label</label>
                     <input type="text" class="form-control" id="modalFragInputValue" placeholder="" value="" />
                  </div>
                  <small>Please edit the value and click save to commit, or close to cancel.</small>
               </div>
               <div class="modal-footer">
                  <button type="button" class="btn btn-default" data-dismiss="modal" id="btnModalCloseBottom">Close</button>
                  <button type="button" class="btn btn-primary" id="btnFragModalSave" style="background-color:<?php echo $_SESSION['accountColour']; ?>;border: 2px solid; border-radius: 6px !important;">Save</button>
                  <input type="hidden" class="form-control" id="modalFragInputField" value=""/>
                  <input type="hidden" class="form-control" id="modalFragInputId" value=""/>
                  <input type="hidden" class="form-control" id="modalFragInputTask" value=""/>
               </div>
            </div><!-- /.modal-content -->
         </div><!-- /.modal-dialog -->
      </div><!-- /.modal -->


<script type="text/javascript">
   var fragServiceId = <?php echo (isset($service_id) ? $service_id : '-1') ?>;

   $(document).ready(function (e)
   {
      /**
       * For disabling or enabling low balance notifications
       */
      $('.btn-frag-toggle-status').on("click", function (e)
      {
         e.preventDefault();

         var status = $(this).attr('connet-status');
         var task = $(this).attr('connet-ajax-task');
         var thisObject = $(this);

         var newClass = "";
         var oldClass = "";
         if(status == "ENABLED")
         {
            newClass = "btn-success";
            oldClass = "btn-danger";
         }
         else
         {
            newClass = "btn-danger";
            oldClass = "btn-danger";
         }

         $(thisObject).closest('div').find('.btn-toggle-text').html('<span class="glyphicon glyphicon-refresh glyphicon-spin"></span> Saving...');

         $.post("../php/ajaxManageServiceHandler.php", {task: task, serviceId: serviceId, status:status}).done(function (data) {
            var json = $.parseJSON(data);
            if (json.success)
            {
               $(thisObject).closest('div').find('.btn-toggle-text').html(status);
               $(thisObject).closest('div').find('button').removeClass(oldClass);
               $(thisObject).closest('div').find('button').addClass(newClass);
            }
            else
            {
               alert("A server error occurred and we could not change the status of this service, please contact technical support.");
            }
         }).fail(function (data) {
            alert("A server error occurred and we could not change the status of this service, please contact technical support.");
         });

      });


      /**
       * This button loads the generic field editor by launching a modal a modal
       */
      $('.btn-frag-edit-service-property').on("click", function (e)
      {
         e.preventDefault();

         var fieldName = $(this).attr('connet-field');
         var fieldTitle = $(this).attr('connet-title');
         var fieldId = $(this).attr('connet-id');
         var fieldTask = $(this).attr('connet-ajax-task');
         var fieldValue = $(this).attr('connet-value');

         $('#modalFragTitle').html("Edit Field: " + fieldTitle);
         $('#modalFragInputLabel').html(fieldTitle);
         $('#modalFragInputValue').val(fieldValue);
         $('#modalFragInputField').val(fieldName);
         $('#modalFragInputId').val(fieldId);
         $('#modalFragInputTask').val(fieldTask);

         $('#modalFragEditField').modal('show');
      });


      $('#btnFragModalSave').on("click", function (e)
      {
         e.preventDefault();

         lockFragModalBusy("Saving...");

         var fieldName = $('#modalFragInputField').val();
         var fieldValue = $('#modalFragInputValue').val();
         var fieldTask = $('#modalFragInputTask').val();
         var fieldId = $('#modalFragInputId').val();

         $.post("../php/ajaxManageServiceHandler.php", {task: fieldTask, serviceId: fragServiceId, fieldId: fieldId, fieldName:fieldName, fieldValue:fieldValue}).done(function (data) {
            var json = $.parseJSON(data);
            if (json.success)
            {
               unlockFragModalBusy();

               //ocate the html elements that hold this data and update them for display
               $("a[connet-id='"+fieldId+"'][connet-field='"+fieldName+"']" ).attr('connet-value', fieldValue);
               $("a[connet-id='"+fieldId+"'][connet-field='"+fieldName+"']" ).parent().find('.connet-field-display').html(fieldValue);
               $('#modalFragEditField').modal('hide');
            }
            else
            {
               unlockFragModalBusy();
               showModalError("A server error occurred and we could not save the data, please contact technical support.");
            }
         }).fail(function (data) {
            unlockFragModalBusy();
            showModalError("A server error occurred and we could not save the data, please contact technical support.");
         });
      });

   });

   //this locks the modal so the user can't do anything, useful when runing ajax calls off of it
   var prevButtonContent = "";
   function lockFragModalBusy(busyMessage)
   {
      prevButtonContent = $('#btnFragModalSave').html();
      $('#btnModalCloseTop').prop("disabled", true);
      $('#btnModalCloseBottom').prop("disabled", true);
      $('#btnFragModalSave').prop("disabled", false);
      $('#btnFragModalSave').html('<span class="glyphicon glyphicon-refresh glyphicon-spin"></span> ' + busyMessage);
   }

   //unlocks the modal so the user can close it
   function unlockFragModalBusy()
   {
      $('#btnModalCloseTop').prop("disabled", false);
      $('#btnModalCloseBottom').prop("disabled", false);
      $('#btnFragModalSave').prop("disabled", false);
      $('#btnFragModalSave').html(prevButtonContent);
      prevButtonContent = "";
   }

   //shows the error in the MODAL with a custom error message
   function showFragModalError(message)
   {
      $("#modAlertText").html(message);
      $("#modAlert").show();
   }

   //hides the modal error and resets the message
   function hideFragModalError()
   {
      $("#modAlertText").html();
      $("#modAlert").hide();
   }

</script>

<!-- Template Footer -->

