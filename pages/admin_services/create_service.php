<?php
   /**
    * Include the header template which sets up the HTML
    *
    * Don't forget to include template_import_script before any Javascripting
    * Don't forget to include template footer.php at the bottom of the page as well
    */
   //-------------------------------------------------------------
   // Template
   //-------------------------------------------------------------
   TemplateHelper::setPageTitle('Create A New Service');
   TemplateHelper::initialize();

   //-------------------------------------------------------------
   // CREATE SERVICE
   //-------------------------------------------------------------

   $service_managers = User::getAllAdministrators();

   if(isset($_GET['account_id']))
   {
      $preselected_account = new Account($_GET['account_id']);
   }
?>

<aside class="right-side">
   <section class="content-header">
      <h1>
         Create A New Service
      </h1>
   </section>

   <section class="content">

      <!-- BACK BUTTON AND SERVICE TITLE -->
      <div class="row">
         <div class="col-lg-2">
            <a class="btn btn-default btn-block btn-sm" href="../manage_service_select.php" style="margin:0px;">
               <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span> Back to Service Selection
            </a>
         </div>
         <div class="col-lg-10">

         </div>
      </div>
      <hr/>

      <!-- ACCOUNT CREATION/SELECTION -->
      <div class="row">
         <div class="col-lg-8 col-lg-offset-2">
            <!-- THE TITLE BOX FOR ACCOUTN CREATION -->
            <div class="box box-primary">
               <div class="box-header with-border">
                  <h3 class="box-title"><i class="fa fa-space-shuttle"></i> Create or Select An Account</h3>
               </div>
               <div class="box-body">
                  <p class="lead">Please select an account to add this service to. Optionally, you can create a new account.</p>
                  <div id="accountContainer" style="text-align: center">
                     <div class="btn-group" role="group" aria-label="...">
                        <button type="button" id="btnSelectAccount" class="btn btn-bg btn-primary">Select Existing Account</button>
                        <button type="button" id="btnCreateAccount" class="btn btn-bg btn-default">Create New Account</button>
                     </div>
                     <hr/>

                     <!-- FOR ACCOUNT CREATION -->
                     <div id="accountCreateContainer">
                        <p>Please enter a name for the new account. You can configure other settings of this account, such as styling and URLs, after service creation.</p>
                        <div class="row">
                           <div class="col-lg-6 col-lg-offset-3">
                              <div class="form-group">
                                 <label for="accountName">New Account Name</label>
                                 <input type="email" class="form-control" id="accountName" name="accountName" placeholder="New Account Name...">
                              </div>
                              <small class="text-danger" id="errorAccountName" style="display:none;" style="display:none;">The account name is invalid.</small>
                           </div>
                        </div>
                     </div>

                     <!-- FOR ACCOUNT SELECTION -->
                     <div id="accountSelectContainer">
                        <p>Please select an account. The service you create wil be added to the selected account.</p>
                        <label for="accountSelectName">Selected Account</label>
                        <h2 id="accountSelectName"  class="<?php echo (isset($preselected_account) ? "text-success" : ""); ?>" style="margin-top:2px;">
                           <?php echo (isset($preselected_account) ? $preselected_account->account_name.' <i class="fa fa-check-circle"></i>' : "No account selected."); ?>
                        </h2>
                        <small class="text-danger" id="errorAccountInvalid" style="display:none;">The account you selected is invalid, please select another one.</small>
                        <br/>
                        <button type="button" id="btnShowAccountSelectPopup" class="btn btn-xs btn-default">Select An Account</button>
                     </div>
                     <br/><br/>
                  </div>
               </div>
            </div>
         </div>
      </div>


      <!-- SERVICE CONFIGURATION -->
      <div class="row">
         <div class="col-lg-8 col-lg-offset-2">

            <!-- THE TITLE BOX FOR SERVICE CREATION -->
            <div class="box box-primary">
               <div class="box-header with-border">
                  <h3 class="box-title"><i class="fa fa-flag"></i> New Service Details</h3>
               </div>
               <div class="box-body">
                  <p class="lead">Please fill out all the relevant information about your new service in the form below.</p>
                  <div class="row">
                     <div class="col-lg-6">
                        <div class="form-group">
                           <label for="accountName">Service Name</label>
                           <input type="email" class="form-control" id="serviceName" name="serviceName" placeholder="New Service Name...">
                           <small class="text-danger" id="errorServiceName" style="display:none;">The service name cannot be blank.</small>
                        </div>
                     </div>
                     <div class="col-lg-6">
                        <div class="form-group">
                           <label for="accountName">Service Login Name</label>
                           <input type="email" class="form-control" id="serviceLogin" name="serviceLogin" placeholder="Service login...">
                           <small class="text-danger" id="errorServiceLogin" style="display:none;">The login name cannot be blank.</small>
                        </div>
                     </div>
                     <div class="col-lg-6">
                        <div class="form-group">
                           <label for="accountName">Billing Type</label>
                           <select class="form-control" id="serviceBillingType" name="serviceBillingType">
                              <option value="POSTPAID" selected>POSTPAID</option>
                              <option value="PREPAID">PREPAID</option>
                           </select>
                        </div>
                     </div>
                     <div class="col-lg-6">
                        <div class="form-group">
                           <label for="accountName">Service Manager</label>
                           <select class="form-control" id="serviceManagerId" name="serviceManagerId">
                              <option value="-1"> - select a manager - </option>
                              <?php foreach($service_managers as $manager) { ?>
                                 <option value="<?php echo $manager->user_id; ?>"><?php echo htmlentities($manager->user_fullname); ?></option>
                              <?php } ?>
                           </select>
                           <small class="text-danger" id="errorServiceManagerId" style="display:none;">Please select a service manager.</small>
                        </div>
                     </div>
                  </div>
                  <div class="row">
                     <hr/>
                     <div class="col-lg-12" style="text-align:center;">
                        <button type="button" id="btnCreateNewService" class="btn btn-bg btn-primary">
                           <i class="fa fa-plus-circle" style="font-size:40pt; margin-top:10px;"></i>
                           <br/>
                           <span style="font-size:20pt;">Create Service</span>
                        </button>
                        <hr/>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>


      <!-- MODALS HERE -->
      <!-- THE SELECT ACOUNT MODAL, LOADED VIA AJAX-->
      <div id="modalFragSelectAccount" class="modal fade" tabindex="-1" role="dialog" data-backdrop="static">
         <div class="modal-dialog">
            <div class="modal-content">
               <div class="modal-header" >
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close" id="btnFragModalCloseTop"><span aria-hidden="true">&times;</span></button>
                  <h4 class="modal-title" id="modalFragSelectAccountTitle">Add Service Permission</h4>
               </div>
               <div class="modal-body">
                  <div class="alert alert-danger" style="display:none;" id="modalFragSelectAccountAlert">
                     <h4><i class="icon fa fa-ban"></i> Error!</h4>
                     <p id="modalFragSelectAccountAlertText">Unfortunately there was an error, please try again.</p>
                  </div>
                  <h5>Please select the service you wish to assign to this user.</h5>
                  <div id="modalFragSelectAccountPageHolder" style="height:400px !important;">
                     <!-- THE AJAX LOADED SERVICE SELECTOR IS PLACE IN HERE -->
                  </div>
               </div>
               <div class="modal-footer">
                  <button type="button" class="btn btn-default" data-dismiss="modal" id="modalFragSelectAccountCloseBottom">Close</button>
               </div>
            </div><!-- /.modal-content -->
         </div><!-- /.modal-dialog -->
      </div><!-- /.modal -->




   </section>
</aside>


<!--  THE JAVASCRIPT IMPORT ALWAYS APPEARS BEFORE LOCAL JAVASCRIPT AT THE BOTTOM OF THE PAGE -->
<?php include("../template_import_script.php"); //must import all scripts first   ?>
<!-- END JAVASCRIPT IMPORT -->

<script type="text/javascript">

   var newAccount = false;
   var serviceAccountId = <?php echo (isset($preselected_account) ? $preselected_account->account_id : -1); ?>;
   var serviceAccountName = "<?php echo (isset($preselected_account) ? $preselected_account->account_name : ""); ?>";

   $(document).ready(function (e)
   {
      //enable the tooltips, as bootstrap standard defines them as opt in
      $('[data-toggle="tooltip"]').tooltip();

      //form INIT
      $('#accountCreateContainer').hide();
      $('#accountSelectContainer').show();

      /***********************************************
       * ACCOUNT LOGIC
       **********************************************/
      /**
       * This button shows the account selection div
       */
      $('#btnSelectAccount').on("click", function (e)
      {
         e.preventDefault();

         $('#btnSelectAccount').removeClass("btn-default").addClass("btn-primary");
         $('#btnCreateAccount').removeClass("btn-primary").addClass("btn-default");
         $('#accountCreateContainer').hide();
         $('#accountSelectContainer').show();

         newAccount = false;
      });

      /**
       * This button shows the account creation div
       */
      $('#btnCreateAccount').on("click", function (e)
      {
         e.preventDefault();

         $('#btnCreateAccount').removeClass("btn-default").addClass("btn-primary");
         $('#btnSelectAccount').removeClass("btn-primary").addClass("btn-default");
         $('#accountSelectContainer').hide();
         $('#accountCreateContainer').show();

         newAccount = true;
      });

      /**
       * This button shows the account selection modal
       */
      $('#btnShowAccountSelectPopup').on("click", function (e)
      {
         e.preventDefault();

         hideFragModalError();

         $('#modalFragSelectAccountPageHolder').html('<span class="glyphicon glyphicon-refresh glyphicon-spin"></span> Loading account list...');

         $.post("../modules/module_account_select.php", {parent_callback_function: 'setSelectedAccount'}).done(function (data) {
            $('#modalFragSelectAccountPageHolder').html(data);
         }).fail(function (data) {
            showModalError("A server error occurred and we could not load the account list, please contact technical support.");
         });

         $('#modalFragSelectAccount').modal('show');
      });

      /***********************************************
       * SERVICE CREATION LOGIC
       **********************************************/
      //this button collects the form data, validates, submits, and proceeds accordingly
      $('#btnCreateNewService').on("click", function (e)
      {
         e.preventDefault();

         console.log("Attempting to create service.");

         var formValid = validateServiceCreateForm();
         if(formValid)
         {
            if(newAccount)
            {
               var accountId = -1;
               var accountName = $("#accountName").val();
            }
            else
            {
               var accountId = serviceAccountId;
               var accountName = serviceAccountName;
            }


            var serviceName = $("#serviceName").val();
            var serviceLogin = $("#serviceLogin").val();
            var serviceBillingType = $("#serviceBillingType").val();
            var serviceManagerId = $("#serviceManagerId").val();

            //send the service information to the server and catch errors appropriately
            $.post("<?php echo WebAppHelper::getBaseServerURL(); ?>/php/ajaxManageServiceHandler.php", {
               task: 'create_new_service',
               accountId: accountId,
               accountName: accountName,
               serviceName:serviceName,
               serviceLogin:serviceLogin,
               serviceBillingType:serviceBillingType,
               serviceManagerId:serviceManagerId
            }).done(function (data) {
               var json = $.parseJSON(data);
               if (json.success)
               {
                  window.location = "<?php echo WebAppHelper::getBaseServerURL(); ?>/pages/manage_service.php?service_id="+json.service_id;
               }
               else
               {
                  if(json.reason == "validation_failed")
                  {
                     alert("There were errors in your form");
                     processServerFormErrors(json.form_errors);
                  }
                  else
                  {
                     alert("A server error occurred and we could not create the service, please contact technical support.");
                  }
               }
            }).fail(function (data) {
               alert("A server error occurred and we could not create the service, please contact technical support.");
            });

         }
      });

      $("#serviceName").change(function() {
         $("#serviceName").parent().removeClass('text-error');
         $("#errorServiceName").hide();
      });

      $("#serviceLogin").change(function() {
         $("#serviceLogin").parent().removeClass('text-error');
         $("#errorServiceLogin").hide();
      });

      $("#serviceManagerId").change(function() {
         $("#serviceManagerId").parent().removeClass('text-error');
         $("#errorServiceManagerId").hide();
      });

   });

   /**
    * Takes the servers form error response and displays it to the form
    * @param formErrors (json object)
    * */
   function processServerFormErrors(formErrors)
   {
      if(formErrors.hasOwnProperty('accountName'))
      {
         $("#accountName").parent().addClass('has-error');
         $("#errorAccountName").html(formErrors.accountName);
         $("#errorAccountName").show();
      }

      if(formErrors.hasOwnProperty('serviceName'))
      {
         $("#serviceName").parent().addClass('has-error');
         $("#errorServiceName").html(formErrors.serviceName);
         $("#errorServiceName").show();
      }

      if(formErrors.hasOwnProperty('serviceLogin'))
      {
         $("#serviceLogin").parent().addClass('text-error');
         $("#errorServiceLogin").html(formErrors.serviceLogin);
         $("#errorServiceLogin").show();
      }
   }

   function validateServiceCreateForm()
   {
      console.log("Validating form.");
      var isFormValid = true;
      resetFormErrors();

      //validate the new or selected account
      if(newAccount)
      {
         var accountName = $("#accountName").val();

         if(accountName == "")
         {
            isFormValid = false;
            $("#accountName").parent().addClass('has-error');
            $("#errorAccountName").html("The account name cannot be blank.");
            $("#errorAccountName").show();

         }
      }
      else
      {
         if(!isInteger(serviceAccountId) || serviceAccountId < 1)
         {
            isFormValid = false;
            $("#accountSelectName").addClass('text-danger');
            $("#errorAccountInvalid").show();
         }
      }

      var serviceName = $("#serviceName").val();
      var serviceLogin = $("#serviceLogin").val();
      var serviceBillingType = $("#serviceBillingType").val();
      var serviceManagerId = $("#serviceManagerId").val();

      if(serviceName == "")
      {
         isFormValid = false;
         $("#serviceName").parent().addClass('text-error');
         $("#errorServiceName").html("The service name cannot be blank.");
         $("#errorServiceName").show();
      }

      if(serviceLogin == "")
      {
         isFormValid = false;
         $("#serviceLogin").parent().addClass('text-error');
         $("#errorServiceLogin").html("The service login name cannot be blank.");
         $("#errorServiceLogin").show();
      }

      if(serviceManagerId < 1)
      {
         isFormValid = false;
         $("#serviceManagerId").parent().addClass('text-error');
         $("#errorServiceManagerId").html("Please select a valid manager.");
         $("#errorServiceManagerId").show();
      }

      return isFormValid;
   }

   function resetFormErrors()
   {
      //for the accounts section
      $("#accountSelectName").removeClass('text-error');
      $("#accountName").parent().removeClass('has-error');
      $("#errorAccountName").hide();
      $("#errorAccountInvalid").hide();

      //for the service section
      $("#serviceName").parent().removeClass('text-error');
      $("#errorServiceName").hide();
      $("#serviceLogin").parent().removeClass('text-error');
      $("#errorServiceLogin").hide();
      $("#serviceManagerId").parent().removeClass('text-error');
      $("#errorServiceManagerId").hide();
   }


   function setSelectedAccount(accountId, accountName)
   {
      console.log(accountId + " NAME: " + accountName);

      $('#accountSelectName').html(accountName + '  <i class="fa fa-check-circle"></i>');
      serviceAccountId = accountId;
      serviceAccountName = accountName;

      $('#accountSelectName').addClass("text-success");
      $("#errorAccountInvalid").hide();

      $('#modalFragSelectAccount').modal('hide');
   }


   /*****************************************
    * HELPERS
    *****************************************/
   function isInteger(value)
   {
      return value % 1 === 0;
   }

   //shows the error in the MODAL with a custom error message
   function showFragModalError(message)
   {
      $("#modalFragSelectAccountAlertText").html(message);
      $("#modalFragSelectAccountAlert").show();
   }

   //hides the modal error and resets the message
   function hideFragModalError()
   {
      $("#modalFragSelectAccountAlertText").html();
      $("#modalFragSelectAccountAlert").hide();
   }
</script>

<!-- Template Footer -->

