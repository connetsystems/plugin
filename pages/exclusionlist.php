<?php
/**
 * Include the header template which sets up the HTML
 *
 * Don't forget to include template_import_script before any Javascripting
 * Don't forget to include template footer.php at the bottom of the page as well
 */
$headerPageTitle = "Exclusion List"; //set the page title for the template import
TemplateHelper::initialize();

echo '<div class="loaderConfirm" id="conf" style="display:none">
            <div class="callout callout-info" style="margin-bottom:0px;">
                <h4>Are you sure?</h4>
                <p id="confTxt"></p>
                <p style="display:none;" id="confId"></p>
            </div>
            
            <table style="width:100%;">
                <tr>
                    <td>
                        <center>
                            <a class="btn btn-block btn-social btn-dropbox" name="" onclick="cancelClick()" style="background-color:' . $accRGB . '; color: #ffffff; border: 2px solid; border-radius: 6px !important; width: 140px; height:36px; margin-top:0px;">
                                <i class="fa fa-ban"></i>No, cancel
                            </a>
                        </center>
                    </td>
                    <td>
                        <center>
                            <a class="btn btn-block btn-social btn-dropbox" name="" onclick="confirmRemove()" style="background-color:' . $accRGB . '; color: #ffffff; border: 2px solid; border-radius: 6px !important; width: 140px; height:36px; margin-top:0px;">
                                <i class="fa fa-check"></i>Yes, remove
                            </a>
                        </center>
                    </td>
                </tr>
            </table>  
        </div>';

if (isset($_GET['addNum']) && $_GET['addNum'] != '') {
   $addID = addToEx($_GET['addNum']);
   //echo '<br><br><br><br>AddID='.$addID;   
}

if (isset($_GET['numId']) && $_GET['numId'] != '') {
   $removeID = removeFromEx($_GET['numId']);
   //echo '<br><br><br><br>RemoveID='.$removeID;
}

$exNums = getExNums();
?>
<aside class="right-side">
   <section class="content-header">
      <h1>
         Blacklist
         <!--small>Control panel</small-->
      </h1>
   </section>

   <section class="content">
      <div class="box box-connet" style="margin-bottom:0px;padding-bottom:1px; border-top-color:<?php echo $accRGB; ?>;">
         <div class="form-group" style="padding-top:10px;padding-left:10px;padding-right:10px;padding-bottom:0px;">
            <div class="callout callout-info" style="margin-bottom:0px;">
               <h4>Blacklist Instructions / Details</h4>
               <p>This page will allow you to add/remove numbers to the blacklist or simply view them below.</p>
            </div>
            <div class="callout callout-danger" id="err" style="margin-bottom:0px;display:none;">
               <h4>Error</h4>
               <p>You tried to add an invalid number.</p>
            </div>
            <?php
            if (isset($_GET['addNum']) && $_GET['addNum'] != '') {
               echo '<div class="callout callout-success" style="margin-bottom:0px;">';
               echo '<h4>Success</h4>';
               echo '<p>You have added number: <b>' . $_GET['addNum'] . '</b></p>';
               echo '</div>';
            }
            if (isset($_GET['numId']) && $_GET['numId'] != '') {
               echo '<div class="callout callout-success" style="margin-bottom:0px;">';
               echo '<h4>Success</h4>';
               echo '<p>You have removed a number.</p>';
               echo '</div>';
            }
            ?>
         </div>
      </div>
   </section>

   <section class="content">
      <div class="box box-connet" style="margin-bottom:15px;padding-bottom:42px; border-top-color:<?php echo $accRGB; ?>;">
         <div class="form-group" style="padding-top:10px;padding-left:10px;padding-right:10px;padding-bottom:0px;">
            <b>Add number to exclusion list: (27123456789)</b>
            <p>
               <input type="text" id="numberToEx" class="form-control" onkeypress="return isNumberKey(event);" maxlength="11" style="border:1px solid #929292;margin-top:5px;height:35px;" name="numberToEx" placeholder="" value="">
            </p>
            <a class="btn btn-block btn-social btn-dropbox "  onclick="addNumToEx(numberToEx.value);" style="background-color: <?php echo $accRGB; ?>; color: #ffffff; border: 2px solid; border-radius: 6px !important; float:left; width: 140px; height:36px; margin-top:0px;">
               <i class="fa fa-plus"></i>Add Number
            </a>
         </div>
      </div>

      <div class="box-body">
         <div class="box-body table-responsive no-padding">
            <table id="example2"  class="table table-hover">
               <thead>
                  <tr>
                     <th style="width:100px;">Date</th>
                     <th>Number</th>
                     <th>Remove</th>
                  </tr>
               </thead>
               <tbody>
                  <?php
                  foreach ($exNums as $key => $value) {
                     echo '<tr>';
                     echo '<td style="width:150px;">' . $value['dnc_created'] . '</td>';
                     echo '<td>' . $value['dnc_msisdn'] . '</td>';
                     echo '<td>' . '
                                                    <a class="btn btn-block btn-social btn-dropbox" name="' . $value['dnc_id'] . '" onclick="removeNum(' . $value['dnc_id'] . ', ' . $value['dnc_msisdn'] . ')" style="background-color:' . $accRGB . '; color: #ffffff; border: 2px solid; border-radius: 6px !important; font-size:0.8em !important; height:24px; width: 140px; margin-top:0px; padding-top:2px;">
                                                        <i class="fa fa-minus" style="font-size:0.8em !important; margin-top:-6px;"></i>Remove Number
                                                    </a>'
                     . '</td>';
                     echo '</tr>';
                  }
                  ?>
               </tbody>
            </table>
         </div>
      </div>
   </section>
</aside>

<!--  THE JAVASCRIPT IMPORT ALWAYS APPEARS BEFORE LOCAL JAVASCRIPT AT THE BOTTOM OF THE PAGE -->
<?php include("template_import_script.php"); //must import all scripts first  ?>
<!-- END JAVASCRIPT IMPORT -->

<script type="text/javascript">
   function isNumberKey(evt)
   {
      var charCode = (evt.which) ? evt.which : evt.keyCode;
      if (charCode != 46 && charCode > 31
              && (charCode < 48 || charCode > 57))
         return false;

      return true;
   }

   function removeNum(id, num)
   {
      $(".loader").fadeIn("slow");
      document.getElementById('conf').style.display = 'block';
      document.getElementById('confTxt').innerHTML = 'You will be removing: ' + num;
      document.getElementById('confId').innerHTML = id;
      document.getElementById('confId').value = id;


      //$(".loaderIcon").fadeIn("slow");
      //window.location = "exclusionlist.php?numId=" + id;
   }

   function cancelClick()
   {
      $(".loader").fadeOut("slow");
      $(".loaderIcon").fadeOut("slow");
      document.getElementById('conf').style.display = 'none';
   }

   function confirmRemove()
   {
      $(".loader").fadeIn("slow");
      $(".loaderIcon").fadeIn("slow");
      var id = document.getElementById('confId').value;
      //alert(id);
      window.location = "exclusionlist.php?numId=" + id;
   }

   function addNumToEx(num)
   {
      //alert(num);
      if (num != '')
      {
         document.getElementById('err').style.display = 'none';
         window.location = "exclusionlist.php?addNum=" + num;
      } else
      {
         document.getElementById('err').style.display = 'block';
      }

   }

</script>

<script type="text/javascript">
   $(function ()
   {
      $('#example2').dataTable({
         "bPaginate": true,
         "bLengthChange": false,
         "bFilter": true,
         "bSort": true,
         "bInfo": true,
         "bAutoWidth": false,
         "iDisplayLength": 100,
         "aoColumns": [
            null,
            null,
            null
         ],
         "aaSorting": [[0, 'desc']]
      });
   });

   jQuery.extend(jQuery.fn.dataTableExt.oSort,
           {
              "title-numeric-pre": function (a) {
                 var x = a.match(/title="*(-?[0-9\.]+)/)[1];
                 return parseFloat(x);
              },
              "title-numeric-asc": function (a, b) {
                 return ((a < b) ? -1 : ((a > b) ? 1 : 0));
              },
              "title-numeric-desc": function (a, b) {
                 return ((a < b) ? 1 : ((a > b) ? -1 : 0));
              }
           });

   jQuery.extend(jQuery.fn.dataTableExt.oSort,
           {
              "formatted-num-pre": function (a) {
                 a = (a === "-" || a === "") ? 0 : a.replace(/[^\d\-\.]/g, "");
                 return parseFloat(a);
              },
              "formatted-num-asc": function (a, b) {
                 return a - b;
              },
              "formatted-num-desc": function (a, b) {
                 return b - a;
              }
           });
</script>

<script type="text/javascript">
   $(window).load(function () {
      $(".loader").fadeOut("slow");
      $(".loaderIcon").fadeOut("slow");
   })
</script>

<!-- Template Footer -->

