<!DOCTYPE html>
<?php
session_start();

/* include("../php/header.php");
  include("../php/allIclusive.php"); */
/**
 * Include the header template which sets up the HTML
 *
 * Don't forget to include template_import_script before any Javascripting
 * Don't forget to include template footer.php at the bottom of the page as well
 */
$headerPageTitle = "Routes / Rates SA"; //set the page title for the template import
TemplateHelper::initialize();
//include("../php/allIclusive.php");
//include("../php/helperRouting.php");

$_GET['country'] = 'South Africa';

if (isset($_GET['range'])) {
   $dateArr = explode(' - ', $_GET['range']);
   // print_r($dateArr);
   $startDate = $dateArr[0];
   $endDate = $dateArr[1];
} else {
   $startDate = date('Y-m-d 00:00:00');
   $endDate = date('Y-m-d 23:59:59');
}
if (isset($_GET['search']) && $_GET['search'] == "1") {
   //get the service name from GET params
   $serviceName = isset($_GET['serviceName']) ? $_GET['serviceName'] : "";
   $mcc = getMCCFromNet($_GET['network']);
   if ((isset($_GET['collection' ]) && $_GET['collection'] != 'Show All') && (isset($_GET['network']) && $_GET['network'] != 'Show All')) {
      $coll = $_GET['collection'];
      $allServices = getBillingData($mcc, $coll, $serviceName);
   } else if ((isset($_GET['network']) && $_GET['network'] != 'Show All') && (isset($_GET['collection']) && strcasecmp($_GET['collection'], "Show All") == 0)) {
      $allServices = getBillingData($mcc, null, $serviceName);
   } else if ((isset($_GET['collection']) && $_GET['collection'] != 'Show All') && (isset($_GET['network']) && strcasecmp($_GET['network'], "Show All") == 0)) {
      $coll = $_GET['collection'];
      $allServices = getBillingData(null, $coll, $serviceName);
   } else {
      $allServices = getBillingData(null, null, $serviceName);
   }
   $billingUnits = getBillingUnits($startDate, $endDate, $mcc);
   $networks = getNetworkViaCountryName('South Africa');
   $collectionsLimit = array();
   $uniqueSMSCArr = array();
   foreach ($allServices as $key => $value) {
      if (!in_array($value['DEFAULT'], $uniqueSMSCArr)) {
         array_push($uniqueSMSCArr, $value['DEFAULT']);
      }

      if (!in_array($value['COLLECTION'], $collectionsLimit)) {
         $collectionsLimit[] = $value['COLLECTION'];
      }
   }
   $allSmscStr = "";
   for ($i = 0; $i < count($uniqueSMSCArr); $i++) {
      $allSmscStr .= "'" . $uniqueSMSCArr[$i] . "_smsc',";
   }
   $allSmscStr = substr($allSmscStr, 0, -1);
   $allSmscStr = (string) $allSmscStr;
   $colCost = getCollectionCost($mcc, $allSmscStr);
   sort($collectionsLimit, SORT_NATURAL | SORT_FLAG_CASE);
   $collections = getCollections();
} else {
   $networks = getNetworkViaCountryName('South Africa');
   $collections = getCollections();
}
?>

<aside class="right-side">
   <section class="content-header">
      <h1>
         South Africa - Routes / Rates
         <!--small>Control panel</small-->
      </h1>
   </section>

   <section class="content">
      <div class="row">
         <div class="col-md-12">
            <div class="box box-connet" style="padding-bottom:1px; border-top-color:<?php echo $accRGB; ?>;">
               <div class="form-group col-md-12" style="padding-top:20px;">
                  <div class="form-group col-md-4">
                     <div id="reportrange" class="select pull-left" onmouseover="" style="cursor: pointer;">
                        <i class="fa fa-calendar fa-lg"></i>
                        <span style="font-size:15px"><?php echo $startDate . " - " . $endDate; ?></span><b class="caret"></b>
                     </div>
                  </div>
               </div>
               <div class="form-group col-md-4">
                  <label>Choose Network</label>
                  <select id="netCheck" class="form-control" onChange="" name="networkCheck">
                     <?php
                     echo "<option SELECTED value='*'>Show All</option>";
                     if (isset($networks)) {
                        foreach ($networks as $key => $value) {
                           //if($value['prefix_network_name'] != '')
                           {
                              if (isset($_GET['network']) && $_GET['network'] == ($value['prefix_network_name'])) {
                                 echo "<option SELECTED>" . $value['prefix_network_name'] . "</option>";
                              } else {
                                 echo "<option>" . $value['prefix_network_name'] . "</option>";
                              }
                           }
                        }
                     }
                     ?>
                  </select>
               </div>
               <div class="form-group col-md-4">
                  <label>Choose Collection</label>
                  <select  id="colCheck" class="form-control" onChange="" name="colCheck">
                     <?php
                     echo "<option SELECTED>Show All</option>";
                     if (isset($collections)) {

                        foreach ($collections as $key => $value) {
                           if (isset($_GET['collection']) && $_GET['collection'] == $value['route_collection_name']) {
                              echo "<option SELECTED>" . $value['route_collection_name'] . "</option>";
                           } else {
                              echo "<option value='" . $value['route_collection_id'] . "' >" . $value['route_collection_name'] . "</option>";
                           }
                        }
                     }
                     if (isset($collectionsLimit)) {
                        for ($i = 0; $i < count($collectionsLimit); $i++) {
                           if ($_GET['collection'] == $collectionsLimit[$i]) {
                              echo "<option SELECTED>" . $collectionsLimit[$i] . "</option>";
                           } else {
                              echo "<option value='" . $collectionsLimit[$i] . "' >" . $collectionsLimit[$i] . "</option>";
                           }
                        }
                     }
                     ?>
                  </select>
               </div>
               <div class="form-group col-md-2">
                  <label>Account / Service:</label>
                  <input  class="form-control" placeholder="Enter Service Or Account Name" size="35" name="serviceName" id="serviceName" type="text" aria-controls="billingTable">
               </div>
               <div class="form-group col-md-2">
                  <a class="btn btn-block btn-social btn-dropbox " onclick="filterSelect2();" style="background-color: <?php echo $accRGB; ?>; height:36px; color: #ffffff; border: 2px solid; border-radius: 6px !important; float:left; width: 100px; margin-top:24px;">
                     <i class="fa fa-search"></i>Search
                  </a>
               </div>
            </div>
         </div>
      </div>
      <div class="row">
         <div class="col-md-12">
            <div class="box box-connet" style="padding-bottom:1px; border-top-color:<?php echo $accRGB; ?>;">
               <div class="box-body table-responsive">
                  <table id="billingTable" class="table table-bordered table-striped dataTable table-hover">
                     <thead>
                        <tr>
                           <th>Service Name</th>
                           <th>Network</th>
                           <th>Country</th>
                           <th>Rate</th>
                           <th>Units</th>
                           <th>Cost</th>
                           <th>Default</th>
                           <th>Cost</th>
                           <th>Ported</th>
                           <th>Collection</th>
                           <th>Status</th>
                           <th>Manager</th>
                        </tr>
                     </thead>
                     <tbody id='billingTableBody'>
                        <?php
                        if (isset($allServices)) {
                           //print_r($allServices);
                           foreach ($allServices as $key => $value) {
                              if (substr($value['COUNTRY'], 0, 3) == "USA") {
                                 $flag = "USA";
                              } else {
                                 $flag = $value['COUNTRY'];
                              }
                              $flag = str_replace('/', '&', $flag);

                              echo '<tr>';
                              echo "<td style='vertical-align:middle;'>" . $value['Service_Name'] . ' (' . $value['service_id'] . ")</td>";

                              $mccNetSubCont = false;
                              if (isset($serProviders) && !empty($serProviders)) {
                                 foreach ($serProviders as $key13 => $value13) {
                                    $imageT = strstr($value13, '.', true);
                                    $net = strstr($value['NETWORK'], ' (', true);

                                    if ($imageT == $net) {
                                       $mccNetSubCont = $imageT;
                                       break;
                                    }
                                 }
                              }

                              if ($mccNetSubCont != false) {
                                 echo "<td style='vertical-align:middle; text-align:center'><img src='../img/serviceproviders/" . $mccNetSubCont . ".png'></td>";
                              } else {
                                 echo "<td style='vertical-align:middle; text-align:center'><img src='../img/serviceproviders/Special.png'>&nbsp;&nbsp;" . $value['NETWORK'] . "</td>";
                              }

                              echo "<td style='text-align:center;'><img src='../img/flags/" . $flag . ".png'></td>";


                              if ($value['CURRENCY'] == 'ZAR') {
                                 //echo "<td style='color:#398a3f;'><center><b>".$value['CURRENCY']."</b></center></td>";
                                 echo '<td style="vertical-align:middle; text-align:center;width:50px;"><b style="color:#993333;">R</b> ' . $value['RATE'] . '</td>';
                              } else {
                                 //echo "<td style='color:#39548a;'><center><b>".$value['CURRENCY']."</b></center></td>";
                                 echo '<td style="vertical-align:middle; text-align:center;width:50px;"><b style="color:#3333dd;"><i class="fa fa-eur"></i></b> ' . $value['RATE'] . '</td>';
                              }

                              $c = 0;
                              if (isset($billingUnits) && !empty($billingUnits)) {
                                 foreach ($billingUnits as $key2 => $value2) {
                                    if ($value2['service_id'] == $value['service_id']) {
                                       if (isset($value2['units']) && $value2['units'] != '') {
                                          echo "<td style='vertical-align:middle; text-align:center;'>" . number_format($value2['units'], 0, '.', ' ') . "</td>";
                                          $c++;
                                          break;
                                       }
                                    }
                                 }
                              }
                              if ($c == 0) {
                                 echo "<td style='vertical-align:middle; text-align:center;'>0</td>";
                              }
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                              $x = 0;
                              if (isset($colCost) && is_array($colCost)) {

                                 foreach ($colCost as $keyX => $valueX) {

                                    $smsc_dv = $value['DEFAULT'] . '_smsc';
                                    $mccmnc_dv = $value['MCCMNC'];

                                    //echo '<br>ss-'.$dv.' - '.$valueX['smsc'];
                                    //echo '<br>'.$valueX['mccmnc'].' - '.$value['MCCMNC'];
                                    if ($valueX['smsc'] == $smsc_dv && $valueX['mccmnc'] == $mccmnc_dv) {

                                       $valueTemp = ' ' . $valueX['cdr_comments'];

                                       if ((strpos($valueTemp, 'eur') != false) || $valueX['cdr_comments'] == 'eur') {
                                          echo '<td id="cc" style="background-color:#c4dec8;vertical-align:middle; text-align:center;width:50px;"><b style="color:#3333dd;"><i class="fa fa-eur"></i></b>' . ($valueX['cost']) . '</td>';
                                          $x++;
                                          break;
                                       } elseif ((strpos($valueTemp, 'usd') != false) || $valueX['cdr_comments'] == 'usd') {
                                          echo '<td id="cc" style="background-color:#c4dec8;vertical-align:middle; text-align:center;width:50px;"><b style="color:#17852c;"><i class="fa fa-usd"></i></b>' . ($valueX['cost']) . '</td>';
                                          $x++;
                                          break;
                                       } elseif ((strpos($valueTemp, 'gbp') != false) || $valueX['cdr_comments'] == 'gbp') {
                                          echo '<td id="cc" style="background-color:#c4dec8;vertical-align:middle; text-align:center;width:50px;"><b style="color:#927119;"><i class="fa fa-gbp"></i></b>' . ($valueX['cost']) . '</td>';
                                          $x++;
                                          break;
                                       } else {
                                          echo '<td id="cc" style="background-color:#c4dec8;vertical-align:middle; text-align:center;width:50px;"><b style="color:#993333;">R </b>' . ($valueX['cost']) . '</td>';
                                          $x++;
                                          break;
                                       }
                                    }

//                                                $smsc_field = $value["DEFAULT"]."_smsc";
//                                                if (strcasecmp($valueX["smsc"], $smsc_field) == 0) {
//                                                    echo '<td id="cc" style="background-color:#c4dec8;vertical-align:middle; text-align:center; width:50px; color:#ff1111;"><b style="color:#993333;">R </b>'.$valueX['cost'].'</td>';
//                                                }
                                 }
                              }


                              if ($x == 0) {
                                 echo '<td id="cc" style="background-color:#c4dec8;vertical-align:middle; text-align:center; width:50px; color:#ff1111;"><b style="color:#993333;">R </b>0.000</td>';
                              }
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                              $mccNetSubCont = false;
                              if (isset($serProviders) && is_array($serProviders)) {
                                 foreach ($serProviders as $key13 => $value13) {
                                    $imageT = strstr($value13, '.', true);

                                    if ($imageT == $value['DEFAULT']) {
                                       $mccNetSubCont = $imageT;
                                       break;
                                    }
                                 }
                              }

                              echo "<td style='vertical-align:middle;background-color:#c4dec8'>" . ucfirst($value['DEFAULT']) . "</td>";

                              $x = 0;
                              if (isset($colCost) && is_array($colCost)) {
                                 foreach ($colCost as $keyX => $valueX) {
                                    $smsc_pt = $value['PORTED'] . '_smsc';
                                    $mccmnc_pt = $value['MCCMNC'];

                                    //echo '<br>ss-'.$dv.' - '.$valueX['smsc'];
                                    if ($valueX['smsc'] == $smsc_pt && $valueX['mccmnc'] == $mccmnc_pt) {
                                       $valueTemp = ' ' . $valueX['cdr_comments'];

                                       if ((strpos($valueTemp, 'eur') != false) || $valueX['cdr_comments'] == 'eur') {
                                          echo '<td id="cc" style="background-color:#c4c4de; vertical-align:middle; text-align:center;width:50px;"><b style="color:#3333dd;"><i class="fa fa-eur"></i></b>' . ($valueX['cost']) . '</td>';
                                          $x++;
                                          break;
                                       } elseif ((strpos($valueTemp, 'usd') != false) || $valueX['cdr_comments'] == 'usd') {
                                          echo '<td id="cc" style="background-color:#c4c4de; vertical-align:middle; text-align:center;width:50px;"><b style="color:#17852c;"><i class="fa fa-usd"></i></b>' . ($valueX['cost']) . '</td>';
                                          $x++;
                                          break;
                                       } elseif ((strpos($valueTemp, 'gbp') != false) || $valueX['cdr_comments'] == 'gbp') {
                                          echo '<td id="cc" style="background-color:#c4c4de; vertical-align:middle; text-align:center;width:50px;"><b style="color:#927119;"><i class="fa fa-gbp"></i></b>' . ($valueX['cost']) . '</td>';
                                          $x++;
                                          break;
                                       } else {
                                          echo '<td id="cc" style="background-color:#c4c4de; vertical-align:middle; text-align:center;width:50px;"><b style="color:#993333;">R </b>' . ($valueX['cost']) . '</td>';
                                          $x++;
                                          break;
                                       }
                                    }
                                 }
                              }

                              if ($x == 0) {
                                 echo '<td id="cc" style="background-color:#c4c4de; vertical-align:middle; text-align:center; width:50px; color:#ff1111;"><b style="color:#993333;">R </b>0.0000</td>';
                              }

                              $mccNetSubCont = false;
                              if (isset($serProviders) && is_array($serProviders)) {
                                 foreach ($serProviders as $key13 => $value13) {
                                    $imageT = strstr($value13, '.', true);

                                    if ($imageT == $value['PORTED']) {
                                       $mccNetSubCont = $imageT;
                                       break;
                                    }
                                 }
                              }

                              /* if($mccNetSubCont != false)
                                {
                                echo '<td style="vertical-align:middle"><img src="../img/serviceproviders/'.$mccNetSubCont.'.png">&nbsp;&nbsp;'.ucfirst($value['PORTED']).'</td>';
                                }
                                else */

                              echo "<td style='vertical-align:middle;background-color:#c4c4de'>" . ucfirst($value['PORTED']) . "</td>";

                              echo "<td style='vertical-align:middle;'>" . $value['COLLECTION'] . "</td>";

                              echo '<td style="vertical-align:middle;">' . $value['STATUS'] . '</td>';

                              echo "<td style='vertical-align:middle;'>" . ucfirst($value['FULLNAME']) . "</td>";
                              //echo "<td style='color:#ff1111;'><center><b>".$value['FULLNAME']."</b></center></td>";

                              echo '</tr>';
                           }
                        }
                        ?>
                     </tbody>
                  </table>
               </div>
               <?php if(count($allServices) > 0 ) {?>
                  <br>
                  <div class="box-body">
                     <div class="form-group">
                        <a href="../php/createCSVSARates.php?mcc=<?php echo $_GET['mcc']; ?>&collection=<?php echo $_GET['collection']; ?>&network=<?php echo $_GET['network']?>&range=<?php echo $_GET['range']?>" class="btn btn-block btn-social btn-dropbox " style="background-color:<?php echo $accRGB; ?>; border: 2px solid; border-radius: 6px !important; float:left; width: 134px; margin-top:-21px;">
                           <i class="fa fa-save"></i>Save Report
                        </a>
                     </div>
                  </div>
               <?php } ?>
            </div>
         </div>
      </div>
   </section>

</aside>

<!--  THE JAVASCRIPT IMPORT ALWAYS APPEARS BEFORE LOCAL JAVASCRIPT AT THE BOTTOM OF THE PAGE -->
<?php include("template_import_script.php"); //must import all scripts first    ?>
<!-- END JAVASCRIPT IMPORT -->
<script type="text/javascript">

   /*$(function()
    {
    $('#billingTable').dataTable
    ({
    "bDestroy":true,
    "bPaginate": true,
    "bLengthChange": false,
    "bFilter": true,
    "bSort": true,
    "bInfo": true,
    "iDisplayLength": 100,
    "bAutoWidth": false,
    "bProcessing": true,
    "bServerSide": false,
    "bSorting": [[ 1, "desc" ]]
    });
    
    });*/

   var repRange = "<?php echo $startDate . ' - ' . $endDate; ?>"

   $(function ()
   {
      /*$('#billingTable').dataTable
       ({
       "bDestroy":true,
       "bPaginate": true,
       "bLengthChange": true,
       "bFilter": true,
       "bSort": true,
       "bInfo": true,
       "iDisplayLength": 100,
       "bAutoWidth": true,
       "bProcessing": true,
       "bServerSide": false,
       "aoColumns": [
       null,
       null,
       null,
       null,
       { "sType": 'formatted-num', targets: 0},
       null,
       null,
       null,
       null,
       null,
       null,
       null
       ],
       "bSorting": [[ 9, "desc" ]]
       });*/

      $('#billingTable').dataTable
              ({
                 "bPaginate": true,
                 "bLengthChange": false,
                 "bFilter": true,
                 "bSort": true,
                 "bSortable": false,
                 "bInfo": true,
                 "bAutoWidth": false,
                 "iDisplayLength": 100,
                 "aoColumns": [
                    null,
                    null,
                    null,
                    null,
                    {"sType": 'formatted-num', targets: 0},
                    null,
                    null,
                    null,
                    null,
                    null,
                    null,
                    null
                 ]

              });

   });

   jQuery.extend(jQuery.fn.dataTableExt.oSort,
           {
              "formatted-num-pre": function (a) {
                 a = (a === "-" || a === "") ? 0 : a.replace(/[^\d\-\.]/g, "");
                 return parseFloat(a);
              },
              "formatted-num-asc": function (a, b) {
                 return a - b;
              },
              "formatted-num-desc": function (a, b) {
                 return b - a;
              }
           });

   $('#reportrange').daterangepicker(
           {
              ranges: {
                 'Today': [moment(), moment()],
                 'Yesterday': [moment().subtract('days', 1), moment().subtract('days', 1)],
                 'Last 7 Days': [moment().subtract('days', 6), moment()],
                 'Last 30 Days': [moment().subtract('days', 29), moment()],
                 'This Month': [moment().startOf('month'), moment().endOf('month')],
                 'Last Month': [moment().subtract('month', 1).startOf('month'), moment().subtract('month', 1).endOf('month')]
              },
              startDate: moment(<?php echo '"' . $startDate . '"'; ?>),
              endDate: moment(<?php echo '"' . $endDate . '"'; ?>)

           },
           function (start, end)
           {
              $('#reportrange span').html(start.format('YYYY-MM-DD 00:00:00') + ' - ' + end.format('YYYY-MM-DD 23:59:59'));
           }
   );

   function filterSelect()
   {
      $(".loader").fadeIn("slow");
      $(".loaderIcon").fadeIn("slow");

      var net = document.getElementById('netCheck').value;
      net = net.trim();
      var coll = document.getElementById('colCheck').value;
      coll = coll.trim();
      window.location = "billing.php?search=1" + "&network=" + net + "&collection=" + coll;
   }

   function filterSelect2()
   {
      $(".loader").fadeIn("slow");
      $(".loaderIcon").fadeIn("slow");
      /*var country = document.getElementById('countryCheck').value;
       country = country.trim();*/
      var net = document.getElementById('netCheck').value;
      net = net.trim();
      var coll = document.getElementById('colCheck').value;
      coll = coll.trim();

      var serviceName = document.getElementById('serviceName').value;
      serviceName = serviceName.trim();

      repRange = $("#reportrange span").html();
      window.location = "billing.php?search=1" + "&network=" + net + "&collection=" + coll + "&serviceName=" + serviceName + "&range=" + repRange;
      //resetValues();
   }
   function filterBySearchTextBox() {

   }


</script>
<script type="text/javascript">
   $(window).load(function () {
      $(".loader").fadeOut("slow");
      $(".loaderIcon").fadeOut("slow");
   })
</script>

<!-- Template Footer -->

