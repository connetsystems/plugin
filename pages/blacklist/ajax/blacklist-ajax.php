<?php

/**
 * TODO: Implement Exceptions
 * 
 */

ob_start();
header('Content-Type: text/plain');

//--------------------------------------------------------------
// Authentication
//--------------------------------------------------------------  
if (false === ($service_id = LoginHelper::getCurrentServiceId())) {

   ob_end_clean();
   header('Content-Type: application/json');

   die(json_encode(array(
       'code' => 401,
       'message' => 'Unauthorized ',
       'data' => null,
   )));
}

$service_id = 451;

//--------------------------------------------------------------
// PDO
//--------------------------------------------------------------  
$pdo = new PDO("mysql:host=" . PLUGIN_DB_MASTER_DB_HOST . ";dbname=" . PLUGIN_DB_MASTER_DB_NAME, PLUGIN_DB_MASTER_DB_USER, PLUGIN_DB_MASTER_DB_PASS, array(
    PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION));

//--------------------------------------------------------------
// Blacklists
//--------------------------------------------------------------  
$sql = <<<SQLGOESHERE
        
SELECT * FROM `campaign_blacklist` 
WHERE `service_id` = :service_id

SQLGOESHERE;

$statement = $pdo->prepare($sql);
$statement->execute(array('service_id' => $service_id));

$blacklists = array();
foreach ($statement->fetchAll(PDO::FETCH_ASSOC) as $row) {

   foreach ($row as $name => $value) {

      if (is_string($value)) {
         $row[$name] = mb_convert_encoding($value, 'UTF-8', 'UTF-8');
      }
   }
   
   $row['blacklist_created'] = date('c', strtotime($row['blacklist_created']));

   $blacklist_id = (integer) $row['blacklist_id'];
   $blacklists[$blacklist_id] = $row;
}

//--------------------------------------------------------------
// Clients / Campaigns
//--------------------------------------------------------------  
$sql = <<<SQLGOESHERE
        
SELECT * FROM `core_service`
LEFT JOIN `campaign_client` USING (`service_id`)        
LEFT JOIN `campaign_campaign` USING (`client_id`)        
WHERE `core_service`.`service_id` = :service_id
  
SQLGOESHERE;

$statement = $pdo->prepare($sql);
$statement->execute(array('service_id' => $service_id));

$clients = array();
foreach ($statement->fetchAll(PDO::FETCH_ASSOC) as $row) {

   foreach ($row as $name => $value) {

      if (is_string($value)) {
         $row[$name] = mb_convert_encoding($value, 'UTF-8', 'UTF-8');
      }
   }

   $service_id = (integer) $row['service_id'];
   $service_name = $row['service_name'];

   $client_id = (integer) $row['client_id'];
   $client_name = $row['client_name'];
   $campaign_id = (integer) $row['campaign_id'];
   $campaign_name = $row['campaign_name'];
   $campaign_code = $row['campaign_code'];

   if (!isset($clients[$client_id])) {

      $clients[$client_id] = array(
          'client_id' => $client_id,
          'client_name' => $client_name,
          'campaigns' => array(
          ),
      );
   }

   $clients[$client_id]['campaigns'][$campaign_id] = array(
       'campaign_id' => $campaign_id,
       'campaign_name' => $campaign_name,
       'campaign_code' => $campaign_code,
   );
}

//--------------------------------------------------------------
// Response
//--------------------------------------------------------------  
ob_end_clean();
header('Content-Type: application/json');

die(json_encode(array(
    'code' => 200,
    'message' => 'Success',
    'data' => array(
        'clients' => $clients,
        'blacklists' => $blacklists
    ),
)));
