
<!-- jQuery -->
<!--<script src="[template-base-url]/js/jquery-2.1.4.min.js"></script>-->
<script src="[template-base-url]/js/jquery.form.min.js"></script>

<!-- Bootstrap -->
<script src="[template-base-url]/js/bootstrap.min.js" type="text/javascript"></script>

<!-- jQuery UI 1.10.3 -->
<script src="[template-base-url]/js/jquery-ui-1.10.3.min.js" type="text/javascript"></script>

<!-- AdminLTE App -->
<script src="[template-base-url]/js/AdminLTE/app.js" type="text/javascript"></script>

<!-- Misc -->
<script src="//cdnjs.cloudflare.com/ajax/libs/raphael/2.1.0/raphael-min.js"></script>
<script src="[template-base-url]/js/plugins/datatables/jquery.dataTables.js" type="text/javascript"></script>
<script src="[template-base-url]/js/plugins/datatables/dataTables.bootstrap.js" type="text/javascript"></script>
<script src="[template-base-url]/js/plugins/colorpicker/bootstrap-colorpicker.min.js" type="text/javascript"></script>
<script src="[template-base-url]/js/plugins/daterangepicker/moment.js" type="text/javascript"></script>
<script src="[template-base-url]/js/plugins/daterangepicker/daterangepickerBulk.js" type="text/javascript"></script>
<script src="[template-base-url]/js/plugins/daterangepicker/daterangepickerNetTraffic.js" type="text/javascript"></script>
<script src="[template-base-url]/js/plugins/chartjs/Chart.min.js" type="text/javascript"></script>

<!-- IMPORT OUR COLOUR PICKER -->
<script type="text/javascript" src="[template-base-url]/js/plugins/colorpicker/bootstrap-colorpicker.min.js"></script>

<!--
<script src="//cdnjs.cloudflare.com/ajax/libs/raphael/2.1.0/raphael-min.js"></script>

<script src="[template-base-url]/js/plugins/morris/morris.min.js" type="text/javascript"></script>
-->

<?php if (isset($_SESSION['mimicUserId'])) { ?>
   <script type="text/javascript">
      $(function () {
         //assign a click listener to the forgot password link
         $("#btnStopMimic").on("click", function (e)
         {
            e.preventDefault();

            $(".loader").fadeIn("slow");
            $(".loaderIcon").fadeIn("slow");
            $("#btnStopMimic").attr('disabled', 'true');

            $.post("[template-base-url]/php/ajaxToggleMimic.php", {mimic_task: 'stop'}).done(function (raw_data) {
               var data = $.parseJSON(raw_data);
               if (data.success)
               {
                  window.location = "[template-base-url]/pages/manage_user.php?user_id="+data.user_id;
               } else
               {
                  alert('There was a problem with disabling mimic mode on this user, please contact the development department.');
                  window.location = "[template-base-url]/index.php";
               }

               $(".loader").fadeOut("slow");
               $(".loaderIcon").fadeOut("slow");
            }).fail(function (data) {
               alert('There was an error: ' + data + '');
               $(".btn_mimic").removeAttr('disabled');
               $(".loaderIcon").fadeOut("slow");
               window.location = "index.php";
            });
         });
      });
   </script>
<?php } ?>


<script type="text/javascript">

   /* Proof of concept select filtering function for select service dropdowns */
   jQuery.fn.filterByText = function (textbox, selectSingleMatch) {
      return this.each(function () {
         var select = this;
         var options = [];
         $(select).find('option').each(function () {
            options.push({value: $(this).val(), text: $(this).text()});
         });
         $(select).data('options', options);
         $(textbox).bind('change keyup', function () {

            var options = $(select).empty().scrollTop(0).data('options');
            var search = $.trim($(this).val());
            var regex = new RegExp(search, 'gi');

            $.each(options, function (i) {
               var option = options[i];
               if (option.text.match(regex) !== null) {
                  $(select).append(
                          $('<option>').text(option.text).val(option.value)
                          );
               }
            });
            if (selectSingleMatch === true &&
                    $(select).children().length === 1) {
               $(select).children().get(0).selected = true;
            }
         });
      });
   };


</script>