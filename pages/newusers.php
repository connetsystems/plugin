<?php
/**
 * Include the header template which sets up the HTML
 *
 * Don't forget to include template_import_script before any Javascripting
 * Don't forget to include template footer.php at the bottom of the page as well
 */
TemplateHelper::setPageTitle('Add Users');
TemplateHelper::initialize();

$user_error = '#aaaaaa';
$error_message = '';
$user_errorMsg = 'Enter new user here..';
$password_error = '#aaaaaa';
$password_errorMsg = 'Enter password here..';
$full_name_error = '#aaaaaa';
$full_name_errorMsg = 'e.g. Jane Smith';

if (isset($_GET['service_id']))
{
   $service_id = $_GET['service_id'];
}

if (isset($_GET['account_id']))
{
   $account_id = $_GET['account_id'];
}
else
{
   $account_id = getAccountId($service_id);
}

if (isset($_GET['account_name']))
{
   $account_name = $_GET['account_name'];
}
else
{
   $account_name = "";
}

if (isset($_GET['successMsg']))
{
   $success_message = 'You have successfully added a new user. ';
}



if (isset($_GET['upd'])) 
{
   $service_id = $_GET['service_id'];
   $validation_count = 0;

   if (isset($_GET['username']) && $_GET['username'] != '') 
   {
      $userCheck = checkUserName($_GET['username']);
      if ($userCheck == 1) 
      {
         $new_user_username = $_GET['username'];
         $validation_count ++;
      } 
      else 
      {
         //$err1 = 1;
         $user_error = '#ffaaaa';
         $error_message = 'The username -' . $_GET['username'] . '- already exists. ';
      }
   } 
   else 
   {
      $error_message = 'Please fill in all the required fields.';
      $user_error = '#ffaaaa';
   }

   if (isset($_GET['password']) && $_GET['password'] != '') 
   {
      $new_user_password = $_GET['password'];
      $validation_count++;
   } 
   else 
   {
      $error_message = 'Please fill in all the required fields.';
      $password_error = "#ffaaaa";
   }

   if (isset($_GET['fullname']) && $_GET['fullname'] != '') 
   {
      $new_user_fullname = $_GET['fullname'];
      $validation_count++;
   } 
   else 
   {
      //$err3 = 1;
      $error_message = 'Please fill in all the required fields.';
      $full_name_error = "#ffaaaa";
   }

   if ($validation_count == 3)
   {
      //user is valid, create the record and add the appropriate configuration
      $inserted_user_id = createNewUser($new_user_username, $new_user_password, $new_user_fullname, $account_id, $service_id);
      addUser($inserted_user_id, $service_id);
      addRolesToUser($inserted_user_id, 'Client');
      createDefaultSkin($account_id);
      $success_message = 'You have successfully added a new user. ';
   }
}


$users = getUsers($service_id);

if (isset($_GET['service_name']))
{
   $service_name = $_GET['service_name'];
}
else
{
   $service_name = getServiceName($service_id);
}

?>

<!-- Right side column. Contains the navbar and content of the page -->
<aside class="right-side">
   <section class="content-header">
      <h1>
         Add Users
      </h1>
   </section>
   <section class="content">
      <div class="row">
         <form action="newusers.php" class="col-md-12" method="get" id="newuser" name="newuser" style="border:none;">
            <div class="box box-connet" style="padding-bottom:1px; border-top-color:<?php echo $accRGB; ?>;">
               <div class="box-body">
                  <div class="callout callout-success" style="height:50px; margin-bottom:0px;">
                     <h4>You have successfully created new service: <b><?php echo htmlentities($service_name); ?></b></h4>
                  </div>
                  <div class="callout callout-info" style="margin-bottom:0px;">
                     <h4>Add Users Instructions / Details</h4>
                     <p>This page will allow you to create NEW USERS and / or add EXISTING USERS.</p>
                  </div>
               </div>
            </div>
            <div class="box box-connet" id="mainBox" style="height:415px; border-top-color:<?php echo $accRGB; ?>;">
               <div class="box-header">
                  <h3 class="box-title">User Details</h3>
               </div>
               <div class="box-body" style="padding:0px;">
                  <div class="sidebar-form-group col-md-12" style="padding-top:10px;">
                     <label>New Username</label>
                     <input type="text" class="form-control" name="username" id="username" maxlength='15' placeholder="<?php echo htmlspecialchars($user_errorMsg); ?>" value="" style="border:1px solid <?php echo htmlentities($user_error); ?>">
                     <br>
                     <label>Password</label>
                     <input type="text" class="form-control" name="password" id="password" maxlength='8' placeholder="<?php echo htmlspecialchars($password_errorMsg); ?>" value="" style="border:1px solid <?php echo htmlentities($password_error); ?>">
                     <br>
                     <label>Full Name</label>
                     <input type="text" class="form-control" name="fullname" id="fullname" placeholder="<?php echo htmlspecialchars($full_name_errorMsg); ?>" value="" style="border:1px solid <?php echo htmlentities($full_name_error); ?>">
                  </div>
                  <br>
                  <div class="sidebar-form-group col-md-12" style="padding-top:10px;padding-bottom:20px;">
                     <!--input type="hidden" name="upd" id="upd" value="updating"-->
                     <?php
                        if (isset($error_message) && $error_message != '')
                        {
                           echo '<div class="callout callout-danger">';
                              echo '<div style="width:80%;">';
                                 echo htmlentities($error_message);
                              echo '</div>';
                              echo '<div style="float:right; margin-top:-22px">';
                                 echo '<i class="fa fa-exclamation-triangle" style="color:#dFb5b4; font-size:20pt;"></i>';
                              echo '</div>';
                           echo '</div>';
                        }
                        elseif (isset($success_message))
                        {
                           echo '<div class="callout callout-success">';
                              echo '<div style="width:80%;">';
                                 echo htmlentities($success_message);
                              echo '</div>';
                              echo '<div style="float:right; margin-top:-22px">';
                                 echo '<i class="fa fa-check" style="color:#5cb157; font-size:20pt;"></i>';
                              echo '</div>';
                           echo '</div>';
                        }
                     ?>
                     <a class="btn btn-social btn-dropbox " onclick="newuser.submit();" style="background-color: <?php echo $accRGB; ?>; color: #ffffff; border: 2px solid; border-radius: 6px !important; float:left; margin-top:0px;">
                        <i class="fa fa-user"></i>Add Additional User
                     </a>
                  </div>
                  <br>
                  <div class="sidebar-form-group col-md-12" style="padding-top:0px;">
                     <div class="box box-connet" style="padding-bottom:1px; border-top-color:<?php echo $accRGB; ?>;">
                        <div class="box-body table-responsive no-padding" >
                           <table class="table table-hover" id="userTable">
                              <tr>
                                 <th>Username</th>
                                 <th>Password</th>
                                 <th>Status</th>
                              </tr>
                              <?php
                              if (isset($users))
                              {
                                 foreach ($users as $key => $value)
                                 {
                                    if ($value['user_username'] === $value['service_login'])
                                    {
                                       $rowColor = ' style="background-color:#f4e8c6;"';
                                    }
                                    else
                                    {
                                       $rowColor = '';
                                    }
                                    echo '<tr' . $rowColor . '>';
                                    echo '<td>' . htmlentities($value['user_username']) . '</td>';
                                    echo '<td>******</td>';

                                    if ($value['user_status'] === "ENABLED")
                                    {
                                       echo '<td><i class="fa fa-check" style="color:#00ff00;"></i>&nbsp;&nbsp;ENABLED</td>';
                                    }
                                    else
                                    {
                                       echo '<td><i class="fa fa-close" style="color:#ff0000;"></i>&nbsp;&nbsp;DISABLED</td>';
                                    }
                                    echo '</tr>';
                                 }
                              }
                              ?>
                              <input type="hidden" name="account_name" value="<?php echo htmlspecialchars($account_name); ?>">
                              <input type="hidden" name="service_name" value="<?php echo htmlspecialchars($service_name); ?>">
                              <input type="hidden" name="account_id" value="<?php echo htmlspecialchars($account_id); ?>">
                              <input type="hidden" name="service_id" value="<?php echo htmlspecialchars($service_id); ?>">
                              <input type="hidden" name="upd" value="upd">
                           </table>
                        </div>
                     </div>
                     <a class="btn btn-social btn-dropbox " onclick="gotoViewService('<?php echo $account_name . ' - ' . $service_name . ' - ' . $service_id; ?>')" style="background-color: <?php echo $accRGB; ?>; color: #ffffff; border: 2px solid; border-radius: 6px !important; float:left; margin-top:0px;">
                        <i class="fa fa-check"></i>Finish
                     </a>
                  </div>
               </div>
            </div>
         </form>
      </div>
   </section>
</aside>

<!--  THE JAVASCRIPT IMPORT ALWAYS APPEARS BEFORE LOCAL JAVASCRIPT AT THE BOTTOM OF THE PAGE -->
<?php include("template_import_script.php"); //must import all scripts first ?>
<!-- END JAVASCRIPT IMPORT -->

<script type="text/javascript">
   $(window).load(function () {
      $(".loader").fadeOut("slow");
      $(".loaderIcon").fadeOut("slow");
   });

   function gotoViewService(name)
   {
      window.location = "viewservice.php?client=" + name;
   }
</script>

<!-- Template Footer -->