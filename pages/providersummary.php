<?php
/**
 * Include the header template which sets up the HTML
 *
 * Don't forget to include template_import_script before any Javascripting
 * Don't forget to include template footer.php at the bottom of the page as well
 */
$headerPageTitle = "Provider Summary"; //set the page title for the template import
TemplateHelper::initialize();

$dir = '../img/serviceproviders';
$serProviders = scandir($dir);

if (isset($_GET['range'])) {
   $dateArr = explode(' - ', $_GET['range']);
   $startDate = $dateArr[0];
   $endDate = $dateArr[1];
} else {
   $startDate = date('Y-m-d 00:00:00');
   $endDate = date('Y-m-d 23:59:59');
}

$provSumm = getProviderSummary($startDate, $endDate);

////////////////////////////////////////////////
////////////////////////////////////////////////
////////////////////////////////////////////////
?>

<aside class="right-side">
   <section class="content-header">
      <h1>
         Provider Summary
         <!--small>Control panel</small-->
      </h1>
   </section>

   <section class="content">
      <div class="box box-connet" style="padding-bottom:1px; border-top-color:<?php echo $accRGB; ?>;">
         <div class="form-group" style="padding-top:10px;padding-left:10px;padding-right:10px;padding-bottom:20px;">
            <div class="callout callout-info">
               <h4>Provider Summary Report Instructions / Details</h4>
               <p>This page provides a Provider Summary for the date range selected below.</p>
            </div>
            <div id="reportrange" class="select pull-left" onmouseover="" style="cursor: pointer;">
               <i class="fa fa-calendar fa-lg"></i>
               <span style="font-size:15px"><?php echo $startDate . " - " . $endDate; ?></span><b class="caret"></b>
            </div>
         </div>
      </div>
   </section>

   <section class="content">
      <div class="box box-connet" style="padding-bottom:1px; border-top-color:<?php echo $accRGB; ?>;">
         <div class="box-header">
            <h3 class="box-title">Provider Summary Total</h3>
         </div><!-- /.box-header -->
         <div class="box-body table-responsive">
            <table id="example2" class="table table-bordered table-striped dataTable table-hover">
               <thead>
                  <tr>
                     <th>Type</th>
                     <th>Country</th>
                     <th>Network</th>
                     <th>SMSC</th>
                     <th><center>Units Sent</center></th>
               <th><center>Cost/Unit</center></th>
               </tr>
               </thead>
               <tbody>
                  <?php
                  foreach ($provSumm as $key => $value) {
                     if (strpos($value['network'], 'Special')) {
                        $logo = trim('Special');
                     } else {
                        $logo = $value['network'];
                        $logoP = strpos($value['network'], ' / ');
                        //echo '<br>logoP='.$logoP.'<-';
                        if ($logoP != '') {
                           $logo = substr($logo, $logoP + 3, strlen($logo));
                        }
                        $logo = strstr($logo, ' (', true);
                        $logo = trim($logo);
                     }

                     if ($logo == '') {
                        $logo = 'Special';
                     }

                     $flag = str_replace(' / ', '', $value['prefix_country_name']);
                     if ($flag == '') {
                        $flag = 'Special';
                     }

                     //$smsc = strstr($value['smsc'], );
                     $smsc = $value['smsc'];
                     $posSmsc = strrpos($smsc, '_');
                     $smsc = (substr($smsc, 0, $posSmsc));
                     echo '<tr>';
                     echo '<td>' . ucfirst($value['type']) . '</td>';
                     echo '<td><img src="../img/flags/' . $flag . '.png">&nbsp;&nbsp;' . $value['prefix_country_name'] . '</td>';
                     if (strpos($value['network'], 'Special')) {
                        echo '<td><img src="../img/serviceproviders/Special.png">&nbsp;&nbsp;' . $value['network'] . '</td>';
                     } else {
                        echo '<td><img src="../img/serviceproviders/' . $logo . '.png">&nbsp;&nbsp;' . $value['network'] . '</td>';
                     }
                     //echo '<td>'.$value['prefix_country_name'].'</td>';
                     //echo '<td>'.$value['network'].'</td>';
                     $counter = 0;
                     foreach ($serProviders as $key3 => $value3) {
                        $value3 = strstr($value3, '.', true);

                        if ($value3 == $smsc) {
                           //echo "<td><img src='../img/serviceproviders/".$value3.".png'>&nbsp;&nbsp;".ucfirst($netw).' ('.$value['5']['buckets'][$i]['mccmnc']['buckets'][$j]['key'].')</td>';
                           echo '<td><img src="../img/serviceproviders/' . $smsc . '.png">&nbsp;&nbsp;' . ucfirst($smsc) . '</td>';
                           $counter++;
                           break;
                        }
                     }
                     if ($counter == 0) {
                        echo "<td><img src='../img/serviceproviders/Special.png'>&nbsp;&nbsp;" . ucfirst($smsc) . '</td>';
                     }
                     echo '<td style="text-align:right">' . $value['units'] . '</td>';
                     echo '<td style="text-align:right">' . $value['cost_price'] . '</td>';
                     echo '</tr>';
                  }
                  ?>
               </tbody>
               <tfoot>
                  <tr>
                     <th>Type</th>
                     <th>Country</th>
                     <th>Network</th>
                     <th>SMSC</th>
                     <th><center>Units Sent</center></th>
               <th><center>Cost/Unit</center></th>
               </tr>
               </tfoot>
            </table>
         </div><!-- /.box-body -->
         <div class="box-body">
            <div class="form-group">
               <a onclick="saveRep();" class="btn btn-block btn-social btn-dropbox " style="background-color:<?php echo $accRGB; ?>; border: 2px solid; border-radius: 6px !important; float:left; width: 134px; margin-top:-21px;">
                  <i class="fa fa-save"></i>Save Report
               </a>
            </div>
         </div>
      </div>
   </section>
</aside>

<!--  THE JAVASCRIPT IMPORT ALWAYS APPEARS BEFORE LOCAL JAVASCRIPT AT THE BOTTOM OF THE PAGE -->
<?php include("template_import_script.php"); //must import all scripts first  ?>
<!-- END JAVASCRIPT IMPORT -->

<script type="text/javascript">
   $('#reportrange').daterangepicker(
           {
              ranges: {
                 'Today': [moment(), moment()],
                 'Yesterday': [moment().subtract('days', 1), moment().subtract('days', 1)],
                 'Last 7 Days': [moment().subtract('days', 6), moment()],
                 'Last 30 Days': [moment().subtract('days', 29), moment()],
                 'This Month': [moment().startOf('month'), moment().endOf('month')],
                 'Last Month': [moment().subtract('month', 1).startOf('month'), moment().subtract('month', 1).endOf('month')]
              },
              startDate: moment(<?php echo '"' . $startDate . '"'; ?>),
              endDate: moment(<?php echo '"' . $endDate . '"'; ?>)
           },
           function (start, end) {
              $(".loader").fadeIn("slow");
              $(".loaderIcon").fadeIn("slow");
              $('#reportrange span').html(start.format('YYYY-MM-DD') + ' - ' + end.format('YYYY-MM-DD'));
              //var serviceName = document.getElementById("client").value;

              var repRange = $("#reportrange span").html();

              window.location = "../pages/providersummary.php?range=" + repRange;
           }
   );
</script>

<script type="text/javascript">

   function saveRep()
   {
      var csvContent = "data:text/csv;charset=utf-8,";

      var data = [["Type", "Country", "Network", "SMSC", "Units Sent", "Cost per Unit"],
<?php
foreach ($provSumm as $key => $value) {
   echo '[';
   echo '"' . $value['type'] . '",';
   echo '"' . $value['prefix_country_name'] . '",';
   echo '"' . $value['network'] . '",';
   echo '"' . $value['smsc'] . '",';
   echo '"' . $value['units'] . '",';
   echo '"' . $value['cost_price'] . '"';
   echo '],';
}
?>
      ];
      data.forEach(function (infoArray, index)
      {
         dataString = infoArray.join(",");
         csvContent += index < data.length ? dataString + "\n" : dataString;
      });
      var encodedUri = encodeURI(csvContent);
      var link = document.createElement("a");
      link.setAttribute("href", encodedUri);
      link.setAttribute("download", "<?php echo 'ProviderSummary_' . $startDate . '-' . $endDate; ?>.csv");

      link.click();
   }

</script>

<script type="text/javascript">
   $(function ()
   {
      $('#example2').dataTable({
         "bPaginate": true,
         "bLengthChange": false,
         "bFilter": true,
         "bSort": true,
         "bInfo": true,
         "bAutoWidth": false,
         "iDisplayLength": 100,
         "aoColumns": [
            null,
            null,
            null,
            null,
            null,
            null
         ],
         "order": [[3, "desc"]],
      });
   });

   jQuery.extend(jQuery.fn.dataTableExt.oSort,
           {
              "title-numeric-pre": function (a) {
                 var x = a.match(/title="*(-?[0-9\.]+)/)[1];
                 return parseFloat(x);
              },
              "title-numeric-asc": function (a, b) {
                 return ((a < b) ? -1 : ((a > b) ? 1 : 0));
              },
              "title-numeric-desc": function (a, b) {
                 return ((a < b) ? 1 : ((a > b) ? -1 : 0));
              }
           });

   jQuery.extend(jQuery.fn.dataTableExt.oSort,
           {
              "formatted-num-pre": function (a) {
                 a = (a === "-" || a === "") ? 0 : a.replace(/[^\d\-\.]/g, "");
                 return parseFloat(a);
              },
              "formatted-num-asc": function (a, b) {
                 return a - b;
              },
              "formatted-num-desc": function (a, b) {
                 return b - a;
              }
           });
</script>

<script type="text/javascript">
   $(window).load(function () {
      $(".loader").fadeOut("slow");
      $(".loaderIcon").fadeOut("slow");
   })
</script>

<!-- Template Footer -->

