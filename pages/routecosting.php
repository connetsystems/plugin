<?php
/**
 * Include the header template which sets up the HTML
 *
 * Don't forget to include template_import_script before any Javascripting
 * Don't forget to include template footer.php at the bottom of the page as well
 */
$headerPageTitle = "Connet Systems | Route Costing"; //set the page title for the template import
TemplateHelper::initialize();

$s = $_SESSION['serviceCredit'];
$sc = ($s / 10000);
if ($sc > 1000) {
   $scS = 'success';
} elseif ($sc < 1000 && $sc > 0) {
   $scS = 'warning';
   # code...
} elseif ($sc <= 0) {
   $scS = 'danger';
   # code...
}
$sc = number_format($sc, 2, '.', ' ');

$dirProviders = '../img/serviceproviders';
$serProviders = scandir($dirProviders);

$dirRoutes = '../img/routes';
$serRoutes = scandir($dirRoutes);

$dirFlags = '../img/flags';
$serFlags = scandir($dirFlags);

$pgUrl = strrchr(realpath(__FILE__), '/');
$pgUrl = '/pages' . $pgUrl;
$pgId = getRoleIDFromUrl($pgUrl);
$pgId = substr($pgId, 0, 1);

$roles = $_SESSION['roles'];

$actual_link = $_SERVER['HTTP_HOST'];

$saver = 0;
$changes = 0;
$s[0] = 'Show All';
$s[1] = 'Show All';
$s[2] = 'Show All';
$s[3] = 'Show All';

if (isset($_GET['upd'])) {
   $changes = 1;
   //echo "ISSET!-".$_GET['upd']."-<br>";
   //echo "ISSET!-".$_GET['smsc']."-<br>";
   //echo "ISSET!-".$_GET['net']."-<br>";
   //echo "ISSET!-".$_GET['countr']."-<br>";

   $routeCostArr = getRouteCost();
   foreach ($routeCostArr as $key => $value) {
      foreach ($_GET as $key2 => $value2) {
         $x = substr($key2, 6, strlen($key2));
         //echo "<br>=".$x;
         if ($value['cdr_id'] == $x) {
            if (substr($key2, 0, 6) == 'idRate') {
               if ($_GET[$key2] == '') {
                  $_GET[$key2] = 0;
               }

               if ($value['cdr_cost_per_unit'] != $_GET[$key2]) {
                  updateServer($_GET[$key2], 'cdr_cost_per_unit', 'reporting.cdr', 'cdr_id', $value['cdr_id']);
                  $changes++;
               }
            } elseif (substr($key2, 0, 6) == 'idComm') {
               if ($value['cdr_comments'] != $_GET[$key2]) {
                  updateServer($_GET[$key2], 'cdr_comments', 'reporting.cdr', 'cdr_id', $value['cdr_id']);
                  $changes++;
               }
            } elseif (substr($key2, 0, 6) == 'idType') {
               if ($value['cdr_type'] != $_GET[$key2]) {
                  updateServer($_GET[$key2], 'cdr_type', 'reporting.cdr', 'cdr_id', $value['cdr_id']);
                  $changes++;
               }
            }
         }
      }
   }
   //echo '<meta http-equiv="refresh" content= "0; URL=routecosting.php?smsc='.$s[0].'&net='.$s[1].'&countr='.$s[2].'" />';
}
if (isset($_GET['smsc']) && isset($_GET['net']) && isset($_GET['countr']) && isset($_GET['typer'])) {
   if ($_GET['smsc'] == 'Show All' && $_GET['net'] == 'Show All' && $_GET['countr'] == 'Show All' && $_GET['typer'] == 'Show All') {
      $routeCostArr = getRouteCost();
   } else {
      $s[0] = $_GET['smsc'];
      $s[1] = $_GET['net'];
      $s[2] = $_GET['countr'];
      $s[3] = $_GET['typer'];
      $routeCostArr = getRouteCost($s);
   }
} else {
   $routeCostArr = getRouteCost();
}

$types = getCdrType();
?>

<!-- Right side column. Contains the navbar and content of the page -->
<aside class="right-side">

   <section class="content-header">
      <h1>
         Route Costing
      </h1>
   </section>

   <section class="content">
      <div class="box box-connet" style="padding-bottom:0px; border-top-color:<?php echo $accRGB; ?>;">
         <div class="box-body">
            <div class="callout callout-info" style="margin-bottom:0px;">
               <h4>Route Costing Instructions / Details</h4>
               <p>This page will show you ALL Route Costing...</p>
            </div>
            <?php
            if ($changes != 0) {
               /* echo '<div style="width:80%;margin-top:50px;">';
                 echo '</div>'; */
               if ($changes == 1) {
                  echo '<div class="callout callout-warning" >';
                  echo "<b>No changes were made...&nbsp;&nbsp;</b>";
                  echo '<div style="float:right; margin-top:-3px">';
                  echo '<i class="fa fa-exclamation-triangle" style="color:#f1e7bc; font-size:20pt;"></i>';
                  echo '</div>';
                  echo '</div>';
               } else {
                  echo '<div class="callout callout-success">';
                  echo "Your changes have been saved.";
                  echo '<div style="float:right; margin-top:-3px">';
                  echo '<i class="fa fa-check" style="color:#5cb157; font-size:20pt;"></i>';
                  echo '</div>';
                  echo '</div>';
               }
            }
            ?>
         </div>
      </div>

      <div class="box box-connet" style="padding-bottom:1px; border-top-color:<?php echo $accRGB; ?>;">
         <div class="box-header">
            <h3 class="box-title">Route Costing</h3>
         </div><!-- /.box-header -->

         <form  class="sidebar-form col-md-12" style="border:none;">
            <div class="sidebar-form-group col-md-3">
               <label>Choose SMSC</label>
               <select class="form-control" id="smscCheck" name="smscCheck" form="smscSpec" onChange="reloadOnSelect(this.value, netCheck.value, conCheck.value, typCheck.value);">
                  <?php
                  echo "<option>Show All</option>";
                  $tArr = array();
                  foreach ($routeCostArr as $key => $value) {
                     $cArr = explode('|', $value['cdr_provider']);
                     foreach ($cArr as $key2 => $value2) {
                        if ($key2 == 2) {
                           $pos = strrpos($cArr[$key2], "_");
                           if ($pos !== false) {
                              $st = substr($cArr[$key2], 0, $pos);
                              if (!in_array($st, $tArr)) {
                                 array_push($tArr, $st);
                              }
                           }
                        }
                     }
                  }
                  asort($tArr);
                  foreach ($tArr as $key => $value) {
                     if (isset($_GET['smsc'])) {
                        if ($_GET['smsc'] == ucfirst($value)) {
                           echo "<option SELECTED>" . ucfirst($value) . "</option>";
                        } else {
                           echo "<option>" . ucfirst($value) . "</option>";
                        }
                     } else {
                        echo "<option>" . ucfirst($value) . "</option>";
                     }
                  }
                  ?>
               </select>
            </div>
            <div class="sidebar-form-group col-md-3">
               <label>Choose Network</label>
               <select class="form-control" id="netCheck" name="netCheck" form="smscSpec" onChange="reloadOnSelect(smscCheck.value, this.value, conCheck.value, typCheck.value);">
                  <?php
                  echo "<option>Show All</option>";
                  $tArr = array();
                  foreach ($routeCostArr as $key => $value) {
                     $cArr = explode('|', $value['cdr_provider']);
                     foreach ($cArr as $key2 => $value2) {
                        if ($key2 == 3) {
                           $mccNet = getNetFromMCC($cArr[$key2]);

                           if (!in_array($mccNet, $tArr)) {
                              array_push($tArr, $mccNet);
                           }
                        }
                     }
                  }
                  asort($tArr);
                  foreach ($tArr as $key => $value) {
                     if (isset($_GET['net'])) {
                        if ($_GET['net'] == ucfirst($value)) {
                           echo "<option SELECTED>" . ucfirst($value) . "</option>";
                        } else {
                           echo "<option>" . ucfirst($value) . "</option>";
                        }
                     } else {
                        echo "<option>" . ucfirst($value) . "</option>";
                     }
                  }
                  ?>
               </select>
            </div>
            <div class="sidebar-form-group col-md-3">
               <label>Choose Country</label>
               <select class="form-control" id="conCheck" name="conCheck" form="smscSpec" onChange="reloadOnSelect(smscCheck.value, netCheck.value, this.value, typCheck.value);">
                  <?php
                  echo "<option>Show All</option>";
                  $tArr = array();
                  foreach ($routeCostArr as $key => $value) {
                     $cArr = explode('|', $value['cdr_provider']);
                     foreach ($cArr as $key2 => $value2) {
                        if ($key2 == 3) {
                           $mccCountry = getCountryFromMCC($cArr[$key2]);

                           if (!in_array($mccCountry, $tArr)) {
                              array_push($tArr, $mccCountry);
                           }
                        }
                     }
                  }
                  asort($tArr);
                  foreach ($tArr as $key => $value) {
                     if (isset($_GET['countr'])) {
                        if ($_GET['countr'] == ucfirst($value)) {
                           echo "<option SELECTED>" . ucfirst($value) . "</option>";
                        } else {
                           echo "<option>" . ucfirst($value) . "</option>";
                        }
                     } else {
                        echo "<option>" . ucfirst($value) . "</option>";
                     }
                  }
                  ?>
               </select>
            </div>
            <div class="sidebar-form-group col-md-3">
               <label>Choose Type</label>
               <select class="form-control" id="typCheck" name="typCheck" form="smscSpec" onChange="reloadOnSelect(smscCheck.value, netCheck.value, conCheck.value, this.value);">
                  <?php
                  echo "<option>Show All</option>";

                  foreach ($types as $key => $value) {
                     if (isset($_GET['typer'])) {
                        if ($_GET['typer'] == ucfirst($value['cdr_type'])) {
                           echo "<option SELECTED>" . ucfirst($value['cdr_type']) . "</option>";
                        } else {
                           echo "<option>" . ucfirst($value['cdr_type']) . "</option>";
                        }
                     } else {
                        echo "<option>" . ucfirst($value['cdr_type']) . "</option>";
                     }
                  }
                  ?>
               </select>
            </div>
         </form>

         <div class="box-body table-responsive">
            <form action="/pages/routecosting.php" method="get" id="saveAllRouteCosting" class="sidebar-form" style="border:0px;" >
               <table id="example2" class="table table-bordered table-striped dataTable table-hover">
                  <thead>
                     <tr>
                        <th style="vertical-align:middle;">#</th>
                        <th style="vertical-align:middle;">DB</th>
                        <!--th>Type</th-->
                        <th style="vertical-align:middle;">SMSC</th>
                        <th style="vertical-align:middle;">Network</th>
                        <th style="vertical-align:middle;">Country</th>
                        <th style="vertical-align:middle;">Rate</th>
                        <th style="vertical-align:middle;">Curr&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</th>
                        <th style="vertical-align:middle;">Effective Date</th>
                        <th style="vertical-align:middle;">Comments</th>
                        <th style="vertical-align:middle;">Type</th>
                     </tr>
                  </thead>
                  <tbody>
                     <?php
                     if (isset($routeCostArr)) {
                        foreach ($routeCostArr as $key => $value) {
                           echo "<tr style='vertical-align:middle'>";
                           $cArr = explode('|', $value['cdr_provider']);
                           echo '<td style="vertical-align:middle; text-align:center;">' . $value['cdr_id'] . '</td>';
                           foreach ($cArr as $key2 => $value2) {
                              if ($key2 == 3) {
                                 $counter = 0;
                                 $mccNet = getNetFromMCC($cArr[$key2]);
                                 $mccNet2 = substr($mccNet, strpos($mccNet, '/'));
                                 //echo "<br>smsc-> --=-".$mccNet;

                                 if (strpos($mccNet2, 'Special') != false) {

                                    echo "<td style='vertical-align:middle'><img src='../img/serviceproviders/Special.png'>&nbsp;&nbsp;" . $mccNet . ' (' . $cArr[$key2] . ")</td>";
                                 } else {
                                    foreach ($serProviders as $key3 => $value3) {
                                       $realName = strstr($value3, '.', true);

                                       $mccNetT = substr($mccNet, strpos($mccNet, '/') + 1);
                                       $mccNetT = trim($mccNetT);

                                       if ($realName == $mccNetT && $mccNetT != '') {
                                          //echo "<br>value-".$realName."- == -".$mccNetT."-";
                                          echo "<td style='vertical-align:middle'><img src='../img/serviceproviders/" . $mccNetT . ".png'>&nbsp;&nbsp;" . $mccNet . ' (' . $cArr[$key2] . ")</td>";
                                          $counter++;
                                          break;
                                       }
                                    }

                                    if ($counter == 0) {
                                       echo "<td style='vertical-align:middle'><img src='../img/serviceproviders/Special.png'>&nbsp;&nbsp;" . $mccNet . ' (' . $cArr[$key2] . ")</td>";
                                    } else {
                                       $counter = 0;
                                    }
                                 }

                                 $mccCountry = getCountryFromMCC($cArr[$key2]);

                                 foreach ($serFlags as $key5 => $value5) {
                                    if (strstr($value5, '.', true) == $mccCountry && $mccCountry != '') {
                                       echo "<td style='vertical-align:middle'><img src='../img/flags/" . $mccCountry . ".png'>&nbsp;&nbsp;" . ucfirst($mccCountry) . "</td>";
                                       $counter++;
                                       break;
                                    }
                                 }

                                 if ($counter == 0) {
                                    echo "<td style='vertical-align:middle'><img src='../img/serviceproviders/Special.png'>&nbsp;&nbsp;" . ucfirst($cArr[$key2]) . "</td>";
                                    //break;
                                 }
                              } elseif ($key2 == 2) {
                                 $name = '';
                                 if ($cArr[$key2] == 'portal_connetsms1_smsc' || $cArr[$key2] == 'portal_connetsms_30mil_smsc' || $cArr[$key2] == 'portal_connetsms30mil_smsc' || $cArr[$key2] == 'portal_connetsms10c_smsc' || $cArr[$key2] == 'portal_connetsms2_smsc') {
                                    $namePos = strrpos($cArr[$key2], "_");

                                    $name = substr($cArr[$key2], 0, $namePos);
                                 } else {
                                    $name = strstr($cArr[$key2], "_", true);
                                 }

                                 $cArr[$key2] = strstr($cArr[$key2], "_", true);


                                 $counter = 0;

                                 foreach ($serProviders as $key4 => $value4) {
                                    if (strstr($value4, '.', true) == $cArr[$key2]) {
                                       echo "<td style='vertical-align:middle'><img src='../img/serviceproviders/" . $cArr[$key2] . ".png'>&nbsp;&nbsp;" . ucfirst($name) . "</td>";
                                       $counter++;
                                       break;
                                    }
                                 }

                                 if ($counter == 0) {
                                    echo "<td style='vertical-align:middle'><img src='../img/serviceproviders/Special.png'>&nbsp;&nbsp;" . ucfirst($cArr[$key2]) . "</td>";
                                 }
                              } else {
                                 if ($cArr[$key2] != 'mtsms') {
                                    echo '<td style="vertical-align:middle">' . ucfirst($cArr[$key2]) . '</td>';
                                 }
                              }
                           }
                           if ($value['cdr_cost_per_unit'] <= 0) {
                              echo '<td><center><input type="text" class="form-control" maxlength="4" style="display:relative !important;border:1px solid #ffaaaa; width:64px; text-align:center;" name="idRate' . $value['cdr_id'] . '" placeholder="" value=""></center></td>';
                              echo '<td style="color:#3333dd;font-size:14pt;vertical-align:middle;"></td>';
                           } else {
                              echo '<td style="vertical-align:middle;"><center><div class="fakey" style="padding-left:10px;width:100px;border-color:#aaffaa;"><div style="display: inline-table;">0.</div><input type="text" onkeypress="return isNumberKey(event)" name="idRate' . $value['cdr_id'] . '" placeholder="" style="background:#ffffff;width:40px;border-color:#aaffaa;border:0px;" maxlength="4" value="' . $value['cdr_cost_per_unit'] . '"></div></center></td>';

                              $valueTemp = ' ' . $value['cdr_comments'];

                              if ((strpos($valueTemp, 'eur') != false) || $value['cdr_comments'] == 'eur') {
                                 echo '<td style="color:#3333dd;vertical-align:middle; text-align:center;"><b><i class="fa fa-eur"></i></b></td>';
                              } elseif ((strpos($valueTemp, 'usd') != false) || $value['cdr_comments'] == 'usd') {
                                 echo '<td style="color:#17852c;vertical-align:middle; text-align:center;"><b><i class="fa fa-usd"></i></b></td>';
                              } elseif ((strpos($valueTemp, 'gbp') != false) || $value['cdr_comments'] == 'gbp') {
                                 echo '<td style="color:#927119;vertical-align:middle; text-align:center;"><b><i class="fa fa-gbp"></i></b></td>';
                              } else {
                                 echo '<td style="color:#993333;vertical-align:middle; text-align:center;"><b>R</b></td>';
                              }
                           }
                           echo '<td style="vertical-align:middle; text-align:center;">' . $value['cdr_effective_date'] . '</td>';
                           if ($value['cdr_comments'] == "") {
                              echo '<td><center><input type="text" class="form-control" maxlength="auto" style="display:relative !important;border:1px solid #ffaaaa; width:auto+10px; text-align:center;" name="idComm' . $value['cdr_id'] . '" placeholder=""></center></td>';
                           } else {
                              echo '<td><center><input type="text" class="form-control" maxlength="auto" style="display:relative !important;border:1px solid #aaffaa; width:auto+10px; text-align:center;" name="idComm' . $value['cdr_id'] . '" placeholder="" value="' . $value['cdr_comments'] . '"></center></td>';
                           }

                           if ($value['cdr_type'] == "") {
                              echo '<td><center><input type="text" class="form-control" maxlength="auto" style="display:relative !important;border:1px solid #ffaaaa; width:auto+10px; text-align:center;" name="idType' . $value['cdr_id'] . '" placeholder=""></center></td>';
                           } else {
                              echo '<td><center><input type="text" class="form-control" maxlength="auto" style="display:relative !important;border:1px solid #aaffaa; width:auto+10px; text-align:center;" name="idType' . $value['cdr_id'] . '" placeholder="" value="' . $value['cdr_type'] . '"></center></td>';
                           }
                           //echo '<td style="vertical-align:middle; text-align:center;">'.$value['cdr_type'].'</td>';
                           echo "</tr>";
                        }
                     }
                     ?>
                  </tbody>
                  <tfoot>
                     <tr>
                        <th>#</th>
                        <th>DB</th>
                        <th>SMSC</th>
                        <th>Network</th>
                        <th>Country</th>
                        <th><center>Rate</center></th>
                  <th><center>Curr</center></th>
                  <th><center>Effective Date</center></th>
                  <th><center>Comments</center></th>
                  <th><center>Type</center></th>
                  </tr>
                  </tfoot>
               </table>
               <?php
               if (isset($_GET['smsc'])) {
                  echo '<input type="hidden" name="smsc" id="smsc" value="' . $_GET['smsc'] . '">';
               }

               if (isset($_GET['net'])) {
                  echo '<input type="hidden" name="net" id="net" value="' . $_GET['net'] . '">';
               }

               if (isset($_GET['countr'])) {
                  echo '<input type="hidden" name="countr" id="countr" value="' . $_GET['countr'] . '">';
               }
               ?>
               <input type="hidden" name="upd" id="upd" value="Save">
            </form>
            <div class="box-footer" style="height:55px;">
               <div class="callout callout-danger" id="clientInfoErr" style="display:none;height:50px;">
                  <div style="width:80%;padding-top:0px;">
                     <h4>You have entered a SERVICE NAME that already exists.</h4>
                  </div>
                  <div style="float:right; margin-top:-32px">
                     <i class="fa fa-exclamation-triangle" style="color:#dFb5b4; font-size:20pt;"></i>
                  </div>
               </div>

               <a class="btn btn-block btn-social btn-dropbox " onclick="saveAllRouteCosting.submit();" style="background-color: <?php echo $accRGB; ?>; color: #ffffff; border: 2px solid; border-radius: 6px !important; float:left; width: 196px; margin-top:0px;">
                  <i class="fa fa-save"></i>Update Route Costing
               </a>
            </div>
         </div><!-- /.box-body -->
      </div><!-- /.box -->
   </section>
   <br>
</aside>

<!--  THE JAVASCRIPT IMPORT ALWAYS APPEARS BEFORE LOCAL JAVASCRIPT AT THE BOTTOM OF THE PAGE -->
<?php include("template_import_script.php"); //must import all scripts first ?>
<!-- END JAVASCRIPT IMPORT -->

<script type="text/javascript">
   $(function ()
   {
      $('#example2').dataTable({
         "bPaginate": true,
         "bLengthChange": false,
         "bFilter": true,
         "bInfo": true,
         "bSort": true,
         "bAutoWidth": true,
         "iDisplayLength": 100,
         /*"aoColumns": [
          null,
          null,
          null,
          null,
          { "sType": 'formatted-num', targets: 0},
          { "sType": 'formatted-num', targets: 0}
          "order": [[ 6, "desc" ]],
          ],*/
      });
   });

   jQuery.extend(jQuery.fn.dataTableExt.oSort, {
      "title-numeric-pre": function (a) {
         var x = a.match(/title="*(-?[0-9\.]+)/)[1];
         return parseFloat(x);
      },
      "title-numeric-asc": function (a, b) {
         return ((a < b) ? -1 : ((a > b) ? 1 : 0));
      },
      "title-numeric-desc": function (a, b) {
         return ((a < b) ? 1 : ((a > b) ? -1 : 0));
      }
   });

   jQuery.extend(jQuery.fn.dataTableExt.oSort, {
      "signed-num-pre": function (a) {
         return (a == "-" || a === "") ? 0 : a.replace('+', '') * 1;
      },
      "signed-num-asc": function (a, b) {
         return ((a < b) ? -1 : ((a > b) ? 1 : 0));
      },
      "signed-num-desc": function (a, b) {
         return ((a < b) ? 1 : ((a > b) ? -1 : 0));
      }
   });

   jQuery.extend(jQuery.fn.dataTableExt.oSort,
           {
              "formatted-num-pre": function (a) {
                 a = (a === "-" || a === "") ? 0 : a.replace(/[^\d\-\.]/g, "");
                 return parseFloat(a);
              },
              "formatted-num-asc": function (a, b) {
                 return a - b;
              },
              "formatted-num-desc": function (a, b) {
                 return b - a;
              }
           });

   jQuery.extend(jQuery.fn.dataTableExt.oSort, {
      "currency-pre": function (a) {
         a = (a === "-") ? 0 : a.replace(/[^\d\-\.]/g, "");
         return parseFloat(a);
      },
      "currency-asc": function (a, b) {
         return a - b;
      },
      "currency-desc": function (a, b) {
         return b - a;
      }
   });
</script>

<script type="text/javascript">
   function reloadOnSelect(x, y, z, a)
   {
      //$(window).load(function() {
      $(".loader").fadeIn("fast");
      $(".loaderIcon").fadeIn("fast");
      // })
      window.location = "../pages/routecosting.php?smsc=" + x + "&net=" + y + "&countr=" + z + "&typer=" + a;
   }
</script>

<script type="text/javascript">
   $(window).load(function () {
      $(".loader").fadeOut("slow");
      $(".loaderIcon").fadeOut("slow");
   })
</script>

<script language=Javascript>
   function isNumberKey(evt)
   {
      var charCode = (evt.which) ? evt.which : evt.keyCode;
      if (charCode != 46 && charCode > 31
              && (charCode < 48 || charCode > 57))
         return false;

      return true;
   }
</script>

<!-- Template Footer -->