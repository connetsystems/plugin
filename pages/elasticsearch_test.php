<?php
/**
 * Include the header tempalte which sets up the HTML
 *
 * Don't forget to include template_import_script before any Javascripting
 * Don't forget to include template footer.php at the bottom of the page as well
 */
$headerPageTitle = "API Test SMS"; //set the page title for the template import
TemplateHelper::initialize();
?>
<!-- header logo: style can be found in header.less -->
<div class="wrapper row-offcanvas row-offcanvas-left">
   <!-- Right side column. Contains the navbar and content of the page -->
   <aside class="left-side sidebar-offcanvas">
      <!-- sidebar: style can be found in sidebar.less -->
      <section class="sidebar">
         <ul class="sidebar-menu">
            <?php
            echo '<div class="callout callout-warning" style="margin-bottom:0px; border-color:#eaeaea; background-color:#eaeaea">';
            echo '<h4>' . $_SESSION['accountName'] . '</h4>';
            echo '<p>' . $_SESSION['serviceName'] . '</p>';
            echo '<p>' . $_SESSION['userName'] . '</p>';
            echo '<span class="label label-' . $scS . '" >R&nbsp;&nbsp;' . $sc . '</span>';

            echo '<p style="margin-top:8px;">' . $_SESSION['serviceBillingType'] . '</p>';
            echo '</div>';
            ?>
            <li>
               <a href="/home.php">
                  <i class="glyphicon glyphicon-home"></i><span>Dashboard</span>
               </a>
            </li>
                    <!--i class="fa fa-home"></i><span>Dashboard</span-->

            <?php
            $openDivCount = 0;
            $active = '';

            for ($i = 0; $i < count($roles); $i++) {
               $roleIC = $roles[$i]['role_icon'];
               $roleNA = $roles[$i]['role_name'];
               $roleUR = $roles[$i]['role_url'];
               $roleID = $roles[$i]['role_id'];
               $roleID_sub = substr($roleID, 0, 1);
               //    echo "<br>2-".$pgId."-=-".$roleID_sub;

               if ($roleID % 100 == 0) {
                  if ($openDivCount == 0) {
                     $openDivCount = 1;
                     //$rId = $roleID%10;
                     //echo "<br>2-".$roleID."-";
                     if ($pgId == $roleID_sub) {
                        $active = 'active';
                     } else {
                        $active = '';
                     }
                     //open div here 
                     echo '<li class="treeview ' . $active . ' ">';
                     echo '<a href="#">';
                     echo '<i class="' . $roleIC . '"></i>';
                     echo '<span>' . $roleNA . '</span>';
                     echo '<i class="fa pull-right fa-angle-left"></i>';
                     echo '</a>';
                     echo '<ul class="treeview-menu" style="display:none;">';
                  } else {
                     echo '</ul>';
                     echo '</li>';

                     if ($pgId == $roleID_sub) {
                        $active = 'active';
                     } else {
                        $active = '';
                     }

                     echo '<li class="treeview ' . $active . ' ">';
                     echo '<a href="#">';
                     echo '<i class="' . $roleIC . '"></i>';
                     echo '<span>' . $roleNA . '</span>';
                     echo '<i class="fa pull-right fa-angle-left"></i>';
                     echo '</a>';
                     echo '<ul class="treeview-menu" style="display:none;">';
                     //close div here
                     //then open new div after here
                  }
               } else {
                  if ($pgUrl == $roleUR) {
                     $active2 = ' class="active" ';
                  } else {
                     $active2 = '';
                  }

                  echo '<li' . $active2 . '>';
                  echo '<a href="' . $roleUR . '">';
                  echo '<i class="' . $roleIC . '"></i> <span>' . $roleNA . '</span>';
                  echo '</a>';
                  echo '</li>';
               }
            }
            echo '</ul>';
            echo '</li>';
            //close last div last
            ?>
         </ul>
      </section>
   </aside>

   <aside class="right-side">
      <section class="content-header">
         <h1>
            API TESTER
            <!--small>Control panel</small-->
         </h1>
      </section>

      <section class="content" style="padding-bottom:0px;">
         <div class="box box-solid" style="padding-top:1px;padding-bottom:1px;margin-bottom:2px;margin-top:2px;">
            <?php
            $mtsms = MTSMS::find(907241662);

            echo "Message content: " . $mtsms->content . "<br/>";
            echo "Parts: " . $mtsms->meta['parts'] . "<br/>";

            echo "<hr/>";

            /*
              //full messages
              $mtsmses = MTSMS::searchByMessageContent("this Christmas with Checkers", 992, 20, 40);

              $count = 1;
              foreach($mtsmses as $sms)
              {
              echo "<br/> (".$count.") ". $sms->content;

              $count ++;
              }
             */

            //place this before any script you want to calculate time
            $time_start = microtime(true);

            $batch_aggr = Batch::getFullSendStats(1071836);

            $time_end = microtime(true);

            //dividing with 60 will give the execution time in minutes other wise seconds
            $execution_time = $time_end - $time_start;

            //execution time of the script
            echo '<b>Total Execution Time:</b> ' . $execution_time . "<br/>";

            echo json_encode($batch_aggr);
            ?>
         </div>
      </section>

      <section class="content" style="margin-top:0px;">

      </section>
   </aside>
</div>


<!--  THE JAVASCRIPT IMPORT ALWAYS APPEARS BEFORE LOCAL JAVASCRIPT AT THE BOTTOM OF THE PAGE -->
<?php include("template_import_script.php"); //must import all scripts first ?>
<!-- END JAVASCRIPT IMPORT -->

<script type="text/javascript">

</script>

<script type="text/javascript" src="../js/plugins/daterangepicker/moment.js"></script>

<script type="text/javascript" src="../js/plugins/daterangepicker/daterangepickerNetTraffic.js"></script>


<script type="text/javascript">
   $(window).load(function () {
      $(".loader").fadeOut("slow");
      $(".loaderIcon").fadeOut("slow");
   })
</script>

<!-- Template Footer -->
