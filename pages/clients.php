<?php
/**
 * Include the header template which sets up the HTML
 *
 * Don't forget to include template_import_script before any Javascripting
 * Don't forget to include template footer.php at the bottom of the page as well
 */
$headerPageTitle = "Connet Systems | Client Report"; //set the page title for the template import
TemplateHelper::initialize();


if (isset($_GET['startDate']) && isset($_GET['endDate'])) {
   $startDate = $_GET['startDate'];
   $endDate = $_GET['endDate'];
} else {
   $startDate = date('Y-m-d 00:00:00');
   $endDate = date('Y-m-d 23:59:59');
}

//echo '<br><br><br><br>---------------------------------->> missing func: getClientReport($startDate, $endDate)';
$clientReport = getClientReport($startDate, $endDate);
//echo '<br>---------------------------------->> 2';
$totalSends = 0;

foreach ($clientReport as $key => $value) {
   $value['Balance'] = $value['Balance'] / 10000;
   $totalSends += $value['Units'];
}

$totalSends = number_format($totalSends, 0, '.', ' ');

/* print_r($clientReport);//."<------------------------------";
  foreach ($clientReport as $key => $value)
  {
  echo '<pre>';
  print_r($value);
  echo '</pre>';
  //  echo "<br>".$key." - ".$value['account_name'];
  } */
//$clients = getClients();
//echo "the client id ".$cl;
/* $funds = getFunds($cl);
  $defaultRouteData = getDefaultRouteData($cl);
  $portedRouteData = getPortedRouteData($cl);
  $users = getUsers($cl);
  $payments = getPayments($cl); */
?>
<!-- Right side column. Contains the navbar and content of the page -->
<aside class="right-side">
   <!-- Content Header (Page header) -->
   <section class="content-header">

      <h1>
         Clients
         <!--small>Control panel</small-->
      </h1>

      <!--ol class="breadcrumb">
          <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
          <li class="active">Client Report</li>
      </ol-->

   </section>

   <section class="content">
      <div class="box box-connet" style="padding-bottom:1px; border-top-color:<?php echo $accRGB; ?>;">
         <div class="box-body" style="padding-bottom:0px;">
            <div class="callout callout-info">
               <h4>Client Report Instructions / Details</h4>
               <p>This page will show you ALL client traffic for the date range that you specify in the calendar below.</p>
            </div>
            <div id="reportrange" class="select pull-left" onmouseover="" style="cursor: pointer;">
               <i class="fa fa-calendar fa-lg"></i>
               <span style="font-size:15px">&nbsp;&nbsp;<?php echo $startDate; ?>&nbsp;&nbsp;-&nbsp;&nbsp;<?php echo $endDate; ?>&nbsp;&nbsp;</span><b class="caret"></b>
            </div>
            <br>
            <br>
            <div class="callout callout-success" style="margin-bottom:10px;">
               <i class="fa fa-thumbs-o-up"></i>
               &nbsp;Total sent for date range: <b><?php echo $totalSends; ?></b>
            </div>
         </div>
      </div>

      <div class="box box-connet" style="padding-bottom:1px; border-top-color:<?php echo $accRGB; ?>;">
         <div class="box-header">
            <h3 class="box-title">Client Traffic Total</h3>
         </div><!-- /.box-header -->
         <div class="box-body table-responsive">
            <table id="example2" class="table table-bordered table-striped dataTable table-hover">
               <thead>
                  <tr>
                     <!--th>#</th-->
                     <th>Account</th>
                     <th>Service</th>
                     <th><center>Units Sent</center></th>
               <th>Manager</th>
               <th>Billing</th>
               <th><center>Balance</center></th>
               </tr>
               </thead>
               <tbody>
                  <?php
                  foreach ($clientReport as $key => $value) {
                     $value['Balance'] = $value['Balance'] / 10000;
                     //$totalSends += $value['Balance'];
                     $value['Balance'] = number_format($value['Balance'], 2, '.', ' ');
                     $value['Units'] = number_format($value['Units'], 0, '.', ' ');
                     echo '<tr>';
                     //echo '<td>'.($key+1).'</td>';
                     echo '<td>' . $value['Account'] . '</td>';
                     echo '<td>' . $value['Service'] . '</td>';
                     echo '<td><center><span class="label label-primary" title="' . $value['Units'] . '">' . $value['Units'] . '</span></center></td>';
                     echo '<td>' . $value['user_fullname'] . '</td>';
                     if ($value['Billing'] == 'POSTPAID') {
                        echo '<td><i style="color:#993311;" class="fa fa-cog fa-spin"></i>&nbsp;&nbsp;' . $value['Billing'] . '</td>';
                     } elseif ($value['Billing'] == 'PREPAID') {
                        echo '<td><i style="color:#009900;" class="fa fa-smile-o"></i>&nbsp;&nbsp;' . $value['Billing'] . '</td>';
                     }
                     //echo '<td>'.$value['Billing'].'</td>';
                     if (substr($value['Balance'], 0, 1) != '-') {
                        echo '<td><center><span class="label label-success" title="' . $value['Balance'] . '">' . $value['Balance'] . '</span></center></td>';
                     } else {
                        echo '<td><center><span class="label label-danger" title="' . $value['Balance'] . '">' . $value['Balance'] . '</span></center></td>';
                     }
                     echo '</tr>';
                  }
                  ?>
               </tbody>
               <tfoot>
                  <tr>
                     <!--th>#</th-->
                     <th>Account</th>
                     <th>Service</th>
                     <th>Manager</th>
                     <th>Billing</th>
                     <th><center>Balance</center></th>
               <th><center>Units Sent</center></th>
               </tr>
               </tfoot>
            </table>
         </div><!-- /.box-body -->
         <div class="box-body">
            <div class="form-group">
               <a onclick="saveRep();" class="btn btn-block btn-social btn-dropbox " style="background-color:<?php echo $accRGB; ?>; border: 2px solid; border-radius: 6px !important; float:left; width: 134px; margin-top:-19px;">
                  <i class="fa fa-save"></i>Save Report
               </a>
            </div>
         </div>
      </div><!-- /.box -->
   </section>

   <br>
</aside>

<!--  THE JAVASCRIPT IMPORT ALWAYS APPEARS BEFORE LOCAL JAVASCRIPT AT THE BOTTOM OF THE PAGE -->
<?php include("template_import_script.php"); //must import all scripts first  ?>
<!-- END JAVASCRIPT IMPORT -->

<script type="text/javascript">
   $('#reportrange').daterangepicker(
           {
              ranges: {
                 'Today': [moment(), moment()],
                 'Yesterday': [moment().subtract('days', 1), moment().subtract('days', 1)],
                 'Last 7 Days': [moment().subtract('days', 6), moment()],
                 'Last 30 Days': [moment().subtract('days', 29), moment()],
                 'This Month': [moment().startOf('month'), moment().endOf('month')],
                 'Last Month': [moment().subtract('month', 1).startOf('month'), moment().subtract('month', 1).endOf('month')]
              },
              startDate: moment(<?php echo '"' . $startDate . '"'; ?>),
              endDate: moment(<?php echo '"' . $endDate . '"'; ?>)
           },
           function (start, end) {
              //alert("hi");
              $('#reportrange span').html(start.format('YYYY-MM-DD 00:00:00') + ' - ' + end.format('YYYY-MM-DD 23:59:59'));
              var st = moment(this.startDate).format("YYYY-MM-DD 00:00:00");
              var et = moment(this.endDate).format("YYYY-MM-DD 23:59:59");
              //window.location.href = 'clientrep.php?startDate=' + st + "&endDate=" + et;
              window.location = "clients.php?startDate=" + st + "&endDate=" + et;
           }
   );
</script>
<!-- AdminLTE for demo purposes -->
<script type="text/javascript">

   function saveRep()
   {
      var csvContent = "data:text/csv;charset=utf-8,";

      var data = [["Account", "Service", "Manager", "Billing", "Balance", "Units Sent"],
<?php
foreach ($clientReport as $key => $value) {
   $value['Balance'] = $value['Balance'] / 10000;
   //$totalSends += $value['Balance'];
   $value['Balance'] = number_format($value['Balance'], 2, '.', '');
   $value['Units'] = number_format($value['Units'], 0, '.', '');
   echo '[';
   echo '"' . $value['Account'] . '",';
   echo '"' . $value['Service'] . '",';
   echo '"' . $value['user_fullname'] . '",';
   echo '"' . $value['Billing'] . '",';
   echo '"' . $value['Balance'] . '",';
   echo '"' . $value['Units'] . '"';
   echo '],';
}
?>
      ];
      data.forEach(function (infoArray, index)
      {
         dataString = infoArray.join(",");
         csvContent += index < data.length ? dataString + "\n" : dataString;
      });
      var encodedUri = encodeURI(csvContent);
      var link = document.createElement("a");
      link.setAttribute("href", encodedUri);
      link.setAttribute("download", "<?php echo $startDate . '-' . $endDate; ?>.csv");

      link.click();
   }


</script>

<script src="../js/plugins/datatables/jquery.dataTables.js" type="text/javascript"></script>

<script src="../js/plugins/datatables/dataTables.bootstrap.js" type="text/javascript"></script>

<script type="text/javascript">

   jQuery.extend(jQuery.fn.dataTableExt.oSort, {
      "title-numeric-pre": function (a) {
         var x = a.match(/title="*(-?[0-9\.]+)/)[1];
         return parseFloat(x);
      },
      "title-numeric-asc": function (a, b) {
         return ((a < b) ? -1 : ((a > b) ? 1 : 0));
      },
      "title-numeric-desc": function (a, b) {
         return ((a < b) ? 1 : ((a > b) ? -1 : 0));
      }
   });

   jQuery.extend(jQuery.fn.dataTableExt.oSort, {
      "signed-num-pre": function (a) {
         return (a == "-" || a === "") ? 0 : a.replace('+', '') * 1;
      },
      "signed-num-asc": function (a, b) {
         return ((a < b) ? -1 : ((a > b) ? 1 : 0));
      },
      "signed-num-desc": function (a, b) {
         return ((a < b) ? 1 : ((a > b) ? -1 : 0));
      }
   });

   jQuery.extend(jQuery.fn.dataTableExt.oSort,
           {
              "formatted-num-pre": function (a) {
                 a = (a === "-" || a === "") ? 0 : a.replace(/[^\d\-\.]/g, "");
                 return parseFloat(a);
              },
              "formatted-num-asc": function (a, b) {
                 return a - b;
              },
              "formatted-num-desc": function (a, b) {
                 return b - a;
              }
           });

   jQuery.extend(jQuery.fn.dataTableExt.oSort, {
      "currency-pre": function (a) {
         a = (a === "-") ? 0 : a.replace(/[^\d\-\.]/g, "");
         return parseFloat(a);
      },
      "currency-asc": function (a, b) {
         return a - b;
      },
      "currency-desc": function (a, b) {
         return b - a;
      }
   });

   $(function ()
   {
      $('#example2').dataTable({
         "bPaginate": true,
         "bLengthChange": false,
         "bFilter": true,
         "bInfo": true,
         "bsort": true,
         "bSortable": true,
         "bAutoWidth": false,
         "iDisplayLength": 100,
         "aoColumns": [
            null,
            null,
            {"sType": 'formatted-num', targets: 0},
            null,
            null,
            {"sType": 'currency', targets: 0}
         ],
      });
   });

</script>

<script type="text/javascript">
   $(window).load(function () {
      $(".loader").fadeOut("slow");
      $(".loaderIcon").fadeOut("slow");

      var oTable = $('#example2').dataTable();
      oTable.fnSort([[2, 'asc']]);

// Sort immediately with columns 0 and 1
   })
</script>

<!-- Template Footer -->