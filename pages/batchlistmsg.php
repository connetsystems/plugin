<?php
/**
 * Include the header template which sets up the HTML
 *
 * Don't forget to include template_import_script before any Javascripting
 * Don't forget to include template footer.php at the bottom of the page as well
 */
$headerPageTitle = "Bulk SMS Messages"; //set the page title for the template import
TemplateHelper::initialize();

ini_set('memory_limit', '256M');

if (isset($_GET['type']) && $_GET['type'] != '') {
   $type = $_GET['type'];
} else {
   $type = '';
}

if (isset($_GET['cli'])) {
   //echo '<br>Cli='.$_GET['cli'];
   $eCli = $_GET['cli'];
}

if (isset($_GET['camp'])) {
   //echo '<br>camp='.$_GET['camp'];
   $eCamp = $_GET['camp'];
}

if (isset($_GET['code'])) {
   $eCode = $_GET['code'];
   ///echo '<br>code='.$_GET['code'];
}

if (isset($_GET['ref'])) {
   $eRef = $_GET['ref'];
   //echo '<br>ref='.$_GET['ref'];
}


if (isset($_GET['rr'])) {
   $rr = $_GET['rr'];
   /* $dateArr1 = explode(' - ', $_GET['range1']);
     $start_date1 = $dateArr1[0];
     $start_dateConv1 = date('Y-m-d', ($start_date1/1000)); */
}

if (isset($_GET['start_date']) && isset($_GET['end_date']))
{
   $start_date = $_GET['start_date'];
   $end_date = $_GET['end_date'];
}
else
{
   $start_date = "";
   $end_date = "";
}

$start_dateConv = date('Y-m-d', ($start_date / 1000));
$end_dateConv = date('Y-m-d', ($end_date / 1000));

if (isset($_GET['range1'])) {
   $range1 = $_GET['range1'];
   $dateArr1 = explode(' - ', $_GET['range1']);
   $start_date1 = strtotime($dateArr1[0]);
   if (isset($dateArr1[1])) {
      $end_date1 = strtotime($dateArr1[1]);
      if ($start_date1 == $end_date1) {
         $end_date1 = $end_date1; // + (24*60*60);
      }
      $start_date1 = $start_date1 * 1000;
      $end_date1 = ($end_date1 + (24 * 60 * 60) - 1) * 1000;
   } else {
      $end_date1 = ($start_date1 + (24 * 60 * 60) - 1) * 1000;
   }
}

/* echo '<br><br><br>S-------------'.$start_date1;
  echo '<br>E-------------'.$end_date1; */

$start_dateConv1 = date('Y-m-d', ($start_date1 / 1000));
$end_dateConv1 = date('Y-m-d', ($end_date1 / 1000));

if (isset($_GET['from'])) {
   $from = $_GET['from'];
   $starting = ($from - 1) * 100;
   //$from = $from - 1;
}

if (isset($_GET['query']) && $_GET['query'] != '') {

   $qryArr = explode(',', $_GET['query']);
   $qryStr = 'AND (';
   for ($i = 0; $i < count($qryArr); $i++) {
      $qryStr .= ' dlr:' . $qryArr[$i] . ' OR';
   }
   $qryStr = substr($qryStr, 0, -3) . ')';
} else {
   $_GET['query'] = '';
   $qryStr = '';
}

if (isset($_GET['sort'])) {
   $sort = urldecode($_GET['sort']);
   $ordering = strstr($sort, ':');
   $ordering = trim($ordering, ':');
   $ordering = trim($ordering, '"');
   $sortUrl = urlencode($sort);
}

if (isset($_GET['searchTerm']) && $_GET['searchTerm'] != '') {
   $searchT = $_GET['searchTerm'];

   $searchTerm = ',{
                     "multi_match" : {
                        "query":    "' . $searchT . '",
                        "fields": ["smsc", "src", "dest", "content", "mccmnc"]
                      }   
                    }';
} else {
   $searchT = '';
   $searchTerm = '';
}


if (isset($_GET['service_id'])) {
   $client = $_GET['service_id'];
   $service_id = $_GET['service_id'];
   $batch_id = $_GET['bId'];

   $dsp = 'inline';
   $dir = '../img/serviceproviders';
   $serProviders = scandir($dir);

   $qry = '{
              "size":100,
              "from":' . $starting . ',
              "sort": {
                ' . $sort . '
              },
              "fields" : ["mtsms_id","timestamp","src","dest","content","dlr","mccmnc","smsc","rdnc"],
              "query": {
                "filtered": {
                  "query": {
                    "bool" : {
                        "must" : [
                            {
                             "query_string": {
                             "query": "service_id:' . $service_id . ' AND meta.batch_id:' . $batch_id . ' ' . $qryStr . '",
                             "analyze_wildcard": true
                                }
                            }
                            ' . $searchTerm . '
                        ]
                    }
                  }

                }
              }
            }';

   $allStats = runRawMTSMSElasticsearchQuery($qry);

   $units = $allStats['hits']['total'];

   $client = getServiceName($_GET['service_id']);
}
?>

<aside class="right-side">
   <section class="content-header">
      <h1>
         Bulk SMS Traffic
         <!--small>Control panel</small-->
      </h1>
   </section>

   <section class="content">
      <div class="box box-connet" style="padding-top:10px;padding-left:10px;padding-right:10px;padding-bottom:45px; border-top-color:<?php echo $accRGB; ?>;">
         <div class="callout callout-info" style="margin-bottom:10px;">
            <h4><?php echo $type; ?> Messages: <?php echo $client; ?></h4>
            <p>You are viewing all <?php echo ucfirst($type); ?> traffic SMS's for the date: <strong><?php echo $start_dateConv; ?> - <?php echo $end_dateConv; ?></strong></p>
         </div>
         <a class="btn btn-block btn-social btn-dropbox" name="nid" onclick="backToTraffic()" style="background-color:<?php echo $accRGB; ?>; border: 2px solid; border-radius: 6px !important; float:left; width: 100px;">
            <i class="fa fa-arrow-left"></i>Back
         </a>
      </div>
   </section>

   <section class="content">
      <div class="box box-connet" style="padding-top:1px; border-top-color:<?php echo $accRGB; ?>;">
         <div class="box-body">
            <div class="box-body table-responsive no-padding">
               <div class="row">
                  <div class="col-xs-11">
                     <div class="dataTables_filter" style="padding-top:6px;">
                        <label>Search <input type="text" id="search_filter" aria-controls="example2"></label>
                     </div>
                  </div>
                  <div class="col-xs-1" style="padding-left:0px !important;">
                     <a class="btn btn-block btn-social btn-dropbox" name="nid" onclick="searchText()" style="background-color:<?php echo $accRGB; ?>; border: 2px solid; border-radius: 6px !important; float:left; width: 100px;">
                        <i class="fa fa-search"></i>Search
                     </a>
                  </div>
               </div>
               <table id="example2"  class="table table-striped table-hover">
                  <thead>
                     <tr>
                        <?php
                        if (isset($_GET['controller'])) {
                           if ($ordering == 'desc') {
                              $ordering = 'asc';
                           } else {
                              $ordering = 'desc';
                           }
                        }
//echo '<th>id</th>';
                        echo '<th style="width:100px;"><a href="../pages/batchlistmsg.php?service_id=' . $_GET['service_id'] . '&cli=' . urlencode($eCli) . '&camp=' . urlencode($eCamp) . '&code=' . urlencode($eCode) . '&ref=' . urlencode($eRef) . '&start_date='  .  $start_date .  '&end_date=' . $end_date . '&type=' . $type . '&bId=' . $_GET['bId'] . '&range1=' . $_GET['range1'] . '&sents=' . $units . '&from=1&controller=1&query=' . $_GET['query'] . '&sort=' . '%22mtsms_id%22%3A%22' . $ordering . '%22' . '">id</a></th>';
                        echo '<th style="width:100px;"><a href="../pages/batchlistmsg.php?service_id=' . $_GET['service_id'] . '&cli=' . urlencode($eCli) . '&camp=' . urlencode($eCamp) . '&code=' . urlencode($eCode) . '&ref=' . urlencode($eRef) . '&start_date='  .  $start_date .  '&end_date=' . $end_date . '&type=' . $type . '&bId=' . $_GET['bId'] . '&range1=' . $_GET['range1'] . '&sents=' . $units . '&from=1&controller=1&query=' . $_GET['query'] . '&sort=' . '%22timestamp%22%3A%22' . $ordering . '%22' . '">Date</a></th>';
                        echo '<th style="width:100px;"><a href="../pages/batchlistmsg.php?service_id=' . $_GET['service_id'] . '&cli=' . urlencode($eCli) . '&camp=' . urlencode($eCamp) . '&code=' . urlencode($eCode) . '&ref=' . urlencode($eRef) . '&start_date='  .  $start_date .  '&end_date=' . $end_date . '&type=' . $type . '&bId=' . $_GET['bId'] . '&range1=' . $_GET['range1'] . '&sents=' . $units . '&from=1&controller=1&query=' . $_GET['query'] . '&sort=' . '%22src%22%3A%22' . $ordering . '%22' . '">Number</a></th>';
//echo '<th style="width:100px;"><a href="../pages/batchlistmsg.php?client='.$_GET['service_id'].'&range='.$_GET['range'].'&type='.$type.'&bId='.$_GET['bId'].'&range1='.$_GET['range1'].'&sents='.$units.'&from=1&controller=1&query='.$_GET['query'].'&sort='.'%22dest%22%3A%22'.$ordering.'%22'.'">To</a></th>';
                        echo '<th>Content</th>';
                        echo '<th style="width:100px;"><a href="../pages/batchlistmsg.php?service_id=' . $_GET['service_id'] . '&cli=' . urlencode($eCli) . '&camp=' . urlencode($eCamp) . '&code=' . urlencode($eCode) . '&ref=' . urlencode($eRef) . '&start_date='  .  $start_date .  '&end_date=' . $end_date . '&type=' . $type . '&bId=' . $_GET['bId'] . '&range1=' . $_GET['range1'] . '&sents=' . $units . '&from=1&controller=1&query=' . $_GET['query'] . '&sort=' . '%22mccmnc%22%3A%22' . $ordering . '%22' . '">Network</a></th>';
                        echo '<th style="width:100px;"><a href="../pages/batchlistmsg.php?service_id=' . $_GET['service_id'] . '&cli=' . urlencode($eCli) . '&camp=' . urlencode($eCamp) . '&code=' . urlencode($eCode) . '&ref=' . urlencode($eRef) . '&start_date='  .  $start_date .  '&end_date=' . $end_date . '&type=' . $type . '&bId=' . $_GET['bId'] . '&range1=' . $_GET['range1'] . '&sents=' . $units . '&from=1&controller=1&query=' . $_GET['query'] . '&sort=' . '%22dlr%22%3A%22' . $ordering . '%22' . '">Status</a></th>';
                        ?>
                     </tr>
                  </thead>
                  <tbody>
                     <?php
                     $dont_display_count = 0;
                     foreach ($allStats as $key => $value) {
                        if ($key == 'hits')
                        {
                           $hits = count($value['hits']);
                           for ($i = 0; $i < $hits; $i++)
                           {
                              //we need to exclude rejected and blacklisted for now
                              $dlr_string = DeliveryReportHelper::dlrMaskToString($value['hits'][$i]['fields']['dlr']['0']);
                              if(strtolower($dlr_string) != "rejected" && strtolower($dlr_string) != "blacklisted")
                              {
                                 $netw = getNetFromMCC($value['hits'][$i]['fields']['mccmnc']['0']);
                                 //$smsc = $value['hits'][$i]['fields']['smsc']['0'];
                                 //$posSmsc = strrpos($smsc, '_');
                                 //$smsc = ucfirst(substr($smsc, 0, $posSmsc));
                                 $time = str_replace('T', ' ', $value['hits'][$i]['fields']['timestamp']['0']);
                                 $time = strstr($time, '+', true);
                                 $timeSec = strrpos($time, ':');
                                 $time = substr($time, 0, $timeSec);

                                 echo '<tr>';
                                 echo '<td>' . $value['hits'][$i]['fields']['mtsms_id']['0'] . '</td>';
                                 echo '<td>' . $time . '</td>';
                                 //echo '<td>'.$value['hits'][$i]['fields']['src']['0'].'</td>';
                                 echo '<td>' . $value['hits'][$i]['fields']['dest']['0'] . '</td>';

                                 //fix the message encoding out of elasticsearch
                                 $message = utf8_decode($value['hits'][$i]['fields']['content']['0']);
                                 $message = helperSMSGeneral::restorePlaceholderValuesForDisplay($message); //reverse the placeholders so they look like the original text the user entered
                                 echo '<td style="white-space:pre-wrap;">' . $message . '</td>';

                                 $counter = 0;
                                 foreach ($serProviders as $key3 => $value3) {
                                    $value3 = strstr($value3, '.', true);

                                    if (!empty($value3) && strpos($netw, $value3) !== false) {
                                       echo "<td style='width:180px;'><img src='../img/serviceproviders/" . $value3 . ".png'>&nbsp" . str_replace(' ', '&nbsp;', ($netw)) . '&nbsp;(' . $value['hits'][$i]['fields']['mccmnc']['0'] . ')</td>';
                                       //echo '<td>'.$netw.'</td>';
                                       $counter++;
                                       break;
                                    }
                                 }
                                 if ($counter == 0) {
                                    echo "<td style='width:180px;'><img src='../img/serviceproviders/Special.png'>&nbsp;" . str_replace(' ', '&nbsp;', ($netw)) . '&nbsp;(' . $value['hits'][$i]['fields']['mccmnc']['0'] . ')</td>';
                                 }

                                 $dlrPos = $value['hits'][$i]['fields']['dlr']['0'];
                                 if (($dlrPos & 1) == 1) {
                                    echo '<td style="color:#009900;">Delivered</td>';
                                 } elseif (($dlrPos & 16) == 16) {
                                    echo '<td style="color:#aa0234;">Rejected</td>';
                                 } elseif (($dlrPos & 32) == 32 && ($dlrPos & 1) == 0 && ($dlrPos & 2) == 0) {
                                    echo '<td style="color:#eec00d;">Pending</td>';
                                 } elseif (($dlrPos & 2) == 2) {
                                    echo '<td style="color:#cb0234;">Failed</td>';
                                 } elseif ($dlrPos == 4288) {
                                    echo '<td style="color:#cb0234;">Failed</td>';
                                 } else {
                                    echo '<td>' . $dlrPos . '</td>';
                                 }
                                 echo '</tr>';
                              }
                              else
                              {
                                 $dont_display_count ++;
                              }
                           }
                        }
                     }
                     ?>
                  </tbody>
               </table>
               <div class="row">
                  <div class="col-xs-6">
                     <div class="dataTables_info" id="example2_info">Showing <?php echo ($starting == 0 ? 1 : $starting); ?> to <?php
                        if (($from * 100) < ($units - $dont_display_count)) {
                           echo ($from * 100);
                        } else {
                           echo ($units - $dont_display_count);
                        }
                        ?> of <?php echo ($units - $dont_display_count); ?> entries
                     </div>
                  </div>
                  <div class="col-xs-6">
                     <div class="dataTables_paginate paging_bootstrap">
                        <ul class="pagination">
                           <?php
                           if ($from > 1) {
                              echo '<li class="prev">';
                              echo '<a href="../pages/batchlistmsg.php?service_id=' . $_GET['service_id'] . '&start_date='  .  $start_date .  '&end_date=' . $end_date . '&cli=' . urlencode($eCli) . '&camp=' . urlencode($eCamp) . '&code=' . urlencode($eCode) . '&ref=' . urlencode($eRef) . '&type=' . $type . '&bId=' . $_GET['bId'] . '&range1=' . $_GET['range1'] . '&sents=' . $units . '&searchTerm=' . $searchT . '&from=1&sort=' . $sortUrl . '&query=' . $_GET['query'] . '">&#8592; First</a>';
                              echo '</li>';
                              if ($from > 2) {
                                 echo '<li>';
                                 echo '<a href="../pages/batchlistmsg.php?service_id=' . $_GET['service_id'] . '&start_date='  .  $start_date .  '&end_date=' . $end_date . '&cli=' . urlencode($eCli) . '&camp=' . urlencode($eCamp) . '&code=' . urlencode($eCode) . '&ref=' . urlencode($eRef) . '&type=' . $type . '&bId=' . $_GET['bId'] . '&range1=' . $_GET['range1'] . '&sents=' . $units . '&searchTerm=' . $searchT . '&from=' . ($from - 2) . '&sort=' . $sortUrl . '&query=' . $_GET['query'] . '">' . ($from - 2) . '</a>';
                                 echo '</li>';
                              }
                              echo '<li>';
                              echo '<a href="../pages/batchlistmsg.php?service_id=' . $_GET['service_id'] . '&start_date='  .  $start_date .  '&end_date=' . $end_date . '&cli=' . urlencode($eCli) . '&camp=' . urlencode($eCamp) . '&code=' . urlencode($eCode) . '&ref=' . urlencode($eRef) . '&type=' . $type . '&bId=' . $_GET['bId'] . '&range1=' . $_GET['range1'] . '&sents=' . $units . '&searchTerm=' . $searchT . '&from=' . ($from - 1) . '&sort=' . $sortUrl . '&query=' . $_GET['query'] . '">' . ($from - 1) . '</a>';
                              echo '</li>';
                           }
                           echo '<li class="active">';
                           echo '<a href="../pages/batchlistmsg.php?service_id=' . $_GET['service_id'] . '&start_date='  .  $start_date .  '&end_date=' . $end_date . '&cli=' . urlencode($eCli) . '&camp=' . urlencode($eCamp) . '&code=' . urlencode($eCode) . '&ref=' . urlencode($eRef) . '&type=' . $type . '&bId=' . $_GET['bId'] . '&range1=' . $_GET['range1'] . '&sents=' . $units . '&searchTerm=' . $searchT . '&from=' . $from . '&sort=' . $sortUrl . '&query=' . $_GET['query'] . '">' . $from . '</a>';
                           echo '</li>';
                           if (($from * 100) < $units) {
                              echo '<li>';
                              echo '<a href="../pages/batchlistmsg.php?service_id=' . $_GET['service_id'] . '&start_date='  .  $start_date .  '&end_date=' . $end_date . '&cli=' . urlencode($eCli) . '&camp=' . urlencode($eCamp) . '&code=' . urlencode($eCode) . '&ref=' . urlencode($eRef) . '&type=' . $type . '&bId=' . $_GET['bId'] . '&range1=' . $_GET['range1'] . '&sents=' . $units . '&searchTerm=' . $searchT . '&from=' . ($from + 1) . '&sort=' . $sortUrl . '&query=' . $_GET['query'] . '">' . ($from + 1) . '</a>';
                              echo '</li>';
                              if ((($from + 1) * 100) < $units) {
                                 echo '<li>';
                                 echo '<a href="../pages/batchlistmsg.php?service_id=' . $_GET['service_id'] . '&start_date='  .  $start_date .  '&end_date=' . $end_date . '&cli=' . urlencode($eCli) . '&camp=' . urlencode($eCamp) . '&code=' . urlencode($eCode) . '&ref=' . urlencode($eRef) . '&type=' . $type . '&bId=' . $_GET['bId'] . '&range1=' . $_GET['range1'] . '&sents=' . $units . '&searchTerm=' . $searchT . '&from=' . ($from + 2) . '&sort=' . $sortUrl . '&query=' . $_GET['query'] . '">' . ($from + 2) . '</a>';
                                 echo '</li>';
                              }
                              echo '<li class="next">';
                              echo '<a href="../pages/batchlistmsg.php?service_id=' . $_GET['service_id'] . '&start_date='  .  $start_date .  '&end_date=' . $end_date . '&cli=' . urlencode($eCli) . '&camp=' . urlencode($eCamp) . '&code=' . urlencode($eCode) . '&ref=' . urlencode($eRef) . '&type=' . $type . '&bId=' . $_GET['bId'] . '&range1=' . $_GET['range1'] . '&sents=' . $units . '&searchTerm=' . $searchT . '&from=' . (round($units / 100) + 1) . '&sort=' . $sortUrl . '&query=' . $_GET['query'] . '">Last &#8594;</a>';
                              echo '</li>';
                           }

                           ?>
                        </ul>
                     </div>
                  </div>
               </div>
               <div class="box-body">
                  <div class="form-group" style="padding-top:30px">
                     <a href="../php/createCSV.php?cl=<?php echo $service_id; ?>&range=<?php echo $range; ?>&bId=<?php echo $batch_id; ?>&query=<?php echo $_GET['query']; ?>&cli=<?php echo urlencode($eCli); ?>&camp=<?php echo urlencode($eCamp); ?>&code=<?php echo urlencode($eCode); ?>&ref=<?php echo urlencode($eRef); ?>&searchTerm=<?php echo $searchT; ?>&total=<?php echo $units; ?>" class="btn btn-block btn-social btn-dropbox" style="background-color:<?php echo $accRGB; ?>; border: 2px solid; border-radius: 6px !important; float:left; width: 134px; margin-top:-21px;">
                        <i class="fa fa-save"></i>Save Report
                     </a>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </section>
</aside>

<!--  THE JAVASCRIPT IMPORT ALWAYS APPEARS BEFORE LOCAL JAVASCRIPT AT THE BOTTOM OF THE PAGE -->
<?php include("template_import_script.php"); //must import all scripts first  ?>
<!-- END JAVASCRIPT IMPORT -->

<script type="text/javascript">
   $(function ()
   {
      $('#example2').dataTable({
         /*"processing": true,
          "serverSide": true,*/
         //"ajax": "scripts/server_processing.php"
         "bPaginate": false,
         "bLengthChange": true,
         "bFilter": false,
         "bSort": false,
         "bInfo": false,
         "bAutoWidth": true,
         "iDisplayLength": 100
                 /*"aoColumns": [
                  null,
                  null,
                  null,
                  null,
                  null,
                  null
                  ],*/
      });
   });

   jQuery.extend(jQuery.fn.dataTableExt.oSort,
           {
              "title-numeric-pre": function (a) {
                 var x = a.match(/title="*(-?[0-9\.]+)/)[1];
                 return parseFloat(x);
              },
              "title-numeric-asc": function (a, b) {
                 return ((a < b) ? -1 : ((a > b) ? 1 : 0));
              },
              "title-numeric-desc": function (a, b) {
                 return ((a < b) ? 1 : ((a > b) ? -1 : 0));
              }
           });

   jQuery.extend(jQuery.fn.dataTableExt.oSort,
           {
              "formatted-num-pre": function (a) {
                 a = (a === "-" || a === "") ? 0 : a.replace(/[^\d\-\.]/g, "");
                 return parseFloat(a);
              },
              "formatted-num-asc": function (a, b) {
                 return a - b;
              },
              "formatted-num-desc": function (a, b) {
                 return b - a;
              }
           });
</script>

<script type="text/javascript">
   function backToTraffic()
   {
      history.back();
   }

   function searchText()
   {
      var serviceId = "<?php echo urlencode($_GET['service_id']); ?>";
      var bId = "<?php echo urlencode($_GET['bId']); ?>";
      var startDate = "<?php echo urlencode($start_date); ?>";
      var endDate = "<?php echo urlencode($end_date); ?>";
      var searchT = document.getElementById('search_filter').value;

      window.location = "../pages/batchlistmsg.php?service_id=" + serviceId + "&start_date=" + startDate + "&end_date=" + endDate + "&bId=" + bId + "&cli=" + "<?php echo urlencode($eCli); ?>" + "&camp=" + "<?php echo urlencode($eCamp); ?>" + "&code=" + "<?php echo urlencode($eCode); ?>" + "&ref=" + "<?php echo urlencode($eRef); ?>" + "&range1=" + repRange1 + "&searchTerm=" + searchT + "&sents=<?php echo $units; ?>" + "&from=1&controller=1&sort=<?php echo $sortUrl; ?>" + "&query=<?php echo $_GET['query']; ?>";
   }
</script>

<script type="text/javascript">
   $(window).load(function () {
      $(".loader").fadeOut("slow");
      $(".loaderIcon").fadeOut("slow");
   })
</script>
<!-- Template Footer -->
