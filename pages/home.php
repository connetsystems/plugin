<?php
/**
 * Include the header template which sets up the HTML
 *
 * Don't forget to include template_import_script before any Javascripting
 * Don't forget to include template footer.php at the bottom of the page as well
 */
$headerPageTitle = "Dashboard"; //set the page title for the template import
TemplateHelper::initialize();

$netTots = getDashNetworkTotals();

$serTots = getDashServiceTotals();

$barData = getDashBarChart();

/* foreach ($barData as $key => $value) 
  {
  echo '<pre>';
  print_r($value);
  echo '</pre>';
  } */
?>

<!-- Right side column. Contains the navbar and content of the page -->
<aside class="right-side">
   <!-- Content Header (Page header) -->
   <section class="content-header">
      <h1>
         Dashboard

      </h1>
      <!--ol class="breadcrumb">
          <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
          <li class="active">Blank page</li>
      </ol-->
   </section>

   <!-- Main content -->

   <!-- Small buttons at Top -->
   <section class="content">
      <div class="row">
         <div class="col-lg-3 col-xs-6">
            <!-- small box -->
            <div class="small-box bg-connetRed">
               <div class="inner">
                  <h3>
                     &nbsp;<!--1 500 000-->
                  </h3>
                  <p>
                     &nbsp;<!--Available SMS's-->
                  </p>
               </div>
               <div class="icon">
                  <i class="ion ion-bag"></i>
               </div>
               <a href="#" class="small-box-footer">
                   &nbsp;<!--More info <i class="fa fa-arrow-circle-right"></i>-->
               </a>
            </div>
         </div><!-- ./col -->
         <div class="col-lg-3 col-xs-6">
            <!-- small box -->
            <div class="small-box bg-connetGreen">
               <div class="inner">
                  <h3>
                     &nbsp;<!-- 734 021sup style="font-size: 20px">%</sup-->
                  </h3>
                  <p>
                     &nbsp;<!-- Delivered SMS's-->
                  </p>
               </div>
               <div class="icon">
                  <i class="ion ion-stats-bars"></i>
               </div>
               <a href="#" class="small-box-footer">
                  &nbsp;<!-- More info <i class="fa fa-arrow-circle-right"></i>-->
               </a>
            </div>
         </div><!-- ./col -->
         <div class="col-lg-3 col-xs-6">
            <!-- small box -->
            <div class="small-box bg-connetYellow">
               <div class="inner">
                  <h3>
                     &nbsp;<!-- 450 000 -->
                  </h3>
                  <p>
                     &nbsp;<!-- Client Numbers-->
                  </p>
               </div>
               <div class="icon">
                  <i class="ion ion-person-add"></i>
               </div>
               <a href="#" class="small-box-footer">
                   &nbsp;<!--More info <i class="fa fa-arrow-circle-right"></i>-->
               </a>
            </div>
         </div><!-- ./col -->
         <div class="col-lg-3 col-xs-6">
            <!-- small box -->
            <div class="small-box bg-connetBlue">
               <div class="inner">
                  <h3>
                     &nbsp;<!-- 65-->
                  </h3>
                  <p>
                     &nbsp;<!--Failed SMS's-->
                  </p>
               </div>
               <div class="icon">
                  <i class="ion ion-pie-graph"></i>
               </div>
               <a href="#" class="small-box-footer">
                  &nbsp;<!-- More info <i class="fa fa-arrow-circle-right"></i>-->
               </a>
            </div>
         </div><!-- ./col -->
      </div><!-- /.row -->
   </section><!-- /.content -->

   <!-- Graph view -->
   <section class="col-lg-12 connectedSortable">
      <div class="nav-tabs-custom">
         <ul class="nav nav-tabs pull-right">
            <!--li class="active"><a href="#revenue-chart" data-toggle="tab">Area</a></li>
            <li><a href="#sales-chart" data-toggle="tab">Donut</a></li-->
            <!--li class="pull-left header"><i class="fa fa-inbox"></i> Daily Deliveries</li-->
            <!-- Morris chart - Sales -->
            <!--div class="chart tab-pane active" id="revenue-chart" style="position: relative; height: 300px;"></div-->
            <!--div class="chart tab-pane" id="sales-chart" style="position: relative; height: 300px;"></div-->
         </ul>
         <div class="tab-content no-padding">
            <div class="box box-connet" style="padding-bottom:1px; border-top-color:<?php echo $accRGB; ?>;">
               <div class="box-header">
                  <h3 class="box-title">This Month / Daily Totals</h3>
               </div>
               <div class="box-body chart-responsive">
                  <div class="chart" id="bar-chart" style="height: 300px;"></div>
               </div><!-- /.box-body -->
            </div><!-- /.box -->
         </div>
      </div><!-- /.nav-tabs-custom -->
   </section><!-- /.Left col -->

   <!--section class="col-lg-12 connectedSortable">
       <div class="box box-solid bg-light-blue-gradient">
           <div class="box-header">

               <div class="pull-right box-tools">
                   <button class="btn btn-primary btn-sm daterange pull-right" data-toggle="tooltip" title="Date range"><i class="fa fa-calendar"></i></button>
                   <button class="btn btn-primary btn-sm pull-right" data-widget='collapse' data-toggle="tooltip" title="Collapse" style="margin-right: 5px;"><i class="fa fa-minus"></i></button>
               </div>
                   <i class="fa fa-map-marker"></i>
                   <h3 class="box-title">
                       Countries Targeted
                   </h3>
           </div>
           <div class="box-body">
               <div id="world-map" style="height: 250px;"></div>
           </div>
       </div>
   </section-->

   <!--Table-->
   <section class="content">
      <div class="row">
         <div class="col-xs-12">
            <div class="box box-connet" style="padding-bottom:1px; border-top-color:<?php echo $accRGB; ?>;">
               <div class="box-header">
                  <h3 class="box-title">Networks</h3>
               </div><!-- /.box-header -->
               <div class="box-body table-responsive">
                  <table id="example2" class="table table-bordered table-hover">
                     <thead>
                        <tr>
                           <th>Country</th>
                           <th>Network</th>
                           <th><center>Unit Cost</center></th>
                     <th><center>Total Cost</center></th>
                     </tr>
                     </thead>
                     <tbody>
                        <?php
                        foreach ($netTots as $key => $value) {
                           echo '<tr>';
                           echo '<td><img src="img/flags/' . $value['prefix_country_name'] . '.png">&nbsp;&nbsp;' . $value['prefix_country_name'] . '</td>';
                           echo '<td><img src="img/serviceproviders/' . $value['prefix_network_name'] . '.png">&nbsp;&nbsp;' . $value['prefix_network_name'] . ' (' . $value['mccmnc'] . ')' . '</td>';
                           echo '<td><center><span class="label label-primary" title="' . $value['units'] . '" >' . $value['units'] . '</span></center></td>';
                           echo '<td><center> R ' . round($value['price'], 2) . '</center></td>';
                           echo '</tr>';
                        }
                        ?>
                     </tbody>
                     <tfoot>
                        <tr>
                           <th>Country</th>
                           <th>Network</th>
                           <th><center>Unit Cost</center></th>
                     <th><center>Total Cost</center></th>
                     </tr>
                     </tfoot>
                  </table>
               </div><!-- /.box-body -->
            </div><!-- /.box -->
         </div>
      </div>
   </section><!-- /.content -->

   <!--Table-->
   <section class="content">
      <div class="row">
         <div class="col-xs-12">
            <div class="box box-connet" style="padding-bottom:1px; border-top-color:<?php echo $accRGB; ?>;">
               <div class="box-header">
                  <h3 class="box-title">Services</h3>
               </div><!-- /.box-header -->
               <div class="box-body table-responsive">
                  <table id="example2" class="table table-bordered table-hover">
                     <thead>
                        <tr>
                           <th>Service Name</th>
                           <th>Units Sent</th>
                           <th>Total Cost</th>
                        </tr>
                     </thead>
                     <tbody>
                        <?php
                        foreach ($serTots as $key => $value) {
                           echo '<tr>';
                           echo '<td>&nbsp;' . $value['service_name'] . '</td>';
                           echo '<td><center><span class="label label-primary" title="' . $value['units'] . '" >' . $value['units'] . '</span></center></td>';
                           echo '<td><center> R ' . round($value['price'], 2) . '</center></td>';
                           echo '</tr>';
                        }
                        ?>
                     </tbody>
                     <tfoot>
                        <tr>
                           <th>Service Name</th>
                           <th>Units Sent</th>
                           <th>Total Cost</th>
                        </tr>
                     </tfoot>
                  </table>
               </div><!-- /.box-body -->
            </div><!-- /.box -->
         </div>
      </div>
   </section><!-- /.content -->

</aside><!-- /.right-side -->

<!--  THE JAVASCRIPT IMPORT ALWAYS APPEARS BEFORE LOCAL JAVASCRIPT AT THE BOTTOM OF THE PAGE -->
<?php include("template_import_script.php"); //must import all scripts first  ?>
<!-- END JAVASCRIPT IMPORT -->


<script type="text/javascript">
   $(function () {
      "use strict";

      // AREA CHART
      /*var area = new Morris.Area({
       element: 'revenue-chart',
       resize: true,
       data: [
       {y: '2011 Q1', item1: 2666, item2: 2666},
       {y: '2011 Q2', item1: 2778, item2: 2294},
       {y: '2011 Q3', item1: 4912, item2: 1969},
       {y: '2011 Q4', item1: 3767, item2: 3597},
       {y: '2012 Q1', item1: 6810, item2: 1914},
       {y: '2012 Q2', item1: 5670, item2: 4293},
       {y: '2012 Q3', item1: 4820, item2: 3795},
       {y: '2012 Q4', item1: 15073, item2: 5967},
       {y: '2013 Q1', item1: 10687, item2: 4460},
       {y: '2013 Q2', item1: 8432, item2: 5713}
       ],
       xkey: 'y',
       ykeys: ['item1', 'item2'],
       labels: ['Item 1', 'Item 2'],
       lineColors: ['#a0d0e0', '#3c8dbc'],
       hideHover: 'auto'
       });
       
       // LINE CHART
       var line = new Morris.Line({
       element: 'line-chart',
       resize: true,
       data: [
       {y: '2011 Q1', item1: 2666},
       {y: '2011 Q2', item1: 2778},
       {y: '2011 Q3', item1: 4912},
       {y: '2011 Q4', item1: 3767},
       {y: '2012 Q1', item1: 6810},
       {y: '2012 Q2', item1: 5670},
       {y: '2012 Q3', item1: 4820},
       {y: '2012 Q4', item1: 15073},
       {y: '2013 Q1', item1: 10687},
       {y: '2013 Q2', item1: 8432}
       ],
       xkey: 'y',
       ykeys: ['item1'],
       labels: ['Item 1'],
       lineColors: ['#3c8dbc'],
       hideHover: 'auto'
       });
       
       //DONUT CHART
       var donut = new Morris.Donut({
       element: 'sales-chart',
       resize: true,
       colors: ["#3c8dbc", "#f56954", "#00a65a"],
       data: [
       {label: "Download Sales", value: 12},
       {label: "In-Store Sales", value: 30},
       {label: "Mail-Order Sales", value: 20}
       ],
       hideHover: 'auto'
       });*/
      //BAR CHART
      var bar = new Morris.Bar({
         element: 'bar-chart',
         resize: true,
         data: [
<?php
foreach ($barData as $key => $value) {
   /*
     [month] => February
     [day] => 17
     [units] => 2557
     [SPLIT_STR(cdr_record_reference,'|',2)] => 375
    */
   echo "{y: '" . $value['day'] . "', a: " . $value['units'] . "},";
   # code...
}
/* echo "{y: '2007', a: 75, b: 65},";
  echo "{y: '2008', a: 50, b: 40},";
  echo "{y: '2009', a: 75, b: 65},";
  echo "{y: '2010', a: 50, b: 40},";
  echo "{y: '2011', a: 75, b: 65},";
  echo "{y: '2012', a: 100, b: 90}"; */
?>
         ],
         barColors: ['#00a65a'],
         xkey: 'y',
         ykeys: ['a'],
         labels: ['Units Sent'],
         hideHover: 'auto'
      });
   });
</script>
<!--script src="js/AdminLTE/dashboard.js" type="text/javascript"></script-->
<!-- AdminLTE dashboard demo (This is only for demo purposes)>
<AdminLTE for demo purposes>
<script src="js/AdminLTE/demo.js" type="text/javascript"></script-->

<!--script src="js/plugins/datatables/jquery.dataTables.js" type="text/javascript"></script>
<script src="js/plugins/datatables/dataTables.bootstrap.js" type="text/javascript"></script-->

<script type="text/javascript">
   $(window).load(function () {
      $(".loader").fadeOut("slow");
      $(".loaderIcon").fadeOut("slow");
   })
</script>


<!-- script type="text/javascript">
    $(function() {
        alert("testing");
        $("#example1").dataTable();
        $('#example2').dataTable({
            "bPaginate": true,
            "bLengthChange": false,
            "bFilter": false,
            "bSort": true,
            "bInfo": true,
            "bAutoWidth": false
        });
    });
</script-->

<!-- Template Footer -->