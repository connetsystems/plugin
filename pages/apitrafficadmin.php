<?php
/**
 * Include the header template which sets up the HTML
 *
 * Don't forget to include template_import_script before any Javascripting
 * Don't forget to include template footer.php at the bottom of the page as well
 */
$headerPageTitle = "All SMS Traffic"; //set the page title for the template import
TemplateHelper::initialize();

if (isset($_SESSION['accountId'])) {
   $acId = $_SESSION['accountId'];
} else {
   $acId = null;
}

$clients = getServicesInAccount($acId);

if (isset($_GET['range'])) {
   $dateArr = explode(' - ', $_GET['range']);
   $startDate = $dateArr[0];
   $endDate = $dateArr[1];
} else {
   $startDate = mktime(0, 0, 0) * 1000;
   $endDate = (time() * 1000);
}

$startDateConv = date('Y-m-d', ($startDate / 1000));
$endDateConv = date('Y-m-d', ($endDate / 1000));

if (isset($_GET['client']) && $_GET['client'] != 'Show All') {
   $cl = $_GET['client'];
   $clp = strrpos($cl, ' ');
   $cl = trim(substr($cl, $clp, strlen($cl)));
   $dsp = 'inline';
   $dir = '../img/routes';
   $serProviders = scandir($dir);
} else {
   $_GET['client'] = "0 - Please select a client...";
   $cl = strstr($_GET['client'], ' ', true);
   $dsp = 'none';
}

/*
  echo '<br><br><br>stime='.$startDate.'=';
  echo '<br><br><br>stime='.$startDateConv.'=';
  echo '<br><br><br>etime='.$endDate.'=';
  echo '<br><br><br>etime='.$endDateConv.'=';
  echo '<br><br><br>client='.$_GET['client'].'=';
  echo '<br><br><br>client='.$cl.'=';

  Verpakt - Verpakt/HTTP123 - 298

  1426888740000
  20th = 1426888740000
  20th = 1426802400000
  20th = 1426888799999
  20th = 1426888799000

  Verpakt - Verpakt/HTTP123 - 298
 */

if ($_GET['client'] != "0 - Please select a client...") {
   $qryMo = '
                    {
                      "size": 0,
                      "aggs": {
                        "5": {
                          "date_histogram": {
                            "field": "timestamp",
                            "interval": "1d",
                            "pre_zone": "+02:00",
                            "pre_zone_adjust_large_interval": false,
                            "min_doc_count": 1,
                            "extended_bounds": {
                                "min": ' . $startDate . ',
                                "max": ' . $endDate . '
                            }
                          },
                          "aggs": {
                            "3": {
                              "terms": {
                                "field": "_type",
                                "size": 0,
                                "order": {
                                  "_count": "desc"
                                }
                              }
                            }
                          }
                        }
                      },
                      "query": {
                        "filtered": {
                          "query": {
                            "query_string": {
                              "query": "service_id:' . $cl . '",
                              "analyze_wildcard": true
                            }
                          },
                          "filter": {
                            "bool": {
                              "must": [
                                {
                                  "range": {
                                    "timestamp": {
                                      "gte": ' . $startDate . ',
                                      "lte": ' . $endDate . '
                                    }
                                  }
                                }
                              ],
                              "must_not": []
                            }
                          }
                        }
                      }
                    }';

   $allStats2 = runRawMOSMSElasticsearchQuery($qryMo);

   $qry = '{
                  "size": 0,
                  "aggs": {
                    "5": {
                      "date_histogram": {
                        "field": "timestamp",
                        "interval": "1d",
                        "pre_zone": "+02:00",
                        "pre_zone_adjust_large_interval": false,
                        "min_doc_count": 1,
                        "extended_bounds": {
                          "min": ' . $startDate . ',
                          "max": ' . $endDate . '
                        }
                      },
                      "aggs": {
                        "1": {
                          "sum": {
                            "script": "doc[\'billing_units\'].value",
                            "lang": "expression"
                          }
                        },
                        "dlr": {
                          "terms": {
                            "field": "dlr",
                            "size": 0,
                            "order": {
                              "dlr_units": "desc"
                            }
                          },
                          "aggs": {
                            "dlr_units": {
                              "sum": {
                                "script": "doc[\'billing_units\'].value",
                                "lang": "expression"
                              }
                            },
                            "rdnc": {
                              "terms": {
                                "field": "rdnc",
                                "size": 0,
                                "order": {
                                  "1": "desc"
                                }
                              },
                              "aggs": {
                                "1": {
                                  "sum": {
                                    "script": "doc[\'billing_units\'].value",
                                    "lang": "expression"
                                  }
                                }
                              }
                            }
                          }
                        }
                      }
                    }
                },
                      "query": {
                        "filtered": {
                          "query": {
                            "query_string": {
                              "query": "service_id:' . $cl . '",
                              "analyze_wildcard": true
                            }
                          },
                          "filter": {
                            "bool": {
                              "must": [
                                {
                                  "range": {
                                    "timestamp": {
                                      "gte": ' . $startDate . ',
                                      "lte": ' . $endDate . '
                                    }
                                  }
                                }
                              ],
                              "must_not": []
                            }
                          }
                        }
                      }
                    }
                ';

   $allStats = runRawMTSMSElasticsearchQuery($qry);
}

$sort = urlencode('"timestamp":"desc"');
$sents = 0;
?>

<aside class="right-side">
   <section class="content-header">
      <h1>
         All SMS Traffic
      </h1>
   </section>

   <section class="content">
      <div class="box box-connet" style="padding-top:1px; border-top-color:<?php echo $accRGB; ?>;">
         <form action="updateservice.php" method="get" id="clientList" class="sidebar-form" style="border:0px;">
            <div class="callout callout-info" style="margin-bottom:0px;">
               <h4>Instructions</h4>
               <p>This page will indicate all traffic for selected date range specified in calendar below.</p>
            </div>
            <div class="callout callout-warning">
               <h4>Note</h4>
               <p>In order to view SMS content you have to click on the corresponding value in the relevant column.</p>
               <div style="float:right; margin-top:-42px;margin-right: 10px;">
                  <i class="fa fa-exclamation-triangle" style="color:#d1ba8a; font-size:20pt;"></i>
               </div>
            </div>
            <div id="reportrange" class="select pull-left" style="cursor:pointer;">
               <i class="fa fa-calendar fa-lg"></i>
               <span style="font-size:15px"><?php echo $startDateConv . " - " . $endDateConv; ?></span><b class="caret"></b>
            </div>
            <br>
            <br>
            <div class="form-group" style="padding-bottom:12px;">
               <label>Client List</label>
               <select class="form-control" name="client" id="client" form="clientList" OnChange="reloadOnSelect(this.value);">
                  <?php
                  if ($cl == '0') {
                     echo '<option SELECTED>Please select a client...</option>';
                  }
                  foreach ($clients as $key => $value) {
                     $sId = $value['service_id'];
                     if ($cl == $sId) {
                        $accountName = $value['account_name'];
                        $serviceName = $value['service_name'];
                        echo "<option name='" . $sId . "' SELECTED>" . $accountName . " - " . $serviceName . " - " . $sId . "</option>";
                     } else {
                        echo "<option name='" . $sId . "'>" . $value['account_name'] . " - " . $value['service_name'] . " - " . $sId . "</option>";
                     }
                  }
                  ?>
               </select>
            </div>
         </form>
      </div>
   </section>

   <section class="content">
      <div class="box box-connet" style="padding-top:1px; border-top-color:<?php echo $accRGB; ?>;">
         <div class="box-body">
            <div class="box-body table-responsive no-padding">
               <table id="example2"  class="table table-hover">
                  <thead>
                     <tr>
                        <th style="width:100px;">Date</th>
                        <th>Description</th>
                        <th style="background-color:#737AA6"><center>Sent</center></th>
                  <th style="background-color:#737AA6"><center>Units</center></th>
                  <th style="background-color:#DEA52F"><center>Pending</center></th>
                  <th style="background-color:#DEA52F"><center>Pending&nbsp;%</center></th>
                  <th style="background-color:#8BA808"><center>Delivered</center></th>
                  <th style="background-color:#8BA808"><center>Delivered&nbsp;%</center></th>
                  <th style="background-color:#f56954"><center>Failed</center></th>
                  <th style="background-color:#f56954"><center>Failed&nbsp;%</center></th>
                  <th style="background-color:#B8252B"><center>Rejected</center></th>
                  <th style="background-color:#B8252B"><center>Rejected&nbsp;%</center></th>
                  <th style="background-color:#979696"><center>Excluded</center></th>
                  <th style="background-color:#979696"><center>Excluded&nbsp;%</center></th>
                  <th style="background-color:#92e7ed"><center>MO&nbsp;Total</center></th>
                  <th style="background-color:#92e7ed"><center>MO/Delivered&nbsp;%</center></th>
                  <!--th><center>View Msgs</center></th-->
                  </tr>
                  </thead>
                  <tbody>
                     <?php
                     if (isset($allStats)) {
                        $dlrArr680 = array();
                        $dlrArr681 = array();
                        $dlrArr682 = array();
                        $dlrArr4288 = array();
                        $dlrArrREJ = array();
                        $dlrPos680 = array();
                        $dlrPos681 = array();
                        $dlrPos682 = array();
                        $dlrPos4288 = array();
                        $dlrPosREJ = array();
                        foreach ($allStats as $key => $value) {
                           if ($key = 'aggregations' && isset($value['5']['buckets'])) {
                              if (count($value['5']['buckets']) > 0) {
                                 for ($z = 0; $z < count($value['5']['buckets']); $z++) {
                                    /* $moCount = 0;
                                      foreach ($moData as $keyZ => $valueZ)
                                      {
                                      if($valueZ['timestamp'] == strstr($value['5']['buckets'][$z]['key_as_string'], 'T', true))
                                      {
                                      $moCount += $valueZ['id'];
                                      }
                                      } */
                                    echo '<tr>';
                                    echo '<td>' . strstr($value['5']['buckets'][$z]['key_as_string'], 'T', true) . '</td>';
                                    echo '<td>All Traffic</td>';
                                    $sents = $value['5']['buckets'][$z]['1']['value'];
                                    $dc = $value['5']['buckets'][$z]['doc_count'];
                                    echo '<td style="background-color:#adb1cb; text-align:right;"; text-align:right;><a style="color:#000000;cursor:pointer;" onclick="viewMsg(`' . strstr($value['5']['buckets'][$z]['key_as_string'], 'T', true) . '`)">' . number_format($dc, 0, '.', ' ') . '</a></td>';
                                    echo '<td style="background-color:#adb1cb; text-align:right;"; text-align:right;><a style="color:#000000;">' . number_format($sents, 0, '.', ' ') . '</a></td>';
                                    //echo '<td style="background-color:#cfd6ed; text-align:right;"; text-align:right;>'.$sents.'</td>';

                                    $dlrArr680['5']['buckets'][$z]['dlr']['buckets'] = 0;
                                    $dlrArr681['5']['buckets'][$z]['dlr']['buckets'] = 0;
                                    $dlrArr682['5']['buckets'][$z]['dlr']['buckets'] = 0;
                                    $dlrArr4288['5']['buckets'][$z]['dlr']['buckets'] = 0;
                                    $dlrArrREJ['5']['buckets'][$z]['dlr']['buckets'] = 0;

                                    for ($i = 0; $i < count($value['5']['buckets'][$z]['dlr']['buckets']); $i++) {
                                       $dlrPos = (integer) $value['5']['buckets'][$z]['dlr']['buckets'][$i]['key'];

                                       if (($dlrPos & 32) == 32 && ($dlrPos & 1) == 0 && ($dlrPos & 2) == 0 && ($dlrPos & 16) == 0) {
                                          $countRdnc = count($value['5']['buckets'][$z]['dlr']['buckets'][$i]['rdnc']['buckets']);
                                          for ($j = 0; $j < $countRdnc; $j++) {
                                             //if($value['5']['buckets'][$z]['dlr']['buckets'][$i]['rdnc']['buckets'][$j]['key'] == 0)
                                             {
                                                if (!in_array($dlrPos, $dlrPos680)) {
                                                   array_push($dlrPos680, $dlrPos);
                                                }

                                                if ($dlrArr680['5']['buckets'][$z]['dlr']['buckets'] == 0) {
                                                   $dlrArr680['5']['buckets'][$z]['dlr']['buckets'] = $value['5']['buckets'][$z]['dlr']['buckets'][$i]['rdnc']['buckets'][$j]['1']['value'];
                                                   //$dlrArr680['5']['buckets'][$z]['dlr']['buckets'] = $value['5']['buckets'][$z]['dlr']['buckets'][$i]['dlr_units']['value'];
                                                } else {
                                                   $dlrArr680['5']['buckets'][$z]['dlr']['buckets'] += $value['5']['buckets'][$z]['dlr']['buckets'][$i]['rdnc']['buckets'][$j]['1']['value'];
                                                   //$dlrArr680['5']['buckets'][$z]['dlr']['buckets'] += $value['5']['buckets'][$z]['dlr']['buckets'][$i]['dlr_units']['value'];
                                                }
                                             }
                                          }
                                       }
                                       if (($dlrPos & 1) == 1) {
                                          if (!in_array($dlrPos, $dlrPos681)) {
                                             array_push($dlrPos681, $dlrPos);
                                          }
                                          if ($dlrArr681['5']['buckets'][$z]['dlr']['buckets'] == 0) {
                                             $dlrArr681['5']['buckets'][$z]['dlr']['buckets'] = $value['5']['buckets'][$z]['dlr']['buckets'][$i]['dlr_units']['value'];
                                          } else {
                                             $dlrArr681['5']['buckets'][$z]['dlr']['buckets'] += $value['5']['buckets'][$z]['dlr']['buckets'][$i]['dlr_units']['value'];
                                          }
                                       } elseif (($dlrPos & 2) == 2) {
                                          if (!in_array($dlrPos, $dlrPos682)) {
                                             array_push($dlrPos682, $dlrPos);
                                          }
                                          if ($dlrArr682['5']['buckets'][$z]['dlr']['buckets'] == 0) {
                                             $dlrArr682['5']['buckets'][$z]['dlr']['buckets'] = $value['5']['buckets'][$z]['dlr']['buckets'][$i]['dlr_units']['value'];
                                          } else {
                                             $dlrArr682['5']['buckets'][$z]['dlr']['buckets'] += $value['5']['buckets'][$z]['dlr']['buckets'][$i]['dlr_units']['value'];
                                          }
                                       }
                                       $countRdnc = count($value['5']['buckets'][$z]['dlr']['buckets'][$i]['rdnc']['buckets']);
                                       for ($j = 0; $j < $countRdnc; $j++) {
                                          if ($value['5']['buckets'][$z]['dlr']['buckets'][$i]['rdnc']['buckets'][$j]['key'] != 0) {
                                             if (!in_array($dlrPos, $dlrPos4288)) {
                                                array_push($dlrPos4288, $dlrPos);
                                             }
                                             if ($dlrArr4288['5']['buckets'][$z]['dlr']['buckets'] == 0) {
                                                $dlrArr4288['5']['buckets'][$z]['dlr']['buckets'] = $value['5']['buckets'][$z]['dlr']['buckets'][$i]['rdnc']['buckets'][$j]['1']['value'];
                                             } else {
                                                $dlrArr4288['5']['buckets'][$z]['dlr']['buckets'] += $value['5']['buckets'][$z]['dlr']['buckets'][$i]['rdnc']['buckets'][$j]['1']['value'];
                                             }
                                          }
                                       }
                                       if (($dlrPos & 16) == 16) {
                                          if (!in_array($dlrPos, $dlrPosREJ)) {
                                             array_push($dlrPosREJ, $dlrPos);
                                          }
                                          if ($dlrArrREJ['5']['buckets'][$z]['dlr']['buckets'] == 0) {
                                             $dlrArrREJ['5']['buckets'][$z]['dlr']['buckets'] = $value['5']['buckets'][$z]['dlr']['buckets'][$i]['dlr_units']['value'];
                                          } else {
                                             $dlrArrREJ['5']['buckets'][$z]['dlr']['buckets'] += $value['5']['buckets'][$z]['dlr']['buckets'][$i]['dlr_units']['value'];
                                          }
                                       }
                                    }

                                    $pendUrlAttach = '';
                                    for ($il = 0; $il < count($dlrPos680); $il++) {
                                       $pendUrlAttach .= (string) $dlrPos680[$il] . '.';
                                    }
                                    $pendUrlAttach = substr($pendUrlAttach, 0, -1);
                                    $pendUrlAttach = ("'" . $pendUrlAttach . "'");

                                    $deliUrlAttach = '';
                                    for ($il = 0; $il < count($dlrPos681); $il++) {
                                       $deliUrlAttach .= (string) $dlrPos681[$il] . '.';
                                    }
                                    $deliUrlAttach = substr($deliUrlAttach, 0, -1);
                                    $deliUrlAttach = ("'" . $deliUrlAttach . "'");

                                    $failUrlAttach = '';
                                    for ($il = 0; $il < count($dlrPos682); $il++) {
                                       $failUrlAttach .= (string) $dlrPos682[$il] . '.';
                                    }
                                    $failUrlAttach = substr($failUrlAttach, 0, -1);
                                    $failUrlAttach = ("'" . $failUrlAttach . "'");

                                    $rejeUrlAttach = '';
                                    for ($il = 0; $il < count($dlrPosREJ); $il++) {
                                       $rejeUrlAttach .= (string) $dlrPosREJ[$il] . '.';
                                    }
                                    $rejeUrlAttach = substr($rejeUrlAttach, 0, -1);
                                    $rejeUrlAttach = ("'" . $rejeUrlAttach . "'");

                                    $exclUrlAttach = '';
                                    for ($il = 0; $il < count($dlrPos4288); $il++) {
                                       $exclUrlAttach .= (string) $dlrPos4288[$il] . '.';
                                    }
                                    $exclUrlAttach = substr($exclUrlAttach, 0, -1);
                                    $exclUrlAttach = ("'" . $exclUrlAttach . "'");
                                    //echo 'echo = '.$pendUrlAttach;
                                    if ($dlrArr680['5']['buckets'][$z]['dlr']['buckets'] != 0) {
                                       echo '<td style="background-color:#e9cb8c; text-align:right;"><a style="color:#000000;cursor:pointer;" onclick="viewMsgVar(' . $pendUrlAttach . ',`' . strstr($value['5']['buckets'][$z]['key_as_string'], 'T', true) . '`,`Pending`)">' . number_format($dlrArr680['5']['buckets'][$z]['dlr']['buckets'], 0, '.', ' ') . '</a></td>';
                                    } else {
                                       echo '<td style="background-color:#e9cb8c; text-align:right;"><a style="color:#000000;">0</a></td>';
                                    }

                                    echo '<td style="background-color:#e9cb8c; text-align:right;">' . round($dlrArr680['5']['buckets'][$z]['dlr']['buckets'] / $sents * 100, 2) . ' %</td>';

                                    if ($dlrArr681['5']['buckets'][$z]['dlr']['buckets'] != 0) {
                                       echo '<td style="background-color:#c0d26e; text-align:right;"><a style="color:#000000;cursor:pointer;" onclick="viewMsgVar(' . $deliUrlAttach . ',`' . strstr($value['5']['buckets'][$z]['key_as_string'], 'T', true) . '`,`Delivered`)">' . number_format($dlrArr681['5']['buckets'][$z]['dlr']['buckets'], 0, '.', ' ') . '</a></td>';
                                    } else {
                                       echo '<td style="background-color:#c0d26e; text-align:right;"><a style="color:#000000;">0</a></td>';
                                    }

                                    echo '<td style="background-color:#c0d26e; text-align:right;">' . round($dlrArr681['5']['buckets'][$z]['dlr']['buckets'] / $sents * 100, 2) . ' %</td>';

                                    if ($dlrArr682['5']['buckets'][$z]['dlr']['buckets'] != 0) {
                                       echo '<td style="background-color:#f8b6ac; text-align:right;"><a style="color:#000000;cursor:pointer;" onclick="viewMsgVar(' . $failUrlAttach . ',`' . strstr($value['5']['buckets'][$z]['key_as_string'], 'T', true) . '`,`Failed`)">' . number_format($dlrArr682['5']['buckets'][$z]['dlr']['buckets'], 0, '.', ' ') . '</a></td>';
                                    } else {
                                       echo '<td style="background-color:#f8b6ac; text-align:right;"><a style="color:#000000;">0</a></td>';
                                    }

                                    echo '<td style="background-color:#f8b6ac; text-align:right;">' . round($dlrArr682['5']['buckets'][$z]['dlr']['buckets'] / $sents * 100, 2) . ' %</td>';

                                    if ($dlrArrREJ['5']['buckets'][$z]['dlr']['buckets'] != 0) {
                                       echo '<td style="background-color:#df8c8f; text-align:right;"><a style="color:#000000;cursor:pointer;" onclick="viewMsgVar(' . $rejeUrlAttach . ',`' . strstr($value['5']['buckets'][$z]['key_as_string'], 'T', true) . '`,`Rejected`)">' . number_format($dlrArrREJ['5']['buckets'][$z]['dlr']['buckets'], 0, '.', ' ') . '</a></td>';
                                    } else {
                                       echo '<td style="background-color:#df8c8f; text-align:right;"><a style="color:#000000;">0</a></td>';
                                    }

                                    echo '<td style="background-color:#df8c8f; text-align:right;">' . round($dlrArrREJ['5']['buckets'][$z]['dlr']['buckets'] / $sents * 100, 2) . '</td>';

                                    echo '<td style="background-color:#b1b1b1; text-align:right;">' . number_format($dlrArr4288['5']['buckets'][$z]['dlr']['buckets'], 0, '.', ' ') . '</td>';
                                    echo '<td style="background-color:#b1b1b1; text-align:right;">' . round($dlrArr4288['5']['buckets'][$z]['dlr']['buckets'] / $sents * 100, 2) . ' %</td>';

                                    $cnn = 0;
                                    foreach ($allStats2 as $key7 => $value7) {
                                       if ($key7 = 'aggregations' && isset($value7['5']['buckets'])) {
                                          if (count($value7['5']['buckets']) > 0) {
                                             for ($y = 0; $y < count($value7['5']['buckets']); $y++) {
                                                if ($value7['5']['buckets'][$y]['key_as_string'] == $value['5']['buckets'][$z]['key_as_string']) {
                                                   $cnn++;
                                                   //echo '<td>'.$value7['5']['buckets'][$y]['3']['buckets']['0']['doc_count'].'</td>';
                                                   echo '<td style="background-color:#cfebed; text-align:right;"><a style="color:#333333;cursor:pointer;" onclick="gotoMo(`' . urlencode(strstr($value['5']['buckets'][$z]['key_as_string'], 'T', true)) . '`, ' . $cl . ', ' . $value7['5']['buckets'][$y]['3']['buckets']['0']['doc_count'] . ');">' . number_format($value7['5']['buckets'][$y]['3']['buckets']['0']['doc_count'], 0, '.', ' ') . '</a></td>';
                                                   if ($dlrArr681['5']['buckets'][$z]['dlr']['buckets'] != 0) {
                                                      echo '<td style="background-color:#cfebed; text-align:right;">' . round($value7['5']['buckets'][$y]['3']['buckets']['0']['doc_count'] / $dlrArr681['5']['buckets'][$z]['dlr']['buckets'] * 100, 2) . ' %</td>';
                                                   } else {
                                                      echo '<td style="background-color:#cfebed; text-align:right;">-</td>';
                                                   }
                                                }
                                             }
                                          }
                                       }
                                    }
                                    if ($cnn == 0) {
                                       echo '<td style="background-color:#cfebed; text-align:right;"><a style="color:#333333;">0</a></td>';
                                       echo '<td style="background-color:#cfebed; text-align:right;"><a style="color:#333333;">0</a></td>';
                                    }
                                    echo '</tr>';
                                 }
                              }
                           }
                        }
                     }
                     ?>
                  </tbody>
               </table>
            </div>
         </div>
         <div class="box-body" style="margin-top:10px;">
            <div class="form-group">
               <a onclick="saveRep();" class="btn btn-block btn-social btn-dropbox " style="background-color:<?php echo $accRGB; ?>; border: 2px solid; border-radius: 6px !important; float:left; width: 134px; margin-top:-21px;">
                  <i class="fa fa-save"></i>Save Report
               </a>
            </div>
         </div>
      </div>
   </section>
</aside>


<!--  THE JAVASCRIPT IMPORT ALWAYS APPEARS BEFORE LOCAL JAVASCRIPT AT THE BOTTOM OF THE PAGE -->
<?php include("template_import_script.php"); //must import all scripts first ?>
<!-- END JAVASCRIPT IMPORT -->

<script type="text/javascript">
   $('#reportrange').daterangepicker(
           {
              //singleDatePicker:true,
              ranges: {
                 'Today': [moment(), moment()],
                 'Yesterday': [moment().subtract('days', 1), moment().subtract('days', 1)],
                 'Last 7 Days': [moment().subtract('days', 6), moment()],
                 'Last 30 Days': [moment().subtract('days', 29), moment()],
                 'This Month': [moment().startOf('month'), moment().endOf('month')],
                 'Last Month': [moment().subtract('month', 1).startOf('month'), moment().subtract('month', 1).endOf('month')]
              },
              /*var mmt = moment();
               
               var mmtMidnight = mmt.startOf('day');*/
              startDate: moment(<?php echo $startDate; ?>),
              endDate: moment(<?php echo $endDate; ?>)
                      //endDate: moment().subtract(1, 'days'),
                      //opens: 'left',
           },
           function (start, end) {

              //$('#reportrange span').html(start.format('YYYY-MM-DD'));

              $('#reportrange span').html(start + ' - ' + end);
              $(".loader").fadeIn("slow");
              $(".loaderIcon").fadeIn("slow");

              var serviceName = document.getElementById("client").value;
              var repRange1 = $("#reportrange span").html();

              //alert(repRange1);
              /*var start = moment(repRange1).unix()*1000;
               var end = moment(repRange1).add(1, 'day');//
               end = (end.unix()-1)*1000;*/
              var repRange = start + ' - ' + end;
              encodeURI(serviceName);
              window.location = "../pages/apitrafficadmin.php?client=" + encodeURIComponent(serviceName) + "&range=" + repRange;
           }
   );

   //$('#reportrange').daterangepicker('showCalendars');
</script>

<script type="text/javascript">
   function saveRep()
   {
      var csvContent = "data:text/csv;charset=utf-8,";

      var data = [["Date", "Description", "Sent", "Units", "Pending", "Pending%", "Delivered", "Delivered%", "Failed", "Failed%", "Rejected", "Rejected%", "Excluded", "Excluded%", "Replies", "Replies/Delivered"],
<?php
if (isset($allStats)) {
   $dlrArr680 = array();
   $dlrArr681 = array();
   $dlrArr682 = array();
   $dlrArr4288 = array();
   $dlrArrREJ = array();
   $dlrPos680 = array();
   $dlrPos681 = array();
   $dlrPos682 = array();
   $dlrPos4288 = array();
   $dlrPosREJ = array();
   foreach ($allStats as $key => $value) {
      if ($key = 'aggregations' && isset($value['5']['buckets'])) {
         if (count($value['5']['buckets']) > 0) {
            for ($z = 0; $z < count($value['5']['buckets']); $z++) {
               echo '[';
               echo '"' . strstr($value['5']['buckets'][$z]['key_as_string'], 'T', true) . '",';
               echo '"All Traffic",';
               $sents = $value['5']['buckets'][$z]['1']['value'];
               $dc = $value['5']['buckets'][$z]['doc_count'];
               echo '"' . number_format($dc, 0, '.', ' ') . '",';
               echo '"' . number_format($sents, 0, '.', ' ') . '",';
               //echo '<td style="background-color:#cfd6ed; text-align:right;"; text-align:right;>'.$sents.'</td>';

               $dlrArr680['5']['buckets'][$z]['dlr']['buckets'] = 0;
               $dlrArr681['5']['buckets'][$z]['dlr']['buckets'] = 0;
               $dlrArr682['5']['buckets'][$z]['dlr']['buckets'] = 0;
               $dlrArr4288['5']['buckets'][$z]['dlr']['buckets'] = 0;
               $dlrArrREJ['5']['buckets'][$z]['dlr']['buckets'] = 0;

               for ($i = 0; $i < count($value['5']['buckets'][$z]['dlr']['buckets']); $i++) {
                  $dlrPos = (integer) $value['5']['buckets'][$z]['dlr']['buckets'][$i]['key'];

                  if (($dlrPos & 32) == 32 && ($dlrPos & 1) == 0 && ($dlrPos & 2) == 0 && ($dlrPos & 16) == 0) {
                     $countRdnc = count($value['5']['buckets'][$z]['dlr']['buckets'][$i]['rdnc']['buckets']);
                     for ($j = 0; $j < $countRdnc; $j++) {
                        //if($value['5']['buckets'][$z]['dlr']['buckets'][$i]['rdnc']['buckets'][$j]['key'] == 0) 
                        {
                           if (!in_array($dlrPos, $dlrPos680)) {
                              array_push($dlrPos680, $dlrPos);
                           }

                           if ($dlrArr680['5']['buckets'][$z]['dlr']['buckets'] == 0) {
                              $dlrArr680['5']['buckets'][$z]['dlr']['buckets'] = $value['5']['buckets'][$z]['dlr']['buckets'][$i]['rdnc']['buckets'][$j]['1']['value'];
                              //$dlrArr680['5']['buckets'][$z]['dlr']['buckets'] = $value['5']['buckets'][$z]['dlr']['buckets'][$i]['dlr_units']['value'];
                           } else {
                              $dlrArr680['5']['buckets'][$z]['dlr']['buckets'] += $value['5']['buckets'][$z]['dlr']['buckets'][$i]['rdnc']['buckets'][$j]['1']['value'];
                              //$dlrArr680['5']['buckets'][$z]['dlr']['buckets'] += $value['5']['buckets'][$z]['dlr']['buckets'][$i]['dlr_units']['value'];
                           }
                        }
                     }
                  }
                  if (($dlrPos & 1) == 1) {
                     if (!in_array($dlrPos, $dlrPos681)) {
                        array_push($dlrPos681, $dlrPos);
                     }
                     if ($dlrArr681['5']['buckets'][$z]['dlr']['buckets'] == 0) {
                        $dlrArr681['5']['buckets'][$z]['dlr']['buckets'] = $value['5']['buckets'][$z]['dlr']['buckets'][$i]['dlr_units']['value'];
                     } else {
                        $dlrArr681['5']['buckets'][$z]['dlr']['buckets'] += $value['5']['buckets'][$z]['dlr']['buckets'][$i]['dlr_units']['value'];
                     }
                  } elseif (($dlrPos & 2) == 2) {
                     if (!in_array($dlrPos, $dlrPos682)) {
                        array_push($dlrPos682, $dlrPos);
                     }
                     if ($dlrArr682['5']['buckets'][$z]['dlr']['buckets'] == 0) {
                        $dlrArr682['5']['buckets'][$z]['dlr']['buckets'] = $value['5']['buckets'][$z]['dlr']['buckets'][$i]['dlr_units']['value'];
                     } else {
                        $dlrArr682['5']['buckets'][$z]['dlr']['buckets'] += $value['5']['buckets'][$z]['dlr']['buckets'][$i]['dlr_units']['value'];
                     }
                  }
                  $countRdnc = count($value['5']['buckets'][$z]['dlr']['buckets'][$i]['rdnc']['buckets']);
                  for ($j = 0; $j < $countRdnc; $j++) {
                     if ($value['5']['buckets'][$z]['dlr']['buckets'][$i]['rdnc']['buckets'][$j]['key'] != 0) {
                        if (!in_array($dlrPos, $dlrPos4288)) {
                           array_push($dlrPos4288, $dlrPos);
                        }
                        if ($dlrArr4288['5']['buckets'][$z]['dlr']['buckets'] == 0) {
                           $dlrArr4288['5']['buckets'][$z]['dlr']['buckets'] = $value['5']['buckets'][$z]['dlr']['buckets'][$i]['rdnc']['buckets'][$j]['1']['value'];
                        } else {
                           $dlrArr4288['5']['buckets'][$z]['dlr']['buckets'] += $value['5']['buckets'][$z]['dlr']['buckets'][$i]['rdnc']['buckets'][$j]['1']['value'];
                        }
                     }
                  }
                  if (($dlrPos & 16) == 16) {
                     if (!in_array($dlrPos, $dlrPosREJ)) {
                        array_push($dlrPosREJ, $dlrPos);
                     }
                     if ($dlrArrREJ['5']['buckets'][$z]['dlr']['buckets'] == 0) {
                        $dlrArrREJ['5']['buckets'][$z]['dlr']['buckets'] = $value['5']['buckets'][$z]['dlr']['buckets'][$i]['dlr_units']['value'];
                     } else {
                        $dlrArrREJ['5']['buckets'][$z]['dlr']['buckets'] += $value['5']['buckets'][$z]['dlr']['buckets'][$i]['dlr_units']['value'];
                     }
                  }
               }

               $pendUrlAttach = '';
               for ($il = 0; $il < count($dlrPos680); $il++) {
                  $pendUrlAttach .= (string) $dlrPos680[$il] . '.';
               }
               $pendUrlAttach = substr($pendUrlAttach, 0, -1);
               $pendUrlAttach = ("'" . $pendUrlAttach . "'");

               $deliUrlAttach = '';
               for ($il = 0; $il < count($dlrPos681); $il++) {
                  $deliUrlAttach .= (string) $dlrPos681[$il] . '.';
               }
               $deliUrlAttach = substr($deliUrlAttach, 0, -1);
               $deliUrlAttach = ("'" . $deliUrlAttach . "'");

               $failUrlAttach = '';
               for ($il = 0; $il < count($dlrPos682); $il++) {
                  $failUrlAttach .= (string) $dlrPos682[$il] . '.';
               }
               $failUrlAttach = substr($failUrlAttach, 0, -1);
               $failUrlAttach = ("'" . $failUrlAttach . "'");

               $rejeUrlAttach = '';
               for ($il = 0; $il < count($dlrPosREJ); $il++) {
                  $rejeUrlAttach .= (string) $dlrPosREJ[$il] . '.';
               }
               $rejeUrlAttach = substr($rejeUrlAttach, 0, -1);
               $rejeUrlAttach = ("'" . $rejeUrlAttach . "'");

               $exclUrlAttach = '';
               for ($il = 0; $il < count($dlrPos4288); $il++) {
                  $exclUrlAttach .= (string) $dlrPos4288[$il] . '.';
               }
               $exclUrlAttach = substr($exclUrlAttach, 0, -1);
               $exclUrlAttach = ("'" . $exclUrlAttach . "'");
               //echo 'echo = '.$pendUrlAttach;
               if ($dlrArr680['5']['buckets'][$z]['dlr']['buckets'] != 0) {
                  echo '"' . number_format($dlrArr680['5']['buckets'][$z]['dlr']['buckets'], 0, '.', ' ') . '",';
               } else {
                  echo '"0",';
               }

               echo '"' . round($dlrArr680['5']['buckets'][$z]['dlr']['buckets'] / $sents * 100, 2) . '",';

               if ($dlrArr681['5']['buckets'][$z]['dlr']['buckets'] != 0) {
                  echo '"' . number_format($dlrArr681['5']['buckets'][$z]['dlr']['buckets'], 0, '.', ' ') . '",';
               } else {
                  echo '"0",';
               }

               echo '"' . round($dlrArr681['5']['buckets'][$z]['dlr']['buckets'] / $sents * 100, 2) . '",';

               if ($dlrArr682['5']['buckets'][$z]['dlr']['buckets'] != 0) {
                  echo '"' . number_format($dlrArr682['5']['buckets'][$z]['dlr']['buckets'], 0, '.', ' ') . '",';
               } else {
                  echo '"0",';
               }

               echo '"' . round($dlrArr682['5']['buckets'][$z]['dlr']['buckets'] / $sents * 100, 2) . '",';

               if ($dlrArrREJ['5']['buckets'][$z]['dlr']['buckets'] != 0) {
                  echo '"' . number_format($dlrArrREJ['5']['buckets'][$z]['dlr']['buckets'], 0, '.', ' ') . '",';
               } else {
                  echo '"0",';
               }

               echo '"' . round($dlrArrREJ['5']['buckets'][$z]['dlr']['buckets'] / $sents * 100, 2) . '",';

               echo '"' . number_format($dlrArr4288['5']['buckets'][$z]['dlr']['buckets'], 0, '.', ' ') . '",';
               echo '"' . round($dlrArr4288['5']['buckets'][$z]['dlr']['buckets'] / $sents * 100, 2) . '",';

               $cnn = 0;
               foreach ($allStats2 as $key7 => $value7) {
                  if ($key7 = 'aggregations' && isset($value7['5']['buckets'])) {
                     if (count($value7['5']['buckets']) > 0) {
                        for ($y = 0; $y < count($value7['5']['buckets']); $y++) {
                           if ($value7['5']['buckets'][$y]['key_as_string'] == $value['5']['buckets'][$z]['key_as_string']) {
                              $cnn++;
                              //echo '<td>'.$value7['5']['buckets'][$y]['3']['buckets']['0']['doc_count'].'</td>';
                              echo '"' . number_format($value7['5']['buckets'][$y]['3']['buckets']['0']['doc_count'], 0, '.', ' ') . '",';

                              if ($dlrArr681['5']['buckets'][$z]['dlr']['buckets'] != 0) {
                                 echo '"' . round($value7['5']['buckets'][$y]['3']['buckets']['0']['doc_count'] / $dlrArr681['5']['buckets'][$z]['dlr']['buckets'] * 100, 2) . '"';
                              } else {
                                 echo '"-"';
                              }
                           }
                        }
                     }
                  }
               }
               if ($cnn == 0) {
                  echo '"0",';
                  echo '"0"';
               }
               echo '],';
            }
         }
      }
   }
}
?>
      ];
      data.forEach(function (infoArray, index)
      {
         dataString = infoArray.join(",");
         csvContent += index < data.length ? dataString + "\n" : dataString;
      });
      var encodedUri = encodeURI(csvContent);
      var link = document.createElement("a");
      link.setAttribute("href", encodedUri);
      link.setAttribute("download", "<?php echo 'APITrafficAdminSummary_' . $startDate . '-' . $endDate; ?>.csv");

      link.click();
   }

   function reloadOnSelect(name)
   {
      $(".loader").fadeIn("slow");
      $(".loaderIcon").fadeIn("slow");
      var serviceName = name;

      var repRange = $("#reportrange span").html();

      var start = moment(repRange.substr(0, 10)).unix() * 1000;
      //alert('start:' + start);
      //var end = moment(repRange.substr(13, 23)).unix()*1000;
      var end = moment(repRange.substr(13, 23)).add(1, 'day');//
      end = (end.unix() - 1) * 1000;
      repRange = start + ' - ' + end;
      encodeURI(serviceName);
      window.location = "../pages/apitrafficadmin.php?client=" + encodeURIComponent(serviceName) + "&range=" + repRange;
   }

   function viewMsg(dater)
   {
      $(".loader").fadeIn("slow");
      $(".loaderIcon").fadeIn("slow");
      var serviceName = document.getElementById("client").value;

      var repRange1 = $("#reportrange span").html();
      var repRange = dater;
      var start = moment(repRange).unix() * 1000;
      var end = moment(repRange).add(1, 'day');//
      end = (end.unix() - 1) * 1000;
      repRange = start + ' - ' + end;
      encodeURI(serviceName);
      window.location = "../pages/apitrafficmsgadmin.php?client=" + encodeURIComponent(serviceName) + "&range=" + repRange + "&range1=" + repRange1 + "&type=Sent&sents=" + <?php echo $sents; ?> + "&from=1&controller=1&sort=<?php echo $sort; ?>";
   }

   function viewMsgVar(val, dater, t)
   {
      $(".loader").fadeIn("slow");
      $(".loaderIcon").fadeIn("slow");
      var serviceName = document.getElementById("client").value;

      var repRange1 = $("#reportrange span").html();
      var repRange = dater;
      var start = moment(repRange).unix() * 1000;
      var end = moment(repRange).add(1, 'day');//
      end = (end.unix() - 1) * 1000;
      repRange = start + ' - ' + end;
      encodeURI(serviceName);
      window.location = "../pages/apitrafficmsgadmin.php?client=" + encodeURIComponent(serviceName) + "&range=" + repRange + "&range1=" + repRange1 + "&type=" + t + "&sents=" + <?php echo $sents; ?> + "&from=1&controller=1&sort=<?php echo $sort; ?>&query=" + val;
   }

   function gotoMo(dater, sId, total)
   {
      $(".loader").fadeIn("slow");
      $(".loaderIcon").fadeIn("slow");

      var repRange1 = $("#reportrange span").html();
      //var end = dater + (24*60*60) - 1;//
      //var repRange = dater;
      var prevPage = "apitrafficadmin";

      window.location = "../pages/apimomsgadmin.php?range=" + dater + "&range1=" + repRange1 + "&prevPage=" + prevPage + "&sId=" + sId + "&total=" + total;
   }
</script>

<script type="text/javascript">
   $(window).load(function () {
      $(".loader").fadeOut("slow");
      $(".loaderIcon").fadeOut("slow");
   })
</script>

<!-- Template Footer -->

