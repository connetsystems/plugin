<?php
/**
 * Include the header template which sets up the HTML
 *
 * Don't forget to include template_import_script before any Javascripting
 * Don't forget to include template footer.php at the bottom of the page as well
 */
//-------------------------------------------------------------
// Template
//-------------------------------------------------------------
TemplateHelper::setPageTitle('Manage Service');
TemplateHelper::initialize();

//-------------------------------------------------------------
// MANAGE SERVICE
//-------------------------------------------------------------
//get the service

if(isset($_GET['service_id']))
{
   $service_id = $_GET['service_id'];
}
else
{
   header('location: manage_service_select.php' );
   die();
}

//get the service object
$service_obj = new Service($service_id);

//determine the current balance and styling class for this service
if($service_obj->service_credit_available / 10000 <= 0)
{
   $service_credit_class = "text-warning";
}
elseif($service_obj->service_credit_available / 10000 < 50)
{
   $service_credit_class = "text-danger";
}
else
{
   $service_credit_class = "text-success";
}
$service_credit_display = number_format($service_obj->service_credit_available / 10000, 2, '.', ' ');


//determine hte styling class for the enabled/disabled display
if($service_obj->service_status == "ENABLED")
{
   $service_status_class = "btn-success";
}
else
{
   $service_status_class = "btn-danger";
}

//--------------------------------------
// FOR LOW BALANCE NOTIFICATIONS
//--------------------------------------
if(isset($service_obj->service_notification_threshold) && $service_obj->service_notification_threshold != "")
{
   $low_balance_notifications_status = "ENABLED";
   $low_balance_notifications_class = "btn-success";
}
else
{
   $low_balance_notifications_status = "DISABLED";
   $low_balance_notifications_class = "btn-danger";
}

//--------------------------------------
// FOR DLR AND MO URLS
//--------------------------------------

if (isset($service_obj->endpoint_dlr_address))
{
   $service_dlr_url = htmlentities($service_obj->endpoint_dlr_address);
}
else
{
   $service_dlr_url = 'Not set';
}

if (isset($service_obj->endpoint_mosms_address))
{
   $service_mo_url = htmlentities($service_obj->endpoint_mosms_address);
}
else
{
   $service_mo_url = 'Not set';
}

if(count($service_obj->users) == 0)
{
   $no_users = true;
}

if(count($service_obj->routing_data) == 0)
{
   $no_routes = true;
}

//if a service ID is set, then the back button to the particular service the user was working on will show
if(isset($_GET['user_id']))
{
   $user_id = $_GET['user_id'];
   $show_user_back_button = true;
   $back_user = new User($user_id);
}
else
{
   $show_user_back_button = false;
}

//if a service ID is set, then the back button to the particular service the user was working on will show
if(isset($_GET['account_id']))
{
   $account_id = $_GET['account_id'];
   $show_account_back_button = true;
   $back_account = new Account($account_id);
}
else
{
   $show_account_back_button = false;
}

?>

<aside class="right-side">
   <section class="content-header">
      <h1>
         Manage Service
      </h1>
   </section>

   <section class="content">

      <?php
         //include the top nav
         $nav_section = "services";
         include('modules/module_admin_nav.php');
      ?>

      <!-- BACK BUTTON AND SERVICE TITLE -->
      <div class="row">
         <div class="col-lg-2">
            <?php if($show_user_back_button) { ?>
               <!-- THE BACK TO USER BUTTON -->
               <a class="btn btn-default btn-block btn-sm" href="manage_user.php?user_id=<?php echo $user_id; ?>#tab_users" style="margin-top:10px; overflow:hidden;">
                  <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span> Back to User "<?php echo htmlentities($back_user->service_name); ?>"
               </a>
            <?php } ?>

            <?php if($show_account_back_button) { ?>
               <!-- THE BACK TO ACCOUNT BUTTON -->
               <a class="btn btn-default btn-block btn-sm" href="manage_account.php?account_id=<?php echo $account_id; ?>#tab_users" style="margin-top:10px; overflow:hidden;">
                  <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span> Back to Service "<?php echo htmlentities($back_account->account_name); ?>"
               </a>
            <?php } ?>
         </div>
         <div class="col-lg-10">

         </div>
      </div>
      <hr/>

      <!-- MAIN SERVICE CONTENT -->
      <?php if($no_routes) { ?>
         <div class="alert alert-danger alert-dismissible">
            <button class="close" aria-hidden="true" data-dismiss="alert" type="button">×</button>
            <h4>
               <i class="icon fa fa-ban"></i>
               No Routes!
            </h4>
            There are no routes assigned to this service. No sending can take place until routes are added.
         </div>
      <?php } ?>

      <?php if($no_users) { ?>
         <div class="alert alert-danger alert-dismissible">
            <button class="close" aria-hidden="true" data-dismiss="alert" type="button">×</button>
            <h4>
               <i class="icon fa fa-ban"></i>
               No Users!
            </h4>
            There are no users attached to this service. The client cannot log in until a particular user is added or created.
         </div>
      <?php } ?>
      <div class="row">


         <!-- SERVICE MAIN DETAILS ACCOUNT ECT -->
         <div class="col-lg-2" style="text-align:right;">
            <img src="<?php echo $service_obj->header_logo; ?>" class="img-responsive thumbnail" alt="Service Logo"/>
         </div>
         <div class="col-lg-8">
            <h1 style="margin:0px;">
               <strong class="connet-field-display"><?php echo $service_obj->service_name; ?></strong>
               <a href="#" class="btn-edit-service-property btn btn-xs btn-primary"
                  connet-id="<?php echo $service_obj->service_id; ?>"
                  connet-value="<?php echo htmlspecialchars($service_obj->service_name); ?>"
                  connet-ajax-task="rename_service"
                  connet-field="service_name"
                  connet-title="Service Name">Rename</a>
            </h1>
         </div>
         <div class="col-lg-2">
            <div class="input-group-btn" style="text-align:center;">
               <button type="button" class="btn <?php echo $service_status_class; ?> dropdown-toggle" id="btnStatus" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                  <span id="btnStatusString">SERVICE <?php echo $service_obj->service_status; ?></span> <span class="caret"></span>
               </button>
               <ul class="dropdown-menu dropdown-menu-right">
                  <li><a class="btn-select-status" connet-service-status="ENABLED" href="#">Enable Service</a></li>
                  <li><a class="btn-select-status" connet-service-status="DISABLED" href="#">Disable Service</a></li>
               </ul>
            </div>
         </div>
      </div>
      <div class="row">
         <!-- SERVICE MAIN DETAILS ACCOUNT ECT -->
         <div class="col-lg-2">
            <div class="box box-solid">
               <div class="box-body" style="text-align:right;">
                  <div class="input-group-btn" style="text-align:center;">
                     <button type="button" class="btn btn-block btn-primary dropdown-toggle" id="btnPaymentType" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        <span id="btnPaymentTypeString"><?php echo $service_obj->service_credit_billing_type; ?></span> <span class="caret"></span>
                     </button>
                     <ul class="dropdown-menu dropdown-menu-right">
                        <li><a class="btn-select-payment-type" connet-payment-type="POSTPAID" href="#">POSTPAID</a></li>
                        <li><a class="btn-select-payment-type" connet-payment-type="PREPAID" href="#">PREPAID</a></li>
                     </ul>
                  </div>
                  <hr/>
                  <h4 style="margin-top:0px;">
                     <small>Parent Account:</small><br/>
                     <strong><a href="manage_account.php?account_id=<?php echo $service_obj->account_id; ?>"><?php echo htmlspecialchars($service_obj->account_name); ?></a></strong>
                  </h4>
                  <h4>
                     <small>Current Balance:</small><br/>
                     <strong class="<?php echo $service_credit_class; ?>"><?php echo $service_credit_display; ?></strong>
                     <br/><small><a href="#" id="btnShortcutAddCredits">[update credits]</a></small>
                  </h4>
                  <h4>
                     <small>Service Manager:</small><br/>
                     <strong><?php echo $service_obj->service_manager; ?></strong>
                  </h4>
                  <h4>
                     <small>Created:</small><br/>
                     <strong><?php echo date('d M Y', strtotime($service_obj->service_created)); ?></strong>
                  </h4>
               </div>
            </div>
         </div>

         <!-- THE TABS THAT HOLD THE CONFIG SECTIONS -->
         <div class="col-lg-10">
            <div class="nav-tabs-custom">
               <!-- Nav tabs -->
               <ul class="nav nav-tabs" id="service_tabs">
                  <li role="presentation">
                     <a href="#tab_overview" aria-controls="tab_overview" role="tab" data-toggle="tab">
                        <i class="fa fa-eye"></i> Service Overview
                     </a>
                  </li>
                  <li role="presentation"><a href="#tab_general" aria-controls="tab_general" role="tab" data-toggle="tab"><i class="fa fa-cogs"></i> General Config</a></li>
                  <li role="presentation">
                     <a href="#tab_routing" aria-controls="tab_routing" role="tab" data-toggle="tab">
                        <?php if($no_routes) { ?>
                           <span class="text-danger"><i class="text-danger fa fa-warning"></i> Routing </span>
                        <?php } else { ?>
                           <i class="fa fa-map-signs"></i> Routing
                        <?php } ?>
                     </a>
                  </li>
                  <li role="presentation">
                     <a href="#tab_users" aria-controls="tab_users" role="tab" data-toggle="tab">
                        <?php if($no_users) { ?>
                           <span class="text-danger"><i class="text-danger fa fa-warning"></i> Users </span>
                        <?php } else { ?>
                           <i class="fa fa-users"></i> Users
                        <?php } ?>
                     </a>
                  </li>
                  <li role="presentation"><a href="#tab_payments" aria-controls="tab_payments" role="tab" data-toggle="tab"><i class="fa fa-money"></i> Payments</a></li>
               </ul>

               <!-- Tab panes -->
               <div class="tab-content">
                  <!-- THE OVERVIEW TAB -->
                  <div role="tabpanel" class="tab-pane active" id="tab_overview">

                  </div>

                  <!-- THE GENERAL SETTINGS TAB -->
                  <div role="tabpanel" class="tab-pane active" id="tab_general">

                  </div>


                  <!-- THE ROUTING TAB -->
                  <div role="tabpanel" class="tab-pane" id="tab_routing">

                  </div>


                  <!-- THE USERS TAB -->
                  <div role="tabpanel" class="tab-pane" id="tab_users">

                  </div>


                  <!-- THE PAYMENTS TAB -->
                  <div role="tabpanel" class="tab-pane" id="tab_payments">

                  </div>
               </div>
            </div>
         </div>
   </section>

   <!-- MODALS HERE -->
   <div id="modalEditField" class="modal fade" tabindex="-1" role="dialog" data-backdrop="static">
      <div class="modal-dialog">
         <div class="modal-content">
            <div class="modal-header" >
               <button type="button" class="close" data-dismiss="modal" aria-label="Close" id="btnModalCloseTop"><span aria-hidden="true">&times;</span></button>
               <h4 class="modal-title" id="modalTitle">Add A New Contact Group</h4>
            </div>
            <div class="modal-body">
               <div class="alert alert-danger" style="display:none;" id="modAlert">
                  <h4><i class="icon fa fa-ban"></i> Error!</h4>
                  <p id="modAlertText">Unfortunately there was an error, please try again.</p>
               </div>
               <div class="form-group">
                  <label for="modalInputValue" id="modalInputLabel">Label</label>
                  <input type="text" class="form-control" id="modalInputValue" placeholder="" value="" />
               </div>
               <small>Please edit the value and click save to commit, or close to cancel.</small>
            </div>
            <div class="modal-footer">
               <button type="button" class="btn btn-default" data-dismiss="modal" id="btnModalCloseBottom">Close</button>
               <button type="button" class="btn btn-primary" id="btnModalSave" style="background-color:<?php echo $_SESSION['accountColour']; ?>;border: 2px solid; border-radius: 6px !important;">Save</button>
               <input type="hidden" class="form-control" id="modalInputField" value=""/>
               <input type="hidden" class="form-control" id="modalInputId" value=""/>
               <input type="hidden" class="form-control" id="modalInputTask" value=""/>
            </div>
         </div><!-- /.modal-content -->
      </div><!-- /.modal-dialog -->
   </div><!-- /.modal -->
</aside>


<!--  THE JAVASCRIPT IMPORT ALWAYS APPEARS BEFORE LOCAL JAVASCRIPT AT THE BOTTOM OF THE PAGE -->
<?php include("template_import_script.php"); //must import all scripts first   ?>
<!-- END JAVASCRIPT IMPORT -->

<script type="text/javascript">
   var serviceId = <?php echo (isset($service_id) ? $service_id : '-1') ?>;
   var activeTab = "";


   $(document).ready(function (e)
   {
      //on load of the page: switch to the currently selected tab
      var hash = window.location.hash;
      if(hash == "") {
         hash = "#tab_overview";
      }

      console.log(hash);

      $('#service_tabs a').click(function(e) {
         e.preventDefault();
         $(this).tab('show');
         $(window).scrollTop(0);
      });

      //store the currently selected tab in the hash value
      $("ul.nav-tabs > li > a").on("shown.bs.tab", function(e) {
         var id = $(e.target).attr("href").substr(1);
         window.location.hash = id;

         loadTabFragment(id);
      });

      //for the update credits shortcut
      $('#btnShortcutAddCredits').click(function(e) {
         e.preventDefault();
         showTab('#tab_payments');
      });

      //show the default loading tab for the page after init
      showTab(hash);


      /**
       * This button loads the generic field editor in the modal
       */
      $('.btn-edit-service-property').on("click", function (e)
      {
         e.preventDefault();

         var fieldName = $(this).attr('connet-field');
         var fieldTitle = $(this).attr('connet-title');
         var fieldId = $(this).attr('connet-id');
         var fieldTask = $(this).attr('connet-ajax-task');
         var fieldValue = $(this).attr('connet-value');

         $('#modalTitle').html("Edit Field: " + fieldTitle);
         $('#modalInputLabel').html(fieldTitle);
         $('#modalInputValue').val(fieldValue);
         $('#modalInputField').val(fieldName);
         $('#modalInputId').val(fieldId);
         $('#modalInputTask').val(fieldTask);

         $('#modalEditField').modal('show');
      });

      /**
       * This button catches the ENABLED/DISABLED status of a service.
       */
      $('.btn-select-status').on("click", function (e)
      {
         e.preventDefault();

         var thisObject = $(this);
         var status = $(this).attr('connet-service-status');
         var fieldName = "service_status";
         var confirmText = "";
         var newClass = "";
         var oldClass = "";
         if(status == "ENABLED")
         {
            confirmText = "Are you sure you want to ENABLE this service?";
            newClass = "btn-success";
            oldClass = "btn-danger";
         }
         else
         {
            confirmText = "Are you sure you want to DISABLE this service?";
            newClass = "btn-danger";
            oldClass = "btn-danger";
         }

         if(confirm(confirmText))
         {
            $('#btnStatusString').html('<span class="glyphicon glyphicon-refresh glyphicon-spin"></span> Saving...');

            $.post("../php/ajaxManageServiceHandler.php", {task: "save_service_field_value", serviceId: serviceId, fieldName: fieldName, fieldValue: status}).done(function (data) {
               var json = $.parseJSON(data);
               if (json.success)
               {
                  $('#btnStatusString').html("SERVICE " + status);
                  $('#btnStatus').removeClass(oldClass);
                  $('#btnStatus').addClass(newClass);
               }
               else
               {
                  alert("A server error occurred and we could not change the status of this service, please contact technical support.");
               }
            }).fail(function (data) {
               unlockModalBusy();
               showModalError("A server error occurred and we could not change the status of this service, please contact technical support.");
            });
         }
      });

      /**
       * This button catches the POSTPIAD/PREPAID payment type of a service.
       */
      $('.btn-select-payment-type').on("click", function (e)
      {
         e.preventDefault();

         var thisObject = $(this);
         var paymentType = $(this).attr('connet-payment-type');
         var fieldName = "service_credit_billing_type";
         var confirmText = "";
         var newClass = "";
         var oldClass = "";
         if(paymentType == "POSTPAID")
         {
            confirmText = "Are you sure you want to set this service to POSTPAID?";
         }
         else
         {
            confirmText = "Are you sure you want to set this service to PREPAID?";
         }

         if(confirm(confirmText))
         {
            $('#btnPaymentTypeString').html('<span class="glyphicon glyphicon-refresh glyphicon-spin"></span> Saving...');

            $.post("../php/ajaxManageServiceHandler.php", {task: "save_service_field_value", serviceId: serviceId, fieldName: fieldName, fieldValue: paymentType}).done(function (data) {
               var json = $.parseJSON(data);
               if (json.success)
               {
                  $('#btnPaymentTypeString').html(paymentType);
               }
               else
               {
                  alert("A server error occurred and we could not change the payment type of this service, please contact technical support.");
               }
            }).fail(function (data) {
               unlockModalBusy();
               showModalError("A server error occurred and we could not change the payment type of this service, please contact technical support.");
            });
         }
      });


      $('#btnModalSave').on("click", function (e)
      {
         e.preventDefault();

         lockModalBusy("Saving...");

         var fieldName = $('#modalInputField').val();
         var fieldValue = $('#modalInputValue').val();
         var fieldTask = $('#modalInputTask').val();
         var fieldId = $('#modalInputId').val();

         $.post("../php/ajaxManageServiceHandler.php", {task: fieldTask, serviceId: fragServiceId, fieldId: fieldId, fieldName:fieldName, fieldValue:fieldValue}).done(function (data) {
            var json = $.parseJSON(data);
            if (json.success)
            {
               unlockModalBusy();

               //ocate the html elements that hold this data and update them for display
               $("a[connet-id='"+fieldId+"'][connet-field='"+fieldName+"']" ).attr('connet-value', fieldValue);
               $("a[connet-id='"+fieldId+"'][connet-field='"+fieldName+"']" ).parent().find('.connet-field-display').html(fieldValue);
               $('#modalEditField').modal('hide');
            }
            else
            {
               unlockModalBusy();
               showModalError("A server error occurred and we could not save the data, please contact technical support.");
            }
         }).fail(function (data) {
            unlockModalBusy();
            showModalError("A server error occurred and we could not save the data, please contact technical support.");
         });
      });

   });

   //set the tab and start the tab load process according to the hash string passed to it
   function showTab(hashTab)
   {
      $('#service_tabs a[href="' + hashTab + '"]').tab('show');
      $(window).scrollTop(0);
   }

   //this locks the modal so the user can't do anything, useful when runing ajax calls off of it
   var prevButtonContent = "";
   function lockModalBusy(busyMessage)
   {
      prevButtonContent = $('#btnModalSave').html();
      $('#btnModalCloseTop').prop("disabled", true);
      $('#btnModalCloseBottom').prop("disabled", true);
      $('#btnModalSave').prop("disabled", false);
      $('#btnModalSave').html('<span class="glyphicon glyphicon-refresh glyphicon-spin"></span> ' + busyMessage);
   }

   //unlocks the modal so the user can close it
   function unlockModalBusy()
   {
      $('#btnModalCloseTop').prop("disabled", false);
      $('#btnModalCloseBottom').prop("disabled", false);
      $('#btnModalSave').prop("disabled", false);
      $('#btnModalSave').html(prevButtonContent);
      prevButtonContent = "";
   }

   //shows the error in the MODAL with a custom error message
   function showModalError(message)
   {
      $("#modAlertText").html(message);
      $("#modAlert").show();
   }

   //hides the modal error and resets the message
   function hideModalError()
   {
      $("#modAlertText").html();
      $("#modAlert").hide();
   }

   //cleans up all the child elements in a tab, for optimization
   function cleanupTabData(tabId)
   {
      if(tabId != "")
      {
         $(tabId).empty();
      }
   }

   function loadTabFragment(tabName)
   {
      cleanupTabData(activeTab);
      showTabLoading(tabName);
      activeTab = tabName;
      var getVariableString = window.location.search.substring(1);

      var scriptPath = "";
      switch (tabName) {
         case "tab_overview":
            scriptPath = "admin_services/frag_service_overview.php?"+getVariableString;
            break;
         case "tab_general":
            scriptPath = "admin_services/frag_service_general_config.php?"+getVariableString;
            break;
         case "tab_routing":
            scriptPath = "admin_services/frag_service_routing.php?"+getVariableString;
            break;
         case "tab_users":
            scriptPath = "admin_services/frag_service_users.php?"+getVariableString;
            break;
         case "tab_payments":
            scriptPath = "admin_services/frag_service_payments.php?"+getVariableString;
            break;
      }

      //load up the page tab we have selected
      $.post(scriptPath, {service_id:serviceId}).done(function (data) {
         $('#'+tabName).html(data);
      }).fail(function (data) {
         showTabLoadingError(divName, "A server error occurred and we could not load this list, please contact technical support.");
      });
   }

   function showTabLoading(elementId)
   {
      $('#'+elementId).html('<div style="width:100%; text-align:center; padding:20px; font-size:20px;"><span class="glyphicon glyphicon-refresh glyphicon-spin"></span><br/>Loading... Please wait.</div>');
   }

   function showTabLoadingError(elementId, message)
   {
      $('#'+elementId).html('<h5 class="text-warning">' + message + '</h5>');
   }

   function showLoading(elementId)
   {
      $('#'+elementId).html('<div style="width:100%; text-align:center; padding:20px;"><span class="glyphicon glyphicon-refresh glyphicon-spin"></span> Loading...</div>');
   }

   function showLoadingError(elementId, message)
   {
      $('#'+elementId).html('<h5 class="text-warning">' + message + '</h5>');
   }
</script>

<!-- Template Footer -->

