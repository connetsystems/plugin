<?php
//prevent user from accessing this page manually without going through the previous pages
if(!isset($_POST['serviceId']))
{
   header('Location: ../clienthome.php' );
   exit();
}

/**
 * Include the header tempalte which sets up the HTML
 *
 * Don't forget to include template_import_script before any Javascripting
 * Don't forget to include template footer.php at the bottom of the page as well
 */
$headerPageTitle = "Bulk SMS"; //set the page title for the template import
TemplateHelper::initialize();

//set_time_limit(360);
//    ini_set('memory_limit', '8192M');
set_time_limit(3000);


$singleView = 'block;';

if ($_SESSION['serviceId'] != '' && $_SESSION['resellerId'] != '') {
   if ($_SESSION['serviceId'] != $_SESSION['resellerId']) {
      $_POST['client'] = $_SESSION['accountName'] . ' - ' . $_SESSION['serviceName'] . " - " . $_SESSION['serviceId'];
      $singleView = 'none;';
   } elseif ($_SESSION['serviceId'] == $_SESSION['resellerId']) {
      $singleView = 'block;';
   }
}

if ($_POST['status'] != 'Scheduled') {
   $_POST['inputScheduleTime'] = date("Y-m-d H:i:s");
}

if (isset($_POST['client'])) {
   $cl = $_POST['client'];
   $clp = strrpos($cl, ' ');
   $cl = trim(substr($cl, $clp, strlen($cl)));
   $dsp = 'block;';

   if (isset($_POST['camId']) && $_POST['camId'] != "") {
      $dsp2 = 'block;';
      $temps = getTempData($_POST['camId']);
   } else {
      $dsp2 = 'none;';
   }
} else {
   $_POST['client'] = "0 - Please select a client...";
   $cl = strstr($_POST['client'], ' ', true);
   $dsp = 'none;';
   $dsp2 = 'none;';
}

if (isset($_SESSION['accountId'])) {
   $acId = $_SESSION['accountId'];
} else {
   $acId = null;
}

$clients = getServicesInAccount($acId);

$contact_helper = new helperContactLists();
$contact_lists = $contact_helper->getContactListClient($cl);

$noConts = 'block;';
if (count($contact_lists) == 0) {
   $noConts = 'none;';
}

////////////////////////////////////////////////

if (!isset($_POST['template'])) {
   $template = 'No template selected';
   $_POST['smsText'] = $_POST['smsText'];
} elseif (isset($_POST['template']) && $_POST['template'] == '') {
   $template = 'No template selected';
   $_POST['smsText'] = $_POST['smsText'];
} elseif (isset($_POST['template']) && $_POST['template'] != '') {
   $template = getTemplateById($_POST['template']);
   $_POST['smsText'] = $template['template_content'];
}

//strip invalid newlines and make them \n
$_POST['smsText'] = str_replace("\r\n", "\n", $_POST['smsText']);
$_POST['smsText'] = str_replace("\r", "\n", $_POST['smsText']);
//------------------------
$noManual = 'block;';
$infoMsg = 'none;';

if (strpos($_POST['smsText'], '{') !== false) {
   $noConts = 'none;';
   $noManual = 'none;';
   $infoMsg = 'block;';
}

////////////////////////////////////////////////
////////////////////////////////////////////////
?>

<aside class="right-side">
   <section class="content-header">
      <h1>
         Bulk SMS Continued
         <!--small>Control panel</small-->
      </h1>
   </section>

   <section class="content">
      <div class="box box-warning" style="border-top-color:<?php echo $accRGB; ?>;display:block">
         <div class="box-body" style="margin-bottom:0px;padding-bottom:1px;">
            <div class="callout callout-warning" id="summary" style="display:block;">
               <h4 class="box-title">
                  Summary
               </h4>
               <p>Below is a summary of the previous page.</p>
            </div>
            <div class="box-body">
               <p><b>Service:</b> <?php echo htmlentities($_POST['client']); ?></p>
               <p><b>Campaign:</b> <?php echo htmlentities($_POST['campaign']); ?></p>
               <p><b>Reference:</b> <?php echo htmlentities($_POST['refer']); ?></p>
               <p><b>Status:</b> <?php echo htmlentities($_POST['status']); ?></p>
               <?php
               if (isset($_POST['inputScheduleTime']) && $_POST['status'] == 'Scheduled') {
                  echo "<p><b>Schedule Time: </b>" . htmlentities($_POST['inputScheduleTime']) . "</p>";
               }
               ?>
               <p><b>Template:</b> <?php
                  if (isset($template['template_description']))
                  {
                     echo htmlentities($template['template_description']);
                  }
                  else
                  {
                     echo "No Template.";
                  }
                  ?></p>
               <p><b>SMS Text:</b> <?php echo htmlentities($_POST['smsText']); ?></p>
            </div>
            <div class="callout callout-warning" id="summary" style="display:<?php echo $infoMsg; ?> ">
               <h4 class="box-title">
                  Caution
               </h4>
               <p>The text of your SMS contains markup characters. If you uploaded a file with multiple columns, you can reference a column by entering a placeholder containing the column number.
                  <br>Example: Hello {2}, your account balance is R{3}.</p>
            </div>
            <form role="form" id="bulkData" form="batch" accept-charset="utf-8" enctype="multipart/form-data" action="confirmbulkfile.php" method="POST">
               <div class="callout callout-danger" id="errNoNums" style="margin-bottom:0px;display:none;">
                  <h4>You have not supplied any numbers!</h4>
                  <p>Please either upload a file, enter numbers manually or select a contact list.</p>
               </div>
               <div class="form-group" id="manualNumsForm" style='margin-top:0px'>
                  <div class="box box-warning" style="border-top-color:<?php echo $accRGB; ?>;display:block;" >
                     <div class="checkbox" style="padding-bottom:10px;">
                        <label style="padding-left:15px;">
                           <input type="checkbox" id="uF" name="upF">
                           Upload CSV File
                        </label>
                     </div>
                     <div class="box-body" id="upCSV" style="padding-bottom:0px;display:none;">
                        <div class="form-group">
                           <div class="box-body" style="padding-bottom:1px;padding-top:5px;">
                              <div class="form-group" style="margin-bottom:10px;">
                                 <label for="uploadCSV">File upload (You can select a single CSV file, see examples below) The uploaded file must be a comma-seperated (.csv) file.
                                    <br>Note: The first column MUST be the recipient cell number, and column names must not be included.</label>
                                 <br>
                                 <div style="padding-left:20px;padding-top:10px;">

                                    <div style="display:inline-block;">
                                       27841234567<br>
                                       27813214567<br>
                                       27837654321<br>
                                       ...
                                    </div>

                                    <div style="display:inline-block;padding-left:10px;padding-right:10px;padding-bottom:50px;vertical-align:middle;">or</div>

                                    <div style="display:inline-block;">
                                       27841234567,"Content for SMS 1 here"<br>
                                       27813214567,"Content for SMS 2 here"<br>
                                       27837654321,"Content for SMS 3 here"<br>
                                       ...
                                    </div>

                                    <div style="display:inline-block;padding-left:10px;padding-right:10px;padding-bottom:50px;vertical-align:middle;">or</div>

                                    <div style="display:inline-block;">
                                       27841234567,"John","Smith","..."<br>
                                       27813214567,"Jane","Doe","..."<br>
                                       27837654321,"Eve","Adams","..."<br>
                                       ...
                                    </div>
                                 </div>
                                 <div class="callout callout-danger" id="fileError" style="display:none;">
                                    <p>You must choose a file and/or a contact list!</p>
                                 </div>
                                 <input type="file" name="uploadCSV" id="uploadCSV" form="bulkData" onchange="getExt(this.value);">
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
               <div class="form-group" id="manualNumsForm" style='margin-top:0px'>
                  <div class="box box-warning" style="border-top-color:<?php echo $accRGB; ?>;display:<?php echo $noManual; ?>" >
                     <div class="callout callout-danger" id="invalidNums" style="margin-bottom:0px;display:none;">
                        <h4>You have supplied numbers which are incorrectly formatted!</h4>
                        <p>Remember to format all numbers with international code, i.e: 27821234567</p>
                     </div>
                     <div class="checkbox" style="padding-bottom:10px;">
                        <label style="padding-left:15px;">
                           <input type="checkbox" id="mN" name="moreN">
                           Add Manual Numbers
                        </label>
                     </div>
                     <div class="box-body" id="manualNums" style="padding-bottom:0px;display:none;">
                        <div class="form-group">
                           <label>Enter up to 10 recipients manually e.g. 27821234582,27831234567</label>
                           <textarea class="form-control" form="bulkData" rows="3" name="numbers" id="numbers" maxchars="120" placeholder=""></textarea>
                           <p class="help-block">Separate multiple cell numbers with comma-, characters.
                              Include country code. (27821234582,27831234567)</p>
                        </div>
                     </div>
                  </div>
               </div>
               <div class="form-group" id="manualNumsForm" style='margin-top:0px;display:<?php echo $noConts; ?>'>
                  <div class="box box-warning" style="border-top-color:<?php echo $accRGB; ?>;display:block;" >
                     <div class="checkbox" style="padding-bottom:10px;">
                        <label style="padding-left:15px;">
                           <input type="checkbox" id="mC" name="moreC">
                           Add Contact List
                        </label>
                     </div>
                     <div class="box-body" id="contactList" style="padding-bottom:0px;display:none;">
                        <div class="form-group">
                           <label>Choose a contact group from the drop down</label>
                           <div class="box-body" id="contactList" style="padding-bottom:1px;display:block;">
                              <div class="form-group">
                                 <select class="form-control" name="contactList" id="contactListId" form="bulkData" >
                                    <?php
                                    echo '<option value="0">Please Select</option>';
                                    echo '<option value="All" >All Contacts</option>';
                                    foreach ($contact_lists as $key => $value)
                                    {
                                       if($value['contact_list_status'] == "ENABLED" && (!isset($value['campaign_id']) || $value['campaign_id'] == $_POST['camId']))
                                       {
                                          echo "<option value=" . $value['contact_list_id'] . " >(" . $value['contact_list_code'] . ") - Group Name: " . $value['contact_list_name'] . " - Description: " . $value['contact_list_description'] . "</option>";
                                       }
                                    }
                                    ?>
                                 </select>
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
               <input type="hidden" name="client" id="client" value="<?php echo htmlentities($_POST['client']); ?>">
               <input type="hidden" name="serviceId" value="<?php echo $cl; ?>">
               <input type="hidden" name="camCode" value="<?php echo htmlentities($_POST['camCode']); ?>">
               <input type="hidden" name="camId" value="<?php echo htmlentities($_POST['camId']); ?>">
               <input type="hidden" name="campN" value="<?php echo htmlentities($_POST['campaign']); ?>">
               <input type="hidden" name="temp" value="<?php
               if (isset($template['template_description'])) {
                  echo $template['template_description'];
               } else {
                  echo "No Template.";
               }
               ?>">
               <input type="hidden" name="refer" value="<?php echo htmlentities($_POST['refer']); ?>">
               <input type="hidden" name="status" value="<?php echo htmlentities($_POST['status']); ?>">
               <input type="hidden" name="sTime" value="<?php echo htmlentities($_POST['inputScheduleTime']); ?>">
               <input type="hidden" name="smsText" value="<?php echo htmlentities($_POST['smsText']); ?>">
               <input type="hidden" name="smsCount" id="smsCount" value="<?php echo htmlentities($_POST['smsCount']); ?>">
            </form>
         </div>
         <div class="box-body" style="display:<?php echo $dsp2; ?>;padding-bottom:18px;margin-top:0px;">
            <div>
               <a onclick="backBtn();" class="btn btn-block btn-social btn-dropbox " style="background-color:<?php echo $accRGB; ?>; color: #ffffff; border: 2px solid; border-radius: 6px !important; float:left; width: 92px; margin-top:-13px;">
                  <i class="fa fa-arrow-left"></i>Back
               </a>
            </div>
            <div class="form-group">
               <a onclick="validateBulk();" class="btn btn-block btn-social btn-dropbox " style="background-color:<?php echo $accRGB; ?>; border: 2px solid; border-radius: 6px !important; float:left; width: 170px; margin-top:-13px;">
                  <i class="fa fa-save"></i>Confirm Bulk SMS
               </a>
            </div>
         </div>
      </div>
   </section>
</aside>

<?php include("template_import_script.php"); //must import all scripts first   ?>

<script type="text/javascript">
   function backBtn()
   {
      var client = "<?php echo $_POST['client']; ?>";
      window.location = "../pages/bulksms.php?client=" + client;
   }
</script>

<script type="text/javascript">
   $(function () {
      $("#numbers").change(function (e) {
         cleanUpNumbersField();
      });

      $("#numbers").keyup(function (e) {
         console.log("Checking validation...");
         var numbersString = $("#smsNumbers").val();
         numbersString = numbersString.replace(/[^0-9,]/g, '');
         $("#smsNumbers").val(numbersString);
      });

      function cleanUpNumbersField() {
         console.log("Checking validation...");
         var numbersString = $("#numbers").val();
         numbersString = numbersString.replace(/[^0-9,]/g, '');
         var numbersArr = numbersString.split(',');
         console.log("size: " + numbersArr.length);
         for (var i = 0; i < numbersArr.length; i++) {
            console.log("NUMBER: " + numbersArr[i]);
            if (numbersArr[i] == "") {
               console.log("Remove it... ");
               numbersArr.splice(i, 1);
            }
         }

         $("#numbers").val(numbersArr.join(','));
      }
   });

   function isNumberKey(evt)
   {
      var charCode = (evt.which) ? evt.which : evt.keyCode;

      if (charCode == 44)
         return true;

      /*alert(charCode);*/
      if (charCode == 46)
         return false;

      if (charCode != 46 && charCode > 31
              && (charCode < 48 || charCode > 57))
         return false;

      return true;
   }

   $('input').on('ifChecked', function (event)
   {
      var checkedId = $(this, 'input').attr('id');
      if (checkedId == 'mN')
      {
         document.getElementById('manualNums').style.display = 'block';
      }
      if (checkedId == 'uF')
      {
         document.getElementById('upCSV').style.display = 'block';
      }
      if (checkedId == 'mC')
      {
         document.getElementById('contactList').style.display = 'block';
      }
   });

   $('input').on('ifUnchecked', function (event)
   {
      var checkedId = $(this, 'input').attr('id');
      if (checkedId == 'uF')
      {
         document.getElementById('upCSV').style.display = 'none';
         document.getElementById('uploadCSV').value = '';
      }
      if (checkedId == 'mN')
      {
         document.getElementById('manualNums').style.display = 'none';
         document.getElementById('numbers').value = '';
      }
      if (checkedId == 'mC')
      {
         document.getElementById('contactList').style.display = 'none';
         document.getElementById('contactListId').selectedIndex = 0;
         ;
      }
   });

   function validateBulk()
   {
      $('#errNoNums').hide();

      var validSubmit = true;
      var csv = document.getElementById('uploadCSV').value;
      var num = document.getElementById('numbers').value;
      var con = document.getElementById('contactListId').value;

      if (csv == '' && con == '0' && num == '')
      {
         validSubmit = false;
         $('#errNoNums').show();
      }
      else if (num != '')
      {
         var checked = checkManualNums();
         if (checked == 0)
         {
            $('#invalidNums').hide();
         } else
         {
            $('#invalidNums').show();
            validSubmit = false;
         }
      }



      if (validSubmit > 0)
      {
         $(".loader").fadeIn("slow");
         $(".loaderIcon").fadeIn("slow");
         bulkData.submit();
      }
      /*else
       {
       alert('You must select at least one of the above options.');
       }*/
   }

   function checkManualNums()
   {
      var num = document.getElementById('numbers').value;
      var res = num.split(",");
      var err = 0;
      for (var i = 0; i < res.length; i++)
      {
         var ss = res[i].substring(0, 1);
         if (ss == '0')
         {
            err++;
         }
      }

      console.log("ss=" + err);
      return err;
   }

   function getExt(filename)
   {
      var ext = filename.substr(-3);
      ext = ext.toLowerCase();

      //console.log("ext=>"+ext+"<=");

      if (ext != 'csv')
      {
         document.getElementById('uploadCSV').value = '';
         alert("You can only upload a CSV file.");
      }

      /*if(ext == filename) return "";
       return ext;*/
   }
</script>

<script type="text/javascript">
   function reloadOnSelect(name)
   {
      $(".loader").fadeIn("slow");
      $(".loaderIcon").fadeIn("slow");
      window.location = "bulksms.php?client=" + name;
   }
</script>

<script type="text/javascript">
   $(window).load(function () {
      $(".loader").fadeOut("slow");
      $(".loaderIcon").fadeOut("slow");
   })
</script>
<!-- Template Footer -->
