<?php
/**
 * Include the header template which sets up the HTML
 *
 * Don't forget to include template_import_script before any Javascripting
 * Don't forget to include template footer.php at the bottom of the page as well
 */
$headerPageTitle = "Update Credits"; //set the page title for the template import
TemplateHelper::initialize();

$succ = 0;
$height = 215;

if (isset($_POST['serviceId']))
{
   echo  $_POST['serviceId'];
   $service_id = $_POST['serviceId'];
   $client_name = getServiceName($service_id);

   //get the current funds for this service
   $previous_amount = getFundsOnly($service_id);
   $previous_amount = $previous_amount / 10000;
   $previous_amount = round($previous_amount, 2);

   $amount = isset($_POST['ammount']) ? $_POST['ammount'] : "0";
   $description = isset($_POST['desc']) ? $_POST['desc'] : "";
   $after_amount = $previous_amount + $amount;

}
else if(!isset($_GET['success']))
{
   echo '<script>window.location = "updatecredits.php"</script>';
}

$cred = getCreditOnly($_SESSION['serviceId']);
$_SESSION['serviceCredit'] = $cred;
?>

<aside class="right-side">
   <section class="content-header">
      <h1>
         Confirm Credit Payment
         <!--small>Control panel</small-->
      </h1>
   </section>

   <section class="content">
      <div class="row col-lg-12">
         <div class="box box-connet" style="height:<?php echo $height; ?>px; margin-top:-15px;display:<?php echo $singleView; ?>; border-top-color:<?php echo $accRGB; ?>;">
            <form action="../php/updatecredits.php" method="post" id="conff" class="sidebar-form" style="border:0px;">
               <div class="form-group">
                  <?php
                  if (isset($_GET['success']) && $_GET['success'] == 1)
                  {
                     echo '<div class="callout callout-success">';
                        echo "You have successfully made a payment.";
                        echo '<div style="float:right; margin-top:-3px">';
                           echo '<i class="fa fa-check" style="color:#5cb157; font-size:20pt;"></i>';
                        echo '</div>';
                     echo '</div>';
                  }
                  elseif (isset($_GET['success']) && $_GET['success'] == 0)
                  {
                     echo '<div class="callout callout-danger">';
                        echo "There was a sever error. We could not udate the credit values. Please try again later.";
                        echo '<div style="float:right; margin-top:-3px">';
                           echo '<i class="fa fa-check" style="color:#5cb157; font-size:20pt;"></i>';
                        echo '</div>';
                     echo '</div>';
                  }
                  else
                  {
                     echo 'You are about to make a payment with the following details:';
                     echo '<div class="form-group" style="padding-left:24px;">';

                     echo '<br><b>Service:</b> ' . $client_name;
                     echo '<input type="hidden" name="serviceId" id="serviceId" value="' . $service_id . '">';


                     echo '<br><b>Previous Amount:</b> R ' . $previous_amount;

                     echo '<br><b>Amount To Pay:</b> R ' . $amount;
                     echo '<input type="hidden" name="ammount" id="ammount" value="' . $amount . '">';

                     echo '<br><b>Amount After Payment:</b> R ' . round($after_amount, 2);

                     echo '<br><b>Description:</b> ' . htmlspecialchars($description);
                     echo '<input type="hidden" name="desc" id="desc" value="' . htmlspecialchars($description) . '">';

                     echo '<input type="hidden" name="confir" id="confir" value="1">';

                     echo '</div>';
                  echo '</div>';
                  echo '<br>';
                  echo '<div class="form-group" style="padding-top:10px;">';
                     echo '<a class="btn btn-block btn-social btn-dropbox" onclick="goBack()" style="background-color:' . $accRGB . '; color: #ffffff; border: 2px solid; border-radius: 6px !important; float:left; width: 100px; height:36px; margin-top:-25px;padding-left:10px;">';
                     echo '<i class="fa fa-arrow-left"></i>Back';
                     echo '</a>';
                     $conff = "document.forms['conff'].submit()";
                     echo '<a class="btn btn-block btn-social btn-dropbox" onclick=' . $conff . ' style="background-color:' . $accRGB . '; color: #ffffff; border: 2px solid; border-radius: 6px !important; float:left; width: 165px; height:36px; margin-top:-25px;padding-left:10px;">';
                     echo '<i class="fa fa-credit-card"></i>Confirm Payment';
                     echo '</a>';
                  echo '</div>';
                  }
                  ?>
            </form>
         </div>
      </div>
   </section>

</aside>

<!--  THE JAVASCRIPT IMPORT ALWAYS APPEARS BEFORE LOCAL JAVASCRIPT AT THE BOTTOM OF THE PAGE -->
<?php include("template_import_script.php"); //must import all scripts first ?>
<!-- END JAVASCRIPT IMPORT -->

<script type="text/javascript">
   function goBack()
   {
      var serviceId = "<?php echo $service_id; ?>"
      var amount = "<?php echo $amount; ?>"
      var desc = "<?php echo urlencode($description); ?>"
      window.location = "../pages/updatecredits.php?service_id=" + serviceId + "&ammount=" + amount + "&desc=" + desc;
   }

   $(window).load(function () {
      $(".loader").fadeOut("slow");
      $(".loaderIcon").fadeOut("slow");
   })
</script>

<!-- Template Footer -->

