<?php
/**
 * Include the header template which sets up the HTML
 *
 * Don't forget to include template_import_script before any Javascripting
 * Don't forget to include template footer.php at the bottom of the page as well
 */
$headerPageTitle = "Routes / Rates"; //set the page title for the template import
TemplateHelper::initialize();


//Get the list of Countries to populate the Choose Country drop down
$countries = getListOfCountries();

if (isset($_GET['range'])) {
   $dateArr = explode(' - ', $_GET['range']);
   $startDate = $dateArr[0];
   $endDate = $dateArr[1];
} else {
   $startDate = date('Y-m-d 00:00:00');
   $endDate = date('Y-m-d 23:59:59');
}

if (isset($_GET['search']) && $_GET['search'] == "1") {

   if ($_GET['country'] == 'Show All') {
      $networks = getListOfNetworks();
   } else {
      $networks = getNetworkViaCountryName($_GET['country']);
   }

   if ($_GET['network'] == 'Show All') {
      $collections = getListOfCollections();
   } else {
      $collections = getCollectionsMCCMNC($_GET['network']);
   }
   //$billingUnits = getBillingUnits($startDate, $endDate,'');
} else {

   $networks = getListOfNetworks();
   $collections = getListOfCollections();
}


if (isset($_GET['search2']) && $_GET['search2'] == "1") {
   $allServices = getInternationalRoutesRates($_GET['country'], $_GET['network'], $_GET['collection'], $startDate, $endDate);
}



/* $sql = 'INSERT INTO `core_service_payment` (service_id, user_id, payment_value, payment_description,payment_previous_balance) 
  VALUES (:service_id, :user_id, :payment_value, :payment_description,:payment_previous_balance)';

  $sql = mysql_encode_param_array($sql, array(
  'service_id' => $selected_service->service_id,
  'user_id' => $_SESSION['auth']['user']->user_id,
  'payment_value' => $request->payment_value,
  'payment_description' => $request->description,
  'payment_previous_balance' => $selected_service->service_credit_available,
  )); */
?>
<?php

function get_currency($from_Currency, $to_Currency, $amount) {

   $amount = urlencode($amount);
   $from_Currency = urlencode($from_Currency);
   $to_Currency = urlencode($to_Currency);

   $url = "http://www.google.com/finance/converter?a=$amount&from=$from_Currency&to=$to_Currency";

   $ch = curl_init();
   $timeout = 0;
   curl_setopt($ch, CURLOPT_URL, $url);
   curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);

   curl_setopt($ch, CURLOPT_USERAGENT, "Mozilla/4.0 (compatible; MSIE 8.0; Windows NT 6.1)");
   curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, $timeout);
   $rawdata = curl_exec($ch);
   curl_close($ch);
   $data = explode('bld>', $rawdata);
   $data = explode($to_Currency, $data[1]);
   return round($data[0], 2);
}

// Call the function to get the currency converted
$exrate = get_currency('EUR', 'ZAR', 1)
?>

<aside class="right-side">
   <section class="content-header">
      <h1>
         Routes / Rates - 1 Euro - ZAR : R <?php echo $exrate; ?>
         <!--small>Control panel</small-->
      </h1>
   </section>

   <section class="content">
      <div class="row">
         <div class="col-md-12">
            <div class="box box-connet" style="padding-bottom:1px; border-top-color:<?php echo $accRGB; ?>;">
               <div class="form-group col-md-12" style="padding-top:20px;">
                  <div class="form-group col-md-4">
                     <div id="reportrange" class="select pull-left" onmouseover="" style="cursor: pointer;">
                        <i class="fa fa-calendar fa-lg"></i>
                        <span style="font-size:15px"><?php echo $startDate . " - " . $endDate; ?></span><b class="caret"></b>
                     </div>
                  </div>
               </div>

               <div class="form-group col-md-4">
                  <label>Choose Country</label>
                  <select class="form-control" id="countryCheck" name="country" onchange="filterSelect();">
                     <?php
                     echo "<option SELECTED>Show All</option>";
                     if (isset($countries)) {
                        foreach ($countries as $key => $value) {
                           $value['prefix_country_name'] = trim($value['prefix_country_name']);
                           if ($value['prefix_country_name'] != '') {
                              if (isset($_GET['country']) && ($_GET['country'] == $value['prefix_country_name'])) {
                                 echo '<option value="' . $value['prefix_country_name'] . ' " SELECTED>' . $value['prefix_country_name'] . '</option>';
                              } else {
                                 echo '<option value="' . $value['prefix_country_name'] . ' ">' . $value['prefix_country_name'] . '</option>';
                              }
                           }
                        }
                     }
                     ?>
                  </select>
               </div>
               <div class="form-group col-md-4">
                  <label>Choose Network</label>
                  <select id="netCheck" class="form-control" name="networkCheck">
                     <?php
                     echo "<option SELECTED>Show All</option>";
                     if (isset($networks)) {
                        foreach ($networks as $key => $value) {
                           if (isset($_GET['network']) && ($_GET['network'] == ($value['prefix_mccmnc']))) {
                              echo "<option value=" . $value['prefix_mccmnc'] . " SELECTED>" . $value['prefix_network_name'] . "</option>";
                           } else {
                              echo "<option value=" . $value['prefix_mccmnc'] . ">" . $value['prefix_network_name'] . "</option>";
                           }
                        }
                     }
                     ?>
                  </select>
               </div>
               <div class="form-group col-md-2">
                  <label>Choose Collection</label>
                  <select id="colCheck" class="form-control" name="colCheck">
                     <?php
                     echo "<option SELECTED>Show All</option>";
                     if (isset($collections)) {
                        foreach ($collections as $key => $value) {
                           if (isset($_GET['collection']) && ($_GET['collection'] == $value['route_collection_id'])) {
                              echo "<option SELECTED value='" . $value['route_collection_id'] . "'>" . $value['route_collection_name'] . "</option>";
                           } else {
                              echo "<option value='" . $value['route_collection_id'] . "' >" . $value['route_collection_name'] . "</option>";
                           }
                        }
                     }
                     /* if(isset($collectionsLimit))
                       {
                       for ($i=0; $i < count($collectionsLimit); $i++)
                       {
                       if($_GET['collection'] == $collectionsLimit[$i])
                       {
                       echo "<option SELECTED>".$collectionsLimit[$i]."</option>";
                       }
                       else
                       {
                       echo "<option value='".$collectionsLimit[$i]."' >".$collectionsLimit[$i]."</option>";
                       }
                       }
                       } */
                     ?>
                  </select>
               </div>
               <div class="form-group col-md-2">
                  <a class="btn btn-block btn-social btn-dropbox " onclick="filterSelect2();" style="background-color: <?php echo $accRGB; ?>; height:36px; color: #ffffff; border: 2px solid; border-radius: 6px !important; float:left; width: 100px; margin-top:24px;">
                     <i class="fa fa-search"></i>Search
                  </a>
               </div>
            </div>
         </div>
      </div>
      <div class="row">
         <div class="col-md-12">
            <div class="box box-connet" style="padding-bottom:1px; border-top-color:<?php echo $accRGB; ?>;">
               <div class="box-body table-responsive">
                  <p style='margin-top:20px;margin-left:20px;'>
                     <input  type='checkbox' name='ce' id='ce' onclick="checking();" />+5%
                  </p>

                  <table id="billingTable" class="table table-bordered table-striped dataTable table-hover">
                     <thead>
                        <tr>
                           <th id="ss">Service Name</th>
                           <th>Network</th>
                           <th>Country</th>
                           <th>Units</th>
                           <th>Service</th>
                           <th>Service R</th>
                           <th>Cost</th>
                           <th style='display:none;'>+5%</th>
                           <th>Cost R</th>
                           <th style='display:none;'>+5%</th>
                           <th>Default</th>
                           <th>Collection</th>
                           <th>Status</th>
                           <th>Manager</th>

                        </tr>
                     </thead>
                     <tbody id='billingTableBody'>
                        <?php
                        if (isset($allServices)) {
                           $ind = 0;
                           foreach ($allServices as $key => $value) {
                              $ind++;
                              if (substr($value['COUNTRY'], 0, 3) == "USA") {
                                 $flag = "USA";
                              } else {
                                 $flag = $value['COUNTRY'];
                              }
                              $flag = str_replace('/', '&', $flag);

                              echo '<tr>';
                              echo "<td>" . $value['Service_Name'] . ' (' . $value['service_id'] . ")</td>";

                              $mccNetSubCont = false;
                              if (isset($serProviders) && !empty($serProviders)) {
                                 foreach ($serProviders as $key13 => $value13) {
                                    $imageT = strstr($value13, '.', true);
                                    $net = strstr($value['NETWORK'], ' (', true);

                                    if ($imageT == $net) {
                                       $mccNetSubCont = $imageT;
                                       break;
                                    }
                                 }
                              }

                              if ($mccNetSubCont != false) {
                                 echo "<td><img src='../img/serviceproviders/" . $mccNetSubCont . ".png'>&nbsp;&nbsp;" . $value['NETWORK'] . "</td>";
                              } else {
                                 echo "<td><img src='../img/serviceproviders/Special.png'>&nbsp;&nbsp;" . $value['NETWORK'] . "</td>";
                              }

                              echo "<td><img src='../img/flags/" . $flag . ".png'>&nbsp;&nbsp;" . $value['COUNTRY'] . "</td>";

                              if (!empty($value['DEFAULTTOTAL'])) {
                                 echo "<td>" . ucfirst($value['DEFAULTTOTAL']) . "</td>";
                              } else {
                                 echo "<td>0</td>";
                              }

                              $errorRate = '';
                              if ($value['CURRENCY'] == 'ZAR') {
                                 //echo "<td style='color:#398a3f;'><center><b>".$value['CURRENCY']."</b></center></td>";
                                 if ($value['RATE'] <= $value['DEFAULTCOST']) {
                                    $errorRate = 'background-color:#ff8585;';
                                 } else {
                                    $errorRate = 'background-color:#e1e1e1;';
                                 }
                                 echo '<td style="vertical-align:middle; text-align:center;"><b style="color:#993333;">R</b> ' . $value['RATE'] . '</td>';
                                 echo '<td style="vertical-align:middle; text-align:center;' . $errorRate . '"><b style="color:#993333;">R</b> ' . $value['RATE'] . '</td>';
                                 //echo '<td style="vertical-align:middle; text-align:center;"></td>';
                              } else {
                                 //echo "<td style='color:#39548a;'><center><b>".$value['CURRENCY']."</b></center></td>";
                                 if ($value['RATE'] * $exrate <= $value['DEFAULTCOST']) {
                                    $errorRate = 'background-color:#ff8585;';
                                 } else {
                                    $errorRate = 'background-color:#e1e1e1;';
                                 }
                                 echo '<td style="vertical-align:middle; text-align:center;"><b style="color:#3333dd;"><i class="fa fa-eur"></i></b> ' . $value['RATE'] . '</td>';
                                 echo '<td style="vertical-align:middle; text-align:center;' . $errorRate . '"><b style="color:#993333;">R</b> ' . number_format($value['RATE'] * $exrate, 4, '.', '') . '</td>';
                              }

                              $valueTemp = ' ' . $value['cdr_comments'];

                              if ((strpos($valueTemp, 'eur') != false) || $value['cdr_comments'] == 'eur') {
                                 echo '<td style="vertical-align:middle; text-align:center;"><b style="color:#3333dd;">&euro;</b> ' . ($value['DEFAULTCOST']) . '</td>';
                                 echo '<td style="vertical-align:middle;display:none;text-align:center;background-color:#ffb395;"><b style="color:#3333dd;">&euro;</b>' . number_format($value['DEFAULTCOST'] * 1.05, 4, '.', '') . '</td>';
                              } elseif ((strpos($valueTemp, 'usd') != false) || $value['cdr_comments'] == 'usd') {
                                 echo '<td style="vertical-align:middle; text-align:center;"><b style="color:#17852c;"><i class="fa fa-usd"></i></b> ' . ($value['DEFAULTCOST']) . '</td>';
                                 echo '<td style="vertical-align:middle;display:none;text-align:center;background-color:#ffb395;"><b style="color:#17852c;"><i class="fa fa-usd"></i></b>' . number_format($value['DEFAULTCOST'] * 1.05, 4, '.', '') . '</td>';
                              } elseif ((strpos($valueTemp, 'gbp') != false) || $value['cdr_comments'] == 'gbp') {
                                 echo '<td style="vertical-align:middle; text-align:center;"><b style="color:#927119;"><i class="fa fa-gbp"></i></b> ' . ($value['DEFAULTCOST']) . '</td>';
                                 echo '<td style="vertical-align:middle;display:none;text-align:center;background-color:#ffb395;"><b style="color:#927119;"><i class="fa fa-gbp"></i></b>' . number_format($value['DEFAULTCOST'] * 1.05, 4, '.', '') . '</td>';
                              } else {
                                 echo '<td style="vertical-align:middle; text-align:center;"><b style="color:#993333;">R</b> ' . ($value['DEFAULTCOST']) . '</td>';
                                 echo '<td style="display:none;"></td>';
                              }

                              if ($value['CURRENCY'] != 'ZAR' && ((strpos($valueTemp, 'eur') != false) || $value['cdr_comments'] == 'eur')) {
                                 if ($value['RATE'] * $exrate <= $value['DEFAULTCOST'] * $exrate) {
                                    $errorRate = 'background-color:#ff8585;';
                                 } else {
                                    $errorRate = 'background-color:#e1e1e1;';
                                 }
                                 //echo '<td style="vertical-align:middle;text-align:right;width:30px;">'.number_format(($value['DEFAULTCOST']/10000)*$exrate, 4, '.', '').'</td>';
                                 echo '<td style="vertical-align:middle; text-align:center;' . $errorRate . '"><b style="color:#993333;">R</b> ' . number_format($value['DEFAULTCOST'] * $exrate, 4, '.', '') . '</td>';
                                 echo '<td style="vertical-align:middle;display:none;text-align:center;"><b style="color:#993333;">R</b> ' . number_format($value['DEFAULTCOST'] * $exrate * 1.05, 4, '.', '') . '</td>';
                              } elseif ((strpos($valueTemp, 'eur') != false) || $value['cdr_comments'] == 'eur') {
                                 if ($value['RATE'] <= $value['DEFAULTCOST'] * $exrate) {
                                    $errorRate = 'background-color:#ff8585;';
                                 } else {
                                    $errorRate = 'background-color:#e1e1e1;';
                                 }
                                 //echo '<td style="vertical-align:middle;text-align:right;width:30px;">'.number_format(($value['DEFAULTCOST']/10000)*$exrate, 4, '.', '').'</td>';
                                 echo '<td style="vertical-align:middle; text-align:center;' . $errorRate . '"><b style="color:#993333;">R</b> ' . number_format($value['DEFAULTCOST'] * $exrate, 4, '.', '') . '</td>';
                                 echo '<td style="vertical-align:middle;display:none;text-align:center;"><b style="color:#993333;">R</b> ' . number_format($value['DEFAULTCOST'] * $exrate * 1.05, 4, '.', '') . '</td>';
                              } else {
                                 echo '<td style="vertical-align:middle; text-align:center;' . $errorRate . '"><b style="color:#993333;">R</b> ' . number_format($value['DEFAULTCOST'], 4, '.', '') . '</td>';
                                 //echo '<td style="vertical-align:middle;text-align:right;width:30px;">'.number_format(($value['DEFAULTCOST']/10000), 4, '.', '').'</td>';
                                 //echo '<td style="vertical-align:middle; text-align:center;"></td>';
                                 echo '<td style="display:none;"></td>';
                              }
                              $mccNetSubCont = false;
                              if (isset($serProviders) && !empty($serProviders)) {
                                 foreach ($serProviders as $key13 => $value13) {
                                    $imageT = strstr($value13, '.', true);

                                    if ($imageT == $value['DEFAULTR']) {
                                       $mccNetSubCont = $imageT;
                                       break;
                                    }
                                 }
                              }

                              if ($mccNetSubCont != false) {
                                 echo '<td style="vertical-align:middle"><img src="../img/serviceproviders/' . $mccNetSubCont . '.png">&nbsp;&nbsp;' . ucfirst($mccNetSubCont) . '</td>';
                              } else {
                                 echo "<td>" . $value['DEFAULTR'] . "</td>";
                              }
                              echo "<td>" . $value['COLLECTION'] . "</td>";
                              echo '<td style="vertical-align:middle;"><i class="fa fa-check" style="color:#00ff00;"></i>&nbsp;&nbsp;' . $value['STATUS'] . '</td>';
                              echo "<td>" . ucfirst($value['FULLNAME']) . "</td>";

                              echo '</tr>';
                           }
                        }
                        ?>
                     </tbody>
                  </table>
               </div>
               <div class="box-body">
                  <div class="form-group">
                     <a onclick="saveRep();" class="btn btn-block btn-social btn-dropbox " style="background-color:<?php echo $accRGB; ?>; border: 2px solid; border-radius: 6px !important; float:left; width: 134px; margin-top:-19px;">
                        <i class="fa fa-save"></i>Save Report
                     </a>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </section>

</aside>

<!--  THE JAVASCRIPT IMPORT ALWAYS APPEARS BEFORE LOCAL JAVASCRIPT AT THE BOTTOM OF THE PAGE -->
<?php include("template_import_script.php"); //must import all scripts first ?>
<!-- END JAVASCRIPT IMPORT -->

<script type="text/javascript">

   function checked(col_no, do_show)
   {
      var tbl = document.getElementById('billingTable');
      var rows = tbl.getElementsByTagName('tr');
      console.log("rows=" + rows);

      for (var row = 0; row < rows.length; row++)
      {
         var cols = rows[row].children;
         console.log("cols=" + cols.length);
         if (col_no >= 0 && col_no < cols.length)
         {
            console.log("col_no=" + col_no);
            var cell = cols[col_no];
            if (cell.tagName == 'TD')
               cell.style.display = do_show ? 'table-cell' : 'none';
            if (cell.tagName == 'TH')
               cell.style.display = do_show ? 'table-cell' : 'none';
         }
      }
   }

   $('input[type="checkbox"][name="ce"]').on('ifChecked', function (event) {
      checked(7, true);
      checked(9, true);
   });

   $('input[type="checkbox"][name="ce"]').on('ifUnchecked', function (event) {
      checked(7, false);
      checked(9, false);
   });

   /*checked(6, false);
    checked(8, false);*/

   function saveRep()
   {
      var csvContent = "data:text/csv;charset=utf-8,";
      var data = [["Account", "Service Name", "Network", "Country", "Status", "Rate", "Currency", "Collection", "DEFAULT", "Manager"],
<?php
if (isset($allServices)) {
   foreach ($allServices as $key => $value) {

      ///print_r($value);
      $array = explode('-', $value['Service_Name']);
      $serviceName = isset($array[1]) ? $array[1] : "";
      $accountName = isset($array[0]) ? $array[0] : "";
      echo '[';
      echo '"' . $accountName . '",';
      echo '"' . $serviceName . '",';
      echo '"' . $value['NETWORK'] . '",';
      echo '"' . $value['COUNTRY'] . '",';
      echo '"' . $value['STATUS'] . '",';
      echo '"' . $value['RATE'] . '",';
      echo '"' . $value['CURRENCY'] . '",';
      echo '"' . $value['COLLECTION'] . '",';
      echo '"' . $value['DEFAULTR'] . '",';
      //PORTED R does not existed in the returned array
      //echo '"' . ucfirst($value['PORTED']) . '",';
      echo '"' . $value['FULLNAME'] . '"';
      echo '],';
   }
}
?>
      ];
      data.forEach(function (infoArray, index)
      {
         dataString = infoArray.join(",");
         csvContent += index < data.length ? dataString + "\n" : dataString;
      });
      var encodedUri = encodeURI(csvContent);
      var link = document.createElement("a");
      link.setAttribute("href", encodedUri);
      link.setAttribute("download", "<?php echo 'Billing ' . $_GET['country'] . ' ' . $_GET['network'] . ' ' . $_GET['collection']; ?>.csv");

      link.click();
   }
</script>



<script type="text/javascript">
   var repRange = "<?php echo $startDate . ' - ' . $endDate; ?>"

   $(function ()
   {
      $('#billingTable').dataTable
              ({
                 "bDestroy": true,
                 "bPaginate": true,
                 "bLengthChange": false,
                 "bFilter": true,
                 "bSort": true,
                 "bInfo": true,
                 "iDisplayLength": 100,
                 "bAutoWidth": false,
                 "bProcessing": true,
                 "bServerSide": false
                         //"bSorting": [[4, "desc"]]
              });

   });

   $('#reportrange').daterangepicker(
           {
              ranges: {
                 'Today': [moment(), moment()],
                 'Yesterday': [moment().subtract('days', 1), moment().subtract('days', 1)],
                 'Last 7 Days': [moment().subtract('days', 6), moment()],
                 'Last 30 Days': [moment().subtract('days', 29), moment()],
                 'This Month': [moment().startOf('month'), moment().endOf('month')],
                 'Last Month': [moment().subtract('month', 1).startOf('month'), moment().subtract('month', 1).endOf('month')]
              },
              startDate: moment(<?php echo '"' . $startDate . '"'; ?>),
              endDate: moment(<?php echo '"' . $endDate . '"'; ?>)

           },
           function (start, end) {
              //$(".loader").fadeIn("slow");

              //$(".loaderIcon").fadeIn("slow");
              $('#reportrange span').html(start.format('YYYY-MM-DD 00:00:00') + ' - ' + end.format('YYYY-MM-DD 23:59:59'));
              //var serviceName = document.getElementById("client").value;

              //encodeURI(serviceName);
              //window.location = "../pages/networktraffic.php?client=" + encodeURIComponent(serviceName) + "&range=" + repRange;

           }
   );

   function filterSelect()
   {
      $(".loader").fadeIn("slow");
      $(".loaderIcon").fadeIn("slow");
      var country = document.getElementById('countryCheck').value;
      country = country.trim();
      var net = document.getElementById('netCheck').value;
      net = net.trim();
      var coll = document.getElementById('colCheck').value;
      coll = coll.trim();



      window.location = "billingnew.php?search=1" + "&country=" + country + "&network=" + net + "&collection=" + coll;

   }

   function filterSelect2()
   {
      $(".loader").fadeIn("slow");
      $(".loaderIcon").fadeIn("slow");
      var country = document.getElementById('countryCheck').value;
      country = country.trim();
      var net = document.getElementById('netCheck').value;
      net = net.trim();
      var coll = document.getElementById('colCheck').value;
      coll = coll.trim();

      repRange = $("#reportrange span").html();

      window.location = "billingnew.php?search2=1&search=1" + "&country=" + country + "&network=" + net + "&collection=" + coll + "&range=" + repRange;

   }

</script>
<script type="text/javascript">
   $(window).load(function () {
      $(".loader").fadeOut("slow");
      $(".loaderIcon").fadeOut("slow");
   })
</script>

<!-- Template Footer -->

