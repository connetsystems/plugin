<?php
/**
 * Include the header template which sets up the HTML
 *
 * Don't forget to include template_import_script before any Javascripting
 * Don't forget to include template footer.php at the bottom of the page as well
 */
//-------------------------------------------------------------
// Template
//-------------------------------------------------------------
TemplateHelper::setPageTitle('Update Service');
TemplateHelper::initialize();

$singleView = 'block;';
require_once("../php/helperPassword.php");

//-------------------------------------------------------------
// Permissions
//-------------------------------------------------------------
if (LoginHelper::isSystemAdmin()) {

   $clients = getClients();
} else {

   $clients = PermissionsHelper::getAllServicesWithPermissions();

   foreach ($clients as $index => $service) { //Can't edit reseller accounts
      $service_id = (integer) $service['service_id'];
      $service_id_reseller = (integer) $service['service_id_reseller'];

      if ($service_id_reseller == $service_id) {

         unset($clients[$index]);
      }
   }
}


if (isset($_POST['service_id'])) {

   $_GET['service_id'] = $_POST['service_id'];
}

if (!isset($_GET['service_id'])) {

   foreach ($clients as $service) {

      $_GET['service_id'] = (integer) $service['service_id'];;
      break;
   }
}

if (count($clients) == 1) {

   $singleView = 'none;';
}

//-------------------------------------------------------------
// OLD Permissions - Not sure if this is still needed
//-------------------------------------------------------------
//check the user has permissions to actually view this service, kill the page if they don't
if (isset($_GET['service_id'])) {

   $service_id = (integer) $_GET['service_id'];
   $account_id = LoginHelper::getCurrentAccountId();

   if (!LoginHelper::isSystemAdmin() && !checkAccountHasAccessToService($account_id, $service_id)) {
      echo '<meta http-equiv="refresh" content= "0; URL=access_denied.php" />';
      die();
   }
}


$userCount = 0;
$routeCount = 0;
$scrPos = 0;

$accountName = "";
$serviceName = "";

$tog1 = "";
$togs1 = "";
$tog2 = "";
$togs2 = "";
$tog3 = "";
$togs3 = "";
$tog4 = "";
$togs4 = "";
$tog5 = "";
$togs5 = "";
$tog6 = "";
$togs6 = "";

///////////////////////////////////////////////////////////
if (isset($_GET['togs1'])) {
   if ($_GET['tog1'] == 'out') {
      $togs1 = 'fail';
   } else {
      $togs1 = 'success';
   }
}
///////////////////////////////////////////////////////////
if (isset($_GET['tog1'])) {
   $tog1 = 'in';
   $scrPos = 200;
   if ($_GET['tog1'] == 'out') {
      $togs1 = 'fail';
   } else {
      $togs1 = 'success';
   }
}

//NIEL
if (isset($_GET['service_id']) && count($_GET) == 1) {
   $tog1 = 'in';
   $togs1 = '';
}

///////////////////////////////////////////////////////////
if ($singleView == 'none;') {
   $tog1 = 'in';
}
///////////////////////////////////////////////////////////
if (isset($_GET['tog2'])) {
   $tog2 = 'in';
   $togs2 = 'success';
   $scrPos = 250;
}
///////////////////////////////////////////////////////////
if (isset($_GET['tog3'])) {
   $tog3 = 'in';
   $scrPos = 300;
   if ($_GET['tog3'] == 'out') {
      $togs3 = 'fail';
   } else {
      $togs3 = 'success';
   }
}
///////////////////////////////////////////////////////////
if (isset($_GET['tog4'])) {
   $tog4 = 'in';
   $togs4 = 'success';
   $scrPos = 350;
}
if (isset($_GET['tog5'])) {
   $tog5 = 'in';
   $togs5 = 'success';
   $scrPos = 400;
}
if (isset($_GET['tog6'])) {
   $tog6 = 'in';
   $togs6 = 'success';
   $scrPos = 450;
}

$changes = "";
if (isset($_GET['change'])) {
   $changes = $_GET['change'];
}

if (isset($_GET['service_id']) || isset($_POST['service_id'])) {
   $dsp = 'inline';
   $dir = '../img/routes';
   $serProviders = scandir($dir);
} else {
   $service_id = 0;
   $dsp = 'none';
}


if ($service_id != 0) {
   if (isset($_GET['delLBN']) && $_GET['delLBN'] == 'del') {
      $del = deleteNotifications($service_id);
      $changes = 'del';
      $togs5 = 'success';
   }

   $alerts = getAlerts($service_id);
   $client = getClient($service_id);
   $defaultRouteData = getDefaultRouteData($service_id);
   $payments = getPayments($service_id);
   $users = getUsers($service_id);
   $funds = getFunds($service_id);
   //$manager = getManager($service_id);
   //$urls = getURLs($service_id);

   $managers = getManagers();
   $serviceData = getServiceData($service_id);

   /* foreach ($serviceData as $key => $value) 
     {
     echo '<pre>';
     print_r($value);
     echo '</pre>';
     } */

   $urls = array();
}

if (isset($_GET['updateClientInfo'])) {
   $change1 = "";
   foreach ($serviceData as $key => $value) {
      /* if($key == 0)
        { */
      $_GET['servName'] = trim($_GET['servName']);
      if ($_GET['servName'] != $value['service_name']) {
         $check = checkServiceName($_GET['servName']);
         if ($check === 1) {
            updateServer($_GET['servName'], "service_name", "core.core_service", "service_id", $service_id);
            $change1 .= "&nbsp;&nbsp;Service&nbsp;Name&nbsp;set&nbsp;to&nbsp;" . $_GET['servName'];
            //echo "<br>DUP1";
         } else {
            //DUPLICATE NAME
            $change1 .= "&nbsp;" . $_GET['servName'] . " already exists!&nbsp;&nbsp;";
            $togs1 = 'fail';
            echo '<meta http-equiv="refresh" content= "0; URL=updateservice.php?service_id=' . $service_id . '&tog1=out&change=' . $change1 . ' " />';
            die();
            //echo "<br>DUP";
         }
      }

      if ($togs1 != 'fail') {
         if ($_GET['accStatus'] != $value['service_status']) {
            updateServer($_GET['accStatus'], "service_status", "core.core_service", "service_id", $service_id);
            $change1 .= "&nbsp;&nbsp;Account&nbsp;Status&nbsp;set&nbsp;to&nbsp;" . $_GET['accStatus'];
         }

         if ($_GET['paymentType'] != $funds['0']) {
            updateServer($_GET['paymentType'], "service_credit_billing_type", "core.core_service_credit", "service_id", $service_id);
            $change1 .= "&nbsp;&nbsp;Payment&nbsp;Type&nbsp;set&nbsp;to&nbsp;" . $_GET['paymentType'];
         }
      }
      //}
   }

   if ($togs1 != 'fail') {
      echo '<meta http-equiv="refresh" content= "0; URL=updateservice.php?service_id=' . $service_id . '&tog1=in&togs1=in&change=' . $change1 . ' " />';
      die();
   }
}

if (isset($_POST['updateRoutes'])) {
   $change2 = "";

   $rCount = $_POST['routeCount'];
   foreach ($defaultRouteData as $key => $value) {
      for ($i = 1; $i <= $rCount; $i++) {
         $newRate = 'routeRate' . $i;
         $newRDNC = 'routeRDNC' . $i;
         $newCur = $_POST['routeCur' . $i];
         $rId = 'routeId' . $i;
         $nRate = ($_POST[$newRate] * 1) . ',0,0,0,' . ($_POST[$newRDNC] * 1);

         if ($_POST[$rId] == $value['route_id']) {
            $rCurr = 0;
            $rRate = 0;
            if ($newCur != $value['route_currency']) {
               $rCurr = 1;
            }

            if ($nRate != $value['RATE']) {
               $rRate = 1;
            }

            if ($rCurr == 1 || $rRate == 1) {
               $costsArr = selectFromMasterDB('CALL UpdateRoute_Rev(' . $value["route_id"] . ', ' . $_SESSION["userId"] . ')', 'core');
            }

            if ($newCur != $value['route_currency']) {
               updateServer($newCur, "route_currency", "core.core_route", "route_id", $_POST[$rId]);
               $changes .= $value['route_currency'] . " changed to " . $newCur . " ";
               $value['route_currency'] = $newCur;
               $defaultRouteData[$key] = $value;
            }

            if ($nRate != $value['RATE']) {
               updateServer($nRate, "route_billing", "core.core_route", "route_id", $_POST[$rId]);
               $changes .= explode(',', $value['RATE'])[0] . " to " . $_POST[$newRate] . " ";
               $value['RATE'] = $nRate;
               $defaultRouteData[$key] = $value;
            }
         }
      }
   }
}

if (isset($_GET['newUser'])) {
   $change3 = "";
   addUser($_GET['newUser'], $_GET['cl']);
   $change3 = "&nbsp;&nbsp;" . $_GET['newUser'] . ".";
   echo '<meta http-equiv="refresh" content= "0; URL=updateservice.php?service_id=' . $service_id . '&tog3=in&change=' . $change3 . ' " />';
   die();
}

if (isset($_GET['updateUsers'])) {
   $change3 = "";
   /* $Id = explode(';', $_GET['updateUsers']);
     array_pop($Id); */
   foreach ($users as $key => $value) {
      $uV = 'user' . $value['user_id'];

      foreach ($_GET as $key2 => $value2) {
         if ($uV == $key2) {
            $_GET[$key2] = trim($_GET[$key2]);
            $_GET[$key2] = str_replace(' ', '', $_GET[$key2]);
            if ($_GET[$key2] != $value['user_username']) {
               if ($value['user_username'] === $value['service_login']) {
                  //echo "<br>".$value['user_id']."<---REPLACE this username in servicelogin in core.service where sid = !!-->".($_GET['sId'])."<--";
                  //$_GET[$key2] = trim($_GET[$key2]);
                  $check = checkUserName($_GET[$key2]);
                  if ($check === 1) {
                     updateServer($_GET[$key2], "user_username", "core.core_user", "user_id", $value['user_id']);
                     updateServer($_GET[$key2], "service_login", "core.core_service", "service_id", $_GET['sId']);
                     //$defaultServ = getDefaultServiceLoginFromSID($service_id);
                     $dlrID = getEPDLRid($_GET['sId'], 'SMPP');
                     updateServer('http://server2:3001/core_sms/interfaces/smpp/smpp_dlr.php?debug=hallodaar&system_id=' . $_GET[$key2], "endpoint_dlr_address", "core.push_endpoint_dlr", "endpoint_dlr_id", $dlrID);
                     $mosmsID = getEPMOSMSid($_GET['sId'], 'SMPP');
                     updateServer('http://server2:3001/core_sms/interfaces/smpp/smpp_mosms.php?debug=hallodaar&system_id=' . $_GET[$key2], "endpoint_mosms_address", "core.push_endpoint_mosms", "endpoint_mosms_id", $mosmsID);
                     //updateServer($service_id, , $_GET['mosmsStatusHTTP'], "SMPP", "");
                     $change3 .= "Username to&nbsp;" . $_GET[$key2] . "&nbsp;&nbsp;";
                  } else {
                     $change3 .= "&nbsp;" . $_GET[$key2] . " already exists!&nbsp;&nbsp;";
                     $togs3 = 'fail';
                     echo '<meta http-equiv="refresh" content= "0; URL=updateservice.php?service_id=' . $service_id . '&tog3=out&change=' . $change3 . ' " />';
                     die();
                  }
               } else {
                  //$_GET[$key2] = trim($_GET[$key2]);
                  //echo "<br>".$value['user_id']."<---REPLACE this username in servicelogin in core.service where sid = !!-->".($_GET['sId']);
                  //echo "<br>NEW USER NAME issnt DEFAULT!!-->".($_GET[$key2]);
                  $check = checkUserName($_GET[$key2]);
                  if ($check === 1) {
                     updateServer($_GET[$key2], "user_username", "core.core_user", "user_id", $value['user_id']);
                     $change3 .= "Username to&nbsp;" . $_GET[$key2] . "&nbsp;&nbsp;";
                  } else {
                     $change3 .= "&nbsp;'" . $_GET[$key2] . "'&nbsp;already exists!&nbsp;&nbsp;";
                     $togs3 = 'fail';
                     echo '<meta http-equiv="refresh" content= "0; URL=updateservice.php?service_id=' . $service_id . '&tog3=out&change=' . $change3 . ' " />';
                     die();
                  }
               }
            }
         }
      }

      $uP = 'pass' . $value['user_id'];
      foreach ($_GET as $key2 => $value2) {
         if ($uP == $key2) {
            //echo "<br>Pmatch-->".($_GET[$key2]);
            $_GET[$key2] = trim($_GET[$key2]);
            $_GET[$key2] = str_replace(' ', '', $_GET[$key2]);
            if ($_GET[$key2] != "" && helperPassword::validatePassword($_GET[$key2], $value['user_password_hashed'])) {
               //echo "<br>NEW PASSWORD!!-->".($_GET[$key2]);
               //save the original password
               updateServer($_GET[$key2], "user_password", "core.core_user", "user_id", $value['user_id']);

               $hashed_password = helperPassword::createHashedPassword($_GET[$key2]);

               updateServer($hashed_password, "user_password_hashed", "core.core_user", "user_id", $value['user_id']);
               $change3 .= "Password set to&nbsp;" . $_GET[$key2] . "&nbsp;&nbsp;";
            }
         }
      }

      $uS = 'stat' . $value['user_id'];
      foreach ($_GET as $key2 => $value2) {
         if ($uS == $key2) {
            //echo "<br>Smatch-->".($_GET[$key2]);
            $_GET[$key2] = trim($_GET[$key2]);
            $_GET[$key2] = str_replace(' ', '', $_GET[$key2]);
            if ($_GET[$key2] != $value['user_status']) {
               //echo "<br>NEW STATUS!!-->".($_GET[$key2]);
               updateServer($_GET[$key2], "user_status", "core.core_user", "user_id", $value['user_id']);
               $change3 .= "User&nbsp;" . $_GET[$key2] . "&nbsp;&nbsp;";
            }
         }
      }

      /* echo "<pre>";
        print_r($value);
        echo "</pre>"; */
   }
   //echo '<br> togs3 = '.$togs3;
   if ($togs3 != 'fail') {
      echo '<meta http-equiv="refresh" content= "0; URL=updateservice.php?service_id=' . $service_id . '&tog3=in&change=' . $change3 . ' " />';
      die();
   }
}

if (isset($_GET['updateLowBalance'])) {
   $change5 = "";

   if (isset($_GET['thresholdLimit']) && $_GET['thresholdLimit'] != '') {
      if (strpos($_GET['thresholdLimit'], '.')) {
         $_GET['thresholdLimit'] = trim($_GET['thresholdLimit']);
         $_GET['thresholdLimit'] = strstr($_GET['thresholdLimit'], '.', true);
         $_GET['thresholdLimit'] = $_GET['thresholdLimit'] * 10000;
      } else {
         $_GET['thresholdLimit'] = trim($_GET['thresholdLimit']) * 10000;
      }

      $cLow1 = checkLowBalanceEntry($service_id, 'service_notification_threshold');
      if ($cLow1 == -1) {
         updateServer($_GET['thresholdLimit'], "service_notification_threshold", "core.core_service_credit", "service_id", $service_id);
         $change5 .= "Threshold Limit set to: " . ($_GET['thresholdLimit'] / 10000) . " ";
      } elseif ($cLow1 == $_GET['thresholdLimit']) {
         $change5 .= "";
      } else {
         updateServer($_GET['thresholdLimit'], "service_notification_threshold", "core.core_service_credit", "service_id", $service_id);
         $change5 .= "Threshold Limit set to: " . ($_GET['thresholdLimit'] / 10000) . " ";
      }
   }

   if (isset($_GET['emailAddresses']) && $_GET['emailAddresses'] != '') {
      $_GET['emailAddresses'] = trim($_GET['emailAddresses']);
      $_GET['emailAddresses'] = str_replace(" ", "", $_GET['emailAddresses']);

      $cLow2 = checkLowBalanceEntry($service_id, 'service_notification_email');

      //echo "<br>email1 = ".$cLow2;
      //echo "<br>email2 = ".$_GET['emailAddresses'];
      if ($cLow2 == -1) {
         updateServer($_GET['emailAddresses'], "service_notification_email", "core.core_service_credit", "service_id", $service_id);
         $change5 .= "Email set to: " . $_GET['emailAddresses'] . " ";
      } elseif ($cLow2 == $_GET['emailAddresses']) {
         $change5 .= "";
      } else {
         updateServer($_GET['emailAddresses'], "service_notification_email", "core.core_service_credit", "service_id", $service_id);
         $change5 .= "Email set to: " . $_GET['emailAddresses'] . " ";
      }
   }

   if (isset($_GET['smsNumbers']) && $_GET['smsNumbers'] != '') {
      $_GET['smsNumbers'] = trim($_GET['smsNumbers']);
      $_GET['smsNumbers'] = str_replace(" ", "", $_GET['smsNumbers']);
      $cLow3 = checkLowBalanceEntry($service_id, 'service_notification_sms');
      if ($cLow3 == -1) {
         updateServer($_GET['smsNumbers'], "service_notification_sms", "core.core_service_credit", "service_id", $service_id);
         $change5 .= "Number set to: " . $_GET['smsNumbers'] . " ";
      } elseif ($cLow3 == $_GET['smsNumbers']) {
         $change5 .= "";
      } else {
         updateServer($_GET['smsNumbers'], "service_notification_sms", "core.core_service_credit", "service_id", $service_id);
         $change5 .= "Number set to: " . $_GET['smsNumbers'] . " ";
      }
   }
   echo '<meta http-equiv="refresh" content= "0; URL=updateservice.php?service_id=' . $service_id . '&tog5=in&change=' . $change5 . ' " />';
   die();
}

if (isset($_GET['updateURL'])) {
   $change6 = "";

   if (isset($_GET['dlrUrlHTTP']) && $_GET['dlrUrlHTTP'] != '') {
      $_GET['dlrUrlHTTP'] = trim($_GET['dlrUrlHTTP']);
      foreach ($serviceData as $key => $value) {
         $dlrId = $value['DLR_ID_LEGACY'];
         $dlrAddr = $value['DLR_ADDRESS_LEGACY'];
         //echo '<br>isat1 = -->'.$dlrId.'<--';
      }

      $cDLR = checkDLR($_GET['dlrUrlHTTP'], $service_id);

      //echo '<br>isat2 = -->'.$cDLR.'<--';
      if ($_GET['dlrUrlHTTP'] != $dlrAddr) {
         if ($cDLR == 0) {
            if (isset($dlrId) && $dlrId != '') {
               //update
               //echo '<br>--->update2';
               updateServer($_GET['dlrUrlHTTP'], "endpoint_dlr_address", "core.push_endpoint_dlr", "endpoint_dlr_id", $dlrId);
               $change6 .= " Updated DLR&nbsp;HTTP&nbsp;URL&nbsp;set&nbsp;to&nbsp;" . $_GET['dlrUrlHTTP'];
            } elseif (is_null($dlrId)) {
               //insert
               //echo '<br>--->insert2';
               setDlrUrls($service_id, $_GET['dlrUrlHTTP'], $_GET['dlrStatusHTTP'], "LEGACY", "strict=1");
               $change6 .= "Created new DLR HTTP URL&nbsp;&nbsp;" . $_GET['dlrUrlHTTP'];
            }
         } else {
            $change6 .= "-2";
            $err6 = $_GET['dlrUrlHTTP'];
            //$change6 .= "Error the Url is already in use."
         }
      }
   }

   if (isset($_GET['dlrStatusHTTP'])) {

      foreach ($serviceData as $key => $value) {
         $dlrStatus = $value['DLR_STATUS_LEGACY'];
         $dlrLegId = $value['DLR_ID_LEGACY'];
      }

      if (isset($dlrStatus) && ($_GET['dlrStatusHTTP'] != $dlrStatus)) {
         updateServer($_GET['dlrStatusHTTP'], "endpoint_dlr_status", "core.push_endpoint_dlr", "endpoint_dlr_id", $dlrLegId);
         $change6 .= " DLR status to: " . $_GET['dlrStatusHTTP'];
      }
   }

   if (isset($_GET['mosmsUrlHTTP']) && $_GET['mosmsUrlHTTP'] != '') {
      $_GET['mosmsUrlHTTP'] = trim($_GET['mosmsUrlHTTP']);
      foreach ($serviceData as $key => $value) {
         $moId = $value['MO_ID_LEGACY'];
         $moAddr = $value['MO_ADDRESS_LEGACY'];
      }

      if ($_GET['mosmsUrlHTTP'] != $moAddr) {
         $cMOSMS = checkMOSMS($_GET['mosmsUrlHTTP'], $service_id);
         if ($cMOSMS == 0) {
            if (isset($moId) && $moId != '') {
               //update
               updateServer($_GET['mosmsUrlHTTP'], "endpoint_mosms_address", "core.push_endpoint_mosms", "endpoint_mosms_id", $moId);
               $change6 .= " Updated MOSMS&nbsp;HTTP&nbsp;URL&nbsp;set&nbsp;to&nbsp;" . $_GET['mosmsUrlHTTP'];
            } else {
               //insert
               setMosmsUrls($service_id, $_GET['mosmsUrlHTTP'], $_GET['mosmsStatusHTTP'], "LEGACY", "strict=1");
               $change6 .= "Created new MOSMS HTTP URL&nbsp;&nbsp;" . $_GET['mosmsUrlHTTP'];
            }
         } else {
            $change6 .= "-2";
            $err6 = $_GET['mosmsUrlHTTP'];
         }
      }
   }

   if (isset($_GET['mosmsStatusHTTP'])) {

      foreach ($serviceData as $key => $value) {
         $moStatus = $value['MO_STATUS_LEGACY'];
         $moLegId = $value['MO_ID_LEGACY'];
      }

      if (isset($moStatus) && ($_GET['mosmsStatusHTTP'] != $moStatus)) {
         updateServer($_GET['mosmsStatusHTTP'], "endpoint_mosms_status", "core.push_endpoint_mosms", "endpoint_mosms_id", $moLegId);
         $change6 .= " MOSMS status to: " . $_GET['mosmsStatusHTTP'];
      }
   }

   if (isset($_GET['StatusSMPP'])) {
      //echo '<br>init!';
      foreach ($serviceData as $key => $value) {
         $smpp = $value['SMPP'];
         $endpointDlrId = $value['DLR_ID_SMPP'];
         $endpointMosmsId = $value['MO_ID_SMPP'];
         $serviceMeta = $value['service_meta'];
         $serviceLogin = $value['service_login'];
      }

      if (strpos($serviceMeta, '&cm_smpp')) {
         //echo '<br>Found';
      } else {
         // echo "<br>NOT: &cm_smpp";
         // echo '<br>---dl=>'.$endpointDlrId.'<=';
         //   echo '<br>---mo=>'.$endpointMosmsId.'<=';

         $serviceMeta = $serviceMeta . "&cm_smpp";
         updateServer($serviceMeta, "service_meta", "core.core_service", "service_id", $service_id);
      }

      if (!isset($endpointDlrId)) {
         setDlrUrls($service_id, 'http://server2:3001/core_sms/interfaces/smpp/smpp_dlr.php?debug=hallodaar&system_id=' . $serviceLogin, $_GET['StatusSMPP'], "SMPP", "");
      }

      if (!isset($endpointMosmsId)) {
         setMosmsUrls($service_id, 'http://server2:3001/core_sms/interfaces/smpp/smpp_mosms.php?debug=hallodaar&system_id=' . $serviceLogin, $_GET['StatusSMPP'], "SMPP", "");
      }


      if (isset($smpp) && $_GET['StatusSMPP'] != $smpp) {
         //Update DRL(SMPP) status
         updateServer($_GET['StatusSMPP'], "endpoint_dlr_status", "core.push_endpoint_dlr", "endpoint_dlr_id", $endpointDlrId);
         //Update MOSMS(SMPP) status
         updateServer($_GET['StatusSMPP'], "endpoint_mosms_status", "core.push_endpoint_mosms", "endpoint_mosms_id", $endpointMosmsId);
         $change6 .= " Updated SMPP status to: " . $_GET['StatusSMPP'];
      }
   }


   if (isset($err6)) {
      echo '<meta http-equiv="refresh" content= "0; URL=updateservice.php?service_id=' . $service_id . '&tog6=in&change=' . $change6 . '&err6=' . $err6 . ' " />';
      die();
   } else {
      echo '<meta http-equiv="refresh" content= "0; URL=updateservice.php?service_id=' . $service_id . '&tog6=in&change=' . $change6 . ' " />';
      die();
   }
}
?>

<aside class="right-side">
   <section class="content-header">
      <h1>
         Update Service
         <!--small>Control panel</small-->
      </h1>
   </section>
   <section class="content">
      <div class="row col-lg-12">
         <div class="box box-solid" style="height:66px; margin-top:-15px;display:<?php echo $singleView; ?>">
            <form action="updateservice.php" method="get" id="clientList" class="sidebar-form" style="border:0px;">
               <div class="form-group">
                  <label>Client List</label>
                  <select class="form-control" name="client" id="clientListSelect" form="clientList" OnChange="reloadOnSelect(this.value);">
                     <?php
                     if ($service_id == '0') {
                        echo '<option SELECTED>Please select a client...</option>';
                     }
                     foreach ($clients as $key => $value) {
                        $sId = $value['service_id'];
                        if ($service_id == $sId) {
                           $accountName = $value['account_name'];
                           $serviceName = $value['service_name'];
                           echo "<option value='" . $sId . "' SELECTED>" . $accountName . " - " . $serviceName . " - " . $sId . "</option>";
                        } else {
                           echo "<option value='" . $sId . "'>" . $value['account_name'] . " - " . $value['service_name'] . " - " . $sId . "</option>";
                        }
                     }
                     ?>
                  </select>
               </div>
               <br>
            </form>
         </div>
         <!-- /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////// -->
         <div class="box box-solid" style="display:<?php echo $dsp; ?>;">
            <div class="box-header">
               <h3 class="box-title"><?php echo $serviceName; ?></h3>
            </div><!-- /.box-header -->
            <div class="box-body">
               <div class="box-group" id="accordion">
                  <!--service info-->
                  <div class="panel box box-info" style="padding-bottom:1px; border-top-color:<?php echo $accRGB; ?>;">
                     <div class="box-header">
                        <h4 class="box-title">
                           <a data-toggle="collapse" data-parent="#accordion" href="#collapse1" onclick="window.scrollTo(0, 200);">
                              Service Information
                           </a>
                        </h4>
                     </div>
                     <div id="collapse1" class="panel-collapse collapse <?php echo $tog1; ?>">
                        <div class="box-body" style="margin-top:-30px;">
                           <form action="updateservice.php" method="get" id="updateAccount" class="sidebar-form" style="border:0px;margin-bottom:0px;">
                              <br>
                              <div class="callout callout-info" style="margin-bottom:-0px;">
                                 <h4>Service Details</h4>
                                 <p>In this section you can update all service related information.
                                 </p>
                              </div>

                              <?php
                              if (isset($funds['1'])) {
                                 $fn = $funds['1'] / 10000; //money_format('%=*^-14#8.2i', $funds['1']);
                                 $fn = number_format($fn, 2, '.', ' ');
                                 if ($funds['1'] < 10000000 && $fn > 0) {
                                    echo '<div class="callout callout-warning" style="margin-bottom:5px;">';
                                    echo '<div style="width:80%;">';
                                    echo '<b>Alert!</b> This balance is running low!<br><br>';
                                    echo "<b>Current Balance:&nbsp;&nbsp;</b><span class='label label-warning'>" . $fn;
                                    echo '</div>';
                                    echo '<div style="float:right; margin-top:-40px">';
                                    echo '<i class="fa fa-exclamation-triangle" style="color:#d1ba8a; font-size:20pt; margin-right:10px;"></i>';
                                    echo '</div>';
                                    echo '</div>';
                                 } elseif (substr($funds['1'], 0, 1) != '-' && $fn != 0) {
                                    echo '<div class="callout callout-success" style="margin-bottom:5px;">';
                                    echo '<div style="width:80%;">';
                                    echo "<b>Current Balance:&nbsp;&nbsp;</b><span class='label label-success'>" . $fn;
                                    echo '</div>';
                                    echo '<div style="float:right; margin-top:-24px">';
                                    echo '<i class="fa fa-thumbs-up" style="color:#5cb157; font-size:20pt; margin-right:10px;"></i>';
                                    echo '</div>';
                                    echo '</div>';
                                 } elseif ($fn == 0) {
                                    echo '<div class="callout callout-danger" style="margin-bottom:5px;">';
                                    echo '<div style="width:80%;">';
                                    echo '<b>Alert!</b> This balance is zero!!<br><br>';
                                    echo "<b>Current Balance:&nbsp;&nbsp;</b><span class='label label-success'>" . $fn;
                                    echo '</div>';
                                    echo '<div style="float:right; margin-top:-40px">';
                                    echo '<i class="fa fa-exclamation-triangle" style="color:#c99b9d; font-size:20pt; margin-right:10px;"></i>';
                                    echo '</div>';
                                    echo '</div>';
                                 } else {
                                    echo '<div class="callout callout-danger" style="margin-bottom:5px;">';
                                    echo '<div style="width:80%;">';
                                    echo '<b>Alert!</b> This balance is negative!!!<br><br>';
                                    echo "<b>Current Balance:&nbsp;&nbsp;</b><span class='label label-danger'>" . $fn;
                                    echo '</div>';
                                    echo '<div style="float:right; margin-top:-40px">';
                                    echo '<i class="fa fa-exclamation-triangle" style="color:#c99b9d; font-size:20pt; margin-right:10px;"></i>';
                                    echo '</div>';
                                    echo '</div>';
                                 }
                              } else {
                                 echo '<div class="callout callout-danger">';
                                 echo '<div style="width:80%;">';
                                 echo '<b>Alert!</b> This balance is zero!!<br><br>';
                                 if (isset($fn)) {
                                    echo "<b>Current Balance:&nbsp;&nbsp;</b><span class='label label-success'>" . $fn;
                                 } else {
                                    echo "<b>Current Balance:&nbsp;&nbsp;</b><span class='label label-success'>" . 'No Data Found...';
                                 }
                                 echo '</div>';
                                 echo '<div style="float:right; margin-top:-40px">';
                                 echo '<i class="fa fa-exclamation-triangle" style="color:#dFb5b4; font-size:20pt; margin-right:10px;"></i>';
                                 echo '</div>';
                                 echo '</div>';
                              }
                              ?>
                                            <!--div class="box box-info" style="padding-bottom:0px; border-top-color:<?php //echo $accRGB;     ?>;"-->
                              <div class="box-body" style="padding-bottom:1px;">
                                 <?php
                                 foreach ($serviceData as $key => $value) {
                                    /* echo '<pre>';
                                      print_r($value);
                                      echo '</pre>'; */

                                    $a[0] = $value['service_notification_threshold'];
                                    $a[1] = $value['service_notification_email'];
                                    $a[2] = $value['service_notification_sms'];

                                    $urls['dlrLegacyUrl'] = $value['DLR_ADDRESS_LEGACY'];
                                    $urls['dlrLegacyStatus'] = $value['DLR_STATUS_LEGACY'];
                                    $urls['moLegacyUrl'] = $value['MO_ADDRESS_LEGACY'];
                                    $urls['moLegacyStatus'] = $value['MO_STATUS_LEGACY'];
                                    $urls['smppStatus'] = $value['SMPP'];

                                    echo '<p>';
                                    echo "<b>Account Name:</b> " . $value['account_name'];
                                    echo '</p>';

                                    echo '<p>';
                                    echo "<b>Service Login:</b> " . $value['service_login'];
                                    echo '</p>';

                                    echo '<b>Service Name:</b>';
                                    echo '<p>';
                                    echo '<input type="text" id="servNa" class="form-control" style="border:1px solid #929292;margin-top:5px;height:35px;" name="servName" placeholder="' . $value['service_name'] . '" value="' . $value['service_name'] . '">';
                                    echo '</p>';

//                                    echo '<p>';
//                                    echo '<b>Service Manager:</b>';
//                                    echo '<select disabled class="form-control" name="chman" form="updateAccount" style="border-color:#929292;margin-top:5px;">';
//                                    if (isset($value['user_fullname'])) {
//                                       echo "<option name='accStatus' SELECTED>" . $value['user_fullname'] . "</option>";
//                                    }
//                                    if (isset($managers)) {
//                                       foreach ($managers as $key2 => $value2) {
//                                          $value['user_fullname'] = strrpos($value2['user_username'], '.');
//                                          $value['user_fullname'] = substr($value2['user_username'], $value['user_fullname'] + 1);
//                                          if ($value['user_fullname'] == "connet" || $value['user_fullname'] == "CONNET") {
//                                             echo "<option name='accStatus'>" . $value2['user_username'] . "</option>";
//                                          }
//                                       }
//                                    }
//                                    echo '</select>';
//                                    echo '</p>';

                                    echo '<label>Status:</label>';
                                    echo '<p>';

                                    if ($value['service_status'] == "ENABLED") {
                                       $cAccStat = '#33cc33;';
                                    } else {
                                       $cAccStat = '#cc3333;';
                                    }

                                    echo '<select class="form-control" name="accStatus" form="updateAccount" style="border-color:' . $cAccStat . ' ">';
                                    echo "<option name='accStatus' SELECTED>" . $value['service_status'] . "</option>";
                                    if ($value['service_status'] == "ENABLED") {
                                       echo "<option name='accStatus'>DISABLED</option>";
                                    } else {
                                       echo "<option name='accStatus'>ENABLED</option>";
                                    }
                                    echo '</select>';
                                    echo '</p>';


                                    echo '<label>Payment Type:</label>';
                                    echo '<p>';

                                    //hacky way to prevent reseller from modifying his own account type
                                    if (isset($_SESSION['resellerId']) && $service_id == $_SESSION['resellerId']) {
                                       //reseller cant change his own service type
                                       $disable_service_type_change = true;
                                    } else {
                                       $disable_service_type_change = false;
                                    }

                                    echo '<select class="form-control" name="paymentType" form="updateAccount" style="border-color:#929292;">';
                                    if ($_SESSION['serviceBillingType'] == "PREPAID") {
                                       echo '<option name="PREPAID">PREPAID</option>';
                                    } else {
                                       echo "<option name='" . $value['service_credit_billing_type'] . "'>" . $value['service_credit_billing_type'] . "</option>";
                                       if (!$disable_service_type_change) {
                                          if ($value['service_credit_billing_type'] == "PREPAID") {
                                             echo "<option>POSTPAID</option>";
                                          } else {
                                             echo "<option>PREPAID</option>";
                                          }
                                       }
                                    }
                                    echo '</select>';
                                    echo '</p>';
                                 }
                                 ?>
                                 <input type="hidden" name="service_id" value="<?php echo $service_id; ?>">
                                 <input type="hidden" name="updateClientInfo" value="1">
                              </div><!-- /.box-body -->
                              <div class="box-footer" ID="clientInfoBoxFooter" style="border-top:0px;padding-top:0px;padding-bottom:0px; margin-bottom:0px;
                              <?php
                              if ($togs1 == 'success') {
                                 echo 'height:74px;';
                              } elseif ($togs1 == 'fail') {
                                 echo 'height:74px;';
                              } else {
                                 echo 'height:35px;';
                              }
                              if (!isset($changes)) {
                                 echo 'height:35px;';
                              }
                              ?>">
                                 <!--document.getElementById('updateRoutes').submit();-->
                                 <a class="btn btn-block btn-social btn-dropbox " onclick="document.getElementById('updateAccount').submit();" style="background-color: <?php echo $accRGB; ?>; color: #ffffff; border: 2px solid; border-radius: 6px !important; float:left; width: 167px; height:36px; margin-top:0px;">
                                    <i class="fa fa-save"></i>Update Client Info
                                 </a>

                                 <?php
                                 if (isset($changes)) {
                                    if ($togs1 == 'success' && $changes != "") {
                                       echo '<div style="width:80%;padding-top:44px;">';
                                       echo '</div>';
                                       echo '<div id="clientSuccess" class="callout callout-success">';
                                       //echo "<b>You have successfully changed:&nbsp;&nbsp;</b><span class='label label-success'>".$changes;
                                       echo "You have successfully changed:&nbsp;&nbsp;<b>" . $changes . "</b>";
                                       echo '<div style="float:right; margin-top:-3px">';
                                       echo '<i class="fa fa-check" style="color:#5cb157; font-size:20pt;"></i>';
                                       echo '</div>';
                                       echo '</div>';
                                    } elseif ($togs1 == 'success' && $changes == "") {
                                       echo '<div style="width:80%;padding-top:44px;">';
                                       echo '</div>';
                                       echo '<div id="clientWarning" class="callout callout-warning" >';
                                       echo "<b>No changes were made...&nbsp;&nbsp;</b>";
                                       echo '<div style="float:right; margin-top:-3px">';
                                       echo '<i class="fa fa-exclamation-triangle" style="color:#f1e7bc; font-size:20pt; margin-right:10px;"></i>';
                                       echo '</div>';
                                       echo '</div>';
                                    } elseif ($togs1 == 'fail' && $changes != "") {
                                       echo '<div style="width:80%;padding-top:44px;">';
                                       echo '</div>';
                                       echo '<div class="callout callout-danger">';
                                       echo "<b>Error:&nbsp;" . $changes . "&nbsp;</b>";
                                       echo '<div style="float:right; margin-top:-3px">';
                                       echo '<i class="fa fa-exclamation-triangle" style="color:#dFb5b4; font-size:20pt;"></i>';
                                       echo '</div>';
                                       echo '</div>';
                                    }
                                 }
                                 ?>
                              </div>
                           </form>
                        </div><!-- /.box-body -->
                     </div>
                  </div>
                  <!--routes-->
                  <div class="panel box box-warning" style="padding-bottom:1px; border-top-color:<?php echo $accRGB; ?>;">
                     <div class="box-header">
                        <h4 class="box-title">
                           <a data-toggle="collapse" data-parent="#accordion" href="#collapse2" onclick="window.scrollTo(0, 200);">
                              Routes
                           </a>
                        </h4>
                     </div>
                     <div id="collapse2" class="panel-collapse collapse <?php echo $tog2; ?>">
                        <form action="updateservice.php?tog2=in" method="post" id="updateRoutes" class="sidebar-form" style="border:0px;" >
                           <div class="callout callout-info" style='height:auto;margin-bottom:-0px;'>
                              <div style="width:100%;height:auto;" >
                                 <h4>Routes Details</h4>
                                 <p>In this section you can view the SMS routes assigned to your service. Routes are displayed per Country, Network, Status and Rate. You can update the price per network in the Rate textbox.</p><p>Please note that 0.15c will be displayed as 0.1500, rates are displayed in 10 000 value.</p>
                                 <!--a class="btn btn-block btn-social btn-dropbox " style="background-color: #367fa9; color: #ffffff; border: 2px solid; border-radius: 6px !important; width: 200px;" onclick="toggleContent('addRoute');">
                                     <i class="fa fa-link"></i>Add New Routes
                                 </a-->
                              </div>
                           </div>

                                        <!--div class="box box-warning" style="padding-bottom:1px; border-top-color:<?php echo $accRGB; ?>;"-->
                           <div class="box-body" style="padding-bottom:0px;">
                              <div class="box-body table-responsive no-padding">
                                 <table id="routesTable" class="table table-bordered table-striped">
                                    <thead>
                                       <tr role="row">
                                          <th class="sorting_asc" role="columnheader" tabindex="0" aria-controls="routesTable" rowspan="1" colspan="1" aria-sort="ascending" aria-label="Country: activate to sort column descending">Country</th>
                                          <th>Network</th>
                                          <th>Status</th>
                                          <th style="text-align:center;">Rate</th>
                                          <?php
                                          if (LoginHelper::isSystemAdmin()) {
                                             echo '<th style="text-align:center;">RDNC</th>';
                                          }
                                          ?>
                                          <th style="text-align:center;">Cur</th>
                                          <?php
                                          if (LoginHelper::isSystemAdmin()) {
                                             echo '<th>Collection</th>';
                                             echo '<th>Default</th>';
                                             echo '<th>Ported</th>';
                                          }
                                          ?>
                                       </tr>
                                    </thead>
                                    <tbody>
                                       <?php
                                       if (isset($defaultRouteData)) {
                                          foreach ($defaultRouteData as $key => $value) {
                                             //see if we can get the default route cost for the resller, so we can create an alert for routes that are less than that price
                                             if (isset($_SESSION['resellerId']) && isset($value['MCCMNC'])) {
                                                $reseller_route_info = getRouteCostingForResellerWithMCCMNC($_SESSION['resellerId'], $value['MCCMNC']);

                                                $default_route_cost = explode(',', $reseller_route_info['route_billing'])[0];
                                             } else {
                                                $default_route_cost = 0;
                                             }

                                             if (substr($value['COUNTRY'], 0, 3) == "USA") {
                                                $flag = "USA";
                                             } else {
                                                $flag = $value['COUNTRY'];
                                             }
                                             $flag = str_replace('/', '&', $flag);

                                             $s1 = strrpos($value['NETWORK'], '/');
                                             $s2 = strrpos($value['NETWORK'], '(');

                                             $logo = substr($value['NETWORK'], $s1, $s2);
                                             $logo = trim($logo);

                                             if ($logo != "Cell C") {  // | $logo != 'Vodacom SA' | $logo != 'MTN-SA'
                                                if ($logo != 'Vodacom SA') {
                                                   if ($logo != 'MTN-SA') {
                                                      if ($logo != 'Telkom Mobile') {
                                                         if ($logo != 'MTN (Nigeria)') {
                                                            $logo = substr($value['NETWORK'], $s1 + 1, $s2 - $s1 - 2);
                                                            $logo = trim($logo);
                                                         }
                                                      }
                                                   }
                                                }
                                             }

                                             echo '<tr >';
                                             echo '<td style="vertical-align:middle;"><img src="../img/flags/' . $flag . '.png">&nbsp;&nbsp;' . $value['COUNTRY'] . '</td>';
                                             if (isset($default_route_cost) && $default_route_cost != 0) {
                                                echo '<td style="vertical-align:middle;"><img src="../img/serviceproviders/' . $logo . '.png">&nbsp;&nbsp;' . $value['NETWORK'] . ' <small class="pull-right">[Your default rate: ' . $default_route_cost . ']</small></td>';
                                             } else {
                                                echo '<td style="vertical-align:middle;"><img src="../img/serviceproviders/' . $logo . '.png">&nbsp;&nbsp;' . $value['NETWORK'] . '</td>';
                                             }
                                             if ($value['STATUS'] == 'ENABLED') {
                                                echo '<td style="vertical-align:middle;"><i class="fa fa-check" style="color:#00ff00;"></i>&nbsp;&nbsp;' . $value['STATUS'] . '</td>';
                                             } else {
                                                echo '<td style="vertical-align:middle;"><i class="fa fa-times" style="color:#ff0000;"></i>&nbsp;&nbsp;' . $value['STATUS'] . '</td>';
                                             }
                                             $rateV = strstr($value['RATE'], ',', true);
                                             $rdncVp = strrpos($value['RATE'], ',');
                                             $rdncV = substr($value['RATE'], $rdncVp + 1);
                                             if ($rdncVp == '') {
                                                $rateV = $value['RATE'];
                                                $rdncV = '0';
                                             }
                                             if (strlen($rateV) < 4) {
                                                $rateVn = '';
                                                for ($c = 0; $c < (4 - strlen($rateV)); $c++) {
                                                   $rateVn .= '0';
                                                }
                                                $rateV = $rateVn . $rateV;
                                             }
                                             if (strlen($rdncV) < 4) {
                                                $rdncVn = '';
                                                for ($c = 0; $c < (4 - strlen($rdncV)); $c++) {
                                                   $rdncVn .= '0';
                                                }
                                                $rdncV = $rdncVn . $rdncV;
                                             }

                                             echo '<td style="vertical-align:middle;"><center><div class="fakey" style="padding-left:5px;width:60px;"><div style="display: inline-table;">0.</div><input type="text" onkeypress="return isNumberKey(event)" name="routeRate' . ($key + 1) . '" id="routeRDNC' . ($key + 1) . '" placeholder="" style="background:#ffffff;width:40px;border-color:#929292;border:0px;" maxlength="4" value="' . $rateV . '"></div></center></td>';
                                             if (LoginHelper::isSystemAdmin()) {
                                                echo '<td style="vertical-align:middle;"><center><div class="fakey" style="padding-left:5px;width:60px;"><div style="display: inline-table;">0.</div><input type="text" onkeypress="return isNumberKey(event)" name="routeRDNC' . ($key + 1) . '" id="routeRDNC' . ($key + 1) . '" placeholder="" style="background:#ffffff;width:40px;border-color:#929292;border:0px;" maxlength="4" value="' . $rdncV . '"></div></center></td>';
                                             } else {
                                                echo '<input type="hidden" name="routeRDNC' . ($key + 1) . '" value="' . $rdncV . '"/>';
                                             }
                                             echo '<input type="hidden" id="routeDefaultResellerCost' . ($key + 1) . '" name="routeDefaultResellerCost' . ($key + 1) . '" value="' . $default_route_cost . '"/>';

                                             if ($value['route_currency'] == 'ZAR') {
                                                echo '<td style="text-align:center;">
                                                                                  <select name="routeCur' . ($key + 1) . '"><option name="cur">' . $value['route_currency'] . '</option><option name="cur">EUR</option></select>
                                                                                  </td>';
                                             } else {
                                                echo '<td style="text-align:center;">
                                                                                  <select name="routeCur' . ($key + 1) . '"><option name="cur">' . $value['route_currency'] . '</option><option name="cur">ZAR</option></select>
                                                                                  </td>';
                                             }

                                             if (LoginHelper::isSystemAdmin()) {
                                                echo '<td style="vertical-align:middle;">' . $value['COLLECTION'] . '</td>';

                                                $mccNetSubCont = false;
                                                foreach ($serProviders as $key13 => $value13) {
                                                   $imageT = strstr($value13, '.', true);

                                                   if ($imageT == $value['DEFAULT']) {
                                                      $mccNetSubCont = $imageT;
                                                      break;
                                                   }
                                                }

                                                if ($mccNetSubCont != false) {
                                                   echo '<td style="vertical-align:middle"><img src="../img/routes/' . $value['DEFAULT'] . '.png">&nbsp;&nbsp;' . $value['DEFAULT'] . '</td>';
                                                } else {
                                                   echo '<td style="vertical-align:middle;">-' . $value['DEFAULT'] . '-</td>';
                                                }

                                                $mccNetSubCont2 = false;
                                                foreach ($serProviders as $key14 => $value14) {
                                                   $imageT2 = strstr($value14, '.', true);

                                                   if ($imageT2 == $value['PORTED']) {
                                                      $mccNetSubCont2 = $imageT2;
                                                      break;
                                                   }
                                                }

                                                if ($mccNetSubCont2 != false) {
                                                   echo '<td style="vertical-align:middle"><img src="../img/routes/' . $value['PORTED'] . '.png">&nbsp;&nbsp;' . $value['PORTED'] . '</td>';
                                                } else {
                                                   echo '<td style="vertical-align:middle;">' . $value['PORTED'] . '</td>';
                                                }
                                             }

                                             echo '<input type="hidden" name="routeId' . ($key + 1) . '" value="' . $value['route_id'] . '">';
                                             echo '</tr>';
                                             $routeCount++;
                                          }
                                       }
                                       ?>
                                    </tbody>
                                 </table>
                              </div><!-- /.box-body -->
                              <input type="hidden" name="service_id" value="<?php echo $service_id; ?>">
                              <input type="hidden" name="updateRoutes" value="1">
                              <input type="hidden" name="routeCount" id="routeCount" value="<?php echo $routeCount; ?>">
                              <input type="hidden" name="serviceBillingType" id="serviceBillingType" value="<?php echo $_SESSION['serviceBillingType']; ?>">

                           </div>
                           <div class="box-footer" style="border-top:0px;padding-top:0px;padding-bottom:2px;margin-bottom:0px;
                           <?php
                           if ($togs2 == 'success') {
                              echo 'height:74px;';
                           } else {
                              echo 'height:35px;';
                           }
                           ?>"
                                >
                              <a class="btn btn-block btn-social btn-dropbox" onclick="validateRoutePrices();" style="background-color: <?php echo $accRGB; ?>; color: #ffffff; border: 2px solid; border-radius:6px !important; float:left; width: 175px; height:36px;">
                                 <i class="fa fa-save"></i>Update Routes Info
                              </a>
                              <?php
                              if ($togs2 == 'success' && $changes != "") {
                                 echo '<div style="width:80%;margin-top:0px;padding-top:44px;">';
                                 echo '</div>';
                                 echo '<div class="callout callout-success">';
                                 echo "You have successfully changed route pricing:&nbsp;&nbsp;<b>" . $changes . "</b>";
                                 echo '<div style="float:right; margin-top:-3px">';
                                 echo '<i class="fa fa-check" style="color:#5cb157; font-size:20pt; margin-right:10px;"></i>';
                                 echo '</div>';
                                 echo '</div>';
                              } elseif ($togs2 == 'success' && $changes == "") {
                                 echo '<div style="width:80%;margin-top:0px;padding-top:44px;">';
                                 echo '</div>';
                                 echo '<div class="callout callout-warning" style="margin-bottom:10px;">';
                                 echo "<b>No changes were made...&nbsp;&nbsp;</b>";
                                 echo '<div style="float:right; margin-top:-3px">';
                                 echo '<i class="fa fa-exclamation-triangle" style="color:#f1e7bc; font-size:20pt; margin-right:10px;"></i>';
                                 echo '</div>';
                                 echo '</div>';
                              }
                              ?>
                           </div>
                           <!--/div-->
                        </form>
                     </div>
                  </div>
                  <!--low balance alerts-->
                  <div class="panel box box-danger" style="padding-bottom:1px; border-top-color:<?php echo $accRGB; ?>;">
                     <div class="box-header">
                        <h4 class="box-title">
                           <a data-toggle="collapse" data-parent="#accordion" href="#collapse5" onclick="window.scrollTo(0, 200);">
                              Low Balance Alerts
                           </a>
                        </h4>
                     </div>
                     <div id="collapse5" class="panel-collapse collapse <?php echo $tog5; ?>">
                        <form action="updateservice.php" method="get" id="updateAlerts" class="sidebar-form" role="form" style="border:0px;" >
                           <div class="callout callout-info" style="margin-bottom:0px;">
                              <h4>Low Balance Alerts Notifications</h4>
                              <p>In this section, you can specify the threshold limit on your service.</p>
                              <p>Notification threshold is based on the monetary value left on the account. Enter the currency amount in the threshold field below.</p>
                           </div>
                           <?php
                           if (LoginHelper::isResellerService()) {
                              echo '<div class="callout callout-danger" style="margin-bottom:0px;">';
                              echo '<h4>You are a re-seller</h4>';
                              echo '<p>The Connet Systems&trade; logo and the Connet Systems&trade; email address will appear on the low balance notification email.</p>';
                              echo '<p>Please ensure you specify the correct email address to receive the notification.</p>';
                              echo '</div>';
                           }
                           ?>
                           <!--div class="box box-danger" style="padding-bottom:1px; border-top-color:<?php echo $accRGB; ?>;"-->
                           <div class="box-body" style="padding-bottom:0px;">
                              <div class="box-body">
                                 <p><b>Notification Threshold / Limit in Currency:</b> ( 1000 )</p>
                                 <input type="text" class="form-control" name="thresholdLimit" placeholder="" value="<?php
                                 if (isset($a[0])) {
                                    echo $a[0] / 10000;
                                 } else {
                                    echo '';
                                 }
                                 ?>" style="border:1px solid #aaaaaa;" >
                                 <br>
                                 <p><b>Notification Email:</b> ( john@smit.com, jane@tims.co.za )</p>
                                 <input type="text" class="form-control" name="emailAddresses" placeholder="" value="<?php
                                 if (isset($a[1])) {
                                    echo $a[1];
                                 } else {
                                    echo '';
                                 }
                                 ?>" style="border:1px solid #aaaaaa;">
                                 <!--br>
                                     <p><b>Notification SMS Number:</b> ( 0821234567, 27837654321 )</p>
                                     <input type="text" class="form-control" id="inputWarning" name="smsNumbers" placeholder="" value="<?php
                                 if (isset($a[2])) {
                                    echo $a[2];
                                 } else {
                                    echo '';
                                 }
                                 ?>" style="border:1px solid #aaaaaa;"-->
                                 <!--input type="text" class="form-control" id="inputWarning" placeholder="Enter ..." style="border:1px solid #aaaaaa;" -->
                                 <input type="hidden" name="service_id" value="<?php echo $service_id; ?>">
                                 <input type="hidden" name="updateLowBalance" value="1">

                              </div>
                           </div>
                           <div class="box-footer" style="padding-top:0px;border-top:0px;margin-bottom:0px;
                           <?php
                           if ($togs5 == 'success') {
                              echo 'height:115px;';
                           } else {
                              echo 'padding-bottom:35px;';
                           }
                           ?>">
                              <p>
                                 <a class="btn btn-block btn-social btn-dropbox " onclick="document.getElementById('updateAlerts').submit();" style="background-color: <?php echo $accRGB; ?>; color: #ffffff; border: 2px solid; border-radius: 6px !important; float:left; width: 160px; height:36px;">
                                    <i class="fa fa-save"></i>Update Alert Info
                                 </a>

                                 <a class="btn btn-block btn-social btn-dropbox " onclick="deleteLBN(<?php echo $service_id; ?>);" style="margin-left:10px;margin-top:0px;background-color: <?php echo $accRGB; ?>; color: #ffffff; border: 2px solid; border-radius: 6px !important; float:left; width: 140px; height:36px;">
                                    <i class="fa fa-trash" style='color:#ff0000'></i>Delete Info
                                 </a>
                              </p>
                              <?php
                              if ($togs5 == 'success' && $changes == "del") {
                                 echo '<div style="width:80%;padding-top:44px;">';
                                 echo '</div>';
                                 echo '<div class="callout callout-success">';
                                 echo "You have successfully deleted low balance notifications</b>";
                                 echo '<div style="float:right; margin-top:-3px">';
                                 echo '<i class="fa fa-check" style="color:#5cb157; font-size:20pt; margin-right:10px;"></i>';
                                 echo '</div>';
                                 echo '</div>';
                              } elseif ($togs5 == 'success' && $changes != "") {
                                 echo '<div style="width:80%;padding-top:44px;">';
                                 echo '</div>';
                                 echo '<div class="callout callout-success">';
                                 echo "You have successfully changed:&nbsp;&nbsp;<b>" . $changes . "</b>";
                                 echo '<div style="float:right; margin-top:-3px">';
                                 echo '<i class="fa fa-check" style="color:#5cb157; font-size:20pt; margin-right:10px;"></i>';
                                 echo '</div>';
                                 echo '</div>';
                              } elseif ($togs5 == 'success' && $changes == "") {
                                 echo '<div style="width:80%;padding-top:44px;">';
                                 echo '</div>';
                                 echo '<div class="callout callout-warning">';
                                 echo "<b>No changes were made...&nbsp;&nbsp;</b>";
                                 echo '<div style="float:right; margin-top:-3px">';
                                 echo '<i class="fa fa-exclamation-triangle" style="color:#f1e7bc; font-size:20pt; margin-right:10px;"></i>';
                                 echo '</div>';
                                 echo '</div>';
                              }
                              ?>
                           </div>
                           <!--/div-->
                        </form>
                     </div>
                  </div>
                  <!--urls-->
                  <div class="panel box box-info" style="padding-bottom:1px; border-top-color:<?php echo $accRGB; ?>;">
                     <div class="box-header">
                        <h4 class="box-title">
                           <a data-toggle="collapse" data-parent="#accordion" href="#collapse6" onclick="window.scrollTo(0, 200);">
                              DLR Responses and MO SMS URL's</a>
                        </h4>
                     </div>
                     <div id="collapse6" class="panel-collapse collapse <?php echo $tog6; ?>" style="padding-bottom:10px;">
                        <form action="updateservice.php" method="get" id="updateURLs" class="sidebar-form" style="border:0px;" >
                           <div class="callout callout-info" style="margin-bottom:0px;">
                              <h4>DLR and MOSMS Details</h4>
                              <p>In this section you can update the delivery responses and reply SMS URLs. You will receive daily delivery responses and reply SMSs via the API on the URLs, which are specified in the section below.</p>
                           </div>

                           <!--1st DLR box-->
                           <!--div class="box box-info" style="padding-bottom:1px; border-top-color:<?php echo $accRGB; ?>;"-->
                           <div class="box-body" style="padding-bottom:0px;">
                              <?php
                              echo '<b><p>DLR URL (Legacy / HTTP):</p></b>';

                              if (isset($urls['dlrLegacyUrl'])) {
                                 $dl = $urls['dlrLegacyUrl'];
                              } else {
                                 $dl = '';
                              }
                              echo '<input type="text" class="form-control" name="dlrUrlHTTP" style="margin-bottom:10px;border:1px solid #aaaaaa;" placeholder="Add HTTP URL here..." value="' . $dl . '">';
                              echo '<b><p>DLR STATUS (Legacy / HTTP):</p></b>';

                              if (isset($urls['dlrLegacyStatus']) && ($urls['dlrLegacyStatus'] == "ENABLED")) {
                                 $c1 = '#33cc33;';
                              } else {
                                 $c1 = '#cc3333;';
                              }
                              echo '<select class="form-control" name="dlrStatusHTTP" style="margin-bottom:10px;border-color:' . $c1 . ' " >';
                              if (isset($urls['dlrLegacyStatus'])) {
                                 echo '<option>' . $urls['dlrLegacyStatus'] . '</option>';
                                 if ($urls['dlrLegacyStatus'] == "ENABLED") {
                                    echo "<option>DISABLED</option>";
                                 } else {
                                    echo "<option>ENABLED</option>";
                                 }
                              } else {
                                 echo "<option>ENABLED</option>";
                                 echo "<option SELECTED>DISABLED</option>";
                              }
                              echo '</select>';
                              ?>
                           </div>
                           <!--/div-->

                           <!--2nd MO box-->
                           <!--div class="box box-info" style="padding-bottom:1px; border-top-color:<?php echo $accRGB; ?>;"-->
                           <div class="box-body">
                              <?php
                              echo '<b><p>MOSMS URL (Legacy / HTTP):</p></b>';
                              if (isset($urls['moLegacyUrl'])) {
                                 $moU = $urls['moLegacyUrl'];
                              } else {
                                 $moU = '';
                              }
                              echo '<input type="text" class="form-control" name="mosmsUrlHTTP" style="margin-bottom:10px;border:1px solid #aaaaaa;" placeholder="Add HTTP URL here..." value="' . $moU . '">';
                              echo '<b><p>MOSMS STATUS (Legacy / HTTP):</p></b>';

                              if (isset($urls['moLegacyStatus']) && $urls['moLegacyStatus'] == "ENABLED") {
                                 $c2 = '#33cc33;';
                              } else {
                                 $c2 = '#cc3333;';
                              }
                              echo '<select class="form-control" name="mosmsStatusHTTP" style="border-color:' . $c2 . '" >';
                              if (isset($urls['moLegacyStatus'])) {
                                 echo '<option>' . $urls['moLegacyStatus'] . '</option>';
                                 if ($urls['moLegacyStatus'] == "ENABLED") {
                                    echo "<option>DISABLED</option>";
                                 } else {
                                    echo "<option>ENABLED</option>";
                                 }
                              } else {
                                 echo "<option>ENABLED</option>";
                                 echo "<option SELECTED>DISABLED</option>";
                              }
                              echo '</select>';
                              ?>
                           </div>
                           <!--/div-->

                           <!--3rd SMPP STATUS box-->
                           <!--div class="box box-info" style="padding-bottom:1px; border-top-color:<?php echo $accRGB; ?>;"-->
                           <div class="box-body">
                              <b><p>SMPP STATUS:</p></b>
                              <?php
                              if (isset($urls['smppStatus'])) {
                                 if ($urls['smppStatus'] == "ENABLED") {
                                    $c3 = '#33cc33;';
                                 } else {
                                    $c3 = '#cc3333;';
                                 }
                              }
                              ?>
                              <select class="form-control" name="StatusSMPP" style="border-color:<?php
                              if (isset($c3)) {
                                 echo $c3;
                              }
                              ?>">
                                         <?php
                                         if (isset($urls['smppStatus'])) {
                                            echo '<option>' . $urls['smppStatus'] . '</option>';
                                            if ($urls['smppStatus'] == "ENABLED") {
                                               echo "<option>DISABLED</option>";
                                            } else {
                                               echo "<option>ENABLED</option>";
                                            }
                                         } else {
                                            echo "<option>ENABLED</option>";
                                            echo "<option SELECTED>DISABLED</option>";
                                         }
                                         ?>
                              </select>
                           </div>
                           <!--/div-->
                           <div class="box-footer" style="border-top:0px; padding-top:0px; padding-bottom:0px; margin-bottom:-20px;
                           <?php
                           if ($togs6 == 'success') {
                              echo 'height:84px;';
                           } else {
                              echo 'padding-bottom:44px;';
                           }
                           ?>
                                ">
                              <input type="hidden" name="service_id" value="<?php echo $service_id; ?>">
                              <input type="hidden" name="updateURL" value="1">
                              <a class="btn btn-block btn-social btn-dropbox " onclick="document.getElementById('updateURLs').submit();" style="background-color: <?php echo $accRGB; ?>; color: #ffffff; border: 2px solid; border-radius: 6px !important; float:left; width: 160px; height:36px;">
                                 <i class="fa fa-save"></i>Update URL Info
                              </a>
                              <?php
                              if ($togs6 == 'success' && $changes != "") {
                                 if ($changes == '-2') {
                                    echo '<div style="width:80%;padding-top:44px;">';
                                    echo '</div>';
                                    echo '<div class="callout callout-danger">';
                                    echo "Alert:<b>&nbsp;'" . $_GET['err6'] . "'&nbsp;</b>URL is already in use!";
                                    echo '<div style="float:right; margin-top:-3px">';
                                    echo '<i class="fa fa-times" style="color:#cc3333; font-size:20pt;"></i>';
                                    echo '</div>';
                                    echo '</div>';
                                 } else {
                                    echo '<div style="width:80%;padding-top:44px;">';
                                    echo '</div>';
                                    echo '<div class="callout callout-success">';
                                    echo "You have successfully changed:&nbsp;&nbsp;<b>" . $changes . "</b>";
                                    echo '<div style="float:right; margin-top:-3px">';
                                    echo '<i class="fa fa-check" style="color:#5cb157; font-size:20pt;"></i>';
                                    echo '</div>';
                                    echo '</div>';
                                 }
                              } elseif ($togs6 == 'success' && $changes == "") {
                                 echo '<div style="width:80%;padding-top:44px;">';
                                 echo '</div>';
                                 echo '<div class="callout callout-warning">';
                                 echo "<b>No changes were made...&nbsp;&nbsp;</b>";
                                 echo '<div style="float:right; margin-top:-3px">';
                                 echo '<i class="fa fa-exclamation-triangle" style="color:#f1e7bc; font-size:20pt;"></i>';
                                 echo '</div>';
                                 echo '</div>';
                              }
                              ?>
                           </div>
                        </form>
                     </div>
                  </div>
                  <!--users-->
                  <div class="panel box box-connet" style="padding-bottom:1px; border-top-color:<?php echo $accRGB; ?>;">
                     <div class="box-header">
                        <h4 class="box-title">
                           <a data-toggle="collapse" data-parent="#accordion" href="#collapse3" onclick="window.scrollTo(0, 200);">
                              Users
                           </a>
                        </h4>
                     </div>
                     <div id="collapse3" class="panel-collapse collapse <?php echo $tog3; ?>">
                        <div class="callout callout-info" style='margin-left:10px; margin-right:10px; margin-top:10px; height:auto; margin-bottom:0px;'>
                           <div style="width:80%;">
                              <h4>Users Details</h4>
                              <p>In this section, you can view all users that have login access to your service.</p>
                              <p>You can update usernames, passwords and disable prior users from gaining access to your service.</p>
                           </div>
                        </div>
                        <div class="callout callout-warning" style='margin-left:10px; margin-right:10px; margin-top:0px; height:auto; margin-bottom:0px;'>
                           <div style="width:80%;">
                              <p>
                                 The user highlighted in orange is the main API user.
                                 <br>If you change this user --YOU MUST NOTIFY THE CLIENT-- of your changes so they can make the necessary changes in their API call/code.

                              </p>
                           </div>
                           <div style="float:right; margin-top:-32px"><i class="fa fa-warning" style="color:#d1ba8a; font-size:20pt; margin-right:10px;"></i></div>
                        </div>

                        <div class="box-body">
                            <!--div class="box box-connet" style="padding-bottom:1px; border-top-color:<?php echo $accRGB; ?>;"-->
                           <div class="box-body table-responsive no-padding">
                              <table ID="userContent" style="display:none;">
                                 <tr style="margin:10px;">
                                 <form action="updateservice.php" method="get" id="addUsers" class="sidebar-form" style="border:0px;">
                                    <td style='width:562px;'>
                                       <select class="form-control" name="newUser" style="margin:7px;border:1px solid #aaaaaa;">
                                          <?php
                                          if (isset($allUsers)) {
                                             foreach ($allUsers as $key => $value) {
                                                if (isset($users)) {
                                                   $checker = checkUser($value['user_username'], $users);
                                                   if ($checker != true) {
                                                      echo "<option value='" . $value['user_id'] . "'>" . $value['user_username'] . "</option>";
                                                   }
                                                }
                                             }
                                          }
                                          ?>
                                       </select>
                                    </td>
                                    <td>
                                       <input type="hidden" name="cl" value="<?php echo $service_id; ?>">
                                       <input type="hidden" name="service_id" value="<?php echo $service_id; ?>">
                                       <a class="btn btn-block btn-social btn-dropbox " style="margin-top:-1px; margin-left:20px; background-color: <?php echo $accRGB; ?>; color: #ffffff; border: 2px solid; border-radius: 6px !important; width: 120px;" onclick="document.getElementById('addUsers').submit();">
                                          <i class="fa fa-plus"></i>Add User
                                       </a>
                                    </td>
                                 </form>
                                 </tr>
                              </table>
                              <table class="table table-hover">
                                 <tr>
                                    <th>Username</th>
                                    <th>Password</th>
                                    <th>Status</th>
                                    <th width="200">Actions</th>
                                 </tr>
                                 <?php
                                 if (isset($users)) {
                                    foreach ($users as $key => $value) {
                                       if ($value['user_username'] === $value['service_login']) {
                                          $rowColor = ' style="background-color:#f4e8c6;"';
                                       } else {
                                          $rowColor = '';
                                       }
                                       echo '<tr' . $rowColor . '>';
                                       echo '<td><h4>' . $value['user_username'] . '</h4></td>';
                                       echo '<td><h4>*******</h4></td>';

                                       if ($value['user_status'] == "ENABLED") {
                                          $c1b = '#33cc33;';
                                       } else {
                                          $c1b = '#cc3333;';
                                       }
                                       echo '<td><h4 style="color:' . $c1b . '">' . $value['user_status'] . '<h4></td>';
                                       echo '<td>';
                                       echo '<a class="btn btn-sm btn-primary" href="manage_users.php?user_id=' . $value['user_id'] . '&service_id=' . $service_id . '">Edit User</a> ';
                                       //NEW ALLOWS FOR A USER TO MIMIC FUNCTIONALITY
                                       if (isset($_SESSION['userSystemAdmin']) && $_SESSION['userSystemAdmin'] == 1) {
                                          echo '<button class="btn btn-sm btn-primary btnMimic" value="' . $value['user_id'] . '">Mimic This User</button>';
                                       }
                                       echo '</td>';
                                       echo '</tr>';
                                    }
                                 }
                                 ?>
                              </table>
                           </div>
                           <!--/div-->
                        </div>
                        <div class="box-footer" style="padding-top:1px;border-top:0px;margin-bottom:5px;
                        <?php
                        if ($togs3 == 'success') {
                           echo 'padding-bottom:10px;';
                        } elseif ($togs3 == 'fail') {
                           echo 'padding-bottom:10px;';
                        } else {
                           echo 'padding-bottom:40px;';
                        }
                        ?>">
                           <a class="btn btn-block btn-social btn-dropbox " onclick="document.getElementById('updateUsers').submit();" style="background-color: <?php echo $accRGB; ?>; color: #ffffff; border: 2px solid; border-radius: 6px !important; float:left; width: 170px;">
                              <i class="fa fa-save"></i>Update Users Info
                           </a>
                           <?php
                           if ($togs3 == 'success' && $changes != "") {
                              echo '<div style="width:80%;margin-top:50px;">';
                              echo '</div>';
                              echo '<div class="callout callout-success" style="margin-bottom:0px;">';
                              echo "You have successfully changed:&nbsp;&nbsp;<b>" . $changes . "</b>";
                              echo '<div style="float:right; margin-top:-3px">';
                              echo '<i class="fa fa-check" style="color:#5cb157; font-size:20pt; margin-right:10px;"></i>';
                              echo '</div>';
                              echo '</div>';
                           } elseif ($togs3 == 'success' && $changes == "") {
                              echo '<div style="width:80%;margin-top:50px;">';
                              echo '</div>';
                              echo '<div class="callout callout-warning" style="margin-bottom:0px;">';
                              echo "<b>No changes were made...&nbsp;&nbsp;</b>";
                              echo '<div style="float:right; margin-top:-3px">';
                              echo '<i class="fa fa-exclamation-triangle" style="color:#ecd48e; font-size:20pt; margin-right:10px;"></i>';
                              echo '</div>';
                              echo '</div>';
                           } elseif ($togs3 == 'fail' && $changes != "") {
                              echo '<div style="width:80%;margin-top:50px;">';
                              echo '</div>';
                              echo '<div class="callout callout-danger" style="margin-bottom:0px;">';
                              echo "<b>Error:&nbsp;" . $changes . "&nbsp;</b>";
                              echo '<div style="float:right; margin-top:-3px">';
                              echo '<i class="fa fa-exclamation-triangle" style="color:#dFb5b4; font-size:20pt; margin-right:10px;"></i>';
                              echo '</div>';
                              echo '</div>';
                           }
                           ?>
                        </div>
                     </div>
                  </div>
                  <!--payments-->
                  <div class="panel box box-success" style="padding-bottom:1px; border-top-color:<?php echo $accRGB; ?>;">
                     <div class="box-header">
                        <h4 class="box-title">
                           <a data-toggle="collapse" data-parent="#accordion" href="#collapse4" onclick="window.scrollTo(0, 200);">
                              Payments
                           </a>
                        </h4>
                     </div>
                     <div id="collapse4" class="panel-collapse collapse <?php echo $tog4; ?>">
                        <div class="box-body">
                           <div class="callout callout-info">
                              <h4>Payment Details</h4>
                              <p>In this section, you can view when credits were purchased and the prior balance on the selected service.</p>
                           </div>
                       <!--div class="box box-success" style="padding-bottom:1px; border-top-color:<?php echo $accRGB; ?>;"-->
                           <div class="box-body">
                              <div class="box-body table-responsive no-padding">
                                 <table class="table table-hover">
                                    <tr>
                                       <th>#</th>
                                       <th>Date</th>
                                       <th>Description</th>
                                       <th>Value</th>
                                       <th>Previous Balance</th>
                                       <?php {
                                          if (LoginHelper::isSystemAdmin()) {
                                             echo '<th>User</th>';
                                          }
                                       }
                                       ?>
                                    </tr>
                                    <?php
                                    if (isset($payments)) {
                                       foreach ($payments as $key => $value) {
                                          echo '<tr>';
                                          echo '<td>' . ($key + 1) . '</td>';
                                          echo '<td>' . $value['payment_timestamp'] . '</td>';
                                          echo '<td>' . $value['payment_description'] . '</td>';
                                          $value['Value'] = str_replace(",", "", $value['Value']);
                                          $value['Value'] = str_replace(".", "", $value['Value']);
                                          $value['Value'] = $value['Value'] / 10000; //money_format('%=*^-14#8.2i', $funds['1']);
                                          $fn1 = number_format($value['Value'], 2, '.', ' ');

                                          if (substr($value['Value'], 0, 1) != '-') {
                                             echo '<td><span class="label label-success">' . $fn1 . '</span></td>';
                                          } else {
                                             echo '<td><span class="label label-danger">' . $fn1 . '</span></td>';
                                          }

                                          $value['Previous Balance'] = str_replace(",", "", $value['Previous Balance']);
                                          $value['Previous Balance'] = str_replace(".", "", $value['Previous Balance']);
                                          $value['Previous Balance'] = $value['Previous Balance'] / 10000;
                                          $fn2 = number_format($value['Previous Balance'], 2, '.', ' ');

                                          if (substr($value['Previous Balance'], 0, 1) != '-') {
                                             echo '<td><span class="label label-success">' . $fn2 . '</span></td>';
                                          } else {
                                             echo '<td><span class="label label-danger">' . $fn2 . '</span></td>';
                                          }
                                          if (LoginHelper::isSystemAdmin()) {
                                             echo '<td>' . $value['user_username'] . '</td>';
                                          }
                                          echo '</tr>';
                                       }
                                    }
                                    ?>
                                 </table>
                              </div><!-- /.box-body -->
                           </div><!-- /.box-body -->
                        </div><!-- /.box-body -->
                        <!--/div-->
                     </div>
                  </div>
               </div>
            </div>
         </div>
   </section>
   <br>
</aside>

<!--  THE JAVASCRIPT IMPORT ALWAYS APPEARS BEFORE LOCAL JAVASCRIPT AT THE BOTTOM OF THE PAGE -->
<?php include("template_import_script.php"); //must import all scripts first     ?>
<!-- END JAVASCRIPT IMPORT -->

<script type="text/javascript">
<?php if (isset($_SESSION['userSystemAdmin']) && $_SESSION['userSystemAdmin'] == 1) { ?>
      $(function () {
         //assign a click listener to the forgot password link
         $(".btnMimic").on("click", function (e)
         {
            e.preventDefault();

            $(".loader").fadeIn("slow");
            $(".loaderIcon").fadeIn("slow");
            var user_id_to_mimic = $(this).attr('value');
            $(".btn_mimic").attr('disabled', 'true');

            $.post("../php/ajaxToggleMimic.php", {mimic_task: 'start', user_id_to_mimic: user_id_to_mimic}).done(function (raw_data) {
               var data = $.parseJSON(raw_data);
               if (data.success)
               {
                  alert('Mimic mode enabled!');
                  window.location = "../clienthome.php";
               } else
               {
                  alert('There was a problem with enabling mimic mode on this user, please contact the development department.');
                  window.location.reload();
               }

               $(".loader").fadeOut("slow");
               $(".loaderIcon").fadeOut("slow");
               $(".btn_mimic").removeAttr('disabled');
            }).fail(function (data) {
               alert('There was an error: ' + data + '');
               $(".btn_mimic").removeAttr('disabled');
               $(".loader").fadeOut("slow");
               $(".loaderIcon").fadeOut("slow");
            });
         });


      });
<?php } ?>

   $(function () {
      $('#routesTable').dataTable({
         "bPaginate": false,
         "bLengthChange": false,
         "bFilter": false,
         "bSort": true,
         "bInfo": true,
         "bAutoWidth": false
      });

   });

   //we just want to check if the route price they are trying to set is below their own reseller route price
   function validateRoutePrices()
   {
      var serviceBillingType = $('#serviceBillingType').val();
      var routeCount = $('#routeCount').val();
      var routeIsLessThanDefault = false;
      for (var i = 0; i < routeCount; i++)
      {
         var routeDefault = $('#routeDefaultResellerCost' + i).val();
         var routeUserCost = $('#routeRDNC' + i).val();

         if (routeDefault > routeUserCost)
         {
            routeIsLessThanDefault = true;
         }
      }


      if (routeIsLessThanDefault)
      {
         if (serviceBillingType == "PREPAID")
         {
            alert("You cannot set a route rate below your own default rate.");
         } else if (confirm("You are trying to set a route to a value below your own default route cost, which will result in negative margins. Do you want to continue?"))
         {
            //submit if they are okay with it
            document.getElementById('updateRoutes').submit();
         }
      } else
      {
         //all good, the price is higher, so submit
         document.getElementById('updateRoutes').submit();
      }
   }
</script>

<script type="text/javascript">
   $(window).load(function () {
      $(".loader").fadeOut("slow");
      $(".loaderIcon").fadeOut("slow");
   });
</script>

<script type="text/javascript">
   function reloadOnSelect(serviceId)
   {
      $(".loader").fadeIn("slow");
      $(".loaderIcon").fadeIn("slow");
      //NIEL window.location = "updateservice.php?service_id=" + encodeURIComponent(serviceId) + "&tog1=in";
      window.location = "updateservice.php?service_id=" + encodeURIComponent(serviceId);
      ;
   }
</script>

<script type='text/javascript'>
   $(window).load(function ()
   {
      $(document).scrollTop(<?php echo $scrPos; ?>);
   });
</script>

<script type="text/javascript">
   function deleteLBN(sId)
   {
      var sId = document.getElementById('clientListSelect').value;
      //alert(name);
      window.location = "updateservice.php?service_id=" + encodeURIComponent(sId) + "&tog5=in&delLBN=del";
   }
</script>

<script type="text/javascript">
   function toggleContent(what)
   {
      if (what == 'addRoute')
      {
         var contentId = document.getElementById("routeContent");
         contentId.style.display == "block" ? contentId.style.display = "none" :
                 contentId.style.display = "block";
      }
      if (what == 'addUser')
      {
         var contentId = document.getElementById("userContent");
         contentId.style.display == "block" ? contentId.style.display = "none" :
                 contentId.style.display = "block";
      }
   }
</script>

<SCRIPT language=Javascript>
   function isNumberKey(evt)
   {
      var charCode = (evt.which) ? evt.which : evt.keyCode;
      if (charCode != 46 && charCode > 31
              && (charCode < 48 || charCode > 57))
         return false;

      return true;
   }
</SCRIPT>

<!-- Template Footer -->
