<?php
/**
 * Include the header tempalte which sets up the HTML
 *
 * Don't forget to include template_import_script before any Javascripting
 * Don't forget to include template footer.php at the bottom of the page as well
 */
$headerPageTitle = "Tutorial"; //set the page title for the template import
TemplateHelper::initialize();


$singleView = 'block;';

if ($_SESSION['serviceId'] != '' && $_SESSION['resellerId'] != '') {
   if ($_SESSION['serviceId'] != $_SESSION['resellerId']) {
      //if(!isset($_GET['client']))
      {
         $_GET['client'] = $_SESSION['accountName'] . ' - ' . $_SESSION['serviceName'] . " - " . $_SESSION['serviceId'];
         //echo "<br><br><br><br>+---------------------->".$_GET['client'];
         $singleView = 'none;';
         // $noShow = 1;
      }
   } elseif ($_SESSION['serviceId'] == $_SESSION['resellerId']) {
      $singleView = 'block;';
      //$noShow = 1;
   }

   /* if($_SESSION['resellerId'] == '')
     {

     $singleView = 'none;';
     $noShow = 1;
     } */
}


////////////////////////////////////////////////
////////////////////////////////////////////////
////////////////////////////////////////////////
?>
<aside class="right-side">
   <section class="content-header">
      <h1>
         Tutorial
         <!--small>Control panel</small-->
      </h1>
   </section>

   <section class="content">
      <div class="row col-lg-12">

         <div class="callout callout-info">
            <!--h4>Tutorial</h4-->
            <p>Please use the tutorials below and the campaign manager user guide to assist you with the Bulk SMS platform.  If you have any questions or concerns, please contact
               <a href="mailto:support@connet-systems.com">support@connet-systems.com</a> or call us on 012 991 4328.</p>
            <p>To download the Connet Systems User Guide <a href='http://plugin.connet-systems.com/download/Plugin_Campaign_Manager_User_Guide.pdf' download>click here</a></p>
            <p>To read the Connet Systems User Guide <a href='http://plugin.connet-systems.com/download/Plugin_Campaign_Manager_User_Guide.pdf' target='_blank'>click here</a></p>
         </div>

         <div class="box box-solid" style="padding-left:10px;padding-top:10px;padding-bottom:5px;">
            <center><iframe width="960" height="540" src="https://www.youtube.com/embed/Ki6UOmazVqw" frameborder="0" allowfullscreen></iframe></center>
         </div>

         <div class="box box-solid" style="padding-left:10px;padding-top:10px;padding-bottom:5px;">
            <center><iframe width="960" height="540" src="https://www.youtube.com/embed/MpYuKZ9QItk" frameborder="0" allowfullscreen></iframe></center>
         </div>

      </div>
   </section>
</aside>

<!--  THE JAVASCRIPT IMPORT ALWAYS APPEARS BEFORE LOCAL JAVASCRIPT AT THE BOTTOM OF THE PAGE -->
<?php include("template_import_script.php"); //must import all scripts first  ?>
<!-- END JAVASCRIPT IMPORT -->

<script type="text/javascript">
   $(window).load(function () {
      $(".loader").fadeOut("slow");
      $(".loaderIcon").fadeOut("slow");
   })
</script>

<!-- Template Footer -->
