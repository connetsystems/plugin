<?php
/**
 * Include the header template which sets up the HTML
 *
 * Don't forget to include template_import_script before any Javascripting
 * Don't forget to include template footer.php at the bottom of the page as well
 */
$headerPageTitle = "Settings"; //set the page title for the template import
TemplateHelper::initialize();

include("../php/upload.php");

$accounts = getAccounts();

$viewImg = 'none;';

if (isset($_GET['account'])) {
   $aId = $_GET['account'];
   $aIdp = strrpos($aId, ' ');
   $aId = trim(substr($aId, $aIdp, strlen($aId)));

   //$set = 
   checkSettingExists($aId);
   /* if($set == 0)
     {

     } */
   $accCol = getAccColor($aId);
   $viewImg = 'block;';
}

/* if(isset($_POST['accId']))
  {
  $aId = $_POST['accId'];
  $accCol = getAccColor($aId);
  $viewImg = 'block;';
  } */
////////////////////////////////////////////////////////
////////////////////////////////////////////////////////
////////////////////////////////////////////////////////
//$re = 0;


if (isset($_POST['rgbVal'])) {
   $aId = $_POST['accId'];
   $userRGB = $_POST['rgbVal'];

   updateNewColor($userRGB, $aId);

   $accCol = getAccColor($aId);

   /* echo "<br>1--".$aId;
     echo "<br>2--".$_POST['rgbVal'];
     echo "<br>3--".$accCol; */
   $viewImg = 'block;';
   //$_SESSION['accountColour'] = $userRGB;
   //$accRGB = $userRGB;
   //$accRGB = ''.getAccColor($_SESSION['accountId']).'';
   // echo '<meta http-equiv="refresh" content= "0; URL=settings.php" />';
} else {
   //$accRGB = ''.getAccColor($_SESSION['accountId']).'';
}

if (isset($_FILES["mainLogo"])) {
   $maxSize = 1024 * 1024;

   $target_dir = "../img/skinlogos/" . $aId . "/";
   if (!is_dir($target_dir)) {
      mkdir($target_dir, 0777);
   }

   $target_file = $target_dir . basename($_FILES["mainLogo"]["name"]);

   //echo "<br>ml-".$target_file;

   $imageFileType = pathinfo($target_file, PATHINFO_EXTENSION);
   // Check if image file is a actual image or fake image

   if (!empty(($_FILES["mainLogo"]["tmp_name"]))) {
      $check = getimagesize($_FILES["mainLogo"]["tmp_name"]);

      if ($check !== false) {
         //    echo "File is an image - " . $check["mime"] . ".";
         $uploadOk = 1;
      } else {
         //    echo "File is not an image.";
         $uploadOk = 0;
      }
   }

   /* if (file_exists($target_file)) 
     {
     echo "Sorry, file already exists.";
     $uploadOk = 0;
     } */

   if ($_FILES["mainLogo"]["size"] > $maxSize) {
      //    echo "Sorry, your file is too large.";
      $uploadOk = 0;
   }

   if ($imageFileType != "jpg" && $imageFileType != "png" && $imageFileType != "jpeg" && $imageFileType != "gif") {
      //   echo "Sorry, only JPG, JPEG, PNG & GIF files are allowed.";
      $uploadOk = 0;
   }

   if ($uploadOk == 1) {
      $target_file = $target_dir . $aId . '.png';
      $ul = move_uploaded_file($_FILES["mainLogo"]["tmp_name"], $target_file);
      // echo '<br>ul-'.$ul;
      if ($ul == 1) {
         $target_file = substr($target_file, 2);
         updateServer($target_file, 'login_logo', 'core_account_settings', 'account_id', $aId);
         //updateServer($target_file, 'header_logo', 'core_account_settings', 'account_id', $_SESSION['userId']);
      }
   }
   /*
     echo "<pre>";
     print_r($_FILES);
     echo "</pre>"; */
}

//
if (isset($_FILES["headLogo"])) {
   $maxSize = 1024 * 1024;
   //$target_dir = "../img/skinlogos/";
   $target_dir = "../img/skinlogos/" . $aId . "/";
   if (!is_dir($target_dir)) {
      mkdir($target_dir, 0777);
   }
   $target_file = $target_dir . basename($_FILES["headLogo"]["name"]);

   //echo "<br>ml-".$target_file;

   $imageFileType = pathinfo($target_file, PATHINFO_EXTENSION);
   // Check if image file is a actual image or fake image

   if (!empty(($_FILES["headLogo"]["tmp_name"]))) {
      $check = getimagesize($_FILES["headLogo"]["tmp_name"]);

      if ($check !== false) {
         // echo "File is an image - " . $check["mime"] . ".";
         $uploadOk = 1;
      } else {
         //echo "File is not an image.";
         $uploadOk = 0;
      }
   }

   /* if (file_exists($target_file)) 
     {
     echo "Sorry, file already exists.";
     $uploadOk = 0;
     } */

   if ($_FILES["headLogo"]["size"] > $maxSize) {
      //echo "Sorry, your file is too large.";
      $uploadOk = 0;
   }

   if ($imageFileType != "jpg" && $imageFileType != "png" && $imageFileType != "jpeg" && $imageFileType != "gif") {
      //echo "Sorry, only JPG, JPEG, PNG & GIF files are allowed.";
      $uploadOk = 0;
   }

   if ($uploadOk == 1) {
      $target_file = $target_dir . $aId . '_header.png';
      $ul = move_uploaded_file($_FILES["headLogo"]["tmp_name"], $target_file);
      //echo '<br>ul-'.$ul;
      if ($ul == 1) {
         $target_file = substr($target_file, 2);
         updateServer($target_file, 'header_logo', 'core_account_settings', 'account_id', $aId);
      }
   }
   /*
     echo "<pre>";
     print_r($_FILES);
     echo "</pre>";
    */
}


if (isset($_FILES["tabIcon"])) {
   $maxSize = 1024 * 1024;
   $target_dir = "../img/skinlogos/" . $aId . "/";
   if (!is_dir($target_dir)) {
      mkdir($target_dir, 0777);
   }
   $target_file = $target_dir . basename($_FILES["tabIcon"]["name"]);

   $imageFileType = pathinfo($target_file, PATHINFO_EXTENSION);
   // Check if image file is a actual image or fake image

   if (!empty(($_FILES["tabIcon"]["tmp_name"]))) {
      $check = getimagesize($_FILES["tabIcon"]["tmp_name"]);

      if ($check !== false) {
         //      echo "File is an image - " . $check["mime"] . ".";
         $uploadOk = 1;
      } else {
         //        echo "File is not an image.";
         $uploadOk = 0;
      }
   }

   /* if (file_exists($target_file)) 
     {
     echo "Sorry, file already exists.";
     $uploadOk = 0;
     } */

   if ($_FILES["tabIcon"]["size"] > $maxSize) {
      //           echo "Sorry, your file is too large.";
      $uploadOk = 0;
   }

   if ($imageFileType != "jpg" && $imageFileType != "png" && $imageFileType != "jpeg" && $imageFileType != "gif") {
      //           echo "Sorry, only JPG, JPEG, PNG & GIF files are allowed.";
      $uploadOk = 0;
   }

   if ($uploadOk == 1) {
      $target_file = $target_dir . $aId . '_icon.png';
      $ul = move_uploaded_file($_FILES["tabIcon"]["tmp_name"], $target_file);
      //         echo '<br>ul-'.$ul;
      if ($ul == 1) {
         $target_file = substr($target_file, 2);
         updateServer($target_file, 'tab_logo', 'core_account_settings', 'account_id', $aId);
      }
   }
   /*
     echo "<pre>";
     print_r($_FILES);
     echo "</pre>"; */
}
//echo '<meta http-equiv="refresh" content= "0; URL=settings.php" />';
?>
<!-- Right side column. Contains the navbar and content of the page -->
<!--//////////////////////////-->
<!--div class="box-header">
    <h3 class="box-title">New Service</h3>
</div-->
<aside class="right-side">
   <section class="content-header">
      <h1>
         Settings
         <!--small>Control panel</small-->
      </h1>
   </section>
   <section class="content">
      <div class="row">
         <div class="col-md-12">
            <div class="box box-connet" style="padding-bottom:1px; border-top-color:<?php echo $accRGB; ?>;">

               <div class="callout callout-info" style="margin:10px;height:auto;">
                  <h4>Settings Instructions / Details</h4>
                  <p>Here you can customise the colour of your interface and upload logos.<br>
                  </p>
               </div>

               <form action="viewservice.php" method="get" id="clientList" class="sidebar-form" style="border:0px;">
                  <div class="form-group">
                     <label>Account List</label>
                     <select class="form-control" name="client" form="clientList" OnChange="reloadOnSelect(this.value);">
                        <?php
                        if ($aId == '0') {
                           echo '<option SELECTED>Please select an account to view...</option>';
                        }
                        foreach ($accounts as $key => $value) {
                           $accountName = $value['account_name'];
                           $accountId = $value['account_id'];

                           if ($aId == $accountId) {
                              echo "<option name='" . $accountId . "' SELECTED>" . $accountName . " - " . $accountId . "</option>";
                           } else {
                              echo "<option name='" . $accountId . "'>" . $accountName . " - " . $accountId . "</option>";
                           }
                        }
                        ?>
                     </select>
                  </div>
               </form>
            </div>
         </div>
      </div>


      <div class="row" style="display:<?php echo $viewImg; ?>">
         <div class="col-md-12">
            <div class="box box-connet" style="padding-bottom:1px; border-top-color:<?php echo $accRGB; ?>; height:610px !important;">
               <!-- Color Picker -->
               <form action="settings.php" method="POST" id="accSet" name="accSet" enctype="multipart/form-data" class="sidebar-form" style="border:0px;">

                  <div class="form-group">
                     <label>Color picker (This is your companies main colour. You can type the exact #RGB value in the textbox.):</label>
                     <div class="input-group my-colorpicker2">
                        <div class="input-group-addon" style="border:1px solid;" >
                           <i></i>
                        </div>
                        <input type="text" name="rgbVal" class="form-control" style="border:1px solid;" value="<?php echo $accCol; ?>" />
                     </div><!-- /.input group -->
                  </div><!-- /.form group -->

                  <div class="box box-connet" style="padding-bottom:0px; border-top-color:<?php echo $accRGB; ?>;">
                     <div class="form-group" style="padding:10px;">
                        <label>Login logo -280 x 80 px- (only JPG, JPEG, PNG & GIF files are allowed)</label>
                        <div style="padding:10px;">
                           <?php
                           echo "<img src=../img/skinlogos/" . $aId . "/" . $aId . ".png >";
                           ?>
                        </div>
                        <input type="file" name="mainLogo" id="mainLogo">
                     </div><!-- /.form group -->
                  </div>
                  <div class="box box-connet" style="border-top-color:<?php echo $accRGB; ?>;">
                     <div class="form-group" style="padding:10px;">
                        <label>Header logo -250 x 50 px- (only JPG, JPEG, PNG & GIF files are allowed)</label>
                        <div style="padding:10px;">
                           <?php
                           echo "<img src=../img/skinlogos/" . $aId . "/" . $aId . "_header.png >";
                           ?>
                        </div>
                        <input type="file" name="headLogo" id="headLogo">
                     </div><!-- /.form group -->
                  </div>
                  <div class="box box-connet" style=" border-top-color:<?php echo $accRGB; ?>;">
                     <div class="form-group" style="padding:10px;">
                        <label>Browser tab icon -24 x 24 px- (only JPG, JPEG, PNG & GIF files are allowed)</label>
                        <div style="padding:10px;">
                           <?php
                           echo "<img src=../img/skinlogos/" . $aId . "/" . $aId . "_icon.png >";
                           ?>
                        </div>
                        <input type="file" name="tabIcon" id="tabIcon">
                     </div><!-- /.form group -->
                  </div>

                  <input type="hidden" name="accId" id="accId" value="<?php echo $aId; ?>">

                  <div class="form-group">
                     <a class="btn btn-block btn-social btn-dropbox " onclick="accSet.submit();" style="background-color:<?php echo $accRGB; ?>; color: #ffffff; border: 2px solid; border-radius: 6px !important; float:left; width: 170px; height:36px;">
                        <i class="fa fa-save"></i>Save Settings
                     </a>

                  </div><!-- /.form group -->
                  <!--input type="submit" value="Upload" class="pure-button pure-button-primary"-->
               </form>
            </div>
         </div>
      </div>
   </section>
</aside>
<!--//////////////////////////-->

<!--  THE JAVASCRIPT IMPORT ALWAYS APPEARS BEFORE LOCAL JAVASCRIPT AT THE BOTTOM OF THE PAGE -->
<?php include("template_import_script.php"); //must import all scripts first  ?>
<!-- END JAVASCRIPT IMPORT -->


<script type="text/javascript">
   /*function reloadOnSelect()
    {
    $(".loader").fadeIn("slow");
    $(".loaderIcon").fadeIn("slow");
    
    //window.location = "billing.php?countryChange=" + countryName;
    }*/
</script>

<script type="text/javascript">
   function reloadOnSelect(name)
   {
      $(".loader").fadeIn("slow");
      $(".loaderIcon").fadeIn("slow");
      encodeURI(name);
      window.location = "settings.php?account=" + encodeURIComponent(name);
   }
</script>

<script type="text/javascript">
   $(function () {

      //Colorpicker
      //$(".my-colorpicker1").colorpicker();
      //color picker with addon
      $(".my-colorpicker2").colorpicker();


   });
</script>

<script type="text/javascript">
   $(window).load(function () {
      $(".loader").fadeOut("slow");
      $(".loaderIcon").fadeOut("slow");
   })
</script>


<!-- Template Footer -->