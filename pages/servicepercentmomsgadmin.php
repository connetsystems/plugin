<?php
/**
 * Include the header template which sets up the HTML
 *
 * Don't forget to include template_import_script before any Javascripting
 * Don't forget to include template footer.php at the bottom of the page as well
 */
$headerPageTitle = "MO Messages"; //set the page title for the template import
TemplateHelper::initialize();

//ini_set('memory_limit', '256M');

if (isset($_GET['range'])) {
   $startDate = $_GET['range'];
   //$dateArr = explode(' - ', $_GET['range']);
   //$startDate = ($dateArr[0]);
   $endDate = $startDate;
   $startDateConv = strtotime($startDate);
   $endDateConv = strtotime($endDate);
   // strtotime($dateArr1[1]);
   //$endDateConv = date('Y-m-d', ($endDate/1000));
   if ($startDateConv == $endDateConv) {
      $endDateConv = $endDateConv + (24 * 60 * 60) - 1;
   }
   $startDateConv = $startDateConv * 1000;
   $endDateConv = $endDateConv * 1000;
}
/* $startDateConv = date('Y-m-d', ($startDate/1000));
  $endDateConv = date('Y-m-d', ($endDate/1000)); */
/*
  echo '<br><br><br>S-------------'.$startDate;
  echo '<br>E-------------'.$endDate;
 */

if (isset($_GET['range1'])) {
   $range1 = $_GET['range1'];
   $dateArr1 = explode(' - ', $_GET['range1']);
   $startDate1 = strtotime($dateArr1[0]);
   $endDate1 = strtotime($dateArr1[1]);
   if ($startDate1 == $endDate1) {
      $endDate1 = $endDate1; // + (24*60*60);    
   }
   $startDate1 = $startDate1 * 1000;
   //$endDate1 = $endDate1*1000;
   $endDate1 = ($endDate1 + (24 * 60 * 60) - 1) * 1000;
}
$startDateConv1 = date('Y-m-d', ($startDate1 / 1000));
$endDateConv1 = date('Y-m-d', ($endDate1 / 1000));

/* echo '<br><br><br>S-------------'.$startDate;
  echo '<br>E-------------'.$endDate; */
$client = getServiceName($_GET['sId']);

$mcc = $_GET['mcc'];
$smsc = $_GET['smsc'];
$size = $_GET['total'];

$qry = '{
                  "size":' . $size . ',
                  "from":0,
                  "sort": {
                    "timestamp":"desc"
                  },
                  "fields" : ["mosms_id","timestamp","src","dest","content","meta.mccmnc","meta.smsc"],
                  "query": {
                    "filtered": {
                      "query": {
                        "bool" : {
                            "must" : [
                                {
                                 "query_string": {
                                 "query": "service_id:' . $_GET['sId'] . ' AND mccmnc:`' . $mcc . '` AND smsc:`' . lcfirst($smsc) . '_smsc`",
                                 "analyze_wildcard": true
                                    }
                                }
                                
                            ]
                        }
                      },
                      "filter": {
                        "bool": {
                          "must": [
                            {
                              "range": {
                                "timestamp": {
                                  "gte": ' . $startDateConv . ',
                                  "lte": ' . $endDateConv . '
                                }
                              }
                            }
                          ],
                          "must_not": []
                        }
                      }
                    }
                  }  
                }
                ';

$allStats = runRawMOSMSElasticsearchQuery($qry);
?>
<aside class="right-side">
   <section class="content-header">
      <h1>
         MO Messages
         <!--small>Control panel</small-->
      </h1>
   </section>

   <section class="content">
      <div class="box box-connet" style="padding-top:10px;padding-left:10px;padding-right:10px;padding-bottom:45px; border-top-color:<?php echo $accRGB; ?>;">

         <div class="callout callout-info" style="margin-bottom:10px;">
            <h4>MO Messages: <?php echo $client; ?></h4>
            <p>You are viewing all reply SMS's for the date: <?php echo $startDateConv1; ?></p>
         </div>

         <a class="btn btn-block btn-social btn-dropbox" name="nid" onclick="backToTraffic()" style="background-color:<?php echo $accRGB; ?>; border: 2px solid; border-radius: 6px !important; float:left; width: 100px;">
            <i class="fa fa-arrow-left"></i>Back
         </a>
      </div>
   </section>

   <section class="content">
      <div class="box box-connet" style="padding-top:1px; border-top-color:<?php echo $accRGB; ?>;">
         <div class="box-body">
            <div class="box-body table-responsive no-padding">
               <table id="example2"  class="table table-striped table-hover">
                  <thead>
                     <tr>
                        <th>mosms_id</th>
                        <th>Timestamp</th>
                        <th>To</th>
                        <th>From</th>
                        <th>Content</th>
                        <th>MCCMNC</th>
                        <th>SMSC</th>
                     </tr>
                  </thead>
                  <tbody>
                     <?php
                     if (isset($allStats) && $allStats != '') {
                        foreach ($allStats as $key => $value) {
                           if ($key = 'hits' && isset($value['hits'])) {
                              $countM0 = count($value['hits']);
                              for ($i = 0; $i < $countM0; $i++) {
                                 $netw = getNetFromMCC($value['hits'][$i]['fields']['meta.mccmnc'][0]);
                                 $smsw = strstr($value['hits'][$i]['fields']['meta.smsc'][0], '_', true);
                                 echo '<tr>';
                                 echo '<td>' . $value['hits'][$i]['_id'] . '</td>';
                                 echo '<td>' . str_replace('T', ' ', strstr($value['hits'][$i]['fields']['timestamp'][0], '+', true)) . '</td>';
                                 echo '<td>' . $value['hits'][$i]['fields']['dest'][0] . '</td>';
                                 echo '<td>' . $value['hits'][$i]['fields']['src'][0] . '</td>';
                                 echo '<td>' . $value['hits'][$i]['fields']['content'][0] . '</td>';
                                 echo '<td><img src="../img/serviceproviders/' . $netw . '.png">&nbsp;&nbsp;' . ucfirst($netw) . '</td>';
                                 echo '<td><img src="../img/serviceproviders/' . $smsw . '.png">&nbsp;&nbsp;' . ucfirst($smsw) . '</td>';
                                 /* echo '<td>'.$value['timestamp'].'</td>';
                                   echo '<td>'.$value['to'].'</td>';
                                   echo '<td>'.$value['from'].'</td>';
                                   echo '<td>'.$value['content'].'</td>';
                                   echo '<td><img src="../img/serviceproviders/'.$value['prefix_network_name'].'.png" />&nbsp;&nbsp;'.$value['prefix_network_name'].'</td>'; */
                                 echo '</tr>';
                              }
                           }
                        }
                     }
                     ?>
                  </tbody>
               </table>

               <div class="box-body">
                  <div class="form-group" style="padding-top:30px">
                     <a onclick="saveRep();" class="btn btn-block btn-social btn-dropbox" style="background-color:<?php echo $accRGB; ?>; border: 2px solid; border-radius: 6px !important; float:left; width: 134px; margin-top:-21px;">
                        <i class="fa fa-save"></i>Save Report
                     </a>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </section>
</aside>

<!--  THE JAVASCRIPT IMPORT ALWAYS APPEARS BEFORE LOCAL JAVASCRIPT AT THE BOTTOM OF THE PAGE -->
<?php include("template_import_script.php"); //must import all scripts first  ?>
<!-- END JAVASCRIPT IMPORT -->

<script type="text/javascript">
   $(function ()
   {
      $('#example2').dataTable({
         "bPaginate": true,
         "bLengthChange": false,
         "bFilter": true,
         "bSort": true,
         "bInfo": true,
         "bAutoWidth": false,
         "iDisplayLength": 100,
         "aoColumns": [
            null,
            null,
            null,
            null,
            null,
            null,
            null
         ],
      });
   });

   jQuery.extend(jQuery.fn.dataTableExt.oSort,
           {
              "title-numeric-pre": function (a) {
                 var x = a.match(/title="*(-?[0-9\.]+)/)[1];
                 return parseFloat(x);
              },
              "title-numeric-asc": function (a, b) {
                 return ((a < b) ? -1 : ((a > b) ? 1 : 0));
              },
              "title-numeric-desc": function (a, b) {
                 return ((a < b) ? 1 : ((a > b) ? -1 : 0));
              }
           });

   jQuery.extend(jQuery.fn.dataTableExt.oSort,
           {
              "formatted-num-pre": function (a) {
                 a = (a === "-" || a === "") ? 0 : a.replace(/[^\d\-\.]/g, "");
                 return parseFloat(a);
              },
              "formatted-num-asc": function (a, b) {
                 return a - b;
              },
              "formatted-num-desc": function (a, b) {
                 return b - a;
              }
           });
</script>

<script type="text/javascript">
   /*function searchText()
    {
    
    var repRange = "<?php echo urlencode($_GET['range']); ?>";
    var repRange1 = "<?php echo urlencode($_GET['range1']); ?>";
    //var client = "<?php echo $_GET['client']; ?>";
    var searchT = document.getElementById('search_filter').value;
    //alert(searchT);
    //window.location = "../pages/apitrafficadmin.php?client=" + serviceName + "&range=" + repRange;
    window.location = "../pages/servicepercentmsgadmin.php?range=" + repRange + "&range1=" + repRange1 + "&mcc=<?php echo $_GET['mcc']; ?>" + "&smsc=<?php echo $_GET['smsc']; ?>";
    }*/

   function backToTraffic()
   {
      var prevPage = "<?php echo $_GET['prevPage']; ?>";
      var repRange = "<?php echo urlencode($startDate1 . ' - ' . $endDate1); ?>";
      var cl = "<?php echo $_GET['sId']; ?>";


      window.location = "../pages/" + prevPage + ".php?range=" + repRange + "&client=" + cl;
   }

   function saveRep()
   {
      var csvContent = "data:text/csv;charset=utf-8,";
      var data = [["Id", "Date", "To", "From", "Content", "MCCMNC", "SMSC"],
<?php
if (isset($allStats) && $allStats != '') {
   foreach ($allStats as $key => $value) {
      if ($key = 'hits' && isset($value['hits'])) {
         $countM0 = count($value['hits']);
         for ($i = 0; $i < $countM0; $i++) {
            $netw = getNetFromMCC($value['hits'][$i]['fields']['meta.mccmnc'][0]);
            $smsw = strstr($value['hits'][$i]['fields']['meta.smsc'][0], '_', true);
            echo '[';
            echo '"' . $value['hits'][$i]['_id'] . '",';
            echo '"' . str_replace('T', ' ', strstr($value['hits'][$i]['fields']['timestamp'][0], '+', true)) . '",';
            echo '"' . $value['hits'][$i]['fields']['dest'][0] . '",';
            echo '"' . $value['hits'][$i]['fields']['src'][0] . '",';
            echo '"' . preg_replace("/[^a-zA-Z0-9\/_|+ -]/", '', ($value['hits'][$i]['fields']['content'][0])) . '",';
            echo '"' . ucfirst($netw) . '",';
            echo '"' . ucfirst($smsw) . '"';
            /* echo '<td>'.$value['timestamp'].'</td>';
              echo '<td>'.$value['to'].'</td>';
              echo '<td>'.$value['from'].'</td>';
              echo '<td>'.$value['content'].'</td>';
              echo '<td><img src="../img/serviceproviders/'.$value['prefix_network_name'].'.png" />&nbsp;&nbsp;'.$value['prefix_network_name'].'</td>'; */
            echo '],';
         }
      }
   }
}
/* if(isset($moData) && $moData != '')
  {
  foreach ($moData as $key => $value)
  {
  echo '[';
  echo '"'.$value['mosms_id'].'",';
  echo '"'.$value['timestamp'].'",';
  echo '"'.$value['to'].'",';
  echo '"'.$value['from'].'",';
  echo '`"'.$value['content'].'"`,';
  echo '"'.$value['prefix_network_name'].'",';
  $smsc = strstr($value['smsc'], '=');
  $smscPos = strrpos($smsc, "_");
  $smsc = substr($smsc, 1, $smscPos-1);
  echo '"'.ucfirst($smsc).'"';
  echo '],';
  }
  } */
?>
      ];


      data.forEach(function (infoArray, index)
      {
         dataString = infoArray.join(",");
         csvContent += index < data.length ? dataString + "\n" : dataString;
      });
      var encodedUri = encodeURI(csvContent);
      var link = document.createElement("a");
      link.setAttribute("href", encodedUri);
      link.setAttribute("download", "<?php echo 'ServicePercentMO_' . $startDateConv1; ?>.csv");

      link.click();
   }
</script>

<script type="text/javascript">
   $(window).load(function () {
      $(".loader").fadeOut("slow");
      $(".loaderIcon").fadeOut("slow");
   })
</script>

<!-- Template Footer -->

