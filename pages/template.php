<?php
/**
 * Include the header template which sets up the HTML
 *
 * Don't forget to include template_import_script before any Javascripting
 * Don't forget to include template footer.php at the bottom of the page as well
 */
$headerPageTitle = "Update Credits"; //set the page title for the template import
TemplateHelper::initialize();

echo '<div class="loader"></div>';
echo '<div class="loaderIcon"></div>';

require_once("../php/allInclusive.php");

$s = $_SESSION['serviceCredit'];
$sc = ($s / 10000);
if ($sc > 1000) {
   $scS = 'success';
} elseif ($sc < 1000 && $sc > 0) {
   $scS = 'warning';
   # code...
} elseif ($sc <= 0) {
   $scS = 'danger';
   # code...
}
$sc = number_format($sc, 2, '.', ' ');

$roles = $_SESSION['roles'];
$pgUrl = strrchr(realpath(__FILE__), '/');
$pgUrl = '/pages' . $pgUrl;
$pgId = getRoleIDFromUrl($pgUrl);
$pgId = substr($pgId, 0, 1);

////////////////////////////////////////////////




/* $sql = 'INSERT INTO `core_service_payment` (service_id, user_id, payment_value, payment_description,payment_previous_balance) 
  VALUES (:service_id, :user_id, :payment_value, :payment_description,:payment_previous_balance)';

  $sql = mysql_encode_param_array($sql, array(
  'service_id' => $selected_service->service_id,
  'user_id' => $_SESSION['auth']['user']->user_id,
  'payment_value' => $request->payment_value,
  'payment_description' => $request->description,
  'payment_previous_balance' => $selected_service->service_credit_available,
  )); */
?>

<div class="wrapper row-offcanvas row-offcanvas-left">
   <!-- Right side column. Contains the navbar and content of the page -->
   <aside class="left-side sidebar-offcanvas">
      <!-- sidebar: style can be found in sidebar.less -->
      <section class="sidebar">
         <ul class="sidebar-menu">
            <?php
            echo '<div class="callout callout-warning" style="margin-bottom:0px; border-color:#eaeaea; background-color:#eaeaea">';
            echo '<h4>' . $_SESSION['accountName'] . '</h4>';
            echo '<p>' . $_SESSION['serviceName'] . '</p>';
            echo '<p>' . $_SESSION['userName'] . '</p>';
            echo '<span class="label label-' . $scS . '" >R&nbsp;&nbsp;' . $sc . '</span>';

            echo '<p style="margin-top:8px;">' . $_SESSION['serviceBillingType'] . '</p>';
            echo '</div>';
            ?>
            <li>
               <a href="/home.php">
                  <i class="glyphicon glyphicon-home"></i><span>Dashboard</span>
               </a>
            </li>
                    <!--i class="fa fa-home"></i><span>Dashboard</span-->

            <?php
            $openDivCount = 0;
            $active = '';

            for ($i = 0; $i < count($roles); $i++) {
               $roleIC = $roles[$i]['role_icon'];
               $roleNA = $roles[$i]['role_name'];
               $roleUR = $roles[$i]['role_url'];
               $roleID = $roles[$i]['role_id'];
               $roleID_sub = substr($roleID, 0, 1);
               //    echo "<br>2-".$pgId."-=-".$roleID_sub;

               if ($roleID % 100 == 0) {
                  if ($openDivCount == 0) {
                     $openDivCount = 1;
                     //$rId = $roleID%10;
                     //echo "<br>2-".$roleID."-";
                     if ($pgId == $roleID_sub) {
                        $active = 'active';
                     } else {
                        $active = '';
                     }
                     //open div here 
                     echo '<li class="treeview ' . $active . ' ">';
                     echo '<a href="#">';
                     echo '<i class="' . $roleIC . '"></i>';
                     echo '<span>' . $roleNA . '</span>';
                     echo '<i class="fa pull-right fa-angle-left"></i>';
                     echo '</a>';
                     echo '<ul class="treeview-menu" style="display:none;">';
                  } else {
                     echo '</ul>';
                     echo '</li>';

                     if ($pgId == $roleID_sub) {
                        $active = 'active';
                     } else {
                        $active = '';
                     }

                     echo '<li class="treeview ' . $active . ' ">';
                     echo '<a href="#">';
                     echo '<i class="' . $roleIC . '"></i>';
                     echo '<span>' . $roleNA . '</span>';
                     echo '<i class="fa pull-right fa-angle-left"></i>';
                     echo '</a>';
                     echo '<ul class="treeview-menu" style="display:none;">';
                     //close div here
                     //then open new div after here
                  }
               } else {
                  if ($pgUrl == $roleUR) {
                     $active2 = ' class="active" ';
                  } else {
                     $active2 = '';
                  }

                  echo '<li' . $active2 . '>';
                  echo '<a href="' . $roleUR . '">';
                  echo '<i class="' . $roleIC . '"></i> <span>' . $roleNA . '</span>';
                  echo '</a>';
                  echo '</li>';
               }
            }
            echo '</ul>';
            echo '</li>';
//close last div last
            ?>
         </ul>
      </section>
   </aside>

   <aside class="right-side">
      <section class="content-header">
         <h1>
            Update Credits
            <!--small>Control panel</small-->
         </h1>
      </section>

      <section class="content">
         <div class="row col-lg-12">
            <div class="box box-solid" style="height:66px; margin-top:-15px;display:<?php echo $singleView; ?>">
               <form action="updateservice.php" method="get" id="clientList" class="sidebar-form" style="border:0px;">
                  <div class="form-group">
                     <label>Client List</label>
                     <select class="form-control" name="client" form="clientList" OnChange="reloadOnSelect(this.value);">
                        <?php
                        if ($cl == '0') {
                           echo '<option SELECTED>Please select a client...</option>';
                        }
                        foreach ($clients as $key => $value) {
                           $sId = $value['service_id'];
                           if ($cl == $sId) {
                              $accountName = $value['account_name'];
                              $serviceName = $value['service_name'];
                              echo "<option name='" . $sId . "' SELECTED>" . $accountName . " - " . $serviceName . " - " . $sId . "</option>";
                           } else {
                              echo "<option name='" . $sId . "'>" . $value['account_name'] . " - " . $value['service_name'] . " - " . $sId . "</option>";
                           }
                        }
                        ?>
                     </select>
                  </div>
                  <br>
               </form>
            </div>
         </div>
      </section>

   </aside>
</div>

<!--  THE JAVASCRIPT IMPORT ALWAYS APPEARS BEFORE LOCAL JAVASCRIPT AT THE BOTTOM OF THE PAGE -->
<?php include("template_import_script.php"); //must import all scripts first ?>
<!-- END JAVASCRIPT IMPORT -->

<script type="text/javascript">
   $(window).load(function () {
      $(".loader").fadeOut("slow");
      $(".loaderIcon").fadeOut("slow");
   })
</script>

<!-- Template Footer -->

