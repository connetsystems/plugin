<?php
/**
 * Include the header template which sets up the HTML
 *
 * Don't forget to include template_import_script before any Javascripting
 * Don't forget to include template footer.php at the bottom of the page as well
 */

TemplateHelper::setPageTitle('Network Traffic Summary');
TemplateHelper::initialize();

// $noShow = 0;

$singleView = 'block;';

if ($_SESSION['serviceId'] != '' && $_SESSION['resellerId'] != '') {
   if ($_SESSION['serviceId'] != $_SESSION['resellerId']) {
      //if(!isset($_GET['client']))
      {
         $_GET['client'] = $_SESSION['accountName'] . ' - ' . $_SESSION['serviceName'] . " - " . $_SESSION['serviceId'];
         //echo "<br><br><br><br>+---------------------->".$_GET['client'];
         $singleView = 'none;';
         // $noShow = 1;
      }
   } elseif ($_SESSION['serviceId'] == $_SESSION['resellerId']) {
      $singleView = 'block;';
      //$noShow = 1;
   }
   /* elseif($_SESSION['resellerId'] == '')
     {

     $noShow = 1;
     } */
}

$clients = getResellersOnly();

if (isset($_GET['range'])) {
   $dateArr = explode(' - ', $_GET['range']);
   $startDate = $dateArr[0];
   $endDate = $dateArr[1];
} else {
   $startDate = date('Y-m-d 00:00:00');
   $endDate = date('Y-m-d 23:59:59');
}

if (isset($_GET['client']) && $_GET['client'] != 'Show All') {
   $fileName = $_GET['client'];
   $cl = $_GET['client'];
   $clp = strrpos($cl, ' ');
   $cl = trim(substr($cl, $clp, strlen($cl)));
   $cle = '';
   $cle = $cl;

   foreach ($clients as $key => $val) {
      if ($cl == $val['service_id']) {
         $acc = $val['account_id'];
      }
   }
   $cle = getSubsFromReseller($acc);
   /* if(isset($cle) && $cle != '')
     {
     foreach ($cle as $key => $value)
     {
     $cle .= $value['service_id'];
     }
     } */
   //echo '<br>1-'.$cle;
   $cle = substr($cle, 0, -15);
   //echo '<br>2-'.$cle;
   $routeCost = getRouteCostingForReseller($cl);

   $dsp = 'inline';
   $dir = '../img/routes';
   $serProviders = scandir($dir);
} else {
   $fileName = 'Show All';
   $_GET['client'] = "0 - Please select a client...";
   $cl = strstr($_GET['client'], ' ', true);
   $dsp = 'none';
   $cle = '';
   foreach ($clients as $key => $value) {
      $cle .= $value['service_id'] . ' OR service_id:';
   }
   $cle = substr($cle, 0, -15);
   //$cle = '*';
}

//  echo '<br><br><br>XXXXXX----------->'.$cle.'<------';

/* if($cl == '0')
  {
  $cl = '';
  foreach ($clients as $key => $value)
  {
  $cl .= $value['service_id'].',';
  }

  $cl = substr($cl, 0, -1);
  //  echo "<br><br><br><br><br><br><br>------>".$cl."<------";

  //$netTraffic = getNetworkTraffic($cl, $startDate, $endDate);
  } */

//$netTraffic = getNetworkTraffic($cl, $startDate, $endDate);

/* else
  {
  } */

//$startDateConv = ;
$startDateUnix = strtotime($startDate) * 1000;
$endDateUnix = strtotime($endDate) * 1000;

$endDateUnix += (23 * 59 * 59 * 1000);

$qry2 = '{
              "size": 0,
              
                  "aggs": {
                    "service_id": {
                      "terms": {
                        "field": "service_id",
                        "size": 0,
                        "order": {
                          "1": "desc"
                        }
                      },
                      "aggs": {
                        "1": {
                          "sum": {
                            "script": "doc[\'billing_units\'].value",
                            "lang": "expression"
                          }
                        },
                    "mccmnc": {
                      "terms": {
                        "field": "mccmnc",
                        "size": 0,
                        "order": {
                          "1": "desc"
                        }
                      },
                      "aggs": {
                        "1": {
                          "sum": {
                            "script": "doc[\'billing_units\'].value",
                            "lang": "expression"
                          }
                        },
                        "dlr": {
                              "terms": {
                                "field": "route_billing",
                                "size": 0,
                                "order": {
                                  "dlr_units": "desc"
                                }
                              },
                              "aggs": {
                                "dlr_units": {
                                  "sum": {
                                    "script": "doc[\'billing_units\'].value",
                                    "lang": "expression"
                                  }
                                }
                            }
                          }
                      
                  
                      }
                    }
                  }
                }
              },
              "query": {
                "filtered": {
                  "query": {
                    "query_string": {
                      "query": "(service_id:' . $cle . ')",
                      "analyze_wildcard": true
                    }
                  },
                  "filter": {
                    "bool": {
                      "must": [
                        {
                          "range": {
                            "timestamp": {
                                "gte": ' . $startDateUnix . ',
                                "lte": ' . $endDateUnix . '
                            }
                          }
                        }
                      ],
                      "must_not": []
                    }
                  }
                }
              }
            }';

$allStats2 = runRawMTSMSElasticsearchQuery($qry2);

$netName = array();
$uniqueMCC = array();

foreach ($allStats2 as $key => $value) {
   if (isset($value['service_id'])) {
      $val = $value['service_id']['buckets'];
      $countBuckets = count($val);

      for ($c = 0; $c < $countBuckets; $c++) {
         $countMcc = count($val[$c]['mccmnc']['buckets']);
         for ($d = 0; $d < $countMcc; $d++) {
            if (!isset($uniqueMCC[$val[$c]['mccmnc']['buckets'][$d]['key']])) {
               $uniqueMCC[$val[$c]['mccmnc']['buckets'][$d]['key']] = $val[$c]['mccmnc']['buckets'][$d]['1']['value'];
               //array_push($uniqueMCC, $val[$c]['mccmnc']['buckets'][$d]['key']);
            } else {
               $uniqueMCC[$val[$c]['mccmnc']['buckets'][$d]['key']] += $val[$c]['mccmnc']['buckets'][$d]['1']['value'];
            }
            $countKeys = count($val[$c]['mccmnc']['buckets'][$d]['dlr']['buckets']);
            for ($e = 0; $e < $countKeys; $e++) {
               $dcC = $val[$c]['mccmnc']['buckets'][$d]['dlr']['buckets'][$e]['doc_count'];
               $dcU = $val[$c]['mccmnc']['buckets'][$d]['dlr']['buckets'][$e]['dlr_units']['value'];
               $dcCost = $val[$c]['mccmnc']['buckets'][$d]['dlr']['buckets'][$e]['key'];
               $newP = $dcCost / $dcU * $dcC;
               //echo '<br><br><br><br>----->>>'.$val[$c]['doc_count'].'<<<-----';
               $netName[getNetFromMCC($val[$c]['mccmnc']['buckets'][$d]['key']) . '---' . $val[$c]['key'] . '-+-' . $val[$c]['mccmnc']['buckets'][$d]['key']] = array($val[$c]['key'], getCountryFromMCC($val[$c]['mccmnc']['buckets'][$d]['key']), $val[$c]['mccmnc']['buckets'][$d]['1']['value'], $newP, $val[$c]['mccmnc']['buckets'][$d]['key']);
               //$netName[getNetFromMCC($val[$c]['mccmnc']['buckets'][$d]['key']).'---'.$val[$c]['key']] = array($val[$c]['key'], getCountryFromMCC($val[$c]['mccmnc']['buckets'][$d]['key']), $val[$c]['mccmnc']['buckets'][$d]['1']['value'], $val[$c]['mccmnc']['buckets'][$d]['dlr']['buckets']['0']['key'], $val[$c]['mccmnc']['buckets'][$d]['key']);
            }
         }
      }
   }
}

/* echo '<pre>';                    
  print_r($uniqueMCC);
  echo '</pre>'; */
?>

<aside class="right-side">
   <section class="content-header">
      <h1>
         Network Traffic Summary
         <!--small>Control panel</small-->
      </h1>
   </section>

   <section class="content" style="padding-bottom:0px;margin-bottom:2px;margin-top:1px;">
      <div class="form-group" style="margin-bottom:2px;;margin-top:1px;">
         <div class="box box-solid" style="margin-bottom:0px;display:block; padding-top:1px;padding-bottom:1px;">
            <form class="sidebar-form" style="border:0px;margin-bottom:0px;">
               <div class="callout callout-info" style="margin-bottom:0px;">
                  <h4>Instructions</h4>
                  <p>This page will show you ALL NETWORK traffic for the date range that you specify in the calendar below.</p>
               </div>
               <div class="callout callout-warning">
                  <h4>Note</h4>
                  <p>The totals represented in rand values are approximations only. The actual value may vary slightly.</p>
                  <div style="float:right; margin-top:-42px;margin-right: 10px;">
                     <i class="fa fa-exclamation-triangle" style="color:#d1ba8a; font-size:20pt;"></i>
                  </div>
               </div>
               <div id="reportrange" class="select pull-left" onmouseover="" style="cursor: pointer;padding-left:10px;">
                  <i class="fa fa-calendar fa-lg"></i>
                  <span style="font-size:15px"><?php echo $startDate . " - " . $endDate; ?></span><b class="caret"></b>
               </div>
               <br>
               <div class="form-group" style="display:<?php echo $singleView; ?>;margin-bottom:0px;">
                  <br>
                  <label>Client List</label>
                  <select class="form-control" name="client" id="client" form="clientList" OnChange="reloadOnSelect(this.value);">
                     <?php
//if($cl == '0')
                     {
                        echo '<option SELECTED>Show All</option>';
                     }
                     foreach ($clients as $key => $value) {
                        $sId = $value['service_id'];
                        if ($cl == $sId) {
                           $accountName = $value['account_name'];
                           $serviceName = $value['service_name'];
                           echo "<option name='" . $sId . "' SELECTED>" . $accountName . " - " . $serviceName . " - " . $sId . "</option>";
                        } else {
                           echo "<option name='" . $sId . "'>" . $value['account_name'] . " - " . $value['service_name'] . " - " . $sId . "</option>";
                        }
                     }
                     ?>
                  </select>
               </div>
               <br>
            </form>
         </div>
      </div>
   </section>

   <section class="content">
      <div class="box box-connet" style="padding-bottom:1px; border-top-color:<?php echo $accRGB; ?>;">
         <div class="box-header">
            <h3 class="box-title">Network Traffic Totals</h3>
         </div><!-- /.box-header -->
         <div class="box-body table-responsive">
            <table id="example" class="table table-bordered table-striped dataTable table-hover">
               <thead>
                  <tr>
                     <th>Network</th>
                     <th style="text-align:right;">Units</th>
                     <th style="text-align:right;">Cost/Unit</th>
                     <th style="text-align:right;">Total Cost</th>
                     <th>Cur</th>
                  </tr>
               </thead>
               <tbody>
                  <?php
                  /* echo '<pre>';
                    print_r($netName);
                    echo '</pre>'; */
                  /* $finalCost = 0;
                    $totalSent = 0; */
                  $totale = 0;
                  $totals = 0;
                  foreach ($uniqueMCC as $key => $value) {
                     $chec = 0;
                     $netw = getNetFromMCC($key);

                     if (strpos($netw, 'Special')) {
                        $logo = trim('Special');
                     } else {
                        $logo = $netw;
                        $logoP = strpos($netw, ' / ');
                        //echo '<br>logoP='.$logoP.'<-';
                        if ($logoP != '') {
                           $logo = substr($logo, $logoP + 3, strlen($logo));
                        }
                        $logo = trim($logo);
                     }

                     if ($logo == '') {
                        $logo = 'Special';
                     }

                     echo '<tr>';
                     if (strpos($netw, 'Special')) {
                        echo '<td><img src="../img/serviceproviders/Special.png">&nbsp;&nbsp;' . $netw . ' (' . $key . ')</td>';
                        //echo '<td>'.$netw.' ('.$key.')</td>';
                     } else {
                        echo '<td><img src="../img/serviceproviders/' . $logo . '.png">&nbsp;&nbsp;' . $netw . ' (' . $key . ')</td>';
                        //echo '<td><img src="../img/serviceproviders/'.$logo.'.png">&nbsp;&nbsp;'.$key.' ('.$value['4'].')'.'</td>';
                     }
                     echo '<td style="text-align:right;">' . number_format($value, 0, '.', ' ') . '</td>';
                     if (isset($routeCost)) {
                        foreach ($routeCost as $key2 => $value2) {
                           if ($value2['route_mccmnc'] == $key) {
                              $value2['route_billing'] = explode(',', $value2['route_billing']);
                              echo '<td style="text-align:right;">' . number_format(($value2['route_billing'][0] / 10000), 4, '.', '') . '</td>';
                              echo '<td style="text-align:right;">' . number_format(($value * $value2['route_billing'][0] / 10000), 2, '.', ' ') . '</td>';
                              echo '<td>' . $value2['route_currency'] . '</td>';
                           }
                        }
                        $chec = 1;
                     }

                     if ($chec == 0) {
                        echo '<td style="text-align:right;">-</td>';
                        echo '<td style="text-align:right;">-</td>';
                        echo '<td style="text-align:right;">-</td>';
                     }
                     echo '</tr>';
                  }
                  ?>
               </tbody>
               <tfoot>
                  <tr>
                     <th></th>
                     <th></th>
                     <th></th>
                     <th></th>
                     <th></th>
                  </tr>
               </tfoot>
            </table>
         </div><!-- /.box-body -->
         <div class="box-body">
            <div class="form-group">
               <a onclick="saveRep();" class="btn btn-block btn-social btn-dropbox " style="background-color:<?php echo $accRGB; ?>; border: 2px solid; border-radius: 6px !important; float:left; width: 134px; margin-top:-21px;">
                  <i class="fa fa-save"></i>Save Report
               </a>
            </div>
         </div>
      </div>
   </section>

   <section class="content">
      <div class="box box-connet" style="padding-bottom:1px; border-top-color:<?php echo $accRGB; ?>;">
         <div class="box-header">
            <h3 class="box-title">Service Traffic Totals</h3>
         </div><!-- /.box-header -->
         <div class="box-body table-responsive">
            <table id="example2" class="table table-bordered table-striped dataTable table-hover">
               <thead>
                  <tr>
                     <th>Service</th>
                     <th>Network</th>
                     <th>Country</th>
                     <th style="text-align:right;">Units&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</th>
                     <th style="text-align:right;">Cost/Unit&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</th>
                     <th style="text-align:right;">Total Cost&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</th>
                     <th>Cur</th>
                  </tr>
               </thead>
               <tbody>
                  <?php
                  /* echo '<pre>';
                    print_r($netName);
                    echo '</pre>'; */
                  /* $finalCost = 0;
                    $totalSent = 0; */
                  $totale = 0;
                  $totals = 0;
                  foreach ($netName as $key => $value) {
                     $key = strstr($key, '---', true);
                     if (strpos($key, 'Special')) {
                        $logo = trim('Special');
                     } else {
                        $logo = $key;
                        $logoP = strpos($key, ' / ');
                        //echo '<br>logoP='.$logoP.'<-';
                        if ($logoP != '') {
                           $logo = substr($logo, $logoP + 3, strlen($logo));
                        }
                        $logo = trim($logo);
                     }

                     if ($logo == '') {
                        $logo = 'Special';
                     }

                     $flag = str_replace(' / ', '', $value['1']);
                     if ($flag == '') {
                        $flag = 'Special';
                     }

                     $sNaC = getServiceNameAndCurrency($value['0'], $value['4']);
                     //if($sNaC->num_rows > 1)
                     if (isset($sNaC)) {

                        if (isset($sNaC->num_rows)) {
                           /* if($sNaC->num_rows == 0)
                             {
                             echo '<br>';
                             print_r($sNaC);
                             echo '<br>sn='.$value['0'];
                             echo '<br>mc='.$value['4'];

                             } */
                           foreach ($sNaC as $key2 => $va) {
                              $sN = $va['service_name'];
                              $cur = $va['route_currency'];
                           }
                        } else {
                           $sN = '--';
                           $cur = '--';
                        }
                     } else {
                        $sN = '-';
                        $cur = '-';
                     }
                     echo '<tr>';

                     echo '<td id="' . $value['0'] . 'name">' . $sN . ' (' . $value['0'] . ')</td>';
                     if (strpos($key, 'Special')) {
                        echo '<td><img src="../img/serviceproviders/Special.png">&nbsp;&nbsp;' . $key . ' (' . $value['4'] . ')' . '</td>';
                     } else {
                        echo '<td><img src="../img/serviceproviders/' . $logo . '.png">&nbsp;&nbsp;' . $key . ' (' . $value['4'] . ')' . '</td>';
                     }

                     echo '<td><img src="../img/flags/' . $flag . '.png">&nbsp;&nbsp;' . $value['1'] . '</td>';

                     $totals += $value['2'];
                     echo '<td style="text-align:right;" id="' . $key . 'units">' . number_format($value['2'], 0, '.', ' ') . '</td>';
                     echo '<td style="text-align:right;" id="' . $key . 'costs">' . number_format(($value['3'] / 10000), 4, '.', ' ') . '</td>';
                     /* if($value['route_currency'] == 'ZAR')
                       {
                       $u = 'R ';
                       }
                       elseif($value['route_currency'] == 'EUR')
                       {
                       $u = '&euro; ';
                       }
                       else
                       {
                       $u = '';
                       } */
                     $totale += ($value['2'] * $value['3'] / 10000);
                     echo '<td style="text-align:right;" id="' . $key . 'total">' . number_format(($value['2'] * $value['3'] / 10000), 2, '.', ' ') . '</td>';
                     echo '<td style="text-align:middle;" id="' . $key . 'cur">' . $cur . '</td>';

                     echo '</tr>';
                  }
                  ?>
               </tbody>
               <tfoot>
                  <tr>
                     <th></th>
                     <th></th>
                     <th></th>
                     <th style="text-align:right;" id="totalSentsCell"><?php echo number_format($totals, 0, '.', ' '); ?></th>
                     <th style="text-align:right;"></th>
                     <th style="text-align:right;" id="totalSentsCost"><?php echo number_format($totale, 2, '.', ' '); ?></th>
                     <th></th>
                  </tr>
               </tfoot>
            </table>
         </div><!-- /.box-body -->
         <div class="box-body">
            <div class="form-group">
               <a onclick="saveRep();" class="btn btn-block btn-social btn-dropbox " style="background-color:<?php echo $accRGB; ?>; border: 2px solid; border-radius: 6px !important; float:left; width: 134px; margin-top:-21px;">
                  <i class="fa fa-save"></i>Save Report
               </a>
            </div>
         </div>
      </div>
   </section>

</aside>


<!--  THE JAVASCRIPT IMPORT ALWAYS APPEARS BEFORE LOCAL JAVASCRIPT AT THE BOTTOM OF THE PAGE -->
<?php include("template_import_script.php"); //must import all scripts first ?>
<!-- END JAVASCRIPT IMPORT -->

<script type="text/javascript">

   function saveRep()
   {
      var csvContent = "data:text/csv;charset=utf-8,";

      var data = [["Service Name", "Network", "Country", "Units Sent", "Cost/Unit", "Total Cost", "Cur"],
<?php
/* foreach($netTraffic as $key => $value) 
  {
  echo '[';
  echo '"'.$value['service_name'].'",';
  echo '"'.$value['prefix_network_name'].' ('.$value['mccmnc'].')'.'",';
  echo '"'.$value['prefix_country_name'].'",';
  echo '"'.$value['units'].'",';
  echo '"'.$value['client_price'].'",';
  echo '"'.round($value['price'], 2).'"';
  echo '],';
  } */
foreach ($netName as $key => $value) {
   $sNaC = getServiceNameAndCurrency($value['0'], $value['4']);

   if (isset($sNaC)) {
      if (isset($sNaC->num_rows)) {
         foreach ($sNaC as $key2 => $va) {
            $sN = $va['service_name'];
            $cur = $va['route_currency'];
         }
      } else {
         $sN = '--';
         $cur = '--';
      }
   } else {
      $sN = '-';
      $cur = '-';
   }

   $key = strstr($key, '---', true);
   echo '[';
   echo '"' . $sN . ' (' . $value['0'] . ')",';
   echo '"' . $key . ' (' . $value['4'] . ')",';
   echo '"' . $value['1'] . '",';
   echo '"' . $value['2'] . '",';
   echo '"' . ($value['3'] / 10000) . '",';
   echo '"' . ($value['2'] * $value['3'] / 10000) . '",';
   echo '"' . $cur . '"';
   echo '],';
}
?>
      ];
      data.forEach(function (infoArray, index)
      {
         dataString = infoArray.join(",");
         csvContent += index < data.length ? dataString + "\n" : dataString;
      });
      var encodedUri = encodeURI(csvContent);
      var link = document.createElement("a");
      link.setAttribute("href", encodedUri);
      link.setAttribute("download", "<?php echo 'NetworkTraffic' . $fileName . ' ' . $startDate . '-' . $endDate; ?>.csv");

      link.click();
   }

</script>

<script type="text/javascript">
   $(function ()
   {
      $('#example2').dataTable({
         "bPaginate": true,
         "bLengthChange": false,
         "bFilter": true,
         "bSort": true,
         "bInfo": true,
         "bAutoWidth": false,
         "iDisplayLength": 100,
         "aoColumns": [
            null,
            null,
            null,
            {"sType": 'formatted-num', targets: 0},
            {"sType": 'formatted-num', targets: 0},
            {"sType": 'formatted-num', targets: 0},
            null
         ],
         "order": [[3, "desc"]],
      });
   });

   jQuery.extend(jQuery.fn.dataTableExt.oSort,
           {
              "title-numeric-pre": function (a) {
                 var x = a.match(/title="*(-?[0-9\.]+)/)[1];
                 return parseFloat(x);
              },
              "title-numeric-asc": function (a, b) {
                 return ((a < b) ? -1 : ((a > b) ? 1 : 0));
              },
              "title-numeric-desc": function (a, b) {
                 return ((a < b) ? 1 : ((a > b) ? -1 : 0));
              }
           });

   jQuery.extend(jQuery.fn.dataTableExt.oSort,
           {
              "formatted-num-pre": function (a) {
                 a = (a === "-" || a === "") ? 0 : a.replace(/[^\d\-\.]/g, "");
                 return parseFloat(a);
              },
              "formatted-num-asc": function (a, b) {
                 return a - b;
              },
              "formatted-num-desc": function (a, b) {
                 return b - a;
              }
           });
</script>

<script type="text/javascript" src="../js/plugins/daterangepicker/moment.js"></script>

<script type="text/javascript" src="../js/plugins/daterangepicker/daterangepickerNetTraffic.js"></script>

<script type="text/javascript">
   $('#reportrange').daterangepicker(
           {
              ranges: {
                 'Today': [moment(), moment()],
                 'Yesterday': [moment().subtract('days', 1), moment().subtract('days', 1)],
                 'Last 7 Days': [moment().subtract('days', 6), moment()],
                 'Last 30 Days': [moment().subtract('days', 29), moment()],
                 'This Month': [moment().startOf('month'), moment().endOf('month')],
                 'Last Month': [moment().subtract('month', 1).startOf('month'), moment().subtract('month', 1).endOf('month')]
              },
              startDate: moment(<?php echo '"' . $startDate . '"'; ?>),
              endDate: moment(<?php echo '"' . $endDate . '"'; ?>)
           },
           function (start, end) {
              $(".loader").fadeIn("slow");
              $(".loaderIcon").fadeIn("slow");
              $('#reportrange span').html(start.format('YYYY-MM-DD') + ' - ' + end.format('YYYY-MM-DD'));
              var serviceName = document.getElementById("client").value;

              var repRange = $("#reportrange span").html();
              encodeURI(serviceName);
              window.location = "../pages/networktrafficsummary.php?client=" + encodeURIComponent(serviceName) + "&range=" + repRange;
           }
   );
</script>

<script type="text/javascript">
   function reloadOnSelect(name)
   {
      var serviceName = name;
      var repRange = $("#reportrange span").daterangepicker('getDate').html();
      encodeURI(serviceName);
      window.location = "../pages/networktrafficsummary.php?client=" + encodeURIComponent(serviceName) + "&range=" + repRange;
   }
</script>

<script type="text/javascript">
   $(window).load(function () {
      $(".loader").fadeOut("slow");
      $(".loaderIcon").fadeOut("slow");
   })
</script>

<!-- Template Footer -->

