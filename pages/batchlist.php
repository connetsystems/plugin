<?php
/**
 * Include the header tempalte which sets up the HTML
 *
 * Don't forget to include template_import_script before any Javascripting
 * Don't forget to include template footer.php at the bottom of the page as well
 */
//-------------------------------------------------------------
// Template
//-------------------------------------------------------------
TemplateHelper::setPageTitle('Batchlist SMS');
TemplateHelper::initialize();
 
$singleView = 'block;';


//-------------------------------------------------------------
// Permissions
//-------------------------------------------------------------
$clients = PermissionsHelper::getAllServicesWithPermissions();

if (isset($_GET['service_id']))
{
   if (!isset($clients[$_GET['service_id']]))
   {
      header('location: ' . $_SERVER['DOCUMENT_URI']);
      die();
   }
}

if (count($clients) == 1)
{
   $_GET['service_id'] =  $_SESSION['serviceId'];
   $singleView = 'none;';
 
} elseif (!isset($_GET['service_id'])) {

   $_GET['service_id'] = $_SESSION['serviceId'];
}

//-------------------------------------------------------------
// Datepicker
//-------------------------------------------------------------
if (isset($_GET['start_date']))
{
   $startDate = $_GET['start_date'];
   $endDate = $_GET['end_date'];
}
else
{
   $startDate = (strtotime('today midnight')) * 1000;
   $endDate = (strtotime('tomorrow midnight')) * 1000;
}

$startDateConv = date('Y-m-d', ($startDate / 1000));
$endDateConv = date('Y-m-d', ($endDate / 1000));

if (isset($_GET['client']) && $_GET['client'] != 'Please select a client...')
{
   $split = explode(' ', trim($_GET['client']));
   $service_id = (integer) end($split);
   
   $dsp = 'inline';

}
else if(isset($_GET['service_id']))
{
   $service_id = $_GET['service_id'];
   $dsp = 'inline';
}
else
{
   $_GET['client'] = "0 - Please select a client...";
   $service_id = strstr($_GET['client'], ' ', true);
   $dsp = 'none';
}

 
if ($service_id != '0') {

   $batch_list = getBatchList($service_id, $startDateConv, $endDateConv);

   $clist = '';
   foreach ($batch_list as $key => $value) {
      $clist .= $value['batch_id'] . ',';
   }
   $clist = substr($clist, 0, -1);

   $sortedData = array();

   //we're gonna use CURL mutli to simulate the MO processing in parralell
   $curl_multi_data = array(array(),array());
   $count = 0;

   foreach ($batch_list as $key => $value)
   {
      if(isset($value['batch_id']) && $value['batch_id'] > 0)
      {
         $sortedData[$value['batch_id']]['DATE'] = $value['batch_created'];
         $sortedData[$value['batch_id']]['CLIENT'] = $value['client_name'];
         $sortedData[$value['batch_id']]['CAMPAIGN'] = $value['campaign_name'];
         $sortedData[$value['batch_id']]['CODE'] = $value['campaign_code'];
         $sortedData[$value['batch_id']]['REF'] = $value['batch_reference'];
         $sortedData[$value['batch_id']]['STATUS'] = $value['batch_submit_status'];
         $sortedData[$value['batch_id']]['TOTAL'] = $value['batch_mtsms_total'];

         $eStartDate = strtotime($value['batch_created']) * 1000;
         $eEndDate = (strtotime($value['batch_created'] . ' + 1 day') - 1) * 1000;

         //get all delivery stats from elasticsearch
         $batch_stats = Batch::getFullSendStats($value['batch_id']);

         $sortedData[$value['batch_id']]['sent'] = $batch_stats['sent'] + $batch_stats['rejected'];
         $sortedData[$value['batch_id']]['mosms'] = 0; //default 0
         $sortedData[$value['batch_id']]['units'] = $batch_stats['units'] + $batch_stats['rejected_units'];
         $sortedData[$value['batch_id']]['pending'] = $batch_stats['pending'];
         $sortedData[$value['batch_id']]['pending_units'] = $batch_stats['pending_units'];
         $sortedData[$value['batch_id']]['pending_codes'] = implode(',', $batch_stats['pending_codes']);
         $sortedData[$value['batch_id']]['delivered'] = $batch_stats['delivered'];
         $sortedData[$value['batch_id']]['delivered_units'] = $batch_stats['delivered_units'];
         $sortedData[$value['batch_id']]['delivered_codes'] = implode(',', $batch_stats['delivered_codes']);
         $sortedData[$value['batch_id']]['failed'] = $batch_stats['failed'] + $batch_stats['rejected'];
         $sortedData[$value['batch_id']]['failed_units'] = $batch_stats['failed_units'] + $batch_stats['rejected_units'];
         $sortedData[$value['batch_id']]['failed_codes'] = implode(',', ($batch_stats['failed_codes'] + $batch_stats['rejected_codes']));
         $sortedData[$value['batch_id']]['rejected'] = 0;
         $sortedData[$value['batch_id']]['blacklisted'] = 0;

         $curl_multi_data[$count]['url']  = "https://".$_SERVER['SERVER_NAME'].'/php/ajaxGetBatchMOTotal.php';
         $curl_multi_data[$count]['post'] = array('batch_id' => $value['batch_id']);
         $count ++;

         //$sortedData[$value['batch_id']]['rejected'] = $batch_stats['rejected'];
         //$sortedData[$value['batch_id']]['rejected_codes'] = implode(',', $batch_stats['rejected_codes']);
         //$sortedData[$value['batch_id']]['blacklisted'] = $batch_stats['blacklisted'];
         //$sortedData[$value['batch_id']]['blacklisted_codes'] = implode(',', $batch_stats['blacklisted_codes']);
      }
   }
}

//dont pull MO counts if there is no batch list to pull
if(isset($curl_multi_data[0]) && count($curl_multi_data[0]) > 0)
{
   echo "getting requests";
    //run the MO query through the CURL multi to speed up getting stats for all the batches by doing parallel processing
    $mo_results = helperCurlMulti::multiRequest($curl_multi_data);

    //cycle through all the batch mos and set the value
    foreach($mo_results as $mo_result)
    {
        //convert JSON object to Array because that is how the script is responding
        $json_mo_result = json_decode($mo_result, true);
        if(isset($json_mo_result['success']) && $json_mo_result['success'] === true)
        {
            $sortedData[$json_mo_result['batch_id']]['mosms'] = $json_mo_result['mo_total'];
        }
    }
}


$sort = urlencode('"timestamp":"desc"');
$sents = 0;
?>

<aside class="right-side">
   <section class="content-header">
      <h1>
         Bulk SMS Traffic
         <!--small>Control panel</small-->
      </h1>
   </section>

   <section class="content" style="padding-bottom:0px;">
      <div class="box box-solid" style="padding-top:1px;padding-bottom:1px;margin-bottom:2px;margin-top:2px;">
         <form action="updateservice.php" method="get" id="clientList" class="sidebar-form" style="border:0px;">



            <?php
            if ($singleView != 'block;') {
               ?>
               <div class="callout callout-info" style="margin-bottom:15px;">
                  <!--h4>Instructions</h4-->
                  <!--p>To view Bulk SMS Uploads, select a date range from your Calendar control.</p-->
                  <p>In order to view SMS content you have to click on the corresponding value in the relevant column.</p>

                  <p>To ABORT a bulk SMS campaign, click on EDIT under Status for the campaign.</p>
                  <!--p>Status column will indicate what status your Bulk SMS Upload is currently in.</p-->
               </div>
               <?php
            } else {
               ?>
               <div class="callout callout-info" style="margin-bottom:15px;">

                  <!--p>To view, select a date range from your Calendar control and then select a service form the Service List.</p-->
                  <p>In order to view SMS content you have to click on the corresponding value in the relevant column.</p>
                  <p>To ABORT a bulk SMS campaign, click on EDIT under Status for the campaign.</p>
                  <!--p>Status column will indicate what status your Bulk SMS Upload is currently in.</p-->
               </div>
               <?php
            }
            ?>


            <!--div class="callout callout-warning">
                <h4>Note</h4>
                <p>Please note in this section you will only be able to view SMSs that were sent via Bulk SMS Menu!</p>
                 <div style="float:right; margin-top:-38px;margin-right: 10px;">
                    <i class="fa fa-exclamation-triangle" style="color:#d1ba8a; font-size:20pt;"></i>
                </div>
            </div-->
            <div class="form-group">
               <label>Select Date Range</label><br>
               <div id="reportrange" class="select" onmouseover="" style="cursor: pointer;">
                  <i class="fa fa-calendar fa-lg"></i>
                  <span style="font-size:15px"><?php echo htmlentities($startDateConv).' - '.htmlentities($endDateConv); ?></span><b class="caret"></b>
               </div>
               <small class="text-warning">N.B. Selecting a long date range will take longer to load.</small>
            </div>
            <div class="form-group" style="display:<?php echo $singleView; ?>">
               <label>Client List</label>
               <select class="form-control" name="client" id="client" form="clientList" OnChange="reloadOnSelect(this.value);">
                  <?php
                  echo '<option SELECTED>Please select a client...</option>';
                  foreach ($clients as $key => $service) {
                     if ($service_id == $service['service_id'])
                     {
                        echo "<option value='" . $service['service_id'] . "' SELECTED>" . htmlentities($service['account_name'] . " - " . $service['service_name'] . " - " . $sId) . "</option>";
                     }
                     else
                     {
                        echo "<option value='" . $service['service_id'] . "'>" . htmlentities($service['account_name'] . " - " . $service['service_name'] . " - " . $sId) . "</option>";
                     }
                  }
                  ?>
               </select>
            </div>
         </form>
      </div>
   </section>

   <section class="content" style="margin-top:0px;">

      <div class="box box-connet" style="padding-bottom:1px; border-top-color:<?php echo $accRGB; ?>;">
         <div class="box-header">
            <h3 class="box-title">
               <a class="btn btn-block btn-social btn-dropbox " onclick="location.reload();" style="background-color: <?php echo $accRGB; ?>; color: #ffffff; border: 2px solid; border-radius: 6px !important; float:left; width: 107px; height:36px; margin-top:0px;">
                  <i class="fa fa-refresh"></i>Refresh
               </a>
            </h3>
         </div><!-- /.box-header -->
         <div class="box-body table-responsive">
            <table id="example2" class="table table-bordered table-striped dataTable table-hover">
               <thead>
                  <tr>
                     <th style="width:70px;vertical-align:middle">Batch Id</th>
                     <th style="vertical-align:middle">Date&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</th>
                     <th style="vertical-align:middle">Client</th>
                     <th style="vertical-align:middle">Campaign</th>
                     <th style="vertical-align:middle">Code</th>
                     <th style="vertical-align:middle">Reference</th>
                     <th style="width:50px;vertical-align:middle">Submitted&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</th>
                     <th style="background-color:#737AA6;width:85px;vertical-align:middle">Sent&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</th>
                     <th style="background-color:#DEA52F;width:85px;vertical-align:middle">Pending&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</th>
                     <th style="background-color:#8BA808;width:85px;vertical-align:middle">Delivered&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</th>
                     <th style="background-color:#f56954;width:85px;vertical-align:middle">Failed&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</th>
                     <th style="background-color:#92e7ed;width:50px;vertical-align:middle">Replies&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</th>
                     <th style="width:90px;vertical-align:middle">Status</th>
                  </tr>
               </thead>
               <tbody>
                  <?php
                  if (isset($sortedData) && $sortedData != '')
                  {
                     foreach ($sortedData as $key => $value) {
                        /* echo '<pre>';
                          print_r($value);
                          echo '</pre>'; */
                        echo '<tr>';
                        echo '<td style="vertical-align:middle">' . htmlentities($key) . '</td>';
                        echo '<td style="vertical-align:middle">' . htmlentities($value['DATE']) . '</td>';
                        echo '<td style="vertical-align:middle">' . htmlentities($value['CLIENT']) . '</td>';
                        echo '<td style="vertical-align:middle">' . htmlentities($value['CAMPAIGN']) . '</td>';
                        echo '<td style="vertical-align:middle">' . htmlentities($value['CODE']) . '</td>';
                        echo '<td style="vertical-align:middle">' . htmlentities($value['REF']) . '</td>';
                        //total sends and units
                        echo '<td style="vertical-align:middle;text-align:right;">' . number_format($value['TOTAL'], 0, ',', ' ') . '</td>';

                        //for sent count
                        if ($value['STATUS'] == 'Completed')
                        {
                           echo '<td style="vertical-align:middle;background-color:#adb1cb;text-align:right;"><a style="color:#000000;cursor:pointer;" onclick="viewMsg(`Sent`, ' . $key . ', `' . urlencode($value['DATE']) . '`, `' . urlencode($value['CLIENT']) . '`, `' . urlencode($value['CAMPAIGN']) . '`, `' . urlencode($value['CODE']) . '`, `' . urlencode($value['REF']) . '`);">' . number_format($value['sent'], 0, '.', ' ') . ' <br/><small>(' . number_format($value['units'], 0, '.', ' ') . ' units)</small></a></td>';
                        }
                        else
                        {
                           echo '<td style="vertical-align:middle;background-color:#adb1cb;text-align:right;"><a style="color:#000000;">' . number_format($value['sent'], 0, '.', ' ') . ' <small>(' . number_format($value['units'], 0, '.', ' ') . ' units)</small></a></td>';
                        }


                        //for pending
                        if (isset($value['pending']) && $value['pending'] != 0)
                        {
                           echo '<td style="vertical-align:middle;background-color:#e9cb8c;text-align:right;"><a style="color:#000000;cursor:pointer;" onclick="viewMsgVar(`'.$value['pending_codes'].'`, `Pending`, ' . $key . ', `' . urlencode($value['DATE']) . '`, `' . urlencode($value['CLIENT']) . '`, `' . urlencode($value['CAMPAIGN']) . '`, `' . urlencode($value['CODE']) . '`, `' . urlencode($value['REF']) . '`);">' . number_format($value['pending'], 0, '.', ' ') . ' <br/><small>(' . number_format($value['pending_units'], 0, '.', ' ') . ' units)</small></a></td>';
                        }
                        else
                        {
                           echo '<td style="vertical-align:middle;background-color:#e9cb8c;text-align:right;">0<br/><small>(0 units)</small></td>';
                        }

                        //for delivered
                        if (isset($value['delivered']) && $value['delivered'] != 0)
                        {
                           echo '<td style="vertical-align:middle;background-color:#c0d26e;text-align:right;"><a style="color:#000000;cursor:pointer;" onclick="viewMsgVar(`'.$value['delivered_codes'].'`,`Delivered`, ' . $key . ', `' . urlencode($value['DATE']) . '`, `' . urlencode($value['CLIENT']) . '`, `' . urlencode($value['CAMPAIGN']) . '`, `' . urlencode($value['CODE']) . '`, `' . urlencode($value['REF']) . '`);">' . number_format($value['delivered'], 0, '.', ' ') . ' <br/><small>(' . number_format($value['delivered_units'], 0, '.', ' ') . ' units)</small></a></td>';
                        }
                        else
                        {
                           echo '<td style="vertical-align:middle;background-color:#c0d26e;text-align:right;">0<br/><small>(0 units)</small></td>';
                        }

                        //for failuers
                        if (isset($value['failed']) && $value['failed'] != 0)
                        {
                           echo '<td style="vertical-align:middle;background-color:#f8b6ac;text-align:right;"><a style="color:#000000;cursor:pointer;" onclick="viewMsgVar(`'.$value['failed_codes'].'`,`Failed`, ' . $key . ', `' . urlencode($value['DATE']) . '`, `' . urlencode($value['CLIENT']) . '`, `' . urlencode($value['CAMPAIGN']) . '`, `' . urlencode($value['CODE']) . '`, `' . urlencode($value['REF']) . '`);">' . number_format($value['failed'], 0, '.', ' ') . ' <br/><small>(' . number_format($value['failed_units'], 0, '.', ' ') . ' units)</small></a></td>';
                        }
                        else
                        {
                           echo '<td style="vertical-align:middle;background-color:#f8b6ac;text-align:right;">0<br/><small>(0 units)</small></td>';
                        }

                        /*
                        if (isset($value['rejected']))
                        {
                           echo '<td style="vertical-align:middle;background-color:#df8c8f;text-align:right;"><a style="color:#000000;cursor:pointer;" onclick="viewMsgVar(`176`, `Rejected`, ' . $key . ', `' . $value['DATE'] . '`, `' . htmlspecialchars($value['CLIENT']) . '`, `' . htmlspecialchars($value['CAMPAIGN']) . '`, `' . htmlspecialchars($value['CODE']) . '`, `' . htmlspecialchars($value['REF']) . '`);">' . number_format($value['rejected'], 0, '.', ' ') . '</a></td>';
                        }
                        else
                        {
                           echo '<td style="vertical-align:middle;background-color:#df8c8f;text-align:right;">0</td>';
                        }


                        if (isset($value['blacklisted']))
                        {
                           echo '<td style="vertical-align:middle;background-color:#aaaaaa;text-align:right;"><a style="color:#000000;cursor:pointer;" onclick="gotoMos(' . $key . ', \'BLACKLIST\', `' . $value['DATE'] . '`, `' . htmlspecialchars($value['CLIENT']) . '`, `' . htmlspecialchars($value['CAMPAIGN']) . '`, `' . htmlspecialchars($value['CODE']) . '`, `' . htmlspecialchars($value['REF']) . '`);">' . number_format($value['blacklisted'], 0, '.', ' ') . '</a></td>';
                        }
                        else
                        {
                           echo '<td style="vertical-align:middle;background-color:#aaaaaa;text-align:right;">0</td>';
                        }
                        */

                        if (isset($value['mosms']))
                        {
                           echo '<td style="vertical-align:middle;background-color:#cfebed;text-align:right;"><a style="color:#000000;cursor:pointer;" onclick="gotoMos(' . $key . ', \'MOSMS\', `' . $value['DATE'] . '`, `' . $service_id . '`, `' . htmlspecialchars($value['CAMPAIGN']) . '`, `' . htmlspecialchars($value['CODE']) . '`, `' . htmlspecialchars($value['REF']) . '`);">' . number_format($value['mosms'], 0, '.', ' ') . '</a></td>';
                        }
                        else
                        {
                           echo '<td style="vertical-align:middle;background-color:#cfebed;text-align:right;">0</td>';
                        }


                        if ($value['STATUS'] == 'Sending' || $value['STATUS'] == 'SEND') {
                           echo '<td style="vertical-align:middle;text-align:left;padding-left:10px;"><i style="color:#993311;" class="fa fa-cog fa-spin"></i>&nbsp;<a style="color:#000000;cursor:pointer;" onclick="gotoPause(' . $key . ')">Sending</a></td>';
                        } elseif ($value['STATUS'] == 'Paused' || $value['STATUS'] == 'PAUSE') {
                           echo '<td style="vertical-align:middle;text-align:left;padding-left:10px;"><i style="color:#e3aa2f;" class="fa fa-warning"></i>&nbsp;<a style="color:#000000;cursor:pointer;" onclick="gotoPause(' . $key . ')">Paused</a></td>';
                        } elseif ($value['STATUS'] == 'ABORT') {
                           echo '<td style="vertical-align:middle;text-align:left;padding-left:10px;"><i style="color:#550000;" class="fa fa-times"></i>&nbsp;<a style="color:#000000;cursor:pointer;" onclick="gotoPause(' . $key . ')">Aborted</a></td>';
                        } elseif ($value['STATUS'] == 'DRAFT') {
                           echo '<td style="vertical-align:middle;text-align:left;padding-left:10px;"><i style="color:#e3aa2f;" class="fa fa-file"></i>&nbsp;<a style="color:#000000;cursor:pointer;" onclick="gotoPause(' . $key . ')">Drafted</a></td>';
                        } elseif ($value['STATUS'] == 'Processing') {
                           echo '<td style="vertical-align:middle;text-align:left;padding-left:10px;"><i style="color:#e3aa2f;" class="fa fa-spinner"></i>&nbsp;<a style="color:#000000;cursor:pointer;" onclick="gotoPause(' . $key . ')">Processing</a></td>';
                        } elseif ($value['STATUS'] == 'Blacklist') {
                           echo '<td style="vertical-align:middle;text-align:left;padding-left:10px;"><i style="color:#000000" class="fa fa-ban"></i>&nbsp;<a style="color:#000000;cursor:pointer;" onclick="gotoPause(' . $key . ')">Blacklisted</a></td>';
                        } elseif ($value['STATUS'] == 'None') {
                           echo '<td style="vertical-align:middle;text-align:left;padding-left:10px;"><i style="color:#e3aa2f;" class="fa fa-circle-thin"></i>&nbsp;<a style="color:#000000;cursor:pointer;" onclick="gotoPause(' . $key . ')">None</a></td>';
                        } elseif ($value['STATUS'] == 'SCHEDULE') {
                           //echo '<td style="text-align:left;padding-left:10px;"><i style="color:#e3aa2f;" class="fa fa-warning"></i>&nbsp;<a style="color:#000000;cursor:pointer;" onclick="gotoPause('.$key.')">'.$value['STATUS'].'</a></td>';
                           echo '<td style="vertical-align:middle;text-align:left;padding-left:10px;"><i style="color:#28628a;" class="fa fa-clock-o"></i>&nbsp;<a style="color:#000000;cursor:pointer;" onclick="gotoPause(' . $key . ')">Scheduled</a></td>';
                        } elseif ($value['STATUS'] == 'Completed') {
                           echo '<td style="vertical-align:middle;text-align:left;padding-left:10px;"><i style="color:#558e66;" class="fa fa-check-square"></i>&nbsp;<a style="color:#000000;cursor:pointer;" onclick="gotoPause(' . $key . ')">' . htmlentities($value['STATUS']) . '</a></td>';
                        } elseif ($value['STATUS'] == 'Error') {
                           echo '<td style="vertical-align:middle;text-align:left;padding-left:10px;"><i style="color:#aa0000;" class="fa fa-exclamation-circle"></i>&nbsp;<a style="color:#000000;cursor:pointer;" onclick="gotoPause(' . $key . ')">' . htmlentities($value['STATUS']) . '</a></td>';
                        } else {
                           echo '<td style="vertical-align:middle;text-align:left;padding-left:10px;"><a style="color:#000000;cursor:pointer;" onclick="gotoPause(' . $key . ')">' . $value['STATUS'] . '</a></td>';
                        }
                        echo '</tr>';
                     }
                  }
                  else
                  {
                     if (isset($batch_list) && $batch_list != '') {
                        foreach ($batch_list as $key => $value) {
                           echo '<tr>';
                           echo '<td>' . htmlentities($value['batch_id']) . '<td>';
                           echo '<td>' . htmlentities($value['batch_created']) . '<td>';
                           echo '<td>' . htmlentities($value['client_name']) . '<td>';
                           echo '<td>' . htmlentities($value['campaign_name']) . '<td>';
                           echo '<td>' . htmlentities($value['campaign_code']) . '<td>';
                           echo '<td>' . htmlentities($value['batch_reference']) . '<td>';
                           echo '<td>-<td>';
                           echo '<td>-<td>';
                           echo '<td>-<td>';
                           echo '<td>-<td>';
                           echo '<td>-<td>';
                           echo '<td>-<td>';
                           echo '<td>-<td>';
                           echo '<td>-<td>';
                           echo '<td>' . htmlentities($value['batch_submit_status']) . '<td>';
                           echo '</tr>';
                        }
                     }
                  }
                  ?>
               </tbody>
            </table>
         </div><!-- /.box-body -->
         <div class="box-body">
            <div class="form-group">
               <a href="../php/createCSVBatchList.php?service_id=<?php echo urlencode($service_id); ?>&service_name=<?php echo urlencode($_SESSION['serviceName']);?>&start_date=<?php echo urlencode($startDateConv); ?>&end_date=<?php echo urlencode($endDateConv); ?>" class="btn btn-block btn-social btn-dropbox " style="background-color:<?php echo $accRGB; ?>; border: 2px solid; border-radius: 6px !important; float:left; width: 134px; margin-top:-21px;">
                  <i class="fa fa-save"></i>Save Report
               </a>
            </div>
         </div>
      </div>
   </section>
</aside>

<div class="loader" style="display:none;"></div>
<div class="loaderIcon" style="display:none;"></div>

<!--  THE JAVASCRIPT IMPORT ALWAYS APPEARS BEFORE LOCAL JAVASCRIPT AT THE BOTTOM OF THE PAGE -->
<?php include("template_import_script.php"); //must import all scripts first  ?>
<!-- END JAVASCRIPT IMPORT -->

<script type="text/javascript">
   $(function ()
   {
      $('#example2').dataTable({
         "bPaginate": true,
         "bLengthChange": false,
         "bFilter": true,
         "bSort": true,
         "bInfo": true,
         "bAutoWidth": false,
         "iDisplayLength": 100,
         "aoColumns": [
            {"sType": 'formatted-num', targets: 0}, //NIEL
            null,
            null,
            null,
            null,
            null,
            {"sType": 'formatted-num', targets: 0},
            {"sType": 'formatted-num', targets: 0},
            {"sType": 'formatted-num', targets: 0},
            {"sType": 'formatted-num', targets: 0},
            {"sType": 'formatted-num', targets: 0},
            {"sType": 'formatted-num', targets: 0},
            {"sType": 'formatted-num', targets: 0},
            {"sType": 'formatted-num', targets: 0},
            null
         ],
         "order": [[3, "desc"]]
      });
   });

   jQuery.extend(jQuery.fn.dataTableExt.oSort,
           {
              "title-numeric-pre": function (a) {
                 var x = a.match(/title="*(-?[0-9\.]+)/)[1];
                 return parseFloat(x);
              },
              "title-numeric-asc": function (a, b) {
                 return ((a < b) ? -1 : ((a > b) ? 1 : 0));
              },
              "title-numeric-desc": function (a, b) {
                 return ((a < b) ? 1 : ((a > b) ? -1 : 0));
              }
           });

   jQuery.extend(jQuery.fn.dataTableExt.oSort,
           {
              "formatted-num-pre": function (a) {
                 a = (a === "-" || a === "") ? 0 : a.replace(/[^\d\-\.]/g, "");
                 return parseFloat(a);
              },
              "formatted-num-asc": function (a, b) {
                 return a - b;
              },
              "formatted-num-desc": function (a, b) {
                 return b - a;
              }
           });
</script>

<script type="text/javascript" src="../js/plugins/daterangepicker/moment.js"></script>

<script type="text/javascript" src="../js/plugins/daterangepicker/daterangepickerNetTraffic.js"></script>

<script type="text/javascript">
   $('#reportrange').daterangepicker(
       {
          ranges: {
             'Today': [moment(), moment()],
             'Yesterday': [moment().subtract('days', 1), moment().subtract('days', 1)],
             'Last 7 Days': [moment().subtract('days', 6), moment()],
             'Last 30 Days': [moment().subtract('days', 29), moment()],
             'This Month': [moment().startOf('month'), moment().endOf('month')],
             'Last Month': [moment().subtract('month', 1).startOf('month'), moment().subtract('month', 1).endOf('month')]
          },
          startDate: moment('<?php echo isset($startDateConv) ? $startDateConv : ''; ?>'),
          endDate: moment('<?php echo isset($endDateConv) ? $endDateConv : ''; ?>')
       },
     function (start, end) {
        $('#reportrange span').html(start.format('YYYY-MM-DD') + ' - ' + end.format('YYYY-MM-DD'));

        $(".loader").fadeIn("slow");
        $(".loaderIcon").fadeIn("slow");

        var serviceName = document.getElementById("client").value;

        var startDateCal = start.format('YYYY-MM-DD');
        startDateCal = moment(startDateCal).unix() * 1000;

        var endDateCal = end.format('YYYY-MM-DD');
        endDateCal = moment(endDateCal).unix() * 1000;

        window.location = "../pages/batchlist.php?client=" + encodeURIComponent(serviceName) + "&start_date=" + startDateCal + "&end_date=" + endDateCal;
     }
   );
</script>

<script type="text/javascript">
   var startDate = "<?php echo urlencode($startDate); ?>";
   var endDate = "<?php echo urlencode($endDate); ?>";

   function gotoPause(bId)
   {
      window.location = "../pages/pause.php?service_id=<?php echo(isset($service_id) ? $service_id : 0); ?>&bId=" + bId + "&start_date=" + startDate + "&end_date=" + endDate;
   }

   function gotoMos(v, t, d, serviceId, camp, code, ref)
   {
      var client = "<?php echo urlencode($_GET['client']); ?>";
      var bId = v;
      var type = t;

      window.location = "../pages/batchlistmo.php?client=" + encodeURIComponent(client) + "&start_date=" + startDate + "&end_date=" + endDate + "&service_id=<?php echo(isset($service_id) ? $service_id : 0); ?>&camp=" + camp + "&code=" + code + "&ref=" + ref + "&bId=" + bId + "&type=" + type + "&rr=" + d;
   }

   function reloadOnSelect(serviceId)
   {
      $(".loader").fadeIn("slow");
      $(".loaderIcon").fadeIn("slow");

      window.location = "../pages/batchlist.php?service_id=" + serviceId + "&start_date=" + startDate + "&end_date=" + endDate;
   }

   function viewMsg(type, bId, d, client, camp, code, ref) {
      $(".loader").fadeIn("slow");
      $(".loaderIcon").fadeIn("slow");

      window.location = "../pages/batchlistmsg.php?service_id=<?php echo(isset($service_id) ? $service_id : 0); ?>&type=" + type + "&cli=" + client + "&camp=" + camp + "&code=" + code + "&ref=" + ref + "&bId=" + bId + "&start_date=" + startDate + "&end_date=" + endDate + "&sents=" + <?php echo urlencode($sents); ?> +"&from=1&controller=1&sort=<?php echo $sort; ?>" + "&rr=" + d;
   }

   function viewMsgVar(codes, type, bId, d, client, camp, code, ref)
   {
      $(".loader").fadeIn("slow");
      $(".loaderIcon").fadeIn("slow");
      window.location = "../pages/batchlistmsg.php?query="+encodeURI(codes)+"&service_id=<?php echo(isset($service_id) ? $service_id : 0); ?>&type=" + type + "&cli=" + client + "&camp=" + camp + "&code=" + code + "&ref=" + ref + "&bId=" + bId + "&start_date=" + startDate + "&end_date=" + endDate + "&sents=" + <?php echo urlencode($sents); ?> + "&from=1&controller=1&sort=<?php echo $sort; ?>&rr=" + d;
   }
</script>

<script type="text/javascript">
   $(window).load(function () {
      $(".loader").fadeOut("slow");
      $(".loaderIcon").fadeOut("slow");
   })
</script>

<!-- Template Footer -->

