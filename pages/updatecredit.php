<?php
/**
 * Include the header template which sets up the HTML
 *
 * Don't forget to include template_import_script before any Javascripting
 * Don't forget to include template footer.php at the bottom of the page as well
 */
$headerPageTitle = "Update Credits"; //set the page title for the template import
TemplateHelper::initialize();



//$clients = getServicesInAccount(null);

if (isset($_SESSION['serviceId']) && $_SESSION['serviceId'] != '') {
   $clients = getServicesInAccountWithOutRid($_SESSION['accountId']);
} else {
   $clients = getClients();
}

$err1 = 0;
$err2 = 0;
$succ = 0;
$height = 300;

if (isset($_GET['client']) && $_GET['client'] != 'Please Select') {
   $fileName = $_GET['client'];
   $cl = $_GET['client'];
   $clp = strrpos($cl, ' ');
   $cl = trim(substr($cl, $clp, strlen($cl)));
   $prevFund = getFundsOnly($cl);
   $prevFund = $prevFund / 10000;
   $prevFund = round($prevFund, 2);
   //$dsp = 'inline';
   //$dir = '../img/routes';
   //$serProviders = scandir($dir);
}

if (isset($_GET['ammount'])) {
   $ammount = $_GET['ammount'];
} else {
   $ammount = '';
}

if (isset($_GET['desc'])) {
   $desc = $_GET['desc'];
} else {
   $desc = '';
}

if (isset($_POST['clientName'])) {
   //echo '<br><br>C-->'.$_POST['prevAm'];
   //echo '<br><br>A-->'.$_POST['ammount'];
   /* echo '<br><br>D-->'.$_POST['desc']; */

   $cl = strrpos($_POST['clientName'], ' ');
   $cl = substr($_POST['clientName'], $cl + 1, strlen($_POST['clientName']));

   $prevFund = getFundsOnly($cl);

   if (isset($_POST['ammount']) && ($_POST['ammount'] > 0) && (isset($_POST['desc']) && $_POST['desc'] != '')) {
      /*    $amount = ($_POST['ammount']*10000);
        //echo '<form method="POST" id="conf" action="confirmcredits.php?clientName='.urlencode($_POST["clientName"]).'&ammount='.$_POST["ammount"].'&desc='.$_POST["desc"].'">';
        //createPayment($cl, $_SESSION['userId'], $amount, $_POST['desc'], $prevFund);
        echo '<script type="text/javascript">';
        echo "gotoConfirm('".$_POST["clientName"]."', '".$_POST['prevAm']."', '".$_POST["ammount"]."', '".$_POST['desc']."');";
        echo '</script>';
        //$succ = 1; */
   } else {
      // $err2 = 1;
   }

   //$prevFund = getFundsOnly($cl);
   $prevFund = $prevFund / 10000;
   $prevFund = round($prevFund, 2);
} else {
   //$err1 = 1;
}


if (isset($prevFund)) {
   $height = 356;
}
/* echo '<pre>';
  print_r($_POST);
  echo '</pre>'; */


/* $sql = 'INSERT INTO `core_service_payment` (service_id, user_id, payment_value, payment_description,payment_previous_balance) 
  VALUES (:service_id, :user_id, :payment_value, :payment_description,:payment_previous_balance)';

  $sql = mysql_encode_param_array($sql, array(
  'service_id' => $selected_service->service_id,
  'user_id' => $_SESSION['auth']['user']->user_id,
  'payment_value' => $request->payment_value,
  'payment_description' => $request->description,
  'payment_previous_balance' => $selected_service->service_credit_available,
  )); */
?>

<aside class="right-side">
   <section class="content-header">
      <h1>
         Update Credits
         <!--small>Control panel</small-->
      </h1>
   </section>
   <br>
   <section class="content">
      <div class="row col-lg-12">
         <div class="box box-connet" id="boxBG" style="height:<?php echo $height; ?>px; margin-top:-15px;display:<?php echo $singleView; ?>; border-top-color:<?php echo $accRGB; ?>;">
            <form action="updatecredit.php" method="post" id="updCredit" class="sidebar-form" style="border:0px;">
               <div class="form-group">
                  <label>Client List</label>
                  <select class="form-control" id="clientName" name="clientName" OnChange="reloadOnSelect(this.value);">
                     <?php
                     if ($cl == '0') {
                        echo '<option SELECTED>Please select a client...</option>';
                     }
                     foreach ($clients as $key => $value) {
                        $sId = $value['service_id'];
                        if ($cl == $sId) {
                           $accountName = $value['account_name'];
                           $serviceName = $value['service_name'];
                           echo "<option name='" . $sId . "' SELECTED>" . $accountName . " - " . $serviceName . " - " . $sId . "</option>";
                        } else {
                           echo "<option name='" . $sId . "'>" . $value['account_name'] . " - " . $value['service_name'] . " - " . $sId . "</option>";
                        }
                     }
                     ?>
                  </select>
               </div>
               <div class="form-group">
                  <div class="box box-solid">
                     <div class="form-group" style="padding:10px;">
                        <?php
                        if (isset($prevFund)) {
                           echo '<label>Current Balance (Balance before payment)</label>';
                           echo '<p>R ' . $prevFund . '</p>';
                           //      echo '<br>';
                        }
                        ?>
                        <input type="hidden" id="prevAm" name="prevAm" value="<?php echo $prevFund; ?>">
                        <label>Payment Amount (Credit to load in RAND value) e.g. 100</label>
                        <input type="text" id="ammount" class="form-control" onkeypress="return isNumberKey(event)" style="border:1px solid #929292;margin-top:5px;height:35px;" name="ammount" placeholder="" value="<?php echo $ammount; ?>">
                        <br>
                        <label>Description</label>
                        <input type="text" id="desc" class="form-control" style="border:1px solid #929292;margin-top:5px;height:35px;" name="desc" placeholder="" value="<?php echo $desc; ?>">
                     </div>
                  </div>
               </div>

               <div id="errAlert" class="callout callout-danger" style="margin-top:-13px; display:none;">
                  Alert:<b>&nbsp;</b>Please fill in all the above fields!
                  <div style="float:right; margin-top:-3px">
                     <i class="fa fa-times" style="color:#cc3333; font-size:20pt;"></i>
                  </div>
               </div>

               <div id="errAlert2" class="callout callout-danger" style="margin-top:-13px; display:none;">
                  Alert:<b>&nbsp;</b>You are trying to assign more credit than you currently have available!
                  <div style="float:right; margin-top:-3px">
                     <i class="fa fa-times" style="color:#cc3333; font-size:20pt;"></i>
                  </div>
               </div>
               <div class="box-body">
                  <div class="form-group" style="padding-top:10px;">
                     <a class="btn btn-block btn-social btn-dropbox " onclick="checkFields();" style="background-color: <?php echo $accRGB; ?>; color: #ffffff; border: 2px solid; border-radius: 6px !important; float:left; width: 145px; height:36px; margin-top:-25px;padding-left:10px;">
                        <i class="fa fa-credit-card"></i>Update Credit
                     </a>
                  </div>
               </div>
            </form>
         </div>
      </div>
   </section>

</aside>

<!--  THE JAVASCRIPT IMPORT ALWAYS APPEARS BEFORE LOCAL JAVASCRIPT AT THE BOTTOM OF THE PAGE -->
<?php include("template_import_script.php"); //must import all scripts first  ?>
<!-- END JAVASCRIPT IMPORT -->

<script type="text/javascript">
   function reloadOnSelect(name)
   {
      $(".loader").fadeIn("slow");
      $(".loaderIcon").fadeIn("slow");

      //var serviceName = name;

      form = document.createElement('form');
      form.setAttribute('method', 'POST');
      form.setAttribute('action', '../pages/updatecredit.php');
      myvar = document.createElement('input');
      myvar.setAttribute('name', 'clientName');
      myvar.setAttribute('type', 'hidden');
      myvar.setAttribute('value', name);
      form.appendChild(myvar);
      document.body.appendChild(form);
      form.submit();

      //var repRange = $("#reportrange span").daterangepicker( 'getDate' ).html();
      //window.location = "../pages/batchlist.php?client=" + serviceName + "&range=" + repRange;
   }

   function gotoConfirm(namer, prevAm, amm, descr)
   {
      $(".loader").fadeIn("slow");
      $(".loaderIcon").fadeIn("slow");

      // var serviceName = name;

      form = document.createElement('form');
      form.setAttribute('method', 'POST');
      form.setAttribute('action', '../pages/confirmcredit.php');

      nameC = document.createElement('input');
      nameC.setAttribute('name', 'clientName');
      nameC.setAttribute('type', 'hidden');
      nameC.setAttribute('value', namer);
      form.appendChild(nameC);
      prevAmount = document.createElement('input');
      prevAmount.setAttribute('name', 'prevAmount');
      prevAmount.setAttribute('type', 'hidden');
      prevAmount.setAttribute('value', prevAm);
      form.appendChild(prevAmount);
      amount = document.createElement('input');
      amount.setAttribute('name', 'ammount');
      amount.setAttribute('type', 'hidden');
      amount.setAttribute('value', amm);
      form.appendChild(amount);
      desc = document.createElement('input');
      desc.setAttribute('name', 'desc');
      desc.setAttribute('type', 'hidden');
      desc.setAttribute('value', descr);
      form.appendChild(desc);

      document.body.appendChild(form);
      form.submit();

      //var repRange = $("#reportrange span").daterangepicker( 'getDate' ).html();
      //window.location = "../pages/batchlist.php?client=" + serviceName + "&range=" + repRange;
   }
</script>

<script type="text/javascript">
   function checkFields()
   {
      var cln = document.getElementById("clientName").value;
      var am = document.getElementById("ammount").value;
      var des = document.getElementById("desc").value;
      var avail = <?php echo round(($_SESSION['serviceCredit'] / 10000), 2); ?>

      /*alert('1' + cln);
       alert('2' + am);
       alert('3' + des);*/

      if ((cln != 'Please select a client...') && (am != '') && (am != 0) && (des != ''))
      {
         if (am > avail)
         {
            var box = document.getElementById("boxBG");
            var newH = parseInt(box.style.height) + 58;
            box.style.height = newH + "px";
            var err = document.getElementById("errAlert2");
            err.style.display = 'block';
         } else
         {
            document.forms['updCredit'].submit();
         }
      } else
      {
         var box = document.getElementById("boxBG");
         var newH = parseInt(box.style.height) + 58;
         box.style.height = newH + "px";
         var err = document.getElementById("errAlert");
         err.style.display = 'block';

      }



   }
</script>

<script type="text/javascript">
   $(window).load(function () {
      $(".loader").fadeOut("slow");
      $(".loaderIcon").fadeOut("slow");
   })
</script>

<SCRIPT language=Javascript>
   function isNumberKey(evt)
   {
      var charCode = (evt.which) ? evt.which : evt.keyCode;

      if (charCode == 45)
         return true;

      if (charCode != 46 && charCode > 31
              && (charCode < 48 || charCode > 57))
         return false;

      return true;
   }
</SCRIPT>
<?php
if (isset($_POST['clientName'])) {
   //echo '<br><br>C-->'.$_POST['prevAm'];
   //echo '<br><br>A-->'.$_POST['ammount'];
   /* echo '<br><br>D-->'.$_POST['desc']; */
   /*
     $cl = strrpos($_POST['clientName'], ' ');
     $cl = substr($_POST['clientName'], $cl+1, strlen($_POST['clientName']));

     $prevFund = getFundsOnly($cl);
    */
   if (isset($_POST['ammount']) && ($_POST['ammount'] != 0) && (isset($_POST['desc']) && $_POST['desc'] != '')) {
      $amount = ($_POST['ammount'] * 10000);
      //echo '<form method="POST" id="conf" action="confirmcredits.php?clientName='.urlencode($_POST["clientName"]).'&ammount='.$_POST["ammount"].'&desc='.$_POST["desc"].'">';
      //createPayment($cl, $_SESSION['userId'], $amount, $_POST['desc'], $prevFund);
      echo '<script type="text/javascript">';
      echo "gotoConfirm('" . $_POST["clientName"] . "', '" . $_POST['prevAm'] . "', '" . $_POST["ammount"] . "', '" . $_POST['desc'] . "');";
      echo '</script>';
      //$succ = 1;
   }
}
?>

<!-- Template Footer -->


