<?php
/**
 * Include the header template which sets up the HTML
 *
 * Don't forget to include template_import_script before any Javascripting
 * Don't forget to include template footer.php at the bottom of the page as well
 */
$headerPageTitle = "Edit Roles"; //set the page title for the template import
TemplateHelper::initialize();


if (isset($_GET['rId'])) {
   editRole($_GET['rId'], $_GET['nIcon'], $_GET['nName'], $_GET['nUrl'], $_GET['nType'], $_GET['nOrder']);
}

if (isset($_GET['addnId'])) {
   $insertSuccess = insertNewRole($_GET['addnId'], $_GET['nIcon'], $_GET['nName'], $_GET['nUrl'], $_GET['nType'], $_GET['nOrder']);
}

$allRoles = getAllRoles();

/* echo '<pre>';
  print_r($allRoles);
  echo '</pre>'; */
?>

<aside class="right-side">
   <section class="content-header">
      <h1>
         Edit Roles
         <!--small>Control panel</small-->
      </h1>
   </section>

   <section class="content">
      <div class="row col-lg-12">
         <div class="box box-connet" style="padding-bottom:1px; border-top-color:<?php echo $accRGB; ?>; height:auto; display:<?php echo $showRoles; ?>" >
            <div class="box-header">
               <h3 class="box-title">Editable Roles:</h3>
            </div>

            <div class="box-body" style="padding:0px;">
               <?php
               if (isset($insertSuccess) && ($insertSuccess == 0)) {
                  echo '<div class="callout callout-danger">';
                  echo "<b>Error:&nbsp;&nbsp;id that you specified already exists.</b>";
                  echo '<div style="float:right; margin-top:-3px">';
                  echo '<i class="fa fa-exclamation-triangle" style="color:#dFb5b4; font-size:20pt;"></i>';
                  echo '</div>';
                  echo '</div>';
               }
               if (isset($insertSuccess) && ($insertSuccess == 1)) {
                  echo '<div class="callout callout-success">';
                  echo "You have successfully added a new role.<b></b>";
                  echo '<div style="float:right; margin-top:-3px">';
                  echo '<i class="fa fa-check" style="color:#5cb157; font-size:20pt;"></i>';
                  echo '</div>';
                  echo '</div>';
               }
               ?>
               <div class="box-body table-responsive">
                  <table id="example2" class="table table-bordered table-striped dataTable table-hover">
                     <thead>
                        <tr>

                           <th>id</th>
                           <th>Role Icon</th>
                           <th>Role Name</th>
                           <th>Role URL</th>
                           <th>Type</th>
                           <th>Order</th>
                           <th>Remove</th>

                        </tr>
                     </thead>

                     <tbody>
                        <?php
                        if (isset($allRoles)) {
                           foreach ($allRoles as $key => $value) {
                              $v = $value['role_id'] % 100;

                              if ($v == 0) {
                                 $color = '#bbbbbb;';
                              } else {
                                 $color = '#f9f9f9;';
                              }

                              echo '<tr name="rollId" value="' . $value['role_id'] . '">';

                              echo '<td style="align:center;background-color:' . $color . '"><center>' . $value['role_id'] . '</center></td>';

                              echo '<td style="background-color:' . $color . '"><center><i class="' . $value['role_icon'] . '"></i>&nbsp;&nbsp;&nbsp;&nbsp;<input style="text-align:left;width:200px;" type="text" id="role_icon' . $value['role_id'] . '" value="' . $value['role_icon'] . '"></center></td>';

                              echo '<td style="background-color:' . $color . '"><input style="text-align:left;width:220px;" type="text" id="role_name' . $value['role_id'] . '" value="' . $value['role_name'] . '"></td>';
                              if ($v != 0) {
                                 echo '<td style="background-color:' . $color . '"><input style="text-align:left; width:240px;" type="text" id="role_url' . $value['role_id'] . '" value="' . $value['role_url'] . '"></td>';
                              } else {
                                 echo '<td style="background-color:' . $color . ';"></td>';
                              }
                              echo '<td style="background-color:' . $color . '"><center><input style="text-align:left;width:160px;" type="text" id="role_type' . $value['role_id'] . '" value="' . $value['role_type'] . '"></center></td>';

                              echo '<td style="background-color:' . $color . '"><center><input style="text-align:right;width:60px;" type="text" id="role_order' . $value['role_id'] . '" value="' . $value['role_order'] . '"></center></td>';

                              echo '<td style="background-color:' . $color . ';">
                                                            <center>
                                                                    <a class="btn btn-block btn-social btn-dropbox" id="' . $value['role_id'] . '" onclick="saveRole(' . $value['role_id'] . ')" style="background-color:' . $accRGB . '; color: #ffffff; border: 2px solid; border-radius: 6px !important; font-size:0.8em !important; height:24px; width: 120px; margin-top:0px; padding-top:2px;">
                                                                        <i class="fa fa-save" style="font-size:1.0em !important; margin-top:-6px;"></i>Save Role
                                                                    </a>
                                                            </center>
                                                          </td>';

                              echo '</tr>';
                           }
                        }
                        echo '<tr>';
                        echo '<td style="align:center;background-color:#88ccbb;"><center><input style="text-align:right;width:60px;" type="text" id="nrole_id" value=""></center></td>';

                        echo '<td style="background-color:#88ccbb;"><center><input style="text-align:left;width:200px;" type="text" id="nrole_icon" value=""></center></td>';

                        echo '<td style="background-color:#88ccbb;"><input style="text-align:left;width:220px;" type="text" id="nrole_name" value=""></td>';

                        echo '<td style="background-color:#88ccbb;"><input style="text-align:left; width:240px;" type="text" id="nrole_url" value=""></td>';

                        echo '<td style="background-color:#88ccbb;"><center><input style="text-align:left;width:160px;" type="text" id="nrole_type" value=""></center></td>';

                        echo '<td style="background-color:#88ccbb;"><center><input style="text-align:right;width:60px;" type="text" id="nrole_order" value=""></center></td>';

                        echo '<td style="background-color:#88ccbb;">
                                                        <center>
                                                                <a class="btn btn-block btn-social btn-dropbox" name="nid" onclick="addRole()" style="background-color:' . $accRGB . '; color: #ffffff; border: 2px solid; border-radius: 6px !important; font-size:0.8em !important; height:24px; width: 120px; margin-top:0px; padding-top:2px;">
                                                                    <i class="fa fa-plus" style="font-size:1.0em !important; margin-top:-6px;"></i>Add Role
                                                                </a>
                                                        </center>
                                                      </td>';
                        echo '</tr>';
                        ?>
                     </tbody>
                  </table>

               </div>
            </div>
         </div>
   </section>
</aside>

<!--  THE JAVASCRIPT IMPORT ALWAYS APPEARS BEFORE LOCAL JAVASCRIPT AT THE BOTTOM OF THE PAGE -->
<?php include("template_import_script.php"); //must import all scripts first ?>
<!-- END JAVASCRIPT IMPORT -->

<script type="text/javascript">
   function saveRole(rId)
   {
      /*var x = 'role_id'+rId;
       alert(' x=' + x);*/
      //var nId = document.getElementById('role_id'+rId).value;
      var nIcon = document.getElementById('role_icon' + rId).value;
      var nName = document.getElementById('role_name' + rId).value;
      if (rId % 100 == 0)
      {
         var nUrl = '0';
      } else
      {
         var nUrl = document.getElementById('role_url' + rId).value;
      }
      var nType = document.getElementById('role_type' + rId).value;
      var nOrder = document.getElementById('role_order' + rId).value;
      //alert(' nId=' + nId);
      window.location = "editroles.php?rId=" + rId + "&nIcon=" + nIcon + "&nName=" + nName + "&nUrl=" + nUrl + "&nType=" + nType + "&nOrder=" + nOrder;
   }
   ;

   function addRole()
   {

      var nId = document.getElementById('nrole_id').value;
      var nIcon = document.getElementById('nrole_icon').value;
      var nName = document.getElementById('nrole_name').value;
      //alert(nId%100);
      if (nId % 100 == 0)
      {
         var nUrl = '0';
      } else
      {
         var nUrl = document.getElementById('nrole_url').value;
      }
      var nType = document.getElementById('nrole_type').value;
      var nOrder = document.getElementById('nrole_order').value;
      //alert(' nId=' + nId);
      window.location = "editroles.php?addnId=" + nId + "&nIcon=" + nIcon + "&nName=" + nName + "&nUrl=" + nUrl + "&nType=" + nType + "&nOrder=" + nOrder;
   }
   ;
</script>

<script type="text/javascript">
   $(window).load(function () {
      $(".loader").fadeOut("slow");
      $(".loaderIcon").fadeOut("slow");
   })
</script>

<!-- Template Footer -->

