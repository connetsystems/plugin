<?php
/**
 * Include the header template which sets up the HTML
 *
 * Don't forget to include template_import_script before any Javascripting
 * Don't forget to include template footer.php at the bottom of the page as well
 */
TemplateHelper::setPageTitle('Create Service');
TemplateHelper::initialize();

define('USE_PDO_EXCEPTIONS', true);
         

//$manError = '#aaaaaa';
$typError = '#aaaaaa';
$bilError = '#aaaaaa';
$accError = '#aaaaaa';
$acc2Error = '#aaaaaa';
$snError = '#aaaaaa';
$slError = '#aaaaaa';
$unError = '#aaaaaa';
$passError = '#aaaaaa';
$vodaError = '#aaaaaa';
$mtnError = '#aaaaaa';
$cellError = '#aaaaaa';
$telkError = '#aaaaaa';
$thrOldError = '#aaaaaa';
$thrEmailError = '#aaaaaa';
$thrCellError = '#aaaaaa';

$accountTypes = ['bulk', 'http', 'smpp'];

if (isset($_GET['upd'])) {
   $validationCount = 0;
   $showError = 0;

   if ($_GET['typeCheck'] != 'Please Select') {
      $accType = $_GET['typeCheck'];
      $validationCount++; // 1
   } else {
      $typError = '#ffaaaa';
      $showError = 1;
   }

   if ($_GET['billingType'] != 'Please Select') {
      $bilType = $_GET['billingType'];
      $validationCount++; // 2
   } else {
      $bilError = '#ffaaaa';
      $showError = 1;
   }

   if ($_GET['serviceName'] != '') {
      if ($_GET['serviceName'] != 'Service Name already exists.') {
         if ($_GET['serviceName'] != 'You did not specify a Service Name.') {
            //echo '<br>get='.$_GET['serviceName'];
            $serviceName = checkServiceName($_GET['serviceName']);
            // echo '<br>getCheck='.$serviceName;
            if ($serviceName == 1) {
               $serviceName = $_GET['serviceName'];
               // echo '<br>serviceName='.$serviceName;
               $validationCount++; // 3
            } else {
               $snError = '#ffaaaa';
               $serviceName = $_GET['serviceName'];
               $serviceNameError = 'Service Name already exists.';
               $showError = 1;
            }
         } else {
            $snError = '#ffaaaa';
            $serviceName = $_GET['serviceName'];
            $serviceNameError = 'You did not specify a Service Name.';
            $showError = 1;
         }
      } else {
         $snError = '#ffaaaa';
         $serviceName = $_GET['serviceName'];
         $serviceNameError = 'You did not specify a Service Name.';
         $showError = 1;
      }
   } else {
      $snError = '#ffaaaa';
      $serviceName = $_GET['serviceName'];
      $serviceNameError = 'You did not specify a Service Name.';
      $showError = 1;
   }

   if ($_GET['serviceLogin'] != '') {
      if ($_GET['serviceLogin'] != 'Service Login already exists.') {
         if ($_GET['serviceLogin'] != 'You did not specify a Service Login.') {
            $serviceLogin = checkServiceLogin($_GET['serviceLogin']);
            if ($serviceLogin == 1) {
               $serviceLogin = $_GET['serviceLogin'];
               $validationCount++; // 4
            } else {
               $slError = '#ffaaaa';
               $serviceLogin = $_GET['serviceLogin'];
               $serviceLoginError = 'Service Login already exists.';
               $showError = 1;
            }
         } else {
            $slError = '#ffaaaa';
            $serviceLogin = $_GET['serviceLogin'];
            $serviceLoginError = 'You did not specify a Service Login.';
            $showError = 1;
         }
      } else {
         $slError = '#ffaaaa';
         $serviceLogin = $_GET['serviceLogin'];
         $serviceLoginError = 'You did not specify a Service Login.';
         $showError = 1;
      }
   } else {
      $slError = '#ffaaaa';
      $serviceLogin = $_GET['serviceLogin'];
      $serviceLoginError = 'You did not specify a Service Login.';
      $showError = 1;
   }

   if ($_GET['servicePass'] != '') {
      if ($_GET['servicePass'] != 'You did not enter a password.') {
         $servicePass = $_GET['servicePass'];
         $validationCount++; // 5
      } else {
         $passError = '#ffaaaa';
         $servicePass = $_GET['servicePass'];
         $servicePassError = 'You did not enter a password.';
         $showError = 1;
      }
   } else {
      $passError = '#ffaaaa';
      $servicePass = $_GET['servicePass'];
      $servicePassError = 'You did not enter a password.';
      $showError = 1;
   }

   if (($_GET['threshold'] == "") && ($_GET['threshEmail'] == "")) {
      $threshold = $_GET['threshold'];
      $threshEmail = $_GET['threshEmail'];
   } elseif (($_GET['threshold'] == "") && (($_GET['threshEmail'] != ""))) {
      $thrOldError = '#ffaaaa';
      $threshold = $_GET['threshold'];
      $threshEmail = $_GET['threshEmail'];
      $validationCount = $validationCount - 2;
      $showError = 1;
   } else {
      if (($_GET['threshold'] != "") && ($_GET['threshEmail'] == "")) {
         $threshold = $_GET['threshold'];
         $thrEmailError = '#ffaaaa';
         $threshEmail = "";
         $validationCount = $validationCount - 2;
         $showError = 1;
      } else {
         $threshold = $_GET['threshold'] * 10000;
         $threshEmail = $_GET['threshEmail'];
         $threshCell = isset($_GET['threshCell']) ? $_GET['threshCell'] : NULL;
      }
   }

   try
   {
      $threshCell = ""; //set as blank, will be handled later when SMS Notifications come online!
      if ($validationCount == 5) {
         if ($_GET['typeCheck'] == 'smpp')
         {
            $moDRL = 'http://server2:3001/core_sms/interfaces/smpp/smpp_mosms.php?debug=hallodaar&system_id=' + $serviceLogin;
            $dlrURL = 'http://server2:3001/core_sms/interfaces/smpp/smpp_dlr.php?debug=hallodaar&system_id=' + $serviceLogin;
            $showError = 0;
            $resellerId = $_SESSION['serviceId'];
            $success = createNewResellerService($accType, $bilType, $serviceName, $serviceLogin, $servicePass, $resellerId, $threshold, $threshEmail, $threshCell, $moDRL, $dlrURL);
         }
         elseif ($_GET['typeCheck'] == 'http')
         {
            $moDRL = $_GET['moDRL'];
            $dlrURL = $_GET['dlrURL'];
            $showError = 0;
            $resellerId = $_SESSION['serviceId'];
            $success = createNewResellerService($accType, $bilType, $serviceName, $serviceLogin, $servicePass, $resellerId, $threshold, $threshEmail, $threshCell, $moDRL, $dlrURL);
         }
         elseif ($_GET['typeCheck'] == 'bulk')
         {
            $showError = 0;
            $resellerId = $_SESSION['serviceId'];
            $success = createNewResellerServiceNoDLRs($accType, $bilType, $serviceName, $serviceLogin, $servicePass, $resellerId, $threshold, $threshEmail, $threshCell);
         }

         $nSID = getServiceID($serviceName);

         foreach ($nSID as $ke => $valu) {
            $nSID = $valu['service_id'];
         }

         if (isset($success) && $success == 1) {
            
            header('location: newroutes.php?service_id=' . urlencode(LAST_INSERTED_SERVICE_ID));
            die();
         }
         else
         {
            $showError = 2;
         }
      }
   } catch (PDOException $e) {

      switch ($e->getCode()) {

         case 23000: // Integrity constraint violation

            $showError = 3;
            break;

         default:
            clog($e->getCode(), $e->getMessage());
            clog($e->getTrace());
            die();
      }
   }
}
?>

<aside class="right-side">
   <section class="content-header">
      <h1>
         Create Service
      </h1>
   </section>

   <section class="content">
      <div class="row">
         <form action="createservice.php" class="col-md-12" method="get" id="newService" style="border:none;">
            <div class="box box-connet" style="padding-bottom:1px; border-top-color:<?php echo $accRGB; ?>;">
               <div class="box-body">
                  <?php if (!isset($showError)) { ?>
                  <div class="callout callout-info" style="margin-bottom:0px;">
                     <h4>Create Service Instructions / Details</h4>
                     <p>This page will allow you to create NEW SERVICES.</p>
                  </div>
                  <?php } ?>
                  <?php
                  if (isset($showError) && $showError == 1) {
                     echo '<br/>';
                     echo '<div class="callout callout-danger" style="margin-bottom:0px;">';
                     echo '<h4>There are errors in the fields below.</h4>';
                     echo '<p>Please rectify the errors below to succesfully create a new service.</p>';
                     echo '</div>';
                  } else if (isset($showError) && $showError == 2) {
                     echo '<br/>';
                     echo '<div class="callout callout-danger" style="margin-bottom:0px;">';
                     echo '<h4>There was a problem creating your service.</h4>';
                     echo '<p>Please contact technical support.</p>';
                     echo '</div>';
                  } else if (isset($showError) && $showError == 3) {
                     echo '<br/>';
                     echo '<div class="callout callout-danger" style="margin-bottom:0px;">';
                     echo '<h4>There was a problem creating your service.</h4>';
                     echo '<p>Duplicate Service Name and/or API Username</p>';
                     echo '</div>';
                  }
                  ?>
               </div>
            </div>

            <div class="box box-connet" id="mainBox" style="padding-bottom:1px; border-top-color:<?php echo $accRGB; ?>; height:660px;" >
               <div class="box-header">
                  <h3 class="box-title">New Service Details:</h3>
               </div>

               <div class="box-body" style="padding:0px;">
                  <div class="sidebar-form-group col-md-12" style="padding-top:10px;">
                     <label>Billing Type</label>
                     <select class="form-control" id="billingType" name="billingType" form="newService" style="border:1px solid <?php echo $bilError; ?>" >
                        <?php
                        if ($_SESSION['serviceBillingType'] == "PREPAID") {
                           echo '<option>Prepaid</option>';
                        } else {
                           echo '<option>Please Select</option>';
                           echo '<option>Postpaid</option>';
                           echo '<option>Prepaid</option>';
                           if (isset($bilType)) {
                              echo "<option SELECTED>" . htmlentities($bilType) . "</option>";
                           }
                        }
                        ?>
                     </select>
                  </div>

                  <div class="sidebar-form-group col-md-12" style="padding-top:10px;">
                     <label>Account Type</label>
                     <select class="form-control" id="typeCheck" name="typeCheck" form="newService" onchange="showUrls(this.value);" style="border:1px solid <?php echo $typError; ?>" >
                        <option>Please Select</option>
                        <?php
                        for ($i = 0; $i < count($accountTypes); $i++) {
                           if ($_GET['typeCheck'] == $accountTypes[$i]) {
                              echo "<option value='" . htmlentities($accountTypes[$i]) . "' selected>" . htmlentities($accountTypes[$i]) . "</option>";
                           } else {
                              echo "<option value='" . htmlentities($accountTypes[$i]) . "'>" . htmlentities($accountTypes[$i]) . "</option>";
                           }
                        }
                        ?>
                     </select>
                  </div>

                  <div class="sidebar-form-group col-md-12" style="padding-top:10px;">
                     <label>Service Name</label>
                     <input type="text" class="form-control" name="serviceName" id="serviceName" style="border:1px solid <?php echo $snError; ?>" placeholder="Service Display Name. eg: Connet/Bulk" value="<?php
                     if (isset($serviceName)) {
                        echo htmlspecialchars($serviceName);
                     }
                     ?>">
                            <?php
                            if (isset($serviceNameError)) {
                               echo "<span class=\"text-danger\">" . htmlentities($serviceNameError) . "</span>";
                            }
                            ?>
                  </div>

                  <div class="sidebar-form-group col-md-12" style="padding-top:10px;">
                     <label>API Username (User login name) </label>
                     <input type="text" class="form-control" name="serviceLogin" id="serviceLogin" maxlength="15" style="border:1px solid <?php echo $slError; ?>" placeholder="Service Login (internal). eg: bulk.connet" value="<?php
                     if (isset($serviceLogin)) {
                        echo htmlspecialchars($serviceLogin);
                     }
                     ?>">
                            <?php
                            if (isset($serviceLoginError)) {
                               echo "<small class=\"text-danger\">" . htmlentities($serviceLoginError) . "</small>";
                            }
                            ?>
                  </div>

                  <div class="sidebar-form-group col-md-12" style="padding-top:10px;">
                     <label>API Password (User login password)</label>
                     <input type="text" class="form-control" name="servicePass" maxlength="8" style="border:1px solid <?php echo $passError; ?>" placeholder="Max 8 characters" value="<?php
                     if (isset($servicePass)) {
                        echo htmlspecialchars($servicePass);
                     }
                     ?>">
                            <?php
                            if (isset($servicePassError)) {
                               echo "<small class=\"text-danger\">" . htmlentities($servicePassError) . "</small>";
                            }
                            ?>
                  </div>

                  <div class="sidebar-form-group col-md-12" style="padding-top:10px;">
                     <label>Notification Threshold / Limit</label>
                     <input type="text" class="form-control" name="threshold" onkeypress="return isNumberKey(event);" style="border:1px solid <?php echo $thrOldError; ?>" placeholder="e.g 1000" value="<?php
                     if (isset($threshold)) {
                        echo htmlspecialchars($threshold);
                     }
                     ?>">
                  </div>

                  <div class="sidebar-form-group col-md-12" style="padding-top:10px;">
                     <label>Notification Email</label>
                     <input type="text" class="form-control" name="threshEmail" style="border:1px solid <?php echo $thrEmailError; ?>" placeholder="e.g john@smit.com, jane@tims.co.za" value="<?php
                     if (isset($threshEmail)) {
                        echo htmlspecialchars($threshEmail);
                     }
                     ?>">
                  </div>

                  <!--
                     DISABLED BECAUSE SMS NOTIFICATIONS DON'T WORK
                     <div class="sidebar-form-group col-md-12" style="padding-top:10px;">
                        <label>Notification SMS Number</label>
                        <input type="text" class="form-control" name="threshCell" style="border:1px solid <?php echo $thrCellError; ?>" placeholder="e.g 0821234567, 27837654321" value="">
                     </div>
                  -->
                  <div class="sidebar-form-group col-md-12" id="dlrURLBox" style="padding-top:10px;display:none;">
                     <label>HTTP DLR URL</label>
                     <input type="text" class="form-control" id="dlrURL" name="dlrURL" style="border:1px solid #aaaaaa;" placeholder="(Leave blank if not needed)" value="<?php
                     if (isset($dlrURL)) {
                        echo htmlspecialchars($dlrURL);
                     }
                     ?>">
                  </div>

                  <div class="sidebar-form-group col-md-12" id="moDRLBox" style="padding-top:10px;display:none;">
                     <label>HTTP MOSMS URL</label>
                     <input type="text" class="form-control" id="moDRL" name="moDRL" style="border:1px solid #aaaaaa;" placeholder="(Leave blank if not needed)" value="<?php
                     if (isset($moDRL)) {
                        echo htmlspecialchars($moDRL);
                     }
                     ?>">
                  </div>
               </div>

               <div class="box-footer" id="btnS" style="height:55px;">
                  <input type="hidden" name="upd" id="upd" value="updating">
                  <a class="btn btn-block btn-social btn-dropbox " onclick="newService.submit();" style="background-color: <?php echo $accRGB; ?>; color: #ffffff; border: 2px solid; border-radius: 6px !important; float:left; width: 180px; margin-top:15px;">
                     <i class="fa fa-save"></i>Create New Service
                  </a>
               </div>
            </div>
         </form>
      </div>
   </section>
</aside>

<!--  THE JAVASCRIPT IMPORT ALWAYS APPEARS BEFORE LOCAL JAVASCRIPT AT THE BOTTOM OF THE PAGE -->
<?php include("template_import_script.php"); //must import all scripts first      ?>
<!-- END JAVASCRIPT IMPORT -->

<script type="text/javascript">
   function showUrls(v)
   {
      var dlrBox = document.getElementById("dlrURLBox");
      var moBox = document.getElementById("moDRLBox");
      var boxH = document.getElementById("btnS").style.height;
      var boxI = parseInt(boxH.substr(0, boxH.length - 2));
      //alert(boxH);
      if (v == 'HTTP')
      {
         dlrBox.style.display = 'block';
         moBox.style.display = 'block';
         boxI = boxI + 740;
         boxH = boxI.toString() + 'px';
         document.getElementById("mainBox").style.height = boxH;
      } else
      {
         dlrBox.style.display = 'none';
         moBox.style.display = 'none';
         boxI = boxI + 600;
         boxH = boxI.toString() + 'px';
         document.getElementById("mainBox").style.height = boxH;

      }
   }

   function setDlr(v)
   {
      //var dlrBox = document.getElementById("dlrURL");
      //    dlrBox.value = 'http://server2:3001/core_sms/interfaces/smpp/smpp_dlr.php?debug=hallodaar&system_id=' + v;
   }

   function setMos(v)
   {
      //var mosBox = document.getElementById("moDRL");
      //    mosBox.value = 'http://server2:3001/core_sms/interfaces/smpp/smpp_mosms.php?debug=hallodaar&system_id=' + v;
   }

   /*function appendToTextboxSN(v)
    {
    var type = document.getElementById("typeCheck").value;
    
    if(v == null)
    {
    var tB = document.getElementById("serviceName");
    tB.value = '/'+type;
    
    moveCaretToStart(tB);
    // Work around Chrome's little problem
    window.setTimeout(function() 
    {
    moveCaretToStart(tB);
    }, 1);
    }
    else
    {
    var n = v.lastIndexOf("/");
    var u = v.substr(n, v.length);
    
    if(u == ('/'+type))
    {
    //  alert(('/'+type) + ' = ' + u);
    var tB = document.getElementById("serviceName");
    tB.value = v;
    
    moveCaretToPos(tB, n);
    // Work around Chrome's little problem
    window.setTimeout(function() 
    {
    moveCaretToPos(tB, n);
    }, 1);
    }
    else if(n == -1)
    {
    x = v.indexOf(type);
    if(x > 0)
    {
    //alert(type+"-some-"+x);
    y = v.substr(0, x);
    
    var tB = document.getElementById("serviceName");
    //         document.getElementById("serviceName").value = '/' + type;
    tB.value = y + '/' + type;
    
    moveCaretToPos(tB, x);
    // Work around Chrome's little problem
    window.setTimeout(function() 
    {
    moveCaretToPos(tB, x);
    }, 1);
    }
    else
    {
    var tB = document.getElementById("serviceName");
    //document.getElementById("serviceName").value = '/' + type;
    tB.value = '/' + type;
    
    moveCaretToStart(tB);
    //Work around Chrome's little problem
    window.setTimeout(function() 
    {
    moveCaretToStart(tB);
    }, 1);
    }
    }
    else
    {
    document.getElementById("serviceName").value = v + '/' + type;
    }
    }
    }
    
    function appendToTextboxSL(v)
    {
    var type = document.getElementById("typeCheck").value;
    
    if(v == null)
    {
    document.getElementById("serviceLogin").value = type + '.';
    }
    else
    {
    var n = v.indexOf(".");
    var u = v.substr(0, n);
    
    if(u == type)
    {
    document.getElementById("serviceLogin").value = v;
    }
    else if(n == -1)
    {
    document.getElementById("serviceLogin").value = type + '.';
    }
    else
    {
    document.getElementById("serviceLogin").value = type + '.' + v;
    }
    }
    }
    
    function moveCaretToStart(el) 
    {
    if (typeof el.selectionStart == "number") 
    {
    el.selectionStart = el.selectionEnd = 0;
    } 
    else if (typeof el.createTextRange != "undefined") 
    {
    el.focus();
    var range = el.createTextRange();
    range.collapse(true);
    range.select();
    }
    }
    
    function moveCaretToPos(el, n) 
    {
    if (typeof el.selectionStart == "number") 
    {
    el.selectionStart = el.selectionEnd = 0 + n;
    } 
    else if (typeof el.createTextRange != "undefined") 
    {
    el.focus();
    var range = el.createTextRange();
    range.collapse(true);
    range.select();
    }
    }*/

   $(window).load(function ()
   {
      /*var val = document.getElementById("account").value;
       checkNewAccount(val);   */
      var tc = document.getElementById('typeCheck').value;
      //if(tc == 'HTTP')
      {
         showUrls(tc);
      }

      $(".loader").fadeOut("slow");
      $(".loaderIcon").fadeOut("slow");
   })
</script>

<!-- Template Footer -->
