<?php
/**
 * Include the header template which sets up the HTML
 *
 * Don't forget to include template_import_script before any Javascripting
 * Don't forget to include template footer.php at the bottom of the page as well
 */
$headerPageTitle = "Upload Contacts"; //set the page title for the template import
TemplateHelper::initialize();

$singleView = 'block;';

if ($_SESSION['serviceId'] != '' && $_SESSION['resellerId'] != '') {
   if ($_SESSION['serviceId'] != $_SESSION['resellerId']) {
      //if(!isset($_GET['client']))
      {
         $_GET['client'] = $_SESSION['accountName'] . ' - ' . $_SESSION['serviceName'] . " - " . $_SESSION['serviceId'];
         //echo "<br><br><br><br>+---------------------->".$_GET['client'];
         $singleView = 'none;';
         // $noShow = 1;
      }
   } elseif ($_SESSION['serviceId'] == $_SESSION['resellerId']) {
      $singleView = 'block;';
      //$noShow = 1;
   }
   /* elseif($_SESSION['resellerId'] == '')
     {

     $noShow = 1;
     } */
}

////////////////////////////////////////////////
////////////////////////////////////////////////
////////////////////////////////////////////////
////////////////////////////////////////////////
////////////////////////////////////////////////
////////////////////////////////////////////////

$success = 0;
$success2 = 0;
$success3 = 0;
$success4 = 0;
$success5 = 0;
$success6 = 0;
$showContacts = "none;";

if (isset($_GET['saveEditContactList']) && $_GET['saveEditContactList'] == 1) {
   $_GET['name'] = trim($_GET['name']);
   $_GET['desc'] = trim($_GET['desc']);

   $success4 = checkNewContactListClient($_GET['sId'], $_GET['name']);
   //echo '<br><br><br>--+- '.$success4;
   if ($success4 == 0) {
      $success4 = updateContactListClient($_GET['ccId'], $_GET['name'], $_GET['desc']);
   }
}

if (isset($_GET['saveEditContact']) && $_GET['saveEditContact'] == 1) {
   $_GET['name'] = trim($_GET['name']);
   $_GET['last'] = trim($_GET['last']);
   $_GET['cont'] = trim($_GET['cont']);

   //$success5 = checkNewCampaignCamp($_GET['cId'], $_GET['name']);        
   /* if($success5 == 0)
     {
     } */
   $success5 = updateContact($_GET['caId'], $_GET['name'], $_GET['last'], $_GET['cont']);
}

if (isset($_GET['newContactList']) && $_GET['newContactList'] == 1) {
   $_GET['nName'] = trim($_GET['nName']);
   $_GET['nDesc'] = trim($_GET['nDesc']);
   $_GET['nCode'] = trim($_GET['nCode']);

   $success = addNewContactList($_GET['sId'], $_GET['nName'], $_GET['nDesc'], $_GET['stat'], $_GET['nCode']);
   /* $success = checkNewCampaignClient($_GET['sId'], $_GET['nName']);
     if($success == 0)
     {
     } */
   //echo 'SUCCESS=>'.$success.'<=';
}

if (isset($_GET['newContact']) && $_GET['newContact'] == 1) {
   $_GET['nCampName'] = trim($_GET['nCampName']);
   $_GET['nCampLast'] = trim($_GET['nCampLast']);
   $_GET['nCampNumb'] = trim($_GET['nCampNumb']);

   $success2 = checkNewContact($_GET['sId'], $_GET['nCampNumb']);

   if ($success2 == 0) {
      $success2 = addNewContact($_GET['sId'], $_GET['nCampName'], $_GET['nCampLast'], $_GET['nCampNumb']);
      if ($success2 > 0) {
         //$newId
         //echo '<br><br><br><br>---'.$success2;
         $success2 = addNewContactLink($_GET['cId'], $success2);
      }
   }
   //echo 'SUCCESS2=>'.$success2.'<=';
}

if (isset($_GET['deleteContact']) && $_GET['deleteContact'] == 1) {
   deleteContact($_GET['delId']);
}

if (isset($_GET['deleteContactList']) && $_GET['deleteContactList'] == 1) {
   deleteContactList($_GET['delId']);
}

if (isset($_SESSION['accountId'])) {
   $acId = $_SESSION['accountId'];
} else {
   $acId = null;
}
$clients = getServicesInAccount($acId);

if (isset($_GET['client'])) {
   $cl = $_GET['client'];
   $clp = strrpos($cl, ' ');
   $cl = trim(substr($cl, $clp, strlen($cl)));

   $CLData = getContactListClient($cl);

   foreach ($CLData as $key => $value) {

      $sN = $value['service_id'];
   }
   $dsp = 'block;';
   $dir = '../img/routes';
   //$serProviders = scandir($dir);
} else {
   $_GET['client'] = "0 - Please select a client...";
   $cl = strstr($_GET['client'], ' ', true);
   $dsp = 'none;';
}

if (isset($_GET['cId'])) {
   $CAData = getContactInfo($_GET['cId']);
   $showContacts = "block;";

   /* foreach ($CAData as $key => $value) 
     {
     echo '<pre>';
     print_r($value);
     echo '</pre>';
     } */
}




/*
  $showTemp = "none;";





  if(isset($_GET['newTemp']) && $_GET['newTemp'] == 1)
  {
  //"campaign.php?newTemp=1&tId=" + tId + "&uId=" + uId + "&nTempDesc=" + nTempDesc + "&nTempText=" + nTempText + "&stat=" + stat + "&client=" + cli;
  $_GET['nTempDesc'] = trim($_GET['nTempDesc']);
  $_GET['nTempText'] = trim($_GET['nTempText']);

  $success3 = addNewTemplate($_GET['tId'], $_GET['uId'], $_GET['nTempDesc'], $_GET['nTempText'], $_GET['stat']);
  }

  if(isset($_GET['saveEditContactList']) && $_GET['saveEditContactList'] == 1)
  {
  $_GET['name'] = trim($_GET['name']);
  $_GET['desc'] = trim($_GET['desc']);

  $success4 = checkNewCampaignClient($_GET['sId'], $_GET['name']);
  if($success4 == 0)
  {
  $success4 = updateCampaignClient($_GET['ccId'], $_GET['name'], $_GET['desc']);
  }
  }



  if(isset($_GET['saveEditTemplate']) && $_GET['saveEditTemplate'] == 1)
  {
  $_GET['text'] = trim($_GET['text']);
  $_GET['desc'] = trim($_GET['desc']);
  $success6 = updateCampaignTemplate($_GET['teId'], $_GET['text'], $_GET['desc']);
  }



  $colorCC = '';
  $colorCA = '';

  if(isset($_GET['cId']))
  {
  $CAData = getCampaignInfo($_GET['cId']);
  $showCamp = "block;";
  }

  if(isset($_GET['tId']))
  {
  $TMData = getCampaignTemp($_GET['tId']);
  $showTemp = "block;";
  }

 */

////////////////////////////////////////////////
////////////////////////////////////////////////
////////////////////////////////////////////////




/* $sql = 'INSERT INTO `core_service_payment` (service_id, user_id, payment_value, payment_description,payment_previous_balance) 
  VALUES (:service_id, :user_id, :payment_value, :payment_description,:payment_previous_balance)';

  $sql = mysql_encode_param_array($sql, array(
  'service_id' => $selected_service->service_id,
  'user_id' => $_SESSION['auth']['user']->user_id,
  'payment_value' => $request->payment_value,
  'payment_description' => $request->description,
  'payment_previous_balance' => $selected_service->service_credit_available,
  )); */
?>
<aside class="right-side">
   <section class="content-header">
      <h1>
         Upload Contacts
         <!--small>Control panel</small-->
      </h1>
   </section>

   <section class="content">
      <div class="row col-lg-12">
         <div class="box box-solid" style="height:66px; margin-top:-15px;display:<?php echo $singleView; ?>">
            <form action="updateservice.php" method="get" id="clientList" class="sidebar-form" style="border:0px;">
               <div class="form-group">
                  <label>Service List</label>
                  <select class="form-control" name="client" form="clientList" OnChange="reloadOnSelect(this.value);">
                     <?php
                     if ($cl == '0') {
                        echo '<option SELECTED>Please select a client...</option>';
                     }
                     foreach ($clients as $key => $value) {
                        $sId = $value['service_id'];
                        if ($cl == $sId) {
                           $accountName = $value['account_name'];
                           $serviceName = $value['service_name'];
                           echo "<option name='" . $sId . "' SELECTED>" . $accountName . " - " . $serviceName . " - " . $sId . "</option>";
                        } else {
                           echo "<option name='" . $sId . "'>" . $value['account_name'] . " - " . $value['service_name'] . " - " . $sId . "</option>";
                        }
                     }
                     ?>
                  </select>
               </div>
               <br>
            </form>
         </div>
         <div class="box box-warning" style="border-top-color:<?php echo $accRGB; ?>;display:<?php echo $dsp; ?>">
            <div class="box-header">
               <h4 class="box-title">
                  Lists
               </h4>
            </div>
            <div class="box-body">
               <?php
               if ($success === -1) {
                  echo '<div class="callout callout-danger">';
                  echo '<h4>Client Name Exists!</h4>';
                  echo '<p>The client name that you specified already exists.</p>';
                  echo '</div>';
               }

               if ($success == '1') {
                  echo '<div class="callout callout-success">';
                  echo '<h4>Client Added!</h4>';
                  echo '<p>Succesfully added a new client.</p>';
                  echo '</div>';
               }

               if ($success4 == '1') {
                  echo '<div class="callout callout-success">';
                  echo '<h4>Client Edited!</h4>';
                  echo '<p>Succesfully edited a client.</p>';
                  echo '</div>';
               }
               ?>
               <a class="btn btn-block btn-social btn-dropbox" name="'.$value['campaign_id'].'" onclick="newT('newContactList');" style="background-color:<?php echo $accRGB; ?>; color: #ffffff; border: 2px solid; border-radius: 6px !important; font-size:0.8em !important; height:24px; width: 162px; margin-top:0px; padding-top:3px;">
                  <i class="fa fa-plus" style="font-size:1.0em !important; color:#ffffff; margin-top:-6px;"></i>Add New Contact List
               </a>
               <div class="box-body table-responsive" style="display:none;" id="newContactList">
                  <table class="table table-bordered table-striped dataTable table-hover">
                     <thead>
                        <tr>
                           <!--th>Service</th-->
                           <th>Contact List Code</th>
                           <th>Contact List Name</th>
                           <th>Contact List Description</th>
                           <th><center>Save</center></th>
                     </tr>
                     </thead>
                     <tbody>
                        <tr>
                            <!--td><?php echo $sN; ?></td-->
                           <td><input type="text" style="height:24px;" class="form-control" id="newClientCode" placeholder="Code"></td>
                           <td><input type="text" style="height:24px;" class="form-control" id="newClientName" placeholder="Name"></td>
                           <td><input type="text" style="height:24px;" class="form-control" id="newClientDesc" placeholder="Description"></td>
                           <td>
                     <center>
                        <a class="btn btn-block btn-social btn-dropbox" name="saveNewClient" onclick="saveNewContactList()" style="background-color:<?php echo $accRGB; ?>; color: #ffffff; border: 2px solid; border-radius: 6px !important; font-size:0.8em !important; height:24px; width: 85px; margin-top:0px; padding-top:3px;">
                           <i class="fa fa-save" style="font-size:1.0em !important; color:#ffffff; margin-top:-6px;"></i>Save
                        </a>
                     </center>
                     </td>
                     </tr>
                     </tbody>
                  </table>
               </div>
               <div class="box-body table-responsive">
                  <table id="example2" class="table table-bordered table-striped dataTable table-hover">
                     <thead>
                        <tr>
                           <th>Contact List Code</th>
                           <th>Contact List Name</th>
                           <th>Contact List Description</th>

                           <th><center>View</center></th>
                     <th><center>Edit</center></th>
                     <th><center>Delete</center></th>
                     </tr>
                     </thead>
                     <tbody>

                        <?php
                        if (isset($CLData) && isset($CLData) != '') {
                           foreach ($CLData as $key => $value) {
                              /* if(isset($_GET['cId']))
                                {
                                if($_GET['cId'] == $value['client_id'])
                                {
                                $colorCC = ' style="background-color:#888888;"';
                                }
                                else
                                {
                                $colorCC = '';
                                }
                                } */
                              //echo '<tr>';
                              echo '<tr id="cctr' . $value['contact_list_id'] . '">';
                              echo '<td>' . $value['contact_list_code'] . '</td>';
                              echo '<td><input type="text" id="clNa' . $value['contact_list_id'] . '" style="border:0px;background-color:transparent;" readonly="readonly" value="' . $value['contact_list_name'] . '"></td>';
                              echo '<td><input type="text" id="clDe' . $value['contact_list_id'] . '" style="border:0px;background-color:transparent;" readonly="readonly" value="' . $value['contact_list_description'] . '"></td>';
                              echo '<td>
                                                        <center>
                                                            <a class="btn btn-block btn-social btn-dropbox" name="' . $value['contact_list_id'] . '" onclick="showContacts(' . $value['contact_list_id'] . ')" style="background-color:' . $accRGB . '; color: #ffffff; border: 2px solid; border-radius: 6px !important; font-size:0.8em !important; height:24px; width: 85px; margin-top:0px; padding-top:3px;">
                                                                <i class="fa fa-binoculars" style="font-size:1.0em !important; color:#ffffff; margin-top:-6px;"></i>View
                                                            </a>
                                                        </center>
                                                      </td>';
                              echo '<td>
                                                        <center>
                                                            <a class="btn btn-block btn-social btn-dropbox" id="edit' . $value['contact_list_id'] . '" onclick="editContactList(' . $value['contact_list_id'] . ')" style="background-color:' . $accRGB . '; color: #ffffff; border: 2px solid; border-radius: 6px !important; font-size:0.8em !important; height:24px; width: 80px; margin-top:0px; padding-top:3px;">
                                                                <i class="fa fa-pencil" style="font-size:1.0em !important;color:#ffffff; margin-top:-6px;"></i>Edit
                                                            </a>
                                                            <a class="btn btn-block btn-social btn-dropbox" id="save' . $value['contact_list_id'] . '" onclick="saveEditContactList(' . $value['contact_list_id'] . ')" style="display:none; background-color:' . $accRGB . '; color: #ffffff; border: 2px solid; border-radius: 6px !important; font-size:0.8em !important; height:24px; width: 80px; margin-top:0px; padding-top:3px;">
                                                                <i class="fa fa-save" style="font-size:1.0em !important;color:#ffffff; margin-top:-6px;"></i>Save
                                                            </a>
                                                        </center>
                                                      </td>';
                              echo '<td>
                                                        <center>
                                                            <a class="btn btn-block btn-social btn-dropbox" name="' . $value['contact_list_id'] . '" onclick="deleteContactList(' . $value['contact_list_id'] . ')" style="background-color:' . $accRGB . '; color: #ffffff; border: 2px solid; border-radius: 6px !important; font-size:0.8em !important; height:24px; width: 92px; margin-top:0px; padding-top:3px;">
                                                                <i class="fa fa-trash" style="font-size:1.0em !important; color:#ff3333; margin-top:-6px;"></i>Delete
                                                            </a>
                                                        </center>
                                                      </td>';
                              echo '</tr>';
                           }
                        }
                        ?>
                     </tbody>
                  </table>
               </div>
            </div>
         </div>
         <div class="box box-warning" style="border-top-color:<?php echo $accRGB; ?>;display:<?php echo $showContacts; ?>">
            <div class="box-header">
               <h4 class="box-title">
                  Contacts
               </h4>
            </div>
            <div class="box-body">
               <?php
               if ($success2 === -1) {
                  echo '<div class="callout callout-danger">';
                  echo '<h4>Contact Name Exists!</h4>';
                  echo '<p>The contact name that you specified already exists.</p>';
                  echo '</div>';
               }

               if ($success2 == '1') {
                  echo '<div class="callout callout-success">';
                  echo '<h4>Contact Added!</h4>';
                  echo '<p>Succesfully added a new contact.</p>';
                  echo '</div>';
               }

               if ($success5 == '1') {
                  echo '<div class="callout callout-success">';
                  echo '<h4>Contact Edited!</h4>';
                  echo '<p>Succesfully edited a contact.</p>';
                  echo '</div>';
               }
               ?>
               <a class="btn btn-block btn-social btn-dropbox" name="'.$value['contact_id'].'" onclick="newT('newContact');" style="background-color:<?php echo $accRGB; ?>; color: #ffffff; border: 2px solid; border-radius: 6px !important; font-size:0.8em !important; height:24px; width: 155px; margin-top:0px; padding-top:3px;">
                  <i class="fa fa-plus" style="font-size:1.0em !important; color:#ffffff; margin-top:-6px;"></i>Add New Contact
               </a>
               <div class="box-body table-responsive" style="display:none;" id="newContact">
                  <table class="table table-bordered table-striped dataTable table-hover">
                     <thead>
                        <tr>
                           <th>Firstname</th>
                           <th>Surname</th>
                           <th>Number</th>
                           <th><center>Save</center></th>
                     </tr>
                     </thead>
                     <tbody>
                        <tr>
                           <td><input type="text" style="height:24px;" class="form-control" id="newCampName" placeholder="Firstname"></td>
                           <td><input type="text" style="height:24px;" class="form-control" id="newCampLast" placeholder="Surname"></td>
                           <td><input type="text" style="height:24px;" class="form-control" id="newCampNumb" placeholder="Number"></td>
                           <td>
                     <center>
                        <a class="btn btn-block btn-social btn-dropbox" onclick="saveNewContact(<?php
                        if (isset($_GET['cId'])) {
                           echo $_GET['cId'];
                        }
                        ?>)" style="background-color:<?php echo $accRGB; ?>; color: #ffffff; border: 2px solid; border-radius: 6px !important; font-size:0.8em !important; height:24px; width: 85px; margin-top:0px; padding-top:3px;">
                           <i class="fa fa-save" style="font-size:1.0em !important; color:#ffffff; margin-top:-6px;"></i>Save
                        </a>
                     </center>
                     </td>
                     </tr>
                     </tbody>
                  </table>
               </div>
               <div class="box-body table-responsive" id="camps" style="display:<?php echo $showContacts; ?>">
                  <table id="example2" class="table table-bordered table-striped dataTable table-hover">
                     <thead>
                        <tr>
                           <th>Firstname</th>
                           <th>Surname</th>
                           <th>Number</th>
                           <th><center>Edit</center></th>
                     <th><center>Delete</center></th>
                     </tr>
                     </thead>
                     <tbody>
                        <?php
                        if (isset($CAData) && $CAData != '') {
                           foreach ($CAData as $key => $value) {
                              echo '<tr id="tr' . $value['contact_id'] . '">';
                              echo '<td><input type="text" id="caNa' . $value['contact_id'] . '" style="border:0px;background-color:transparent;" readonly="readonly" value="' . $value['contact_firstname'] . '"></td>';
                              echo '<td><input type="text" id="caCo' . $value['contact_id'] . '" style="border:0px;background-color:transparent;" readonly="readonly" value="' . $value['contact_lastname'] . '"></td>';
                              echo '<td><input type="text" id="caDe' . $value['contact_id'] . '" style="border:0px;background-color:transparent;" readonly="readonly" value="' . $value['contact_msisdn'] . '"></td>';
                              echo '<td>
                                                            <center>
                                                                <a class="btn btn-block btn-social btn-dropbox" id="editC' . $value['contact_id'] . '" onclick="editContact(' . $value['contact_id'] . ')" style="display:block; background-color:' . $accRGB . '; color: #ffffff; border: 2px solid; border-radius: 6px !important; font-size:0.8em !important; height:24px; width: 85px; margin-top:0px; padding-top:3px;">
                                                                    <i class="fa fa-pencil" style="font-size:1.0em !important;color:#ffffff; margin-top:-6px;"></i>Edit
                                                                </a>
                                                                <a class="btn btn-block btn-social btn-dropbox" id="saveC' . $value['contact_id'] . '" onclick="saveEditContact(' . $value['contact_id'] . ')" style="display:none; background-color:' . $accRGB . '; color: #ffffff; border: 2px solid; border-radius: 6px !important; font-size:0.8em !important; height:24px; width: 85px; margin-top:0px; padding-top:3px;">
                                                                    <i class="fa fa-save" style="font-size:1.0em !important;color:#ffffff; margin-top:-6px;"></i>Save
                                                                </a>
                                                            </center>
                                                          </td>';
                              echo '<td>
                                                            <center>
                                                                <a class="btn btn-block btn-social btn-dropbox" name="' . $value['contact_id'] . '" onclick="deleteContact(' . $value['contact_id'] . ')" style="background-color:' . $accRGB . '; color: #ffffff; border: 2px solid; border-radius: 6px !important; font-size:0.8em !important; height:24px; width: 92px; margin-top:0px; padding-top:3px;">
                                                                    <i class="fa fa-trash" style="font-size:1.0em !important; color:#ff3333; margin-top:-6px;"></i>Delete
                                                                </a>
                                                            </center>
                                                          </td>';
                              echo '</tr>';
                           }
                        }
                        ?>
                     </tbody>
                  </table>
               </div>
            </div>
         </div>

      </div>
   </section>
</aside>

<!--  THE JAVASCRIPT IMPORT ALWAYS APPEARS BEFORE LOCAL JAVASCRIPT AT THE BOTTOM OF THE PAGE -->
<?php include("template_import_script.php"); //must import all scripts first  ?>
<!-- END JAVASCRIPT IMPORT -->

<script type="text/javascript">
   function reloadOnSelect(name)
   {
      $(".loader").fadeIn("slow");
      $(".loaderIcon").fadeIn("slow");
      window.location = "contactlistsadd.php?client=" + name;
   }

   function newT(type)
   {
      document.getElementById(type).style.display = 'block';
   }

   function saveNewContactList()
   {
      var sId = "<?php
if (isset($cl)) {
   echo $cl;
}
?>";
      var uId = "<?php echo $_SESSION['userId']; ?>";
      var nName = document.getElementById('newClientName').value;
      var nDesc = document.getElementById('newClientDesc').value;
      var nCode = document.getElementById('newClientCode').value;
      var stat = 'ENABLED';
      var cli = "<?php
if (isset($_GET['client'])) {
   echo $_GET['client'];
}
?>";

      window.location = "contactlists.php?newContactList=1&sId=" + sId + "&uId=" + uId + "&nName=" + nName + "&nDesc=" + nDesc + "&nCode=" + nCode + "&stat=" + stat + "&client=" + cli;
   }

   function saveNewContact(client)
   {
      var cId = client;
      var sId = "<?php
if (isset($cl)) {
   echo $cl;
}
?>";
      var nCampName = document.getElementById('newCampName').value;
      var nCampLast = document.getElementById('newCampLast').value;
      var nCampNumb = document.getElementById('newCampNumb').value;
      //var stat = 'ENABLED';
      var cli = "<?php
if (isset($_GET['client'])) {
   echo $_GET['client'];
}
?>";

      window.location = "contactlists.php?newContact=1&cId=" + cId + "&sId=" + sId + "&nCampName=" + nCampName + "&nCampLast=" + nCampLast + "&nCampNumb=" + nCampNumb + "&client=" + cli;
   }

   function editContact(v)
   {
      var caNa = 'caNa' + v;
      var caDe = 'caDe' + v;
      var caCo = 'caCo' + v;
      var caEd = 'editC' + v;
      var caSa = 'saveC' + v;

      var name = document.getElementById(caNa);
      name.style.border = "thin solid #aaaaaa";
      name.style.background = '#ffffff';
      name.removeAttribute('readonly');

      var desc = document.getElementById(caDe);
      desc.style.border = "thin solid #aaaaaa";
      desc.style.background = '#ffffff';
      desc.removeAttribute('readonly');

      var code = document.getElementById(caCo);
      code.style.border = "thin solid #aaaaaa";
      code.style.background = '#ffffff';
      code.removeAttribute('readonly');

      var editBtn = document.getElementById(caEd);
      editBtn.style.display = 'none';

      var saveBtn = document.getElementById(caSa);
      saveBtn.style.display = 'block';
   }

   function editContactList(v)
   {
      //alert(v);
      var clNa = 'clNa' + v;
      var clDe = 'clDe' + v;
      var clEd = 'edit' + v;
      var clSa = 'save' + v;

      var name = document.getElementById(clNa);
      name.style.border = "thin solid #aaaaaa";
      name.style.background = '#ffffff';
      name.removeAttribute('readonly');

      var desc = document.getElementById(clDe);
      desc.style.border = "thin solid #aaaaaa";
      desc.style.background = '#ffffff';
      desc.removeAttribute('readonly');

      var editBtn = document.getElementById(clEd);
      editBtn.style.display = 'none';

      var saveBtn = document.getElementById(clSa);
      saveBtn.style.display = 'block';
   }

   function saveEditContact(v)
   {
      //alert(v);
      var caNa = 'caNa' + v;
      var caDe = 'caDe' + v;
      var caCo = 'caCo' + v;
      var cli = "<?php
if (isset($_GET['client'])) {
   echo $_GET['client'];
}
?>";
      var cId = "<?php
if (isset($_GET['cId'])) {
   echo $_GET['cId'];
}
?>";
      var name = document.getElementById(caNa).value;
      var cont = document.getElementById(caDe).value;
      var last = document.getElementById(caCo).value;
      window.location = "contactlists.php?saveEditContact=1&caId=" + v + "&name=" + name + "&last=" + last + "&cont=" + cont + "&client=" + cli + "&cId=" + cId;
   }

   function saveEditContactList(v)
   {
      var clNa = 'clNa' + v;
      var clDe = 'clDe' + v;
      var sId = "<?php
if (isset($cl)) {
   echo $cl;
}
?>";
      var cli = "<?php
if (isset($_GET['client'])) {
   echo $_GET['client'];
}
?>";
      var name = document.getElementById(clNa).value;
      var desc = document.getElementById(clDe).value;
      window.location = "contactlists.php?saveEditContactList=1&ccId=" + v + "&name=" + name + "&desc=" + desc + "&client=" + cli + "&sId=" + sId;
   }

   function deleteContactList(clId)
   {
      var cli = "<?php
if (isset($_GET['client'])) {
   echo $_GET['client'];
}
?>";
      window.location = "contactlists.php?deleteContactList=1&client=" + cli + "&delId=" + clId;
   }

   function deleteContact(id)
   {
      var cli = "<?php
if (isset($_GET['client'])) {
   echo $_GET['client'];
}
?>";
      var cId = "<?php
if (isset($_GET['cId'])) {
   echo $_GET['cId'];
}
?>";
      window.location = "contactlists.php?deleteContact=1&client=" + cli + "&cId=" + cId + "&delId=" + id;
   }

   function showContacts(cId)
   {
      $(".loader").fadeIn("slow");
      $(".loaderIcon").fadeIn("slow");
      window.location = "contactlists.php?client=" + "<?php echo $_GET['client']; ?>" + "&cId=" + cId;
   }
</script>

<script type="text/javascript">
   $(window).load(function () {
      $(".loader").fadeOut("slow");
      $(".loaderIcon").fadeOut("slow");
   })
</script>

<!-- Template Footer -->

