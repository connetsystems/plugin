<?php
   /**
    * Include the header template which sets up the HTML
    *
    * Don't forget to include template_import_script before any Javascripting
    * Don't forget to include template footer.php at the bottom of the page as well
    */
   if(!isset($_GET['file_path']) || !isset($_GET['service_id']))
   {
      header("Location: /pages/contactlists.php");
   }

   $headerPageTitle = "Contacts"; //set the page title for the template import
   TemplateHelper::initialize();

   //init the contact hlper so we can make DB queries
   $contact_helper = new helperContactLists();

   $service_id = $_GET['service_id'];
   $file_path = $_GET['file_path'];
   $selected_list_id = $_GET['list_id'];

   if(isset($_GET['error']))
   {
      $error = $_GET['error'];
   }


   //get the contact lists
   $contact_lists = $contact_helper->getContactListClient($service_id);

   /*****************************************************************************
    * Lets do some pre-processing on the file, determine column count and then
    * output some example rows so the user can configure the data
    *****************************************************************************/

   //init our needed vars
   $row_count = 0;
   $max_col_count = 0;
   $file_processing_error = false;

   //this array holds our 10 or less rows that we display on the page so the user can assign columns
   $example_row_array = array();

   //check for the delimiter of this file so we can read it
   $delimiter = helperSMSGeneral::getDelimiter($file_path);

   //open the file for writing
   if (($handle = fopen($file_path, "r")) !== FALSE)
   {
      while (($data = fgetcsv($handle, 0, $delimiter, '"')) !== false)
      {
         //if we are on the 10th or less rows, add it to our example array for display on this page
         if($row_count < 10)
         {
            array_push($example_row_array, $data);
         }

         //get the max column count
         $max_col_count = count($data);
         $row_count ++;
      }
      fclose($handle);
   }
   else
   {
      $file_processing_error = true;
   }

?>

<aside class="right-side">
   <section class="content-header">
      <h1>
         Contacts - Configure Import Data
      </h1>
   </section>

   <section class="content">
      <div class="row col-lg-12">
         <?php
            if($file_processing_error)
            {
               echo '<div class="callout callout-danger">';
                  echo '<h4>File Error</h4>';
                  echo '<p>We could not process this file. Please check your formatting.</p>';
               echo '</div>';
            }
         ?>

         <?php if(!$file_processing_error && $row_count > 0) { ?>
            <form role="form" action="../php/contactsProcessCSVFile.php" method="POST" id="data_config_form">
               <div class="row">
                  <div class="col-lg-6">
                     <div class="form-group">
                        <label class="control-label">Destination Group For Contacts:</label>
                        <select name="selected_contact_list_id" id="selected_contact_list_id" class="form-control">
                           <?php
                           //cycle through the contact lists for the selector
                           foreach($contact_lists as $list)
                           {
                              //check if this is the originally selected contact list
                              $selected = "";
                              if($selected_list_id == $list['contact_list_id'])
                              {
                                 $selected = ' selected';
                              }

                              $contact_count_in_list = $contact_helper->countContactsInList($list['contact_list_id']);
                              echo '<option value="'.$list['contact_list_id'].'" '.$selected.'>'.$list['contact_list_name'].' (' . $contact_count_in_list . ' contacts)</options>';
                           }
                           ?>
                        </select>
                     </div>
                  </div>
               </div>

               <p>Please use the table below to configure your contact data. Please assign the MSISDN, First Name and Last Name fields to the correct columns, and then click "Import Contacts".</p>

               <div class="callout callout-danger" style="display:none;" id="err_callout_columns">
                  <h4>Column Error</h4>
                  <p>A column for MSISDN, First Name and Last Name must be selected.</p>
               </div>
               <!-- CONTACTS IN THE LIST GO HERE -->
               <table class="table table-bordered table-striped table-hover dataTable">
                  <thead>
                     <tr>
                        <?php

                           //we need to output the header columns on the table so the user can assign which columns represent which piece of contact data
                           for($c = 0; $c < $max_col_count; ++ $c)
                           {
                              echo '<th>';
                                 echo '<select name="col_select'.$c.'" id="col_select'.$c.'" col_index="'.$c.'" class="form-control input-sm col_selector">';
                                    echo '<option value="contact_msisdn" '.($c == 0 ? 'selected' : '').'>MSISDN</option>';
                                    echo '<option value="contact_firstname" '.($c == 1 ? 'selected' : '').'>First Name</option>';
                                    echo '<option value="contact_lastname" '.($c == 2 ? 'selected' : '').'>Last Name</option>';
                                    echo '<option value="-1" '.($c > 2 ? 'selected' : '').'>-ignore column-</option>';
                                 echo '</select>';
                              echo '</th>';
                           }

                        ?>
                     </tr>
                  </thead>
                  <tbody>
                  <?php

                     //now lets outout the 10 example rows so the user can see what their data looks like and select the correct columns
                     foreach($example_row_array as $example_row)
                     {
                        echo '<tr>';
                        for($r = 0; $r < $max_col_count; ++ $r)
                        {
                           echo '<td>';

                           //check that dat actually exists for this column
                           if(isset($example_row[$r]))
                           {
                              echo $example_row[$r];
                           }
                           else
                           {
                              echo '<small>- no data -</small>';
                           }
                           echo '</td>';
                        }
                        echo '</tr>';
                     }

                  ?>
                  </tbody>
               </table>
               <div style="text-align: right;">
                  <p><small>Showing <strong>1</strong> to <strong><?php echo count($example_row_array); ?></strong> of a total <strong><?php echo $row_count; ?> rows</strong> in your file.</small></p>
               </div>
               <div class="row">
                  <div class="col-lg-6 col-lg-offset-6" style="text-align:right;">
                     <p>
                        When you are ready, click the button below to finish the import.<br/>
                        <small>This can take long with large files. Please do not close your browser or navigate away from this page while the file is processing.</small>
                     </p>
                     <button class="btn btn-primary" type="submit" id="btnStartImport" style="background-color:<?php echo $accRGB; ?>;border: 2px solid; border-radius: 6px !important;">
                        <i class="fa fa-upload"></i> Import Contacts
                     </button>
                     <input type="hidden" name="service_id" value="<?php echo $service_id; ?>" />
                     <input type="hidden" name="file_path" value="<?php echo $file_path; ?>" />
                     <input type="hidden" name="row_count" value="<?php echo $row_count; ?>" />
                     <input type="hidden" name="col_count" value="<?php echo $max_col_count; ?>" />
                  </div>
               </div>
            </form>
         <?php }  /* END IF CHECK TO SEE IF THERE ARE NO ROWS OR ERRORS */ ?>
      </div>
   </section>
</aside>


<!--  THE JAVASCRIPT IMPORT ALWAYS APPEARS BEFORE LOCAL JAVASCRIPT AT THE BOTTOM OF THE PAGE -->
<?php include("template_import_script.php"); //must import all scripts first  ?>
<!-- END JAVASCRIPT IMPORT -->

<script type="text/javascript">

   var serviceId = <?php echo $service_id; ?>;
   var colCount = <?php echo $max_col_count; ?>;
   $(document).ready(function (e) {

      $(".loader").fadeOut("fast");
      $(".loaderIcon").fadeOut("fast");

      /********************************************************************
      * THE FUNCTION BELOW CHECKS ALL THE COLUMN SELECT BOXES AND INSURES
      * THAT THE USER CANNOT SELECT THE SAME VALUE IN TWO DIFFERENT BOXES
      *********************************************************************/
      $(".col_selector").on('change', function() {

         console.log("On change called.");
         var thisSelectedValue = $(this).val();

         for(var i = 0; i < colCount; i++)
         {
            //make sure we aren't comparing the select box with itself
            if($("#col_select"+i).attr("col_index") != $(this).attr("col_index"))
            {
               var checkSelectedValue = $("#col_select"+i).val();

               console.log("Checking value "+checkSelectedValue+" for selectbox: "+i);

               //check if the value of this selector matches the one the user just chose
               if(thisSelectedValue == checkSelectedValue)
               {
                  console.log("MATCH!!!");
                  //reset the value of this selector
                  $("#col_select"+i).val('-1');
               }
            }
         }
      });

      //Run some validation on the column selectors before allowing the user to continue
      $('#data_config_form').submit(function(e) {
         $('#btnStartImport').html('<span class="glyphicon glyphicon-refresh glyphicon-spin"></span> Processing...');
         $("#err_callout_columns").hide();

         //check that all the required columns are selected
         var msisdnSelected = false;

         //validation for name and surname removed for now
         //var firstNameSelected = false;
         //var lastNameSelected = false;

         for(var i = 0; i < colCount; i++)
         {
            //get the value of the checkbox and compare
            var checkSelectedValue = $("#col_select"+i).val();

            switch(checkSelectedValue)
            {
               case "contact_msisdn":
                  msisdnSelected = true;
                  break;
               /*
               case "contact_firstname":
                  //firstNameSelected = true;
                  break;
               case "contact_lastname":
                  //lastNameSelected = true;
                  break;
               */
            }
         }

         if(msisdnSelected) // && firstNameSelected && lastNameSelected)
         {
            //submit the form
            return true;
         }
         else
         {
            //show the error
            $('#btnStartImport').html('<i class="fa fa-upload"></i> Import Contacts');
            $("#err_callout_columns").show();
            return false;
         }
      });

   });

</script>

<!-- Template Footer -->

