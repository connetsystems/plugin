<?php
/**
 * Include the header template which sets up the HTML
 *
 * Don't forget to include template_import_script before any Javascripting
 * Don't forget to include template_footer.php at the bottom of the page as well
 */
//set the page title for the template import
error_reporting(E_ALL);
ini_set('display_errors', 1);

$headerPageTitle = 'Financial Stats';
TemplateHelper::initialize();

if ($_SESSION['userId'] == 3 || $_SESSION['userId'] == 15 || $_SESSION['userId'] == 40 || $_SESSION['userId'] == 92 || $_SESSION['userId'] == 479 || $_SESSION['userId'] == 1109 || $_SESSION['userId'] == 225 || $_SESSION['userId'] == 267)
{
   $user_id = "Show All";
}
else
{
   $user_id = $_SESSION['userId'];
}

if (isset($_GET['tdate']) && isset($_GET['fdate']))
{
   $tdate = $_GET['tdate'];
   $fdate = $_GET['fdate'];

   $finData = getFinancialProfit($fdate, $tdate,$user_id);

}
else
{

   $fdate = DateTime::createFromFormat('Y-m-d',date("Y-m-")."01");
   $tdate = DateTime::createFromFormat('Y-m-d',date("Y-m-d"));
   $tdate->modify( 'first day of next month' );

   $tdate = '2016-04-01';
   $fdate = '2016-05-01';

   //$finData = getFinancialProfit($fdate->format('Y-m-d'),$tdate->format('Y-m-d'),$user_id);
   $finData = getFinancialProfit($fdate, $tdate,$user_id);
}




if (isset($finData) && !empty($finData)) {

//Dashboard
//Exclusion Table Array
   $reformat_type = organizeExclusion($finData);

//Sales Person Array - Tab
   $reformat_sales = organizeSalesPerson($finData,$user_id);

//Account Array - Tab
   $reformat_account = organizeAccount($finData,$user_id);

//Network - Charts
   $reformat_networkchart = organizeNetworkCharts($finData);


//Network - Tab
   $reformat_network = organizeNetwork($fdate, $tdate);

//Provider - Tab
   $reformat_smsc = organizeProvider($fdate, $tdate,$user_id);


//Day
   $reformat_day = array();
   foreach ($finData as $key => $value) {
      //type
      if (!array_key_exists($value['day'], $reformat_day)) {
         $day = array();
         $day['units'] = $value['units'];
         $day['d'] = date("d", strtotime($value['day']));

         $reformat_day[$value['day']] = $day;
      } else {
         $day = $reformat_day[$value['day']];
         $day['units'] += $value['units'];
         $day['d'] = date("d", strtotime($value['day']));

         $reformat_day[$value['day']] = $day;
      }
   }
//Sort Network by profit total and unit total
   foreach ($reformat_day as $key => $row) {
      $dayarray[$key] = $row['d'];
   }
   array_multisort($dayarray, SORT_ASC, $reformat_day);




//Chart
   $morris = array();
   foreach ($reformat_day as $day) {
      array_push($morris, '{day: ' . $day['d'] . ', value: ' . $day['units'] . '}');
   }
//echo implode(',', $morris);
//print_r($morris);
//{day: '2008', value: 20}
//Network Units Donut
   $donut_network = array();
   foreach ($reformat_networkchart as $key => $value) {
      if ($value['mccmnc'] == '65501') {
         $net_data = array();
         $net_data['units'] = $value['units'];
         $net_data['network'] = "Vodacom";
         $donut_network[$net_data['network']] = $net_data;
      } else if ($value['mccmnc'] == '65502') {
         $net_data = array();
         $net_data['units'] = $value['units'];
         $net_data['network'] = "Telkom";
         $donut_network[$net_data['network']] = $net_data;
      } else if ($value['mccmnc'] == '65507') {
         $net_data = array();
         $net_data['units'] = $value['units'];
         $net_data['network'] = "Cell-C";
         $donut_network[$net_data['network']] = $net_data;
      } else if ($value['mccmnc'] == '65510') {
         $net_data = array();
         $net_data['units'] = $value['units'];
         $net_data['network'] = "MTN";
         $donut_network[$net_data['network']] = $net_data;
      } else {
         if (array_key_exists("Int", $donut_network)) {
            $net_data = $donut_network["Int"];
         } else {
            $net_data = array();
            $net_data['network'] = "Int";
            $net_data['units'] = 0;
         }
         $net_data['units'] += $value['units'];

         $donut_network[$net_data['network']] = $net_data;
      }
   }
//print_r($donut_network);

   $donut_data_n = array();
   foreach ($donut_network as $m) {
//{label: "Download Sales", value: 12},
      array_push($donut_data_n, '{label: "' . $m['network'] . '", value: ' . $m['units'] . '}');
   }
//print_r($donut_data_n);
//Network Profit Donut
   $donut_network_p = array();
   foreach ($reformat_networkchart as $key => $value) {
      if ($value['mccmnc'] == '65501') {
         $net_data_p = array();
         $net_data_p['profit'] = $value['profit'];
         $net_data_p['network'] = "Vodacom";
         $donut_network_p[$net_data_p['network']] = $net_data_p;
      } else if ($value['mccmnc'] == '65502') {
         $net_data_p = array();
         $net_data_p['profit'] = $value['profit'];
         $net_data_p['network'] = "Telkom";
         $donut_network_p[$net_data_p['network']] = $net_data_p;
      } else if ($value['mccmnc'] == '65507') {
         $net_data_p = array();
         $net_data_p['profit'] = $value['profit'];
         $net_data_p['network'] = "Cell-C";
         $donut_network_p[$net_data_p['network']] = $net_data_p;
      } else if ($value['mccmnc'] == '65510') {
         $net_data_p = array();
         $net_data_p['profit'] = $value['profit'];
         $net_data_p['network'] = "MTN";
         $donut_network_p[$net_data_p['network']] = $net_data_p;
      } else {
         if (array_key_exists("Int", $donut_network_p)) {
            $net_data = $donut_network_p["Int"];
         } else {
            $net_data_p = array();
            $net_data_p['network'] = "Int";
            $net_data_p['profit'] = 0;
         }
         $net_data_p['profit'] += $value['profit'];

         $donut_network_p[$net_data_p['network']] = $net_data_p;
      }
   }
//print_r($donut_network_p);

   $donut_data_p = array();
   foreach ($donut_network_p as $p) {
//{label: "Download Sales", value: 12},
      array_push($donut_data_p, '{label: "' . $p['network'] . '", value: ' . $p['profit'] . '}');
   }
//print_r($donut_data_n);
   ?>


   <link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/morris.js/0.5.1/morris.css">
   <script src="//ajax.googleapis.com/ajax/libs/jquery/1.9.0/jquery.min.js"></script>
   <script src="//cdnjs.cloudflare.com/ajax/libs/raphael/2.1.0/raphael-min.js"></script>
   <script src="//cdnjs.cloudflare.com/ajax/libs/morris.js/0.5.1/morris.min.js"></script>

   <aside class="right-side">
      <section class="content-header">
         <div class="row">
            <div class="col-sm-4">
               Financial Stats
            </div>
            <div class="col-sm-8" style="vertical-align: right;">
               <form action="financialstats.php" method="post" style="border:none;">
                  <table>
                     <tr>
                        <td>
                           <button type="button" class="btn btn-block btn-primary" style="width:80px;" onclick="choosemonth('2015-12-01', '2016-01-01')";>December</button>
                        </td>
                        <td>
                           <button type="button" class="btn btn-block btn-primary" style="width:80px;" onclick="choosemonth('2016-01-01', '2016-02-01')";>January</button>
                        </td>
                        <td>
                           <button type="button" class="btn btn-block btn-primary" style="width:80px;" onclick="choosemonth('2016-02-01', '2016-03-01')";>February</button>
                        </td>
                        <td>
                           <button type="button" class="btn btn-block btn-primary" style="width:80px;" onclick="choosemonth('2016-03-01', '2016-04-01')";>March</button>
                        </td>
                        <td>
                           <button type="button" class="btn btn-block btn-primary" style="width:80px;" onclick="choosemonth('2016-04-01', '2016-05-01')";>April</button>
                        </td>
                     </tr>
                  </table>
               </form>
            </div>
         </div>
      </section>


      <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
         <div class="modal-dialog modal-lg" style="width: 80%;">
            <div class="modal-content">
               <div class="modal-header">
                  <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                  <h4 class="modal-title" id="myModalLabel"></h4>
               </div>
               <div class="modal-body" id="modal-bodyku"></div>
               <div class="modal-footer" id="modal-footerq">
               </div>
            </div>
         </div>
      </div>





      <section class="content col-lg-12" >
         <!-- Nav tabs -->
         <ul class="nav nav-tabs" role="tablist">
            <li role="presentation" class="active"><a href="#dash" aria-controls="dash" role="tab" data-toggle="tab">Dashboard</a></li>
            <li role="presentation"><a href="#sales" aria-controls="sales" role="tab" data-toggle="tab">Sales Person</a></li>
            <li role="presentation"><a href="#account" aria-controls="account" role="tab" data-toggle="tab">Account</a></li>
            <?php
            if ($user_id == 'Show All') {
               echo '<li role="presentation"><a href="#network" aria-controls="network" role="tab" data-toggle="tab">Network</a></li>';
               echo '<li role="presentation"><a href="#provider" aria-controls="invoice" role="tab" data-toggle="tab">Provider</a></li>';
            }
            ?>
         </ul>
         <div class="tab-content">
            <div role="tabpanel" class="tab-pane active" id="dash">
               <section class="content">
                  <div class="row">
                     <div class="col-md-12">
                        <div class="box box-solid box-primary">
                           <div class="box-header">
                              <h3 class="box-title">Daily Totals</h3>
                           </div><!-- /.box-header -->
                           <div id="myfirstchart" style="height: 250px; width: 900px;"></div>
                        </div>
                     </div>
                  </div>
                  <div class="row" <?php
                  if ($user_id != 'Show All') {
                     echo 'style="display:none;"'; //hide div
                  }
                  ?>>
                     <div class="col-md-3">
                        <div class="box box-solid box-info">
                           <div class="box-header">
                              <h3 class="box-title">Units Totals</h3>
                           </div>
                           <div class="box-body">
                              <div id="donut_network_units" style="height: 200px; width: 200px;"></div>
                           </div>
                        </div>
                     </div>
                     <div class="col-md-3">
                        <div class="box box-solid box-info">
                           <div class="box-header">
                              <h3 class="box-title">Profit Totals</h3>
                           </div>
                           <div class="box-body">
                              <div id="donut_network_profit" style="height: 200px; width: 200px;"></div>
                           </div>
                        </div>
                     </div>
                     <div class="col-md-6">
                        <div class="box box-solid box-warning">
                           <div class="box-header">
                              <h3 class="box-title">Exclusion</h3>
                           </div>
                           <div class="box-body">
                              <table id="exclusionTable" class="table table-hover table-bordered table-striped">
                                 <thead>
                                    <tr role="row">
                                       <th>Type</th>
                                       <th align="right">Units</th>
                                       <th align="right">Client</th>
                                       <th align="right">Connet</th>
                                       <th align="right">Profit</th>
                                       <th>Profit %</th>
                                    </tr>
                                 </thead>
                                 <tbody>
                                    <?php

                                    $units = 0;
                                    $client = 0;
                                    $connet = 0;
                                    $profit = 0;

                                    foreach ($reformat_type as $key => $value) {
                                       echo '<tr>';

                                       echo '<td align="left" style="padding: 1px;font-size: 13px;vertical-align:middle;">' . $key . '</td>';
                                       echo '<td align="right" style="padding: 1px;font-size: 13px;vertical-align:middle;">' . number_format($value['units'], 0, 0, ' ') . '</td>';
                                       echo '<td align="right" style="padding: 1px;font-size: 13px;vertical-align:middle;">R ' . number_format($value['client'], 2, '.', ' ') . '</td>';
                                       echo '<td align="right" style="padding: 1px;font-size: 13px;vertical-align:middle;">R ' . number_format($value['connet'], 2, '.', ' ') . '</td>';
                                       echo '<td align="right" style="padding: 1px;font-size: 13px;vertical-align:middle;">R ' . number_format($value['profit'], 2, '.', ' ') . '</td>';
                                       echo '<td align="right" style="vertical-align:middle;width:70px;padding: 1px;font-size: 13px;">' . number_format((($value['profit'] / $value['client']) * 100), 2, '.', '') . ' %</td>';

                                       echo '</tr>';

                                       $units += $value['units'];
                                       $client += $value['client'];
                                       $connet += $value['connet'];
                                       $profit += $value['profit'];
                                    }
                                    ?>
                                 </tbody>
                                 <tfoot>
                                 <?php
                                    echo '<tr>';
                                    echo '<td align="right"><b>Totals</b></td>';
                                    echo '<td align="right" style="vertical-align:middle;padding: 1px;font-size: 13px;"><b>' . number_format($units, 0, 0, ' ') . '</b></td>';
                                    echo '<td align="right" style="vertical-align:middle;padding: 1px;font-size: 13px;"><b>R ' . number_format($client, 2, '.', ' ') . '</b></td>';
                                    echo '<td align="right" style="vertical-align:middle;padding: 1px;font-size: 13px;"><b>R ' . number_format($connet, 2, '.', ' ') . '</b></td>';
                                    echo '<td align="right" style="vertical-align:middle;padding: 1px;font-size: 13px;"><b>R ' . number_format($profit, 2, '.', ' ') . '</b></td>';
                                    echo '<td align="right">&nbsp;</td>';
                                    echo '</tr>';

                                 ?>
                                 </tfoot>
                              </table>
                           </div>
                        </div>
                     </div>
                  </div>


               </section>

            </div>
            <div role="tabpanel" class="tab-pane" id="sales">
               <section class="content" >
                  <div class="row">
                     <div class="col-md-12">
                        <!-- SALES TABLE -->
                        <div class="box box-solid box-primary">
                           <div class="box-header">
                              <h3 class="box-title">Sales Person (sort-profit)</h3>
                           </div><!-- /.box-header -->
                           <div class="box-body">
                              <table id="salesTable" class="table table-bordered table-striped dataTable table-hover">
                                 <thead>
                                    <tr role="row">
                                       <th>Person</th>
                                       <th>Units</th>
                                       <th>Client</th>
                                       <th>Connet</th>
                                       <th>Profit</th>
                                       <th>Profit %</th>
                                    </tr>
                                 </thead>
                                 <tbody>
                                 <?php

                                    $units = 0;
                                    $client = 0;
                                    $connet = 0;
                                    $profit = 0;

                                    foreach ($reformat_sales as $key => $value)
                                    {

                                       echo '<tr>';

                                       echo '<td style="vertical-align:middle;width:70px;padding: 1px;font-size: 13px;">' . $key . '('.count($value['service_ids']).')</td>';
                                       echo '<td align="right" style="vertical-align:middle;width:70px;padding: 1px;font-size: 13px;">' . number_format($value['units'], 0, 0, ' ') . '</td>';
                                       echo '<td align="right" style="vertical-align:middle;width:70px;padding: 1px;font-size: 13px;">R ' . number_format($value['client'], 2, '.', ' ') . '</td>';
                                       echo '<td align="right" style="vertical-align:middle;width:70px;padding: 1px;font-size: 13px;">R ' . number_format($value['connet'], 2, '.', ' ') . '</td>';
                                       echo '<td align="right" style="vertical-align:middle;width:70px;padding: 1px;font-size: 13px;">R ' . number_format($value['profit'], 2, '.', ' ') . '</td>';
                                       echo '<td align="right" style="vertical-align:middle;width:70px;padding: 1px;font-size: 13px;">' . number_format((($value['profit'] / $value['client']) * 100), 2, '.', '') . ' %</td>';

                                       echo '</tr>';

                                       $units += $value['units'];
                                       $client += $value['client'];
                                       $connet += $value['connet'];
                                       $profit += $value['profit'];
                                    }

                                 }
                                 ?>
                              </tbody>
                                 <tfoot>
                                    <?php
                                       echo '<tr>';
                                       echo '<td align="right"><b>Totals</b></td>';
                                       echo '<td align="right" style="vertical-align:middle;width:70px;padding: 1px;font-size: 13px;"><b>' . number_format($units, 0, 0, ' ') . '</b></td>';
                                       echo '<td align="right" style="vertical-align:middle;width:70px;padding: 1px;font-size: 13px;"><b>R ' . number_format($client, 2, '.', ' ') . '</b></td>';
                                       echo '<td align="right" style="vertical-align:middle;width:70px;padding: 1px;font-size: 13px;"><b>R ' . number_format($connet, 2, '.', ' ') . '</b></td>';
                                       echo '<td align="right" style="vertical-align:middle;width:70px;padding: 1px;font-size: 13px;"><b>R ' . number_format($profit, 2, '.', ' ') . '</b></td>';
                                       echo '<td align="right">&nbsp;</td>';
                                       echo '</tr>';
                                    ?>
                                 </tfoot>
                           </table>
                        </div><!-- /.box-body -->
                        <div class="box-body">
                           <div class="form-group">
                              <a onclick="saveRep();" class="btn btn-block btn-social btn-dropbox " style="background-color:<?php echo $accRGB; ?>; border: 2px solid; border-radius: 6px !important; float:left; width: 134px; margin-top:-19px;">
                                 <i class="fa fa-save"></i>Save Report
                              </a>
                           </div>
                        </div>
                     </div><!-- /.box -->
                  </div>
               </div>
            </section>
            </div>
            <div role="tabpanel" class="tab-pane" id="account">
               <section class="content" >
                  <div class="row">
                     <div class="col-md-12">
                        <!-- ACCOUNT TABLE -->
                        <div class="box box-solid box-success">
                           <div class="box-header">
                              <h3 class="box-title">Accounts (sort-profit)</h3>
                           </div><!-- /.box-header -->
                           <div class="box-body">
                              <table id="accountTable" class="table table-hover table-bordered table-striped">
                                 <thead>
                                    <tr role="row">
                                       <th>Account Name</th>
                                       <th align="right">Units</th>
                                       <th align="right">Client</th>
                                       <th align="right">Connet</th>
                                       <th align="right">Profit</th>
                                    </tr>
                                 </thead>
                                 <tbody>
                                    <?php

                                       $units = 0;
                                       $client = 0;
                                       $connet = 0;
                                       $profit = 0;

                                       foreach ($reformat_account as $key => $value)
                                       {
                                          echo '<tr>';

                                          if($value['profit'] < 0)
                                          {
                                             echo '<td style="vertical-align:middle;width:70px;padding: 1px;font-size: 13px;background-color:#fbc1c1;cursor: pointer;"><span onClick="open_container(\'Show All\',\'Show All\',\''.$key.'\',\'Show All\');">' . $key . ' ('.count($value['service_ids']).') '.($value['is_profit_error'] ? '<span class="badge bg-red">error</span>' : '').'</span></td>';
                                             echo '<td align="right" style="vertical-align:middle;width:70px;padding: 1px;font-size: 13px;background-color:#fbc1c1;;">' . number_format($value['units'], 0, 0, ' ') . '</td>';
                                             echo '<td align="right" style="vertical-align:middle;width:70px;padding: 1px;font-size: 13px;background-color:#fbc1c1;;">R ' . number_format($value['client'], 2, '.', ' ') . '</td>';
                                             echo '<td align="right" style="vertical-align:middle;width:70px;padding: 1px;font-size: 13px;background-color:#fbc1c1;;">R ' . number_format($value['connet'], 2, '.', ' ') . '</td>';
                                             echo '<td align="right" style="vertical-align:middle;width:70px;padding: 1px;font-size: 13px;background-color:#fbc1c1;;">R ' . number_format($value['profit'], 2, '.', ' ') . '</td>';
                                          }
                                          else
                                          {
                                             echo '<td style="vertical-align:middle;width:70px;padding: 1px;font-size: 13px;cursor: pointer;"><span onClick="open_container(\'Show All\',\'Show All\',\''.$key.'\',\'Show All\');">' . $key . ' ('.count($value['service_ids']).') '.($value['is_profit_error'] ? '<span class="badge bg-red">error</span>' : '').'</span></td>';
                                             echo '<td align="right" style="vertical-align:middle;width:70px;padding: 1px;font-size: 13px;">' . number_format($value['units'], 0, 0, ' ') . '</td>';
                                             echo '<td align="right" style="vertical-align:middle;width:70px;padding: 1px;font-size: 13px;">R ' . number_format($value['client'], 2, '.', ' ') . '</td>';
                                             echo '<td align="right" style="vertical-align:middle;width:70px;padding: 1px;font-size: 13px;">R ' . number_format($value['connet'], 2, '.', ' ') . '</td>';
                                             echo '<td align="right" style="vertical-align:middle;width:70px;padding: 1px;font-size: 13px;">R ' . number_format($value['profit'], 2, '.', ' ') . '</td>';
                                          }

                                          echo '</tr>';


                                          $units += $value['units'];
                                          $client += $value['client'];
                                          $connet += $value['connet'];
                                          $profit += $value['profit'];

                                       }

                                    ?>
                                 </tbody>
                                 <tfoot>
                                    <?php
                                    echo '<tr>';
                                       echo '<td align="right"><b>Totals</b></td>';
                                       echo '<td align="right" style="vertical-align:middle;width:70px;padding: 1px;font-size: 13px;"><b>' . number_format($units, 0, 0, ' ') . '</b></td>';
                                       echo '<td align="right" style="vertical-align:middle;width:70px;padding: 1px;font-size: 13px;"><b>R ' . number_format($client, 2, '.', ' ') . '</b></td>';
                                       echo '<td align="right" style="vertical-align:middle;width:70px;padding: 1px;font-size: 13px;"><b>R ' . number_format($connet, 2, '.', ' ') . '</b></td>';
                                       echo '<td align="right" style="vertical-align:middle;width:70px;padding: 1px;font-size: 13px;"><b>R ' . number_format($profit, 2, '.', ' ') . '</b></td>';
                                       echo '</tr>';
                                    ?>
                                 </tfoot>
                              </table>
                           </div><!-- /.box-body -->
                        </div><!-- /.box -->
                  </div>
               </section>
            </div>
            <div role="tabpanel" class="tab-pane" id="network">
               <section class="content" >
               <div class="row">
                  <div class="col-md-12">
                     <div class="box box-solid box-info">
                        <div class="box-header">
                           <h3 class="box-title">Networks (sort-units)</h3>
                        </div><!-- /.box-header -->
                        <div class="box-body">
                           <table id="networksTable" class="table table-hover table-bordered table-striped">
                              <thead>
                                 <tr role="row">
                                    <th>Networks</th>
                                    <th>Units (Excluded)</th>
                                    <th>Invoice Amount</th>
                                    <th>Profit (Excluded)</th>
                                 </tr>
                              </thead>
                              <tbody>
                                 <?php

                                 $units = 0;
                                 $units_rdnc = 0;
                                 $connet = 0;
                                 $profit = 0;
                                 $profit_rdnc = 0;

                                 foreach ($reformat_network as $key => $value) {
                                    echo '<tr>';

                                    if($value['profit_zar'] < 0)
                                    {
                                       echo '<td style="vertical-align:middle;width:70px;padding: 1px;font-size: 13px;background-color:#fbc1c1;cursor: pointer;"><span onClick="open_container(\''.$value['mccmnc'].'\',\''.$value['prefix_network_name'].'\',\'Show All\',\'Show All\');">'.$value['prefix_network_name'].' ('.$value['mccmnc'].')</span></td>';

                                       echo '<td align="right" style="vertical-align:middle;padding: 1px;font-size: 13px;background-color:#fbc1c1;">'.number_format($value['units'], 0, 0, ' ').' ('.number_format($value['units_rdnc'], 0, 0, ' ').')</td>';

                                       /*if($value['connet_cur'] == 'EUR')
                                       {
                                          echo '<td align="right" style="vertical-align:middle;width:70px;padding: 1px;font-size: 13px;background-color:#fbc1c1;"><b><i class="fa fa-eur" style="color:#3333dd;"></i></b> '.number_format($value['connet_eur'], 2, '.', ' ').'</td>';
                                       }
                                       else
                                       {
                                          echo '<td align="right" style="vertical-align:middle;width:70px;padding: 1px;font-size: 13px;background-color:#fbc1c1;"><span style="color:#993333;"><b>R</b></span> '.number_format($value['connet_zar'], 2, '.', ' ').'</td>';
                                       }
                                       */
                                       echo '<td align="right" style="vertical-align:middle;width:70px;padding: 1px;font-size: 13px;background-color:#fbc1c1;"><span style="color:#993333;"><b>R</b></span> '.number_format($value['connet_zar'], 2, '.', ' ').'</td>';

                                       echo '<td align="right" style="vertical-align:middle;width:70px;padding: 1px;font-size: 13px;background-color:#fbc1c1;"><span style="color:#993333;"><b>R</b></span>  '.number_format($value['profit_zar'], 2, '.', ' ').'(<span style="color:#993333;"><b>R</b></span> '.number_format($value['profit_zar_rdnc'], 2, '.', ' ').')</td>';
                                    }
                                    else
                                    {
                                       echo '<td style="vertical-align:middle;width:70px;padding: 1px;font-size: 13px;cursor: pointer;"><span onClick="open_container(\''.$value['mccmnc'].'\',\''.$value['prefix_network_name'].'\',\'Show All\',\'Show All\');">'.$value['prefix_network_name'].' ('.$value['mccmnc'].')</span></td>';

                                       echo '<td align="right" style="vertical-align:middle;padding: 1px;font-size: 13px;">'.number_format($value['units'], 0, 0, ' ').' ('.number_format($value['units_rdnc'], 0, 0, ' ').')</td>';


                                       /*if($value['connet_cur'] == 'EUR')
                                       {
                                          echo '<td align="right" style="vertical-align:middle;width:70px;padding: 1px;font-size: 13px;"><b><i class="fa fa-eur" style="color:#3333dd;"></i></b> '.number_format($value['connet_eur'], 2, '.', ' ').'</td>';
                                       }
                                       else
                                       {
                                          echo '<td align="right" style="vertical-align:middle;width:70px;padding: 1px;font-size: 13px;"><span style="color:#993333;"><b>R</b></span>  '.number_format($value['connet_zar'], 2, '.', ' ').'</td>';
                                       }
                                       */
                                       echo '<td align="right" style="vertical-align:middle;width:70px;padding: 1px;font-size: 13px;"><span style="color:#993333;"><b>R</b></span>  '.number_format($value['connet_zar'], 2, '.', ' ').'</td>';

                                       echo '<td align="right" style="vertical-align:middle;width:70px;padding: 1px;font-size: 13px;"><span style="color:#993333;"><b>R</b></span>  '.number_format($value['profit_zar'], 2, '.', ' ').' (<span style="color:#993333;"><b>R</b></span> '.number_format($value['profit_zar_rdnc'], 2, '.', ' ').')</td>';
                                    }


                                    echo '</tr>';

                                    $units += $value['units'];
                                    $units_rdnc += $value['units_rdnc'];
                                    $connet += $value['connet_zar'];
                                    $profit += $value['profit_zar'];
                                    $profit_rdnc += $value['profit_zar_rdnc'];
                                 }
                                 ?>
                              </tbody>
                              <tfoot>
                              <?php
                                 echo '<tr>';
                                 echo '<td align="right"><b>Totals</b></td>';
                                 echo '<td align="right" style="vertical-align:middle;width:70px;padding: 1px;font-size: 13px;"><b>'.number_format($units, 0, 0, ' ').' ('.number_format($units_rdnc, 0, 0, ' ').')</b></td>';

                                 echo '<td align="right" style="vertical-align:middle;width:70px;padding: 1px;font-size: 13px;"><b>R '.number_format($connet, 2, '.', ' ').'</b></td>';
                                 echo '<td align="right" style="vertical-align:middle;width:70px;padding: 1px;font-size: 13px;"><b>R '.number_format($profit, 2, '.', ' ').'(R '.number_format($profit_rdnc, 2, '.', ' ').')</b></td>';
                                 echo '</tr>';

                                 echo '<tr>';
                                 echo '<td align="right"><b>Totals</b></td>';

                                 $units_totals = $units + $units_rdnc;
                                 echo '<td align="right" style="vertical-align:middle;padding: 1px;font-size: 13px;"><b>'.number_format($units_totals, 0, 0, ' ').'</b></td>';
                                 echo '<td align="right" style="vertical-align:middle;padding: 1px;font-size: 13px;"><b></b></td>';
                                 $profit_totals = $profit + $profit_rdnc;
                                 echo '<td align="right" style="vertical-align:middle;padding: 1px;font-size: 13px;"><b><span style="color:#993333;">R </span>'.number_format($profit_totals, 2, 0, ' ').'</b></td>';
                                 echo '</tr>'
                              ?>
                              </tfoot>
                           </table>
                        </div><!-- /.box -->
                     </div>
                  </div>
               </div>
               </section>
            </div>
            <div role="tabpanel" class="tab-pane" id="provider" >
            <section class="content" >
               <div class="row">
                  <div class="col-md-12">
                     <div class="box box-solid box-info">
                        <div class="box-header">
                           <h3 class="box-title">Invoice</h3>
                        </div><!-- /.box-header -->
                        <div class="box-body">
                           <table id="routesTable" class="table table-hover table-bordered table-striped">
                              <thead>
                              <tr role="row">
                                 <th>Provider</th>
                                 <th>Units (Excluded)</th>
                                 <th>Invoice Amount</th>
                                 <th>Profit (Excluded)</th>
                              </tr>
                              </thead>
                              <tbody>
                              <?php

                              $units = 0;
                              $units_rdnc = 0;
                              $connet = 0;
                              $profit = 0;
                              $profit_rdnc = 0;

                              foreach ($reformat_smsc as $key => $value) {
                                 echo '<tr>';

                                 $logo = trim($value['smsc']);

                                 switch ($logo)
                                 {
                                    case "mtn_smsc":
                                       $logo ="MTN-SA";
                                       break;
                                    case "vodacom_smsc":
                                       $logo ="Vodacom SA";
                                       break;
                                    case "telkom_smsc":
                                       $logo ="Telkom Mobile";
                                       break;
                                    case "cellc_smsc":
                                       $logo ="Cell C";
                                       break;
                                    case "mtn_rev_smsc":
                                       $logo ="MTN-SA";
                                       break;
                                    case "vodacom_pr_smsc":
                                       $logo ="Vodacom SA";
                                       break;
                                    case "mtc_smsc":
                                       $logo ="MTC";
                                       break;
                                    default:
                                       $logo = strtok($logo, '_');
                                       break;
                                 }

                                 if($value['profit_zar'] < 0)
                                 {
                                    echo '<td style="vertical-align:middle;width:70px;padding: 1px;font-size: 13px;background-color:#fbc1c1;cursor: pointer;"><span onClick="open_container(\'Show All\',\'Show All\',\'Show All\',\''.$value['smsc'].'\');"><img src="../img/serviceproviders/'.strtok($logo, '_').'.png"/> '.$value['smsc'].'</span></td>';
                                    echo '<td align="right" style="vertical-align:middle;width:70px;padding: 1px;font-size: 13px;background-color:#fbc1c1;">'.number_format($value['units'], 0, 0, ' ').' ('.number_format($value['units_rdnc'], 0, 0, ' ').')</td>';

                                    if($value['connet_cur'] == 'EUR')
                                    {
                                       echo '<td align="right" style="vertical-align:middle;width:70px;padding: 1px;font-size: 13px;background-color:#fbc1c1;"><b><i class="fa fa-eur" style="color:#3333dd;"></i></b> '.number_format($value['connet_eur'], 2, '.', ' ').'</td>';
                                    }
                                    else
                                    {
                                       echo '<td align="right" style="vertical-align:middle;width:70px;padding: 1px;font-size: 13px;background-color:#fbc1c1;"><span style="color:#993333;"><b>R</b></span> '.number_format($value['connet_zar'], 2, '.', ' ').'</td>';
                                    }

                                    echo '<td align="right" style="vertical-align:middle;width:70px;padding: 1px;font-size: 13px;background-color:#fbc1c1;"><span style="color:#993333;"><b>R</b></span>  '.number_format($value['profit_zar'], 2, '.', ' ').'(<span style="color:#993333;"><b>R</b></span> '.number_format($value['profit_zar_rdnc'], 2, '.', ' ').')</td>';
                                 }
                                 else
                                 {
                                    echo '<td style="vertical-align:middle;width:70px;padding: 1px;font-size: 13px;cursor: pointer;"><span onClick="open_container(\'Show All\',\'Show All\',\'Show All\',\''.$value['smsc'].'\');"><img src="../img/serviceproviders/'.strtok($logo, '_').'.png"/> '.$value['smsc'].'</span></td>';
                                    echo '<td align="right" style="vertical-align:middle;width:70px;padding: 1px;font-size: 13px;">'.number_format($value['units'], 0, 0, ' ').' ('.number_format($value['units_rdnc'], 0, 0, ' ').')</td>';

                                    if($value['connet_cur'] == 'EUR')
                                    {
                                       echo '<td align="right" style="vertical-align:middle;width:70px;padding: 1px;font-size: 13px;"><b><i class="fa fa-eur" style="color:#3333dd;"></i></b> '.number_format($value['connet_eur'], 2, '.', ' ').'</td>';
                                    }
                                    else
                                    {
                                       echo '<td align="right" style="vertical-align:middle;width:70px;padding: 1px;font-size: 13px;"><span style="color:#993333;"><b>R</b></span>  '.number_format($value['connet_zar'], 2, '.', ' ').'</td>';
                                    }

                                    echo '<td align="right" style="vertical-align:middle;width:70px;padding: 1px;font-size: 13px;"><span style="color:#993333;"><b>R</b></span>  '.number_format($value['profit_zar'], 2, '.', ' ').' (<span style="color:#993333;"><b>R</b></span> '.number_format($value['profit_zar_rdnc'], 2, '.', ' ').')</td>';
                                 }


                                 echo '</tr>';

                                 $units += $value['units'];
                                 $units_rdnc += $value['units_rdnc'];
                                 $connet += $value['connet_zar'];
                                 $profit += $value['profit_zar'];
                                 $profit_rdnc += $value['profit_zar_rdnc'];
                              }
                              ?>
                              </tbody>
                              <tfoot>
                                 <?php
                                 echo '<tr>';
                                 echo '<td align="right"><b>Totals</b></td>';
                                 echo '<td align="right" style="vertical-align:middle;width:70px;padding: 1px;font-size: 13px;"><b>'.number_format($units, 0, 0, ' ').' ('.number_format($units_rdnc, 0, 0, ' ').')</b></td>';

                                 echo '<td align="right" style="vertical-align:middle;width:70px;padding: 1px;font-size: 13px;"><b>R '.number_format($connet, 2, '.', ' ').'</b></td>';
                                 echo '<td align="right" style="vertical-align:middle;width:70px;padding: 1px;font-size: 13px;"><b>R '.number_format($profit, 2, '.', ' ').'(R '.number_format($profit_rdnc, 2, '.', ' ').')</b></td>';
                                 echo '</tr>';

                                 echo '<tr>';
                                 echo '<td align="right"><b>Totals</b></td>';

                                 $units_totals = $units + $units_rdnc;
                                 echo '<td align="right" style="vertical-align:middle;padding: 1px;font-size: 13px;"><b>'.number_format($units_totals, 0, 0, ' ').'</b></td>';
                                 echo '<td align="right" style="vertical-align:middle;padding: 1px;font-size: 13px;"><b></b></td>';
                                 $profit_totals = $profit + $profit_rdnc;
                                 echo '<td align="right" style="vertical-align:middle;padding: 1px;font-size: 13px;"><b><span style="color:#993333;">R </span>'.number_format($profit_totals, 2, 0, ' ').'</b></td>';
                                 echo '</tr>'
                                 ?>
                              </tfoot>
                           </table>

                        </div><!-- /.box-body -->
                     </div><!-- /.box -->
                  </div>
               </div>
            </section>
         </div>
      </div>
   </section>




</aside>

<!--  THE JAVASCRIPT IMPORT ALWAYS APPEARS BEFORE LOCAL JAVASCRIPT AT THE BOTTOM OF THE PAGE -->
                                 <?php include("template_import_script.php"); //must import all scripts first    ?>
<!-- END JAVASCRIPT IMPORT -->
<script src="../js/plugins/datatables/jquery.dataTables.js" type="text/javascript"></script>

<script src="../js/plugins/datatables/dataTables.bootstrap.js" type="text/javascript"></script>




<script type="text/javascript">
   $(window).load(function () {
      $(".loader").fadeOut("slow");
      $(".loaderIcon").fadeOut("slow");
   })
</script>
<script type="text/javascript">

   $(document).ready(function() {
      //Sales Table Sorting
      $('#salesTable').DataTable( {
         "bPaginate": false, //Remove pageing frmo the datatable
         "bLengthChange": false, //Remove the show records per page control
         "aaSorting": [[4,'desc']],
         "aoColumns": [
            null,
            { "sType": "currency" },
            { "sType": "currency" },
            { "sType": "currency" },
            { "sType": "currency" },
            { "sType": "currency" }
         ]
      } );

      //Accounting Table sorting
      $('#accountTable').DataTable( {
         "bPaginate": false, //Remove pageing frmo the datatable
         "bLengthChange": false, //Remove the show records per page control
         "aaSorting": [[4,'desc']],
         "aoColumns": [
            null,
            { "sType": "currency" },
            { "sType": "currency" },
            { "sType": "currency" },
            { "sType": "currency" }
         ]
      } );

      //Networks Table sorting
      $('#networksTable').DataTable( {
         "bPaginate": false, //Remove pageing frmo the datatable
         "bLengthChange": false, //Remove the show records per page control
         "aaSorting": [[3,'desc']],
         "aoColumns": [
            null,
            { "sType": "currency" },
            { "sType": "currency" },
            { "sType": "currency" }
         ]
      } );

      //Provider Table sorting
      $('#routesTable').DataTable( {
         "bPaginate": false, //Remove pageing frmo the datatable
         "bLengthChange": false, //Remove the show records per page control
         "aaSorting": [[3,'desc']],
         "aoColumns": [
            null,
            { "sType": "currency" },
            { "sType": "currency" },
            { "sType": "currency" }
         ]
      } );



   } );




   function choosemonth(fromdate, todate)
   {

      var fdate = fromdate;
      var tdate = todate;
      window.location = "financialstats.php?fdate=" + fdate + "&tdate=" + tdate;
   }


   jQuery.extend(jQuery.fn.dataTableExt.oSort, {
      "title-numeric-pre": function (a) {
         var x = a.match(/title="*(-?[0-9\.]+)/)[1];
         return parseFloat(x);
      },
      "title-numeric-asc": function (a, b) {
         return ((a < b) ? -1 : ((a > b) ? 1 : 0));
      },
      "title-numeric-desc": function (a, b) {
         return ((a < b) ? 1 : ((a > b) ? -1 : 0));
      }
   });

   jQuery.extend(jQuery.fn.dataTableExt.oSort, {
      "signed-num-pre": function (a) {
         return (a == "-" || a === "") ? 0 : a.replace('+', '') * 1;
      },
      "signed-num-asc": function (a, b) {
         return ((a < b) ? -1 : ((a > b) ? 1 : 0));
      },
      "signed-num-desc": function (a, b) {
         return ((a < b) ? 1 : ((a > b) ? -1 : 0));
      }
   });

   jQuery.extend(jQuery.fn.dataTableExt.oSort,
       {
          "formatted-num-pre": function (a) {
             a = (a === "-" || a === "") ? 0 : a.replace(/[^\d\-\.]/g, "");
             return parseFloat(a);
          },
          "formatted-num-asc": function (a, b) {
             return a - b;
          },
          "formatted-num-desc": function (a, b) {
             return b - a;
          }
       });

   jQuery.extend(jQuery.fn.dataTableExt.oSort, {
      "currency-pre": function (a) {
         a = (a === "-") ? 0 : a.replace(/[^\d\-\.]/g, "");
         return parseFloat(a);
      },
      "currency-asc": function (a, b) {
         return a - b;
      },
      "currency-desc": function (a, b) {
         return b - a;
      }
   });


   //************************************************************************
   //Charts
   //************************************************************************
   //Array ( [2016-01-01] => Array ( [units] => 265394 [d] => 01 )
   new Morris.Bar({
      // ID of the element in which to draw the chart.
      element: 'myfirstchart',
      // Chart data records -- each entry in this array corresponds to a point on
      // the chart.

      data: [<?php echo implode(',', $morris); ?>],
      //data: [{day: 01, value: 265394},{day: 02, value: 234980},{day: 03, value: 207633}],

      // The name of the data record attribute that contains x-values.
      xkey: 'day',
      // A list of names of data record attributes that contain y-values.
      ykeys: ['value'],
      // Labels for the ykeys -- will be displayed when you hover over the
      // chart.
      labels: ['Value']
   });
   Morris.Donut({
      element: 'donut_network_units',
      colors: ["#ef0a21", "#ffca08", "#000000", "#3a89c9", "#56b85f"],
      data: [<?php echo implode(',', $donut_data_n); ?>]
      //data: [{label: "Download Sales", value: 12},{label: "In-Store Sales", value: 30},{label: "Mail-Order Sales", value: 20}]
      //data: [ {label: "MTN", value: 19248626},{label: "Cell-C", value: 3600746},{label: "Telkom", value: 443759},{label: "International", value: 1003691}]

   });
   Morris.Donut({
      element: 'donut_network_profit',
      colors: ["#ef0a21", "#ffca08", "#000000", "#3a89c9", "#56b85f"],
      data: [<?php echo implode(',', $donut_data_p); ?>]
      //data: [{label: "Download Sales", value: 12},{label: "In-Store Sales", value: 30},{label: "Mail-Order Sales", value: 20}]
      //data: [ {label: "MTN", value: 19248626},{label: "Cell-C", value: 3600746},{label: "Telkom", value: 443759},{label: "International", value: 1003691}]

   });

   function open_container(mccmnc,prefix_network_name,account_name,smsc)
   {
      var content = '<span class="glyphicon glyphicon-refresh glyphicon-spin"></span> Loading, please wait...';
      var title = "";
      if(mccmnc == "Show All")
      {
         title = prefix_network_name;
      }
      else
      {
         title = title.concat(prefix_network_name," (",mccmnc,")");
      }

      var footer = '<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>';

      setModalBox(title,content,footer);
      $('#myModal').modal('show');

      //run our file processor before submitting it to niel
      $.post("../pages/financialstats_network.php", {mccmnc:mccmnc, prefix_network_name:prefix_network_name,account_name:account_name,smsc:smsc, tdate: "<?php echo $tdate; ?>", fdate:"<?php echo $fdate; ?>"}).done(function (data) {
         $('#modal-bodyku').html(data);

      }).fail(function (data) {
         $('#modal-bodyku').html("Än error occurred.");
      });
   }


   function setModalBox(title,content,footer)
   {
      document.getElementById('modal-bodyku').innerHTML=content;
      document.getElementById('myModalLabel').innerHTML=title;
      document.getElementById('modal-footerq').innerHTML=footer;

   }

<!-- Template Footer -->

