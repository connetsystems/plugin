<?php
/**
 * Include the header template which sets up the HTML
 *
 * Don't forget to include template_import_script before any Javascripting
 * Don't forget to include template footer.php at the bottom of the page as well
 */
$headerPageTitle = "Add Route"; //set the page title for the template import
TemplateHelper::initialize();

if (isset($_SESSION['serviceId']) && $_SESSION['serviceId'] != '') {
   $clients = getServicesInAccountWithOutRid($_SESSION['accountId']);
} else {
   $clients = getClients();
}

if (isset($_GET['client']) && $_GET['client'] != 'Please Select') {
   $cl = $_GET['client'];
   $clp = strrpos($cl, ' ');
   $cl = trim(substr($cl, $clp, strlen($cl)));
   if (strpos($cl, '&')) {
      $cl = strstr($cl, '&', true);
   }
   $dsp = 'block';
   $dsp2 = 'none';
   /* $dir = '../img/routes';
     $serProviders = scandir($dir); */
} elseif (isset($_GET['sId'])) {
   $cl = $_GET['sId'];
   $dsp = 'block';
} else {
   $cl = '0';
   $dsp = 'block';
   $dsp2 = 'block';
}

if (isset($_GET['client2']) && $_GET['client2'] != 'Please Select') {
   $cl2 = $_GET['client2'];
   $clp = strrpos($cl2, ' ');
   $cl2 = trim(substr($cl2, $clp, strlen($cl2)));
   if (strpos($cl2, '&')) {
      $cl2 = strstr($cl2, '&', true);
   }
   //$dsp = 'block';
   //echo '<br>cl2='.$cl2;
   $dsp2 = 'block';
   $subRoutes2 = getResellerDefaultRouteData($cl2);
   /* $dir = '../img/routes';
     $serProviders = scandir($dir); */
} elseif (isset($_GET['sId2'])) {
   $cl2 = $_GET['sId2'];
   $subRoutes2 = getResellerDefaultRouteData($cl2);
   $dsp = 'block';
} else {
   $cl2 = '0';
}

if (isset($cl)) {
   $sId = $cl;
   $sId2 = $cl2;

   if (isset($_GET['removeId'])) {
      //echo '<br>REMOVIN!!!---->'.$_GET['removeId'];
      $remId = $_GET['removeId'];
      removeRouteFromSubAccount($remId);
   }

   if (isset($_GET['nVal'])) {
      $nVal = $_GET['nVal'];
      $manId = $_GET['manId'];
      $rDesc = urldecode($_GET['rDesc']);
      $provId = urldecode($_GET['provId']);
      $msgType = urldecode($_GET['msgType']);
      $rdn = urldecode($_GET['rdn']);
      $rc = urldecode($_GET['rc']);
      $sts = urldecode($_GET['sts']);
      $regex = urldecode($_GET['regex']);
      $prior = urldecode($_GET['prior']);
      $mcc = urldecode($_GET['mcc']);
      $rcid = urldecode($_GET['rcid']);

      createNewClientRoutes($nVal, $sId, $manId, $rDesc, $provId, $msgType, $rdn, $rc, $sts, $regex, $prior, $mcc, $rcid);
   }

   //$resellerRoutes = getResellerDefaultRouteData($_SESSION['serviceId'], $sId);
   $subRoutes = getAddedDefaultRouteData($sId);
}
?>


<!-- Right side column. Contains the navbar and content of the page -->
<aside class="right-side">
   <!--//////////////////////////-->
   <section class="content-header">
      <h1>
         Add Routes To Client
      </h1>
   </section>

   <section class="content">
      <div class="row">
         <form action="newresellerservice.php" class="col-md-12" method="get" id="newService" style="border:none;">
            <!--div class="col-md-12"-->
            <div class="box box-connet" style="padding-bottom:10px; border-top-color:<?php echo $accRGB; ?>;">
               <div class="box-body" id="notify" style="padding-bottom:0px;">
                  <div class="callout callout-info" style="margin-bottom:0px;">
                     <h4>Add Routes To Client Instructions / Details</h4>
                     <p>Here you should add the routes you wish to assign to the service you just created.</p>
                  </div>
                  <div id="notifyE" class="callout callout-danger" style="display:none;" >
                     <h4>Error:</h4>
                     <p>The selling price has to be greater than or the same as the cost price.</p>
                  </div>
               </div>
            </div>
         </form>
      </div>
   </section>

   <!--Routes from -->
   <section class="content">
      <div class="box box-connet" style="margin-bottom:10px; border-top-color:<?php echo $accRGB; ?>; height:auto; display:<?php echo $dsp; ?>;" >
         <div class="box-header">
            <h3 class="box-title">Routes from:</h3>
         </div>
         <div class="box-body">
            <!--label>Client List</label-->
            <select class="form-control" name="client2" id="client2" form="clientList2" OnChange="reloadOnSelect();">
               <?php
               if ($cl2 == '0') {
                  echo '<option SELECTED>Please Select</option>';
               }
               foreach ($clients as $key => $value) {
                  $sIdt = $value['service_id'];
                  if ($cl2 == $sIdt) {
                     $accountName = $value['account_name'];
                     $serviceName = $value['service_name'];
                     echo "<option name='" . $sIdt . "' SELECTED>" . $accountName . " - " . $serviceName . " - " . $sIdt . "</option>";
                  } else {
                     echo "<option name='" . $sIdt . "'>" . $value['account_name'] . " - " . $value['service_name'] . " - " . $sIdt . "</option>";
                  }
               }
               ?>
            </select>
         </div>
         <div class="box-body" style="padding:0px;">
            <div class="box-body table-responsive">
               <table id="example2" class="table table-bordered table-striped dataTable table-hover">
                  <thead>
                     <tr>
                        <!--th>#</th-->
                        <th>Country</th>
                        <th>Network</th>
                        <th><center>SMSC</center></th>
                  <th><center>SELL RATE</center></th>
                  <th><center>Remove Route</center></th>
                  </tr>
                  </thead>

                  <tbody>
                     <?php
                     if (isset($subRoutes2)) {
                        $numId = 0;

                        foreach ($subRoutes2 as $key2 => $value2) {
                           $rDescPos = strrpos($value2['route_description'], ' ');
                           $rDesc = substr($value2['route_description'], 0, $rDescPos - 1);
                           $rDesc = urlencode($rDesc);

                           $urlReg = urlencode($value2['route_match_regex']);

                           if (strpos($value2['NETWORK'], 'Special')) {
                              $logo = trim('Special');
                           } else {
                              $logo = strstr($value2['NETWORK'], ' (', true);
                              $logoP = strpos($logo, ' / ');
                              //echo '<br>logoP='.$logoP.'<-';
                              if ($logoP != '') {
                                 $logo = substr($logo, $logoP + 3, strlen($logo));
                              }
                              $logo = trim($logo);
                           }

                           $flag = str_replace(' / ', '', $value2['COUNTRY']);
                           echo '<tr name="routeId" value2="' . $value2['route_id'] . '">';
                           echo '<td style="vertical-align:middle;"><img src="../img/flags/' . $flag . '.png">&nbsp;&nbsp;' . $value2['COUNTRY'] . '</td>';
                           //echo '<br>->'.$logo.'.png<';
                           $img = 'http://portal.connet-systems.com/img/serviceproviders/' . $logo . '.png';
                           $header_response = get_headers($img, 1);
                           if (strpos($header_response[0], "404") !== false) {
                              //echo "<td><img src='../img/serviceproviders/Special.png'>&nbsp;&nbsp;".$value2['NETWORK']."</td>";
                              echo '<td style="vertical-align:middle;"><img src="../img/serviceproviders/Special.png">&nbsp;&nbsp;' . $value2['NETWORK'] . '</td>';
                              // FILE DOES NOT EXIST
                           } else {
                              //echo "<td><img src='../img/serviceproviders/".$logo.".png'>&nbsp;&nbsp;".$value2['NETWORK']."</td>";
                              echo '<td style="vertical-align:middle;"><img src="../img/serviceproviders/' . $logo . '.png">&nbsp;&nbsp;' . $value2['NETWORK'] . '</td>';
                              // FILE EXISTS!!
                           }
                           if ($value2['STATUS'] == 'ENABLED') {
                              echo '<td style="vertical-align:middle;"><center><i class="fa fa-check" style="color:#00ff00;"></i>&nbsp;&nbsp;' . $value2['STATUS'] . '</center></td>';
                           } else {
                              echo '<td style="vertical-align:middle;"><center><i class="fa fa-times" style="color:#ff0000;"></i>&nbsp;&nbsp;' . $value2['STATUS'] . '</center></td>';
                           }
                           echo '<td style="vertical-align:middle;"><center><div style="display: inline-table;"></div>' . $value2['RATE'] . '</div></center></td>';
                           echo '<td style="vertical-align:middle;">
                                                        <center>
                                                                <a class="btn btn-block btn-social btn-dropbox" name="routeRate' . $value2['route_id'] . '" id="routeRate' . $value2['route_id'] . '" onclick="addForeign(this.name, ' . $numId . ', ' . $value2['provider_id'] . ', \'' . $value2['route_msg_type'] . '\', \'' . urlencode($value2['route_description']) . '\', \'' . urlencode($value2['route_display_name']) . '\', \'' . urlencode($value2['route_code']) . '\', \'' . $value2['STATUS'] . '\', \'' . $urlReg . '\', \'' . $value2['route_match_priority'] . '\', \'' . $value2['route_billing'] . '\', \'' . urlencode($value2['MCCMNC']) . '\', ' . $value2['route_collection_id'] . ')" style="background-color:' . $accRGB . '; color: #ffffff; border: 2px solid; border-radius: 6px !important; width: 140px; margin-top:0px;">
                                                                    <i class="fa fa-plus"></i>Assign Route
                                                                </a>
                                                        </center>
                                                    </td>';
                           echo '</tr>';

                           $numId++;
                        }
                     }
                     ?>
                  </tbody>
               </table>
            </div>
         </div>
   </section>

   <!--Routes to -->
   <section class="content">
      <div class="box box-connet" style="padding-bottom:1px; border-top-color:<?php echo $accRGB; ?>; height:auto; display:<?php echo $dsp; ?>;" >
         <div class="box-header">
            <h3 class="box-title">Assigned routes:</h3>
         </div>
         <div class="box-body">
            <!--label>Client List (Assigning To)</label-->
            <select class="form-control" name="client" id="client" form="clientList" OnChange="reloadOnSelect();">
               <?php
               if ($cl == '0') {
                  echo '<option SELECTED>Please Select</option>';
               }
               foreach ($clients as $key => $value) {
                  $sIdt = $value['service_id'];
                  if ($cl == $sIdt) {
                     $accountName = $value['account_name'];
                     $serviceName = $value['service_name'];
                     echo "<option name='" . $sIdt . "' SELECTED>" . $accountName . " - " . $serviceName . " - " . $sIdt . "</option>";
                  } else {
                     echo "<option name='" . $sIdt . "'>" . $value['account_name'] . " - " . $value['service_name'] . " - " . $sIdt . "</option>";
                  }
               }
               ?>
            </select>
         </div>
         <div class="box-body" style="padding:0px;">
            <div class="box-body table-responsive">
               <table id="example2" class="table table-bordered table-striped dataTable table-hover">
                  <thead>
                     <tr>
                        <!--th>#</th-->
                        <th>Country</th>
                        <th>Network</th>
                        <th><center>SMSC</center></th>
                  <th><center>SELL RATE</center></th>
                  <th><center>Remove Route</center></th>
                  </tr>
                  </thead>

                  <tbody>
                     <?php
                     if (isset($subRoutes)) {
                        $numId = 0;
                        foreach ($subRoutes as $key => $value) {
                           if (strpos($value['NETWORK'], 'Special')) {
                              $logo = trim('Special');
                           } else {
                              $logo = strstr($value['NETWORK'], ' (', true);
                              $logoP = strpos($logo, ' / ');
                              //echo '<br>logoP='.$logoP.'<-';
                              if ($logoP != '') {
                                 $logo = substr($logo, $logoP + 3, strlen($logo));
                              }
                              $logo = trim($logo);
                           }

                           $flag = str_replace(' / ', '', $value['COUNTRY']);
                           echo '<tr name="routeId" value="' . $value['route_id'] . '">';
                           echo '<td style="vertical-align:middle;"><img src="../img/flags/' . $flag . '.png">&nbsp;&nbsp;' . $value['COUNTRY'] . '</td>';
                           //echo '<br>->'.$logo.'.png<';
                           $img = 'http://portal.connet-systems.com/img/serviceproviders/' . $logo . '.png';
                           $header_response = get_headers($img, 1);
                           if (strpos($header_response[0], "404") !== false) {
                              //echo "<td><img src='../img/serviceproviders/Special.png'>&nbsp;&nbsp;".$value['NETWORK']."</td>";
                              echo '<td style="vertical-align:middle;"><img src="../img/serviceproviders/Special.png">&nbsp;&nbsp;' . $value['NETWORK'] . '</td>';
                              // FILE DOES NOT EXIST
                           } else {
                              //echo "<td><img src='../img/serviceproviders/".$logo.".png'>&nbsp;&nbsp;".$value['NETWORK']."</td>";
                              echo '<td style="vertical-align:middle;"><img src="../img/serviceproviders/' . $logo . '.png">&nbsp;&nbsp;' . $value['NETWORK'] . '</td>';
                              // FILE EXISTS!!
                           }
                           if ($value['STATUS'] == 'ENABLED') {
                              echo '<td style="vertical-align:middle;"><center><i class="fa fa-check" style="color:#00ff00;"></i>&nbsp;&nbsp;' . $value['STATUS'] . '</center></td>';
                           } else {
                              echo '<td style="vertical-align:middle;"><center><i class="fa fa-times" style="color:#ff0000;"></i>&nbsp;&nbsp;' . $value['STATUS'] . '</center></td>';
                           }
                           echo '<td style="vertical-align:middle;"><center><div style="display: inline-table;"></div>' . $value['RATE'] . '</div></center></td>';

                           echo '<td style="vertical-align:middle;">
                                                        <center>
                                                                <a class="btn btn-block btn-social btn-dropbox" name="' . $value['route_id'] . '" onclick="removeRate(' . $value['route_id'] . ')" style="background-color:' . $accRGB . '; color: #ffffff; border: 2px solid; border-radius: 6px !important; width: 140px; margin-top:0px;">
                                                                    <i class="fa fa-minus"></i>Remove Route
                                                                </a>
                                                        </center>
                                                    </td>';
                           echo '</tr>';

                           $numId++;
                        }
                     }
                     ?>
                  </tbody>
               </table>

                <!--a class="btn btn-block btn-social btn-dropbox" name="" onclick="gotoAddUsers(<?php echo $sId; ?>)" style="background-color:<?php echo $accRGB; ?>; color: #ffffff; border: 2px solid; border-radius: 6px !important; width: 140px; margin-top:0px;">
                    <i class="fa fa-users"></i>Add Users
                </a-->
            </div>
            </section>


            </aside>


            <!--  THE JAVASCRIPT IMPORT ALWAYS APPEARS BEFORE LOCAL JAVASCRIPT AT THE BOTTOM OF THE PAGE -->
            <?php include("template_import_script.php"); //must import all scripts first ?>
            <!-- END JAVASCRIPT IMPORT -->

            <script type="text/javascript">
               $(window).load(function ()
               {
                  $(".loader").fadeOut("slow");
                  $(".loaderIcon").fadeOut("slow");
               })
            </script>

            <script type="text/javascript">

               /*function gotoAddUsers(sId)
                {
                $(".loader").fadeIn("slow");
                $(".loaderIcon").fadeIn("slow");
                window.location = "newuser.php?sId=" + sId;
                };*/


               function removeRate(rId)
               {
                  //alert(' rId=' + rId);
                  window.location = "addrouteadmin.php?sId=" + <?php echo $sId; ?> + "&sId2=" + <?php echo $sId2; ?> + "&removeId=" + rId;
               }
               ;

               function urldecode(url)
               {
                  return decodeURIComponent(url.replace(/\+/g, ' '));
               }

               function reloadOnSelect()
               {
                  name = document.getElementById('client').value;
                  name2 = document.getElementById('client2').value;
                  $(".loader").fadeIn("slow");
                  $(".loaderIcon").fadeIn("slow");
                  encodeURI(name);
                  encodeURI(name2);
                  window.location = "addrouteadmin.php?client=" + encodeURIComponent(name) + "&client2=" + encodeURIComponent(name2);
               }

               function addForeign(v, numId, provider_id, route_msg_type, rDesc, route_display_name, route_code, STATUS, route_match_regex, route_match_priority, route_billing, MCCMNC, route_collection_id)
               {
                  //alert(rDesc);
                  routeToAdd = 'routeRate' + v;
                  /*var r2Add = document.getElementById(routeToAdd);
                   r2Add = parseInt(r2Add.value);*/
                  //var r2Add = urldecode(route_billing);
                  var oldRoute = urldecode(route_billing);
                  var n = oldRoute.indexOf(",");
                  oldRoute = oldRoute.substr(0, n);
                  oldRoute = parseInt(oldRoute);

                  addRouteAndReload(v, oldRoute, numId, provider_id, route_msg_type, rDesc, route_display_name, route_code, STATUS, route_match_regex, route_match_priority, route_billing, MCCMNC, route_collection_id);

               }

               function addRouteAndReload(routeId, oldRoute, numId, provider_id, route_msg_type, rDesc, route_display_name, route_code, STATUS, route_match_regex, route_match_priority, route_billing, MCCMNC, route_collection_id)
               {
                  var manId = <?php echo $_SESSION['userId']; ?>;
                  //alert(manId);
                  //alert(rDesc);
                  $(".loader").fadeIn("slow");
                  $(".loaderIcon").fadeIn("slow");

                  window.location = "addrouteadmin.php?sId2=" + <?php echo $sId2; ?> + "&sId=" + <?php echo $sId; ?> + "&rId=" + routeId + "&nVal=" + oldRoute + "&numId=" + numId + "&manId=" + manId + "&rDesc=" + rDesc + "&provId=" + provider_id + "&msgType=" + route_msg_type + "&rdn=" + route_display_name + "&rc=" + route_code + "&sts=" + STATUS + "&regex=" + route_match_regex + "&prior=" + route_match_priority + "&mcc=" + MCCMNC + "&rcid=" + route_collection_id;
               }
               ;

            </script>

            <script language=Javascript>
               function isNumberKey(oV, v, evt)
               {
                  var charCode = (evt.which) ? evt.which : evt.keyCode;

                  if (charCode != 46 && charCode > 31
                          && (charCode < 48 || charCode > 57))
                     return false;

                  return true;
               }
            </script>

            <!-- Template Footer -->