<?php
/**
 * Include the header template which sets up the HTML
 *
 * Don't forget to include template_import_script before any Javascripting
 * Don't forget to include template footer.php at the bottom of the page as well
 */
//-------------------------------------------------------------
// Template
//-------------------------------------------------------------
TemplateHelper::setPageTitle('Update Credits');
TemplateHelper::initialize();

//-------------------------------------------------------------
// Permissions
//-------------------------------------------------------------
if (isset($_GET['client'])) {

   $split = explode(' ', trim($_GET['client']));
   header('location: ' . $_SERVER['DOCUMENT_URI'] . '?service_id=' . (integer) end($split));
   die();
}

if (LoginHelper::isSystemAdmin()) {

   $clients = getClients();
} else {
   /**
    * This is flawed, if an account has more than one reseller service, then
    * It will see all of their sub-services, even if they don't have permissions...
    */
   $account_id = LoginHelper::getCurrentAccountId();
   $clients = getServicesInAccountWithOutRid($account_id);

   foreach ($clients as $index => $service) { //Can't edit reseller accounts
      $service_id = (integer) $service['service_id'];
      $service_id_reseller = (integer) $service['service_id_reseller'];

      if ($service_id_reseller == $service_id) {

         unset($clients[$index]);
      }
   }
}

if (isset($_GET['service_id'])) {

   $service_id = (integer) $_GET['service_id'];
   $permission = false;
   foreach ($clients as $service) {

      $_service_id = (integer) $service['service_id'];
      if ($service_id === $_service_id) {
         $permission = true;
         break;
      }
   }

   if (!$permission) {

      header('location: ' . $_SERVER['DOCUMENT_URI']);
      die();
   }
}

//-------------------------------------------------------------
// Credits
//-------------------------------------------------------------
$err1 = 0;
$err2 = 0;
$succ = 0;
$height = 300;

if (isset($_GET['service_id'])) {
   $service_id = $_GET['service_id'];

   //get the current funds for this service
   $prevFund = getFundsOnly($service_id);
   $prevFund = $prevFund / 10000;
   $prevFund = round($prevFund, 2);
} else {
   $serviceId = -1;
}

if (isset($_GET['ammount'])) {
   $ammount = $_GET['ammount'];
} else {
   $ammount = '';
}

if (isset($_GET['desc'])) {
   $desc = $_GET['desc'];
} else {
   $desc = '';
}

if (isset($prevFund)) {
   $height = 356;
}
?>

<aside class="right-side">
   <section class="content-header">
      <h1>
         Update Credits
         <!--small>Control panel</small-->
      </h1>
   </section>
   <br>
   <section class="content">
      <div class="row col-lg-12">
         <div class="box box-connet" id="boxBG" style="height:<?php echo $height; ?>px; margin-top:-15px;display:<?php echo $singleView; ?>; border-top-color:<?php echo $accRGB; ?>;">
            <form action="confirmcredits.php" method="post" id="updCredit" class="sidebar-form" style="border:0px;">
               <div class="form-group">
                  <label>Client List</label>
                  <select class="form-control" id="serviceId" name="serviceId" OnChange="reloadOnSelect(this.value);">
                     <?php
                     if ($service_id == '0') {
                        echo '<option value="-1" SELECTED>Please select a client...</option>';
                     }
                     foreach ($clients as $key => $value) {
                        if ($service_id == $value['service_id']) {
                           echo "<option value='" . $value['service_id'] . "' SELECTED>" . htmlentities($value['account_name'] . " - " . $value['service_name'] . " - " . $value['service_id']) . "</option>";
                        } else {
                           echo "<option value='" . $value['service_id'] . "'>" . htmlentities($value['account_name'] . " - " . $value['service_name'] . " - " . $value['service_id']) . "</option>";
                        }
                     }
                     ?>
                  </select>
               </div>
               <?php
               if (isset($_GET['success']) && $_GET['success'] == 1) {
                  echo '<div class="callout callout-success">';
                  echo "You have successfully made a payment.";
                  echo '<div style="float:right; margin-top:-3px">';
                  echo '<i class="fa fa-check" style="color:#5cb157; font-size:20pt;"></i>';
                  echo '</div>';
                  echo '</div>';
               } elseif (isset($_GET['success']) && $_GET['success'] == 0) {
                  echo '<div class="callout callout-danger">';
                  echo "There was a sever error. We could not udate the credit values. Please try again later.";
                  echo '<div style="float:right; margin-top:-3px">';
                  echo '<i class="fa fa-check" style="color:#5cb157; font-size:20pt;"></i>';
                  echo '</div>';
                  echo '</div>';
               }
               ?>

               <div class="form-group">
                  <div class="box box-solid">
                     <div class="form-group" style="padding:10px;">
                        <?php
                        if (isset($prevFund)) {
                           echo '<label>Current Balance (Balance before payment)</label>';
                           echo '<p>R ' . $prevFund . '</p>';
                           //      echo '<br>';
                        }
                        ?>
                        <input type="hidden" id="prevAm" name="prevAmount" value="<?php echo $prevFund; ?>">
                        <label>Payment Amount (Credit to load in RAND value) e.g. 100</label>
                        <input type="text" id="ammount" class="form-control" onkeypress="return isNumberKey(event)" style="border:1px solid #929292;margin-top:5px;height:35px;" name="ammount" placeholder="" value="<?php echo $ammount; ?>">
                        <br>
                        <label>Description</label>
                        <input type="text" id="desc" class="form-control" style="border:1px solid #929292;margin-top:5px;height:35px;" name="desc" placeholder="" value="<?php echo htmlspecialchars($desc); ?>">
                     </div>
                  </div>
               </div>

               <div id="errAlert" class="callout callout-danger" style="margin-top:-13px; display:none;">
                  Alert:<b>&nbsp;</b>Please fill in all the above fields!
                  <div style="float:right; margin-top:-3px">
                     <i class="fa fa-times" style="color:#cc3333; font-size:20pt;"></i>
                  </div>
               </div>

               <div id="errAlert2" class="callout callout-danger" style="margin-top:-13px; display:none;">
                  Alert:<b>&nbsp;</b>You cannot assign more funds than you already have!
                  <div style="float:right; margin-top:-3px">
                     <i class="fa fa-times" style="color:#cc3333; font-size:20pt;"></i>
                  </div>
               </div>

               <div id="errAlert3" class="callout callout-danger" style="margin-top:-13px; display:none;">
                  Alert:<b>&nbsp;</b>You cannot subtract more than what is available in the sub account!
                  <div style="float:right; margin-top:-3px">
                     <i class="fa fa-times" style="color:#cc3333; font-size:20pt;"></i>
                  </div>
               </div>

               <div class="form-group" style="padding-top:10px;">
                  <a class="btn btn-block btn-social btn-dropbox " onclick="checkFields();" style="background-color: <?php echo $accRGB; ?>; color: #ffffff; border: 2px solid; border-radius: 6px !important; float:left; width: 145px; height:36px; margin-top:-25px;padding-left:10px;">
                     <i class="fa fa-credit-card"></i>Update Credit
                  </a>
               </div>

            </form>
         </div>
      </div>
   </section>
</aside>

<!--  THE JAVASCRIPT IMPORT ALWAYS APPEARS BEFORE LOCAL JAVASCRIPT AT THE BOTTOM OF THE PAGE -->
<?php include("template_import_script.php"); //must import all scripts first   ?>
<!-- END JAVASCRIPT IMPORT -->

<script type="text/javascript">
   function reloadOnSelect(serviceId)
   {
      $(".loader").fadeIn("slow");
      $(".loaderIcon").fadeIn("slow");

      window.location = "../pages/updatecredits.php?service_id=" + serviceId;
   }

   function checkFields()
   {
      var serviceId = document.getElementById("serviceId").value;
      var am = document.getElementById("ammount").value;
      var des = document.getElementById("desc").value;
      var currentAmount = <?php echo ($s / 10000); ?>;
      var accType = "<?php echo $_SESSION['serviceBillingType']; ?>";
      //number_format
      /*alert('1' + serviceId);
       alert('3' + des);*/
      //alert('reqAmount=' + am);
      var subAccAmount = parseFloat("<?php echo $prevFund; ?>");
      var fAm = parseFloat(am);
      //alert('subCA=' + subAccAmount + fAm);


      if (accType == "PREPAID" && subAccAmount + fAm < 0)
      {
         var err3 = document.getElementById("errAlert3");
         err3.style.display = 'block';
      }
      else if ((serviceId != -1) && (am != '') && (am != 0) && (des != ''))
      {
         if (currentAmount < am && accType == "PREPAID")
         {
            //alert('YESSSS!');
            var box = document.getElementById("boxBG");
            var newH = parseInt(box.style.height) + 58;
            box.style.height = newH + "px";
            var err2 = document.getElementById("errAlert2");
            err2.style.display = 'block';
         }
         else
         {
            document.forms['updCredit'].submit();
         }
         //var box = document.getElementById("boxBG");
         //var newH = parseInt(box.style.height)+58;
         //box.style.height = newH + "px";
         var err = document.getElementById("errAlert");
         err.style.display = 'none';
      }
      else
      {
         var box = document.getElementById("boxBG");
         var newH = parseInt(box.style.height) + 58;
         box.style.height = newH + "px";
         var err = document.getElementById("errAlert");
         err.style.display = 'block';
      }
   }

   $(window).load(function () {
      $(".loader").fadeOut("slow");
      $(".loaderIcon").fadeOut("slow");
   })

   function isNumberKey(evt)
   {
      var charCode = (evt.which) ? evt.which : evt.keyCode;

      if (charCode == 45)
         return true;

      if (charCode != 46 && charCode > 31
              && (charCode < 48 || charCode > 57))
         return false;

      return true;
   }
</script>

<!-- Template Footer -->

