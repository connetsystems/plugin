<?php
/**
 * Include the header template which sets up the HTML
 *
 * Don't forget to include template_import_script before any Javascripting
 * Don't forget to include template footer.php at the bottom of the page as well
 */
$headerPageTitle = "Connet Systems | Network Report"; //set the page title for the template import
TemplateHelper::initialize();

$cl = "";
$accountName = "";
$serviceName = "";

$pgUrl = strrchr(realpath(__FILE__), '/');
$pgUrl = '/pages' . $pgUrl;
$pgId = getRoleIDFromUrl($pgUrl);
$pgId = substr($pgId, 0, 1);

$roles = $_SESSION['roles'];

if (isset($_GET['startDate']) && isset($_GET['endDate'])) {
   $startDate = $_GET['startDate'];
   $endDate = $_GET['endDate'];
} else {
   $startDate = date('Y-m-d 00:00:00');
   $endDate = date('Y-m-d 23:59:59');
}

$networkReport = getNetworkReport($startDate, $endDate);

$totalSends = 0;

foreach ($networkReport as $key => $value) {
   $totalSends += $value['cdr_record_units'];
}

$totalSends = number_format($totalSends, 0, '.', ' ');
?>
<!-- Right side column. Contains the navbar and content of the page -->
<aside class="right-side">
   <!-- Content Header (Page header) -->
   <section class="content-header">
      <h1>
         Networks
         <!--small>Control panel</small-->
      </h1>

      <!--ol class="breadcrumb">
          <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
          <li class="active">Network Report</li>
      </ol-->
   </section>
   <!-- Main content -->
   <!-- we are adding the .panel class so bootstrap.js collapse plugin detects it -->
   <section class="content">
      <div class="box box-connet" style="padding-bottom:1px; border-top-color:<?php echo $accRGB; ?>;">
         <div class="box-body">
            <div class="callout callout-info">
               <h4>Network Report Instructions / Details</h4>
               <p>This page will show you ALL NETWORK traffic for the date range that you specify in the calendar below.</p>
            </div>
            <div id="reportrange" class="select pull-left" onmouseover="" style="cursor: pointer;">
               <i class="fa fa-calendar fa-lg"></i>
               <span style="font-size:15px">&nbsp;&nbsp;<?php echo $startDate; ?>&nbsp;&nbsp;-&nbsp;&nbsp;<?php echo $endDate; ?>&nbsp;&nbsp;</span><b class="caret"></b>
            </div>
            <br>

            <br>
            <div class="callout callout-success" style="margin-bottom:10px;">
               <i class="fa fa-thumbs-o-up"></i>
               &nbsp;Total sent for date range: <b><?php echo $totalSends; ?></b>
            </div>
         </div><!-- /.box-body -->
      </div>

      <div class="box box-connet" style="padding-bottom:1px; border-top-color:<?php echo $accRGB; ?>;">
         <div class="box-header">
            <h3 class="box-title">Network Traffic Total</h3>
         </div><!-- /.box-header -->
         <div class="box-body table-responsive">
            <table id="example2" class="table table-bordered table-striped dataTable table-hover">
               <thead>
                  <tr>
                     <!--th>#</th-->
                     <th>Country</th>
                     <th>Network</th>
                     <th>SMSC</th>
                     <th><center>Units Sent</center></th>
               <th><center>Cost/Unit</center></th>
               </tr>
               </thead>
               <tbody>
                  <?php
                  foreach ($networkReport as $key => $value) {
                     $value['cdr_record_units'] = number_format($value['cdr_record_units'], 0, '.', ' ');
                     $pos = strpos($value['smsc'], "_");
                     $smsc = substr($value['smsc'], 0, $pos);
                     $posr = strrpos($value['smsc'], "_");
                     $value['smsc'] = substr($value['smsc'], 0, $posr);
                     if (substr($value['prefix_country_name'], 0, 3) == "USA") {
                        $flag = "USA";
                     } else {
                        $flag = $value['prefix_country_name'];
                     }
                     $flag = str_replace('/', '&', $flag);
                     echo '<tr>';
                     //$s1 = substr_count($value['network'], '/');
                     $s1 = strrpos($value['network'], '/');
                     $s2 = strpos($value['network'], ' (');

                     $logo = substr($value['network'], $s1, $s2 - $s1);
                     $logo = trim($logo);
                     //echo "<br>1logi-".$logo.'-';
                     if ($logo != "Cell C") {  // | $logo != 'Vodacom SA' | $logo != 'MTN-SA'
                        if ($logo != 'Vodacom SA') {
                           if ($logo != 'MTN-SA') {
                              if ($logo != 'Telkom Mobile') {
                                 if ($logo != 'MTN (Nigeria)') {
                                    $logo = substr($value['network'], $s1 + 1, $s2 - $s1);
                                    $logo = trim($logo);
                                 }
                              }
                           }
                        }
                     }
                     $actual_link = $_SERVER['HTTP_HOST'];

                     $img = 'http://' . $actual_link . '/img/flags/' . $value['prefix_country_name'] . '.png';
                     //$img = $actual_link.'/connet.png';
                     //echo "<br>2logi-".$img.'-';
                     $header_response = get_headers($img, 1);
                     if (strpos($header_response[0], "404") !== false) {
                        echo "<td><img src='../img/serviceproviders/Special.png'>&nbsp;&nbsp;" . $value['prefix_country_name'] . "</td>";
                        // FILE DOES NOT EXIST
                     } else {
                        echo '<td><img src="../img/flags/' . $flag . '.png">&nbsp;&nbsp;' . $value['prefix_country_name'] . '</td>';
                        // FILE EXISTS!!
                     }

                     //$img = '../img/serviceproviders/'.$logo.'.png';
                     $img = 'http://' . $actual_link . '/img/flags/' . $value['prefix_country_name'] . '.png';
                     $header_response = get_headers($img, 1);
                     if (strpos($header_response[0], "404") !== false) {
                        echo "<td><img src='../img/serviceproviders/Special.png'>&nbsp;&nbsp;" . $value['network'] . "</td>";
                        // FILE DOES NOT EXIST
                     } else {
                        echo '<td><img src="../img/serviceproviders/' . $logo . '.png">&nbsp;&nbsp;' . $value['network'] . '</td>';
                        // FILE EXISTS!!
                     }
                     // echo '<td>'..'</td>';
                     //$img = '../img/routes/'.$value['smsc'].'.png';
                     $img = 'http://' . $actual_link . '/img/serviceproviders/' . $smsc . '.png';
                     $header_response = get_headers($img, 1);
                     if (strpos($header_response[0], "404") !== false) {
                        echo "<td><img src='../img/serviceproviders/Special.png'>&nbsp;&nbsp;" . ucfirst($value['smsc']) . "</td>";
                        // FILE DOES NOT EXIST
                     } else {
                        //echo "<td><img src='../img/serviceproviders/".$logo.".png'>&nbsp;&nbsp;".$value['NETWORK']."</td>";
                        echo '<td><img src="../img/serviceproviders/' . $smsc . '.png">&nbsp;&nbsp;' . ucfirst($value['smsc']) . '</td>';
                        // FILE EXISTS!!
                     }

                     echo '<td><center><span class="label label-primary" title="' . $value['cdr_record_units'] . '" >' . $value['cdr_record_units'] . '</span></center></td>';

                     if ($value['cdr_cost_per_unit'] <= 0) {
                        echo '<td><center><span class="label label-danger" title="' . $value['cdr_cost_per_unit'] . '" >' . ($value['cdr_cost_per_unit'] / 10000) . '</span></center></td>';
                     } else {
                        echo '<td><center><span class="label label-success" title="' . $value['cdr_cost_per_unit'] . '" >' . ($value['cdr_cost_per_unit'] / 10000) . '</span></center></td>';
                     }
                     echo '</tr>';
                  }
                  ?>
               </tbody>
               <tfoot>
                  <tr>
                     <!--th>#</th-->
                     <th>Country</th>
                     <th>Network</th>
                     <th>SMSC</th>
                     <th><center>Units Sent</center></th>
               <th><center>Cost/Unit</center></th>
               </tr>
               </tfoot>
            </table>
         </div><!-- /.box-body -->
         <div class="box-body">
            <div class="form-group">
               <a onclick="saveRep();" class="btn btn-block btn-social btn-dropbox " style="background-color:<?php echo $accRGB; ?>; border: 2px solid; border-radius: 6px !important; float:left; width: 134px; margin-top:-19px;">
                  <i class="fa fa-save"></i>Save Report
               </a>
            </div>
         </div>
      </div>
   </section>
   <br>
</aside>

<!--  THE JAVASCRIPT IMPORT ALWAYS APPEARS BEFORE LOCAL JAVASCRIPT AT THE BOTTOM OF THE PAGE -->
<?php include("template_import_script.php"); //must import all scripts first  ?>
<!-- END JAVASCRIPT IMPORT -->

<script type="text/javascript">
   $('#reportrange').daterangepicker(
           {
              ranges: {
                 'Today': [moment(), moment()],
                 'Yesterday': [moment().subtract('days', 1), moment().subtract('days', 1)],
                 'Last 7 Days': [moment().subtract('days', 6), moment()],
                 'Last 30 Days': [moment().subtract('days', 29), moment()],
                 'This Month': [moment().startOf('month'), moment().endOf('month')],
                 'Last Month': [moment().subtract('month', 1).startOf('month'), moment().subtract('month', 1).endOf('month')]
              },
              startDate: moment(<?php echo '"' . $startDate . '"'; ?>),
              endDate: moment(<?php echo '"' . $endDate . '"'; ?>)
           },
           function (start, end) {
              $(".loader").fadeIn("slow");
              $(".loaderIcon").fadeIn("slow");
              $('#reportrange span').html(start.format('YYYY-MM-DD') + ' - ' + end.format('YYYY-MM-DD'));
              var st = moment(this.startDate).format("YYYY-MM-DD HH:mm:ss");
              var et = moment(this.endDate).format("YYYY-MM-DD HH:mm:ss");
              //window.location.href = 'clientrep.php?startDate=' + st + "&endDate=" + et;
              window.location = "../pages/netrep.php?startDate=" + st + "&endDate=" + et;
           }
   );
</script>
<!-- AdminLTE for demo purposes -->
<script type="text/javascript">
   function saveRep()
   {
      var csvContent = "data:text/csv;charset=utf-8,";
      var data = [["Country", "Network", "SMSC", "Units Sent", "Cost/Unit"],
<?php
foreach ($networkReport as $key => $value) {
   $value['cdr_record_units'] = number_format($value['cdr_record_units'], 0, '.', '');
   $pos = strpos($value['smsc'], "_");
   $smsc = substr($value['smsc'], 0, $pos);
   $posr = strrpos($value['smsc'], "_");
   $value['smsc'] = substr($value['smsc'], 0, $posr);

   echo '[';
   echo '"' . $value['prefix_country_name'] . '",';
   echo '"' . $value['network'] . '",';
   echo '"' . ucfirst($value['smsc']) . '",';
   echo '"' . $value['cdr_record_units'] . '",';
   echo '"' . ($value['cdr_cost_per_unit'] / 10000) . '"';
   echo '],';
}
?>
      ];
      data.forEach(function (infoArray, index)
      {
         dataString = infoArray.join(",");
         csvContent += index < data.length ? dataString + "\n" : dataString;
      });
      var encodedUri = encodeURI(csvContent);
      var link = document.createElement("a");
      link.setAttribute("href", encodedUri);
      link.setAttribute("download", "<?php echo $startDate . '-' . $endDate; ?>.csv");

      link.click();
   }
</script>

<script src="../js/plugins/datatables/jquery.dataTables.js" type="text/javascript"></script>

<script src="../js/plugins/datatables/dataTables.bootstrap.js" type="text/javascript"></script>


<script type="text/javascript">
   $(function ()
   {

      $('#example2').dataTable({
         "bPaginate": false,
         "bLengthChange": false,
         "bFilter": true,
         "bSort": true,
         "bSortable": false,
         "bInfo": true,
         "bAutoWidth": false,
         "iDisplayLength": 100,
         "aoColumns": [
            null,
            null,
            null,
            {"sType": 'formatted-num', targets: 0},
            {"sType": 'title-numeric', targets: 0}
         ],
         "order": [[3, "desc"]],
      });
   });

   jQuery.extend(jQuery.fn.dataTableExt.oSort,
           {
              "title-numeric-pre": function (a) {
                 var x = a.match(/title="*(-?[0-9\.]+)/)[1];
                 return parseFloat(x);
              },
              "title-numeric-asc": function (a, b) {
                 return ((a < b) ? -1 : ((a > b) ? 1 : 0));
              },
              "title-numeric-desc": function (a, b) {
                 return ((a < b) ? 1 : ((a > b) ? -1 : 0));
              }
           });

   jQuery.extend(jQuery.fn.dataTableExt.oSort,
           {
              "formatted-num-pre": function (a) {
                 a = (a === "-" || a === "") ? 0 : a.replace(/[^\d\-\.]/g, "");
                 return parseFloat(a);
              },
              "formatted-num-asc": function (a, b) {
                 return a - b;
              },
              "formatted-num-desc": function (a, b) {
                 return b - a;
              }
           });
</script>

<script type="text/javascript">
   $(window).load(function () {
      $(".loader").fadeOut("slow");
      $(".loaderIcon").fadeOut("slow");

      var oTable = $('#example2').dataTable();
      oTable.fnSort([[3, 'asc']]);

   })
</script>

<!-- Template Footer -->