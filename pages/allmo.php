<?php
/**
 * Include the header template which sets up the HTML
 *
 * Don't forget to include template_import_script before any Javascripting
 * Don't forget to include template footer.php at the bottom of the page as well
 */
//-------------------------------------------------------------
// Template
//-------------------------------------------------------------
TemplateHelper::setPageTitle('Bulk SMS Traffic');
TemplateHelper::initialize();

$singleView = 'block;';

//-------------------------------------------------------------
// Permissions
//-------------------------------------------------------------
$clients = PermissionsHelper::getAllServicesWithPermissions();

if (isset($_GET['client'])) {

   $split = explode(' ', trim($_GET['client']));
   if (!isset($clients[(integer) end($split)])) {

      header('location: ' . $_SERVER['DOCUMENT_URI']);
      die();
   }
}

if (count($clients) == 1) {

   $_GET['client'] = $_SESSION['accountName'] . ' - ' . $_SESSION['serviceName'] . " - " . $_SESSION['serviceId'];
   $singleView = 'none;';
  
} elseif (!isset($_GET['client'])) {

   $_GET['client'] = $_SESSION['accountName'] . ' - ' . $_SESSION['serviceName'] . " - " . $_SESSION['serviceId'];
}

//-------------------------------------------------------------
// Datepicker
//-------------------------------------------------------------
if (isset($_GET['range'])) {
   $dateArr = explode(' - ', $_GET['range']);
   $startDate = $dateArr[0];
   $endDate = $dateArr[1];
} else {
   //$startDate = (time() - (1 * 24 * 60 * 60))*1000;
   $startDate = (strtotime('today midnight')) * 1000;
   $endDate = (time() * 1000);
   $_GET['range'] = $startDate . ' - ' . $endDate;
}

$startDateConv = date('Y-m-d', ($startDate / 1000));
$endDateConv = date('Y-m-d', ($endDate / 1000));

//-------------------------------------------------------------
// Client
//-------------------------------------------------------------
if (isset($_GET['client']) && $_GET['client'] != 'Please select a client...') {

   $split = explode(' ', trim($_GET['client']));
   $cl = (integer) end($split);

   $dsp = 'inline';
} else {
   $_GET['client'] = "0 - Please select a client...";
   $cl = 0;
   $dsp = 'none';
}

//-------------------------------------------------------------
// Fetch MOs
//-------------------------------------------------------------
if ($cl != '0') {

   $allMos = getAllMos($cl, $startDateConv, $endDateConv);
}
?>

<aside class="right-side">
   <section class="content-header">
      <h1>
         Batch Replies
         <!--small>Control panel</small-->
      </h1>
   </section>

   <section class="content" style="padding-bottom:0px;">
      <div class="box box-solid" style="padding-top:1px;padding-bottom:1px;margin-bottom:2px;margin-top:2px;">
         <form action="" method="get" id="clientList" class="sidebar-form" style="border:0px;">
            <label>Select Date Range</label><br>
            <div id="reportrange" class="select pull-left" onmouseover="" style="cursor: pointer;">
               <i class="fa fa-calendar fa-lg"></i>
               <span style="font-size:15px"><?php echo $startDateConv . " - " . $endDateConv; ?></span><b class="caret"></b>
            </div>
            <br>
            <div class="form-group" style="display:<?php echo $singleView; ?>">
               <br>
               <label>Client List</label>
               <select class="form-control" name="client" id="client" form="clientList" OnChange="reloadOnSelect(this.value);">
                  <?php
                  echo '<option SELECTED>Please select a client...</option>';
                  foreach ($clients as $key => $value) {
                     $sId = $value['service_id'];
                     if ($cl == $sId) {
                        $accountName = $value['account_name'];
                        $serviceName = $value['service_name'];
                        echo "<option name='" . $sId . "' SELECTED>" . $accountName . " - " . $serviceName . " - " . $sId . "</option>";
                     } else {
                        echo "<option name='" . $sId . "'>" . $value['account_name'] . " - " . $value['service_name'] . " - " . $sId . "</option>";
                     }
                  }
                  ?>
               </select>
            </div>
         </form>
      </div>
   </section>

   <section class="content" style="margin-top:0px;">

      <div class="box box-connet" style="padding-bottom:28px; border-top-color:<?php echo $accRGB; ?>;">

         <div class="box-body table-responsive">
            <table id="example2" class="table table-bordered table-striped dataTable table-hover">
               <thead>
                  <tr>
                     <th style="width:200px; vertical-align:middle">Message Data</th>
                     <th style="width:100px; vertical-align:middle">From</th>
                     <th style="width:130px; vertical-align:middle">Client</th>
                     <th style="width:130px; vertical-align:middle">Campaign</th>
                     <th style="vertical-align:middle">Code</th>
                     <th style="vertical-align:middle">Reference</th>
                     <th style="vertical-align:middle">Content&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</th>
                  </tr>
               </thead>
               <tbody>
                  <?php
                  if (isset($allMos) && $allMos != '') {
                     $tots = 0;
                     foreach ($allMos as $key => $value) {
                        $tots++;
                        echo '<tr>';
                        echo '<td style="vertical-align:middle">';
                        echo '<span style="display:none;">' . (0x7FFFFFFF - strtotime($value['Date'])) . '</span>';
                        echo '<span class="text-primary"><strong>Message ID:</strong> ' . $value['Id'] . '</span><br/>';
                        echo '<strong>Batch ID:</strong> ' . $value['batch_id'] . '<br/>';
                        echo '<strong>Date:</strong> ' . $value['Date'] . '<br/>';
                        echo '</td>';
                        echo '<td style="vertical-align:middle">' . $value['From'] . '</td>';
                        echo '<td style="vertical-align:middle">' . $value['Client'] . '</td>';
                        echo '<td style="vertical-align:middle">' . $value['Campaign'] . '</td>';
                        echo '<td style="vertical-align:middle">' . $value['Campaign Code'] . '</td>';
                        echo '<td style="vertical-align:middle"><small>' . $value['Reference'] . '</small></td>';
                        echo '<td style="vertical-align:middle"><span class="text-primary">' . $value['Content'] . '</span></td>';
                        echo '</tr>';
                     }
                  }
                  ?>
               </tbody>
               <!--tfoot>
                   <tr>
                       <th>Batch Id</th>
                       <th>Date</th>
                       <th>Client</th>
                       <th>Campaign</th>
                       <th>Code</th>
                       <th>Reference</th>
                       <th><center>Submitted</center></th>
                       <th><center>Sent</center></th>
                       <th><center>Units</center></th>
                       <th><center>Pending</center></th>
                       <th><center>Delivered</center></th>
                       <th><center>Failed</center></th>
                       <th><center>Rejected</center></th>
                       <th><center>Blacklist</center></th>
                       <th><center>Replies</center></th>
                       <th><center>Status</center></th>
                   </tr>
               </tfoot-->
            </table>
         </div><!-- /.box-body -->
         <div class="box-body">
            <div class="form-group">
                <!--a onclick="saveRep();" class="btn btn-block btn-social btn-dropbox " style="background-color:<?php echo $accRGB; ?>; border: 2px solid; border-radius: 6px !important; float:left; width: 134px; margin-top:-1px;">
                    <i class="fa fa-save"></i>Save Report
                </a-->
               <a href="../php/createCSVAllMO.php?total=<?php echo $tots; ?>&sd=<?php echo $startDateConv; ?>&ed=<?php echo $endDateConv; ?>&sId=<?php echo $cl; ?>" class="btn btn-block btn-social btn-dropbox" style="background-color:<?php echo $accRGB; ?>; border: 2px solid; border-radius: 6px !important; float:left; width: 134px; margin-top:-21px;">
                  <i class="fa fa-save"></i>Save Report
               </a>
            </div>
         </div>
      </div>
   </section>
</aside>

<!--  THE JAVASCRIPT IMPORT ALWAYS APPEARS BEFORE LOCAL JAVASCRIPT AT THE BOTTOM OF THE PAGE -->
<?php include("template_import_script.php"); //must import all scripts first     ?>
<!-- END JAVASCRIPT IMPORT -->

<script type="text/javascript">
   $(function ()
   {
      $('#example2').dataTable({
         "bPaginate": true,
         "bLengthChange": false,
         "bFilter": true,
         "bSort": true,
         "bInfo": true,
         "bAutoWidth": false,
         "iDisplayLength": 100,
         "aoColumns": [
            null, //NIEL {"sType": 'formatted-num', targets: 0},
            null, //NIEL {"sType": 'formatted-num', targets: 0},
            null, //NIEL {"sType": 'formatted-num', targets: 0},
            null, //NIEL {"sType": 'formatted-num', targets: 0},
            null,
            null,
            null
         ],
         "order": [[3, "desc"]]
      });
   });

   jQuery.extend(jQuery.fn.dataTableExt.oSort,
           {
              "title-numeric-pre": function (a) {
                 var x = a.match(/title="*(-?[0-9\.]+)/)[1];
                 return parseFloat(x);
              },
              "title-numeric-asc": function (a, b) {
                 return ((a < b) ? -1 : ((a > b) ? 1 : 0));
              },
              "title-numeric-desc": function (a, b) {
                 return ((a < b) ? 1 : ((a > b) ? -1 : 0));
              }
           });

   jQuery.extend(jQuery.fn.dataTableExt.oSort,
           {
              "formatted-num-pre": function (a) {
                 a = (a === "-" || a === "") ? 0 : a.replace(/[^\d\-\.]/g, "");
                 return parseFloat(a);
              },
              "formatted-num-asc": function (a, b) {
                 return a - b;
              },
              "formatted-num-desc": function (a, b) {
                 return b - a;
              }
           });
</script>

<script type="text/javascript" src="../js/plugins/daterangepicker/moment.js"></script>

<script type="text/javascript" src="../js/plugins/daterangepicker/daterangepickerNetTraffic.js"></script>

<script type="text/javascript">
   $('#reportrange').daterangepicker(
           {
              //$endDate = $dateArr[1];
              //singleDatePicker:true,
              ranges: {
                 'Today': [moment(), moment()],
                 'Yesterday': [moment().subtract('days', 1), moment().subtract('days', 1)],
                 'Last 7 Days': [moment().subtract('days', 6), moment()],
                 'Last 30 Days': [moment().subtract('days', 29), moment()],
                 'This Month': [moment().startOf('month'), moment().endOf('month')],
                 'Last Month': [moment().subtract('month', 1).startOf('month'), moment().subtract('month', 1).endOf('month')]
              },
              startDate: moment(<?php echo $startDate; ?>),
              endDate: moment(<?php echo $endDate; ?>)
           },
           function (start, end) {
              $('#reportrange span').html(start.format('YYYY-MM-DD'));
              $(".loader").fadeIn("slow");
              $(".loaderIcon").fadeIn("slow");

              var serviceName = document.getElementById("client").value;

              //var repRange = $("#reportrange span").html();
              //var repRange1 = $("#reportrange span").html();
              /*var start = moment(repRange1.substr(0, 10)).unix()*1000;
               var end = moment(repRange1.substr(13, 23)).add(1, 'day');//
               alert(repRange1.substr(13, 23));
               end = (end.unix()-1)*1000;*/
              var repRange = start + ' - ' + end;
              //alert(repRange);
              encodeURI(serviceName);
              window.location = "../pages/allmo.php?client=" + encodeURIComponent(serviceName) + "&range=" + repRange;
           }
   );
</script>

<script type="text/javascript">
   function saveRep()
   {
      var csvContent = "data:text/csv;charset=utf-8,";

      var data = [["Batch Id", "Id", "Date", "From", "Client", "Campaign", "Code", "Reference", "Content"],
<?php
if (isset($allMos) && $allMos != '') {
   foreach ($allMos as $key => $value) {
      echo '[';
      echo '"' . $value['batch_id'] . '",';
      echo '"' . $value['Id'] . '",';
      echo '"' . $value['Date'] . '",';
      echo '"' . $value['From'] . '",';
      echo '"' . $value['Client'] . '",';
      echo '"' . $value['Campaign'] . '",';
      echo '"' . $value['Campaign Code'] . '",';
      echo '"' . $value['Reference'] . '",';
      echo '`"' . ($value['Content']) . '"`';
      echo '],';
   }
}
?>
      ];
      data.forEach(function (infoArray, index)
      {
         dataString = infoArray.join(",");
         csvContent += index < data.length ? dataString + "\n" : dataString;
      });
      var encodedUri = encodeURI(csvContent);
      var link = document.createElement("a");
      link.setAttribute("href", encodedUri);
      link.setAttribute("download", "<?php echo 'BulkAllReplies_' . $cl . '_' . $startDate . '-' . $endDate; ?>.csv");

      link.click();
   }

</script>

<script type="text/javascript">

   function reloadOnSelect(name)
   {
      $(".loader").fadeIn("slow");
      $(".loaderIcon").fadeIn("slow");
      //var repRange = $("#reportrange span").daterangepicker( 'getDate' ).html();
      //alert('start:' + start);
      //var end = moment(repRange.substr(13, 23)).unix()*1000;
      var serviceName = name;
      var repRange1 = $("#reportrange span").html();
      var start = moment(repRange1.substr(0, 10)).unix() * 1000;
      var end = moment(repRange1.substr(13, 23)).add(1, 'day');//
      end = (end.unix() - 1) * 1000;
      var repRange = start + ' - ' + end;

      encodeURI(serviceName);
      window.location = "../pages/allmo.php?client=" + encodeURIComponent(serviceName) + "&range=" + repRange;
   }

   /*function viewMsg(type, bId, d, client, camp, code, ref)
    {
    $(".loader").fadeIn("slow");
    $(".loaderIcon").fadeIn("slow");
    
    var repRange1 = $("#reportrange span").html();
    var start = moment(repRange1.substr(0, 10)).unix()*1000;
    var end = moment(repRange1.substr(13, 23)).add(1, 'day');//
    end = (end.unix()-1)*1000;
    var repRange = start + ' - ' + end;
    
    var sId = <?php echo $cl; ?>;
    window.location = "../pages/batchlistmsg.php?client=" + sId + "&type=" + type + "&cli=" + client + "&camp=" + camp + "&code=" + code + "&ref=" + ref + "&bId=" + bId + "&range1=" + repRange1 + "&range=" + repRange + "&sents=" + <?php echo $sents; ?> + "&from=1&controller=1&sort=<?php echo $sort; ?>" + "&rr=" + d;
    }
    
    function viewMsgVar(val, type, bId, d, client, camp, code, ref)
    {
    $(".loader").fadeIn("slow");
    $(".loaderIcon").fadeIn("slow");
    
    var repRange1 = $("#reportrange span").html();
    var start = moment(repRange1.substr(0, 10)).unix()*1000;
    var end = moment(repRange1.substr(13, 23)).add(1, 'day');//
    end = (end.unix()-1)*1000;
    var repRange = start + ' - ' + end;
    
    var sId = <?php echo $cl; ?>;
    window.location = "../pages/batchlistmsg.php?client=" + sId + "&type=" + type + "&cli=" + client + "&camp=" + camp + "&code=" + code + "&ref=" + ref + "&bId=" + bId + "&range1=" + repRange1 + "&range=" + repRange + "&sents=" + <?php echo $sents; ?> + "&from=1&controller=1&sort=<?php echo $sort; ?>&query=" + val + "&rr=" + d;
    }*/
</script>

<script type="text/javascript">
   $(window).load(function () {
      $(".loader").fadeOut("slow");
      $(".loaderIcon").fadeOut("slow");
   })
</script>

<!-- Template Footer -->

