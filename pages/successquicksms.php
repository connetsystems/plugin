<?php
//prevents caching of form data
header("Cache-Control: no-store, no-cache, must-revalidate, max-age=0");
header("Cache-Control: post-check=0, pre-check=0", false);
header("Pragma: no-cache");

/**
 * Include the header template which sets up the HTML
 *
 * Don't forget to include template_import_script before any Javascripting
 * Don't forget to include template footer.php at the bottom of the page as well
 */
$headerPageTitle = "SMS Send"; //set the page title for the template import
TemplateHelper::initialize();

$success = 1;

if (isset($_POST['serviceId'])) {
   //Here we check to see if the post random check number is the same as the one initially set on login.
   //If they are then we allow the sms to go out and we reset the random check num so the sms wont go out on page refresh.
   if ($_POST['smsRandCheck'] == $_SESSION['randomSendCheck']) {
      $_SESSION['randomSendCheck'] = rand(0, 1000);
      /* echo "<br>".$_POST['serviceId'];
        echo "<br>".$_POST['numbers'];
        echo "<br>".trim($_POST['smsText']);
        echo "<br>".$_SESSION['userName']; */

      //$details = validateSMSSend($_POST['serviceId']);
      $details = validateSMSSendNew($_POST['serviceId'], $_SESSION['userId']);

      /* foreach ($details as $key => $value) {
        # code...
        echo '<pre>';
        print_r($value);
        echo '</pre>';
        } */

      //echo '<br>he1----------------------------------------------------------='.count($details);
      if (count($details) == 0) {

         $success = 2;
      } else {
         foreach ($details as $key => $value) {
            $pass = urlencode($value['user_password']);
            $account = urlencode($value['service_login']);
            $userN = urlencode($value['user_username']);
         }
      }

      if ($success != 2) {
         $nCheck = explode(',', $_POST['numbers']);
         if (count($nCheck) > 1) {
            $type = 'batch';
         } else {
            $type = 'single';
         }

         //$_POST['smsText'] = str_replace(' ', '+', $_POST['smsText']);
         $gu = trim(guid(), '{}');
         $gu = str_replace('-', '', $gu);
         $smsUrl = "http://sms.connet-systems.com/submit/" . $type . "/?username=" . $userN . "&password=" . $pass . "&account=" . $account . "&da=" . $_POST['numbers'] . "&ud=" . $_POST['smsText'] . "&id=" . $gu;
         //echo '<br><br><br><br>url='.$smsUrl;
         $statusOfSMS = get_http_response_code($smsUrl);
         if ($statusOfSMS == "401") {
            $success = 2;
         } elseif ($statusOfSMS == "202") {
            $success = 0;
            //$_POST['serviceId'] = 0;
            //unset($_POST['serviceId']);
         } elseif ((strpos($statusOfSMS, '700') !== false) || (strpos($statusOfSMS, 'Accepted') !== false)) {
            $success = 0;
            //$_POST['serviceId'] = 0;
            //unset($_POST['serviceId']);
         } elseif ($statusOfSMS == "200") {
            $success = 0;
            //$_POST['serviceId'] = 0;
            //unset($_POST['serviceId']);
         } else {
            $success = 3;
         }
      }
   } else {
      $success = 4;
   }
   //echo '<br><br><br>=------------------------------------------------Sewrver STATUS:'.$statusOfSMS;
   //echo '<br><br><br>=----------------------------------------STRING:'.$smsUrl;
}

function get_http_response_code($smsUrl) {
   $headers = get_headers($smsUrl);
   //echo '<br>so1='.$headers[0];
   $headers = substr($headers[0], 9, 3);
   //echo '<br>so2='.$headers;

   return $headers;
}
?>
<aside class="right-side">
   <section class="content-header">
      <h1>
         SMS Send
      </h1>
   </section>

   <section class="content">
      <div class="row col-lg-12">
         <div class="box box-warning" style="border-top-color:<?php echo $accRGB; ?>;margin-top:0px;" id="mainBox">
            <div class="box-body" style="padding-bottom:15px;">
               <?php
//echo '<br>status:'.$statusOfSMS;
//echo '<br>check'.strpos($statusOfSMS, '700');
               if ($success == 0) {
                  //echo '<h4>Successfully submitted SMS</h4>';
                  echo '<div class="callout callout-success" style="padding-bottom:5px;margin-bottom:0px;">';
                  echo '<p>SMS sent.</p>';
                  echo '</div>';
                  echo '<div style="padding-bottom:35px;">';
                  echo '<a onclick="gotoViewBatch();" class="btn btn-social btn-dropbox " style="background-color:' . $accRGB . '; border: 2px solid; border-radius: 6px !important; float:left; margin-top:5px;">';
                  echo '<i class="fa fa-book"></i>View SMS Traffic';
                  echo '</a>';
                  echo '</div>';
               }

               if ($success == 1) {
                  echo '<h4>Error 1:</h4>';
                  echo '<div class="callout callout-danger" style="padding-bottom:5px;margin-bottom:0px;">';
                  echo '<p>There is an error submitting your SMS, please try again.</p>';
                  echo '</div>';
               }

               if ($success == 2) {
                  echo '<h4>Error 2:</h4>';
                  echo '<div class="callout callout-danger" style="padding-bottom:5px;margin-bottom:0px;">';
                  echo '<p>You dont have permissions to use this user to send on this account.</p>';
                  echo '</div>';
               }

               if ($success == 3) {
                  echo '<h4>Error 3:</h4>';
                  echo '<div class="callout callout-danger" style="padding-bottom:5px;margin-bottom:0px;">';
                  echo '<p>You do not have enough credits on your account.</p>';
                  echo '</div>';
               }

               if ($success == 4) {
                  echo '<h4>Notice:</h4>';
                  echo '<div class="callout callout-warning" style="padding-bottom:5px;margin-bottom:0px;">';
                  echo '<p>Your message has already been sent.</p>';
                  echo '</div>';
               }
               ?>
            </div>
         </div>
      </div>
      <!--//You dont have permissions to use this user to send on this account!-->
   </section>

</aside>

<!--  THE JAVASCRIPT IMPORT ALWAYS APPEARS BEFORE LOCAL JAVASCRIPT AT THE BOTTOM OF THE PAGE -->
<?php include("template_import_script.php"); //must import all scripts first ?>
<!-- END JAVASCRIPT IMPORT -->

<script type="text/javascript">
   function gotoViewBatch()
   {
      var serviceId = <?php echo $_POST['serviceId']; ?>;
      window.location = "../pages/apitraffic.php?client=" + serviceId;/* + "&range=" + repRange + "&range1=" + repRange1 + "&sents=" + <?php echo $sentCount; ?> + "&from=1&controller=1&sort=<?php echo $sort; ?>&mcc=" + mcc + "&smsc=" + smsc;*/
   }
</script>

<script type="text/javascript">
   $(window).load(function () {
      $(".loader").fadeOut("slow");
      $(".loaderIcon").fadeOut("slow");
   })
</script>

<!-- Template Footer -->

