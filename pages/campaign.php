<?php
/**
 * Include the header template which sets up the HTML
 *
 * Don't forget to include template_import_script before any Javascripting
 * Don't forget to include template footer.php at the bottom of the page as well
 * 
 * 
 * TODO: THERE ARE NO CHECK TO CONFIRM CAMPAIGNS BELONG TO A SERVICE
 * 
 */
//-------------------------------------------------------------
// Template
//-------------------------------------------------------------
TemplateHelper::setPageTitle('Campaigns');
TemplateHelper::initialize();

$singleView = 'block;';

//-------------------------------------------------------------
// Permissions
//-------------------------------------------------------------
$clients = PermissionsHelper::getAllServicesWithPermissions();

if (isset($_GET['client'])) {

   $split = explode(' ', trim($_GET['client']));
   if (!isset($clients[(integer) end($split)])) {
      
      header('location: ' . $_SERVER['DOCUMENT_URI']);
      die();      
   }
}

if (count($clients) == 1) {

   $_GET['client'] = $_SESSION['accountName'] . ' - ' . $_SESSION['serviceName'] . " - " . $_SESSION['serviceId'];
   $singleView = 'none;';

} elseif (!isset($_GET['client'])) {

   $_GET['client'] = $_SESSION['accountName'] . ' - ' . $_SESSION['serviceName'] . " - " . $_SESSION['serviceId'];
} 

////////////////////////////////////////////////
////////////////////////////////////////////////
////////////////////////////////////////////////
$showCamp = "none;";
$showTemp = "none;";
$success = 0;
$success2 = 0;
$success3 = 0;
$success4 = 0;
$success5 = 0;
$success6 = 0;

if (isset($_GET['newClient']) && $_GET['newClient'] == 1)
{
   $_GET['nName'] = trim($_GET['nName']);
   $_GET['nDesc'] = trim($_GET['nDesc']);

   $success = checkNewCampaignClient($_GET['sId'], $_GET['nName'], $_GET['nDesc']);
   if ($success == 0)
   {
      $success = addNewCampaignClient($_GET['sId'], $_GET['uId'], $_GET['nName'], $_GET['nDesc'], $_GET['stat']);
   }
}

if (isset($_GET['newCamp']) && $_GET['newCamp'] == 1) {
   $_GET['nCampName'] = trim($_GET['nCampName']);
   $_GET['nCampCode'] = trim($_GET['nCampCode']);
   $_GET['nCampCode'] = trim($_GET['nCampCode']);
   $_GET['nCampDesc'] = trim($_GET['nCampDesc']);
   $_GET['stat'] = trim($_GET['stat']);
   /* "campaign.php?newCamp=1&cId=" + cId + "&uId=" + uId + "&nCampName=" + nCampName + "&nCampCode=" + nCampCode + "&nCampDesc=" + nCampDesc + "&stat=" + stat + "&client=" + cli; */
   $success2 = checkNewCampaignCamp($_GET['cId'], $_GET['nCampName'], $_GET['nCampCode'], $_GET['nCampDesc'], $_GET['stat']);
   if ($success2 == 0) {
      $success2 = addNewCampaignCamp($_GET['cId'], $_GET['uId'], $_GET['nCampName'], $_GET['nCampCode'], $_GET['nCampDesc'], $_GET['stat']);
   }
   //echo 'SUCCESS2=>'.$success2.'<=';
}

if (isset($_GET['newTemp']) && $_GET['newTemp'] == 1) {
   //"campaign.php?newTemp=1&tId=" + tId + "&uId=" + uId + "&nTempDesc=" + nTempDesc + "&nTempText=" + nTempText + "&stat=" + stat + "&client=" + cli;
   $_GET['nTempDesc'] = trim($_GET['nTempDesc']);
   $_GET['nTempText'] = trim($_GET['nTempText']);
   $_GET['nTempText'] = str_replace("'", "\'", $_GET['nTempText']);
   $_GET['nTempText'] = urldecode($_GET['nTempText']);
   //echo "<br>Saving new template";

   $success3 = addNewTemplate($_GET['tId'], $_GET['uId'], $_GET['nTempDesc'], $_GET['nTempText'], $_GET['stat']);
}

if (isset($_GET['saveEditClient']) && $_GET['saveEditClient'] == 1) {
   $_GET['name'] = trim($_GET['name']);
   $_GET['desc'] = trim($_GET['desc']);

   $success4 = checkNewCampaignClient($_GET['sId'], $_GET['name'], $_GET['desc']);
   if ($success4 == 0) {
      //echo "Editing Client name=".$_GET['name'];
      //echo "<br>Editing Client desc=".$_GET['desc'];
      $success4 = updateCampaignClient($_GET['ccId'], $_GET['name'], $_GET['desc']);
   }
}

if (isset($_GET['saveEditCampaign']) && $_GET['saveEditCampaign'] == 1) {
   $_GET['name'] = trim($_GET['name']);
   $_GET['desc'] = trim($_GET['desc']);
   $_GET['code'] = trim($_GET['code']);

   $success5 = checkNewCampaignCamp($_GET['cId'], $_GET['name'], $_GET['desc'], $_GET['code'], $_GET['status']);
   if ($success5 == 0) {
      $success5 = updateCampaignCampaign($_GET['caId'], $_GET['name'], $_GET['desc'], $_GET['code'], $_GET['status']);
   }
}

if (isset($_GET['saveEditTemplate']) && $_GET['saveEditTemplate'] == 1)
{
   $_GET['text'] = trim($_GET['text']);
   //$_GET['text'] = str_replace("&", "&amp", $_GET['text']);
   //$_GET['text'] = urlencode($_GET['text']);
   $_GET['desc'] = trim($_GET['desc']);

   $success6 = updateCampaignTemplate($_GET['teId'], $_GET['text'], $_GET['desc'], $_GET['status']);
}
 
if (isset($_GET['client'])) {

   $split = explode(' ', trim($_GET['client']));
   $cl = (integer) end($split);
   
   $CCData = getCampaignClient($cl);

   foreach ($CCData as $key => $value) {
      /* echo '<pre>';
        print_r($value);
        echo '</pre>'; */
      $sN = $value['service_name'];
   }
   $dsp = 'block;';
   $dir = '../img/routes';
   //$serProviders = scandir($dir);
} else {
   $_GET['client'] = "0 - Please select a client...";
   $cl = strstr($_GET['client'], ' ', true);
   $dsp = 'none;';
}

$colorCC = '';
$colorCA = '';

if (isset($_GET['cId'])) {
   $CAData = getCampaignInfo($_GET['cId']);
   $showCamp = "block;";
}

if (isset($_GET['tId'])) {
   $TMData = getCampaignTemp($_GET['tId']);
   $showTemp = "block;";
}

function htmlallentities($str) {
   $res = '';
   $strlen = strlen($str);
   for ($i = 0; $i < $strlen; $i++) {
      $byte = ord($str[$i]);
      if ($byte < 128) // 1-byte char
         $res .= $str[$i];
      elseif ($byte < 192)
         ; // invalid utf8
      elseif ($byte < 224) // 2-byte char
         $res .= '&#' . ((63 & $byte) * 64 + (63 & ord($str[++$i]))) . ';';
      elseif ($byte < 240) // 3-byte char
         $res .= '&#' . ((15 & $byte) * 4096 + (63 & ord($str[++$i])) * 64 + (63 & ord($str[++$i]))) . ';';
      elseif ($byte < 248) // 4-byte char
         $res .= '&#' . ((15 & $byte) * 262144 + (63 & ord($str[++$i])) * 4096 + (63 & ord($str[++$i])) * 64 + (63 & ord($str[++$i]))) . ';';
   }
   return $res;
}

////////////////////////////////////////////////
////////////////////////////////////////////////
////////////////////////////////////////////////
?>


<style>
   textarea{
      resize:none;
      overflow:hidden;
      min-height:100px;
      max-height:200px;
   }
</style>
<aside class="right-side">
   <section class="content-header">
      <h1>
         Campaigns
         <!--small>Control panel</small-->
      </h1>
   </section>

   <section class="content">
      <div class="row col-lg-12">
         <div class="box box-solid" style="height:66px; margin-top:-15px;display:<?php echo $singleView; ?>">
            <form action="updateservice.php" method="get" id="clientList" class="sidebar-form" style="border:0px;">
               <div class="form-group">
                  <label>Service List</label>
                  <select class="form-control" name="client" form="clientList" OnChange="reloadOnSelect(this.value);">
                     <?php
                     if ($cl == '0') {
                        echo '<option SELECTED>Please select a client...</option>';
                     }
                     foreach ($clients as $key => $value) {
                        $sId = $value['service_id'];
                        if ($cl == $sId) {
                           $accountName = $value['account_name'];
                           $serviceName = $value['service_name'];
                           echo "<option name='" . $sId . "' SELECTED>" . $accountName . " - " . $serviceName . " - " . $sId . "</option>";
                        } else {
                           echo "<option name='" . $sId . "'>" . $value['account_name'] . " - " . $value['service_name'] . " - " . $sId . "</option>";
                        }
                     }
                     ?>
                  </select>
               </div>
               <br>
            </form>
         </div>
         <div class="box box-warning" style="border-top-color:<?php echo $accRGB; ?>;display:<?php echo $dsp; ?>">
            <div class="box-header">
               <h4 class="box-title">
                  Clients
               </h4>
            </div>
            <div class="box-body">
               <?php
               if ($success === -1) {
                  echo '<div class="callout callout-danger">';
                  echo '<h4 style="padding-top:12px;">Client Name Exists!</h4>';
                  echo '<p>The client name that you specified already exists.</p>';
                  echo '</div>';
               }

               if ($success == '1') {
                  echo '<div class="callout callout-success">';
                  echo '<h4 style="padding-top:12px;">Client Added!</h4>';
                  //echo '<p>Succesfully added a new client.</p>';
                  echo '</div>';
               }

               if ($success4 == '1') {
                  echo '<div class="callout callout-success">';
                  echo '<h4 style="padding-top:12px;">Client Edited!</h4>';
                  //echo '<p>Succesfully edited a client.</p>';
                  echo '</div>';
               }
               ?>
               <a class="btn btn-block btn-social btn-dropbox" name="'.$value['campaign_id'].'" onclick="newT('newClient');" style="background-color:<?php echo $accRGB; ?>; color: #ffffff; border: 2px solid; border-radius: 6px !important; font-size:0.8em !important; height:24px; width: 135px; margin-top:0px; padding-top:3px;">
                  <i class="fa fa-plus" style="font-size:1.0em !important; color:#ffffff; margin-top:-6px;"></i>Add New Client
               </a>
               <div class="box-body table-responsive" style="display:none;" id="newClient">
                  <table class="table table-bordered table-striped dataTable table-hover">
                     <thead>
                        <tr>
                           <th>Service</th>
                           <th>Client Name</th>
                           <th>Client Description</th>
                           <th><center>Save</center></th>
                     </tr>
                     </thead>
                     <tbody>
                        <tr>
                           <td><?php echo $sN; ?></td>
                           <td ><input type="text" style="height:24px;" class="form-control" id="newClientName" placeholder="Name"></td>
                           <td><input type="text" style="height:24px;" class="form-control" id="newClientDesc" placeholder="Description"></td>
                           <td>
                     <center>
                        <a class="btn btn-block btn-social btn-dropbox" name="saveNewClient" onclick="saveNewClient()" style="background-color:<?php echo $accRGB; ?>; color: #ffffff; border: 2px solid; border-radius: 6px !important; font-size:0.8em !important; height:24px; width: 85px; margin-top:0px; padding-top:3px;">
                           <i class="fa fa-save" style="font-size:1.0em !important; color:#ffffff; margin-top:-6px;"></i>Save
                        </a>
                     </center>
                     </td>
                     </tr>
                     </tbody>
                  </table>
               </div>
               <div class="box-body table-responsive">
                  <table id="example2" class="table table-bordered table-striped dataTable table-hover">
                     <thead>
                        <tr>
                           <th>Service</th>
                           <th>Client Name</th>
                           <th>Client Description</th>
                           <th>Campaigns</th>
                           <th><center>View/Update</center></th>
                     <th><center>Edit</center></th>
                     </tr>
                     </thead>
                     <tbody>

                        <?php
                        if (isset($CCData) && isset($CCData) != '') {
                           foreach ($CCData as $key => $value) {
                              if (isset($_GET['cId'])) {
                                 if ($_GET['cId'] == $value['client_id']) {
                                    $colorCC = 'background-color:#888888';
                                 } else {
                                    $colorCC = '';
                                 }
                              }
                              echo '<tr id="cctr' . $value['client_id'] . '">';
                              echo '<td style="' . $colorCC . '">' . $value['service_name'] . '</td>';
                              echo '<td style="' . $colorCC . '"><input type="text" id="clNa' . $value['client_id'] . '" style="border:0px;background-color:transparent;width:240px;" readonly="readonly" value="' . $value['client_name'] . '"></td>';
                              echo '<td style="' . $colorCC . '"><input type="text" id="clDe' . $value['client_id'] . '" style="border:0px;background-color:transparent;width:360px;" readonly="readonly" value="' . $value['client_description'] . '"></td>';
                              echo '<td style="' . $colorCC . '">' . $value['campaign total'] . '</td>';
                              echo '<td style="' . $colorCC . '">
                                                        <center>
                                                            <a class="btn btn-block btn-social btn-dropbox" name="' . $value['client_id'] . '" onclick="showCampaign(' . $value['client_id'] . ')" style="background-color:' . $accRGB . '; color: #ffffff; border: 2px solid; border-radius: 6px !important; font-size:0.8em !important; height:24px; width: 121px; margin-top:0px; padding-top:3px;">
                                                                <i class="fa fa-binoculars" style="font-size:1.0em !important; color:#ffffff; margin-top:-6px;"></i>View/Update
                                                            </a>
                                                        </center>
                                                      </td>';

                              if ($value['client_name'] == 'Default') {
                                 echo '<td style="vertical-align:middle;text-align:center;' . $colorCA . '">Cannot Edit Default</td>';
                              } else {
                                 echo '<td style="' . $colorCC . '">
                                                            <center>
                                                                <a class="btn btn-block btn-social btn-dropbox" id="edit' . $value['client_id'] . '" onclick="editClient(' . $value['client_id'] . ')" style="background-color:' . $accRGB . '; color: #ffffff; border: 2px solid; border-radius: 6px !important; font-size:0.8em !important; height:24px; width: 85px; margin-top:0px; padding-top:3px;">
                                                                    <i class="fa fa-pencil" style="font-size:1.0em !important;color:#ffffff; margin-top:-6px;"></i>Edit
                                                                </a>
                                                                <a class="btn btn-block btn-social btn-dropbox" id="save' . $value['client_id'] . '" onclick="saveEditClient(' . $value['client_id'] . ')" style="display:none; background-color:' . $accRGB . '; color: #ffffff; border: 2px solid; border-radius: 6px !important; font-size:0.8em !important; height:24px; width: 85px; margin-top:0px; padding-top:3px;">
                                                                    <i class="fa fa-save" style="font-size:1.0em !important;color:#ffffff; margin-top:-6px;"></i>Save
                                                                </a>
                                                            </center>
                                                          </td>';
                              }
                              echo '</tr>';
                           }
                        }
                        ?>
                     </tbody>
                  </table>
               </div>
            </div>
         </div>
         <div class="box box-warning" style="border-top-color:<?php echo $accRGB; ?>;display:<?php echo $showCamp; ?>">
            <div class="box-header">
               <h4 class="box-title">
                  Campaigns
               </h4>
            </div>
            <div class="box-body">
               <?php
               if ($success2 === -1) {
                  echo '<div class="callout callout-danger">';
                  echo '<h4 style="padding-top:12px;">Campaign Name Exists!</h4>';
                  echo '<p>The campaign name that you specified already exists.</p>';
                  echo '</div>';
               }

               if ($success2 == '1') {
                  echo '<div class="callout callout-success">';
                  echo '<h4 style="padding-top:12px;">Campaign Added!</h4>';
                  //echo '<p>Succesfully added a new campaign.</p>';
                  echo '</div>';
               }

               if ($success5 == '1') {
                  echo '<div class="callout callout-success">';
                  echo '<h4 style="padding-top:12px;">Campaign Edited!</h4>';
                  //echo '<p>Succesfully edited a campaign.</p>';
                  echo '</div>';
               }
               ?>
               <a class="btn btn-block btn-social btn-dropbox" name="'.$value['campaign_id'].'" onclick="newT('newCampaign');" style="background-color:<?php echo $accRGB; ?>; color: #ffffff; border: 2px solid; border-radius: 6px !important; font-size:0.8em !important; height:24px; width: 155px; margin-top:0px; padding-top:3px;">
                  <i class="fa fa-plus" style="font-size:1.0em !important; color:#ffffff; margin-top:-6px;"></i>Add New Campaign
               </a>
               <div class="box-body table-responsive" style="display:none;" id="newCampaign">
                  <table class="table table-bordered table-striped dataTable table-hover">
                     <thead>
                        <tr>
                           <th>Name</th>
                           <th style="width:80px;">Code</th>
                           <th>Description</th>
                           <th><center>Save</center></th>
                     </tr>
                     </thead>
                     <tbody>
                        <tr>
                           <td><input type="text" style="height:24px;" class="form-control" id="newCampName" placeholder="Name"></td>
                           <td><input type="text" style="height:24px;" class="form-control" id="newCampCode" placeholder="Code"></td>
                           <td><input type="text" style="height:24px;" class="form-control" id="newCampDesc" placeholder="Description"></td>
                           <td>
                     <center>
                        <a class="btn btn-block btn-social btn-dropbox" onclick="saveNewCampaign(<?php
                        if (isset($_GET['cId'])) {
                           echo $_GET['cId'];
                        }
                        ?>)" style="background-color:<?php echo $accRGB; ?>; color: #ffffff; border: 2px solid; border-radius: 6px !important; font-size:0.8em !important; height:24px; width: 85px; margin-top:0px; padding-top:3px;">
                           <i class="fa fa-save" style="font-size:1.0em !important; color:#ffffff; margin-top:-6px;"></i>Save
                        </a>
                     </center>
                     </td>
                     </tr>
                     </tbody>
                  </table>
               </div>
               <div class="box-body table-responsive" id="camps" style="display:<?php echo $showCamp; ?>">
                  <table id="example2" class="table table-bordered table-striped dataTable table-hover">
                     <thead>
                        <tr>
                           <th>Service</th>
                           <th>Client Name</th>
                           <th>Campaign Name</th>
                           <th style="width:80px;">Code</th>
                           <th>Description</th>
                           <th>Status</th>
                           <th>Templates</th>
                           <th><center>View/Update</center></th>
                     <th><center>Edit</center></th>
                     </tr>
                     </thead>
                     <tbody>
                        <?php
                        /* campaign_id, client_id, service_name, c.user_id, campaign_name, campaign_code, campaign_description, campaign_status */
                        if (isset($CAData) && isset($CAData) != '') {
                           foreach ($CAData as $key => $value) {
                              if (isset($_GET['tId'])) {
                                 if ($_GET['tId'] == $value['campaign_id']) {
                                    $colorCA = 'background-color:#888888;';
                                 } else {
                                    $colorCA = '';
                                 }
                              }
                              echo '<tr id="tr' . $value['campaign_id'] . '">';
                              echo '<td style="vertical-align:middle;' . $colorCA . '">' . $value['service_name'] . '</td>';
                              echo '<td style="vertical-align:middle;' . $colorCA . '">' . $value['client_name'] . '</td>';
                              echo '<td style="vertical-align:middle;' . $colorCA . '"><input type="text" id="caNa' . $value['campaign_id'] . '" style="border:0px;background-color:transparent;width:200px;" readonly="readonly" value="' . $value['campaign_name'] . '"></td>';
                              echo '<td style="vertical-align:middle;' . $colorCA . '"><input type="text" id="caCo' . $value['campaign_id'] . '" style="border:0px;background-color:transparent;width:70px;" readonly="readonly" value="' . $value['campaign_code'] . '"></td>';
                              echo '<td style="vertical-align:middle;' . $colorCA . '"><input type="text" id="caDe' . $value['campaign_id'] . '" style="border:0px;background-color:transparent;width:300px;" readonly="readonly" value="' . $value['campaign_description'] . '"></td>';

                              echo '<td style="' . $colorCA . '">';
                              echo '<select class="form-control" disabled id="caSt' . $value['campaign_id'] . '">';
                              echo "<option SELECTED>" . $value['campaign_status'] . "</option>";
                              if ($value['campaign_status'] == "ENABLED") {
                                 echo "<option>DISABLED</option>";
                              } else {
                                 echo "<option>ENABLED</option>";
                              }
                              echo '</select>';
                              echo '</td>';

                              echo '<td style="vertical-align:middle;' . $colorCA . '">' . $value['template total'] . '</td>';


                              echo '<td style="vertical-align:middle;' . $colorCA . '">
                                                            <center>
                                                                <a class="btn btn-block btn-social btn-dropbox" name="' . $value['campaign_id'] . '" onclick="showTemplate(' . $value['campaign_id'] . ')" style="background-color:' . $accRGB . '; color: #ffffff; border: 2px solid; border-radius: 6px !important; font-size:0.8em !important; height:24px; width: 121px; margin-top:0px; padding-top:3px;">
                                                                    <i class="fa fa-binoculars" style="font-size:1.0em !important; color:#ffffff; margin-top:-6px;"></i>View/Update
                                                                </a>
                                                            </center>
                                                          </td>';
                              if ($value['client_name'] == 'Default' && $value['campaign_name'] == 'Default') {
                                 echo '<td style="vertical-align:middle;text-align:center;' . $colorCA . '">Cannot Edit Default</td>';
                              } else {
                                 echo '<td style="vertical-align:middle;' . $colorCA . '">
                                                        <center>
                                                            <a class="btn btn-block btn-social btn-dropbox" id="editC' . $value['campaign_id'] . '" onclick="editCampaign(' . $value['campaign_id'] . ')" style="display:block; background-color:' . $accRGB . '; color: #ffffff; border: 2px solid; border-radius: 6px !important; font-size:0.8em !important; height:24px; width: 85px; margin-top:0px; padding-top:3px;">
                                                                <i class="fa fa-pencil" style="font-size:1.0em !important;color:#ffffff; margin-top:-6px;"></i>Edit
                                                            </a>
                                                            <a class="btn btn-block btn-social btn-dropbox" id="saveC' . $value['campaign_id'] . '" onclick="saveEditCampaign(' . $value['campaign_id'] . ')" style="display:none; background-color:' . $accRGB . '; color: #ffffff; border: 2px solid; border-radius: 6px !important; font-size:0.8em !important; height:24px; width: 85px; margin-top:0px; padding-top:3px;">
                                                                <i class="fa fa-save" style="font-size:1.0em !important;color:#ffffff; margin-top:-6px;"></i>Save
                                                            </a>
                                                        </center>
                                                      </td>';
                              }
                              echo '</tr>';
                           }
                        }
                        ?>
                     </tbody>
                  </table>
               </div>
            </div>
         </div>
         <div class="box box-warning" style="border-top-color:<?php echo $accRGB; ?>;display:<?php echo $showTemp; ?>">
            <div class="box-header">
               <h4 class="box-title">
                  Templates
               </h4>
            </div>
            <div class="box-body">
               <?php
               if ($success3 == '1') {
                  echo '<div class="callout callout-success">';
                  echo '<h4 style="padding-top:12px;">Template Added!</h4>';
                  //echo '<p>Succesfully added a new template.</p>';
                  echo '</div>';
               }

               if ($success6 == '1') {
                  echo '<div class="callout callout-success">';
                  echo '<h4 style="padding-top:12px;">Template Edited!</h4>';
                  //echo '<p>Succesfully edited a new template.</p>';
                  echo '</div>';
               }
               ?>
               <a class="btn btn-block btn-social btn-dropbox" name="'.$value['campaign_id'].'" onclick="newT('newTemplate');" style="background-color:<?php echo $accRGB; ?>; color: #ffffff; border: 2px solid; border-radius: 6px !important; font-size:0.8em !important; height:24px; width: 150px; margin-top:0px; padding-top:3px;">
                  <i class="fa fa-plus" style="font-size:1.0em !important; color:#ffffff; margin-top:-6px;"></i>Add New Template
               </a>
               <div class="box-body table-responsive" style="display:none;" id="newTemplate">
                  <table class="table table-bordered table-striped dataTable table-hover">
                     <thead>
                        <tr>
                           <th style="width:200px;">Description</th>
                           <th>Text</th>
                           <th style="width:200px;">Counter</th>
                           <th style="width:150px;"><center>Save</center></th>
                     </tr>
                     </thead>
                     <tbody>
                        <tr>
                           <td><input type="text" style="height:24px;" class="form-control" id="newTempDesc" placeholder="Description"></td>
                           <td><input type="text" style="height:24px;" class="form-control" id="smsText" placeholder="Text"></td>
                           <td id="counter"></td>
                           <td>
                     <center>
                        <a class="btn btn-block btn-social btn-dropbox" name="'.$value['client_id'].'" onclick="saveNewTemplate(<?php
               if (isset($_GET['tId'])) {
                  echo $_GET['tId'];
               }
               ?>)" style="background-color:<?php echo $accRGB; ?>; color: #ffffff; border: 2px solid; border-radius: 6px !important; font-size:0.8em !important; height:24px; width: 85px; margin-top:0px; padding-top:3px;">
                           <i class="fa fa-save" style="font-size:1.0em !important; color:#ffffff; margin-top:-6px;"></i>Save
                        </a>
                     </center>
                     </td>
                     </tr>
                     </tbody>
                  </table>
               </div>
               <div class="box-body table-responsive" id="temps" style="display:<?php echo $showTemp; ?>;">
                  <table id="example2" class="table table-bordered table-striped dataTable table-hover">
                     <thead>
                        <tr>
                           <th width="250">Description</th>
                           <th width="480">Template</th>
                           <th>Counter</th>
                           <th>Status</th>
                           <th>Edit</th>
                        </tr>
                     </thead>
                     <tbody>
                        <?php
                        /* template_id, user_id, template_content, template_description, template_status, template_optout_content */
                        if (isset($TMData) && isset($TMData) != '') {
                           foreach ($TMData as $key => $value) {
                              // characters, 1 SMS's
                              //get the SMS length and units
                              $smsLength = helperSMSGeneral::getSMSCharacterCount($value['template_content']);
                              $units = helperSMSGeneral::getSMSUnits($smsLength);

                              $len = $smsLength . ' characters, ' . $units . " SMS";

                              echo '<tr>';
                              echo '<td><div class="form-group" style="max-width:250px;">';
                              echo '<p id="teDe' . $value['template_id'] . '_text" style="white-space:pre-wrap;">' . htmlentities($value['template_description']) . '</p>';
                              echo '<textarea id="teDe' . $value['template_id'] . '" class="form-control" style="display:none;" maxlength="80">' . $value['template_description'] . '</textarea>';
                              echo '<small id="teDe' . $value['template_id'] . '_error" class="text-danger" style="display:none;">This field cannot be blank.</small>';
                              echo '</div></td>';
                              echo '<td><div class="form-group" style="max-width:480px;">';
                              echo '<p id="teTe' . $value['template_id'] . '_text" style="white-space:pre-wrap;">' . htmlentities($value['template_content']) . '</p>';
                              echo '<textarea id="teTe' . $value['template_id'] . '" class="form-control" onkeyup="countIt(' . $value['template_id'] . ')" value="' . $value['template_content'] . '" style="display:none;">' . htmlentities($value['template_content']) . '</textarea>';
                              echo '<small id="teTe' . $value['template_id'] . '_error" class="text-danger" style="display:none;">This field cannot be blank.</small>';
                              echo '</div></td>';
                              echo '<td id="count' . $value['template_id'] . '">' . $len . '</td>';

                              echo '<td>';

                              echo '<p id="teSt' . $value['template_id'] . '_text">' . $value['template_status'] . "</p>";

                              echo '<select class="form-control" id="teSt' . $value['template_id'] . '" style="display:none;">';
                              echo "<option SELECTED>" . $value['template_status'] . "</option>";
                              if ($value['template_status'] == "ENABLED") {
                                 echo "<option>DISABLED</option>";
                              } else {
                                 echo "<option>ENABLED</option>";
                              }
                              echo '</select>';
                              echo '</td>';

                              echo '<td>
                                   <center>
                                       <a class="btn btn-block btn-social btn-dropbox" id="editT' . $value['template_id'] . '" onclick="editTemplate(' . $value['template_id'] . ')" style="display:block;background-color:' . $accRGB . '; color: #ffffff; border: 2px solid; border-radius: 6px !important; font-size:0.8em !important; height:24px; width: 85px; margin-top:0px; padding-top:3px;">
                                           <i class="fa fa-pencil" style="font-size:1.0em !important; margin-top:-6px;"></i>Edit
                                       </a>
                                       <a class="btn btn-block btn-social btn-dropbox" id="saveT' . $value['template_id'] . '" onclick="saveEditTemplate(' . $value['template_id'] . ')" style="display:none;background-color:' . $accRGB . '; color: #ffffff; border: 2px solid; border-radius: 6px !important; font-size:0.8em !important; height:24px; width: 85px; margin-top:0px; padding-top:3px;">
                                           <i class="fa fa-save" style="font-size:1.0em !important; margin-top:-6px;"></i>Save
                                       </a>
                                   </center>
                                 </td>';
                              echo '</tr>';
                           }
                        }
                        ?>
                     </tbody>
                  </table>
               </div>
            </div>
         </div>
      </div>
   </section>
</aside>

<!--  THE JAVASCRIPT IMPORT ALWAYS APPEARS BEFORE LOCAL JAVASCRIPT AT THE BOTTOM OF THE PAGE -->
<?php include("template_import_script.php"); //must import all scripts first  ?>
<!-- END JAVASCRIPT IMPORT -->

<script type="text/javascript">
   var area = document.getElementById("smsText");
   var message = document.getElementById("counter");


</script>
<script type="text/javascript">


   function countIt(v)
   {
      //validation
      var thisText = $("#teTe" + v).val();

      //replacing curly quotes with straight quotes
      var fixText = thisText
              .replace(/[\u2018\u2019]/g, "'")
              .replace(/[\u201C\u201D]/g, '"')
              .replace("{ }", "( )")
              .replace("{}", "()")
              .replace(/\u2013|\u2014/g, "-");  //long dash
      //.replace(/\u20ac/g, 'E'); //euro sign

      $("#teTe" + v).val(fixText);

      var teTeBox = 'teTe' + v;
      var countTxt = 'count' + v;

      var areaT = document.getElementById(teTeBox);
      var messageT = document.getElementById(countTxt);

      if (areaT.value.length == 0)
      {
         messageT.innerHTML = "0 characters, 0 SMS";
      }
      else
      {
         var tot = 0;

         var str = areaT.value;
         var newLength = areaT.value.length;
         console.log("Curr length: " + newLength);
         var count = occurrences(str, '[', 'false');
         if (count != 0)
         {
            newLength = newLength + (2 * count) - count;
         }
         count = occurrences(str, ']', 'false');
         if (count != 0)
         {
            newLength = newLength + (2 * count) - count;
         }
         count = occurrences(str, '^', 'false');
         if (count != 0)
         {
            newLength = newLength + (2 * count) - count;
         }
         count = occurrences(str, '|', 'false');
         if (count != 0)
         {
            newLength = newLength + (2 * count) - count;
         }
         count = occurrences(str, '~', 'false');
         if (count != 0)
         {
            newLength = newLength + (2 * count) - count;
         }
         count = occurrences(str, '\\', 'false');
         if (count != 0)
         {
            newLength = newLength + (2 * count) - count;
         }
         count = occurrences(str, '\u20AC', 'false');
         if (count != 0)
         {
            newLength = newLength + (2 * count) - count;
         }

         count = occurrences(str, '\u21A1', 'false');
         if (count != 0)
         {
            newLength = newLength + (2 * count) - count;
         }
         count = occurrences(str, '{', 'false');
         if (count != 0)
         {
            newLength = newLength + (2 * count) - count;
         }
         count = occurrences(str, '}', 'false');
         if (count != 0)
         {
            newLength = newLength + (2 * count) - count;
         }

         if (newLength <= 160)
         {
            tot = Math.ceil((newLength) / 160);
         } else
         {
            tot = Math.ceil((newLength) / 153);
         }

         messageT.innerHTML = newLength + " characters, " + tot + " SMS";
      }

   }

   function occurrences(string, subString, allowOverlapping)
   {
      string += "";
      subString += "";
      if (subString.length <= 0)
         return string.length + 1;

      var n = 0, pos = 0;
      var step = (allowOverlapping) ? (1) : (subString.length);

      while (true)
      {
         pos = string.indexOf(subString, pos);
         if (pos >= 0) {
            n++;
            pos += step;
         } else
            break;
      }
      return(n);
   }

   function editClient(v)
   {
      //alert(v);
      var clNa = 'clNa' + v;
      var clDe = 'clDe' + v;
      var clEd = 'edit' + v;
      var clSa = 'save' + v;

      var name = document.getElementById(clNa);
      name.style.border = "thin solid #aaaaaa";
      name.style.background = '#ffffff';
      name.removeAttribute('readonly');

      var desc = document.getElementById(clDe);
      desc.style.border = "thin solid #aaaaaa";
      desc.style.background = '#ffffff';
      desc.removeAttribute('readonly');

      var editBtn = document.getElementById(clEd);
      editBtn.style.display = 'none';

      var saveBtn = document.getElementById(clSa);
      saveBtn.style.display = 'block';
   }

   function saveEditClient(v)
   {
      var clNa = 'clNa' + v;
      var clDe = 'clDe' + v;
      var sId = "<?php
if (isset($cl)) {
   echo $cl;
}
?>";
      var cli = "<?php
if (isset($_GET['client'])) {
   echo $_GET['client'];
}
?>";
      var name = document.getElementById(clNa).value;
      var desc = document.getElementById(clDe).value;

      window.location = "campaign.php?saveEditClient=1&ccId=" + v + "&name=" + name + "&desc=" + desc + "&client=" + cli + "&sId=" + sId;
   }

   function editCampaign(v)
   {
      var caNa = 'caNa' + v;
      var caDe = 'caDe' + v;
      var caCo = 'caCo' + v;
      var caSt = 'caSt' + v;
      var caEd = 'editC' + v;
      var caSa = 'saveC' + v;

      var name = document.getElementById(caNa);
      name.style.border = "thin solid #aaaaaa";
      name.style.background = '#ffffff';
      name.removeAttribute('readonly');

      var desc = document.getElementById(caDe);
      desc.style.border = "thin solid #aaaaaa";
      desc.style.background = '#ffffff';
      desc.removeAttribute('readonly');

      var code = document.getElementById(caCo);
      code.style.border = "thin solid #aaaaaa";
      code.style.background = '#ffffff';
      code.removeAttribute('readonly');

      var stat = document.getElementById(caSt);
      stat.disabled = false;
      //stat.style.border = "thin solid #aaaaaa";
      //stat.style.background = '#ffffff';
      //stat.removeAttribute('readonly');


      var editBtn = document.getElementById(caEd);
      editBtn.style.display = 'none';

      var saveBtn = document.getElementById(caSa);
      saveBtn.style.display = 'block';
   }

   function saveEditCampaign(v)
   {
      //alert(v);
      var caNa = 'caNa' + v;
      var caDe = 'caDe' + v;
      var caCo = 'caCo' + v;
      var caSt = 'caSt' + v;
      var cli = "<?php
if (isset($_GET['client'])) {
   echo $_GET['client'];
}
?>";
      var cId = "<?php
if (isset($_GET['cId'])) {
   echo $_GET['cId'];
}
?>";
      var name = document.getElementById(caNa).value;
      var desc = document.getElementById(caDe).value;
      var code = document.getElementById(caCo).value;
      var stat = document.getElementById(caSt).value;

      window.location = "campaign.php?saveEditCampaign=1&caId=" + v + "&name=" + name + "&desc=" + desc + "&code=" + code + "&client=" + cli + "&cId=" + cId + "&status=" + stat;
   }

   function editTemplate(v)
   {
      countIt(v);

      var teDe = 'teDe' + v;
      var teTe = 'teTe' + v;
      var teEd = 'editT' + v;
      var teSa = 'saveT' + v;
      var teSt = 'teSt' + v;

      $('#' + teDe).show();
      $('#' + teDe + '_text').hide();

      $('#' + teTe).show();
      $('#' + teTe + '_text').hide();

      $('#' + teSt).show();
      $('#' + teSt + '_text').hide();

      var editBtn = document.getElementById(teEd);
      editBtn.style.display = 'none';

      var saveBtn = document.getElementById(teSa);
      saveBtn.style.display = 'block';
   }

   function saveEditTemplate(v)
   {

      countIt(v);

      var teDe = 'teDe' + v;
      var teTe = 'teTe' + v;
      var teSt = 'teSt' + v;

      var cli = "<?php
if (isset($_GET['client'])) {
   echo $_GET['client'];
}
?>";
      var cId = "<?php
if (isset($_GET['cId'])) {
   echo $_GET['cId'];
}
?>";
      var tId = "<?php
if (isset($_GET['tId'])) {
   echo $_GET['tId'];
}
?>";
      var desc = encodeURIComponent(document.getElementById(teDe).value);
      var text = encodeURIComponent(document.getElementById(teTe).value);
      var estat = document.getElementById(teSt).value;

      if (desc == "")
      {
         $('#' + teDe).parent().addClass('has-error');
         $('#' + teDe + '_error').show();
      } else if (text == "")
      {
         $('#' + teTe).parent().addClass('has-error');
         $('#' + teTe + '_error').show();
      } else
      {
         $('#' + teDe).parent().removeClass('has-error');
         $('#' + teDe + '_error').hide();
         $('#' + teTe).parent().removeClass('has-error');
         $('#' + teTe + '_error').hide();
         window.location = "campaign.php?saveEditTemplate=1&teId=" + v + "&text=" + text + "&desc=" + desc + "&client=" + cli + "&cId=" + cId + "&tId=" + tId + "&status=" + estat;
      }

   }

   function saveNewClient()
   {
      var sId = "<?php
if (isset($cl)) {
   echo $cl;
}
?>";
      var uId = "<?php echo $_SESSION['userId']; ?>";
      var nName = encodeURIComponent(document.getElementById('newClientName').value);
      var nDesc = encodeURIComponent(document.getElementById('newClientDesc').value);
      var stat = 'ENABLED';
      var cli = "<?php
if (isset($_GET['client'])) {
   echo $_GET['client'];
}
?>";

      window.location = "campaign.php?newClient=1&sId=" + sId + "&uId=" + uId + "&nName=" + nName + "&nDesc=" + nDesc + "&stat=" + stat + "&client=" + cli;
   }

   function saveNewCampaign(client)
   {
      var cId = client;
      var uId = "<?php echo $_SESSION['userId']; ?>";
      var nCampName = encodeURIComponent(document.getElementById('newCampName').value);
      var nCampCode = encodeURIComponent(document.getElementById('newCampCode').value);
      var nCampDesc = encodeURIComponent(document.getElementById('newCampDesc').value);
      var stat = 'ENABLED';
      var cli = "<?php
if (isset($_GET['client'])) {
   echo $_GET['client'];
}
?>";

      window.location = "campaign.php?newCamp=1&cId=" + cId + "&uId=" + uId + "&nCampName=" + nCampName + "&nCampCode=" + nCampCode + "&nCampDesc=" + nCampDesc + "&stat=" + stat + "&client=" + cli;
   }

   function saveNewTemplate(temp)
   {
      //alert("temp=" + temp);
      var tId = temp;
      var uId = "<?php echo $_SESSION['userId']; ?>";
      var nTempDesc = encodeURIComponent(document.getElementById('newTempDesc').value);
      var nTempText = encodeURIComponent(document.getElementById('smsText').value);
      //nTempText = nTempText.replace("'", "&#39");
      var stat = 'ENABLED';
      var cli = "<?php
if (isset($_GET['client'])) {
   echo $_GET['client'];
}
?>";
      var cId = "<?php
if (isset($_GET['cId'])) {
   echo $_GET['cId'];
}
?>";

      window.location = "campaign.php?newTemp=1&tId=" + tId + "&uId=" + uId + "&nTempDesc=" + nTempDesc + "&nTempText=" + nTempText + "&stat=" + stat + "&client=" + cli + "&cId=" + cId;
   }

   function newT(type)
   {
      //alert(type);
      document.getElementById(type).style.display = 'block';
   }

   function showCampaign(cId)
   {
      $(".loader").fadeIn("slow");
      $(".loaderIcon").fadeIn("slow");
      window.location = "campaign.php?client=" + "<?php echo $_GET['client']; ?>" + "&cId=" + cId;
   }
   function showTemplate(tId)
   {
      $(".loader").fadeIn("slow");
      $(".loaderIcon").fadeIn("slow");
      window.location = "campaign.php?client=<?php echo $_GET['client']; ?>&cId=<?php
if (isset($_GET['cId'])) {
   echo $_GET['cId'];
}
?>" + "&tId=" + tId;
   }
   function reloadOnSelect(name)
   {
      $(".loader").fadeIn("slow");
      $(".loaderIcon").fadeIn("slow");
      window.location = "campaign.php?client=" + name;
   }

   function auto_grow(element)
   {
      element.style.height = "5px";
      element.style.height = (element.scrollHeight) + "px";
   }
</script>
<script type="text/javascript">
   $(window).load(function () {
      $(".loader").fadeOut("slow");
      $(".loaderIcon").fadeOut("slow");
   })
</script>

<!-- Template Footer -->

