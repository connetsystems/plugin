<?php
/**
 * Include the header template which sets up the HTML
 *
 * Don't forget to include template_import_script before any Javascripting
 * Don't forget to include template footer.php at the bottom of the page as well
 */
$headerPageTitle = "Confirm Quick SMS"; //set the page title for the template import
TemplateHelper::initialize();

if (isset($_POST['numbers'])) {
   $_POST['numbers'] = trim($_POST['numbers']);
   if (strpos($_POST['numbers'], ',')) {
      if (substr($_POST['numbers'], -1) == ',') {
         $_POST['numbers'] = substr($_POST['numbers'], 0, strlen($_POST['numbers']) - 1);
      }

      $numbersArr = explode(',', $_POST['numbers']);
   } else {
      $numbersArr[0] = $_POST['numbers'];
      $numCount = 1;
   }



   $numToSendString = '';
   $numToSendString2 = '';
   $invalidNumToSendString = '';
   foreach ($numbersArr as $key => $value)
   {
      if (substr($numbersArr[$key], 0, 1) != '0')
      {
         $numToSendString .= $numbersArr[$key] . ',';
      }
      else
      {
         $invalidNumToSendString .= $numbersArr[$key] . ',';
         unset($numbersArr[$key]);
      }
   }
   $numbersArr = array_values($numbersArr);
   $numCount = count($numbersArr);
   $numToSendString = substr($numToSendString, 0, -1);
   $deDupedNums = array_unique($numbersArr);

   $numToSendStringDeDup = '';
   foreach ($deDupedNums as $key => $value) {
      $numToSendStringDeDup .= $deDupedNums[$key] . ',';
   }
   $numToSendStringDeDup = substr($numToSendStringDeDup, 0, -1);

   echo "<script>console.log('Numbers " . implode($numbersArr, ',') . "');</script>";

   $mncCheckedNumbers = helpers\mnp::mnp_prefix_lookup($numbersArr);

   $numString = '';
   foreach ($mncCheckedNumbers as $key => $value)
   {
      if ((isset($value['valid']) && $value['valid'] == 1) && (isset($value['mccmnc']) && $value['mccmnc'] != ''))
      {
         $numString .= $value['mccmnc'] . ',';
         $numToSendString2 .= $value['msisdn'] . ',';
      }
      else
      {
         $invalidNumToSendString .= $value['msisdn'] . ',';
      }
   }

   $numString = substr($numString, 0, -1);
   $numToSendString2 = substr($numToSendString2, 0, -1);
   $invalidNumToSendString = substr($invalidNumToSendString, 0, -1);
   $costsArr = getSMSCost($_POST['service_id'], $numString);

   $totalCost = 0;
   $summaryCostArr = array();
   $summaryCountryArr = array();
   $summaryNetworkArr = array();
   $summaryCurrencyArr = array();

   $smsText = $_POST['smstext'];
   $serviceId = $_POST['service_id'];

   //get the SMS length and units
   $smsLength = helperSMSGeneral::getSMSCharacterCount($smsText);
   $units = helperSMSGeneral::getSMSUnits($smsLength);

   foreach ($mncCheckedNumbers as $key => $value)
   {
      if (isset($costsArr))
      {
         foreach ($costsArr as $key2 => $value2)
         {
            if ($value['mccmnc'] == $value2['prefix_mccmnc'])
            {
               $summaryCostArr[$key] = $value2['route_billing'];
               $totalCost += $units * $value2['route_billing'];
               $summaryCountryArr[$key] = $value2['prefix_country_name'];

               echo "<script>console.log('Country " . $value2['prefix_country_name'] . "');</script>";

               $summaryNetworkArr[$key] = $value2['prefix_network_name'];
               $summaryCurrencyArr[$key] = $value2['route_currency'];
               //$totalCost += $value2['route_billing'];
            }
         }
      }
      else
      {
         echo '<script>window.location = "quicksms.php?error=1&service_id=' . $_POST['service_id'] . '";</script>';
         die();
      }
   }

   $uniqueCountries = array_count_values($summaryCountryArr);
   if (count($uniqueCountries) == 0) {
      echo '<script>window.location = "quicksms.php?error=1&service_id=' . $_POST['service_id'] . '";</script>';
      die();
   }

   $uniqueNetworks = array_count_values($summaryNetworkArr);
   if (is_array($numbersArr)) {
      $uniqueNumbers = array_count_values($numbersArr);
   } else {
      $uniqueNumbers = 1;
   }

   $nArr = count($numbersArr);
   $uArr = count($uniqueNumbers);

   $duplicateNumbers = $nArr - $uArr;


   //check if the user actually has enough credit to send to these numbers
   $insufficient_funds = false;
   $available_credit = getServiceCredit($_POST['service_id']);
   $service_type = getServiceType($_POST['service_id']);
   if ($service_type == 'PREPAID' && $available_credit < $totalCost) {
      $insufficient_funds = true;
   }
}
?>

<aside class="right-side">
   <section class="content-header">
      <h1>
         Confirm Quick SMS
         <!--small>Control panel</small-->
      </h1>
   </section>

   <section class="content">

      <div class="row col-lg-12">
         <div class="box box-warning" style="border-top-color:<?php echo $accRGB; ?>;margin-top:0px;" id="mainBox">
            <div class="box-body" style="padding-bottom:48px;">
               <div class="callout callout-warning" style="margin-bottom:-20px;">
                  <p style="margin-bottom:0px;">Please confirm the following information below:</p>
               </div>
               <br/>
               <?php
               if ($insufficient_funds) {
                  echo '<div class="callout callout-danger" id="numssError" style="margin-bottom:0px;">';
                  echo '<p style="color:#B94A48;margin-bottom:0px;">You do not have enough funds to complete this quick send. Please contact the accounts department.</p>';
                  echo '<div style="float:right; margin-top:-20px; margin-right:0px;">';
                  echo '<i class="fa fa-exclamation-triangle" style="color:#c99b9d; font-size:16pt;"></i>';
                  echo '</div>';
                  echo '</div>';
               } else if ($invalidNumToSendString != "") {
                  echo '<div class="callout callout-danger" style="margin-bottom:-20px;">';
                  echo '<p style="margin-bottom:0px;">The following numbers are in an invalid format and/or you do not have routes set. They will not be sent:</p>';
                  echo '<p style="margin-bottom:0px;"><b>' . $invalidNumToSendString . '</b></p>';
                  echo '<div style="float:right; margin-top:-29px;margin-right: 10px;">';
                  echo '<i class="fa fa-exclamation-triangle" style="color:#c99b9d; font-size:15pt;"></i>';
                  echo '</div>';
                  echo '</div>';
                  echo '<br>';
               }
               ?>


               <div class="box-body">
                  <?php
                  /* echo "<p style='background-color:;'>";
                    if($numCount > 1)
                    {
                    echo "<p>You are submitting <b>".$units."</b> SMS to <b>".$numCount."</b> numbers.</p>";
                    }
                    else
                    {
                    echo "<p>You are submitting <b>".$units."</b> SMS to <b>".$numCount."</b> number.</p>";
                    }
                    //echo "<p><b>Total estimated cost of SMS:</b>  R".$totalCost." (ZAR).</p>";
                    echo "</p>"; */
                  echo "<p><b>Content:</b>&nbsp;" . $smsText . '</p>';
                  echo "<p><b>SMS length:</b>&nbsp;" . $smsLength . ' characters.</p>';
                  ?>
               </div>

               <div class="box-body table-responsive">
                  <table id="example2" class="table table-bordered table-striped dataTable table-hover">
                     <thead>
                        <tr>
                           <th>Country</th>
                           <th>Network</th>
                           <th>Number</th>
                           <th><center>Units</center></th>
                     <th><center>Cost</center></th>
                     <th><center>Currency</center></th>
                     </tr>
                     </thead>
                     <tbody>
                        <?php
                        //do some calculation
                        $intCountries = 0;
                        $totalCost = 0;
                        $totUnit = 0;
                        $totNu = 0;
                        $randCountries = array();
                        $uNet = array();
                        foreach ($mncCheckedNumbers as $key => $value)
                        {
                           if (isset($value['valid']) && $value['valid'] == 1 && (isset($value['mccmnc']) && $value['mccmnc'] != ''))
                           {
                              if (count($summaryCountryArr) > 0)
                              {
                                 $totNu++;

                                 //determine the currency
                                 $cur = $summaryCurrencyArr[$key];

                                 //determine the country flag
                                 if (substr($summaryCountryArr[$key], 0, 3) == "USA") //special handling for the USA flag
                                 {
                                    $flag = "USA";
                                 }
                                 else
                                 {
                                    $flag = $summaryCountryArr[$key];
                                 }

                                 $flag = str_replace('/', '&', $flag);

                                 //get the logo string for the network
                                 $logo = $summaryNetworkArr[$key];
                                 if (strpos($logo, '/')) {
                                    $logo = strstr($logo, '/');
                                    $logo = substr($logo, 2);
                                 }


                                 if (!in_array($summaryCountryArr[$key], $randCountries))
                                 {
                                    array_push($randCountries, $summaryCountryArr[$key]);
                                 }

                                 if (!in_array($summaryNetworkArr[$key], $uNet))
                                 {
                                    array_push($uNet, $summaryNetworkArr[$key]);
                                 }


                                 $logo = trim($logo);
                                 $cost = ($summaryCostArr[$key] / 10000) * $units;
                                 if (strlen($cost) == 1) {
                                    $cost = $cost . '.00';
                                 }
                                 if (strlen($cost) == 3) {
                                    $cost = $cost . '0';
                                 }
                                 $totalCost += $cost;
                                 if (strlen($totalCost) == 1) {
                                    $totalCost = $totalCost . '.00';
                                 }
                                 if (strlen($totalCost) == 3) {
                                    $totalCost = $totalCost . '0';
                                 }
                                 $totUnit += $units;

                                 //write out the row
                                 echo '<tr>';
                                    echo '<td><img src="../img/flags/' . $flag . '.png"> ' . $summaryCountryArr[$key] . '</td>';
                                    echo '<td><img src="../img/serviceproviders/' . $logo . '.png"> ' . $summaryNetworkArr[$key] . ' (' . $value['mccmnc'] . ')</td>';

                                    if ($uniqueNumbers[$value['msisdn']] == 1 || $uniqueNumbers == 1)
                                    {
                                       echo '<td>' . $value['msisdn'] . '</td>';
                                    }
                                    else
                                    {
                                       echo '<td style="background-color:#ffaa88;">' . $value['msisdn'] . '</td>';
                                    }

                                    echo '<td align="center">' . $units . '</td>';

                                    echo '<td align="center">' . $cost . '</td>';

                                    echo '<td align="center">' . $cur . '</td>';
                                 echo '</tr>';

                              }
                           }
                        }
                        ?>
                        <tr>
                           <td></td>
                           <td><strong>Total Networks: <?php echo count($uNet); ?></strong></td>
                           <td><strong>Total Numbers: <?php echo $totNu; ?></strong></td>
                           <td align="center"><strong>Total Units: <?php echo $totUnit; ?></strong></td>
                           <td align="center">-</td>
                           <td align="center">-</td>
                        </tr>
                     </tbody>
                  </table>
               </div>

               <?php
                  $exArr = explode(',', $numToSendString2);

                  $uniNus = array_count_values($exArr);
                  $uniCount = 1;
                  foreach($uniNus as $key => $value)
                  {
                     if($value > 1)
                     {
                        $uniCount = 0;
                     }
                  }

                  if ($duplicateNumbers > 0 && $uniCount == 0)
                  {
                     $dspx = 'inline-block';
                  }
                  else
                  {
                     $dspx = 'none';
                  }
               ?>
               <div style="display:<?php echo $dspx; ?>;">
                  <a onclick="removeDups();" class="btn btn-block btn-social btn-dropbox " style="background-color:<?php echo $accRGB; ?>; color: #ffffff; border: 2px solid; border-radius: 6px !important; float:left; width: 175px; margin-top:0px;">
                     <i class="fa fa-retweet"></i>Remove Duplicates
                  </a>
               </div>
               <div>
                  <a onclick="backBtn()" class="btn btn-block btn-social btn-dropbox " style="background-color:<?php echo $accRGB; ?>; color: #ffffff; border: 2px solid; border-radius: 6px !important; float:left; width: 92px; margin-top:0px;">
                     <i class="fa fa-arrow-left"></i>Back
                  </a>
               </div>
               <?php
               if (!$insufficient_funds && count($summaryCountryArr) > 0) {
                  ?>
                  <div style="display:block;">
                     <a onclick="confirmSubmit(<?php echo $_POST['service_id']; ?>)" class="btn btn-block btn-social btn-dropbox " style="background-color:<?php echo $accRGB; ?>; color: #ffffff; border: 2px solid; border-radius: 6px !important; float:left; width: 240px; margin-top:0px;">
                        <i class="fa fa-save"></i>Confirm and Send Quick SMS
                     </a>
                  </div>
                  <?php
               }
               ?>

            </div>
         </div>
      </div>
   </section>
</aside>

<!--  THE JAVASCRIPT IMPORT ALWAYS APPEARS BEFORE LOCAL JAVASCRIPT AT THE BOTTOM OF THE PAGE -->
<?php include("template_import_script.php"); //must import all scripts first   ?>
<!-- END JAVASCRIPT IMPORT -->

<script type="text/javascript">
   function backBtn()
   {
      history.back();
   }

   function removeDups()
   {
      //alert('removeDups');

      var client = "<?php echo $_POST['service_id']; ?>";

      var smsText = "<?php echo urlencode($smsText); ?>";

      var service_id = "<?php echo $_POST['service_id']; ?>";

      var numbers = "<?php echo $numToSendStringDeDup; ?>";

      window.location = "../pages/confirmquicksms.php?numbers=" + numbers + "&smstext=" + smsText + "&serviceId=" + service_id + "&client=" + client;
      //numbers=27824139864%2C27761363038%2C27761363038%2C&smstext=sdg&serviceId=87&client=Connet+Systems+%28Pty%29+Ltd+-+Thinus%2FBulk+-+87
   }

   function confirmSubmit(v)
   {

      $(".loader").fadeIn("slow");
      $(".loaderIcon").fadeIn("slow");

      form = document.createElement('form');
      form.setAttribute('method', 'POST');
      form.setAttribute('action', '../pages/successquicksms.php');

      serviceId = document.createElement('input');
      serviceId.setAttribute('name', 'serviceId');
      serviceId.setAttribute('type', 'hidden');
      serviceId.setAttribute('value', v);
      form.appendChild(serviceId);
      numbers = document.createElement('input');
      numbers.setAttribute('name', 'numbers');
      numbers.setAttribute('type', 'hidden');
      numbers.setAttribute('value', "<?php echo $numToSendString2; ?>");
      form.appendChild(numbers);
      smsText = document.createElement('input');
      smsText.setAttribute('name', 'smsText');
      smsText.setAttribute('type', 'hidden');
      smsText.setAttribute('value', "<?php echo urlencode($smsText); ?>");
      form.appendChild(smsText);
      smsRandCheck = document.createElement('input');
      smsRandCheck.setAttribute('name', 'smsRandCheck');
      smsRandCheck.setAttribute('type', 'hidden');
      smsRandCheck.setAttribute('value', "<?php echo urlencode($_SESSION['randomSendCheck']); ?>");
      form.appendChild(smsRandCheck);

      document.body.appendChild(form);
      form.submit();
   }
</script>

<script type="text/javascript">
   $(window).load(function ()
   {

      $(".loader").fadeOut("slow");
      $(".loaderIcon").fadeOut("slow");
      /*sleep(4);
       fixHeight();                */
      //setTimeout(fixHeight, 5000);
   })

   /*function fixHeight()
    {
    var boxH = document.getElementById("mainBox").style.height;
    var boxI = parseInt(boxH.substr(0, boxH.length-2));
    boxI = boxI + 130;
    boxH = boxI.toString() + 'px';
    alert(boxH)
    document.getElementById("mainBox").style.height = boxH;
    }*/
</script>

<!-- Template Footer -->

