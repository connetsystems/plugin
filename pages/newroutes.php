<?php
/**
 * Include the header template which sets up the HTML
 *
 * Don't forget to include template_import_script before any Javascripting
 * Don't forget to include template footer.php at the bottom of the page as well
 */
TemplateHelper::setPageTitle('Assign Routes');
TemplateHelper::initialize();
 
if (isset($_GET['service_id'])) {
   $service_id = $_GET['service_id'];

   $resellerRoutes = getResellerDefaultRouteData($_SESSION['serviceId'], $service_id);
   $subRoutes = getAddedDefaultRouteData($service_id);
}

if (isset($_SESSION['accountId'])) {
   $acId = $_SESSION['accountId'];
   if ($acId == 1) {
      $clients = getServicesInAccount($acId);
   } else {
      $clients = getServicesInAccountWithOutRid($acId);
   }
} else {
   $acId = null;
   $clients = getServicesInAccount($acId);
}
?>
<!-- Right side column. Contains the navbar and content of the page -->
<aside class="right-side">
   <!--//////////////////////////-->
   <section class="content-header">
      <h1>
         Add Routes
      </h1>
   </section>

   <section class="content">
      <div class="row">
         <form action="newroutes.php" class="col-md-12" method="get" id="newService" style="border:none;">
            <!--div class="col-md-12"-->
            <div class="box box-connet" style="padding-bottom:1px; border-top-color:<?php echo $accRGB; ?>;">
               <div class="box-body" id="notify" style="height:100px;">
                  <div class="callout callout-info">
                     <h4>Add Routes To Client Instructions / Details</h4>
                     <p>Here you should add the routes you wish to assign to the service you just created.</p>
                  </div>
                  <div id="notifyE" class="callout callout-danger" style="display:none;" >
                     <h4>Error:</h4>
                     <p>The selling price has to be greater than or the same as the cost price.</p>
                  </div>
               </div>
            </div>

            <div class="box box-connet" style="padding-bottom:1px; border-top-color:<?php echo $accRGB; ?>; height:auto;" >
               <div class="box-header">
                  <h3 class="box-title">Available routes:</h3>
               </div>
               <div class="box-body">
                  <table id="example2" class="table table-bordered table-striped dataTable table-hover">
                     <thead>
                        <tr>
                           <th>Country</th>
                           <th>Network</th>
                           <th><center>STATUS</center></th>
                     <th><center>COST RATE</center></th>
                     <th><center>SELL RATE</center></th>
                     <th><center>Assign Route</center></th>
                     </tr>
                     </thead>

                     <tbody>
                        <!--form action="newresellerroutes.php" method="get" id="resellerRouteRates" name="resellerRouteRates" class="sidebar-form" style="border:0px;"-->
                        <?php
                        $numId = 0;
//$resellerRouteRates = "'resellerRouteRates'";
//echo '<form action="newresellerroutes.php" method="get" id="resellerRouteRates" class="sidebar-form" style="border:0px;">';
                        if (isset($resellerRoutes)) {
                           foreach ($resellerRoutes as $key => $value) {
                              /*
                                $value['COUNTRY'];
                                $value['NETWORK'];
                                $value['STATUS'];
                                $value['RATE'];
                                $value['COLLECTION'];
                                $value['route_collection_id'];
                                $value['MCCMNC'];
                                $value['route_id'];
                                $value['DEFAULT'];
                                $value['PORTED'];
                                $value['provider_id'];
                                $value['service_id'];
                                $value['route_msg_type'];
                                $value['route_description'];
                                $value['route_display_name'];
                                $value['route_code'];
                                $value['route_match_regex'];
                                $value['route_match_priority'];
                                $value['route_billing'];
                               */
                              $rDescPos = strrpos($value['route_description'], ' ');
                              $rDesc = substr($value['route_description'], 0, $rDescPos - 1);
                              $rDesc = urlencode($rDesc);
                              $value['route_display_name'] = urlencode($value['route_display_name']);
                              $value['route_code'] = urlencode($value['route_code']);
                              $value['route_billing'] = urlencode($value['route_billing']);

                              //echo '<br>rdesc-'.$value['route_display_name'].'-';

                              $logo = trim(strstr($value['NETWORK'], ' (', true));

                              $urlReg = urlencode($value['route_match_regex']);

                              echo '<tr name="routeId" value="' . htmlspecialchars($value['route_id']) . '">';
                              echo '<td style="vertical-align:middle;"><img src="../img/flags/' . $value['COUNTRY'] . '.png">&nbsp;&nbsp;' . htmlentities($value['COUNTRY']) . '</td>';
                              echo '<td style="vertical-align:middle;"><img src="../img/serviceproviders/' . trim($logo) . '.png">&nbsp;&nbsp;' .  htmlentities( $value['NETWORK']) . '</td>';
                              if ($value['STATUS'] == 'ENABLED') {
                                 echo '<td style="vertical-align:middle;"><center><i class="fa fa-check" style="color:#00ff00;"></i>&nbsp;&nbsp;' . htmlentities($value['STATUS']) . '</center></td>';
                              } else {
                                 echo '<td style="vertical-align:middle;"><center><i class="fa fa-times" style="color:#ff0000;"></i>&nbsp;&nbsp;' . htmlentities($value['STATUS']) . '</center></td>';
                              }
                              echo '<td style="vertical-align:middle;"><center><div style="display: inline-table;"></div>' . htmlentities($value['RATE']) . '</div></center></td>';

                              echo '<td style="vertical-align:middle;">
                                                                        <center>
                                                                            <div class="fakey" style="padding-left:10px;width:100px;">
                                                                                <div style="display: inline-table;">0.</div>
                                                                                    <input type="text" onkeypress="return isNumberKey(' . $value['RATE'] . ', this.value, event)" id="routeRate' . $value['route_id'] . '" name="routeRate' . $value['route_id'] . '" placeholder="" style="background:#ffffff;width:40px;border-color:#929292;border:0px;" maxlength="4" value="' . substr($value['RATE'], 2) . '">
                                                                            </div>
                                                                        </center>
                                                                </td>';

                              echo '<td style="vertical-align:middle;">
                                                                    <center>
                                                                            <a class="btn btn-block btn-social btn-dropbox btnAddRouteAndRate" route_id="' . $value['route_id'] . '"  style="background-color:' . $accRGB . '; color: #ffffff; border: 2px solid; border-radius: 6px !important; width: 140px; margin-top:0px;">
                                                                                <i class="fa fa-plus"></i>Assign Route
                                                                            </a>
                                                                    </center>
                                                                </td>';
                              echo '</tr>';

                              $numId++;
                           }
                        }
//echo '</form>';
                        ?>
                        <!--/form-->
                     </tbody>
                  </table>
               </div>
            </div>

            <div class="box box-connet" style="padding-bottom:1px; border-top-color:<?php echo $accRGB; ?>; height:auto;" >
               <div class="box-header">
                  <h3 class="box-title">Assigned routes:</h3>
               </div>

               <div class="box-body" style="padding:0px;">
                  <div class="box-body table-responsive">
                     <table id="example2" class="table table-bordered table-striped dataTable table-hover">
                        <thead>
                           <tr>
                              <!--th>#</th-->
                              <th>Country</th>
                              <th>Network</th>
                              <th><center>SMSC</center></th>
                        <th><center>SELL RATE</center></th>
                        <th><center>Remove Route</center></th>
                        </tr>
                        </thead>

                        <tbody>
                           <?php
                           if (isset($subRoutes)) {
                              $numId = 0;

                              foreach ($subRoutes as $key => $value) {
                                 $logo = trim(strstr($value['NETWORK'], ' (', true));

                                 echo '<tr name="routeId" value="' . htmlspecialchars($value['route_id']) . '">';
                                 echo '<td style="vertical-align:middle;"><img src="../img/flags/' . $value['COUNTRY'] . '.png">&nbsp;&nbsp;' . htmlentities($value['COUNTRY']) . '</td>';
                                 echo '<td style="vertical-align:middle;"><img src="../img/serviceproviders/' . trim($logo) . '.png">&nbsp;&nbsp;' . htmlentities($value['NETWORK']) . '</td>';
                                 if ($value['STATUS'] == 'ENABLED') {
                                    echo '<td style="vertical-align:middle;"><center><i class="fa fa-check" style="color:#00ff00;"></i>&nbsp;&nbsp;' . htmlentities($value['STATUS']) . '</center></td>';
                                 } else {
                                    echo '<td style="vertical-align:middle;"><center><i class="fa fa-times" style="color:#ff0000;"></i>&nbsp;&nbsp;' .  htmlentities( $value['STATUS']) . '</center></td>';
                                 }
                                 echo '<td style="vertical-align:middle;"><center><div style="display: inline-table;"></div>' . htmlentities($value['RATE']) . '</div></center></td>';

                                 echo '<td style="vertical-align:middle;">
                                                                <center>
                                                                        <a class="btn btn-block btn-social btn-dropbox btnRemoveRoute" route_id="' . htmlspecialchars($value['route_id']) . '" style="background-color:' . $accRGB . '; color: #ffffff; border: 2px solid; border-radius: 6px !important; width: 140px; margin-top:0px;">
                                                                            <i class="fa fa-minus"></i>Remove Route
                                                                        </a>
                                                                </center>
                                                            </td>';
                                 echo '</tr>';

                                 $numId++;
                              }
                           }
                           ?>
                        </tbody>
                     </table>
                     <br>
                     <?php
                     if ($numId != 0) {
                        echo '<a class="btn btn-block btn-social btn-dropbox" name="" onclick="gotoAddUsers(' . $service_id . ')" style="background-color:' . $accRGB . '; color: #ffffff; border: 2px solid; border-radius: 6px !important; width: 140px; margin-top:0px;">';
                        echo '<i class="fa fa-users"></i>Add Users';
                        echo '</a>';
                     }
                     ?>
                  </div>
               </div>
         </form>
      </div>
   </section>
</aside>

<!--  THE JAVASCRIPT IMPORT ALWAYS APPEARS BEFORE LOCAL JAVASCRIPT AT THE BOTTOM OF THE PAGE -->
<?php include("template_import_script.php"); //must import all scripts first ?>
<!-- END JAVASCRIPT IMPORT -->

<script type="text/javascript">
   $(window).load(function ()
   {
      $(".loader").fadeOut("slow");
      $(".loaderIcon").fadeOut("slow");
   });

   $(function () {
      var ajaxBusy = false;
      //assign a click listener to the forgot password link
      $(".btnAddRouteAndRate").on("click", function (e) {
         e.preventDefault();
         if (!ajaxBusy)
         {
            ajaxBusy = true;
            var thisButton = $(this);
            var thisButtonHtml = $(this).html();
            thisButton.prop('disabled', true);
            thisButton.html('<span class="glyphicon glyphicon-refresh glyphicon-spin"></span> Adding route...');
            var routeId = thisButton.attr('route_id');
            var routeRate = $("#routeRate" + routeId).val();
            var serviceId = <?php echo json_encode(isset($service_id) ? $service_id : 0); ?>;

            //run the addition script
            $.post("../php/ajaxAddOrRemoveRoute.php", {
               task: 'add',
               routeId: routeId,
               routeRate: routeRate,
               serviceId: serviceId
            }).done(function (data) {
               var json = $.parseJSON(data);

               if (json.success) {
                  location.reload();
               } else {
                  ajaxBusy = false;
                  thisButton.html(thisButtonHtml);
                  thisButton.prop('disabled', false);
                  alert("An error occurred and we were unable to assign this route.")
               }

            }).fail(function (data) {
               ajaxBusy = false;
               thisButton.html(thisButtonHtml);
               thisButton.prop('disabled', false);
               alert("An error occurred and we were unable to assign this route.")
            });
         }
      });

      //assign a click listener to the forgot password link
      $(".btnRemoveRoute").on("click", function (e) {
         e.preventDefault();
         if (!ajaxBusy) {
            ajaxBusy = true;
            var thisButton = $(this);
            var thisButtonHtml = $(this).html();
            thisButton.prop('disabled', true);
            thisButton.html('<span class="glyphicon glyphicon-refresh glyphicon-spin"></span> Adding route...');

            var routeId = $(this).attr('route_id');

            //run the remove script
            $.post("../php/ajaxAddOrRemoveRoute.php", {
               task: 'remove',
               routeId: routeId
            }).done(function (data) {
               var json = $.parseJSON(data);

               if (json.success) {
                  location.reload();
               } else {
                  ajaxBusy = false;
                  thisButton.html(thisButtonHtml);
                  thisButton.prop('disabled', false);
                  alert("An error occurred and we were unable to assign this route.")
               }

            }).fail(function (data) {
               ajaxBusy = false;
               thisButton.html(thisButtonHtml);
               thisButton.prop('disabled', false);
               alert("An error occurred and we were unable to assign this route.")
            });
         }
      });
   });


   function gotoAddUsers(service_id)
   {
      $(".loader").fadeIn("slow");
      $(".loaderIcon").fadeIn("slow");
      window.location = "newusers.php?service_id=" + service_id;
   }
   ;


   function isNumberKey(oV, v, evt)
   {
      var charCode = (evt.which) ? evt.which : evt.keyCode;

      if (charCode != 46 && charCode > 31
              && (charCode < 48 || charCode > 57))
         return false;

      return true;

   }
</script>

<!-- Template Footer -->