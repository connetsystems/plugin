<?php
/**
 * Include the header template which sets up the HTML
 *
 * Don't forget to include template_import_script before any Javascripting
 * Don't forget to include template footer.php at the bottom of the page as well
 */
$headerPageTitle = "Bulk SMS " . $_GET['type']; //set the page title for the template import
TemplateHelper::initialize();


if (isset($_GET['type'])) {
   if ($_GET['type'] == 'MOSMS') {
      $typeDis = 'mosms_id';
   } else {
      $typeDis = 'mtsms_id';
   }
   $_GET['type'] = ucfirst(strtolower($_GET['type']));
}

if (isset($_GET['cli'])) {
   //echo '<br>Cli='.$_GET['cli'];
   $eCli = $_GET['cli'];
}

if (isset($_GET['camp'])) {
   //echo '<br>camp='.$_GET['camp'];
   $eCamp = $_GET['camp'];
}

if (isset($_GET['code'])) {
   $eCode = $_GET['code'];
   ///echo '<br>code='.$_GET['code'];
}

if (isset($_GET['ref'])) {
   $eRef = $_GET['ref'];
   //echo '<br>ref='.$_GET['ref'];
}

if (isset($_GET['rr'])) {
   $rr = $_GET['rr'];
   /* $dateArr1 = explode(' - ', $_GET['range1']);
     $startDate1 = $dateArr1[0];
     $startDateConv1 = date('Y-m-d', ($startDate1/1000)); */
}


if (isset($_GET['start_date']) && isset($_GET['end_date']))
{
   $start_date = $_GET['start_date'];
   $end_date = $_GET['end_date'];
}
else
{
   $start_date = "";
   $end_date = "";
}

if (isset($_GET['service_id']))
{
   $service_id = $_GET['service_id'];
   $service_name = getServiceName($service_id);
}
else
{
   $service_id = 0;
   $service_name = "(no service selected)";
}

if (isset($_GET['bId']))
{

   $modata = MOSMS::getAllByBatchId($_GET['bId']);
   //echo '<br>coun='.count($modata);
}
?>
<aside class="right-side">
   <section class="content-header">
      <h1>
         Bulk SMS <?php echo $_GET['type']; ?>
         <!--small>Control panel</small-->
      </h1>
   </section>

   <section class="content">
      <div class="box box-connet" style="padding-top:10px;padding-left:10px;padding-right:10px;padding-bottom:45px; border-top-color:<?php echo $accRGB; ?>;">
         <div class="callout callout-info" style="margin-bottom:10px;">
            <h4><?php echo $_GET['type']; ?> Messages: <?php echo $service_name; ?></h4>
            <p>You are viewing all <?php echo $_GET['type']; ?> SMS's for the date: <?php echo $rr; ?></p>
         </div>
         <a class="btn btn-block btn-social btn-dropbox" name="nid" onclick="backToTraffic()" style="background-color:<?php echo $accRGB; ?>; border: 2px solid; border-radius: 6px !important; float:left; width: 100px;">
            <i class="fa fa-arrow-left"></i>Back
         </a>
      </div>
   </section>

   <section class="content">
      <div class="box box-connet" style="padding-top:1px; border-top-color:<?php echo $accRGB; ?>;">
         <div class="box-body">
            <div class="box-body table-responsive no-padding">
               <table id="example2" class="table table-bordered table-striped dataTable table-hover">
                  <thead>
                     <tr>
                        <th><?php echo $typeDis; ?>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</th>
                        <th style='width:100px;'>Date</th>
                        <th>Number</th>
                        <th>Content</th>
                     </tr>
                  </thead>
                  <tbody>
                     <?php
                     if (isset($modata) && $modata != '')
                     {
                        $tots = 0;
                        foreach ($modata as $mosms)
                        {
                           $tots++;

                           echo '<tr>';
                              echo '<td>' . $mosms->mosms_id . '</td>';
                              echo '<td style="width:170px;">' .str_replace('T', ' ', strstr($mosms->timestamp, '+', true)) . '</td>';
                              if (strtolower($_GET['type']) == 'rejected' || strtolower($_GET['type']) == 'blacklist') {
                                 echo '<td>' . $mosms->dest . '</td>';
                              } else {
                                 if (strlen($mosms->src) > 5) {
                                    echo '<td>' . $mosms->src . '</td>';
                                 } else {
                                    echo '<td>-</td>';
                                 }
                              }
                              echo '<td>' . utf8_decode(utf8_decode($mosms->content)) . '</td>';
                           echo '</tr>';
                        }
                     }
                     ?>
                  </tbody>
               </table>
               <div class="box-body">
                  <div class="form-group" style="padding-top:30px">
                     <a href="../php/createCSVbatchlistMO.php?service_id=<?php echo $service_id; ?>&batch_id=<?php echo $_GET['bId']; ?>" class="btn btn-block btn-social btn-dropbox" style="background-color:<?php echo $accRGB; ?>; border: 2px solid; border-radius: 6px !important; float:left; width: 134px; margin-top:-21px;">
                        <i class="fa fa-save"></i>Save Report
                     </a>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </section>
</aside>

<!--  THE JAVASCRIPT IMPORT ALWAYS APPEARS BEFORE LOCAL JAVASCRIPT AT THE BOTTOM OF THE PAGE -->
<?php include("template_import_script.php"); //must import all scripts first ?>
<!-- END JAVASCRIPT IMPORT -->

<script type="text/javascript">
   $(function ()
   {
      $('#example2').dataTable({
         /*"processing": true,
          "serverSide": true,*/
         //"ajax": "scripts/server_processing.php"
         "bPaginate": true,
         "bLengthChange": true,
         "bFilter": true,
         "bSort": true,
         "bInfo": true,
         "bAutoWidth": true,
         "iDisplayLength": 100
                 /*"aoColumns": [
                  null,
                  null,
                  null,
                  null,
                  null,
                  null
                  ],*/
      });
   });

   jQuery.extend(jQuery.fn.dataTableExt.oSort,
           {
              "title-numeric-pre": function (a) {
                 var x = a.match(/title="*(-?[0-9\.]+)/)[1];
                 return parseFloat(x);
              },
              "title-numeric-asc": function (a, b) {
                 return ((a < b) ? -1 : ((a > b) ? 1 : 0));
              },
              "title-numeric-desc": function (a, b) {
                 return ((a < b) ? 1 : ((a > b) ? -1 : 0));
              }
           });

   jQuery.extend(jQuery.fn.dataTableExt.oSort,
           {
              "formatted-num-pre": function (a) {
                 a = (a === "-" || a === "") ? 0 : a.replace(/[^\d\-\.]/g, "");
                 return parseFloat(a);
              },
              "formatted-num-asc": function (a, b) {
                 return a - b;
              },
              "formatted-num-desc": function (a, b) {
                 return b - a;
              }
           });
</script>

<script type="text/javascript">
   function backToTraffic()
   {
      var serviceName = "<?php echo ($_GET['client']); ?>";
      var startDate = "<?php echo urlencode($start_date); ?>";
      var endDate = "<?php echo urlencode($end_date); ?>";

      window.location = "../pages/batchlist.php?client=" + (serviceName) + "&start_date=" + startDate + "&end_date=" + endDate;
   }

</script>

<script type="text/javascript">
   $(window).load(function () {
      $(".loader").fadeOut("slow");
      $(".loaderIcon").fadeOut("slow");
   })
</script>
<!-- Template Footer -->

