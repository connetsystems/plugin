<?php
/**
 * Include the header template which sets up the HTML
 *
 * Don't forget to include template_import_script before any Javascripting
 * Don't forget to include template footer.php at the bottom of the page as well
 */
$headerPageTitle = "Manage Users"; //set the page title for the template import
TemplateHelper::initialize();

require_once('../php/config/dbConn.php');
require_once('../php/allInclusive.php');
require_once('../php/helperPassword.php');


if (isset($_POST['service_id'])) {
   $service_id = $_POST['service_id'];
} else if (isset($_GET['service_id'])) {
   $service_id = $_GET['service_id'];
}

//check the user has permissions to actually view this service, kill the page if they don't
if (isset($service_id) && $_SESSION['userSystemAdmin'] != 1 && !checkAccountHasAccessToService($_SESSION['accountId'], $service_id)) {
   echo '<meta http-equiv="refresh" content= "0; URL=access_denied.php" />';
   die();
}

/*
 * NEW! SQL search for prefix URL to get resources for an account prefix
 */
$conn = getPDOMasterCore();

if (isset($_POST['change_password']) && $_POST['change_password'] == 1)
{
   $user_id = $_POST['user_id'];
   $new_password = $_POST['new_password'];
   $new_password_confirm = $_POST['new_password_confirm'];
   if ($new_password == "" || $new_password_confirm == "")
   {
      $password_error = true;
      $password_error_string = "You must fill in both fields.";
   }
   else if (strcmp($new_password, $new_password_confirm) !== 0)
   {
      $password_error = true;
      $password_error_string = "The passwords you entered do not match, please try again.";
   }
   else
   {
      //hash and save the new password for the user
      $hashed_password = helperPassword::createHashedPassword($new_password);

      //prepare the data
      $data_update_password = array(':user_id' => $user_id, ':new_password' => $new_password, ':hashed_password' => $hashed_password);

      //prepare the query
      $stmt_update_password = $conn->prepare("UPDATE core_user
                    SET user_password = :new_password, user_password_hashed = :hashed_password
                    WHERE user_id = :user_id ");
      //execute it
      $stmt_update_password->execute($data_update_password);

      $password_success = true;
      $password_success_string = "The password for this user was successfully changed.";
   }
}
else if (isset($_POST['update_details']) && $_POST['update_details'] == 1)
{
   $user_id = $_POST['user_id'];
   $username = $_POST['username'];
   $user_status = $_POST['user_status'];
   $first_name = $_POST['first_name'];
   $last_name = $_POST['last_name'];
   $company_name = $_POST['company_name'];
   $email_address = $_POST['email_address'];
   $cell_number = $_POST['cell_number'];
   $account_id = $_POST['account_id'];
   $service_id = $_POST['service_id'];

   if ($first_name == "" && $last_name == "" && $company_name == "" && $email_address == "" && $cell_number == "" && $username == "") {
      $password_error = true;
      $password_error_string = "You must fill in at least one field.";
   } else {
      //cleanup cell number
      if ($cell_number != "" && substr($cell_number, 0, 1) == '0') {
         $cell_number = "27" . substr($cell_number, 1);
      }

      //prepare the data
      $data_update = array(':user_id' => $user_id, ':lastname' => $last_name, ':companyname' => $company_name, ':emailaddress' => $email_address, ':cellnumber' => $cell_number);

      //we need to check if the account_details row exists for this user, if not, we create it
      $stmt_check_exists = $conn->prepare('SELECT * FROM core.account_details WHERE core_user_id = ?');
      $stmt_check_exists->bindParam(1, $user_id, PDO::PARAM_INT);
      $stmt_check_exists->execute();
      $row_check = $stmt_check_exists->fetch(PDO::FETCH_ASSOC);

      if (!$row_check) { //does the record exist?
         //Doesn't exist, prepare an insert statement
         $data_details = array(':user_id' => $user_id, ':firstname' => $first_name, ':lastname' => $last_name, ':companyname' => $company_name, ':emailaddress' => $email_address,
             ':cellnumber' => $cell_number, ':tutorial_seen' => 1, ':verified' => 1, ':account_id' => $account_id, ':service_id' => $service_id); //tutorial seen and verified on by default because this must be an old account
         //prepare the query
         $stmt_details = $conn->prepare("INSERT INTO core.account_details (core_user_id, firstname, lastname, companyname, emailaddress, cellnumber, tutorial_seen, verified, account_id, service_id)
                    VALUES (:user_id, :firstname, :lastname, :companyname, :emailaddress, :cellnumber, :tutorial_seen, :verified, :account_id, :service_id) ");
      } else {
         //Does exist, prepare an UPDATE statement
         $data_details = array(':user_id' => $user_id, ':firstname' => $first_name, ':lastname' => $last_name, ':companyname' => $company_name, ':emailaddress' => $email_address,
             ':cellnumber' => $cell_number);

         //prepare the query
         $stmt_details = $conn->prepare("UPDATE core.account_details
                    SET firstname = :firstname, lastname = :lastname, companyname = :companyname, emailaddress = :emailaddress, cellnumber = :cellnumber
                    WHERE core_user_id = :user_id ");
      }

      //execute it
      $stmt_details->execute($data_details);

      echo $service_id;
      //save the username if it changed
      $check = checkUserName($username);
      if ($check === 1) {
         //get the old data to run a comparison
         $service_data = getServiceData($service_id);
         $user_account = getUser($user_id);

         updateServer($username, "user_username", "core.core_user", "user_id", $user_id);

         //check if the old username matches the services account username, which means the account username must also be updated otherwise API endpoints will break
         if ($user_account['user_username'] === $service_data[0]['service_login']) {
            updateServer($username, "service_login", "core.core_service", "service_id", $service_id);
            //$defaultServ = getDefaultServiceLoginFromSID($cl);
            $dlrID = getEPDLRid($service_id, 'SMPP');
            updateServer('http://server2:3001/core_sms/interfaces/smpp/smpp_dlr.php?debug=hallodaar&system_id=' . $username, "endpoint_dlr_address", "core.push_endpoint_dlr", "endpoint_dlr_id", $dlrID);
            $mosmsID = getEPMOSMSid($service_id, 'SMPP');
            updateServer('http://server2:3001/core_sms/interfaces/smpp/smpp_mosms.php?debug=hallodaar&system_id=' . $username, "endpoint_mosms_address", "core.push_endpoint_mosms", "endpoint_mosms_id", $mosmsID);
         }
      }

      //lastly, the user status
      updateServer($user_status, "user_status", "core.core_user", "user_id", $user_id);

      $update_success = true;
      $update_success_string = "The new settings were successfully saved.";
   }
} else if (isset($_GET['user'])) {
   $user_id = substr($_GET['user'], (strpos($_GET['user'], ' - ') + 3), strlen($_GET['user']));
} else if (isset($_GET['user_id'])) {
   $user_id = $_GET['user_id'];
}

/*
 * PAGE INIT
 */
//get the full users list
//if the user is admin
if (isset($_SESSION['userSystemAdmin']) && $_SESSION['userSystemAdmin'] == 1) {
   $has_user_permission = true;
   $users = getAllUsers();
} else if (isset($service_id)) {
   $users = getUsers($service_id);
} else {
   $users = getUsers($_SESSION['service_id']);
}


//check if we wnat to see a users information, and whether or not we are allowed to
if (isset($user_id)) {
   $has_user_permission = false;
   if (isset($_SESSION['userSystemAdmin']) && $_SESSION['userSystemAdmin'] == 1) {
      $has_user_permission = true;
   } else {
      //check that this user has permission to view this user
      foreach ($users as $user) {
         if ($user['user_id'] == $user_id) {
            $has_user_permission = true;
         }
      }
   }

   if ($has_user_permission) {
      //get the users account_details
      $user_account = getUser($user_id);

      //fetch the data
      $user_account_details = getUserAccountDetails($user_id);
   }
}
?>




<aside class="right-side">
   <section class="content-header">
      <h1>
         Manage Users
         <!--small>Control panel</small-->
      </h1>
   </section>

   <section class="content">

      <div class="row col-lg-12">
         <div class="box box-solid" style="height:66px; margin-top:-15px;display:<?php echo $singleView; ?>">
            <form action="updateservice.php" method="get" id="clientList" class="sidebar-form" style="border:0px;">
               <div class="form-group">
                  <label>Users List</label>
                  <select class="form-control" name="client" form="clientList" OnChange="reloadOnSelect(this.value);">
                     <?php
                     if ($user_id == '0') {
                        echo '<option SELECTED>Please select a user...</option>';
                     }
                     foreach ($users as $key => $value) {
                        $uId = $value['user_id'];
                        $userName = $value['user_username'];
                        if ($user_id == $uId) {
                           //$serviceName = $value['service_name'];
                           echo "<option value='" . $uId . "' SELECTED>" . $userName . " - " . $uId . "</option>";
                        } else {
                           echo "<option value='" . $uId . "'>" . $userName . " - " . $uId . "</option>";
                        }
                     }
                     ?>
                  </select>
               </div>
               <br>
            </form>
         </div>
         <?php
         if ($has_user_permission && isset($user_id) && $user_id > 0) {
            ?>
            <div class="row">
               <div class="col-lg-8">
                  <div class="box box-connet" style="padding-bottom:1px; border-top-color:<?php echo $accRGB; ?>; height:auto;" >
                     <div class="box-header">
                        <h3 class="box-title">Account Details for "<?php echo $user_account['user_username']; ?>"</h3>

                        <?php
                        //NEW ALLOWS FOR A USER TO MIMIC FUNCTIONALITY
                        if (isset($_SESSION['userSystemAdmin']) && $_SESSION['userSystemAdmin'] == 1) {
                           echo '<button class="btn btn-sm btn-primary pull-right btnMimic"  style="margin:5px;" value="' . $user_id . '">Mimic This User</button>';
                        }
                        ?>
                     </div>
                     <div class="box-body">
                        <?php
                        if (isset($update_error) && $update_error == true) {
                           echo '<div class="callout callout-danger" style="margin-bottom:0px;">';
                           echo '<b>Update error: ' . $update_error_string . '</b>';
                           echo '<div style="float:right; margin-top:-3px">';
                           echo '<i class="fa fa-exclamation-triangle" style="color:#dFb5b4; font-size:20pt; margin-right:10px;"></i>';
                           echo '</div>';
                           echo '</div><br/>';
                        }

                        if (isset($update_success) && $update_success == true) {
                           echo '<div class="callout callout-success" style="margin-bottom:0px;">';
                           echo '<b>' . $update_success_string . '</b>';
                           echo '<div style="float:right; margin-top:-3px">';
                           echo '<i class="fa fa-check" style="color:#5cb157; font-size:20pt; margin-right:10px;"></i>';
                           echo '</div>';
                           echo '</div><br/>';
                        }

                        $username = "";
                        $first_name = "";
                        $last_name = "";
                        $company_name = "";
                        $email_address = "";
                        $cell_number = "";
                        if (isset($user_account_details)) {
                           $username = $user_account['user_username'];
                           $first_name = $user_account_details['firstname'];
                           $last_name = $user_account_details['lastname'];
                           $company_name = $user_account_details['companyname'];
                           $email_address = $user_account_details['emailaddress'];
                           $cell_number = $user_account_details['cellnumber'];
                        }
                        ?>
                        <p>You can update some of the specific account details of this user using this form.</p>
                        <form role="form" action="manage_users.php" method="post" id="updateDetails" style="border:0px;">
                           <div class="row">
                              <div class="col-lg-6">
                                 <div class="form-group">
                                    <label for="new_password">Username:</label>
                                    <input type="text" class="form-control" maxlength="40" name="username" placeholder="No first name entered..." value="<?php echo $username; ?>" />
                                 </div>
                              </div>
                              <div class="col-lg-6">
                                 <div class="form-group">
                                    <label for="new_password_confirm">Enabled:</label>
                                    <?php
                                    if ($user_account['user_status'] == "ENABLED") {
                                       $c1b = '#33cc33;';
                                    } else {
                                       $c1b = '#cc3333;';
                                    }
                                    ?>
                                    <select class="form-control" name="user_status" style="border-color:<?php echo $c1b; ?>">
                                       <option value="DISABLED" <?php if ($user_account['user_status'] == "DISABLED") echo ' selected'; ?>>DISABLED</option>
                                       <option value="ENABLED" <?php if ($user_account['user_status'] == "ENABLED") echo ' selected'; ?>>ENABLED</option>
                                    </select>
                                 </div>
                              </div>
                           </div>
                           <div class="row">
                              <div class="col-lg-4">
                                 <div class="form-group">
                                    <label for="new_password">First Name:</label>
                                    <input type="text" class="form-control" maxlength="40" name="first_name" placeholder="No first name entered..." value="<?php echo $first_name; ?>" />
                                 </div>
                              </div>
                              <div class="col-lg-4">
                                 <div class="form-group">
                                    <label for="new_password_confirm">Last Name:</label>
                                    <input type="text" class="form-control" maxlength="40" name="last_name" placeholder="No last name entered..." value="<?php echo $last_name; ?>" />
                                 </div>
                              </div>
                              <div class="col-lg-4">
                                 <div class="form-group">
                                    <label for="new_password_confirm">Company Name:</label>
                                    <input type="text" class="form-control" maxlength="40" name="company_name" placeholder="No company name entered..." value="<?php echo $company_name; ?>" />
                                 </div>
                              </div>
                           </div>
                           <div class="row">
                              <div class="col-lg-6">
                                 <div class="form-group">
                                    <label for="new_password">Email Address:</label>
                                    <input type="text" class="form-control" maxlength="40" name="email_address" placeholder="No email address entered..." value="<?php echo $email_address; ?>" />
                                 </div>
                              </div>
                              <div class="col-lg-6">
                                 <div class="form-group">
                                    <label for="new_password_confirm">Cell:</label>
                                    <input type="text" class="form-control" maxlength="12" name="cell_number" placeholder="No cellphone number entered..." value="<?php echo $cell_number; ?>" />
                                 </div>
                              </div>
                           </div>

                           <input type="hidden" name="user_id" id="user_id" value="<?php echo $user_id; ?>">
                           <input type="hidden" name="account_id" id="account_id" value="<?php echo $user_account['user_account_id']; ?>">
                           <input type="hidden" name="service_id" id="service_id" value="<?php echo $user_account['user_default_service_id']; ?>">
                           <input type="hidden" name="update_details" id="update_details" value="1">
                           <a class="btn btn-block btn-social" onclick="$('#updateDetails').submit();" style="background-color: <?php echo $accRGB; ?>; color: #ffffff; border: 2px solid; border-radius: 6px !important; width: 200px;">
                              <i class="fa fa-save"></i> Update Account Details
                           </a>
                        </form>
                     </div>
                  </div>
               </div>
               <div class="col-lg-4">
                  <div class="box box-connet" style="padding-bottom:1px; border-top-color:<?php echo $accRGB; ?>; height:auto;" >
                     <div class="box-header">
                        <h3 class="box-title">Change Password for "<?php echo $user_account['user_username']; ?>"</h3>
                     </div>
                     <div class="box-body">

                     </div>
                  </div>
               </div>
            </div>

         <?php } ?>

      </div>
   </section>

</aside>

<!--  THE JAVASCRIPT IMPORT ALWAYS APPEARS BEFORE LOCAL JAVASCRIPT AT THE BOTTOM OF THE PAGE -->
<?php include("template_import_script.php"); //must import all scripts first  ?>
<!-- END JAVASCRIPT IMPORT -->

<script type="text/javascript">
<?php if (isset($_SESSION['userSystemAdmin']) && $_SESSION['userSystemAdmin'] == 1) { ?>
      $(function () {
         //assign a click listener to the forgot password link
         $(".btnMimic").on("click", function (e)
         {
            e.preventDefault();

            $(".loader").fadeIn("slow");
            $(".loaderIcon").fadeIn("slow");
            var user_id_to_mimic = $(this).attr('value');
            $(".btn_mimic").attr('disabled', 'true');

            $.post("../php/ajaxToggleMimic.php", {mimic_task: 'start', user_id_to_mimic: user_id_to_mimic}).done(function (raw_data) {
               var data = $.parseJSON(raw_data);
               if (data.success)
               {
                  alert('Mimic mode enabled!');
                  window.location = "../clienthome.php";
               } else
               {
                  alert('There was a problem with enabling mimic mode on this user, please contact the development department.');
                  window.location.reload();
               }

               $(".loader").fadeOut("slow");
               $(".loaderIcon").fadeOut("slow");
               $(".btn_mimic").removeAttr('disabled');
            }).fail(function (data) {
               alert('There was an error: ' + data + '');
               $(".btn_mimic").removeAttr('disabled');
               $(".loader").fadeOut("slow");
               $(".loaderIcon").fadeOut("slow");
            });
         });
      });
<?php } ?>

   function reloadOnSelect(userId)
   {
      $(".loader").fadeIn("slow");
      $(".loaderIcon").fadeIn("slow");
      window.location = "manage_users.php?service_id=<?php echo $service_id; ?>&user_id=" + userId;
   }
</script>

<script type="text/javascript">
   $(window).load(function () {
      $(".loader").fadeOut("slow");
      $(".loaderIcon").fadeOut("slow");
   })
</script>

<!-- Template Footer -->
