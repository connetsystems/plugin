<?php
/**
 * Include the header template which sets up the HTML
 *
 * Don't forget to include template_import_script before any Javascripting
 * Don't forget to include template footer.php at the bottom of the page as well
 */
$headerPageTitle = "Service Percent Messages"; //set the page title for the template import
TemplateHelper::initialize();

ini_set('memory_limit', '256M');

if (isset($_GET['type']) && $_GET['type'] != '') {
   $type = $_GET['type'];
}

if (isset($_GET['range'])) {
   $range = $_GET['range'];
   $dateArr = explode(' - ', $_GET['range']);
   $startDate = ($dateArr[0]);
   $endDate = ($dateArr[1]);
   if ($startDate == $endDate) {
      $endDate = $endDate + (24 * 60 * 60);
   }
   $startDate = $startDate * 1000;
   $endDate = $endDate * 1000;
}

/* echo '<br><br><br>S-------------'.$startDate;
  echo '<br>E-------------'.$endDate; */

$startDateConv = date('Y-m-d', ($startDate / 1000));
$endDateConv = date('Y-m-d', ($endDate / 1000));

if (isset($_GET['range1'])) {
   $range1 = $_GET['range1'];
   $dateArr1 = explode(' - ', $_GET['range1']);
   $startDate1 = strtotime($dateArr1[0]);
   $endDate1 = strtotime($dateArr1[1]);
   if ($startDate1 == $endDate1) {
      $endDate1 = $endDate1; // + (24*60*60);    
   }
   $startDate1 = $startDate1 * 1000;
   $endDate1 = ($endDate1 + (24 * 60 * 60) - 1) * 1000;
}

/* echo '<br><br><br>S-------------'.$startDate1;
  echo '<br>E-------------'.$endDate1; */

$startDateConv1 = date('Y-m-d', ($startDate1 / 1000));
$endDateConv1 = date('Y-m-d', ($endDate1 / 1000));

/* if(isset($_GET['sents']))
  {
  $units = $_GET['sents'];
  } */

if (isset($_GET['from'])) {
   $from = $_GET['from'];
   $starting = ($from - 1) * 100;
   //$from = $from - 1;
} else {
   $starting = 0;
}

if (isset($_GET['query']) && $_GET['query'] != '') {
   //$dlr
   //echo "<br><br><br>---BUERY-".$_GET['query'];
   //echo "<br>AND dlr:161 OR dlr:681";
   //echo "<br>-->".$qryStr;

   $qryArr = explode('.', $_GET['query']);
   $qryStr = 'AND (';
   for ($i = 0; $i < count($qryArr); $i++) {
      $qryStr .= ' dlr:' . $qryArr[$i] . ' OR';
   }
   $qryStr = substr($qryStr, 0, -3) . ')';
} else {
   $_GET['query'] = '';
   $qryStr = '';
}

if (isset($_GET['sort'])) {
   $sort = urldecode($_GET['sort']);
   $ordering = strstr($sort, ':');
   $ordering = trim($ordering, ':');
   $ordering = trim($ordering, '"');
   $sortUrl = urlencode($sort);
}

if (isset($_GET['searchTerm']) && $_GET['searchTerm'] != '') {
   $searchT = $_GET['searchTerm'];

   $searchTerm = ',{
                     "multi_match" : {
                        "query":    "' . $searchT . '",
                        "fields": ["mtsms_id", "src", "dest", "content", "mccmnc"]
                      }   
                    }';
} else {
   $searchT = '';
   $searchTerm = '';
}

if (isset($_GET['range'])) {
   $client = $_GET['client'];
   $client = substr($client, 0, -4);
   $cl = $_GET['client'];
   $clp = strrpos($cl, ' ');
   $cl = trim(substr($cl, $clp, strlen($cl)));
   $dsp = 'inline';
   $dir = '../img/serviceproviders';
   $serProviders = scandir($dir);
   $mcc = $_GET['mcc'];
   //$smsc = $_GET['smsc'];
   //echo '<br><br><br><br>br='..'=';

   $qry = '{
              "size":100,
              "from":' . $starting . ',
              "sort": {
                ' . $sort . '
              },
              "fields" : ["mtsms_id","timestamp","src","dest","content","dlr","mccmnc","rdnc"],
              "query": {
                "filtered": {
                  "query": {
                    "bool" : {
                        "must" : [
                            {
                             "query_string": {
                             "query": "service_id:' . $cl . ' AND mccmnc:' . $mcc . ' ' . $qryStr . '",
                             "analyze_wildcard": true
                                }
                            }
                            ' . $searchTerm . '
                        ]
                    }
                  },
                  "filter": {
                    "bool": {
                      "must": [
                        {
                          "range": {
                            "timestamp": {
                              "gte": ' . $startDate . ',
                              "lte": ' . $endDate . '
                            }
                          }
                        }
                      ],
                      "must_not": []
                    }
                  }
                }
              }
            }';


   $allStats = runRawMCoreElasticsearchQuery($qry);

   foreach ($allStats as $key => $value) {
      if ($key == 'hits') {
         $units = $value['total'];
      }
   }
}
?>

<aside class="right-side">
   <section class="content-header">
      <h1>
         Service Percent Messages
         <!--small>Control panel</small-->
      </h1>
   </section>

   <section class="content">
      <div class="box box-connet" style="padding-top:10px;padding-left:10px;padding-right:10px;padding-bottom:45px; border-top-color:<?php echo $accRGB; ?>;">

         <div class="callout callout-info" style="margin-bottom:10px;">
            <!--h4>Service Percent Messages Instructions / Details</h4>
            <p>This page will show you all service percent traffic SMS's for the date range that you specified in the previous page.</p-->
            <h4><?php echo $type; ?> Messages: <?php echo $client; ?></h4>
            <p>You are viewing all <?php echo ucfirst($type); ?> traffic SMS's for the date: <?php echo $startDateConv; ?></p>
         </div>

         <a class="btn btn-block btn-social btn-dropbox" name="nid" onclick="backToTraffic()" style="background-color:<?php echo $accRGB; ?>; border: 2px solid; border-radius: 6px !important; float:left; width: 100px;">
            <i class="fa fa-arrow-left"></i>Back
         </a>
      </div>
   </section>

   <section class="content">
      <div class="box box-connet" style="padding-top:1px; border-top-color:<?php echo $accRGB; ?>;">
         <div class="box-body">
            <div class="box-body table-responsive no-padding">
               <div class="row">
                  <div class="col-xs-11">
                     <div class="dataTables_filter" style="padding-top:6px;">
                        <label>Search: <input type="text" id="search_filter" aria-controls="example2"></label>
                     </div>
                  </div>
                  <div class="col-xs-1">
                     <a class="btn btn-block btn-social btn-dropbox" name="nid" onclick="searchText()" style="background-color:<?php echo $accRGB; ?>; border: 2px solid; border-radius: 6px !important; float:left; width: 100px;">
                        <i class="fa fa-search"></i>Search
                     </a>
                  </div>
               </div>
               <table id="example2"  class="table table-striped table-hover">
                  <thead>
                     <tr>
                        <?php
//echo '<br>-sort='.$sortUrl;
                        if (isset($_GET['controller'])) {
                           //echo '<br>0-order='.$ordering.'-';
                           if ($ordering == 'desc') {
                              $ordering = 'asc';
                              //echo '<br>1-order='.$ordering.'-';
                           } else {
                              $ordering = 'desc';
                              //echo '<br>2-order='.$ordering.'-';
                           }
                           //echo '<br>3-order='.$ordering.'-';
                        }
                        echo '<th style="width:100px;"><a href="../pages/servicepercentmsg.php?client=' . urlencode($_GET['client']) . '&range=' . $_GET['range'] . '&type=' . $_GET['type'] . '&range1=' . $_GET['range1'] . '&sents=' . $units . '&from=1&controller=1&query=' . $_GET['query'] . '&sort=' . '%22mtsms_id%22%3A%22' . $ordering . '%22' . '&mcc=' . $mcc . '">id</a></th>';
                        echo '<th style="width:100px;"><a href="../pages/servicepercentmsg.php?client=' . urlencode($_GET['client']) . '&range=' . $_GET['range'] . '&type=' . $_GET['type'] . '&range1=' . $_GET['range1'] . '&sents=' . $units . '&from=1&controller=1&query=' . $_GET['query'] . '&sort=' . '%22timestamp%22%3A%22' . $ordering . '%22' . '&mcc=' . $mcc . '">Date</a></th>';
                        echo '<th style="width:100px;"><a href="../pages/servicepercentmsg.php?client=' . urlencode($_GET['client']) . '&range=' . $_GET['range'] . '&type=' . $_GET['type'] . '&range1=' . $_GET['range1'] . '&sents=' . $units . '&from=1&controller=1&query=' . $_GET['query'] . '&sort=' . '%22src%22%3A%22' . $ordering . '%22' . '&mcc=' . $mcc . '">From</a></th>';
                        echo '<th style="width:100px;"><a href="../pages/servicepercentmsg.php?client=' . urlencode($_GET['client']) . '&range=' . $_GET['range'] . '&type=' . $_GET['type'] . '&range1=' . $_GET['range1'] . '&sents=' . $units . '&from=1&controller=1&query=' . $_GET['query'] . '&sort=' . '%22dest%22%3A%22' . $ordering . '%22' . '&mcc=' . $mcc . '">To</a></th>';
                        echo '<th>Content</th>';
                        echo '<th style="width:100px;"><a href="../pages/servicepercentmsg.php?client=' . urlencode($_GET['client']) . '&range=' . $_GET['range'] . '&type=' . $_GET['type'] . '&range1=' . $_GET['range1'] . '&sents=' . $units . '&from=1&controller=1&query=' . $_GET['query'] . '&sort=' . '%22mccmnc%22%3A%22' . $ordering . '%22' . '&mcc=' . $mcc . '">Network</a></th>';
//echo '<th style="width:100px;"><a href="../pages/servicepercentmsg.php?client='.$_GET['client'].'&range='.$_GET['range'].'&type='.$_GET['type'].'&range1='.$_GET['range1'].'&sents='.$units.'&from=1&controller=1&query='.$_GET['query'].'&sort='.'%22smsc%22%3A%22'.$ordering.'%22'.'&mcc='.$mcc.'">SMSC</a></th>';
                        echo '<th style="width:100px;"><a href="../pages/servicepercentmsg.php?client=' . urlencode($_GET['client']) . '&range=' . $_GET['range'] . '&type=' . $_GET['type'] . '&range1=' . $_GET['range1'] . '&sents=' . $units . '&from=1&controller=1&query=' . $_GET['query'] . '&sort=' . '%22dlr%22%3A%22' . $ordering . '%22' . '&mcc=' . $mcc . '">Status</a></th>';
                        ?>
                     </tr>
                  </thead>
                  <tbody>
                     <?php
                     foreach ($allStats as $key => $value) {
                        if ($key == 'hits') {
                           $hits = count($value['hits']);
                           for ($i = 0; $i < $hits; $i++) {
                              $netw = getNetFromMCC($value['hits'][$i]['fields']['mccmnc']['0']);
                              //$smsc = $value['hits'][$i]['fields']['smsc']['0'];
                              //$smsc2 = $value['hits'][$i]['fields']['smsc']['0'];
                              //$posSmsc = strrpos($smsc, '_');
                              //$smsc = ucfirst(substr($smsc, 0, $posSmsc));
                              $time = str_replace('T', ' ', $value['hits'][$i]['fields']['timestamp']['0']);
                              $time = strstr($time, '+', true);
                              $timeSec = strrpos($time, ':');
                              $time = substr($time, 0, $timeSec);

                              echo '<tr>';
                              echo '<td>' . $value['hits'][$i]['fields']['mtsms_id']['0'] . '</td>';
                              echo '<td>' . $time . '</td>';
                              echo '<td>' . $value['hits'][$i]['fields']['src']['0'] . '</td>';
                              echo '<td>' . $value['hits'][$i]['fields']['dest']['0'] . '</td>';
                              echo '<td>' . preg_replace('/[^(\x20-\x7F)]*/', '', $value['hits'][$i]['fields']['content']['0']) . '</td>';

                              $counter = 0;
                              foreach ($serProviders as $key3 => $value3) {
                                 $value3 = strstr($value3, '.', true);

                                 if (!empty($value3) && strpos($netw, $value3) !== false) {
                                    echo "<td style='vertical-align:middle;width:180px;'><img src='../img/serviceproviders/" . $value3 . ".png'>&nbsp;&nbsp;" . ucfirst($netw) . ' (' . $value['hits'][$i]['fields']['mccmnc']['0'] . ')</td>';
                                    //echo '<td>'.$netw.'</td>';
                                    $counter++;
                                    break;
                                 }
                              }
                              if ($counter == 0) {
                                 echo "<td style='vertical-align:middle;width:180px;'><img src='../img/serviceproviders/Special.png'>&nbsp;&nbsp;" . ucfirst($netw) . ' (' . $value['hits'][$i]['fields']['mccmnc']['0'] . ')</td>';
                              }

                              $counter = 0;
                              /* foreach ($serProviders as $key3 => $value3)
                                {
                                if($value3 == (lcfirst($smsc).'.png'))
                                {
                                echo "<td style='vertical-align:middle;width:120px;'><img src='../img/serviceproviders/".lcfirst($smsc).".png'>&nbsp;&nbsp;".$smsc.'</td>';
                                $counter++;
                                }
                                }
                                if($counter == 0)
                                {
                                echo "<td style='vertical-align:middle;width:120px;'><img src='../img/serviceproviders/Special.png'>&nbsp;&nbsp;".$smsc.'</td>';
                                } */

                              $dlrPos = $value['hits'][$i]['fields']['dlr']['0'];
                              if (($dlrPos & 1) == 1) {
                                 echo '<td style="vertical-align:middle;color:#009900;">Delivered</td>';
                              } elseif (($dlrPos & 32) == 32 && ($dlrPos & 1) == 0 && ($dlrPos & 2) == 0 && ($dlrPos & 16) == 0) {
                                 echo '<td style="vertical-align:middle;color:#eec00d;">Pending</td>';
                              } elseif (($dlrPos & 2) == 2) {
                                 echo '<td style="vertical-align:middle;color:#cb0234;">Failed</td>';
                              } elseif (($dlrPos & 16) == 16) {
                                 echo '<td style="vertical-align:middle;color:#cb0234;">Rejected</td>';
                              } elseif ($dlrPos == 4288) {
                                 echo '<td style="vertical-align:middle;color:#223322;">Excluded</td>';
                              } else {
                                 echo '<td>' . $dlrPos . '</td>';
                              }
                              echo '</tr>';
                           }
                        }
                     }
                     ?>
                  </tbody>
               </table>
               <div class="row">
                  <div class="col-xs-6">
                     <div class="dataTables_info" id="example2_info">Showing <?php echo ($starting); ?> to <?php
                        if (($from * 100) < $units) {
                           echo ($from * 100);
                        } else {
                           echo $units;
                        }
                        ?> of <?php echo $units; ?> entries
                     </div>
                  </div>
                  <div class="col-xs-6">
                     <div class="dataTables_paginate paging_bootstrap">
                        <ul class="pagination">
                           <?php
                           if ($from > 1) {
                              echo '<li class="prev">';
                              echo '<a href="../pages/servicepercentmsg.php?client=' . urlencode($_GET['client']) . '&range=' . $_GET['range'] . '&type=' . $_GET['type'] . '&range1=' . $_GET['range1'] . '&mcc=' . $mcc . '&sents=' . $units . '&searchTerm=' . $searchT . '&from=1&sort=' . $sortUrl . '&query=' . $_GET['query'] . '">&#8592; First</a>';
                              echo '</li>';
                              if ($from > 2) {
                                 echo '<li>';
                                 echo '<a href="../pages/servicepercentmsg.php?client=' . urlencode($_GET['client']) . '&range=' . $_GET['range'] . '&type=' . $_GET['type'] . '&range1=' . $_GET['range1'] . '&mcc=' . $mcc . '&sents=' . $units . '&searchTerm=' . $searchT . '&from=' . ($from - 2) . '&sort=' . $sortUrl . '&query=' . $_GET['query'] . '">' . ($from - 2) . '</a>';
                                 echo '</li>';
                              }
                              echo '<li>';
                              echo '<a href="../pages/servicepercentmsg.php?client=' . urlencode($_GET['client']) . '&range=' . $_GET['range'] . '&type=' . $_GET['type'] . '&range1=' . $_GET['range1'] . '&mcc=' . $mcc . '&sents=' . $units . '&searchTerm=' . $searchT . '&from=' . ($from - 1) . '&sort=' . $sortUrl . '&query=' . $_GET['query'] . '">' . ($from - 1) . '</a>';
                              echo '</li>';
                           }
                           echo '<li class="active">';
                           echo '<a href="../pages/servicepercentmsg.php?client=' . urlencode($_GET['client']) . '&range=' . $_GET['range'] . '&type=' . $_GET['type'] . '&range1=' . $_GET['range1'] . '&mcc=' . $mcc . '&sents=' . $units . '&searchTerm=' . $searchT . '&from=' . $from . '&sort=' . $sortUrl . '&query=' . $_GET['query'] . '">' . $from . '</a>';
                           echo '</li>';
                           if (($from * 100) < $units) {
                              echo '<li>';
                              echo '<a href="../pages/servicepercentmsg.php?client=' . urlencode($_GET['client']) . '&range=' . $_GET['range'] . '&type=' . $_GET['type'] . '&range1=' . $_GET['range1'] . '&mcc=' . $mcc . '&sents=' . $units . '&searchTerm=' . $searchT . '&from=' . ($from + 1) . '&sort=' . $sortUrl . '&query=' . $_GET['query'] . '">' . ($from + 1) . '</a>';
                              echo '</li>';
                              if ((($from + 1) * 100) < $units) {
                                 echo '<li>';
                                 echo '<a href="../pages/servicepercentmsg.php?client=' . urlencode($_GET['client']) . '&range=' . $_GET['range'] . '&type=' . $_GET['type'] . '&range1=' . $_GET['range1'] . '&mcc=' . $mcc . '&sents=' . $units . '&searchTerm=' . $searchT . '&from=' . ($from + 2) . '&sort=' . $sortUrl . '&query=' . $_GET['query'] . '">' . ($from + 2) . '</a>';
                                 echo '</li>';
                              }
                              echo '<li class="next">';
                              echo '<a href="../pages/servicepercentmsg.php?client=' . urlencode($_GET['client']) . '&range=' . $_GET['range'] . '&type=' . $_GET['type'] . '&range1=' . $_GET['range1'] . '&mcc=' . $mcc . '&sents=' . $units . '&searchTerm=' . $searchT . '&from=' . (floor($units / 100) + 1) . '&sort=' . $sortUrl . '&query=' . $_GET['query'] . '">Last &#8594;</a>';
                              echo '</li>';
                           }
                           ?>
                        </ul>
                     </div>
                  </div>
               </div>
               <div class="box-body">
                  <div class="form-group" style="padding-top:30px">
                     <a href="../php/createCSVNet.php?repName=Service_Report&net=1&cl=<?php echo $cl; ?>&range=<?php echo $range; ?>&query=<?php echo $_GET['query']; ?>&searchTerm=<?php echo $searchT; ?>&mcc=<?php echo $mcc; ?>&total=<?php echo $units; ?>" class="btn btn-block btn-social btn-dropbox" style="background-color:<?php echo $accRGB; ?>; border: 2px solid; border-radius: 6px !important; float:left; width: 134px; margin-top:-21px;">
                        <i class="fa fa-save"></i>Save Report
                     </a>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </section>
</aside>

<!--  THE JAVASCRIPT IMPORT ALWAYS APPEARS BEFORE LOCAL JAVASCRIPT AT THE BOTTOM OF THE PAGE -->
<?php include("template_import_script.php"); //must import all scripts first  ?>
<!-- END JAVASCRIPT IMPORT -->

<script type="text/javascript">
   /* $(function() 
    {
    $('#example2').dataTable({
    
    "bPaginate": false,
    "bLengthChange": false,
    "bFilter": false,
    "bSort": false,
    "bInfo": false,
    "bAutoWidth": false,
    "iDisplayLength": 100,
    "aoColumns": [
    null,
    null,
    null,
    null,
    null,
    null,
    null,
    null
    ],
    });
    });
    
    jQuery.extend( jQuery.fn.dataTableExt.oSort, 
    {
    "title-numeric-pre": function ( a ) {
    var x = a.match(/title="*(-?[0-9\.]+)/)[1];
    return parseFloat( x );
    },
    
    "title-numeric-asc": function ( a, b ) {
    return ((a < b) ? -1 : ((a > b) ? 1 : 0));
    },
    
    "title-numeric-desc": function ( a, b ) {
    return ((a < b) ? 1 : ((a > b) ? -1 : 0));
    }
    } );
    
    jQuery.extend( jQuery.fn.dataTableExt.oSort, 
    {
    "formatted-num-pre": function ( a ) {
    a = (a === "-" || a === "") ? 0 : a.replace( /[^\d\-\.]/g, "" );
    return parseFloat( a );
    },
    
    "formatted-num-asc": function ( a, b ) {
    return a - b;
    },
    
    "formatted-num-desc": function ( a, b ) {
    return b - a;
    }
    } );*/
</script>

<script type="text/javascript">
   function searchText()
   {

      var repRange = "<?php echo urlencode($_GET['range']); ?>";
      var repRange1 = "<?php echo urlencode($_GET['range1']); ?>";
      var searchT = document.getElementById('search_filter').value;
      var client = "<?php echo $_GET['client']; ?>";
      //alert(searchT);
      //window.location = "../pages/apitrafficadmin.php?client=" + serviceName + "&range=" + repRange;
      //window.location = "../pages/servicepercentmsg.php?range=" + repRange + "&range1=" + repRange1 + "&searchTerm=" + searchT + "&sents=<?php echo $units; ?>" + "&from=1&controller=1&sort=<?php echo $sortUrl; ?>" + "&query=<?php echo $_GET['query']; ?>" + "&mcc=<?php echo $_GET['mcc']; ?>" + "&smsc=<?php echo $_GET['smsc']; ?>";
      encodeURI(client);
      var t = "<?php echo $_GET['type']; ?>";
      window.location = "../pages/servicepercentmsg.php?client=" + encodeURIComponent(client) + "&type=" + t + "&range=" + repRange + "&range1=" + repRange1 + "&searchTerm=" + searchT + "&sents=<?php echo $units; ?>" + "&from=1&controller=1&sort=<?php echo $sortUrl; ?>" + "&query=<?php echo $_GET['query']; ?>" + "&mcc=<?php echo $_GET['mcc']; ?>";
   }

   function backToTraffic()
   {

      repRange = "<?php echo urlencode($startDate1 . ' - ' . $endDate1); ?>";
      var client = "<?php echo $_GET['client']; ?>";
      //window.location = "../pages/servicepercent.php?range=" + repRange;
      encodeURI(client);
      window.location = "../pages/servicepercent.php?range=" + repRange + "&client=" + encodeURIComponent(client);
   }
</script>

<script type="text/javascript">
   $(window).load(function () {
      $(".loader").fadeOut("slow");
      $(".loaderIcon").fadeOut("slow");
   })
</script>

<!-- Template Footer -->

