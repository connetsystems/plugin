<?php
/**
 * Include the header template which sets up the HTML
 *
 * Don't forget to include template_import_script before any Javascripting
 * Don't forget to include template footer.php at the bottom of the page as well
 */
$headerPageTitle = "Collections - Europe"; //set the page title for the template import
TemplateHelper::initialize();

$startDate = date('Y-m-d');
$endDate = date("Y-m-d H:i:s");

//echo ''.$startDate;
//$colData = getCollectionsAfrica($startDate, $endDate);
//Middle East & Africa Collections
$colData = getCollectionsInternational(96);

/* foreach ($colData as $key => $value) {
  # code...
  echo '<pre>';
  print_r($value);
  echo '</pre>';
  } */

function get_currency($from_Currency, $to_Currency, $amount) {
   $amount = urlencode($amount);
   $from_Currency = urlencode($from_Currency);
   $to_Currency = urlencode($to_Currency);

   $url = "http://www.google.com/finance/converter?a=$amount&from=$from_Currency&to=$to_Currency";

   $ch = curl_init();
   $timeout = 0;
   curl_setopt($ch, CURLOPT_URL, $url);
   curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);

   curl_setopt($ch, CURLOPT_USERAGENT, "Mozilla/4.0 (compatible; MSIE 8.0; Windows NT 6.1)");
   curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, $timeout);
   $rawdata = curl_exec($ch);
   curl_close($ch);
   $data = explode('bld>', $rawdata);
   $data = explode($to_Currency, $data[1]);

   return round($data[0], 2);
}

// Call the function to get the currency converted
$exrate = get_currency('EUR', 'ZAR', 1);
?>

<aside class="right-side">
   <section class="content-header">
      <h1>
         Europe - 1 Euro - ZAR : R <?php echo $exrate; ?>
      </h1>
   </section>

   <section class="content">
      <div class="box-body col-md-12" style="margin-top:0px;">
         <div class="panel box box-warning" style="padding-bottom:1px; border-top-color:<?php echo $accRGB; ?>;">
            <div class="box-body">
               <div class="box-body table-responsive no-padding">
                  <table id="routesTable" class="table table-hover table-bordered table-striped">
                     <thead>
                        <tr role="row">
                           <th>ID</th>
                           <th>Country</th>
                           <th>Network</th>
                           <th>Default Route</th>
                           <th colspan="2">Price</th>
                           <th>Nexmo</th>
                           <th>Silver</th>
                           <th>Clx</th>
                           <th>Portal</th>
                           <th colspan="2">Price R</th>
                           <th>Date</th>
                        </tr>
                     </thead>
                     <tbody>
                        <?php
                        $k = 0;
                        $oldName = '';
                        $countColData = 0;
                        foreach ($colData as $key => $value) {
                           $tot = 0;

                           if ($oldName != $value['route_collection_name']) {
                              $oldName = $value['route_collection_name'];
                           }
                        }

                        $tempCol = "";


                        $ks = 0;
                        $countrynew = "";
                        foreach ($colData as $key => $value) {



                           $country = $value['prefix_country_name'];
                           /* if($countrynew == "" )
                             {

                             }
                             else */
                           //if($key%4 == 0 && $key != 0)
                           if ($country != $countrynew) {
                              if ($countrynew != '') {
                                 echo '<tr style="height:20px;">';
                                 echo '<td></td>';
                                 echo '<td></td>';
                                 echo '<td></td>';
                                 echo '<td></td>';
                                 echo '<td></td>';
                                 echo '</tr>';
                              }
                              $countrynew = $country;

                              //$ks++;
                           }


                           echo '<tr>';

                           //$emptyRow++;
                           echo '<td style="vertical-align:middle;width:50px;">' . $value['cdr_id'] . '</td>';



                           if (substr($value['prefix_country_name'], 0, 3) == "USA") {
                              $flag = "USA";
                           } else {
                              $flag = $value['prefix_country_name'];
                           }
                           $flag = str_replace('/', '&', $flag);

                           //echo '<td>'.$value['prefix_country_name'].'</td>';
                           echo '<td style="vertical-align:middle;width:300px;"><img src="../img/flags/' . $flag . '.png">&nbsp;&nbsp;' . $value['prefix_country_name'] . '</td>';



                           $logo = trim(strstr($value['prefix_network_name'], '(', true));

                           //echo '<td style="vertical-align:middle;width:300px;"><img src="../img/serviceproviders/'.$logo.'.png">&nbsp;&nbsp;'.$value['prefix_network_name'].'</td>';

                           echo '<td style="vertical-align:middle;width:300px;"><img src="../img/serviceproviders/' . $logo . '.png">&nbsp;&nbsp;' . $value['prefix_network_name'] . '</td>';


                           //$routeIcon = strstr($value['default_route'], '_', true);
                           $backgroundcolor = "ffffff";
                           if ($value['default_route'] == "silverstreet_smsc") {
                              //echo '<td style="vertical-align:middle;background-color:#fce0ed;width:200px;">'.$value['default_route'].'</td>';
                              $backgroundcolor = "fce0ed";
                           } elseif ($value['default_route'] == "clx_smsc") {
                              //echo '<td style="vertical-align:middle;background-color:#dde5e7;width:200px;">'.$value['default_route'].'</td>';
                              $backgroundcolor = "dde5e7";
                           } elseif ($value['default_route'] == "nexmo_smsc") {
                              //echo '<td style="vertical-align:middle;background-color:#e3f8ff;width:200px;">'.$value['default_route'].'</td>';
                              $backgroundcolor = "e3f8ff";
                           } elseif ($value['default_route'] == "portal_smsc") {
                              //echo '<td style="vertical-align:middle;background-color:#b9ffc6;width:200px;">'.$value['default_route'].'</td>';
                              $backgroundcolor = "b9ffc6";
                           } elseif ($value['default_route'] == "clxsignal_smsc") {
                              //echo '<td style="vertical-align:middle;background-color:#b9ffc6;width:200px;">'.$value['default_route'].'</td>';
                              $backgroundcolor = "fcfac2";
                           } else {
                              //echo '<td style="vertical-align:middle;background-color:#ffffff;width:200px;">'.$value['default_route'].'</td>';
                              $backgroundcolor = "ffffff";
                           }
                           echo '<td style="vertical-align:middle;background-color:#' . $backgroundcolor . ';width:200px;">' . $value['default_route'] . '</td>';
                           //}

                           $valueTemp = ' ' . $value['cdr_comments'];

                           if ((strpos($valueTemp, 'eur') != false) || $value['cdr_comments'] == 'eur') {
                              echo '<td style="color:#3333dd;vertical-align:middle; text-align:center;width:5px;background-color:#' . $backgroundcolor . ';"><b><i class="fa fa-eur"></i></b></td>';
                           } elseif ((strpos($valueTemp, 'usd') != false) || $value['cdr_comments'] == 'usd') {
                              echo '<td style="color:#17852c;vertical-align:middle; text-align:center;width:5px;background-color:#' . $backgroundcolor . ';"><b><i class="fa fa-usd"></i></b></td>';
                           } elseif ((strpos($valueTemp, 'gbp') != false) || $value['cdr_comments'] == 'gbp') {
                              echo '<td style="color:#927119;vertical-align:middle; text-align:center;width:5px;background-color:#' . $backgroundcolor . ';"><b><i class="fa fa-gbp"></i></b></td>';
                           } else {
                              echo '<td style="color:#993333;vertical-align:middle; text-align:center;width:5px;background-color:#' . $backgroundcolor . ';"><b>R</b></td>';
                           }
                           echo '<td style="vertical-align:middle;text-align:right;width:30px;background-color:#' . $backgroundcolor . ';">' . number_format(($value['cdr_cost_per_unit'] / 10000), 4, '.', '') . '</td>';



                           if (number_format(($value['nexmo_price'] / 10000), 4, '.', '') == '0.0000') {
                              echo '<td style="vertical-align:middle;text-align:right;width:30px;color:#ff1111;background-color:#e3f8ff;">N/A</td>';
                           } else {
                              echo '<td style="vertical-align:middle;text-align:right;width:30px;background-color:#e3f8ff;">' . number_format(($value['nexmo_price'] / 10000), 4, '.', '') . '</td>';
                           }

                           if (number_format(($value['silverstreet_price'] / 10000), 4, '.', '') == '0.0000') {
                              echo '<td style="vertical-align:middle;text-align:right;width:30px;color:#ff1111;background-color:#fce0ed;">N/A</td>';
                           } else {
                              echo '<td style="vertical-align:middle;text-align:right;width:30px;background-color:#fce0ed;">' . number_format(($value['silverstreet_price'] / 10000), 4, '.', '') . '</td>';
                           }

                           if (number_format(($value['clx_price'] / 10000), 4, '.', '') == '0.0000') {
                              echo '<td style="vertical-align:middle;text-align:right;width:30px;color:#ff1111;background-color:#dde5e7;">N/A</td>';
                           } else {
                              echo '<td style="vertical-align:middle;text-align:right;width:30px;background-color:#dde5e7;">' . number_format(($value['clx_price'] / 10000), 4, '.', '') . '</td>';
                           }





                           echo '<td style="vertical-align:middle;text-align:right;width:30px;color:#ff1111;background-color:#b9ffc6;">N/A</td>';

                           echo '<td style="color:#993333;vertical-align:middle; text-align:center;width:5px;"><b>R</b></td>';
                           if ((strpos($valueTemp, 'eur') != false) || $value['cdr_comments'] == 'eur') {
                              echo '<td style="vertical-align:middle;text-align:right;width:30px;">' . number_format(($value['cdr_cost_per_unit'] / 10000) * $exrate, 4, '.', '') . '</td>';
                           } else {
                              echo '<td style="vertical-align:middle;text-align:right;width:30px;">' . number_format(($value['cdr_cost_per_unit'] / 10000), 4, '.', '') . '</td>';
                           }
                           echo '<td style="vertical-align:middle;text-align:right;width:80px;">' . $value['cdr_effective_date'] . '</td>';




                           echo '</tr>';
                        }
                        ?>
                     </tbody>
                  </table>
               </div><!-- /.box-body -->
            </div>
         </div>
      </div>
   </section>
</aside>

<!--  THE JAVASCRIPT IMPORT ALWAYS APPEARS BEFORE LOCAL JAVASCRIPT AT THE BOTTOM OF THE PAGE -->
<?php include("template_import_script.php"); //must import all scripts first ?>
<!-- END JAVASCRIPT IMPORT -->

<script type="text/javascript">
   $(window).load(function () {
      $(".loader").fadeOut("slow");
      $(".loaderIcon").fadeOut("slow");
   })
</script>

<!-- Template Footer -->
