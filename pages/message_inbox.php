<?php
/**
 * Include the header template which sets up the HTML
 *
 * Don't forget to include template_import_script before any Javascripting
 * Don't forget to include template footer.php at the bottom of the page as well
 */
$headerPageTitle = "Message Inbox"; //set the page title for the template import
TemplateHelper::initialize();

$show_service_selector = 'block;';

//-------------------------------------------------------------
// Permissions
//-------------------------------------------------------------
$clients = PermissionsHelper::getAllServicesWithPermissions();

if (isset($_GET['service_id'])) {

   if (!isset($clients[(integer) $_GET['service_id']])) {
      header('location: ' . $_SERVER['DOCUMENT_URI']);
      die();
   }
}

if (count($clients) == 1)
{
   $_GET['service_id'] = LoginHelper::getCurrentServiceId();
   $show_service_selector = 'none;';
}
elseif (!isset($_GET['service_id']))
{
   $_GET['service_id'] = LoginHelper::getCurrentServiceId();
}


//------------------------------------------------------------
// PULL THE CONVERSATIONS
//------------------------------------------------------------
//check the service ID reference
if(isset($_GET['service_id']))
{
   $service_id = $_GET['service_id'];
}
else
{
   $service_id = $_SESSION['service_id'];
}

if(isset($_GET['start_date']) && isset($_GET['end_date']))
{
   $start_date = $_GET['start_date'];
   $end_date = $_GET['end_date'];
}
else
{
   $start_date = date('Y-m-d'); //default
   $end_date = date('Y-m-d'); //default
}

//check for a search query
if (isset($_GET['search_type']) && isset($_GET['search_string']) && $_GET['search_string'] != "")
{
   $search_type = $_GET['search_type'];
   $search_string = $_GET['search_string'];
   $is_search = true;
}
else
{
   $is_search = false;
   $search_type = "mosms_content";
   $search_type_display = 'MSOSMS Content';
   $search_string = "";
}

//convert to unix for elasticsearch
$start_date_timestamp = new DateTime($start_date);
$end_date_timestamp = new DateTime($end_date);

//for paging
$records_per_page = 25;
if(isset($_GET['page']))
{
   $page = $_GET['page'];
}
else
{
   $page = 1; //default
}

//calculate the from from the page and size vars
$from = ($page - 1) * $records_per_page;


//figure out what get variables we need to add to these URLs
if ($is_search)
{
   switch($search_type)
   {
      case 'mosms_content' :
         $search_type_display = 'MSOSMS Content';
         $mo_results = MOSMS::searchMOSMSForArray('content', $search_string, $service_id, $from, $records_per_page, $start_date, $end_date);
         break;
      case 'mosms_source' :
         $search_type_display = 'MSOSMS Source (MSISDN)';
         $mo_results = MOSMS::searchMOSMSForArray('src', $search_string, $service_id, $from, $records_per_page, $start_date, $end_date);
         break;
   }
   //print_r($mo_results);

   $base_page_url = 'message_inbox.php?service_id='.$service_id.'&start_date='.$start_date.'&end_date='.$end_date.'&search_type=' . urlencode($search_type) . '&search_string=' . urlencode($search_string);
   $csv_url = '../php/createCSVMessageInbox.php?service_id='.$service_id.'&start_date='.$start_date.'&end_date='.$end_date.'&search_type=' . urlencode($search_type) . '&search_string=' . urlencode($search_string);
}
else
{
   //no get all the MOs for the page
   $mo_results = MOSMS::getAllByServiceId($service_id, $from, $records_per_page, $start_date, $end_date);
   $base_page_url = 'message_inbox.php?service_id='.$service_id.'&start_date='.$start_date.'&end_date='.$end_date;
   $csv_url = '../php/createCSVMessageInbox.php?service_id='.$service_id.'&start_date='.$start_date.'&end_date='.$end_date;
}

$total_mos_in_search = $mo_results['total_search_hits'];
$mo_list = $mo_results['objects'];

//cycle through the MOs and get the associated MTSMS records
foreach($mo_list as $key => $mo_sms)
{
   //echo "<br/>".$mo_sms->meta['mtsms_id']."<br/>";
   //get hte MTSMS information and attach it to the MOSMS
   $mo_mtsms = MTSMS::find($mo_sms->meta['mtsms_id']);
   $mo_sms->mtsms = $mo_mtsms;

   //get the batch data
   $mo_batch = getBatchData($mo_mtsms->meta['batch_id']);

   if(isset($mo_batch) && isset($mo_batch['batch_id']))
   {
      $mo_sms->batch = $mo_batch;
   }

   $mo_list[$key] = $mo_sms;
}

//our counter for the count column
$page_mo_count = $from;

?>

<aside class="right-side">
   <section class="content-header">
      <h1>
         Message Inbox
         <!--small>Control panel</small-->
      </h1>
   </section>

   <section class="content" style="padding-bottom:0px;">
      <div class="box box-solid" style="padding:10px;margin-bottom:5px;margin-top:5px;">
         <div class="row">
            <div class="col-lg-12">
               <div class="form-group" style="display:<?php echo $show_service_selector; ?>">
                  <label>Client List</label>
                  <select class="form-control" name="client" id="client" form="clientList" OnChange="reloadOnSelect(this.value);">
                     <?php
                        if ($service_id == -1) {
                           echo '<option value="-1" SELECTED>Please select a client...</option>';
                        }
                        foreach ($clients as $client) {
                           if ($service_id == $client['service_id']) {
                              echo "<option value='" . $client['service_id'] . "' SELECTED>" . $client['account_name'] . " - " . $client['service_name'] . " - " . $client['service_id'] . "</option>";
                           } else {
                              echo "<option value='" . $client['service_id'] . "'>" . $client['account_name'] . " - " . $client['service_name'] . " - " . $client['service_id'] . "</option>";
                           }
                        }
                     ?>
                  </select>
               </div>
            </div>
         </div>
         <div class="row">
            <div class="col-lg-6">
               <!-- THE DATE SELECTOR -->
               <label>Select Date Range</label><br>
               <div id="reportrange" class="select pull-left" onmouseover="" style="cursor: pointer;">
                  <i class="fa fa-calendar fa-lg"></i>
                  <span style="font-size:15px"><?php echo $start_date . " - " . $end_date; ?></span><b class="caret"></b>
               </div>
            </div>
            <div class="col-lg-6">
               <!-- FOR SEARCHING AND FILTERING -->
               <label>Search Message Inbox</label><br>
               <div class="input-group input-group-sm">
                  <div class="input-group-btn">
                     <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" id="btnSearchType" search_type="<?php echo $search_type; ?>"><?php echo $search_type_display; ?> <span class="caret"></span></button>
                     <ul class="dropdown-menu">
                        <li><a href="#" class="btnSetSearchType" search_type="mosms_content">MOSMS Content</a></li>
                        <li><a href="#" class="btnSetSearchType" search_type="mosms_source">MOSMS Source (MSISDN)</a></li>
                     </ul>
                  </div>
                  <input type="text" class="form-control" id="inputSearchField" value="<?php echo $search_string; ?>">
                  <span class="input-group-btn">
                     <button class="btn btn-default" type="button" id="btnSearchMessageInbox">Search</button>
                  </span>
               </div>
            </div>
         </div>
      </div>

      <br/>
      <?php if(count($mo_list) == 0) { ?>
      <div class="box box-connet" style="padding-bottom:5px; border-top-color:<?php echo $accRGB; ?>;">
         <div class="box-body" style="padding-top:5px; padding-bottom:0px; text-align:center;">
            <?php if($is_search) { ?>
               <p>No results match you query. Please check your search type, and try to use complete words and numbers, such as "STOP" or "27835556666".</p>
            <?php } else { ?>
               <p>There are no messages in this inbox.</p>
            <?php } ?>
         </div>
      </div>
      <?php } ?>
      <?php foreach($mo_list as $mo_sms) { ?>
         <div class="box box-connet" style="padding-bottom:5px; border-top-color:<?php echo $accRGB; ?>;">
            <div class="box-body" style="padding-top:5px; padding-bottom:0px;">
               <div class="row">
                  <div class="col-lg-9">
                     <div class="row">
                        <div class="col-lg-6">
                           <h5 style="color:<?php echo $accRGB; ?>; margin:5px 0px;"><?php echo ( ++ $page_mo_count); ?> | <i class="fa fa-mobile"></i> Reply Message (MOSMS)</h5>
                           <h3 style="margin:8px 2px 8px 15px;"><i><?php echo ($mo_sms->content != "" ? '"'.$mo_sms->content.'"' : '<span class="text-warning">-blank MO-</span>'); ?></i></h3>
                        </div>
                        <div class="col-lg-6">
                           <h5 style="color:<?php echo $accRGB; ?>; margin:5px 0px;"><i class="fa fa-send"></i> Original Outgoing Message (MTSMS)</h5>
                           <p style="margin:8px 2px 8px 15px;">
                              <strong id="mo_mtsms_partial_text<?php echo $mo_sms->mosms_id; ?>"><i>"<span><?php echo htmlentities(substr($mo_sms->mtsms->content, 0, 50)).'...'; ?>"</i></strong>
                              <strong id="mo_mtsms_full_text<?php echo $mo_sms->mosms_id; ?>" style="display:none;"><i>"<span><?php echo htmlentities($mo_sms->mtsms->content); ?></span>"</i></strong> -
                              <a href="#" class="show-more" mosms_id="<?php echo $mo_sms->mosms_id; ?>">[More]</a>
                           </p>
                        </div>
                     </div>
                     <div class="row">
                        <div class="col-lg-6">
                           <p>
                              <small style="float:left;">Source: <strong class="text-info"><?php echo $mo_sms->src; ?></strong></small>
                              <small style="float:right;" class="text-">Time Received: <strong><?php echo str_replace('T', ' ', strstr($mo_sms->timestamp, '+', true)); ?></strong></small>
                           </p>
                        </div>
                        <div class="col-lg-6">
                           <p>
                              <small style="float:left;" >Source: <strong class="text-info"><?php echo $mo_sms->mtsms->src; ?></strong></small>
                              <small style="float:right;">Time Sent: <strong><?php echo str_replace('T', ' ', strstr($mo_sms->mtsms->timestamp, '+', true)); ?></strong></small>
                           </p>
                        </div>
                     </div>
                  </div>
                  <div class="col-lg-3">
                     <?php if(isset($mo_sms->batch['batch_id'])) { ?>
                        <div class="row">
                           <div class="col-lg-6">
                              <h5 style="color:<?php echo $accRGB; ?>; margin:5px 0px;"><i class="fa fa-rocket"></i> Batch Info</h5>
                              <p>
                                 Batch ID: <strong><?php echo $mo_sms->batch['batch_id']; ?></strong><br/>
                                 Batch Name: <strong><?php echo $mo_sms->batch['batch_reference']; ?></strong><br/>
                                 <small>Date: <strong><?php echo $mo_sms->batch['batch_created']; ?></strong></small>
                              </p>
                           </div>
                           <div class="col-lg-6">
                              <a href="batchlist.php?service_id=<?php echo $service_id; ?>&start_date=<?php echo $start_date_timestamp->getTimestamp() * 1000; ?>&end_date=<?php echo $end_date_timestamp->getTimestamp() * 1000; ?>" class="btn btn-primary" style="margin-top:10px; float:right;">
                                 <i class="fa fa-rocket" style="font-size:20pt"></i><br/>
                                 View In<br/> Batch List
                              </a>
                           </div>
                        </div>
                     <?php } else { ?>
                        <h5><i class="fa fa-rocket"></i> No batch info exists for this MO.</h5>
                     <?php } ?>
                  </div>
               </div>
            </div>
         </div>
      <?php } ?>

      <!-- PAGINATION GOES HERE -->
      <div class="row">
         <div class="col-lg-12" style="text-align: right;">
            <?php if(count($mo_list) > 0) { ?>
            <p>Showing
               <strong><?php echo $from + 1; ?></strong> to
               <strong><?php echo $page_mo_count; ?></strong> of a total
               <strong><?php echo $total_mos_in_search; ?> MOSMS</strong> records.
            </p>
            <?php } ?>
            <nav>
               <ul class="pagination" style="text-align: left;">
                  <?php
                  if ($total_mos_in_search != 0) {

                     /**
                      * PAGINATION CODE IS HERE
                      */
                     $total_pages = ceil($total_mos_in_search / $records_per_page);

                     if ($page != 1) {
                        echo '<li>';
                        echo '<a href="' . $base_page_url . '&page=1" aria-label="First">';
                        echo '<span aria-hidden="true" class="glyphicon glyphicon-fast-backward"></span>';
                        echo '</a>';
                        echo '</li>';

                        echo '<li>';
                        echo '<a href="' . $base_page_url . '&page=' . ($page - 1) . '" aria-label="Previous">';
                        echo '<span aria-hidden="true" class="glyphicon glyphicon-chevron-left"></span>';
                        echo '</a>';
                        echo '</li>';
                     }

                     for ($p = 1; $p <= $total_pages; ++$p) {
                        //check if we have too many pages to display them all in the paginator
                        if ($p >= ($page - 5) && $p <= ($page + 5)) {
                           $active_page_class = '';
                           $active_link_style = '';
                           if ($p == $page) {
                              $active_page_class = 'class="active"';
                              $active_link_style = ' style="background:' . $accRGB . ';"';
                           }

                           echo '<li ' . $active_page_class . '><a href="' . $base_page_url . '&page=' . $p . '" ' . $active_link_style . '>' . $p . '</a></li>';
                        }
                     }

                     if ($page != $total_pages) {
                        echo '<li>';
                        echo '<a href="' . $base_page_url . '&page=' . ($page + 1) . '" aria-label="Next">';
                        echo '<span aria-hidden="true" class="glyphicon glyphicon-chevron-right"></span>';
                        echo '</a>';
                        echo '</li>';

                        echo '<li>';
                        echo '<a href="' . $base_page_url . '&page=' . $total_pages . '" aria-label="Last">';
                        echo '<span aria-hidden="true" class="glyphicon glyphicon-fast-forward"></span>';
                        echo '</a>';
                        echo '</li>';
                     }
                  }
                  ?>
               </ul>
            </nav>
         </div>
      </div>
         <div class="box-body">
            <div class="form-group">
                <!--a onclick="saveRep();" class="btn btn-block btn-social btn-dropbox " style="background-color:<?php echo $accRGB; ?>; border: 2px solid; border-radius: 6px !important; float:left; width: 134px; margin-top:-1px;">
                    <i class="fa fa-save"></i>Save Report
                </a-->
               <a href="<?php echo $csv_url; ?>" class="btn btn-block btn-social btn-dropbox" style="background-color:<?php echo $accRGB; ?>; border: 2px solid; border-radius: 6px !important; float:left; width: 134px; margin-top:-21px;">
                  <i class="fa fa-save"></i>Save Report
               </a>
            </div>
         </div>
      </div>
   </section>
</aside>

<!--  THE JAVASCRIPT IMPORT ALWAYS APPEARS BEFORE LOCAL JAVASCRIPT AT THE BOTTOM OF THE PAGE -->
<?php include("template_import_script.php"); //must import all scripts first  ?>
<!-- END JAVASCRIPT IMPORT -->

<script type="text/javascript">
   var startDate = "<?php echo $start_date; ?>";
   var endDate = "<?php echo $end_date; ?>";

   $(document).ready(function (e)
   {
      //hide the loading popup if it is showing
      $(".loader").fadeOut("fast");
      $(".loaderIcon").fadeOut("fast");

      $('.show-more').on("click", function (e)
      {
         e.preventDefault();

         var mosmsId = $(this).attr('mosms_id');

         if($('#mo_mtsms_partial_text'+mosmsId).is(":visible") )
         {
            $(this).html("[Less]");
            $('#mo_mtsms_partial_text'+mosmsId).hide();
            $('#mo_mtsms_full_text'+mosmsId).show();
         }
         else
         {
            $(this).html("[More]");
            $('#mo_mtsms_full_text'+mosmsId).hide();
            $('#mo_mtsms_partial_text'+mosmsId).show();
         }
      });

      /**********************************
       * BUTTONS FOR HANDLING SEARCHING
       **********************************/
      $(".btnSetSearchType").on("click", function (e)
      {
         e.preventDefault();

         var searchType = $(this).attr('search_type');

         $("#btnSearchType").attr('search_type', searchType);
         $("#btnSearchType").html($(this).html());
      });


      $("#btnSearchMessageInbox").on("click", function (e)
      {
         e.preventDefault();
         runSearch();
      });

      $('#reportrange').daterangepicker(
          {
             //$endDate = $dateArr[1];
             //singleDatePicker:true,
             ranges: {
                'Today': [moment(), moment()],
                'Yesterday': [moment().subtract('days', 1), moment().subtract('days', 1)],
                'Last 7 Days': [moment().subtract('days', 6), moment()],
                'Last 30 Days': [moment().subtract('days', 29), moment()],
                'This Month': [moment().startOf('month'), moment().endOf('month')],
                'Last Month': [moment().subtract('month', 1).startOf('month'), moment().subtract('month', 1).endOf('month')]
             },
             startDate: moment(<?php echo $startDate; ?>),
             endDate: moment(<?php echo $endDate; ?>)
          },
          function (start, end) {
             $('#reportrange span').html(start.format('YYYY-MM-DD'));
             $(".loader").fadeIn("slow");
             $(".loaderIcon").fadeIn("slow");

             var serviceId = document.getElementById("client").value;
             window.location = "../pages/message_inbox.php?service_id=" + serviceId + "&start_date=" + encodeURIComponent(start.format('YYYY-MM-DD')) + "&end_date=" + encodeURIComponent(end.format('YYYY-MM-DD'));
          }
      );
   });



   function reloadOnSelect(serviceId)
   {
      $(".loader").fadeIn("fast");
      $(".loaderIcon").fadeIn("fast");

      window.location = "../pages/message_inbox.php?service_id=" + serviceId + "&start_date=" + encodeURIComponent(startDate) + "&end_date=" + encodeURIComponent(endDate);
   }


   //this function grabs the search variables and refreshes the page to run a search
   function runSearch()
   {
      var searchType = $('#btnSearchType').attr('search_type');
      var searchString = $('#inputSearchField').val();

      var serviceId = document.getElementById("client").value;
      window.location = "../pages/message_inbox.php?service_id=" + serviceId + "&start_date=" + encodeURIComponent(startDate) + "&end_date=" + encodeURIComponent(endDate) +"&search_type=" + encodeURIComponent(searchType) + "&search_string=" + encodeURIComponent(searchString);
   }

</script>

<!-- Template Footer -->

