<?php
/**
 * Include the header template which sets up the HTML
 *
 * Don't forget to include template_import_script before any Javascripting
 * Don't forget to include template footer.php at the bottom of the page as well
 */
$headerPageTitle = "Batch Status"; //set the page title for the template import
TemplateHelper::initialize();

$service_id = $_GET['service_id'];
$bId = $_GET['bId'];
$start_date = $_GET['start_date'];
$end_date = $_GET['end_date'];

///////////////////////////////////////////////////////////////////////////////////////////////////
if (isset($_GET['nStat']) && $_GET['nStat'] != '')
{
   if (isset($_GET['nShed']) && $_GET['nShed'] != '')
   {
      $res = updateWithTime($bId, $_GET['nStat'], $_GET['nShed']);
   }
   else
   {
      $res = updateWithoutTime($bId, $_GET['nStat']);
   }
}
///////////////////////////////////////////////////////////////////////////////////////////////////

$bData = getBatchStatusData($bId);

foreach ($bData as $key => $value)
{
   $bSched = $value['batch_schedule'];
}

////////////////////////////////////////////////
////////////////////////////////////////////////
//////////////////////////////////////////////// 
?>

<aside class="right-side">
   <section class="content-header">
      <h1>
         Bulk SMS Traffic Status
         <!--small>Control panel</small-->
      </h1>
   </section>

   <section class="content">
      <div class="row col-lg-12">
         <div class="box box-solid" style="">
            <div class="box box-warning" style="border-top-color:<?php echo $accRGB; ?>;display:<?php echo $dsp; ?>">
               <div class="box-body">
                  <div class="callout callout-info" style="margin-bottom:0px;">
                     <h4>Instructions</h4>
                     <p>To ABORT a bulk SMS campaign, click on EDIT under Status for the campaign. Completed batches can not be aborted.</p>
                  </div>
                  <?php
                  if (isset($res) && $res == 1) {
                     echo '<div class="callout callout-success">';
                     echo '<h4>Success</h4>';
                     echo '<p>Your changes have been saved successfully.</p>';
                     echo '</div>';
                  }

                  if (isset($res) && $res == 0) {
                     echo '<div class="callout callout-error">';
                     echo '<h4>Error</h4>';
                     echo '<p>Your changes have not been saved.</p>';
                     echo '</div>';
                  }

                  $bName = getBatchReferenceAndCampId($bId);
                  foreach ($bName as $key => $value) {
                     $bRef = $value['batch_reference'];
                     $campN = $value['campaign_id'];
                  }

                  $campData = getClientAndCampaignName($campN);
                  foreach ($campData as $key => $value) {
                     $clName = $value['client_name'];
                     $caName = $value['campaign_name'];
                  }

                  foreach ($bData as $key => $value) {
                     echo '<p style="padding-top:10px;"><b>Client Name: </b>' . $clName . '</p>';
                     echo '<p><b>Campaign Name: </b>' . $caName . '</p>';
                     echo '<p><b>Campaign Code: </b>' . $value['batch_identifier'] . '</p>';
                     echo '<p><b>Reference: </b>' . $bRef . '</p>';
                     echo '<p><b>Delivery Status: </b>' . $value['batch_submit_status'] . '</p>';
                     echo '<p><b>Delivery Schedule: </b>' . $value['batch_schedule'] . '</p>';
                     echo '<p><b>Status: </b>' . $value['batch_status'] . ' - ' . $value['batch_status_message'] . '</p>';
                     if (($value['batch_status'] == 'WAIT' || $value['batch_status'] == 'SEND') && $value['batch_submit_status'] != 'ABORT') {
                        //echo '<br><br><b>New Batch Status: </b>';
                        echo '<label>New Bulk Status:</label>';
                        echo '<select class="form-control" name="newStatus" id="newStatus" onchange="checkTheStatus(this.value)">';
                        echo '<option value="CHOOSE">Change status here..</option>';
                        echo '<option value="PAUSE">Pause</option>';
                        echo '<option value="SCHEDULE">Schedule</option>';
                        echo '<option value="SEND">Send Immediately</option>';
                        echo '<option value="ABORT">Abort</option>';
                        echo '</select>';

                        echo '<label id="scheduledTimerLabel" style="display:none;padding-top:10px;">New Bulk Schedule</label>';
                        echo '<div id="scheduledTimer" class="select pull-left" style="cursor: pointer;padding-left:10px;display:none;">';
                        echo '<i class="fa fa-calendar fa-lg"></i>';
                        echo '<span style="font-size:15px" form="bulkData" id="sTime">&nbsp;&nbsp;' . $bSched . '</span><b class="caret"></b>';
                        echo '</div><br><br>';
                     }
                     echo '<p><b>Message: </b>' . helperSMSGeneral::restorePlaceholderValuesForDisplay($value['batch_csv_content']) . '</p>';
                     $serviceName = getServiceName($service_id);
                     echo '<p><b>Service: </b>' . $serviceName . '</p>';
                     $uName = getUserNameFromID($value['user_id']);
                     echo '<p><b>Submitted by: </b>' . $uName . '</p>';
                     echo '<p><b>Created: </b>' . $value['batch_created'] . '</p>';
                     $value['batch_meta'] = urldecode($value['batch_meta']);
                     $value['batch_meta'] = strstr($value['batch_meta'], "=");
                     $value['batch_meta'] = substr($value['batch_meta'], 1);
                     echo '<p><b>Filename: </b>' . $value['batch_meta'] . '</p>';
                  }
                  ?>

               </div>
               <div class="box-footer" style="height:57px; display:inline-block" id="">
                  <a onclick="goBack();" class="btn btn-block btn-social btn-dropbox " style="background-color:<?php echo $accRGB; ?>; border: 2px solid; border-radius: 6px !important; float:left; width: 100px; margin-top:0px;">
                     <i class="fa fa-arrow-left"></i>Back
                  </a>
               </div>
               <?php
               //if($value['batch_status'] == 'WAIT' || $value['batch_status'] == 'SEND')
               if (($value['batch_status'] == 'WAIT' || $value['batch_status'] == 'SEND') && $value['batch_submit_status'] != 'ABORT') {
                  ?>
                  <div class="box-footer" style="height:57px; display:none" id="footer">
                     <a onclick="saveStatus();" class="btn btn-block btn-social btn-dropbox " style="background-color:<?php echo $accRGB; ?>; border: 2px solid; border-radius: 6px !important; float:left; width: 130px; margin-top:0px;">
                        <i class="fa fa-save"></i>Save Status
                     </a>
                  </div>
                  <?php
               }
               ?>
            </div>
         </div>
      </div>
   </section>
</aside>
<!--  THE JAVASCRIPT IMPORT ALWAYS APPEARS BEFORE LOCAL JAVASCRIPT AT THE BOTTOM OF THE PAGE -->
<?php include("template_import_script.php"); //must import all scripts first ?>
<!-- END JAVASCRIPT IMPORT -->

<script type="text/javascript">
   var ender = '<?php echo $bSched; ?>';
   $('#scheduledTimer').daterangepicker(
           {
              singleDatePicker: true,
              timePicker: true,
              timePickerIncrement: 5,
              timePicker12Hour: false,
              format: 'YYYY-MM-DD h:mm A',
              timePickerSeconds: false
                      //minDate:Date.now

           },
           function (start, end, label)
           {
              /*alert(label);
               alert(moment(end, 'MMM DD YYYY, h:mmA'));
               var years = moment(start, 'MMM DD YYYY, h:mmA');
               alert(years);*/

              $('#scheduledTimer').html(end.format("YYYY-MM-DD HH:mm:ss"));

              ender = end.format("YYYY-MM-DD HH:mm:ss");
              //enderPos = ender.indexOf("G");
              //ender = ender.substr(0, enderPos);
              //alert(ender);
              //$('#scheduledTimer').html(moment(start));
           }
   );
</script>

<script type="text/javascript">
   var startDate = "<?php echo urlencode($start_date); ?>";
   var endDate = "<?php echo urlencode($end_date); ?>";

   function goBack()
   {
      var sId = <?php echo $service_id; ?>;
      var bId = <?php echo $bId; ?>;

      window.location = "../pages/batchlist.php?service_id=" + sId + "&bId=" + bId + "&start_date=" + startDate + "&end_date=" + endDate;
   }

   function saveStatus()
   {
      var newStat = document.getElementById('newStatus').value;
      var sId = <?php echo $service_id; ?>;
      var bId = <?php echo $bId; ?>;
      //alert(newStat);
      if (newStat == 'SCHEDULE')
      {
         //dateRanger = document.getElementById('scheduledTimer');
         //alert(ender);   
         window.location = "../pages/pause.php?service_id=" + sId + "&bId=" + bId + "&nStat=" + newStat + "&nShed=" + ender  + "&start_date=" + startDate + "&end_date=" + endDate;
      } else
      {
         window.location = "../pages/pause.php?service_id=" + sId + "&bId=" + bId + "&nStat=" + newStat + "&start_date=" + startDate + "&end_date=" + endDate;
      }
   }

   function checkTheStatus(v)
   {
      ///alert(v);
      var dateRanger = document.getElementById('scheduledTimer');
      var dateRangerLabel = document.getElementById('scheduledTimerLabel');
      if (v == 'SCHEDULE')
      {
         dateRanger.style.display = "block";
         dateRangerLabel.style.display = "block";

      } else
      {
         dateRanger.style.display = "none";
         dateRangerLabel.style.display = "none";
      }
      var theFooter = document.getElementById('footer');
      if (v == 'CHOOSE')
      {
         theFooter.style.display = "none";
      } else
      {
         theFooter.style.display = "inline-block";
      }
   }

</script>

<script type="text/javascript">
   $(window).load(function () {
      $(".loader").fadeOut("slow");
      $(".loaderIcon").fadeOut("slow");
   })
</script>

<!-- Template Footer -->

