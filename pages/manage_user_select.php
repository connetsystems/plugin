<?php
   /**
    * Include the header template which sets up the HTML
    *
    * Don't forget to include template_import_script before any Javascripting
    * Don't forget to include template footer.php at the bottom of the page as well
    */
   //-------------------------------------------------------------
   // Template
   //-------------------------------------------------------------
   TemplateHelper::setPageTitle('Manage Users');
   TemplateHelper::initialize();

   //-------------------------------------------------------------
   // MANAGE USERS
   //-------------------------------------------------------------
   //get all the user for this page

?>

<aside class="right-side">
   <section class="content-header">
      <h1>
         Select A User
      </h1>
   </section>

   <section class="content">

      <?php
         //include the top nav
         $nav_section = "users";
         include('modules/module_admin_nav.php');
      ?>

      <!-- SEARCH BAR AND ADD BUTTON -->
      <div class="row">
         <div class="col-lg-9">
            <!-- SEARCH BAR -->
            <div class="input-group">
               <input type="text" class="form-control" id="inputSearchUser" placeholder="Type to filter users..." aria-label="">
               <div class="input-group-btn">
                  <button type="button" class="btn btn-default dropdown-toggle" id="btnSearchType" search_type="all" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                     <span id="btnSearchTypeString">Search All</span> <span class="caret"></span>
                  </button>
                  <ul class="dropdown-menu dropdown-menu-right">
                     <li><a class="search-type" search_type="all" search_type_string="Search All" href="#">Search All</a></li>
                     <li><a class="search-type" search_type="user_id" search_type_string="User ID" href="#">User ID</a></li>
                     <li><a class="search-type" search_type="user_username" search_type_string="Username" href="#">Username</a></li>
                  </ul>
               </div>
            </div>
         </div>
         <div class="col-lg-3">
            <a class="btn btn-primary btn-block" href="<?php echo WebAppHelper::getBaseServerURL(); ?>/pages/admin_users/create_user.php"><i class="fa fa-user-plus" aria-hidden="true"></i> Create A New User</a>
         </div>
      </div>
      <hr/>

      <p>Please select a user to manage.</p>
      <div class="row">
         <div class="col-lg-1">
            <p style="margin-left:10px;"><strong>User ID</strong></p>
         </div>
         <div class="col-lg-4">
            <p style="margin-left:10px;"><strong>Username</strong></p>
         </div>
         <div class="col-lg-7" style="text-align:right;">
            <p style="margin-right:10px;"><strong>User Details</strong></p>
         </div>
      </div>
      <div class="row">
         <div class="col-lg-12">
            <!-- THE LIST OF SERVICES ON THIS PAGE -->
            <div class="list-group" id="listUsersHolder">
               <a href="#" class="list-group-item" style="text-align:center;" id="listItemLoading">
                  <p class="list-group-item-text"><span class="glyphicon glyphicon-refresh glyphicon-spin"></span> Loading users...</p>
               </a>
            </div>
         </div>
      </div>
   </section>
</aside>


<!--  THE JAVASCRIPT IMPORT ALWAYS APPEARS BEFORE LOCAL JAVASCRIPT AT THE BOTTOM OF THE PAGE -->
<?php include("template_import_script.php"); //must import all scripts first   ?>
<!-- END JAVASCRIPT IMPORT -->

<script type="text/javascript">

   var page = 1;
   var limit = 25;
   var searchType = "all";
   var searchTerm = "";
   var loadingListItem = null;
   var isLoading = false;
   var noMoreUsers = false;

   $(document).ready(function (e)
   {
      loadingListItem = $('#listItemLoading');

      //load the initial users
      loadUserListViaAjax();

      //hide the loading popup if it is showing
      $(".loader").fadeOut("fast");
      $(".loaderIcon").fadeOut("fast");

      /***************************************
       * Listener for changing the search type
       ***************************************/
      $(".search-type").on("click", function (e)
      {
         e.preventDefault();
         setSearchType($(this).attr('search_type'), $(this).attr('search_type_string'));
      });

      /***************************************
       * Listener for changing the search type
       ***************************************/
      $("#inputSearchUser").keyup(function (e)
      {
         e.preventDefault();
         searchTerm = $(this).val();
         showListLoading();
         keyDelay(function() { runSearchForUser(); }, 400);
      });

      /***************************************
       * Listener for changing the search type
       ***************************************/
      $("#inputSearchUser").mouseup(function (e)
      {
         e.preventDefault();
         searchTerm = $(this).val();
         showListLoading();
         keyDelay(function() { runSearchForUser(); }, 400);
      });

      /*************************************
       * For loading more users
       * This listener checks scroll position and starts loading more items
       *
       *************************************/
      $(window).scroll(function()
      {
         if(!noMoreUsers && !isLoading && ($(window).scrollTop() + 80) >= $(document).height() - $(window).height())
         {
            isLoading = true;
            page++;
            console.log("Loading page : " + page);

            loadUserListViaAjax();
         }
      });

   });

   function reloadOnSelect(serviceId)
   {
      $(".loader").fadeIn("slow");
      $(".loaderIcon").fadeIn("slow");
      window.location = "contactlists.php?service_id=" + serviceId;
   }

   //this function grabs the search variables and refreshes the page to run a search
   function runSearchForUser()
   {
      noMoreUsers = false;

      //remove the loading element and clear all the users, then show loading
      loadingListItem.detach();
      $('#listUsersHolder').empty();
      loadingListItem.appendTo('#listUsersHolder');

      //reset the page number as we are now doing a search
      page = 1;

      loadUserListViaAjax();
   }

   function setSearchType(type, typeString)
   {
      var doSearch = false;
      if(type != searchType && searchTerm != "")
      {
         doSearch = true;
      }
      searchType = type;
      $("#btnSearchTypeString").html(typeString);

      if(doSearch)
      {
         runSearchForUser();
      }
   }




   /*************************************
    * This function uses the current properties of the page,
    * such as "page" number, search terms, and loads the User list dyamically.
    *
    *************************************/
   function loadUserListViaAjax()
   {
      //run our ajaxController to save the details
      $.post("../php/ajaxAdminGetUserList.php", {page: page, limit: limit, search_type:searchType, search_term:searchTerm}).done(function (data) {
         try
         {
            var json = $.parseJSON(data);

            if (json.success)
            {
               if(json.is_search == true && json.page == 0 && json.users.length == 0)
               {
                  noMoreUsers = true;
                  showNoResults();
               }
               else if(json.users.length == 0)
               {
                  noMoreUsers = true;
                  showNoMoreUsers();
               }
               else if(json.users.length < limit)
               {
                  addUsersToList(json.users);
                  showNoMoreUsers();
               }
               else
               {

                  addUsersToList(json.users);
               }
            }
            else
            {
               showLoadingFailure();
            }

         }
         catch (err)
         {
            showLoadingFailure();
            console.log("Could not load list: " + err.message);

         }

         isLoading = false;

      }).fail(function (data) {
         showLoadingFailure();
      });
   }

   /*************************************
    * This function takes the dynamically
    * loaded json users and populates the list
    *
    *************************************/
   function addUsersToList(users)
   {
      //remove the loading element and add the users
      loadingListItem.detach();

      clearListMessages();

      for(var i = 0; i < users.length; ++ i)
      {
         var user = users[i];

         var headerLogo = '/img/skinlogos/1/1_header.png';
         if(user.header_logo != "" && user.header_logo != null)
         {
            headerLogo = user.header_logo;
            console.log("Header logo: " + user.header_logo);
         }

         var full_name = "n/a";
         if((user.firstname != "" && user.firstname != null) || (user.lastname != "" && user.lastname != null))
         {
            full_name = user.firstname + " " + user.lastname;
         }

         var email = "n/a";
         if(user.emailaddress != "" && user.emailaddress != null)
         {
            email = user.emailaddress;
         }

         //could do with an actual tempalting language here, but for the sake of time, we have not
         $('#listUsersHolder').append('<a href="manage_user.php?user_id=' + user.user_id + '" class="list-group-item"'
            + ' data-toggle="tooltip" data-placement="bottom" title="Click to manage this user.">'
            + '<div class="row">'
            + '<div class="col-lg-1">'
            + '<h2 class="list-group-item-heading"><strong>' + user.user_id + '</strong></h2>'
            + '</div>'
            + '<div class="col-lg-4">'
            + '<h4 class="list-group-item-heading">User: <strong>' + user.user_username + '</strong></h4>'
            + '</div>'
            + '<div class="col-lg-3">'
             + '<p class="list-group-item-text">Full Name: ' + full_name + '</p>'
            + '</div>'
             + '<div class="col-lg-4">'
             + '<p class="list-group-item-text">Email: ' +  email + '</p>'
             + '</div>'
            + '</div>'
            + '</a>');

      }

      loadingListItem.appendTo('#listUsersHolder');

      //refresh tooltips
      $('[data-toggle="tooltip"]').tooltip();
   }

   function clearListMessages()
   {
      $('#listItemNoResults').remove();
      $('#listItemEnd').remove();
      $('#listItemError').remove();
   }

   function showNoResults()
   {
      clearListMessages();

      //remove the loading element and show no results
      loadingListItem.detach();
      $('#listUsersHolder').append('<div id="listItemNoResults" class="list-group-item text-warning" style="text-align:center;"> No results match your query.</div>');
   }

   function showNoMoreUsers()
   {
      clearListMessages();

      //remove the loading element and show no results
      loadingListItem.detach();
      $('#listUsersHolder').append('<div id="listItemEnd" class="list-group-item" style="text-align:center;"> End of user list.</div>');
   }

   function showLoadingFailure()
   {
      clearListMessages();

      //remove the loading element and show no results
      loadingListItem.detach();
      $('#listUsersHolder').append('<div id="listItemError" class="list-group-item text-danger" style="text-align:center;"> Could not load anymore results, please check your internet connection.</div>');
   }

   function showListLoading()
   {
      //remove the loading element and clear all the services, then show loading
      loadingListItem.detach();
      $('#listUsersHolder').empty();
      loadingListItem.appendTo('#listUsersHolder');
   }

   /**
    * This delay is used to prevent the ajax call been made immediately while the user types, preventing too many requests
    */
   var keyDelay = (function(){
      var timer = 0;
      return function(callback, ms){
         clearTimeout (timer);
         timer = setTimeout(callback, ms);
      };
   })();

</script>

<!-- Template Footer -->

