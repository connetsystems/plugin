<?php
/**
 * Include the header template which sets up the HTML
 *
 * Don't forget to include template_import_script before any Javascripting
 * Don't forget to include template footer.php at the bottom of the page as well
 */
$headerPageTitle = "API Traffic Messages"; //set the page title for the template import
TemplateHelper::initialize();


ini_set('memory_limit', '512M');

////////////////////////////////////////////////

if (isset($_GET['range'])) {
   $range = $_GET['range'];
   $dateArr = explode(' - ', $_GET['range']);
   $startDate = $dateArr[0];
   $endDate = $dateArr[1];
}

$startDateConv = date('Y-m-d', ($startDate / 1000));
$endDateConv = date('Y-m-d', ($endDate / 1000));

/* if(isset($_GET['sents']))
  {
  $units = $_GET['sents'];
  } */

if (isset($_GET['from'])) {
   $from = $_GET['from'];
   $starting = ($from - 1) * 100;
   //$from = $from - 1;
}

if (isset($_GET['sort'])) {
   $sort = urldecode($_GET['sort']);
   $ordering = strstr($sort, ':');
   $ordering = trim($ordering, ':');
   $ordering = trim($ordering, '"');
   $sortUrl = urlencode($sort);
}

if (isset($_GET['searchTerm']) && $_GET['searchTerm'] != '') {
   $searchT = $_GET['searchTerm'];

   $searchTerm = ',{
                     "multi_match" : {
                        "query":    "' . $searchT . '",
                        "fields": ["smsc", "src", "dest", "content", "mccmnc"]
                      }   
                    }';
} else {
   $searchT = '';
   $searchTerm = '';
}

if (isset($_GET['client'])) {
   $client = $_GET['client'];
   $cl = $_GET['client'];
   $clp = strrpos($cl, ' ');
   $cl = trim(substr($cl, $clp, strlen($cl)));
   $dsp = 'inline';
   $dir = '../img/serviceproviders';
   $serProviders = scandir($dir);

   $qry = '{
              "size":5300,
              "from":60000,
              "sort": {
                ' . $sort . '
              },
              "fields" : ["mtsms_id","timestamp","src","dest","content","dlr","mccmnc","smsc","rdnc"],
              "query": {
                "filtered": {
                  "query": {
                    "bool" : {
                        "must" : [
                            {
                             "multi_match" : {
                                "query":    "telkom_smsc",
                                "fields": ["smsc"]
                              }
                            }
                        ]
                    }
                  },
                  "filter": {
                    "bool": {
                      "must": [
                        {
                          "range": {
                            "timestamp": {
                                "gte": 1414800000000,
                                "lte": 1428451200000
                            }
                          }
                        }
                      ],
                      "must_not": []
                    }
                  }
                }
              }
            }';


   $allStats = runRawMCoreElasticsearchQuery($qry);

   foreach ($allStats as $key => $value) {
      if ($key == 'hits') {
         $units = $value['total'];
      }
   }
}
?>

<aside class="right-side">
   <section class="content-header">
      <h1>
         API Traffic Messages
         <!--small>Control panel</small-->
      </h1>
   </section>

   <section class="content">
      <div class="box box-connet" style="padding-top:10px;padding-left:10px;padding-right:10px;padding-bottom:45px; border-top-color:<?php echo $accRGB; ?>;">

         <div class="callout callout-info" style="margin-bottom:10px;">
            <h4>API Traffic Messages Instructions / Details</h4>
            <p>This page will show you all API traffic SMS's for the date range that you specified in the previous page.</p>
         </div>

         <a class="btn btn-block btn-social btn-dropbox" name="nid" onclick="backToTraffic()" style="background-color:<?php echo $accRGB; ?>; border: 2px solid; border-radius: 6px !important; float:left; width: 100px;">
            <i class="fa fa-arrow-left"></i>Back
         </a>
      </div>
   </section>

   <section class="content">
      <div class="box box-connet" style="padding-top:1px; border-top-color:<?php echo $accRGB; ?>;">
         <div class="box-body">
            <div class="box-body table-responsive no-padding">
               <div class="row">
                  <div class="col-xs-11">
                     <div class="dataTables_filter" style="padding-top:6px;">
                        <label>Search: <input type="text" id="search_filter" aria-controls="example2"></label>
                     </div>
                  </div>
                  <div class="col-xs-1">
                     <a class="btn btn-block btn-social btn-dropbox" name="nid" onclick="searchText()" style="background-color:<?php echo $accRGB; ?>; border: 2px solid; border-radius: 6px !important; float:left; width: 100px;">
                        <i class="fa fa-search"></i>Search
                     </a>
                  </div>
               </div>
               <table id="example2"  class="table table-striped table-hover">
                  <thead>
                     <tr>
                        <?php
//echo '<br>-sort='.$sortUrl;
                        if (isset($_GET['controller'])) {
                           //echo '<br>0-order='.$ordering.'-';
                           if ($ordering == 'desc') {
                              $ordering = 'asc';
                              //echo '<br>1-order='.$ordering.'-';
                           } else {
                              $ordering = 'desc';
                              //echo '<br>2-order='.$ordering.'-';
                           }
                           //echo '<br>3-order='.$ordering.'-';
                        }
                        echo '<th style="width:100px;"><a href="../pages/apitrafficmsgadmin.php?client=' . $_GET['client'] . '&range=' . $_GET['range'] . '&sents=' . $units . '&from=1&controller=1&sort=' . '%22timestamp%22%3A%22' . $ordering . '%22' . '">Date</a></th>';
                        echo '<th style="width:100px;"><a href="../pages/apitrafficmsgadmin.php?client=' . $_GET['client'] . '&range=' . $_GET['range'] . '&sents=' . $units . '&from=1&controller=1&sort=' . '%22src%22%3A%22' . $ordering . '%22' . '">From</a></th>';
                        echo '<th style="width:100px;"><a href="../pages/apitrafficmsgadmin.php?client=' . $_GET['client'] . '&range=' . $_GET['range'] . '&sents=' . $units . '&from=1&controller=1&sort=' . '%22dest%22%3A%22' . $ordering . '%22' . '">To</a></th>';
                        echo '<th>Content</th>';
                        echo '<th style="width:100px;"><a href="../pages/apitrafficmsgadmin.php?client=' . $_GET['client'] . '&range=' . $_GET['range'] . '&sents=' . $units . '&from=1&controller=1&sort=' . '%22mccmnc%22%3A%22' . $ordering . '%22' . '">Network</a></th>';
                        echo '<th style="width:100px;"><a href="../pages/apitrafficmsgadmin.php?client=' . $_GET['client'] . '&range=' . $_GET['range'] . '&sents=' . $units . '&from=1&controller=1&sort=' . '%22smsc%22%3A%22' . $ordering . '%22' . '">SMSC</a></th>';
                        echo '<th style="width:100px;"><a href="../pages/apitrafficmsgadmin.php?client=' . $_GET['client'] . '&range=' . $_GET['range'] . '&sents=' . $units . '&from=1&controller=1&sort=' . '%22dlr%22%3A%22' . $ordering . '%22' . '">Status</a></th>';
                        ?>
                     </tr>
                  </thead>
                  <tbody>
                     <?php
                     foreach ($allStats as $key => $value) {
                        if ($key == 'hits') {
                           $hits = count($value['hits']);
                           for ($i = 0; $i < $hits; $i++) {
                              $netw = getNetFromMCC($value['hits'][$i]['fields']['mccmnc']['0']);
                              $smsc = $value['hits'][$i]['fields']['smsc']['0'];
                              $posSmsc = strrpos($smsc, '_');
                              $smsc = ucfirst(substr($smsc, 0, $posSmsc));
                              $time = str_replace('T', ' ', $value['hits'][$i]['fields']['timestamp']['0']);
                              $time = strstr($time, '+', true);
                              $timeSec = strrpos($time, ':');
                              $time = substr($time, 0, $timeSec);

                              echo '<tr>';
                              echo '<td>' . $time . '</td>';
                              echo '<td>' . $value['hits'][$i]['fields']['src']['0'] . '</td>';
                              echo '<td>' . $value['hits'][$i]['fields']['dest']['0'] . '</td>';
                              echo '<td>' . preg_replace('/[^(\x20-\x7F)]*/', '', $value['hits'][$i]['fields']['content']['0']) . '</td>';

                              /* $counter = 0;
                                foreach ($serProviders as $key3 => $value3)
                                {
                                $value3 = strstr($value3, '.', true);

                                if(!empty($value3) && strpos($netw, $value3) !== false)
                                {
                                echo "<td style='width:180px;'><img src='../img/serviceproviders/".$value3.".png'>&nbsp;&nbsp;".ucfirst($netw).' ('.$value['hits'][$i]['fields']['mccmnc']['0'].')</td>';
                                //echo '<td>'.$netw.'</td>';
                                $counter++;
                                break;
                                }
                                }
                                if($counter == 0)
                                {
                                } */
                              echo "<td style='width:180px;'>" . ucfirst($netw) . ' (' . $value['hits'][$i]['fields']['mccmnc']['0'] . ')</td>';

                              /* $counter = 0;
                                foreach ($serProviders as $key3 => $value3)
                                {
                                if($value3 == (lcfirst($smsc).'.png'))
                                {
                                echo "<td style='width:120px;'><img src='../img/serviceproviders/".lcfirst($smsc).".png'>&nbsp;&nbsp;".$smsc.'</td>';
                                $counter++;
                                }
                                }
                                if($counter == 0)
                                {
                                } */
                              echo "<td style='width:120px;'>" . $smsc . '</td>';

                              $dlrPos = $value['hits'][$i]['fields']['dlr']['0'];
                              if (($dlrPos & 1) == 1) {
                                 echo '<td style="color:#009900;">Delivered</td>';
                              } elseif (($dlrPos & 32) == 32 && ($dlrPos & 1) == 0 && ($dlrPos & 2) == 0) {
                                 echo '<td style="color:#eec00d;">Pending</td>';
                              } elseif (($dlrPos & 2) == 2) {
                                 echo '<td style="color:#cb0234;">Failed</td>';
                              } else {
                                 echo '<td>' . $dlrPos . '</td>';
                              }
                              echo '</tr>';
                           }
                        }
                     }
                     ?>
                  </tbody>
               </table>
               <div class="row">
                  <div class="col-xs-6">
                     <div class="dataTables_info" id="example2_info">Showing <?php echo ($starting); ?> to <?php
                        if (($from * 100) < $units) {
                           echo ($from * 100);
                        } else {
                           echo $units;
                        }
                        ?> of <?php echo $units; ?> entries
                     </div>
                  </div>
                  <div class="col-xs-6">
                     <div class="dataTables_paginate paging_bootstrap">
                        <ul class="pagination">
                           <?php
                           if ($from > 1) {
                              echo '<li class="prev">';
                              echo '<a href="../pages/apitrafficmsgadmin.php?client=' . $_GET['client'] . '&range=' . $_GET['range'] . '&sents=' . $units . '&searchTerm=' . $searchT . '&from=1&sort=' . $sortUrl . '">&#8592; First</a>';
                              echo '</li>';
                              if ($from > 2) {
                                 echo '<li>';
                                 echo '<a href="../pages/apitrafficmsgadmin.php?client=' . $_GET['client'] . '&range=' . $_GET['range'] . '&sents=' . $units . '&searchTerm=' . $searchT . '&from=' . ($from - 2) . '&sort=' . $sortUrl . '">' . ($from - 2) . '</a>';
                                 echo '</li>';
                              }
                              echo '<li>';
                              echo '<a href="../pages/apitrafficmsgadmin.php?client=' . $_GET['client'] . '&range=' . $_GET['range'] . '&sents=' . $units . '&searchTerm=' . $searchT . '&from=' . ($from - 1) . '&sort=' . $sortUrl . '">' . ($from - 1) . '</a>';
                              echo '</li>';
                           }
                           echo '<li class="active">';
                           echo '<a href="../pages/apitrafficmsgadmin.php?client=' . $_GET['client'] . '&range=' . $_GET['range'] . '&sents=' . $units . '&searchTerm=' . $searchT . '&from=' . $from . '&sort=' . $sortUrl . '">' . $from . '</a>';
                           echo '</li>';
                           if (($from * 100) < $units) {
                              echo '<li>';
                              echo '<a href="../pages/apitrafficmsgadmin.php?client=' . $_GET['client'] . '&range=' . $_GET['range'] . '&sents=' . $units . '&searchTerm=' . $searchT . '&from=' . ($from + 1) . '&sort=' . $sortUrl . '">' . ($from + 1) . '</a>';
                              echo '</li>';
                              if ((($from + 1) * 100) < $units) {
                                 echo '<li>';
                                 echo '<a href="../pages/apitrafficmsgadmin.php?client=' . $_GET['client'] . '&range=' . $_GET['range'] . '&sents=' . $units . '&searchTerm=' . $searchT . '&from=' . ($from + 2) . '&sort=' . $sortUrl . '">' . ($from + 2) . '</a>';
                                 echo '</li>';
                              }
                              echo '<li class="next">';
                              echo '<a href="../pages/apitrafficmsgadmin.php?client=' . $_GET['client'] . '&range=' . $_GET['range'] . '&sents=' . $units . '&searchTerm=' . $searchT . '&from=' . (round($units / 100) + 1) . '&sort=' . $sortUrl . '">Last &#8594;</a>';
                              echo '</li>';
                           }
                           ?>
                        </ul>
                     </div>
                  </div>
               </div>
               <div class="box-body">
                  <div class="form-group" style="padding-top:30px">
                     <a onclick="saveRep();" class="btn btn-block btn-social btn-dropbox" style="background-color:<?php echo $accRGB; ?>; border: 2px solid; border-radius: 6px !important; float:left; width: 134px; margin-top:-21px;">
                        <i class="fa fa-save"></i>Save Report
                     </a>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </section>

</aside>

<!--  THE JAVASCRIPT IMPORT ALWAYS APPEARS BEFORE LOCAL JAVASCRIPT AT THE BOTTOM OF THE PAGE -->
<?php include("template_import_script.php"); //must import all scripts first  ?>
<!-- END JAVASCRIPT IMPORT -->

<script type="text/javascript">
   $(function ()
   {
      $('#example2').dataTable({
         "bPaginate": false,
         "bLengthChange": false,
         "bFilter": false,
         "bSort": false,
         "bInfo": false,
         "bAutoWidth": false,
         "iDisplayLength": 100,
         "aoColumns": [
            null,
            null,
            null,
            null,
            null,
            null,
            null
         ],
         "order": [[3, "desc"]],
      });
   });

   jQuery.extend(jQuery.fn.dataTableExt.oSort,
           {
              "title-numeric-pre": function (a) {
                 var x = a.match(/title="*(-?[0-9\.]+)/)[1];
                 return parseFloat(x);
              },
              "title-numeric-asc": function (a, b) {
                 return ((a < b) ? -1 : ((a > b) ? 1 : 0));
              },
              "title-numeric-desc": function (a, b) {
                 return ((a < b) ? 1 : ((a > b) ? -1 : 0));
              }
           });

   jQuery.extend(jQuery.fn.dataTableExt.oSort,
           {
              "formatted-num-pre": function (a) {
                 a = (a === "-" || a === "") ? 0 : a.replace(/[^\d\-\.]/g, "");
                 return parseFloat(a);
              },
              "formatted-num-asc": function (a, b) {
                 return a - b;
              },
              "formatted-num-desc": function (a, b) {
                 return b - a;
              }
           });
</script>

<script type="text/javascript">
   function searchText()
   {
      serviceName = "<?php echo urlencode($_GET['client']); ?>";
      repRange = "<?php echo urlencode($_GET['range']); ?>";
      var searchT = document.getElementById('search_filter').value;
      //alert(searchT);
      //window.location = "../pages/apitrafficadmin.php?client=" + serviceName + "&range=" + repRange;
      window.location = "../pages/apitrafficmsgadmin.php?client=" + serviceName + "&range=" + repRange + "&searchTerm=" + searchT + "&sents=<?php echo $units; ?>" + "&from=1&controller=1&sort=<?php echo $sortUrl; ?>";
   }

   function backToTraffic()
   {
      serviceName = "<?php echo urlencode($_GET['client']); ?>";
      repRange = "<?php echo urlencode($_GET['range']); ?>";
      window.location = "../pages/apitrafficadmin.php?client=" + serviceName + "&range=" + repRange;
   }

   function saveRep()
   {

   }
</script>

<script type="text/javascript">
   $(window).load(function () {
      $(".loader").fadeOut("slow");
      $(".loaderIcon").fadeOut("slow");
   })
</script>

<!-- Template Footer -->

