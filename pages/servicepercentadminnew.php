<?php
/**
 * Include the header template which sets up the HTML
 *
 * Don't forget to include template_import_script before any Javascripting
 * Don't forget to include template footer.php at the bottom of the page as well
 */
$headerPageTitle = "Service %"; //set the page title for the template import
TemplateHelper::initialize();

$dir = '../img/serviceproviders';
$serProviders = scandir($dir);

if (isset($_GET['range'])) {
   $dateArr = explode(' - ', $_GET['range']);
   $startDate = $dateArr[0];
   $endDate = $dateArr[1];
} else {
   //$dateArr[0] = (time() - (1 * 24 * 60 * 60))*1000;
   $dateArr[0] = (strtotime('today midnight')) * 1000;
   $dateArr[1] = (time() * 1000);
   $startDate = $dateArr[0];
   $endDate = $dateArr[1];
}

$startDateConv = date('Y-m-d', ($startDate / 1000));
$endDateConv = date('Y-m-d', ($endDate / 1000));


if (isset($_SESSION['accountId'])) {
   $acId = $_SESSION['accountId'];
} else {
   $acId = null;
}

$clients = getServicesInAccount($acId);

if (isset($_GET['client']) && $_GET['client'] != 'Please Select') {
   $fileName = $_GET['client'];
   $cl = $_GET['client'];
   $clp = strrpos($cl, ' ');
   $cl = trim(substr($cl, $clp, strlen($cl)));
   //$dsp = 'inline';
   //$dir = '../img/routes';
   //$serProviders = scandir($dir);
} else {
   $fileName = 'Please Select';
   $_GET['client'] = "0 - Please select a client...";
   $cl = strstr($_GET['client'], ' ', true);
   //$dsp = 'none';
}

if ($fileName != 'Please Select') {
   $moData = getMOsForElasticsSId($cl, $startDateConv, $endDateConv);

   $qry = '{
                  "size": 0,
                  "aggs": {
                    "5": {
                      "date_histogram": {
                        "field": "timestamp",
                        "interval": "1d",
                        "pre_zone": "+02:00",
                        "pre_zone_adjust_large_interval": false,
                        "min_doc_count": 1,
                        "extended_bounds": {
                            "min": ' . $startDate . ',
                            "max": ' . $endDate . '
                        }
                      },
                      "aggs": {
                        "mccmnc": {
                          "terms": {
                            "field": "mccmnc",
                            "size": 0,
                            "order": {
                              "1": "desc"
                            }
                          },
                          "aggs": {
                            "1": {
                              "sum": {
                                "script": "doc[\'billing_units\'].value",
                                "lang": "expression"
                              }
                            },
                            "smsc": {
                              "terms": {
                                "field": "smsc",
                                "size": 0,
                                "order": {
                                  "1": "desc"
                                }
                              },
                              "aggs": {
                                "1": {
                                  "sum": {
                                    "script": "doc[\'billing_units\'].value",
                                    "lang": "expression"
                                  }
                                },
                                "dlr": {
                                  "terms": {
                                    "field": "dlr",
                                    "size": 0,
                                    "order": {
                                      "dlr_units": "desc"
                                    }
                                  },
                                  "aggs": {
                                    "dlr_units": {
                                      "sum": {
                                        "script": "doc[\'billing_units\'].value",
                                        "lang": "expression"
                                      }
                                    },
                                    "rdnc": {
                                      "terms": {
                                        "field": "rdnc",
                                        "size": 0,
                                        "order": {
                                          "1": "desc"
                                        }
                                      },
                                      "aggs": {
                                        "1": {
                                          "sum": {
                                            "script": "doc[\'billing_units\'].value",
                                            "lang": "expression"
                                          }
                                        }
                                      }
                                    }
                                  }
                                }
                              }
                            }
                          }
                        }
                      }
                    }
                  },
                  "query": {
                    "filtered": {
                      "query": {
                        "query_string": {
                          "query": "service_id:' . $cl . '",
                          "analyze_wildcard": true
                        }
                      },
                      "filter": {
                        "bool": {
                          "must": [
                            {
                              "range": {
                                "timestamp": {
                                  "gte": ' . $startDate . ',
                                  "lte": ' . $endDate . '
                                }
                              }
                            }
                          ],
                          "must_not": []
                        }
                      }
                    }
                  }
                }
                ';

   $allStats = runRawMCoreElasticsearchQuery($qry);
}

$sort = urlencode('"timestamp":"desc"');
$tempFail = 0;
$sents = 0;
////////////////////////////////////////////////
////////////////////////////////////////////////
////////////////////////////////////////////////
?>

<aside class="right-side">
   <section class="content-header">
      <h1>
         Service %
      </h1>
   </section>

   <section class="content">
      <div class="row col-lg-12">
         <div class="box box-solid">
            <form action="ereport.php" method="get" id="clientList" class="sidebar-form" style="border:0px;">
               <div class="form-group" style="padding-top:10px; padding-bottom:1px;margin-bottom:0px;">
                  <div class="callout callout-info">
                     <h4>Service % Details</h4>
                     <p>In this report, you will be able to view response % per service for the date range specified below.</p>
                  </div>
                  <div id="reportrange" class="select pull-left" style="cursor: pointer;">
                     <i class="fa fa-calendar fa-lg"></i>
                     <span style="font-size:15px"><?php echo $startDateConv . " - " . $endDateConv; ?></span><b class="caret"></b>
                  </div>
                  <br>
                  <div class="form-group" style="margin-bottom:0px;">
                     <br>
                     <label>Client List</label>
                     <select class="form-control" name="client" id="client" form="clientList" OnChange="reloadOnSelect(this.value);">
                        <?php
//if($cl == '0')
                        {
                           echo '<option SELECTED>Please Select</option>';
                        }
                        foreach ($clients as $key => $value) {
                           $sId = $value['service_id'];
                           if ($cl == $sId) {
                              $accountName = $value['account_name'];
                              $serviceName = $value['service_name'];
                              echo "<option name='" . $sId . "' SELECTED>" . $accountName . " - " . $serviceName . " - " . $sId . "</option>";
                           } else {
                              echo "<option name='" . $sId . "'>" . $value['account_name'] . " - " . $value['service_name'] . " - " . $sId . "</option>";
                           }
                        }
                        ?>
                     </select>
                  </div>
               </div>
               <br>
            </form>
         </div>

         <div class="box-body">
            <div class="box-body table-responsive no-padding">
               <table id="example2"  class="table table-hover">
                  <thead>
                     <tr>
                        <th style="width:100px;">Date</th>
                        <th>Network</th>
                        <th>SMSC</th>
                        <th style="background-color:#737AA6"><center>SENT</center></th>
                  <th style="background-color:#737AA6"><center>UNITS</center></th>
                  <th style="background-color:#DEA52F"><center>PEND</center></th>
                  <th style="background-color:#DEA52F"><center>PEND&nbsp;%</center></th>
                  <th style="background-color:#8BA808"><center>DELV</center></th>
                  <th style="background-color:#8BA808"><center>DELV&nbsp;%</center></th>
                  <th style="background-color:#f56954"><center>FAIL</center></th>
                  <th style="background-color:#f56954"><center>FAIL&nbsp;%</center></th>
                  <th style="background-color:#B8252B"><center>REJ</center></th>
                  <th style="background-color:#B8252B"><center>REJ&nbsp;%</center></th>
                  <th style="background-color:#979696"><center>EXC</center></th>
                  <th style="background-color:#979696"><center>EXC&nbsp;%</center></th>
                  <th style="background-color:#92e7ed"><center>MO&nbsp;Total</center></th>
                  <th style="background-color:#92e7ed"><center>MO/Delv&nbsp;%</center></th>
                  </tr>
                  </thead>
                  <tbody>
                     <?php
                     $sentCount = 0;
                     if (isset($allStats)) {
                        $dlrArr681 = array();
                        $dlrArr680 = array();
                        $dlrArr682 = array();
                        $dlrArr4288 = array();
                        $dlrArrREJ = array();

                        $dlrPos680 = array();
                        $dlrPos681 = array();
                        $dlrPos682 = array();
                        $dlrPosREJ = array();

                        $networkCount = array();
                        $smscCount = array();
                        $unitsCount = 0;
                        $pendingCount = 0;
                        $pendingPCount = 0;
                        $delCount = 0;
                        $delPCount = 0;
                        $failCount = 0;
                        $failPCount = 0;
                        $rejCount = 0;
                        $rejPCount = 0;
                        $exCount = 0;
                        //$exPCount = 0;
                        $moCount = 0;
                        $moPCount = 0;
                        $rowCount = 0;

                        foreach ($allStats as $key => $value) {
                           if ($key == 'aggregations') {
                              for ($i = 0; $i < count($value['5']['buckets']); $i++) {
                                 for ($j = 0; $j < count($value['5']['buckets'][$i]['mccmnc']['buckets']); $j++) {
                                    for ($l = 0; $l < count($value['5']['buckets'][$i]['mccmnc']['buckets'][$j]['smsc']['buckets']); $l++) {
                                       $smsc = $value['5']['buckets'][$i]['mccmnc']['buckets'][$j]['smsc']['buckets'][$l]['key'];
                                       $posSmsc = strrpos($smsc, '_');
                                       $smsc = ucfirst(substr($smsc, 0, $posSmsc));

                                       if (!in_array($value['5']['buckets'][$i]['mccmnc']['buckets'][$j]['smsc']['buckets'][$l]['key'], $smscCount)) {
                                          array_push($smscCount, $value['5']['buckets'][$i]['mccmnc']['buckets'][$j]['smsc']['buckets'][$l]['key']);
                                       }

                                       echo '<tr>';
                                       $rowCount++;
                                       //echo '<br>rc='.$rowCount;
                                       $dater = strstr($value['5']['buckets'][$i]['key_as_string'], 'T', true);
                                       echo '<td>' . $dater . '</td>';
                                       $dater2 = strtotime($dater);
                                       $mcc = $value['5']['buckets'][$i]['mccmnc']['buckets'][$j]['key'];
                                       $netw = getNetFromMCC($mcc);
                                       //$netw = getNetFromMCC($value['5']['buckets'][$i]['mccmnc']['buckets'][$j]['key']);
                                       if (!in_array($value['5']['buckets'][$i]['mccmnc']['buckets'][$j]['key'], $networkCount)) {
                                          array_push($networkCount, $value['5']['buckets'][$i]['mccmnc']['buckets'][$j]['key']);
                                       }

                                       $counter = 0;
                                       foreach ($serProviders as $key3 => $value3) {
                                          $value3 = strstr($value3, '.', true);

                                          if (!empty($value3) && strpos($netw, $value3) !== false) {
                                             echo "<td><img src='../img/serviceproviders/" . $value3 . ".png'>&nbsp;&nbsp;" . ucfirst($netw) . ' (' . $value['5']['buckets'][$i]['mccmnc']['buckets'][$j]['key'] . ')</td>';
                                             $counter++;
                                             break;
                                          }
                                       }
                                       if ($counter == 0) {
                                          echo "<td><img src='../img/serviceproviders/Special.png'>&nbsp;&nbsp;" . ucfirst($netw) . ' (' . $value['5']['buckets'][$i]['mccmnc']['buckets'][$j]['key'] . ')</td>';
                                       }

                                       $counter = 0;
                                       foreach ($serProviders as $key3 => $value3) {
                                          if ($value3 == (lcfirst($smsc) . '.png')) {
                                             echo "<td><img src='../img/serviceproviders/" . lcfirst($smsc) . ".png'>&nbsp;&nbsp;" . $smsc . '</td>';
                                             $counter++;
                                          }
                                       }
                                       if ($counter == 0) {
                                          echo "<td><img src='../img/serviceproviders/Special.png'>&nbsp;&nbsp;" . $smsc . '</td>';
                                       }

                                       echo '<td style="background-color:#adb1cb; text-align:right"><a style="color:#000000;cursor:pointer;" onclick="viewMsg(' . urlencode($dater2) . ', `' . $mcc . '`, `' . $value['5']['buckets'][$i]['mccmnc']['buckets'][$j]['smsc']['buckets'][$l]['key'] . '`)" >' . number_format($value['5']['buckets'][$i]['mccmnc']['buckets'][$j]['smsc']['buckets'][$l]['doc_count'], 0, '.', '&nbsp;') . '</a></td>';
                                       $sentCount += $value['5']['buckets'][$i]['mccmnc']['buckets'][$j]['smsc']['buckets'][$l]['doc_count'];
                                       //echo '<br>sc='.$sentCount;
                                       echo '<td style="background-color:#adb1cb; text-align:right">' . number_format($value['5']['buckets'][$i]['mccmnc']['buckets'][$j]['smsc']['buckets'][$l]['1']['value'], 0, '.', '&nbsp;') . '</td>';
                                       $unitsCount += $value['5']['buckets'][$i]['mccmnc']['buckets'][$j]['smsc']['buckets'][$l]['1']['value'];

                                       $dlrArr680[$value['5']['buckets'][$i]['mccmnc']['buckets'][$j]['smsc']['buckets'][$l]['key']] = 0;
                                       $dlrArr681[$value['5']['buckets'][$i]['mccmnc']['buckets'][$j]['smsc']['buckets'][$l]['key']] = 0;
                                       $dlrArr682[$value['5']['buckets'][$i]['mccmnc']['buckets'][$j]['smsc']['buckets'][$l]['key']] = 0;
                                       $dlrArr4288[$value['5']['buckets'][$i]['mccmnc']['buckets'][$j]['smsc']['buckets'][$l]['key']] = 0;
                                       $dlrArrREJ[$value['5']['buckets'][$i]['mccmnc']['buckets'][$j]['smsc']['buckets'][$l]['key']] = 0;

                                       for ($k = 0; $k < count($value['5']['buckets'][$i]['mccmnc']['buckets'][$j]['smsc']['buckets'][$l]['dlr']['buckets']); $k++) {
                                          $dlrPos = (integer) $value['5']['buckets'][$i]['mccmnc']['buckets'][$j]['smsc']['buckets'][$l]['dlr']['buckets'][$k]['key'];
                                          //if(in_array('680', $value['5']['buckets'][$i]['mccmnc']['buckets'][$j]['smsc']['buckets'][$l]['dlr']['buckets'][$k]))
                                          if (($dlrPos & 32) == 32 && ($dlrPos & 1) == 0 && ($dlrPos & 2) == 0 && ($dlrPos & 16) == 0) {
                                             $countRdnc = count($value['5']['buckets'][$i]['mccmnc']['buckets'][$j]['smsc']['buckets'][$l]['dlr']['buckets'][$k]['rdnc']['buckets']);
                                             for ($jj = 0; $jj < $countRdnc; $jj++) {
                                                //if($value['5']['buckets'][$i]['mccmnc']['buckets'][$j]['smsc']['buckets'][$l]['dlr']['buckets'][$k]['rdnc']['buckets'][$jj]['key'] == 0)
                                                {
                                                   if (!in_array($dlrPos, $dlrPos680)) {
                                                      array_push($dlrPos680, $dlrPos);
                                                   }
                                                   if ($dlrArr680[$value['5']['buckets'][$i]['mccmnc']['buckets'][$j]['smsc']['buckets'][$l]['key']] == 0) {
                                                      //$dlrArr680['5']['buckets']['0']['dlr']['buckets'] = $value['5']['buckets']['0']['dlr']['buckets'][$i]['rdnc']['buckets'][$j]['1']['value'];
                                                      $dlrArr680[$value['5']['buckets'][$i]['mccmnc']['buckets'][$j]['smsc']['buckets'][$l]['key']] = $value['5']['buckets'][$i]['mccmnc']['buckets'][$j]['smsc']['buckets'][$l]['dlr']['buckets'][$k]['rdnc']['buckets'][$jj]['doc_count'];
                                                   } else {
                                                      $dlrArr680[$value['5']['buckets'][$i]['mccmnc']['buckets'][$j]['smsc']['buckets'][$l]['key']] += $value['5']['buckets'][$i]['mccmnc']['buckets'][$j]['smsc']['buckets'][$l]['dlr']['buckets'][$k]['rdnc']['buckets'][$jj]['doc_count'];
                                                   }
                                                }
                                             }
                                             /* if($dlrArr680[$value['5']['buckets'][$i]['mccmnc']['buckets'][$j]['smsc']['buckets'][$l]['key']] == 0)
                                               {
                                               $dlrArr680[$value['5']['buckets'][$i]['mccmnc']['buckets'][$j]['smsc']['buckets'][$l]['key']] = $value['5']['buckets'][$i]['mccmnc']['buckets'][$j]['smsc']['buckets'][$l]['dlr']['buckets'][$k]['doc_count'];
                                               }
                                               else
                                               {
                                               $dlrArr680[$value['5']['buckets'][$i]['mccmnc']['buckets'][$j]['smsc']['buckets'][$l]['key']] += $value['5']['buckets'][$i]['mccmnc']['buckets'][$j]['smsc']['buckets'][$l]['dlr']['buckets'][$k]['doc_count'];
                                               } */
                                          }
                                          if (($dlrPos & 1) == 1) {
                                             if (!in_array($dlrPos, $dlrPos681)) {
                                                array_push($dlrPos681, $dlrPos);
                                             }
                                             if ($dlrArr681[$value['5']['buckets'][$i]['mccmnc']['buckets'][$j]['smsc']['buckets'][$l]['key']] == 0) {
                                                $dlrArr681[$value['5']['buckets'][$i]['mccmnc']['buckets'][$j]['smsc']['buckets'][$l]['key']] = $value['5']['buckets'][$i]['mccmnc']['buckets'][$j]['smsc']['buckets'][$l]['dlr']['buckets'][$k]['doc_count'];
                                             } else {
                                                $dlrArr681[$value['5']['buckets'][$i]['mccmnc']['buckets'][$j]['smsc']['buckets'][$l]['key']] += $value['5']['buckets'][$i]['mccmnc']['buckets'][$j]['smsc']['buckets'][$l]['dlr']['buckets'][$k]['doc_count'];
                                             }
                                          } elseif (($dlrPos & 2) == 2) {
                                             if (!in_array($dlrPos, $dlrPos682)) {
                                                array_push($dlrPos682, $dlrPos);
                                             }
                                             if ($dlrArr682[$value['5']['buckets'][$i]['mccmnc']['buckets'][$j]['smsc']['buckets'][$l]['key']] == 0) {
                                                $dlrArr682[$value['5']['buckets'][$i]['mccmnc']['buckets'][$j]['smsc']['buckets'][$l]['key']] = $value['5']['buckets'][$i]['mccmnc']['buckets'][$j]['smsc']['buckets'][$l]['dlr']['buckets'][$k]['doc_count'];
                                             } else {
                                                $dlrArr682[$value['5']['buckets'][$i]['mccmnc']['buckets'][$j]['smsc']['buckets'][$l]['key']] += $value['5']['buckets'][$i]['mccmnc']['buckets'][$j]['smsc']['buckets'][$l]['dlr']['buckets'][$k]['doc_count'];
                                             }
                                          }
                                          $countRdnc = count($value['5']['buckets'][$i]['mccmnc']['buckets'][$j]['smsc']['buckets'][$l]['dlr']['buckets'][$k]['rdnc']['buckets']);
                                          for ($jj = 0; $jj < $countRdnc; $jj++) {
                                             if ($value['5']['buckets'][$i]['mccmnc']['buckets'][$j]['smsc']['buckets'][$l]['dlr']['buckets'][$k]['rdnc']['buckets'][$jj]['key'] != 0) {
                                                if ($dlrArr4288[$value['5']['buckets'][$i]['mccmnc']['buckets'][$j]['smsc']['buckets'][$l]['key']] == 0) {
                                                   //$dlrArr4288['5']['buckets']['0']['dlr']['buckets'] = $value['5']['buckets']['0']['dlr']['buckets'][$i]['rdnc']['buckets'][$j]['1']['value'];
                                                   $dlrArr4288[$value['5']['buckets'][$i]['mccmnc']['buckets'][$j]['smsc']['buckets'][$l]['key']] = $value['5']['buckets'][$i]['mccmnc']['buckets'][$j]['smsc']['buckets'][$l]['dlr']['buckets'][$k]['rdnc']['buckets'][$jj]['doc_count'];
                                                } else {
                                                   $dlrArr4288[$value['5']['buckets'][$i]['mccmnc']['buckets'][$j]['smsc']['buckets'][$l]['key']] += $value['5']['buckets'][$i]['mccmnc']['buckets'][$j]['smsc']['buckets'][$l]['dlr']['buckets'][$k]['rdnc']['buckets'][$jj]['doc_count'];
                                                }
                                             }
                                          }
                                          if (($dlrPos & 16) == 16) {
                                             if (!in_array($dlrPos, $dlrPosREJ)) {
                                                array_push($dlrPosREJ, $dlrPos);
                                             }
                                             if ($dlrArrREJ[$value['5']['buckets'][$i]['mccmnc']['buckets'][$j]['smsc']['buckets'][$l]['key']] == 0) {
                                                $dlrArrREJ[$value['5']['buckets'][$i]['mccmnc']['buckets'][$j]['smsc']['buckets'][$l]['key']] = $value['5']['buckets'][$i]['mccmnc']['buckets'][$j]['smsc']['buckets'][$l]['dlr']['buckets'][$k]['doc_count'];
                                             } else {
                                                $dlrArrREJ[$value['5']['buckets'][$i]['mccmnc']['buckets'][$j]['smsc']['buckets'][$l]['key']] += $value['5']['buckets'][$i]['mccmnc']['buckets'][$j]['smsc']['buckets'][$l]['dlr']['buckets'][$k]['doc_count'];
                                             }
                                          }
                                       }

                                       $pendUrlAttach = '';
                                       for ($il = 0; $il < count($dlrPos680); $il++) {
                                          $pendUrlAttach .= (string) $dlrPos680[$il] . '.';
                                       }
                                       $pendUrlAttach = substr($pendUrlAttach, 0, -1);
                                       $pendUrlAttach = ("'" . $pendUrlAttach . "'");

                                       $deliUrlAttach = '';
                                       for ($il = 0; $il < count($dlrPos681); $il++) {
                                          $deliUrlAttach .= (string) $dlrPos681[$il] . '.';
                                       }
                                       $deliUrlAttach = substr($deliUrlAttach, 0, -1);
                                       $deliUrlAttach = ("'" . $deliUrlAttach . "'");

                                       $failUrlAttach = '';
                                       for ($il = 0; $il < count($dlrPos682); $il++) {
                                          $failUrlAttach .= (string) $dlrPos682[$il] . '.';
                                       }
                                       $failUrlAttach = substr($failUrlAttach, 0, -1);
                                       $failUrlAttach = ("'" . $failUrlAttach . "'");

                                       $rejeUrlAttach = '';
                                       for ($il = 0; $il < count($dlrPosREJ); $il++) {
                                          $rejeUrlAttach .= (string) $dlrPosREJ[$il] . '.';
                                       }
                                       $rejeUrlAttach = substr($rejeUrlAttach, 0, -1);
                                       $rejeUrlAttach = ("'" . $rejeUrlAttach . "'");

                                       $cnt = 0;
                                       foreach ($dlrArr680 as $key2 => $value2) {
                                          if ($key2 == $value['5']['buckets'][$i]['mccmnc']['buckets'][$j]['smsc']['buckets'][$l]['key']) {
                                             $cnt = 1;
                                             if ($value2 == 0) {
                                                echo '<td style="background-color:#e9cb8c; text-align:right">0</td>';
                                             } else {
                                                echo '<td style="background-color:#e9cb8c; text-align:right"><a style="color:#000000;cursor:pointer;" onclick="viewMsgVar(' . urlencode($dater2) . ', ' . $pendUrlAttach . ',' . $pendingCount . ',`' . $mcc . '`, `' . $value['5']['buckets'][$i]['mccmnc']['buckets'][$j]['smsc']['buckets'][$l]['key'] . '`,`Pending`)">' . number_format($value2, 0, '.', '&nbsp;') . '</a></td>';
                                             }
                                             echo '<td style="background-color:#e9cb8c; text-align:right">' . number_format(($value2 / $value['5']['buckets'][$i]['mccmnc']['buckets'][$j]['smsc']['buckets'][$l]['doc_count'] * 100), 2) . ' %</td>';
                                             //array_shift($dlrArr680);
                                             $pendingCount += $value2;
                                             $pendingPCount += number_format(($value2 / $value['5']['buckets'][$i]['mccmnc']['buckets'][$j]['smsc']['buckets'][$l]['doc_count'] * 100), 2);
                                          }
                                       }
                                       if ($cnt == 0) {
                                          echo '<td style="background-color:#e9cb8c;">' . '' . '</td>';
                                          echo '<td style="background-color:#e9cb8c;">' . '' . '</td>';
                                       }

                                       $cnt = 0;
                                       foreach ($dlrArr681 as $key2 => $value2) {
                                          if ($key2 == $value['5']['buckets'][$i]['mccmnc']['buckets'][$j]['smsc']['buckets'][$l]['key']) {
                                             $cnt = 1;
                                             $delCount += $value2;
                                             $delPCount += number_format(($value2 / $value['5']['buckets'][$i]['mccmnc']['buckets'][$j]['smsc']['buckets'][$l]['doc_count'] * 100), 2);
                                             if ($value2 == 0) {
                                                echo '<td style="background-color:#c0eccd; text-align:right">0</td>';
                                             } else {
                                                echo '<td style="background-color:#c0eccd; text-align:right"><a style="color:#000000;cursor:pointer;" onclick="viewMsgVar(' . urlencode($dater2) . ', ' . $deliUrlAttach . ',' . $delCount . ',`' . $mcc . '`, `' . $value['5']['buckets'][$i]['mccmnc']['buckets'][$j]['smsc']['buckets'][$l]['key'] . '`,`Delivered`)">' . number_format($value2, 0, '.', '&nbsp;') . '</a></td>';
                                             }
                                             echo '<td style="background-color:#c0eccd; text-align:right">' . number_format(($value2 / $value['5']['buckets'][$i]['mccmnc']['buckets'][$j]['smsc']['buckets'][$l]['doc_count'] * 100), 2) . ' %</td>';
                                             //array_shift($dlrArr681);
                                          }
                                       }
                                       if ($cnt == 0) {
                                          echo '<td style="background-color:#c0eccd;">' . '' . '</td>';
                                          echo '<td style="background-color:#c0eccd;">' . '' . '</td>';
                                       }

                                       $cnt = 0;
                                       foreach ($dlrArr682 as $key2 => $value2) {
                                          if ($key2 == $value['5']['buckets'][$i]['mccmnc']['buckets'][$j]['smsc']['buckets'][$l]['key']) {
                                             $cnt = 1;
                                             $failCount += $value2;
                                             $failPCount += number_format(($value2 / $value['5']['buckets'][$i]['mccmnc']['buckets'][$j]['smsc']['buckets'][$l]['doc_count'] * 100), 2);
                                             $tempFail = $value2;
                                             if ($value2 == 0) {
                                                echo '<td style="background-color:#ecc0c0; text-align:right">0</td>';
                                             } else {
                                                echo '<td style="background-color:#ecc0c0; text-align:right"><a style="color:#000000;cursor:pointer;" onclick="viewMsgVar(' . urlencode($dater2) . ', ' . $failUrlAttach . ',' . $failCount . ',`' . $mcc . '`, `' . $value['5']['buckets'][$i]['mccmnc']['buckets'][$j]['smsc']['buckets'][$l]['key'] . '`,`Failed`)">' . number_format($value2, 0, '.', '&nbsp;') . '</a></td>';
                                             }
                                             echo '<td style="background-color:#ecc0c0; text-align:right">' . number_format(($value2 / $value['5']['buckets'][$i]['mccmnc']['buckets'][$j]['smsc']['buckets'][$l]['doc_count'] * 100), 2) . ' %</td>';
                                             //array_shift($dlrArr682);
                                          }
                                       }
                                       if ($cnt == 0) {
                                          echo '<td style="background-color:#ecc0c0;">' . '' . '</td>';
                                          echo '<td style="background-color:#ecc0c0;">' . '' . '</td>';
                                       }

                                       $cnt = 0;
                                       foreach ($dlrArrREJ as $key2 => $value2) {
                                          if ($key2 == $value['5']['buckets'][$i]['mccmnc']['buckets'][$j]['smsc']['buckets'][$l]['key']) {
                                             $cnt = 1;
                                             $rejCount += $value2;
                                             $rejPCount += number_format(($value2 / $value['5']['buckets'][$i]['mccmnc']['buckets'][$j]['smsc']['buckets'][$l]['doc_count'] * 100), 2);
                                             if ($value2 == 0) {
                                                echo '<td style="background-color:#dbb2b2; text-align:right">0</td>';
                                             } else {
                                                echo '<td style="background-color:#dbb2b2; text-align:right"><a style="color:#000000;cursor:pointer;" onclick="viewMsgVar(' . urlencode($dater2) . ', ' . $rejeUrlAttach . ',' . $rejCount . ',`' . $mcc . '`, `' . $value['5']['buckets'][$i]['mccmnc']['buckets'][$j]['smsc']['buckets'][$l]['key'] . '`,`Rejected`)">' . number_format($value2, 0, '.', '&nbsp;') . '</a></td>';
                                             }
                                             echo '<td style="background-color:#dbb2b2; text-align:right">' . number_format(($value2 / $value['5']['buckets'][$i]['mccmnc']['buckets'][$j]['smsc']['buckets'][$l]['doc_count'] * 100), 2) . ' %</td>';
                                             //array_shift($dlrArr680);
                                          }
                                       }
                                       if ($cnt == 0) {
                                          echo '<td style="background-color:#dbb2b2;">' . '' . '</td>';
                                          echo '<td style="background-color:#dbb2b2;">' . '' . '</td>';
                                       }

                                       $cnt = 0;
                                       foreach ($dlrArr4288 as $key2 => $value2) {
                                          if ($key2 == $value['5']['buckets'][$i]['mccmnc']['buckets'][$j]['smsc']['buckets'][$l]['key']) {
                                             $cnt = 1;
                                             echo '<td style="background-color:#b1b1b1; text-align:right">' . $value2 . '</td>';
                                             if ($tempFail == 0) {
                                                echo '<td style="background-color:#b1b1b1; text-align:right">0.00 %</td>';
                                             } else {
                                                //echo '<td style="background-color:#b1b1b1; text-align:right">'.number_format(($value2/$tempFail*100), 2).' %</td>';
                                                echo '<td style="background-color:#b1b1b1; text-align:right">' . number_format(($value2 / $tempFail * 100), 2) . ' %</td>';
                                             }
                                             //array_shift($dlrArr682);
                                             $exCount += $value2;
                                             //$exPCount += number_format(($value2/$value['5']['buckets'][$i]['mccmnc']['buckets'][$j]['smsc']['buckets'][$l]['doc_count']*100), 2);
                                          }
                                       }
                                       if ($cnt == 0) {
                                          echo '<td style="background-color:#b1b1b1;">' . '' . '</td>';
                                          echo '<td style="background-color:#b1b1b1;">' . '' . '</td>';
                                       }

                                       $cnt = 0;
                                       foreach ($moData as $key4 => $value4) {
                                          if ($dater == $value4['timestamp']) {
                                             if (($value4['mccmnc'] == $value['5']['buckets'][$i]['mccmnc']['buckets'][$j]['key']) && ($value4['smsc'] == $value['5']['buckets'][$i]['mccmnc']['buckets'][$j]['smsc']['buckets'][$l]['key'])) {
                                                $cnt = 1;
                                                echo '<td style="background-color:#cfebed; text-align:right"><a style="color:#333333;cursor:pointer;" onclick="gotoMo(' . urlencode($dater2) . ', ' . $cl . ', ' . $mcc . ', `' . $value['5']['buckets'][$i]['mccmnc']['buckets'][$j]['smsc']['buckets'][$l]['key'] . '`);">' . number_format($value4['id'], 0, '.', '&nbsp;') . '</a></td>';
                                                $moCount += $value4['id'];
                                                if ($dlrArr681[$value4['smsc']] != 0) {
                                                   echo '<td style="background-color:#cfebed; text-align:right">' . round(($value4['id'] / $dlrArr681[$value4['smsc']] * 100), 2) . ' %</td>';
                                                   $moPCount += round(($value4['id'] / $dlrArr681[$value4['smsc']] * 100), 2);
                                                } else {
                                                   echo '<td style="background-color:#cfebed;">' . '' . '</td>';
                                                }
                                             }
                                          }
                                       }
                                       if ($cnt == 0) {
                                          echo '<td style="background-color:#cfebed;">' . '' . '</td>';
                                          echo '<td style="background-color:#cfebed;">' . '' . '</td>';
                                       }
                                       echo '</tr>';
                                    }
                                 }
                              }
                           }
                        }
                     }
                     ?>
                  </tbody>
                  <?php
                  if ($fileName != 'Please Select') {
                     ?>
                     <tfoot>
                        <tr>
                           <td>Totals:</td>
                           <td><b><?php echo count($networkCount); ?></b></td>
                           <td><b><?php echo count($smscCount); ?></b></td>
                           <td style = "background-color:#e1e4ef; text-align:right;"><b><?php echo number_format($sentCount, 0, '.', '&nbsp;'); ?></b></td>
                           <td style = "background-color:#e1e4ef; text-align:right;"><b><?php echo number_format($unitsCount, 0, '.', '&nbsp;'); ?></b></td>
                           <td style = "background-color:#f2f1d6; text-align:right;"><b><?php echo number_format($pendingCount, 0, '.', '&nbsp;'); ?></b></td>
                           <?php
                           if ($sentCount != 0) {
                              echo '<td style = "background-color:#f2f1d6; text-align:right;"><b>' . round(($pendingCount / $sentCount * 100), 2) . '</b>&nbsp;%</td>';
                           } else {
                              echo '<td style = "background-color:#f2f1d6; text-align:right;"><b>-</b>&nbsp;%</td>';
                           }
                           ?>
                           <td style = "background-color:#d3efdb; text-align:right;"><b><?php echo number_format($delCount, 0, '.', '&nbsp;'); ?></b></td>
                           <?php
                           if ($sentCount != 0) {
                              echo '<td style = "background-color:#d3efdb; text-align:right;"><b>' . round(($delCount / $sentCount * 100), 2) . '</b>&nbsp;%</td>';
                           } else {
                              echo '<td style = "background-color:#d3efdb; text-align:right;"><b>-</b>&nbsp;%</td>';
                           }
                           ?>
                           <td style = "background-color:#f5dcdc; text-align:right;"><b><?php echo number_format($failCount, 0, '.', '&nbsp;'); ?></b></td>
                           <?php
                           if ($sentCount != 0) {
                              echo '<td style = "background-color:#f5dcdc; text-align:right;"><b>' . round(($failCount / $sentCount * 100), 2) . '&nbsp;%</b></td>';
                           } else {
                              echo '<td style = "background-color:#f5dcdc; text-align:right;"><b>-</b>&nbsp;%</td>';
                           }
                           ?>
                           <td style = "background-color:#f2cfcf; text-align:right;"><b><?php echo number_format($rejCount, 0, '.', '&nbsp;'); ?></b></td>
                           <?php
                           if ($sentCount != 0) {
                              echo '<td style = "background-color:#f2cfcf; text-align:right;"><b>' . round(($rejCount / $sentCount * 100), 2) . '&nbsp;%</b></td>';
                              //echo '<td style = "background-color:#d3efdb; text-align:right;"><b>'.round(($delCount/$sentCount*100), 2).'</b>&nbsp;%</td>';
                           } else {
                              echo '<td style = "background-color:#f2cfcf; text-align:right;"><b>-</b>&nbsp;%</td>';
                           }
                           ?>
                           <td style = "background-color:#dddddd; text-align:right;"><b><?php echo number_format($exCount, 0, '.', '&nbsp;'); ?></b></td>
                           <?php
                           if ($sentCount != 0) {
                              echo '<td style = "background-color:#dddddd; text-align:right;"><b>' . round(($exCount / $sentCount * 100), 2) . '&nbsp;%</b></td>';
                              //echo '<td style = "background-color:#d3efdb; text-align:right;"><b>'.round(($delCount/$sentCount*100), 2).'</b>&nbsp;%</td>';
                           } else {
                              echo '<td style = "background-color:#dddddd; text-align:right;"><b>-</b>&nbsp;%</td>';
                           }
                           ?>
                           <td style = "background-color:#e1eff0; text-align:right;"><b><?php echo number_format($moCount, 0, '.', '&nbsp;'); ?></b></td>
                           <?php
                           if ($sentCount != 0) {
                              //echo '<td style = "background-color:#d3efdb; text-align:right;"><b>'.round(($delCount/$sentCount*100), 2).'</b>&nbsp;%</td>';
                              echo '<td style = "background-color:#e1eff0; text-align:right;"><b>' . round(($moCount / $sentCount * 100), 2) . '&nbsp;%</b></td>';
                           } else {
                              echo '<td style = "background-color:#e1eff0; text-align:right;"><b>-</b>&nbsp;%</td>';
                           }
                           ?>
                        </tr>
                     </tfoot>
                     <?php
                  }
                  ?>
               </table>
            </div>
            <div class="box-body">
               <div class="form-group" style="padding-top:30px">
                  <a onclick="saveRep();" class="btn btn-block btn-social btn-dropbox" style="background-color:<?php echo $accRGB; ?>; border: 2px solid; border-radius: 6px !important; float:left; width: 134px; margin-top:-21px;">
                     <i class="fa fa-save"></i>Save Report
                  </a>
               </div>
            </div>
         </div>
      </div>
   </section>
</aside>

<!--  THE JAVASCRIPT IMPORT ALWAYS APPEARS BEFORE LOCAL JAVASCRIPT AT THE BOTTOM OF THE PAGE -->
<?php include("template_import_script.php"); //must import all scripts first ?>
<!-- END JAVASCRIPT IMPORT -->

<script type="text/javascript">
   $(function ()
   {
      $('#example2').dataTable({
         "bPaginate": false,
         "bLengthChange": false,
         "bFilter": true,
         "bSort": true,
         "bInfo": true,
         "bAutoWidth": false,
         "iDisplayLength": 100,
         "aoColumns": [
            null,
            null,
            null,
            {"sType": 'formatted-num', targets: 0},
            {"sType": 'formatted-num', targets: 0},
            {"sType": 'formatted-num', targets: 0},
            {"sType": 'formatted-num', targets: 0},
            {"sType": 'formatted-num', targets: 0},
            {"sType": 'formatted-num', targets: 0},
            {"sType": 'formatted-num', targets: 0},
            {"sType": 'formatted-num', targets: 0},
            {"sType": 'formatted-num', targets: 0},
            {"sType": 'formatted-num', targets: 0},
            {"sType": 'formatted-num', targets: 0},
            {"sType": 'formatted-num', targets: 0},
            {"sType": 'formatted-num', targets: 0},
            {"sType": 'formatted-num', targets: 0}
         ],
      });
   });

   jQuery.extend(jQuery.fn.dataTableExt.oSort,
           {
              "title-numeric-pre": function (a) {
                 var x = a.match(/title="*(-?[0-9\.]+)/)[1];
                 return parseFloat(x);
              },
              "title-numeric-asc": function (a, b) {
                 return ((a < b) ? -1 : ((a > b) ? 1 : 0));
              },
              "title-numeric-desc": function (a, b) {
                 return ((a < b) ? 1 : ((a > b) ? -1 : 0));
              }
           });

   jQuery.extend(jQuery.fn.dataTableExt.oSort,
           {
              "formatted-num-pre": function (a) {
                 a = (a === "-" || a === "") ? 0 : a.replace(/[^\d\-\.]/g, "");
                 return parseFloat(a);
              },
              "formatted-num-asc": function (a, b) {
                 return a - b;
              },
              "formatted-num-desc": function (a, b) {
                 return b - a;
              }
           });
</script>

<script type="text/javascript">
   $('#reportrange').daterangepicker(
           {
              ranges: {
                 'Today': [moment(), moment()],
                 'Yesterday': [moment().subtract('days', 1), moment().subtract('days', 1)],
                 'Last 7 Days': [moment().subtract('days', 6), moment()],
                 'Last 30 Days': [moment().subtract('days', 29), moment()],
                 'This Month': [moment().startOf('month'), moment().endOf('month')],
                 'Last Month': [moment().subtract('month', 1).startOf('month'), moment().subtract('month', 1).endOf('month')]
              },
              startDate: moment(<?php echo $startDate; ?>),
              endDate: moment(<?php echo $endDate; ?>)
           },
           function (start, end) {
              $(".loader").fadeIn("slow");
              $(".loaderIcon").fadeIn("slow");
              $('#reportrange span').html(start + ' - ' + end);
              var serviceName = document.getElementById("client").value;
              var repRange = $("#reportrange span").html();
              //var picker = eventTarget.data('datetimepicker');
              // alert(repRange);

              encodeURI(serviceName);
//                    window.location = "../pages/servicepercent.php?range=" + repRange;
              window.location = "../pages/servicepercentadmin.php?client=" + encodeURIComponent(serviceName) + "&range=" + repRange;
           }
   );
</script>

<script type="text/javascript">
   function reloadOnSelect(name)
   {
      var serviceName = name;
      //var repRange = $("#reportrange span").daterangepicker( 'getDate' ).html();
      //$('#reportrange span').daterangepicker();
      var repRange = $("#reportrange span").html();
      var start = moment(repRange.substr(0, 10)).unix() * 1000;
      //alert('start:' + start);
      //var end = moment(repRange.substr(13, 23)).unix()*1000;
      var end = moment(repRange.substr(13, 23)).add(1, 'day');//
      end = (end.unix() - 1) * 1000;
      //alert('end:' + repRange.substr(13, 23));
      repRange = start + ' - ' + end;

      //alert(repRange);

      //var serviceName = document.getElementById("client").value;
      encodeURI(serviceName);
      window.location = "../pages/servicepercentadmin.php?client=" + encodeURIComponent(serviceName) + "&range=" + repRange;
   }
</script>

<script type="text/javascript">
   function saveRep()
   {
      var csvContent = "data:text/csv;charset=utf-8,";

      var data = [["Date", "Network", "SMSC", "Sent", "Units", "Pending", "Pending %", "Delivered", "Delivered %", "Failed", "Failed %", "Rejected", "Rejected %", "Excluded", "Excluded %", "MO Total", "MO/Delivered %"],
<?php
if (isset($allStats)) {
   foreach ($allStats as $key => $value) {
      if ($key == 'aggregations') {
         for ($i = 0; $i < count($value['5']['buckets']); $i++) {
            for ($j = 0; $j < count($value['5']['buckets'][$i]['mccmnc']['buckets']); $j++) {
               for ($l = 0; $l < count($value['5']['buckets'][$i]['mccmnc']['buckets'][$j]['smsc']['buckets']); $l++) {
                  $smsc = $value['5']['buckets'][$i]['mccmnc']['buckets'][$j]['smsc']['buckets'][$l]['key'];
                  $posSmsc = strrpos($smsc, '_');
                  $smsc = ucfirst(substr($smsc, 0, $posSmsc));

                  echo '[';
                  $dater = strstr($value['5']['buckets'][$i]['key_as_string'], 'T', true);
                  echo '"' . $dater . '",';

                  $netw = getNetFromMCC($value['5']['buckets'][$i]['mccmnc']['buckets'][$j]['key']);
                  $counter = 0;


                  echo '"' . ucfirst($netw) . ' (' . $value['5']['buckets'][$i]['mccmnc']['buckets'][$j]['key'] . ')",';
                  echo '"' . $smsc . '",';
                  echo '"' . $value['5']['buckets'][$i]['mccmnc']['buckets'][$j]['smsc']['buckets'][$l]['doc_count'] . '",';
                  echo '"' . $value['5']['buckets'][$i]['mccmnc']['buckets'][$j]['smsc']['buckets'][$l]['1']['value'] . '",';

                  $dlrArr680[$value['5']['buckets'][$i]['mccmnc']['buckets'][$j]['smsc']['buckets'][$l]['key']] = 0;
                  $dlrArr681[$value['5']['buckets'][$i]['mccmnc']['buckets'][$j]['smsc']['buckets'][$l]['key']] = 0;
                  $dlrArr682[$value['5']['buckets'][$i]['mccmnc']['buckets'][$j]['smsc']['buckets'][$l]['key']] = 0;
                  $dlrArr4288[$value['5']['buckets'][$i]['mccmnc']['buckets'][$j]['smsc']['buckets'][$l]['key']] = 0;
                  $dlrArrREJ[$value['5']['buckets'][$i]['mccmnc']['buckets'][$j]['smsc']['buckets'][$l]['key']] = 0;

                  for ($k = 0; $k < count($value['5']['buckets'][$i]['mccmnc']['buckets'][$j]['smsc']['buckets'][$l]['dlr']['buckets']); $k++) {
                     $dlrPos = (integer) $value['5']['buckets'][$i]['mccmnc']['buckets'][$j]['smsc']['buckets'][$l]['dlr']['buckets'][$k]['key'];
                     //if(in_array('680', $value['5']['buckets'][$i]['mccmnc']['buckets'][$j]['smsc']['buckets'][$l]['dlr']['buckets'][$k]))
                     if (($dlrPos & 32) == 32 && ($dlrPos & 1) == 0 && ($dlrPos & 2) == 0 && ($dlrPos & 16) == 0) {
                        $countRdnc = count($value['5']['buckets'][$i]['mccmnc']['buckets'][$j]['smsc']['buckets'][$l]['dlr']['buckets'][$k]['rdnc']['buckets']);
                        for ($jj = 0; $jj < $countRdnc; $jj++) {
                           //if($value['5']['buckets'][$i]['mccmnc']['buckets'][$j]['smsc']['buckets'][$l]['dlr']['buckets'][$k]['rdnc']['buckets'][$jj]['key'] == 0) 
                           {
                              if ($dlrArr680[$value['5']['buckets'][$i]['mccmnc']['buckets'][$j]['smsc']['buckets'][$l]['key']] == 0) {
                                 //$dlrArr680['5']['buckets']['0']['dlr']['buckets'] = $value['5']['buckets']['0']['dlr']['buckets'][$i]['rdnc']['buckets'][$j]['1']['value'];
                                 $dlrArr680[$value['5']['buckets'][$i]['mccmnc']['buckets'][$j]['smsc']['buckets'][$l]['key']] = $value['5']['buckets'][$i]['mccmnc']['buckets'][$j]['smsc']['buckets'][$l]['dlr']['buckets'][$k]['rdnc']['buckets'][$jj]['doc_count'];
                              } else {
                                 $dlrArr680[$value['5']['buckets'][$i]['mccmnc']['buckets'][$j]['smsc']['buckets'][$l]['key']] += $value['5']['buckets'][$i]['mccmnc']['buckets'][$j]['smsc']['buckets'][$l]['dlr']['buckets'][$k]['rdnc']['buckets'][$jj]['doc_count'];
                              }
                           }
                        }
                     }
                     if (($dlrPos & 1) == 1) {
                        if ($dlrArr681[$value['5']['buckets'][$i]['mccmnc']['buckets'][$j]['smsc']['buckets'][$l]['key']] == 0) {
                           $dlrArr681[$value['5']['buckets'][$i]['mccmnc']['buckets'][$j]['smsc']['buckets'][$l]['key']] = $value['5']['buckets'][$i]['mccmnc']['buckets'][$j]['smsc']['buckets'][$l]['dlr']['buckets'][$k]['doc_count'];
                        } else {
                           $dlrArr681[$value['5']['buckets'][$i]['mccmnc']['buckets'][$j]['smsc']['buckets'][$l]['key']] += $value['5']['buckets'][$i]['mccmnc']['buckets'][$j]['smsc']['buckets'][$l]['dlr']['buckets'][$k]['doc_count'];
                        }
                     }
                     if (($dlrPos & 2) == 2) {
                        if ($dlrArr682[$value['5']['buckets'][$i]['mccmnc']['buckets'][$j]['smsc']['buckets'][$l]['key']] == 0) {
                           $dlrArr682[$value['5']['buckets'][$i]['mccmnc']['buckets'][$j]['smsc']['buckets'][$l]['key']] = $value['5']['buckets'][$i]['mccmnc']['buckets'][$j]['smsc']['buckets'][$l]['dlr']['buckets'][$k]['doc_count'];
                        } else {
                           $dlrArr682[$value['5']['buckets'][$i]['mccmnc']['buckets'][$j]['smsc']['buckets'][$l]['key']] += $value['5']['buckets'][$i]['mccmnc']['buckets'][$j]['smsc']['buckets'][$l]['dlr']['buckets'][$k]['doc_count'];
                        }
                     }
                     $countRdnc = count($value['5']['buckets'][$i]['mccmnc']['buckets'][$j]['smsc']['buckets'][$l]['dlr']['buckets'][$k]['rdnc']['buckets']);
                     for ($jj = 0; $jj < $countRdnc; $jj++) {
                        if ($value['5']['buckets'][$i]['mccmnc']['buckets'][$j]['smsc']['buckets'][$l]['dlr']['buckets'][$k]['rdnc']['buckets'][$jj]['key'] != 0) {
                           if ($dlrArr4288[$value['5']['buckets'][$i]['mccmnc']['buckets'][$j]['smsc']['buckets'][$l]['key']] == 0) {
                              //$dlrArr4288['5']['buckets']['0']['dlr']['buckets'] = $value['5']['buckets']['0']['dlr']['buckets'][$i]['rdnc']['buckets'][$j]['1']['value'];
                              $dlrArr4288[$value['5']['buckets'][$i]['mccmnc']['buckets'][$j]['smsc']['buckets'][$l]['key']] = $value['5']['buckets'][$i]['mccmnc']['buckets'][$j]['smsc']['buckets'][$l]['dlr']['buckets'][$k]['rdnc']['buckets'][$jj]['doc_count'];
                           } else {
                              $dlrArr4288[$value['5']['buckets'][$i]['mccmnc']['buckets'][$j]['smsc']['buckets'][$l]['key']] += $value['5']['buckets'][$i]['mccmnc']['buckets'][$j]['smsc']['buckets'][$l]['dlr']['buckets'][$k]['rdnc']['buckets'][$jj]['doc_count'];
                           }
                        }
                     }
                     if (($dlrPos & 16) == 16) {
                        if ($dlrArrREJ[$value['5']['buckets'][$i]['mccmnc']['buckets'][$j]['smsc']['buckets'][$l]['key']] == 0) {
                           $dlrArrREJ[$value['5']['buckets'][$i]['mccmnc']['buckets'][$j]['smsc']['buckets'][$l]['key']] = $value['5']['buckets'][$i]['mccmnc']['buckets'][$j]['smsc']['buckets'][$l]['dlr']['buckets'][$k]['doc_count'];
                        } else {
                           $dlrArrREJ[$value['5']['buckets'][$i]['mccmnc']['buckets'][$j]['smsc']['buckets'][$l]['key']] += $value['5']['buckets'][$i]['mccmnc']['buckets'][$j]['smsc']['buckets'][$l]['dlr']['buckets'][$k]['doc_count'];
                        }
                     }
                  }
                  $cnt = 0;
                  foreach ($dlrArr680 as $key2 => $value2) {
                     if ($key2 == $value['5']['buckets'][$i]['mccmnc']['buckets'][$j]['smsc']['buckets'][$l]['key']) {
                        $cnt = 1;
                        echo '"' . $value2 . '",';
                        echo '"' . number_format(($value2 / $value['5']['buckets'][$i]['mccmnc']['buckets'][$j]['smsc']['buckets'][$l]['doc_count'] * 100), 2) . '",';
                        //array_shift($dlrArr680);        
                        /* $pendingCount += $value2;
                          $pendingPCount += number_format(($value2/$value['5']['buckets'][$i]['mccmnc']['buckets'][$j]['smsc']['buckets'][$l]['doc_count']*100), 2); */
                     }
                  }
                  if ($cnt == 0) {
                     echo '"0",';
                     echo '"0",';
                  }

                  $cnt = 0;
                  foreach ($dlrArr681 as $key2 => $value2) {
                     if ($key2 == $value['5']['buckets'][$i]['mccmnc']['buckets'][$j]['smsc']['buckets'][$l]['key']) {
                        $cnt = 1;
                        echo '"z' . $value2 . '",';
                        echo '"z' . number_format(($value2 / $value['5']['buckets'][$i]['mccmnc']['buckets'][$j]['smsc']['buckets'][$l]['doc_count'] * 100), 2) . '",';
                        //array_shift($dlrArr680);        
                        /* $pendingCount += $value2;
                          $pendingPCount += number_format(($value2/$value['5']['buckets'][$i]['mccmnc']['buckets'][$j]['smsc']['buckets'][$l]['doc_count']*100), 2); */
                     }
                  }
                  if ($cnt == 0) {
                     echo '"Z0",';
                     echo '"Z0",';
                  }

                  $cnt = 0;
                  foreach ($dlrArr682 as $key2 => $value2) {
                     if ($key2 == $value['5']['buckets'][$i]['mccmnc']['buckets'][$j]['smsc']['buckets'][$l]['key']) {
                        $cnt = 1;
                        echo '"' . $value2 . '",';
                        echo '"' . number_format(($value2 / $value['5']['buckets'][$i]['mccmnc']['buckets'][$j]['smsc']['buckets'][$l]['doc_count'] * 100), 2) . '",';
                        $tempFail = $value2;
                     }
                  }
                  if ($cnt == 0) {
                     echo '"' . '0' . '",';
                     echo '"' . '0' . '",';
                  }

                  $cnt = 0;
                  foreach ($dlrArrREJ as $key2 => $value2) {
                     if ($key2 == $value['5']['buckets'][$i]['mccmnc']['buckets'][$j]['smsc']['buckets'][$l]['key']) {
                        $cnt = 1;
                        echo '"' . $value2 . '",';
                        echo '"' . number_format(($value2 / $value['5']['buckets'][$i]['mccmnc']['buckets'][$j]['smsc']['buckets'][$l]['doc_count'] * 100), 2) . '",';
                        //array_shift($dlrArr682);       
                     }
                  }
                  if ($cnt == 0) {
                     echo '"' . '0' . '",';
                     echo '"' . '0' . '",';
                  }

                  $cnt = 0;
                  foreach ($dlrArr4288 as $key2 => $value2) {
                     if ($key2 == $value['5']['buckets'][$i]['mccmnc']['buckets'][$j]['smsc']['buckets'][$l]['key']) {
                        $cnt = 1;
                        echo '"' . $value2 . '",';
                        if ($tempFail == 0) {
                           echo '"0",';
                           //echo '<td style="background-color:#b1b1b1; text-align:right">0.00 %</td>';
                        } else {
                           $tn = number_format(($value2 / $tempFail * 100), 2);
                           echo '"\"' . $tn . '\"",';
                           //echo '"'.number_format(($value2/$value['5']['buckets'][$i]['mccmnc']['buckets'][$j]['smsc']['buckets'][$l]['doc_count']*100), 2).'",';
                           //echo '<td style="background-color:#b1b1b1; text-align:right">'.number_format(($value2/$tempFail*100), 2).' %</td>';
                        }
                        //array_shift($dlrArr682);       
                     }
                  }
                  if ($cnt == 0) {
                     echo '"' . '0' . '",';
                     echo '"' . '0' . '",';
                  }

                  $cnt = 0;
                  foreach ($moData as $key4 => $value4) {
                     if ($dater == $value4['timestamp']) {
                        if (($value4['mccmnc'] == $value['5']['buckets'][$i]['mccmnc']['buckets'][$j]['key']) && ($value4['smsc'] == $value['5']['buckets'][$i]['mccmnc']['buckets'][$j]['smsc']['buckets'][$l]['key'])) {
                           $cnt = 1;
                           echo '"' . $value4['id'] . '",';
                           if ($dlrArr681[$value4['smsc']] != 0) {
                              echo '"' . round(($value4['id'] / $dlrArr681[$value4['smsc']] * 100), 2) . '"';
                           } else {
                              echo '"' . '0' . '"';
                           }
                        }
                     }
                  }
                  if ($cnt == 0) {
                     echo '"' . '0' . '",';
                     echo '"' . '0' . '"';
                  }
                  echo '],';
               }
            }
         }
      }
   }
}
?>
      ];
      data.forEach(function (infoArray, index)
      {
         dataString = infoArray.join(",");
         csvContent += index < data.length ? dataString + "\n" : dataString;
      });
      var encodedUri = encodeURI(csvContent);
      var link = document.createElement("a");
      link.setAttribute("href", encodedUri);
      link.setAttribute("download", "<?php echo 'ServicePercent_' . $fileName . '_' . $startDateConv . '-' . $endDateConv; ?>.csv");

      link.click();
   }

   function viewMsg(dater, mcc, smsc)
   {
      $(".loader").fadeIn("slow");
      $(".loaderIcon").fadeIn("slow");

      var serviceName = document.getElementById("client").value;
      var repRange1 = $("#reportrange span").html();
      //alert(repRange1);
      //alert(dater);
      var end = dater + (24 * 60 * 60) - 1;//
      //end = (end.unix()-1)*1000;
      var repRange = dater + ' - ' + end;
      //end = (end.unix()-1)*1000;
      console.log(repRange);
      //window.location = "../pages/apitrafficmsg.php?client=" + serviceName + "&range=" + repRange;
      encodeURI(serviceName);
      window.location = "../pages/servicepercentmsgadmin.php?client=" + encodeURIComponent(serviceName) + "&range=" + repRange + "&range1=" + repRange1 + "&type=Sent&sents=" + <?php echo $sentCount; ?> + "&from=1&controller=1&sort=<?php echo $sort; ?>&mcc=`" + mcc + "`&smsc=" + smsc;
   }

   function viewMsgVar(dater, val, valNum, mcc, smsc, t)
   {
      $(".loader").fadeIn("slow");
      $(".loaderIcon").fadeIn("slow");
      var serviceName = document.getElementById("client").value;
      var repRange1 = $("#reportrange span").html();
      /*alert(val);
       alert(valNum);*/
      //var start = moment(dater).unix();
      //var end = dater + (24*60*60) - 1;//
      //end = (end.unix()-1)*1000;
      /*var start = <?php echo $dateArr[0]; ?>;
       var end = <?php echo $dateArr[0]; ?>;//
       var repRange = start + ' - ' + end;*/

      var end = dater + (24 * 60 * 60) - 1;//
      //end = (end.unix()-1)*1000;
      var repRange = dater + ' - ' + end;
      //alert(repRange);
      //alert(val);
      /*var start = moment(repRange1).unix()*1000;
       var end = moment(repRange1).add(1, 'day');//
       end = (end.unix()-1)*1000;
       var repRange = start + ' - ' + end;*/
      encodeURI(serviceName);
      window.location = "../pages/servicepercentmsgadmin.php?client=" + encodeURIComponent(serviceName) + "&range=" + repRange + "&range1=" + repRange1 + "&type=" + t + "&sents=" + valNum + "&from=1&controller=1&sort=<?php echo $sort; ?>&query=" + val + "&mcc=`" + mcc + "`&smsc=" + smsc;
   }

   function gotoMo(dater, sId, mcc, smsc)
   {
      $(".loader").fadeIn("slow");
      $(".loaderIcon").fadeIn("slow");

      var repRange1 = $("#reportrange span").html();
      var end = dater + (24 * 60 * 60) - 1;//
      var repRange = dater + ' - ' + end;
      var prevPage = "servicepercentadmin";

      window.location = "../pages/servicepercentmomsgadmin.php?range=" + repRange + "&range1=" + repRange1 + "&mcc=" + mcc + "&smsc=" + smsc + "&prevPage=" + prevPage + "&sId=" + sId;
   }
</script>

<script type="text/javascript">
   $(window).load(function () {
      $(".loader").fadeOut("slow");
      $(".loaderIcon").fadeOut("slow");
   })
</script>

<!-- Template Footer -->

