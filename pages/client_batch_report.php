<?php
   /**
    * Include the header template which sets up the HTML
    *
    * Don't forget to include template_import_script before any Javascripting
    * Don't forget to include template footer.php at the bottom of the page as well
    */
   //-------------------------------------------------------------
   // Template
   //-------------------------------------------------------------
   TemplateHelper::setPageTitle('Batch Report');
   TemplateHelper::initialize();

   //-------------------------------------------------------------
   // MANAGE SERVICE
   //-------------------------------------------------------------

   //get the batch ID
   if(!isset($_GET['batch_id']))
   {
      header('location: client_reports.php' );
      die();
   }
   else
   {
      $batch_id = $_GET['batch_id'];

      //get the batch info and check if we can acutally view this batch using permissions
      $batch_obj = Batch::getBatchData($batch_id);
      $service_id = $batch_obj['service_id'];

      if(!PermissionsHelper::hasServicePermission($service_id))
      {
         echo "You do not have permission to view this batch.";
         die();
      }

      //get the service object
      $service_obj = new Service($service_id);

      //get some totals for the page
      $sent_total = $batch_obj['send_stats']['sent'];
      $delivered_total = $batch_obj['send_stats']['delivered'];
      $pending_total = $batch_obj['send_stats']['pending'];
      $failed_total = $batch_obj['send_stats']['failed'];
      $rejected_total = $batch_obj['send_stats']['rejected'] + $batch_obj['send_stats']['blacklisted'];
      $replies_total = $batch_obj['send_stats']['mo_count'];

      if(strtolower($batch_obj['batch_submit_status']) == "completed")
      {
         $total_cost = "";
         foreach($batch_obj['send_stats']['total_cost'] as $currency => $cost)
         {
            $total_cost .= '<span class="text-info">('.$currency.')</span> '.number_format(($cost/10000), 2, '.', ' ').' <br/>';
         }
      }
      else
      {
         $total_cost = "n/a";
      }

      //do some processing for the batch status
      if($batch_obj['batch_submit_status_can_change'] === true) {
         $batch_status_tooltip = "Click here to change the status of this batch.";
      }
      else
      {
         $batch_status_tooltip = "Batch status cannot be changed.";
      }

   }

?>

<aside class="right-side">
   <section class="content-header">
      <h1>
         Batch Report
      </h1>
   </section>

   <section class="content">
      <div class="row">
         <div class="col-lg-12">
            <!-- THE BACK TO REPORTS -->
            <a class="btn btn-default btn-sm" href="client_reports.php?service_id=<?php echo $service_id; ?>#tab_batches" style="margin:0 0 10px 0;">
               <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span> Back to Batch list
            </a>
         </div>
         <div class="col-lg-12">
            <p class="lead">
               You can manage this batch as well as download CSV reports using the tabs below.
            </p>
            <?php if(strtolower($batch_obj['batch_submit_status']) == "sending") { ?>
               <!-- THE BACK TO USER BUTTON -->
               <div class="alert alert-warning alert-dismissible">
                  <button class="close" aria-hidden="true" data-dismiss="alert" type="button">×</button>
                  <h4>
                     <i class="icon fa fa-ban"></i>
                     Batch Stats Incomplete
                  </h4>
                  This batch is currently sending, so batch stats and costs will remain incomplete until the batch has been fully processed.
               </div>
            <?php } ?>
         </div>
      </div>

      <hr/>

      <!-- BATCH CONTENT -->
      <div class="row">

         <!-- BATCH OVERVIEW DETAILS -->
         <div class="col-lg-2">
            <div class="box box-solid">
               <div class="box-body" style="text-align:right;">
                  <h2 style="margin-top:0px;">
                     <small>Batch Reference:</small><br/>
                     <strong><?php echo htmlspecialchars($batch_obj['batch_reference']); ?></strong>
                  </h2>
                  <hr/>
                  <div class="input-group-btn" data-toggle="tooltip" data-placement="top" title="<?php echo $batch_status_tooltip; ?>">
                     <button style="text-align:center;" type="button"
                        class="btn btn-block <?php echo $batch_obj['batch_submit_status_btn_class']; ?> dropdown-toggle" id="btnBatchStatus" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        <span id="btnPaymentTypeString">Status: <?php echo $batch_obj['batch_submit_status']; ?></span>
                        <?php if($batch_obj['batch_submit_status_can_change'] === true) { ?>
                           <span class="caret"></span>
                        <?php } ?>
                     </button>
                     <?php if($batch_obj['batch_submit_status_can_change'] === true) { ?>
                        <ul class="dropdown-menu dropdown-menu-right">
                           <li><a class="btn-select-batch-status" connet-batch-status="PAUSE" href="#">PAUSE</a></li>
                           <li><a class="btn-select-batch-status" connet-batch-status="SCHEDULE" href="#">SCHEDULE</a></li>
                           <li><a class="btn-select-batch-status" connet-batch-status="SEND" href="#">SEND</a></li>
                           <li><a class="btn-select-batch-status" connet-batch-status="ABORT" href="#">ABORT</a></li>
                        </ul>
                     <?php } ?>
                  </div>
                  <hr/>
                  <h3 style="margin-top:0px;">
                     <small>Total Cost:</small><br/>
                     <strong><?php echo $total_cost; ?></strong>
                  </h3>
                  <h4 style="margin-top:0px;">
                     <small>Client Name:</small><br/>
                     <strong><?php echo htmlspecialchars($batch_obj['client_name']); ?></strong>
                  </h4>
                  <h4 style="margin-top:0px;">
                     <small>Campaign Name:</small><br/>
                     <strong><?php echo htmlspecialchars($batch_obj['campaign_name']); ?></strong>
                  </h4>
                  <h4 style="margin-top:0px;">
                     <small>Campaign Code:</small><br/>
                     <strong><?php echo htmlspecialchars($batch_obj['campaign_code']); ?></strong>
                  </h4>
                  <h4>
                     <small>Launch Date:</small><br/>
                     <strong><?php echo date('H:i - d M Y', strtotime($batch_obj['batch_schedule'])); ?></strong>
                  </h4>
               </div>
            </div>
         </div>

         <!-- BATCH STATS TABS -->
         <div class="col-lg-10">
            <div class="nav-tabs-custom">
               <!-- Nav tabs -->
               <ul class="nav nav-tabs" id="service_tabs">
                  <li role="presentation">
                     <a href="#tab_overview" aria-controls="tab_overview" role="tab" data-toggle="tab"><i class="fa fa-eye"></i> Batch Overview</a>
                  </li>
                  <li role="presentation">
                     <a href="#tab_sent" aria-controls="tab_sent" role="tab" data-toggle="tab"><i class="fa fa-send"></i> Sent (<?php echo $sent_total; ?>)</a>
                  </li>
                  <li role="presentation">
                     <a href="#tab_delivered" aria-controls="tab_delivered" role="tab" data-toggle="tab"><i class="text-success fa fa-check-circle"></i> Delivered (<?php echo $delivered_total; ?>)</a>
                  </li>
                  <li role="presentation">
                     <a href="#tab_pending" aria-controls="tab_pending" role="tab" data-toggle="tab"><i class="text-warning fa fa-question-circle"></i> Pending (<?php echo $pending_total; ?>)</a>
                  </li>
                  <li role="presentation">
                     <a href="#tab_failed" aria-controls="tab_failed" role="tab" data-toggle="tab"><i class="text-danger fa fa-times-circle"></i> Failed (<?php echo $failed_total; ?>)</a>
                  </li>
                  <li role="presentation">
                     <a href="#tab_rejected" aria-controls="tab_rejected" role="tab" data-toggle="tab"><i class="text-danger fa fa-minus-circle"></i> Rejected (<?php echo $rejected_total; ?>)</a>
                  </li>
                  <li role="presentation">
                     <a href="#tab_replies" aria-controls="tab_replies" role="tab" data-toggle="tab"><i class="text-info fa fa-reply"></i> Replies (<span class="reply-count-display"><?php echo $replies_total; ?></span>)</a>
                  </li>
               </ul>

               <!-- Tab panes -->
               <div class="tab-content">
                  <!-- THE OVERVIEW TAB -->
                  <div role="tabpanel" class="tab-pane active" id="tab_overview">

                  </div>

                  <!-- THE SENT TAB -->
                  <div role="tabpanel" class="tab-pane active" id="tab_sent"></div>
                  <!-- THE DELIVERED TAB -->
                  <div role="tabpanel" class="tab-pane" id="tab_delivered"></div>
                  <!-- THE PENDING TAB -->
                  <div role="tabpanel" class="tab-pane" id="tab_pending"></div>
                  <!-- THE FAILED TAB -->
                  <div role="tabpanel" class="tab-pane" id="tab_failed"></div>
                  <!-- THE BLACKLISTED TAB -->
                  <div role="tabpanel" class="tab-pane" id="tab_rejected"></div>
                  <!-- THE REPLIES TAB -->
                  <div role="tabpanel" class="tab-pane" id="tab_replies"></div>

               </div>
            </div>
         </div>
      </div>
   </section>

   <!-- MODALS HERE -->
   <div id="modalEditField" class="modal fade" tabindex="-1" role="dialog" data-backdrop="static">
      <div class="modal-dialog">
         <div class="modal-content">
            <div class="modal-header" >
               <button type="button" class="close" data-dismiss="modal" aria-label="Close" id="btnModalCloseTop"><span aria-hidden="true">&times;</span></button>
               <h4 class="modal-title" id="modalTitle">Add A New Contact Group</h4>
            </div>
            <div class="modal-body">
               <div class="alert alert-danger" style="display:none;" id="modAlert">
                  <h4><i class="icon fa fa-ban"></i> Error!</h4>
                  <p id="modAlertText">Unfortunately there was an error, please try again.</p>
               </div>
               <div class="form-group">
                  <label for="modalInputValue" id="modalInputLabel">Label</label>
                  <input type="text" class="form-control" id="modalInputValue" placeholder="" value="" />
               </div>
               <small>Please edit the value and click save to commit, or close to cancel.</small>
            </div>
            <div class="modal-footer">
               <button type="button" class="btn btn-default" data-dismiss="modal" id="btnModalCloseBottom">Close</button>
               <button type="button" class="btn btn-primary" id="btnModalSave" style="background-color:<?php echo $_SESSION['accountColour']; ?>;border: 2px solid; border-radius: 6px !important;">Save</button>
               <input type="hidden" class="form-control" id="modalInputField" value=""/>
               <input type="hidden" class="form-control" id="modalInputId" value=""/>
               <input type="hidden" class="form-control" id="modalInputTask" value=""/>
            </div>
         </div><!-- /.modal-content -->
      </div><!-- /.modal-dialog -->
   </div><!-- /.modal -->
</aside>


<!--  THE JAVASCRIPT IMPORT ALWAYS APPEARS BEFORE LOCAL JAVASCRIPT AT THE BOTTOM OF THE PAGE -->
<?php include("template_import_script.php"); //must import all scripts first   ?>
<!-- END JAVASCRIPT IMPORT -->

<script type="text/javascript">
   var serviceId = <?php echo (isset($service_id) ? $service_id : '-1') ?>;
   var batchId = <?php echo (isset($batch_id) ? $batch_id : '-1') ?>;
   var replyCount = 0;
   var activeTab = "";
   var activeTabPageNumber = 1;


   $(document).ready(function (e)
   {
      //on load of the page: switch to the currently selected tab
      var hash = window.location.hash;
      if(hash == "") {
         hash = "#tab_overview";
      }

      console.log(hash);

      $('#service_tabs a').click(function(e) {
         e.preventDefault();
         $(this).tab('show');
         $(window).scrollTop(0);
      });

      //store the currently selected tab in the hash value
      $("ul.nav-tabs > li > a").on("shown.bs.tab", function(e) {
         var id = $(e.target).attr("href").substr(1);
         window.location.hash = id;

         loadTabFragment(id);
      });

      //for the update credits shortcut
      $('#btnShortcutAddCredits').click(function(e) {
         e.preventDefault();
         showTab('#tab_payments');
      });

      //show the default loading tab for the page after init
      showTab(hash);

      /**
       * This button catches the ENABLED/DISABLED status of a service.
       */
      $('.btn-select-batch-status').on("click", function (e)
      {
         e.preventDefault();
         var batchStatus = $(this).attr('connet-batch-status');

         if(confirm("Are you sure you want to set the batch status to "+status+"?"))
         {
            $('#btnBatchStatus').html('<span class="glyphicon glyphicon-refresh glyphicon-spin"></span> Saving...');
            $('#btnBatchStatus').attr('disabled', true);

            $.post("../php/ajaxReportingBatchHandler.php", {task: "change_batch_status", serviceId: serviceId, batchId: batchId, batchStatus:batchStatus}).done(function (data) {
               var json = $.parseJSON(data);
               if (json.success)
               {
                  $('#btnBatchStatus').html('<span class="glyphicon glyphicon-refresh glyphicon-spin"></span> Reloading...');
                  location.reload();
               }
               else
               {
                  $('#btnBatchStatus').removeAttr('disabled');
                  alert("A server error occurred and we could not change the status of this service, please contact technical support.");
               }
            }).fail(function (data) {
               unlockModalBusy();
               $('#btnBatchStatus').removeAttr('disabled');
               showModalError("A server error occurred and we could not change the status of this service, please contact technical support.");
            });
         }
      });

      //setup the date range picker
      $('#reports_date_picker').daterangepicker(
          {
             ranges: {
                'Today': [moment(), moment()],
                'Yesterday': [moment().subtract('days', 1), moment().subtract('days', 1)],
                'Last 7 Days': [moment().subtract('days', 6), moment()],
                'Last 30 Days': [moment().subtract('days', 29), moment()],
                'This Month': [moment().startOf('month'), moment().endOf('month')],
                'Last Month': [moment().subtract('month', 1).startOf('month'), moment().subtract('month', 1).endOf('month')]
             },
             startDate: moment('<?php echo isset($start_date_string) ? $start_date_string : ''; ?>'),
             endDate: moment('<?php echo isset($end_date_string) ? $end_date_string : ''; ?>')
          },
          function (start, end) {
             $('#reports_date_picker span').html(start.format('YYYY-MM-DD') + ' - ' + end.format('YYYY-MM-DD'));

             var startDateCal = start.format('YYYY-MM-DD');

             var endDateCal = end.format('YYYY-MM-DD');

             window.location = "../pages/client_reports.php?service_id=" + encodeURIComponent(serviceId) + "&start_date=" + startDateCal + "&end_date=" + endDateCal + activeTab;
          }
      )

      //delay the reply counter loading so other tasks can complete
      setTimeout(getReplyCount, 2000);

   });

   //set the tab and start the tab load process according to the hash string passed to it
   function showTab(hashTab)
   {
      $('#service_tabs a[href="' + hashTab + '"]').tab('show');
      $(window).scrollTop(0);
   }

   //this locks the modal so the user can't do anything, useful when runing ajax calls off of it
   var prevButtonContent = "";
   function lockModalBusy(busyMessage)
   {
      prevButtonContent = $('#btnModalSave').html();
      $('#btnModalCloseTop').prop("disabled", true);
      $('#btnModalCloseBottom').prop("disabled", true);
      $('#btnModalSave').prop("disabled", false);
      $('#btnModalSave').html('<span class="glyphicon glyphicon-refresh glyphicon-spin"></span> ' + busyMessage);
   }

   //unlocks the modal so the user can close it
   function unlockModalBusy()
   {
      $('#btnModalCloseTop').prop("disabled", false);
      $('#btnModalCloseBottom').prop("disabled", false);
      $('#btnModalSave').prop("disabled", false);
      $('#btnModalSave').html(prevButtonContent);
      prevButtonContent = "";
   }

   //shows the error in the MODAL with a custom error message
   function showModalError(message)
   {
      $("#modAlertText").html(message);
      $("#modAlert").show();
   }

   //hides the modal error and resets the message
   function hideModalError()
   {
      $("#modAlertText").html();
      $("#modAlert").hide();
   }

   //cleans up all the child elements in a tab, for optimization
   function cleanupTabData(tabId)
   {
      if(tabId != "")
      {
         $(tabId).empty();
      }
   }

   function loadTabFragment(tabName)
   {
      cleanupTabData(activeTab);
      showTabLoading(tabName);
      activeTab = tabName;
      var getVariableString = window.location.search.substring(1);

      var scriptPath = "";
      switch (tabName) {
         case "tab_overview":
            scriptPath = "client_reports/frag_reports_batch_overview.php?"+getVariableString;
            break;
         case "tab_sent":
            scriptPath = "client_reports/frag_reports_batch_sends.php?send_type=all&"+getVariableString;
            break;
         case "tab_delivered":
            scriptPath = "client_reports/frag_reports_batch_sends.php?send_type=delivered&"+getVariableString;
            break;
         case "tab_pending":
            scriptPath = "client_reports/frag_reports_batch_sends.php?send_type=pending&"+getVariableString;
            break;
         case "tab_failed":
            scriptPath = "client_reports/frag_reports_batch_sends.php?send_type=failed&"+getVariableString;
            break;
         case "tab_rejected":
            scriptPath = "client_reports/frag_reports_batch_sends.php?send_type=rejected&"+getVariableString;
            break;
         case "tab_replies":
            scriptPath = "client_reports/frag_reports_batch_replies.php?"+getVariableString;
            break;
      }

      //load up the page tab we have selected
      $.post(scriptPath, {service_id:serviceId, batch_id:batchId, page:activeTabPageNumber}).done(function (data) {
         $('#'+tabName).html(data);
      }).fail(function (data) {
         showTabLoadingError(divName, "A server error occurred and we could not load this list, please contact technical support.");
      });
   }

   function getReplyCount()
   {
      $('.reply-count-display').html('<span class="glyphicon glyphicon-refresh glyphicon-spin"></span>');

      //load up the page tab we have selected
      $.post("../php/ajaxReportingBatchHandler.php", {task: 'get_batch_reply_count', serviceId: serviceId, batchId: batchId}).done(function (data) {
         var json = $.parseJSON(data);
         if (json.success)
         {
            $('.reply-count-display').html(json.reply_total);
            replyCount = json.reply_total;
         }
         else
         {
            $('.reply-count-display').html('0');
         }
      }).fail(function () {
         $('.reply-count-display').html('0');
      });
   }

   function paginateOnTab(pageNumber)
   {
      activeTabPageNumber = pageNumber;
      loadTabFragment(activeTab);
   }

   function showTabLoading(elementId)
   {
      $('#'+elementId).html('<div style="width:100%; text-align:center; padding:20px; font-size:20px;"><span class="glyphicon glyphicon-refresh glyphicon-spin"></span><br/>Loading... Please wait.</div>');
   }

   function showTabLoadingError(elementId, message)
   {
      $('#'+elementId).html('<h5 class="text-warning">' + message + '</h5>');
   }

   function showLoading(elementId)
   {
      $('#'+elementId).html('<div style="width:100%; text-align:center; padding:20px;"><span class="glyphicon glyphicon-refresh glyphicon-spin"></span> Loading...</div>');
   }

   function showLoadingError(elementId, message)
   {
      $('#'+elementId).html('<h5 class="text-warning">' + message + '</h5>');
   }
</script>

<!-- Template Footer -->

