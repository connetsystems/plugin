<?php
/**
 * Include the header template which sets up the HTML
 *
 * Don't forget to include template_import_script before any Javascripting
 * Don't forget to include template footer.php at the bottom of the page as well
 */
//-------------------------------------------------------------
// Template
//-------------------------------------------------------------
TemplateHelper::setPageTitle('Manage Account');
TemplateHelper::initialize();

//-------------------------------------------------------------
// MANAGE ACCOUNT
//-------------------------------------------------------------
//get the account

if(isset($_GET['account_id']))
{
   $account_id = $_GET['account_id'];
}
else
{
   header('location: manage_account_select.php' );
   die();
}

//get the account object
$account_obj = new Account($account_id);

//determine hte styling class for the enabled/disabled display
if($account_obj->account_status == "ENABLED")
{
   $account_status_class = "btn-success";
}
else
{
   $account_status_class = "btn-danger";
}

if(!isset($account_obj->header_logo) || $account_obj->header_logo == "")
{
   $header_logo = '/img/skinlogos/1/1_header.png';
}
else
{
   $header_logo = $account_obj->header_logo;
}

//if a service ID is set, then the back button to the particular service the user was working on will show
if(isset($_GET['service_id']))
{
   $service_id = $_GET['service_id'];
   $show_service_back_button = true;
   $back_service = new Service($service_id);
}
else
{
   $show_service_back_button = false;
}

//if a service ID is set, then the back button to the particular service the user was working on will show
if(isset($_GET['user_id']))
{
   $user_id = $_GET['user_id'];
   $show_user_back_button = true;
   $back_user = new User($user_id);
}
else
{
   $show_user_back_button = false;
}

?>

<aside class="right-side">
   <section class="content-header">
      <h1>
         Manage Account
      </h1>
   </section>

   <section class="content">

      <?php
         //include the top nav
         $nav_section = "accounts";
         include('modules/module_admin_nav.php');
      ?>

      <!-- BACK BUTTON AND ACCOUNT TITLE -->
      <div class="row">
         <div class="col-lg-2">
            <?php if($show_user_back_button) { ?>
               <!-- THE BACK TO USER BUTTON -->
               <a class="btn btn-default btn-block btn-sm" href="manage_user.php?user_id=<?php echo $user_id; ?>#tab_users" style="margin-top:10px; overflow:hidden;">
                  <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span> Back to User "<?php echo htmlentities($back_user->user_username); ?>"
               </a>
            <?php } ?>

            <?php if($show_service_back_button) { ?>
               <!-- THE BACK TO SERVICE BUTTON -->
               <a class="btn btn-default btn-block btn-sm" href="manage_service.php?service_id=<?php echo $service_id; ?>#tab_users" style="margin-top:10px; overflow:hidden;">
                  <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span> Back to Service "<?php echo htmlentities($back_service->service_name); ?>"
               </a>
            <?php } ?>
         </div>
         <div class="col-lg-10">

         </div>
      </div>
      <hr/>

      <div class="row">

         <!-- ACCOUNT MAIN DETAILS ACCOUNT ECT -->
         <div class="col-lg-2" style="text-align:right;">
            <img src="<?php echo $header_logo; ?>" class="img-responsive thumbnail" alt="Account Logo"/>
         </div>
         <div class="col-lg-8">
            <h1 style="margin:0px;">
               <strong class="connet-field-display"><?php echo $account_obj->account_name; ?></strong>
               <a href="#" class="btn-edit-account-property btn btn-xs btn-primary"
                  connet-id="<?php echo $account_obj->account_id; ?>"
                  connet-value="<?php echo htmlspecialchars($account_obj->account_name); ?>"
                  connet-ajax-task="rename_account"
                  connet-field="account_name"
                  connet-title="Account Name">Rename</a>
            </h1>
         </div>
         <div class="col-lg-2">
            <div class="input-group-btn" style="text-align:center;">
               <button type="button" class="btn <?php echo $account_status_class; ?> dropdown-toggle" id="btnStatus" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                  <span id="btnStatusString">ACCOUNT <?php echo $account_obj->account_status; ?></span> <span class="caret"></span>
               </button>
               <ul class="dropdown-menu dropdown-menu-right">
                  <li><a class="btn-select-status" connet-account-status="ENABLED" href="#">Enable Account</a></li>
                  <li><a class="btn-select-status" connet-account-status="DISABLED" href="#">Disable Account</a></li>
               </ul>
            </div>
         </div>
      </div>
      <div class="row">
         <!-- ACCOUNT MAIN DETAILS ACCOUNT ECT -->
         <div class="col-lg-2">
            <div class="box box-solid">
               <div class="box-body" style="text-align:right;">
                  <h4 style="margin-top:0px;">
                     <small>Total Services:</small><br/>
                     <strong><?php echo count($account_obj->account_services_simple); ?></strong>
                  </h4>
                  <h4 style="margin-top:0px;">
                     <small>Total Users:</small><br/>
                     <strong><?php echo count($account_obj->account_users_simple); ?></strong>
                  </h4>

                  <h4 style="margin-top:0px;">
                     <small>Parent Account:</small><br/>
                     <?php if(isset($service_obj->parent_account_id)) { ?>
                        <strong><a href="manage_account.php?account_id=<?php echo $service_obj->parent_account_id; ?>"><?php echo $account_obj->parent_account_name; ?></a></strong>
                     <?php } else { ?>
                        <i>n/a</i>
                     <?php } ?>

                  </h4>
                  <h4>
                     <small>Created:</small><br/>
                     <strong><?php echo date('d M Y', strtotime($account_obj->account_created)); ?></strong>
                  </h4>
               </div>
            </div>
         </div>

         <!-- THE TABS THAT HOLD THE CONFIG SECTIONS -->
         <div class="col-lg-10">
            <div class="nav-tabs-custom">
               <!-- Nav tabs -->
               <ul class="nav nav-tabs" id="account_tabs">
                  <li role="presentation"><a href="#tab_general" aria-controls="tab_general" role="tab" data-toggle="tab"><i class="fa fa-cogs"></i> Account Config</a></li>
                  <li role="presentation"><a href="#tab_services" aria-controls="tab_services" role="tab" data-toggle="tab"><i class="fa fa-server"></i> Account Services</a></li>
                  <li role="presentation"><a href="#tab_users" aria-controls="tab_users" role="tab" data-toggle="tab"><i class="fa fa-users"></i> Account Users</a></li>
               </ul>

               <!-- Tab panes -->
               <div class="tab-content">

                  <!-- THE GENERAL SETTINGS TAB -->
                  <div role="tabpanel" class="tab-pane active" id="tab_general">

                  </div>

                  <!-- THE GENERAL SERVICES TAB -->
                  <div role="tabpanel" class="tab-pane active" id="tab_services">

                  </div>

                  <!-- THE ACCOUNT USERS TAB -->
                  <div role="tabpanel" class="tab-pane active" id="tab_users">

                  </div>
               </div>
            </div>
         </div>
   </section>

   <!-- MODALS HERE -->
   <div id="modalEditField" class="modal fade" tabindex="-1" role="dialog" data-backdrop="static">
      <div class="modal-dialog">
         <div class="modal-content">
            <div class="modal-header" >
               <button type="button" class="close" data-dismiss="modal" aria-label="Close" id="btnModalCloseTop"><span aria-hidden="true">&times;</span></button>
               <h4 class="modal-title" id="modalTitle">Add A New Contact Group</h4>
            </div>
            <div class="modal-body">
               <div class="alert alert-danger" style="display:none;" id="modAlert">
                  <h4><i class="icon fa fa-ban"></i> Error!</h4>
                  <p id="modAlertText">Unfortunately there was an error, please try again.</p>
               </div>
               <div class="form-group">
                  <label for="modalInputValue" id="modalInputLabel">Label</label>
                  <input type="text" class="form-control" id="modalInputValue" placeholder="" value="" />
               </div>
               <small>Please edit the value and click save to commit, or close to cancel.</small>
            </div>
            <div class="modal-footer">
               <button type="button" class="btn btn-default" data-dismiss="modal" id="btnModalCloseBottom">Close</button>
               <button type="button" class="btn btn-primary" id="btnModalSave" style="background-color:<?php echo $_SESSION['accountColour']; ?>;border: 2px solid; border-radius: 6px !important;">Save</button>
               <input type="hidden" class="form-control" id="modalInputField" value=""/>
               <input type="hidden" class="form-control" id="modalInputId" value=""/>
               <input type="hidden" class="form-control" id="modalInputTask" value=""/>
            </div>
         </div><!-- /.modal-content -->
      </div><!-- /.modal-dialog -->
   </div><!-- /.modal -->
</aside>


<!--  THE JAVASCRIPT IMPORT ALWAYS APPEARS BEFORE LOCAL JAVASCRIPT AT THE BOTTOM OF THE PAGE -->
<?php include("template_import_script.php"); //must import all scripts first   ?>
<!-- END JAVASCRIPT IMPORT -->

<script type="text/javascript">
   var accountId = <?php echo (isset($account_id) ? $account_id : '-1') ?>;
   var activeTab = "";


   $(document).ready(function (e)
   {
      //on load of the page: switch to the currently selected tab
      var hash = window.location.hash;
      if(hash == "") {
         hash = "#tab_general";
      }

      console.log(hash);

      $('#account_tabs a').click(function(e) {
         e.preventDefault();
         $(this).tab('show');
         $(window).scrollTop(0);
      });

      //store the currently selected tab in the hash value
      $("ul.nav-tabs > li > a").on("shown.bs.tab", function(e) {
         var id = $(e.target).attr("href").substr(1);
         window.location.hash = id;

         loadTabFragment(id);
      });


      //show the default loading tab for the page after init
      showTab(hash);


      /**
       * This button loads the generic field editor in the modal
       */
      $('.btn-edit-account-property').on("click", function (e)
      {
         e.preventDefault();

         var fieldName = $(this).attr('connet-field');
         var fieldTitle = $(this).attr('connet-title');
         var fieldId = $(this).attr('connet-id');
         var fieldTask = $(this).attr('connet-ajax-task');
         var fieldValue = $(this).attr('connet-value');

         $('#modalTitle').html("Edit Field: " + fieldTitle);
         $('#modalInputLabel').html(fieldTitle);
         $('#modalInputValue').val(fieldValue);
         $('#modalInputField').val(fieldName);
         $('#modalInputId').val(fieldId);
         $('#modalInputTask').val(fieldTask);

         $('#modalEditField').modal('show');
      });

      /**
       * This button catches the ENABLED/DISABLED status of a account.
       */
      $('.btn-select-status').on("click", function (e)
      {
         e.preventDefault();

         var thisObject = $(this);
         var status = $(this).attr('connet-account-status');
         var fieldName = "account_status";
         var confirmText = "";
         var newClass = "";
         var oldClass = "";
         if(status == "ENABLED")
         {
            confirmText = "Are you sure you want to ENABLE this account?";
            newClass = "btn-success";
            oldClass = "btn-danger";
         }
         else
         {
            confirmText = "Are you sure you want to DISABLE this account?";
            newClass = "btn-danger";
            oldClass = "btn-danger";
         }

         if(confirm(confirmText))
         {
            $('#btnStatusString').html('<span class="glyphicon glyphicon-refresh glyphicon-spin"></span> Saving...');

            $.post("../php/ajaxManageAccountHandler.php", {task: "save_account_field_value", accountId: accountId, fieldName: fieldName, fieldValue: status}).done(function (data) {
               var json = $.parseJSON(data);
               if (json.success)
               {
                  $('#btnStatusString').html("ACCOUNT " + status);
                  $('#btnStatus').removeClass(oldClass);
                  $('#btnStatus').addClass(newClass);
               }
               else
               {
                  alert("A server error occurred and we could not change the status of this account, please contact technical support.");
               }
            }).fail(function (data) {
               unlockModalBusy();
               showModalError("A server error occurred and we could not change the status of this account, please contact technical support.");
            });
         }
      });

      /**
       * This button catches the POSTPIAD/PREPAID payment type of a account.
       */
      $('.btn-select-payment-type').on("click", function (e)
      {
         e.preventDefault();

         var thisObject = $(this);
         var paymentType = $(this).attr('connet-payment-type');
         var fieldName = "account_credit_billing_type";
         var confirmText = "";
         var newClass = "";
         var oldClass = "";
         if(paymentType == "POSTPAID")
         {
            confirmText = "Are you sure you want to set this account to POSTPAID?";
         }
         else
         {
            confirmText = "Are you sure you want to set this account to PREPAID?";
         }

         if(confirm(confirmText))
         {
            $('#btnPaymentTypeString').html('<span class="glyphicon glyphicon-refresh glyphicon-spin"></span> Saving...');

            $.post("../php/ajaxManageAccountHandler.php", {task: "save_account_field_value", accountId: accountId, fieldName: fieldName, fieldValue: paymentType}).done(function (data) {
               var json = $.parseJSON(data);
               if (json.success)
               {
                  $('#btnPaymentTypeString').html(paymentType);
               }
               else
               {
                  alert("A server error occurred and we could not change the payment type of this account, please contact technical support.");
               }
            }).fail(function (data) {
               unlockModalBusy();
               showModalError("A server error occurred and we could not change the payment type of this account, please contact technical support.");
            });
         }
      });


      $('#btnModalSave').on("click", function (e)
      {
         e.preventDefault();

         lockModalBusy("Saving...");

         var fieldName = $('#modalInputField').val();
         var fieldValue = $('#modalInputValue').val();
         var fieldTask = $('#modalInputTask').val();
         var fieldId = $('#modalInputId').val();

         $.post("../php/ajaxManageAccountHandler.php", {task: fieldTask, accountId: fragAccountId, fieldId: fieldId, fieldName:fieldName, fieldValue:fieldValue}).done(function (data) {
            var json = $.parseJSON(data);
            if (json.success)
            {
               unlockModalBusy();

               //ocate the html elements that hold this data and update them for display
               $("a[connet-id='"+fieldId+"'][connet-field='"+fieldName+"']" ).attr('connet-value', fieldValue);
               $("a[connet-id='"+fieldId+"'][connet-field='"+fieldName+"']" ).parent().find('.connet-field-display').html(fieldValue);
               $('#modalEditField').modal('hide');
            }
            else
            {
               unlockModalBusy();
               showModalError("A server error occurred and we could not save the data, please contact technical support.");
            }
         }).fail(function (data) {
            unlockModalBusy();
            showModalError("A server error occurred and we could not save the data, please contact technical support.");
         });
      });

   });

   //set the tab and start the tab load process according to the hash string passed to it
   function showTab(hashTab)
   {
      $('#account_tabs a[href="' + hashTab + '"]').tab('show');
      $(window).scrollTop(0);
   }

   //this locks the modal so the user can't do anything, useful when runing ajax calls off of it
   var prevButtonContent = "";
   function lockModalBusy(busyMessage)
   {
      prevButtonContent = $('#btnModalSave').html();
      $('#btnModalCloseTop').prop("disabled", true);
      $('#btnModalCloseBottom').prop("disabled", true);
      $('#btnModalSave').prop("disabled", false);
      $('#btnModalSave').html('<span class="glyphicon glyphicon-refresh glyphicon-spin"></span> ' + busyMessage);
   }

   //unlocks the modal so the user can close it
   function unlockModalBusy()
   {
      $('#btnModalCloseTop').prop("disabled", false);
      $('#btnModalCloseBottom').prop("disabled", false);
      $('#btnModalSave').prop("disabled", false);
      $('#btnModalSave').html(prevButtonContent);
      prevButtonContent = "";
   }

   //shows the error in the MODAL with a custom error message
   function showModalError(message)
   {
      $("#modAlertText").html(message);
      $("#modAlert").show();
   }

   //hides the modal error and resets the message
   function hideModalError()
   {
      $("#modAlertText").html();
      $("#modAlert").hide();
   }

   //cleans up all the child elements in a tab, for optimization
   function cleanupTabData(tabId)
   {
      if(tabId != "")
      {
         $(tabId).empty();
      }
   }

   function loadTabFragment(tabName)
   {
      cleanupTabData(activeTab);
      showTabLoading(tabName);
      activeTab = tabName;
      var getVariableString = window.location.search.substring(1);

      var scriptPath = "";
      switch (tabName) {
         case "tab_general":
            scriptPath = "admin_accounts/frag_account_general.php?"+getVariableString;
            break;
         case "tab_services":
            scriptPath = "admin_accounts/frag_account_services.php?"+getVariableString;
            break;
         case "tab_users":
            scriptPath = "admin_accounts/frag_account_users.php?"+getVariableString;
            break;
      }

      //load up the page tab we have selected
      $.post(scriptPath, {account_id:accountId}).done(function (data) {
         $('#'+tabName).html(data);
      }).fail(function (data) {
         showTabLoadingError(divName, "A server error occurred and we could not load this list, please contact technical support.");
      });
   }

   function showTabLoading(elementId)
   {
      $('#'+elementId).html('<div style="width:100%; text-align:center; padding:20px; font-size:20px;"><span class="glyphicon glyphicon-refresh glyphicon-spin"></span><br/>Loading... Please wait.</div>');
   }

   function showTabLoadingError(elementId, message)
   {
      $('#'+elementId).html('<h5 class="text-warning">' + message + '</h5>');
   }

   function showLoading(elementId)
   {
      $('#'+elementId).html('<div style="width:100%; text-align:center; padding:20px;"><span class="glyphicon glyphicon-refresh glyphicon-spin"></span> Loading...</div>');
   }

   function showLoadingError(elementId, message)
   {
      $('#'+elementId).html('<h5 class="text-warning">' + message + '</h5>');
   }
</script>

<!-- Template Footer -->

