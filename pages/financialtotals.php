<?php
/**
 * Include the header template which sets up the HTML
 *
 * Don't forget to include template_import_script before any Javascripting
 * Don't forget to include template_footer.php at the bottom of the page as well
 */
//set the page title for the template import
error_reporting(E_ALL);
ini_set('display_errors', 1);

$headerPageTitle = 'Financial Stats';
TemplateHelper::initialize();


//Get montly units based on date range for the array
$monltytotals_array = getMonthly_Units_Totals("2015-01-01", "2016-05-01");

if (isset($_GET['monthname']) && isset($_GET['year']))
{
   $monthname = $_GET['monthname'];
   $year = $_GET['year'];

   $fdate = DateTime::createFromFormat('j-M-Y', '01-'.substr($monthname,0,3).'-'.$year);

   $tdate = DateTime::createFromFormat('j-M-Y', '01-'.substr($monthname,0,3).'-'.$year);
   $tdate->modify( 'first day of next month' );

   //Get daily units based on date range for the array
   $dailyytotals_array = getDay_Units_Totals($fdate->format('Y-m-d'),$tdate->format('Y-m-d'));

}
else
{
   $fdate = DateTime::createFromFormat('Y-m-d',date("Y-m-d"));

   $tdate = DateTime::createFromFormat('Y-m-d',date("Y-m-d"));
   $tdate->modify( 'first day of next month' );

   //Get daily units based on date range for the array
   $dailyytotals_array = getDay_Units_Totals($fdate->format('Y-m-d'),$tdate->format('Y-m-d'));


}





   ?>


   <aside class="right-side">

      <section class="content col-lg-12" >
         <!-- Nav tabs -->
         <ul class="nav nav-tabs" role="tablist">
            <li role="presentation" class="active"><a href="#dash" aria-controls="dash" role="tab" data-toggle="tab">Dashboard</a></li>
         </ul>
         <div class="tab-content">
            <div role="tabpanel" class="tab-pane active" id="dash">
               <section class="content">
                  <div class="row">
                     <div class="col-md-12">
                        <div class="box box-solid box-primary">
                           <div class="box-header">
                              <h3 class="box-title">Year Totals</h3>
                           </div><!-- /.box-header -->
                           <div class="box-body">
                              <table id="exclusionTable" class="table table-hover table-bordered table-striped">
                                 <thead>
                                 <tr role="row">
                                    <th align="right">Year</th>
                                    <th align="right">January</th>
                                    <th align="right">February</th>
                                    <th align="right">March</th>
                                    <th align="right">April</th>
                                    <th align="right">May</th>
                                    <th align="right">June</th>
                                    <th align="right">July</th>
                                    <th align="right">August</th>
                                    <th align="right">September</th>
                                    <th align="right">October</th>
                                    <th align="right">November</th>
                                    <th align="right">December</th>
                                 </tr>
                                 </thead>
                                 <tbody>
                                 <?php
                                 $year_new = "";
                                 foreach ($monltytotals_array as $key => $value) {
                                    $year = $value['Year'];

                                    if ($year != $year_new) {

                                       if ($year_new != '') {
                                          echo '</tr>';
                                       }
                                       echo '<tr>';
                                       echo '<td align="left" style="padding: 0px;vertical-align:middle;">'.$value['Year'].'</td>';
                                       echo '<td align="left" style="padding: 0px;vertical-align:middle;cursor: pointer;"><span onClick="open_container(\''.$value['month'].'\',\''.$value['Year'].'\');">'.number_format($value['cdr_record_units'], 0, 0, ' ').'</span></td>';

                                       $year_new = $year;
                                    }
                                    else
                                    {
                                       echo '<td align="left" style="padding: 0px;vertical-align:middle;cursor: pointer;"><span onClick="open_container(\''.$value['month'].'\',\''.$value['Year'].'\');">'.number_format($value['cdr_record_units'], 0, 0, ' ').'</span></td>';
                                    }
                                 }

                                 if ($year_new != '') {
                                    echo '</tr>';
                                 }

                                 ?>
                                 </tbody>
                              </table>
                           </div>
                        </div>
                     </div>
                  </div>
               </section>
               <section class="content" >
                  <div class="row">
                     <div class="col-md-12">
                        <!-- SALES TABLE -->
                        <div class="box box-solid box-primary">
                           <div class="box-header">
                              <h3 class="box-title">Daily Totals - <?php echo $fdate->format('F Y'); ?></h3>
                           </div><!-- /.box-header -->
                           <div class="box-body">
                              <table id="DailyTotalsTable" class="table table-bordered table-striped dataTable table-hover">
                                 <thead>
                                 <tr role="row">
                                    <th align="right">Account</th>
                                    <th align="right">Service</th>
                                    <th align="right">Billing</th>
                                    <th align="right">Status</th>
                                    <th align="right">1</th>
                                    <th align="right">2</th>
                                    <th align="right">3</th>
                                    <th align="right">4</th>
                                    <th align="right">5</th>
                                    <th align="right">6</th>
                                    <th align="right">7</th>
                                    <th align="right">8</th>
                                    <th align="right">9</th>
                                    <th align="right">10</th>
                                    <th align="right">11</th>
                                    <th align="right">12</th>
                                    <th align="right">13</th>
                                    <th align="right">14</th>
                                    <th align="right">15</th>
                                    <th align="right">16</th>
                                    <th align="right">17</th>
                                    <th align="right">18</th>
                                    <th align="right">19</th>
                                    <th align="right">20</th>
                                    <th align="right">21</th>
                                    <th align="right">22</th>
                                    <th align="right">23</th>
                                    <th align="right">24</th>
                                    <th align="right">25</th>
                                    <th align="right">26</th>
                                    <th align="right">27</th>
                                    <th align="right">28</th>
                                    <th align="right">29</th>
                                    <th align="right">30</th>
                                    <th align="right">31</th>
                                 </tr>
                                 </thead>
                                 <tbody>
                                 <?php
                                 $service_name_new = "";
                                 foreach ($dailyytotals_array as $key => $value) {
                                    $service_name = $value['service_name'];

                                    if ($service_name != $service_name_new) {

                                       if ($service_name_new != '') {
                                          echo '</tr>';
                                       }
                                       echo '<tr>';
                                       if ($value['service_name'] == 'Totals')
                                       {
                                          /*
                                          echo '<td align="left" style="padding: 0px;vertical-align:middle;font-size: 12px;"><b>'.$value['account_name'].'</b></td>';
                                          echo '<td align="left" style="padding: 0px;vertical-align:middle;font-size: 12px;"><b>'.$value['service_name'].'</b></td>';
                                          echo '<td align="left" style="padding: 0px;vertical-align:middle;font-size: 12px;"><b>'.$value['service_credit_billing_type'].'</b></td>';
                                          echo '<td align="left" style="padding: 0px;vertical-align:middle;font-size: 12px;"><b>'.$value['service_status'].'</b></td>';
                                          echo '<td align="left" style="padding: 0px;vertical-align:middle;font-size: 11px;"><b>'.number_format($value['1'], 0, 0, ' ') . '</b></td>';
                                          echo '<td align="left" style="padding: 0px;vertical-align:middle;font-size: 11px;"><b>'.number_format($value['2'], 0, 0, ' ') . '</b></td>';
                                          echo '<td align="left" style="padding: 0px;vertical-align:middle;font-size: 11px;"><b>'.number_format($value['3'], 0, 0, ' ') . '</b></td>';
                                          echo '<td align="left" style="padding: 0px;vertical-align:middle;font-size: 11px;"><b>'.number_format($value['4'], 0, 0, ' ') . '</b></td>';
                                          echo '<td align="left" style="padding: 0px;vertical-align:middle;font-size: 11px;"><b>'.number_format($value['5'], 0, 0, ' ') . '</b></td>';
                                          echo '<td align="left" style="padding: 0px;vertical-align:middle;font-size: 11px;"><b>'.number_format($value['6'], 0, 0, ' ') . '</b></td>';
                                          echo '<td align="left" style="padding: 0px;vertical-align:middle;font-size: 11px;"><b>'.number_format($value['7'], 0, 0, ' ') . '</b></td>';
                                          echo '<td align="left" style="padding: 0px;vertical-align:middle;font-size: 11px;"><b>'.number_format($value['8'], 0, 0, ' ') . '</b></td>';
                                          echo '<td align="left" style="padding: 0px;vertical-align:middle;font-size: 11px;"><b>'.number_format($value['9'], 0, 0, ' ') . '</b></td>';
                                          echo '<td align="left" style="padding: 0px;vertical-align:middle;font-size: 11px;"><b>'.number_format($value['10'], 0, 0, ' ') . '</b></td>';
                                          echo '<td align="left" style="padding: 0px;vertical-align:middle;font-size: 11px;"><b>'.number_format($value['11'], 0, 0, ' ') . '</b></td>';
                                          echo '<td align="left" style="padding: 0px;vertical-align:middle;font-size: 11px;"><b>'.number_format($value['12'], 0, 0, ' ') . '</b></td>';
                                          echo '<td align="left" style="padding: 0px;vertical-align:middle;font-size: 11px;"><b>'.number_format($value['13'], 0, 0, ' ') . '</b></td>';
                                          echo '<td align="left" style="padding: 0px;vertical-align:middle;font-size: 11px;"><b>'.number_format($value['14'], 0, 0, ' ') . '</b></td>';
                                          echo '<td align="left" style="padding: 0px;vertical-align:middle;font-size: 11px;"><b>'.number_format($value['15'], 0, 0, ' ') . '</b></td>';
                                          echo '<td align="left" style="padding: 0px;vertical-align:middle;font-size: 11px;"><b>'.number_format($value['16'], 0, 0, ' ') . '</b></td>';
                                          echo '<td align="left" style="padding: 0px;vertical-align:middle;font-size: 11px;"><b>'.number_format($value['17'], 0, 0, ' ') . '</b></td>';
                                          echo '<td align="left" style="padding: 0px;vertical-align:middle;font-size: 11px;"><b>'.number_format($value['18'], 0, 0, ' ') . '</b></td>';
                                          echo '<td align="left" style="padding: 0px;vertical-align:middle;font-size: 11px;"><b>'.number_format($value['19'], 0, 0, ' ') . '</b></td>';
                                          echo '<td align="left" style="padding: 0px;vertical-align:middle;font-size: 11px;"><b>'.number_format($value['20'], 0, 0, ' ') . '</b></td>';
                                          echo '<td align="left" style="padding: 0px;vertical-align:middle;font-size: 11px;"><b>'.number_format($value['21'], 0, 0, ' ') . '</b></td>';
                                          echo '<td align="left" style="padding: 0px;vertical-align:middle;font-size: 11px;"><b>'.number_format($value['22'], 0, 0, ' ') . '</b></td>';
                                          echo '<td align="left" style="padding: 0px;vertical-align:middle;font-size: 11px;"><b>'.number_format($value['23'], 0, 0, ' ') . '</b></td>';
                                          echo '<td align="left" style="padding: 0px;vertical-align:middle;font-size: 11px;"><b>'.number_format($value['24'], 0, 0, ' ') . '</b></td>';
                                          echo '<td align="left" style="padding: 0px;vertical-align:middle;font-size: 11px;"><b>'.number_format($value['25'], 0, 0, ' ') . '</b></td>';
                                          echo '<td align="left" style="padding: 0px;vertical-align:middle;font-size: 11px;"><b>'.number_format($value['26'], 0, 0, ' ') . '</b></td>';
                                          echo '<td align="left" style="padding: 0px;vertical-align:middle;font-size: 11px;"><b>'.number_format($value['27'], 0, 0, ' ') . '</b></td>';
                                          echo '<td align="left" style="padding: 0px;vertical-align:middle;font-size: 11px;"><b>'.number_format($value['28'], 0, 0, ' ') . '</b></td>';
                                          echo '<td align="left" style="padding: 0px;vertical-align:middle;font-size: 11px;"><b>'.number_format($value['29'], 0, 0, ' ') . '</b></td>';
                                          echo '<td align="left" style="padding: 0px;vertical-align:middle;font-size: 11px;"><b>'.number_format($value['30'], 0, 0, ' ') . '</b></td>';
                                          echo '<td align="left" style="padding: 0px;vertical-align:middle;font-size: 11px;"><b>'.number_format($value['31'], 0, 0, ' ') . '</b></td>';
                                          */
                                       }
                                       else {
                                          echo '<td align="left" style="padding: 0px;vertical-align:middle;font-size: 12px;">' . $value['account_name'] . '</td>';
                                          echo '<td align="left" style="padding: 0px;vertical-align:middle;font-size: 12px;">' . $value['service_name'] . '</td>';
                                          echo '<td align="left" style="padding: 0px;vertical-align:middle;font-size: 12px;">'.$value['service_credit_billing_type'].'</td>';
                                          echo '<td align="left" style="padding: 0px;vertical-align:middle;font-size: 12px;">'.$value['service_status'].'</td>';
                                          if($value['1'] == '0'){
                                             echo '<td align="left" style="padding: 0px;vertical-align:middle;font-size: 11px;">' . number_format($value['1'], 0, 0, ' ') . '</td>';
                                          }else{
                                             echo '<td align="left" style="padding: 0px;vertical-align:middle;font-size: 11px;background-color:#d4f9d9;">' . number_format($value['1'], 0, 0, ' ') . '</td>';
                                          };
                                          if($value['2'] == '0'){
                                             echo '<td align="left" style="padding: 0px;vertical-align:middle;font-size: 11px;">' . number_format($value['2'], 0, 0, ' ') . '</td>';
                                          }else{
                                             echo '<td align="left" style="padding: 0px;vertical-align:middle;font-size: 11px;background-color:#d4f9d9;">' . number_format($value['2'], 0, 0, ' ') . '</td>';
                                          };
                                          if($value['3'] == '0'){
                                             echo '<td align="left" style="padding: 0px;vertical-align:middle;font-size: 11px;">' . number_format($value['3'], 0, 0, ' ') . '</td>';
                                          }else{
                                             echo '<td align="left" style="padding: 0px;vertical-align:middle;font-size: 11px;background-color:#d4f9d9;">' . number_format($value['3'], 0, 0, ' ') . '</td>';
                                          };
                                          if($value['4'] == '0'){
                                             echo '<td align="left" style="padding: 0px;vertical-align:middle;font-size: 11px;">' . number_format($value['4'], 0, 0, ' ') . '</td>';
                                          }else{
                                             echo '<td align="left" style="padding: 0px;vertical-align:middle;font-size: 11px;background-color:#d4f9d9;">' . number_format($value['4'], 0, 0, ' ') . '</td>';
                                          };
                                          if($value['5'] == '0'){
                                             echo '<td align="left" style="padding: 0px;vertical-align:middle;font-size: 11px;">' . number_format($value['5'], 0, 0, ' ') . '</td>';
                                          }else{
                                             echo '<td align="left" style="padding: 0px;vertical-align:middle;font-size: 11px;background-color:#d4f9d9;">' . number_format($value['5'], 0, 0, ' ') . '</td>';
                                          };
                                          if($value['6'] == '0'){
                                             echo '<td align="left" style="padding: 0px;vertical-align:middle;font-size: 11px;">' . number_format($value['6'], 0, 0, ' ') . '</td>';
                                          }else{
                                             echo '<td align="left" style="padding: 0px;vertical-align:middle;font-size: 11px;background-color:#d4f9d9;">' . number_format($value['6'], 0, 0, ' ') . '</td>';
                                          };
                                          if($value['7'] == '0'){
                                             echo '<td align="left" style="padding: 0px;vertical-align:middle;font-size: 11px;">' . number_format($value['7'], 0, 0, ' ') . '</td>';
                                          }else{
                                             echo '<td align="left" style="padding: 0px;vertical-align:middle;font-size: 11px;background-color:#d4f9d9;">' . number_format($value['7'], 0, 0, ' ') . '</td>';
                                          };
                                          if($value['8'] == '0'){
                                             echo '<td align="left" style="padding: 0px;vertical-align:middle;font-size: 11px;">' . number_format($value['8'], 0, 0, ' ') . '</td>';
                                          }else{
                                             echo '<td align="left" style="padding: 0px;vertical-align:middle;font-size: 11px;background-color:#d4f9d9;">' . number_format($value['8'], 0, 0, ' ') . '</td>';
                                          };
                                          if($value['9'] == '0'){
                                             echo '<td align="left" style="padding: 0px;vertical-align:middle;font-size: 11px;">' . number_format($value['9'], 0, 0, ' ') . '</td>';
                                          }else{
                                             echo '<td align="left" style="padding: 0px;vertical-align:middle;font-size: 11px;background-color:#d4f9d9;">' . number_format($value['9'], 0, 0, ' ') . '</td>';
                                          };
                                          if($value['10'] == '0'){
                                             echo '<td align="left" style="padding: 0px;vertical-align:middle;font-size: 11px;">' . number_format($value['10'], 0, 0, ' ') . '</td>';
                                          }else{
                                             echo '<td align="left" style="padding: 0px;vertical-align:middle;font-size: 11px;background-color:#d4f9d9;">' . number_format($value['10'], 0, 0, ' ') . '</td>';
                                          };
                                          if($value['11'] == '0'){
                                             echo '<td align="left" style="padding: 0px;vertical-align:middle;font-size: 11px;">' . number_format($value['11'], 0, 0, ' ') . '</td>';
                                          }else{
                                             echo '<td align="left" style="padding: 0px;vertical-align:middle;font-size: 11px;background-color:#d4f9d9;">' . number_format($value['11'], 0, 0, ' ') . '</td>';
                                          };
                                          if($value['12'] == '0'){
                                             echo '<td align="left" style="padding: 0px;vertical-align:middle;font-size: 11px;">' . number_format($value['12'], 0, 0, ' ') . '</td>';
                                          }else{
                                             echo '<td align="left" style="padding: 0px;vertical-align:middle;font-size: 11px;background-color:#d4f9d9;">' . number_format($value['12'], 0, 0, ' ') . '</td>';
                                          };
                                          if($value['13'] == '0'){
                                             echo '<td align="left" style="padding: 0px;vertical-align:middle;font-size: 11px;">' . number_format($value['13'], 0, 0, ' ') . '</td>';
                                          }else{
                                             echo '<td align="left" style="padding: 0px;vertical-align:middle;font-size: 11px;background-color:#d4f9d9;">' . number_format($value['13'], 0, 0, ' ') . '</td>';
                                          };
                                          if($value['14'] == '0'){
                                             echo '<td align="left" style="padding: 0px;vertical-align:middle;font-size: 11px;">' . number_format($value['14'], 0, 0, ' ') . '</td>';
                                          }else{
                                             echo '<td align="left" style="padding: 0px;vertical-align:middle;font-size: 11px;background-color:#d4f9d9;">' . number_format($value['14'], 0, 0, ' ') . '</td>';
                                          };
                                          if($value['15'] == '0'){
                                             echo '<td align="left" style="padding: 0px;vertical-align:middle;font-size: 11px;">' . number_format($value['15'], 0, 0, ' ') . '</td>';
                                          }else{
                                             echo '<td align="left" style="padding: 0px;vertical-align:middle;font-size: 11px;background-color:#d4f9d9;">' . number_format($value['15'], 0, 0, ' ') . '</td>';
                                          };
                                          if($value['16'] == '0'){
                                             echo '<td align="left" style="padding: 0px;vertical-align:middle;font-size: 11px;">' . number_format($value['16'], 0, 0, ' ') . '</td>';
                                          }else{
                                             echo '<td align="left" style="padding: 0px;vertical-align:middle;font-size: 11px;background-color:#d4f9d9;">' . number_format($value['16'], 0, 0, ' ') . '</td>';
                                          };
                                          if($value['17'] == '0'){
                                             echo '<td align="left" style="padding: 0px;vertical-align:middle;font-size: 11px;">' . number_format($value['17'], 0, 0, ' ') . '</td>';
                                          }else{
                                             echo '<td align="left" style="padding: 0px;vertical-align:middle;font-size: 11px;background-color:#d4f9d9;">' . number_format($value['17'], 0, 0, ' ') . '</td>';
                                          };
                                          if($value['18'] == '0'){
                                             echo '<td align="left" style="padding: 0px;vertical-align:middle;font-size: 11px;">' . number_format($value['18'], 0, 0, ' ') . '</td>';
                                          }else{
                                             echo '<td align="left" style="padding: 0px;vertical-align:middle;font-size: 11px;background-color:#d4f9d9;">' . number_format($value['18'], 0, 0, ' ') . '</td>';
                                          };
                                          if($value['19'] == '0'){
                                             echo '<td align="left" style="padding: 0px;vertical-align:middle;font-size: 11px;">' . number_format($value['19'], 0, 0, ' ') . '</td>';
                                          }else{
                                             echo '<td align="left" style="padding: 0px;vertical-align:middle;font-size: 11px;background-color:#d4f9d9;">' . number_format($value['19'], 0, 0, ' ') . '</td>';
                                          };
                                          if($value['20'] == '0'){
                                             echo '<td align="left" style="padding: 0px;vertical-align:middle;font-size: 11px;">' . number_format($value['20'], 0, 0, ' ') . '</td>';
                                          }else{
                                             echo '<td align="left" style="padding: 0px;vertical-align:middle;font-size: 11px;background-color:#d4f9d9;">' . number_format($value['20'], 0, 0, ' ') . '</td>';
                                          };
                                          if($value['21'] == '0'){
                                             echo '<td align="left" style="padding: 0px;vertical-align:middle;font-size: 11px;">' . number_format($value['21'], 0, 0, ' ') . '</td>';
                                          }else{
                                             echo '<td align="left" style="padding: 0px;vertical-align:middle;font-size: 11px;background-color:#d4f9d9;">' . number_format($value['21'], 0, 0, ' ') . '</td>';
                                          };
                                          if($value['22'] == '0'){
                                             echo '<td align="left" style="padding: 0px;vertical-align:middle;font-size: 11px;">' . number_format($value['22'], 0, 0, ' ') . '</td>';
                                          }else{
                                             echo '<td align="left" style="padding: 0px;vertical-align:middle;font-size: 11px;background-color:#d4f9d9;">' . number_format($value['22'], 0, 0, ' ') . '</td>';
                                          };
                                          if($value['23'] == '0'){
                                             echo '<td align="left" style="padding: 0px;vertical-align:middle;font-size: 11px;">' . number_format($value['23'], 0, 0, ' ') . '</td>';
                                          }else{
                                             echo '<td align="left" style="padding: 0px;vertical-align:middle;font-size: 11px;background-color:#d4f9d9;">' . number_format($value['23'], 0, 0, ' ') . '</td>';
                                          };
                                          if($value['24'] == '0'){
                                             echo '<td align="left" style="padding: 0px;vertical-align:middle;font-size: 11px;">' . number_format($value['24'], 0, 0, ' ') . '</td>';
                                          }else{
                                             echo '<td align="left" style="padding: 0px;vertical-align:middle;font-size: 11px;background-color:#d4f9d9;">' . number_format($value['24'], 0, 0, ' ') . '</td>';
                                          };
                                          if($value['25'] == '0'){
                                             echo '<td align="left" style="padding: 0px;vertical-align:middle;font-size: 11px;">' . number_format($value['25'], 0, 0, ' ') . '</td>';
                                          }else{
                                             echo '<td align="left" style="padding: 0px;vertical-align:middle;font-size: 11px;background-color:#d4f9d9;">' . number_format($value['25'], 0, 0, ' ') . '</td>';
                                          };
                                          if($value['26'] == '0'){
                                             echo '<td align="left" style="padding: 0px;vertical-align:middle;font-size: 11px;">' . number_format($value['26'], 0, 0, ' ') . '</td>';
                                          }else{
                                             echo '<td align="left" style="padding: 0px;vertical-align:middle;font-size: 11px;background-color:#d4f9d9;">' . number_format($value['26'], 0, 0, ' ') . '</td>';
                                          };
                                          if($value['27'] == '0'){
                                             echo '<td align="left" style="padding: 0px;vertical-align:middle;font-size: 11px;">' . number_format($value['27'], 0, 0, ' ') . '</td>';
                                          }else{
                                             echo '<td align="left" style="padding: 0px;vertical-align:middle;font-size: 11px;background-color:#d4f9d9;">' . number_format($value['27'], 0, 0, ' ') . '</td>';
                                          };
                                          if($value['28'] == '0'){
                                             echo '<td align="left" style="padding: 0px;vertical-align:middle;font-size: 11px;">' . number_format($value['28'], 0, 0, ' ') . '</td>';
                                          }else{
                                             echo '<td align="left" style="padding: 0px;vertical-align:middle;font-size: 11px;background-color:#d4f9d9;">' . number_format($value['28'], 0, 0, ' ') . '</td>';
                                          };
                                          if($value['29'] == '0'){
                                             echo '<td align="left" style="padding: 0px;vertical-align:middle;font-size: 11px;">' . number_format($value['29'], 0, 0, ' ') . '</td>';
                                          }else{
                                             echo '<td align="left" style="padding: 0px;vertical-align:middle;font-size: 11px;background-color:#d4f9d9;">' . number_format($value['29'], 0, 0, ' ') . '</td>';
                                          };
                                          if($value['30'] == '0'){
                                             echo '<td align="left" style="padding: 0px;vertical-align:middle;font-size: 11px;">' . number_format($value['30'], 0, 0, ' ') . '</td>';
                                          }else{
                                             echo '<td align="left" style="padding: 0px;vertical-align:middle;font-size: 11px;background-color:#d4f9d9;">' . number_format($value['30'], 0, 0, ' ') . '</td>';
                                          };
                                          if($value['31'] == '0'){
                                             echo '<td align="left" style="padding: 0px;vertical-align:middle;font-size: 11px;">' . number_format($value['31'], 0, 0, ' ') . '</td>';
                                          }else{
                                             echo '<td align="left" style="padding: 0px;vertical-align:middle;font-size: 11px;background-color:#d4f9d9;">' . number_format($value['31'], 0, 0, ' ') . '</td>';
                                          };

                                       }

                                       $service_name_new = $service_name;
                                    }
                                    else
                                    {
                                       echo '<td align="left" style="padding: 0px;vertical-align:middle;font-size: 11px;">' . number_format($value['1'], 0, 0, ' ') . '</td>';
                                       echo '<td align="left" style="padding: 0px;vertical-align:middle;font-size: 11px;">' . number_format($value['2'], 0, 0, ' ') . '</td>';
                                       echo '<td align="left" style="padding: 0px;vertical-align:middle;font-size: 11px;">' . number_format($value['3'], 0, 0, ' ') . '</td>';
                                       echo '<td align="left" style="padding: 0px;vertical-align:middle;font-size: 11px;">' . number_format($value['4'], 0, 0, ' ') . '</td>';
                                       echo '<td align="left" style="padding: 0px;vertical-align:middle;font-size: 11px;">' . number_format($value['5'], 0, 0, ' ') . '</td>';
                                       echo '<td align="left" style="padding: 0px;vertical-align:middle;font-size: 11px;">' . number_format($value['6'], 0, 0, ' ') . '</td>';
                                       echo '<td align="left" style="padding: 0px;vertical-align:middle;font-size: 11px;">' . number_format($value['7'], 0, 0, ' ') . '</td>';
                                       echo '<td align="left" style="padding: 0px;vertical-align:middle;font-size: 11px;">' . number_format($value['8'], 0, 0, ' ') . '</td>';
                                       echo '<td align="left" style="padding: 0px;vertical-align:middle;font-size: 11px;">' . number_format($value['9'], 0, 0, ' ') . '</td>';
                                       echo '<td align="left" style="padding: 0px;vertical-align:middle;font-size: 11px;">' . number_format($value['10'], 0, 0, ' ') . '</td>';
                                       echo '<td align="left" style="padding: 0px;vertical-align:middle;font-size: 11px;">' . number_format($value['11'], 0, 0, ' ') . '</td>';
                                       echo '<td align="left" style="padding: 0px;vertical-align:middle;font-size: 11px;">' . number_format($value['12'], 0, 0, ' ') . '</td>';
                                       echo '<td align="left" style="padding: 0px;vertical-align:middle;font-size: 11px;">' . number_format($value['13'], 0, 0, ' ') . '</td>';
                                       echo '<td align="left" style="padding: 0px;vertical-align:middle;font-size: 11px;">' . number_format($value['14'], 0, 0, ' ') . '</td>';
                                       echo '<td align="left" style="padding: 0px;vertical-align:middle;font-size: 11px;">' . number_format($value['15'], 0, 0, ' ') . '</td>';
                                       echo '<td align="left" style="padding: 0px;vertical-align:middle;font-size: 11px;">' . number_format($value['16'], 0, 0, ' ') . '</td>';
                                       echo '<td align="left" style="padding: 0px;vertical-align:middle;font-size: 11px;">' . number_format($value['17'], 0, 0, ' ') . '</td>';
                                       echo '<td align="left" style="padding: 0px;vertical-align:middle;font-size: 11px;">' . number_format($value['18'], 0, 0, ' ') . '</td>';
                                       echo '<td align="left" style="padding: 0px;vertical-align:middle;font-size: 11px;">' . number_format($value['19'], 0, 0, ' ') . '</td>';
                                       echo '<td align="left" style="padding: 0px;vertical-align:middle;font-size: 11px;">' . number_format($value['20'], 0, 0, ' ') . '</td>';
                                       echo '<td align="left" style="padding: 0px;vertical-align:middle;font-size: 11px;">' . number_format($value['21'], 0, 0, ' ') . '</td>';
                                       echo '<td align="left" style="padding: 0px;vertical-align:middle;font-size: 11px;">' . number_format($value['22'], 0, 0, ' ') . '</td>';
                                       echo '<td align="left" style="padding: 0px;vertical-align:middle;font-size: 11px;">' . number_format($value['23'], 0, 0, ' ') . '</td>';
                                       echo '<td align="left" style="padding: 0px;vertical-align:middle;font-size: 11px;">' . number_format($value['24'], 0, 0, ' ') . '</td>';
                                       echo '<td align="left" style="padding: 0px;vertical-align:middle;font-size: 11px;">' . number_format($value['25'], 0, 0, ' ') . '</td>';
                                       echo '<td align="left" style="padding: 0px;vertical-align:middle;font-size: 11px;">' . number_format($value['26'], 0, 0, ' ') . '</td>';
                                       echo '<td align="left" style="padding: 0px;vertical-align:middle;font-size: 11px;">' . number_format($value['27'], 0, 0, ' ') . '</td>';
                                       echo '<td align="left" style="padding: 0px;vertical-align:middle;font-size: 11px;">' . number_format($value['28'], 0, 0, ' ') . '</td>';
                                       echo '<td align="left" style="padding: 0px;vertical-align:middle;font-size: 11px;">' . number_format($value['29'], 0, 0, ' ') . '</td>';
                                       echo '<td align="left" style="padding: 0px;vertical-align:middle;font-size: 11px;">' . number_format($value['30'], 0, 0, ' ') . '</td>';
                                       echo '<td align="left" style="padding: 0px;vertical-align:middle;font-size: 11px;">' . number_format($value['31'], 0, 0, ' ') . '</td>';
                                    }
                                 }

                                 if ($year_new != '') {
                                    echo '</tr>';
                                 }

                                 ?>
                                 </tbody>
                              </table>
                           </div><!-- /.box-body -->
                        </div><!-- /.box -->
                     </div>
                  </div>
               </section>
            </div>
      </div>
   </section>




</aside>

<!--  THE JAVASCRIPT IMPORT ALWAYS APPEARS BEFORE LOCAL JAVASCRIPT AT THE BOTTOM OF THE PAGE -->
                                 <?php include("template_import_script.php"); //must import all scripts first    ?>
<!-- END JAVASCRIPT IMPORT -->
<script src="../js/plugins/datatables/jquery.dataTables.js" type="text/javascript"></script>

<script src="../js/plugins/datatables/dataTables.bootstrap.js" type="text/javascript"></script>

<script type="text/javascript">
   $(window).load(function () {
      $(".loader").fadeOut("slow");
      $(".loaderIcon").fadeOut("slow");
   })
</script>

<script type="text/javascript">

   $(document).ready(function() {
      //Sales Table Sorting
      $('#DailyTotalsTable').DataTable( {
         "bPaginate": false, //Remove pageing frmo the datatable
         "bLengthChange": false, //Remove the show records per page control
         "aaSorting": [[0,'asc']],
         "aoColumns": [
            null,
            null,
            null,
            null,
            null,
            null,
            null,
            null,
            null,
            null,
            null,
            null,
            null,
            null,
            null,
            null,
            null,
            null,
            null,
            null,
            null,
            null,
            null,
            null,
            null,
            null,
            null,
            null,
            null,
            null,
            null,
            null,
            null,
            null,
            null
            //{ "sType": "currency" },
            //{ "sType": "currency" },
            //{ "sType": "currency" },
            //{ "sType": "currency" },
            //{ "sType": "currency" }
         ]
      } );

   } );



      function open_container(monthname,year)
      {
         //run our file processor before submitting it to niel
         window.location.href = "../pages/financialtotals.php?monthname=" + monthname + "&year=" + year;

      }




</script>

<!-- Template Footer -->

