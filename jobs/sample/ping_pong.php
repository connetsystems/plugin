<?php

define('ERROR_REPORTING', true);
require_once('ConnetAutoloader.php');
preset::daemon();

//--------------------------------------------------------------
// Client
//--------------------------------------------------------------     
$client = new RedisJobClient();
$job_id = $client->callBackground('ping', 'Ping...');

//--------------------------------------------------------------
// Worker
//--------------------------------------------------------------     
$worker = new RedisJobWorker();
$worker->addServer();
$worker->addFunction('ping', function (RedisJob $job) {

   $job->sendResponse('Pong!');
});

//--------------------------------------------------------------
// Loop
//--------------------------------------------------------------     
while (false !== ($job = $worker->work())) {

   if (!is_null($job)) {

      clog('Finished Job:', $job['job_id']);
   }
}

//--------------------------------------------------------------
// Exit
//--------------------------------------------------------------     
clog::info('* Exit');
die();

//********************** end of file ******************************
