<?php

//--------------------------------------------------------------
// Constants
//--------------------------------------------------------------     
define('ERROR_REPORTING', true);
define('LOG_FILENAME', '/var/log/connet/plugin/sample_network_lookup_job.log');

//--------------------------------------------------------------
// Includes
//--------------------------------------------------------------     
require_once('ConnetAutoloader.php');
preset::daemon();

require_once('../../helpers/NetworkLookupHelper.php');

//--------------------------------------------------------------
// Worker
//--------------------------------------------------------------   
class SampleNetworkLookupJob {

   /**
    * File Line Count
    * 
    * @throws Exception
    */
   static function getLineCount($filename) {
      // <editor-fold defaultstate="collapsed">
      if (false === ($filename = realpath($filename))) {

         throw new Exception('File not found');
      }

      $output = array();
      exec($cmd = 'wc -l "' . str_replace('"', '\"', realpath($filename)) . '"', $output);
      return (integer) explode(' ', $output[0])[0];
      // </editor-fold>
   }

   /**
    * Process Job
    * 
    * @param RedisJob $job
    * @throws Exception
    */
   static function process(RedisJob $job) {
      // <editor-fold defaultstate="collapsed">
      //--------------------------------------------------------------
      // Force Prefix Reload
      //--------------------------------------------------------------         
      NetworkLookupHelper::fetchPrefixes();

      //--------------------------------------------------------------
      // Line Count
      //--------------------------------------------------------------   
      $lines = self::getLineCount($job['filename']);

      //--------------------------------------------------------------
      // Open
      //--------------------------------------------------------------         
      if (false === ($handle = fopen($job['filename'], 'r'))) {

         throw new Exception('Cannot open file');
      }

      clog('Processing Job:', $job->getJobId(), 'Filename:', basename($job['filename']));
      $job->sendProgress(0, $lines, 'Processing');

      //--------------------------------------------------------------
      // Process
      //-------------------------------------------------------------- 
      $count = $progress = $empty = $invalid = $unknown = $ported = $duplicate = 0;
      $networks = array();
      $dummy = array();
      $start = microtime(true);

      //--------------------------------------------------------------
      // Read
      //--------------------------------------------------------------  
      $data = fgetcsv($handle);
      while ($data !== false) {

         //--------------------------------------------------------------
         // Fetch a couple of rows for Bulk MNP Lookup
         //--------------------------------------------------------------               
         $msisdn_array = array();
         do {

            $count++;

            //--------------------------------------------------------------
            // Empty
            //--------------------------------------------------------------            
            if (!isset($data[0])) {

               $empty++;
               continue;
            }

            //--------------------------------------------------------------
            // Msisdn
            //--------------------------------------------------------------             
            $msisdn = preg_replace("/[^0-9]/", '', $data[0]);
            if (!isset($msisdn[7])) {

               $invalid++;
               continue;
            }

            if ($msisdn[0] == '0') {
               $msisdn = '27' . substr($msisdn, 1);
            }

            //--------------------------------------------------------------
            // Duplicate
            //--------------------------------------------------------------              
            if (isset($dummy[$msisdn])) {

               $duplicate++;
               continue;
            }

            $dummy[$msisdn] = 1;

            $msisdn_array[] = $msisdn;
         } while (false !== ($data = fgetcsv($handle)) && $count < 50);


         //--------------------------------------------------------------
         // Network Lookup
         //--------------------------------------------------------------             
         foreach (NetworkLookupHelper::networkLookup($msisdn_array) as $lookup) {

            if (is_null($lookup['mccmnc'])) {

               $unknown++;
               continue;
            }

            if ($lookup['ported']) {
               $ported++;
            }

            $mccmnc = $lookup['mccmnc'];
            if (isset($networks[$mccmnc])) {

               $networks[$mccmnc] ++;
            } else {

               $networks[$mccmnc] = 1;
            }
         }

         //--------------------------------------------------------------
         // Statistics
         //--------------------------------------------------------------           
         $elapsed = microtime(true) - $start;
         if ($data === false || $elapsed >= 1) {

            $progress += $count;

            clog::info(implode(' ', array(
                'Processed:', number_format($progress), '/', number_format($lines),
                '(' . number_format($count / $elapsed, 3) . ' /s)'
            )));
            $start = microtime(true);
            $count = 0;

            //--------------------------------------------------------------
            // Progress
            //--------------------------------------------------------------               
            $job->sendResponse(array(
                'lines' => $lines,
                'empty' => $empty,
                'invalid' => $invalid,
                'duplicate' => $duplicate,
                'networks' => array(
                    'ported' => $ported,
                    'unknown' => $unknown,
                    'mccmnc' => $networks,
                )
            ));

            $job->sendProgress($progress, $lines, 'Processing');
         }
      }

      //--------------------------------------------------------------
      // Done
      //--------------------------------------------------------------         
      $job->sendProgress($progress, $lines, 'Completed');
      fclose($handle);
      // </editor-fold>
   }

}

//--------------------------------------------------------------
// Loop
//--------------------------------------------------------------     
$worker = new RedisJobWorker();
$worker->addServer('appstaging1')->addFunction('sample_network_lookup', 'SampleNetworkLookupJob::process');

//--------------------------------------------------------------
// Loop
//--------------------------------------------------------------  
clog::info('* Entering Main Loop');
while (false !== ($job_id = $worker->work())) {

   if (!is_null($job_id)) {

      clog::info('* Finished Job:', $job_id);
   }
}

//--------------------------------------------------------------
// Exit
//--------------------------------------------------------------  
clog::info('* Exit');
die();

//********************** end of file ******************************
