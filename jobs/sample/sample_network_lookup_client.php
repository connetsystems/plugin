<?php

//--------------------------------------------------------------
// Includes
//--------------------------------------------------------------     
define('ERROR_REPORTING', true);
require_once('ConnetAutoloader.php');
preset::daemon();

//--------------------------------------------------------------
// Test Client
//--------------------------------------------------------------     
$client = new RedisJobClient('appstaging1');
$job_id = $client->callBackground('sample_network_lookup', array(
    'filename' => '/var/spool/connet/mnp_bulk/cm_z3WYrfcab884aec0ee543371423cd3d9f97108.csv.columns.10k.content-appstaging1553a46f5c6d535.07863564.content',
        ));

clog('Job Submitted:', $job_id);
         
while (false !== ($state = $client->getState($job_id)) && !daemon::terminated()) {


   if ($state != RedisJob::STATE_QUEUED) {

      list($min, $max, $string) = $client->getProgress($job_id);
      clog('Job Processing:', $job_id, 'Progress:', number_format($min), '/', number_format($max), '-', $string);
          
      switch ($state) {

         case RedisJob::STATE_SUCCESS:
            clog('Job Finished:', $job_id, 'Response:', $client->getResponse($job_id));
            $client->deleteJob($job_id);
            break;
         case RedisJob::STATE_ERROR:
            clog('Job Failed:', $job_id, 'Error:', $client->getError($job_id));
            $client->deleteJob($job_id);
            break;
         case RedisJob::STATE_EXCEPTION:
            clog('Job Exception:', $job_id, 'Error:', $client->getException($job_id));
            $client->deleteJob($job_id);
            break;
      }
   }

   sleep(1);
}


//--------------------------------------------------------------
// Exit
//--------------------------------------------------------------  
clog::info('* Exit');
die();

//********************** end of file ******************************